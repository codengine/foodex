__author__ = 'sohel'

from django.db import models
from foodex.models.clients.retailers import Retailers

class PendingRetailerManager(models.Manager):
    def get_queryset(self):
        return super(PendingRetailerManager, self).get_queryset().filter(type=Retailers.__name__,is_approved=False)
