__author__ = 'sohel'

from django.db import models

class ApprovedRetailerManager(models.Manager):
    def get_queryset(self):
        return super(ApprovedRetailerManager, self).get_queryset().filter(type="Retailers",is_approved=True)
