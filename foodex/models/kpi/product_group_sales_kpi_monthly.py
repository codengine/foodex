import calendar
from decimal import Decimal
from datetime import datetime
from datetime import timedelta
from django.db import models
from django.db.models.query_utils import Q
from django.db import transaction
from openpyxl.styles import Style
from openpyxl.styles.alignment import Alignment
from openpyxl.styles.protection import Protection
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig
from blackwidow.core.models.config.exporter_config import ExporterConfig
from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
from blackwidow.core.models.config.importer_config import ImporterConfig
from blackwidow.core.models.contracts.kpi_base import MonthlyKPI
from blackwidow.core.models.log.logs import ErrorLog
from blackwidow.core.models.organizations.organization import Organization
from blackwidow.core.models.roles.role import Role
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.engine.decorators.enable_export import enable_export
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, get_models_with_decorator
from blackwidow.engine.extensions.clock import Clock
from config.apps import INSTALLED_APPS
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum
from foodex.helpers.query_helper import QueryHelper
from foodex.models.kpi.target_product_group import TargetProductGroup
from foodex.models.kpi.unit_product_sales_monthly_kpi import UnitProductSalesMonthlyKPI

__author__ = 'Sohel'

@decorate(enable_import, enable_export, route(route='product-group-sales-kpi-monthly', module=ModuleEnum.Targets, display_name='Product Group Monthly Sales KPI', group='Targets'))
class ProductGroupSalesMonthlyKPI(MonthlyKPI):
    group = models.ForeignKey(TargetProductGroup)

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.AdvancedImport, ViewActionEnum.AdvancedExport]

    @classmethod
    def get_button_title(cls, button=ViewActionEnum.Details):
        if button == ViewActionEnum.AdvancedExport:
            return "Download this table"
        elif button == ViewActionEnum.AdvancedImport:
            return "Upload changes to this table"

    @classmethod
    def get_unit_kpi_enabled_product_groups(cls, **kwargs):
        request = kwargs.get('request')
        filter_enabled = False
        if request:
            filter_id = request.GET.get('group:id')
            filter_enabled = request.GET.get('filter') == 'true'
            try:
                if filter_enabled:
                    filter_id = int(filter_id)
            except Exception as exp:
                filter_enabled = False
        product_groups = TargetProductGroup.objects.all()
        if filter_enabled:
            product_groups = product_groups.filter(pk=filter_id)
        product_groups = product_groups.exclude(is_active=False)
        if request and not request.GET.get('filter'):
            first_group = product_groups.first()
            if first_group:
                product_groups = product_groups.filter(pk=first_group.pk)
        return product_groups

    @classmethod
    def table_columns(cls):
        return "code", "user", "month_name", "year", "product", "target", "achieved"

    @classmethod
    def get_left_cols_fixed(cls):
        return ( "Name", )

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', "user", 'month_name', 'year', 'product', 'target', 'achieved']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        return d

    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        ExporterConfig.objects.filter(model=cls.__name__, organization=organization).delete()
        exporter_config, created = ExporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)

        columns = [
            ExporterColumnConfig(column=0, column_name='ID', property_name='id', ignore=False),
            ExporterColumnConfig(column=1, column_name='Name', property_name='name', ignore=False),
            ExporterColumnConfig(column=2, column_name='Group Id', property_name='group_id', ignore=False),
            ExporterColumnConfig(column=3, column_name='Group Name', property_name='group_name', ignore=False),
        ]
        for i in range(0,48):
            columns += [ExporterColumnConfig(column=i + 4, column_name='value'+str(i), property_name='render_value_'+str(i), ignore=False)]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)

        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):
        # workbook.protection.sheet = True
        return self.pk, row_number + 1

    @classmethod
    def initialize_export(cls, workbook=None, columns=None,  query_set=None, **kwargs):
        # workbook.protection.sheet = True
        center_style = Style(alignment=Alignment(horizontal='center', vertical='center'))
        if len(columns) < 50:
            return workbook, 1

        row = 1
        workbook.cell(row=row, column=1).value = "ID"
        workbook.cell(row=row, column=2).value = "Name"
        workbook.cell(row=row, column=3).value = "Group ID"
        workbook.cell(row=row, column=4).value = "Group Name"
        start_column = 4
        end_column = 5
        niteration = 0
        for i in range(len(columns) - 4):
            if i % 4 == 0:
                start_column = i + 5
            if i % 4 == 3:
                end_column = i + 5
                niteration += 1
                workbook.cell(row=row, column=start_column).value = cls.map_month_name(niteration)
                workbook.cell(row=row, column=start_column).style = center_style
                workbook.merge_cells(start_row=row, start_column=start_column, end_row=row, end_column=end_column)

        now_dt = Clock.now()
        current_year = now_dt.year
        previous_year = current_year - 1
        row = 2
        for i in range(len(columns) - 4):
            if i % 2 == 0:
                start_column = i + 5
            if i % 2 == 1:
                end_column = i + 5
                if i % 4 == 1:
                    workbook.cell(row=row, column=start_column).value = str(previous_year)
                elif i % 4 == 3:
                    workbook.cell(row=row, column=start_column).value = str(current_year)
                workbook.cell(row=row, column=start_column).style = center_style
                workbook.merge_cells(start_row=row, start_column=start_column, end_row=row, end_column=end_column)

        row = 3
        for i in range(len(columns) - 4):
            if i % 2 == 0:
                workbook.cell(row=row, column= i + 5 ).value = "Target"
            elif i % 2 == 1:
                workbook.cell(row=row, column= i + 5 ).value = "Actual"

        workbook.cell(row=1, column=1).style = center_style
        workbook.merge_cells(start_row=1, start_column=1, end_row=3, end_column=1)
        workbook.cell(row=1, column=2).style = center_style
        workbook.merge_cells(start_row=1, start_column=2, end_row=3, end_column=2)
        workbook.cell(row=1, column=3).style = center_style
        workbook.merge_cells(start_row=1, start_column=3, end_row=3, end_column=3)
        workbook.cell(row=1, column=4).style = center_style
        workbook.merge_cells(start_row=1, start_column=4, end_row=3, end_column=4)

        tday = Clock.now()
        current_year = tday.year
        previous_year = current_year - 1

        current_month = tday.month
        row_number = 4

        product_groups = cls.get_unit_kpi_enabled_product_groups(user=kwargs.get('user'), request=kwargs.get('request'))
        ###Now get all users with enable_kpi decorator.
        kpi_enabled_models = get_models_with_decorator('enable_kpi', INSTALLED_APPS, include_class=True)
        for _model_class in kpi_enabled_models:
            all_users = _model_class.objects.all()

            user_ids = all_users.values_list('pk', flat=True)

            product_group_ids = product_groups.values_list('pk', flat=True)

            user_kpis = cls.fetch_user_kpis(user_ids, previous_year, current_year, product_group_ids)

            def find_kpi_for_user(user_id, year, month, target_or_actual, group_id):
                try:
                    kpi_value = user_kpis[user_id][year][month][group_id][target_or_actual]
                    return kpi_value
                except:
                    pass

            for each_user in all_users:
                for pg in product_groups:
                    month_index = 1
                    protection = Protection(hidden=True, locked=True)
                    protected_style = Style(protection=protection)
                    for index, column in enumerate(columns):
                        ccolumn = column.column + 1
                        should_protected = False
                        if month_index < current_month:
                            should_protected = True
                        cell_value = ''
                        if index == 0:
                            cell_value = each_user.pk
                            workbook.cell(row=row_number, column=ccolumn).value = cell_value
                            workbook.cell(row=row_number, column=ccolumn).style = protected_style
                        elif index == 1:
                            cell_value = each_user.name
                            workbook.cell(row=row_number, column=ccolumn).value = cell_value
                            workbook.cell(row=row_number, column=ccolumn).style = protected_style
                        elif index == 2:
                            cell_value = pg.pk
                            workbook.cell(row=row_number, column=ccolumn).value = cell_value
                            workbook.cell(row=row_number, column=ccolumn).style = protected_style
                        elif index == 3:
                            cell_value = pg.name
                            workbook.cell(row=row_number, column=ccolumn).value = cell_value
                            workbook.cell(row=row_number, column=ccolumn).style = protected_style
                        else:
                            mindex = index - 4
                            if mindex % 4 == 0: ###Previous Year Target
                                cell_value = find_kpi_for_user(each_user.pk, previous_year, month_index, "target", pg.pk) #cls.get_target(each_user.pk, previous_year, month_index)
                                workbook.cell(row=row_number, column=ccolumn).value = cell_value
                                if should_protected:
                                    workbook.cell(row=row_number, column=ccolumn).style = protected_style
                            elif mindex % 4 == 1: ###Previous Year Actual.
                                cell_value = find_kpi_for_user(each_user.pk, previous_year, month_index, "actual", pg.pk) #cls.get_actual(each_user.pk, previous_year, month_index)
                                workbook.cell(row=row_number, column=ccolumn).value = cell_value
                                if should_protected:
                                    workbook.cell(row=row_number, column=ccolumn).style = protected_style
                            elif mindex % 4 == 2: ###Current Year Target
                                if month_index <= current_month:
                                    cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "target", pg.pk) #cls.get_target(each_user.pk, current_year, month_index)
                                    workbook.cell(row=row_number, column=ccolumn).value = cell_value
                                    if should_protected:
                                        workbook.cell(row=row_number, column=ccolumn).style = protected_style
                                else:

                                    cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "target", pg.pk) #cls.get_target(each_user.pk, current_year, month_index)
                                    workbook.cell(row=row_number, column=ccolumn).value = cell_value
                            elif mindex % 4 == 3: ###Current Year Actual
                                if month_index <= current_month:
                                    cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "actual", pg.pk) #cls.get_actual(each_user.pk, current_year, month_index)
                                    workbook.cell(row=row_number, column=ccolumn).value = cell_value
                                    if should_protected:
                                        workbook.cell(row=row_number, column=ccolumn).style = protected_style
                                else:
                                    cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "actual", pg.pk) #cls.get_actual(each_user.pk, current_year, month_index)
                                    workbook.cell(row=row_number, column=ccolumn).value = cell_value

                                ###Index Month Index After Each Index.
                                month_index += 1
                    row_number += 1

        return workbook, row_number

    @classmethod
    def finalize_export(cls, workbook=None, row_count=None, **kwargs):
        file_name = '%s_%s' % (cls.__name__, Clock.timestamp())
        return workbook, file_name

    @classmethod
    def fetch_user_kpis(cls, user_ids, previous_year, current_year, group_ids):
        all_kpi_objects = cls.objects.filter(user_id__in=user_ids, year__in=[ previous_year, current_year ], month__in=[ (i + 1) for i in range(12) ], group_id__in=group_ids)
        user_kpis = {}
        for kpi in all_kpi_objects:
            if not kpi.user_id in user_kpis:
                user_kpis[kpi.user_id] = {
                    kpi.year: {
                        kpi.month: {
                            kpi.group_id: {
                                "target": kpi.target,
                                "actual": kpi.achieved
                            }
                        }
                    }
                }
            else:

                if not kpi.year in user_kpis[kpi.user_id]:
                    user_kpis[kpi.user_id][kpi.year] = {
                        kpi.month: {
                            kpi.group_id: {
                                "target": kpi.target,
                                "actual": kpi.achieved
                            }
                        }
                    }
                else:
                    if not kpi.month in user_kpis[kpi.user_id][kpi.year]:
                        user_kpis[kpi.user_id][kpi.year][kpi.month] = {
                            kpi.group_id: {
                                "target": kpi.target,
                                "actual": kpi.achieved
                            }
                        }
                    else:
                        if not kpi.group_id in user_kpis[kpi.user_id][kpi.year][kpi.month]:
                            user_kpis[kpi.user_id][kpi.year][kpi.month][kpi.group_id] = {
                                "target": kpi.target,
                                "actual": kpi.achieved
                            }

        return user_kpis

    @classmethod
    def get_kpi_data_rows(cls, **kwargs):
        rows = []
        tday = Clock.now()
        current_year = tday.year
        previous_year = current_year - 1

        current_month = tday.month
        product_groups = cls.get_unit_kpi_enabled_product_groups(**kwargs)

        user_kpis = {}

        ###Now get all users with enable_kpi decorator.
        kpi_enabled_models = get_models_with_decorator('enable_kpi', INSTALLED_APPS, include_class=True)
        for _model_class in kpi_enabled_models:
            all_users = _model_class.objects.all()

            user_ids = all_users.values_list('pk', flat=True)

            product_group_ids = product_groups.values_list('pk', flat=True)

            user_kpis = cls.fetch_user_kpis(user_ids, previous_year, current_year, product_group_ids)

            def find_kpi_for_user(user_id, year, month, target_or_actual, group_id):
                try:
                    kpi_value = user_kpis[user_id][year][month][group_id][target_or_actual]
                    return kpi_value
                except:
                    pass

            for each_user in all_users:
                for pg in product_groups:
                    row = []
                    month_index = 1
                    for index in range(49):
                        cell_value = ''
                        if index == 0:
                            cell_value = each_user
                        else:
                            mindex = index - 1
                            if mindex % 4 == 0: ###Previous Year Target
                                cell_value = find_kpi_for_user(each_user.pk, previous_year, month_index, "target", pg.pk)
                            elif mindex % 4 == 1: ###Previous Year Actual.
                                cell_value = find_kpi_for_user(each_user.pk, previous_year, month_index, "actual", pg.pk)
                            elif mindex % 4 == 2: ###Current Year Target
                                if month_index <= current_month:
                                    cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "target", pg.pk)
                                else:

                                    cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "target", pg.pk)
                            elif mindex % 4 == 3: ###Current Year Actual
                                if month_index <= current_month:
                                    cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "actual", pg.pk)
                                else:
                                    cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "actual", pg.pk)

                                ###Index Month Index After Each Index.
                                month_index += 1
                        if not cell_value:
                            cell_value = '-'
                        row += [ cell_value ]
                    rows += [ row ]
        return rows

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        ImporterConfig.objects.filter(model=cls.__name__, organization=organization).delete()
        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        importer_config.starting_row = 3
        importer_config.starting_column = 0
        importer_config.save(**kwargs)
        columns = [
            ImporterColumnConfig(column=0, column_name='ID', property_name='id', ignore=False),
            ImporterColumnConfig(column=1, column_name='Name', property_name='name', ignore=False),
            ImporterColumnConfig(column=2, column_name='Group Id', property_name='group_id', ignore=False),
            ImporterColumnConfig(column=3, column_name='Group Name', property_name='group_name', ignore=False),
        ]
        for i in range(0,48):
                columns += [ImporterColumnConfig(column=i + 4, column_name='value'+str(i), property_name='render_value', ignore=False)]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        return data

    @classmethod
    def process_user_data(cls,user_id, user_name, group_id, group_name, current_month, previous_month, current_year, previous_year, item, user=None, organization=None, **kwargs):
        editable_from = previous_month * 4
        sorted_items = sorted(item.items(), key=lambda x: int(x[0]))
        start_day = 1
        month_index = 1
        for index, (key, value) in enumerate(sorted_items):
            weekday, end_day = calendar.monthrange(current_year, month_index)
            start_ts = Clock.get_timestamp_from_date(str(start_day)+"/"+str(month_index)+"/"+str(current_year)+" 00:00:00")
            end_ts = Clock.get_timestamp_from_date(str(end_day)+"/"+str(month_index)+"/"+str(current_year)+" 00:00:00")
            key = int(key)
            if index < editable_from - 1:

                if index % 4 == 3:
                    month_index += 1

                continue

            if index % 4 == 2:
                month_name = cls.map_month_name(month_index)
                try:
                    with transaction.atomic():
                        target_value = Decimal(value)
                        monthly_kpi = cls.objects.filter(year=current_year, month = month_index, user_id = user_id, group_id = group_id)
                        if monthly_kpi.exists():
                            monthly_kpi = monthly_kpi.first()
                        else:
                            monthly_kpi = cls()
                        monthly_kpi.year = current_year
                        monthly_kpi.month = month_index
                        monthly_kpi.month_name = month_name
                        monthly_kpi.user_id = user_id
                        monthly_kpi.group_id= group_id
                        monthly_kpi.name = "Monthly KPI for "+month_name
                        monthly_kpi.target = target_value
                        monthly_kpi.start_time = start_ts
                        monthly_kpi.end_time = end_ts
                        monthly_kpi.organization = organization
                        monthly_kpi.save()

                        tpg = TargetProductGroup.objects.get(pk=group_id)
                        product_ids = tpg.products.values_list('pk', flat=True)
                        for pid in product_ids:
                            upsmk = UnitProductSalesMonthlyKPI.objects.filter(year=current_year, month = month_index, user_id = user_id, product_id = pid, created_from_group=True)
                            if upsmk.exists():
                                upsmk_object = upsmk.first()
                            else:
                                upsmk_object = UnitProductSalesMonthlyKPI()

                            upsmk_object.year = current_year
                            upsmk_object.month = month_index
                            upsmk_object.month_name = month_name
                            upsmk_object.user_id = user_id
                            upsmk_object.product_id= pid
                            upsmk_object.name = "Monthly KPI for "+month_name
                            upsmk_object.target = target_value
                            upsmk_object.start_time = start_ts
                            upsmk_object.end_time = end_ts
                            upsmk_object.organization = organization
                            upsmk_object.created_from_group = True
                            upsmk_object.save()

                except Exception as exp:
                    ErrorLog.log(exp, organization=organization)
            if index % 4 == 3:
                month_index += 1


    @classmethod
    def post_processing_import_completed(cls,items=[], user=None, organization=None, **kwargs):
        # super().post_processing_import_completed(items,user,organization,**kwargs)
        tday = Clock.now()
        current_year = tday.year
        previous_year = current_year - 1

        current_month = tday.month

        first_day_this_month = datetime.strptime("1/"+str(tday.month)+"/"+str(tday.year),"%d/%m/%Y")
        last_month_data = first_day_this_month - timedelta(days=1)

        previous_month = last_month_data.month

        for item in items:
            user_id = int(item['0'])
            user_name = item['1']
            group_id = int(item['2'])
            group_name = item['3']
            data_item = item
            data_item.pop('0')
            data_item.pop('1')
            data_item.pop('2')
            data_item.pop('3')
            cls.process_user_data(user_id,user_name, group_id, group_name, current_month, previous_month, current_year,
                                  previous_year, data_item,user,organization,**kwargs)

    @classmethod
    def calculate_monthly_kpi(cls, pk_val, month, year, product_id, user_id):
        week_day, last_day = calendar.monthrange(year, month)
        now_timestamp = Clock.timestamp()

        start_time_string = "%s/%s/%s 00:00:00" % (1, str(month), str(year))
        start_time = Clock.get_timestamp_from_date(start_time_string) #point_datetime.timestamp()*1000 #Clock.get_timestamp_from_date("%s/%s/%s %s:%s:%s", (str(first_datetime.day), str(month_number), str(year_number), str(first_datetime.hour), str(first_datetime.minute), str(first_datetime.second)))
        target_end_datestring = "%s/%s/%s 23:59:59" % (str(last_day), str(month), str(year))
        target_end_time = Clock.get_timestamp_from_date(target_end_datestring)
        if target_end_time > now_timestamp:
            target_end_time = now_timestamp

        total_sales = QueryHelper.get_total_unit_product_sales_for_user(user_id, product_id, start_time, target_end_time)
        if total_sales is not None:
            monthly_kpi = UnitProductSalesMonthlyKPI.objects.get(pk=pk_val)
            monthly_kpi.achieved = total_sales
            monthly_kpi.last_calculated = Clock.timestamp()
            monthly_kpi.save()

    @classmethod
    def start_timer(cls):
        import time
        cls.start_time = time.time()

    @classmethod
    def end_timer(cls):
        if hasattr(cls, 'start_timer'):
            if cls.start_time:
                import time
                cls.end_time = time.time()

                cls.time_taken = cls.end_time - cls.start_time

                cls.start_time = False

            else:
                cls.time_taken = False
                print("Not started the timer")

        else:
            cls.time_taken = False
            print("Not started the timer")


    @classmethod
    def print_time_taken(cls):
        if cls.time_taken:
            print("Time taken: % seconds" % cls.time_taken)
        else:
            print("No timer started")

    @classmethod
    def create_missing_unit_product_target(cls, year, month_index, month_name, start_ts, end_ts, user_id, product_id, target_value=0, organization=None):
        upsmk = UnitProductSalesMonthlyKPI.objects.filter(year=year, month = month_index, user_id = user_id, product_id = product_id, created_from_group=True)
        if not upsmk.exists():
            upsmk_object = UnitProductSalesMonthlyKPI()
            upsmk_object.year = year
            upsmk_object.month = month_index
            upsmk_object.month_name = month_name
            upsmk_object.user_id = user_id
            upsmk_object.product_id= product_id
            upsmk_object.name = "Monthly KPI for "+month_name
            if target_value:
                upsmk_object.target = target_value
            upsmk_object.start_time = start_ts
            upsmk_object.end_time = end_ts
            upsmk_object.organization = organization if organization else Organization.objects.filter(is_master=True).first()
            upsmk_object.created_from_group = True
            upsmk_object.save()
            return True

    @classmethod
    def create_missing_product_group_targets(cls, year, month_index, month_name, user_id, target_value=0, organization=None, previous_object_count = 0, limit=100):
        tpg_objects = TargetProductGroup.objects.all()
        weekday, end_day = calendar.monthrange(year, month_index)
        start_ts = Clock.get_timestamp_from_date(str(1)+"/"+str(month_index)+"/"+str(year)+" 00:00:00")
        end_ts = Clock.get_timestamp_from_date(str(end_day)+"/"+str(month_index)+"/"+str(year)+" 00:00:00")

        object_count = previous_object_count

        for tpg in tpg_objects:
            print("Entering product group %s" % str(tpg))
            monthly_kpi = cls.objects.filter(year=year, month = month_index, user_id = user_id, group_id = tpg.pk)
            if not monthly_kpi.exists():
                monthly_kpi = cls()
                monthly_kpi.year = year
                monthly_kpi.month = month_index
                monthly_kpi.month_name = month_name
                monthly_kpi.user_id = user_id
                monthly_kpi.group_id= tpg.pk
                monthly_kpi.name = "Monthly KPI for "+month_name
                monthly_kpi.target = target_value
                monthly_kpi.start_time = start_ts
                monthly_kpi.end_time = end_ts
                monthly_kpi.organization = organization
                monthly_kpi.save()

                object_count += 1

            for product in tpg.products.all():
                print("Entering product %s" % product)
                created = cls.create_missing_unit_product_target(year, month_index, month_name, start_ts, end_ts, user_id, product.pk, target_value=target_value, organization=organization)
                if created:
                    object_count += 1
        return object_count

    @classmethod
    def create_missing_product_kpis(cls, user_id, year_range=[]):
        tday = Clock.now()
        current_year = tday.year
        organization = Organization.objects.filter(is_master=True).first()
        object_count = 0
        for year in year_range:
            print("Entering year %s" % str(year))
            if year == current_year:
                months = [ ( i + 1 ) for i in range(tday.month) ]
            elif year < current_year:
                months = [ ( i + 1 ) for i in range(12) ]
            else:
                months = []

            for month_index in months:
                month_list = cls.get_months()
                print("Entering month %s" % month_index)
                month_name = month_list[month_index - 1]
                object_count = cls.create_missing_product_group_targets(year, month_index, month_name, user_id, target_value=0,
                                                                        organization=organization, previous_object_count=object_count)

        return object_count

    @classmethod
    def create_missing_product_targets(cls, *args, **kwargs):
        kpi_enabled_models = get_models_with_decorator('enable_kpi', INSTALLED_APPS, include_class=True)

        tday = Clock.now()
        current_year = tday.year
        previous_year = current_year - 1

        organization = Organization.objects.filter(is_master=True).first()

        proceed = True

        cls.start_timer()

        model_names = [ _m.__name__ for _m in kpi_enabled_models ]

        role_ids = Role.objects.filter(name__in=model_names).values_list('pk', flat=True)

        all_users = ConsoleUser.objects.filter(role_id__in=role_ids)

        cls.end_timer()

        cls.print_time_taken()

        for each_user in all_users:
            print("Entering User %s" % str(each_user))
            object_count = cls.create_missing_product_kpis(each_user.pk, year_range=[ previous_year, current_year ])

    @classmethod
    def initialize_unit_product_targets(cls, *args, **kwargs):

        cls.start_timer()

        kpi_enabled_models = get_models_with_decorator('enable_kpi', INSTALLED_APPS, include_class=True)

        tday = Clock.now()
        current_year = tday.year
        previous_year = current_year - 1

        organization = Organization.objects.filter(is_master=True).first()

        model_names = [ _m.__name__ for _m in kpi_enabled_models ]

        role_ids = Role.objects.filter(name__in=model_names).values_list('pk', flat=True)

        all_users = ConsoleUser.objects.filter(role_id__in=role_ids)

        all_user_ids = all_users.values_list('pk', flat=True)

        tpg_objects = TargetProductGroup.objects.all().prefetch_related('products')

        product_ids = []

        for tpg_object in tpg_objects:
            product_ids += tpg_object.products.values_list('pk', flat=True)

        product_ids = list(set(product_ids))

        product_count = len(product_ids)

        month_index_list = [ ( i + 1, previous_year ) for i in range(12) ]
        month_index_list += [ ( i + 1, current_year ) for i in range(12) ]

        total_record_count_for_two_year = product_count * 24

        total_record_count_for_one_year = product_count * 12

        total_record_count_for_one_month = product_count * 1

        month_completed = {}

        year_completed = {}

        user_completed = {}

        user_product_kpis = {}

        completed_product_kpis = UnitProductSalesMonthlyKPI.objects.filter(user_id__in=all_user_ids, product_id__isnull=False)

        month_names = cls.get_months()

        month_ts = {}

        for month_index, year in month_index_list:
            weekday, end_day = calendar.monthrange(year, month_index)
            start_ts = Clock.get_timestamp_from_date(str(1)+"/"+str(month_index)+"/"+str(year)+" 00:00:00")
            end_ts = Clock.get_timestamp_from_date(str(end_day)+"/"+str(month_index)+"/"+str(year)+" 00:00:00")

            if not month_ts.get(year):
                month_ts[year] = {}

            if not month_ts[year].get(month_index):
                month_ts[year][month_index] = {
                    'start': start_ts,
                    'end': end_ts
                }

        for pkpi in completed_product_kpis:
            if not pkpi.user_id in user_product_kpis:
                user_product_kpis[pkpi.user_id] = {
                  pkpi.year: {
                      pkpi.month: [ pkpi.product_id ]
                  }
                }
            if not pkpi.year in user_product_kpis[pkpi.user_id]:
                user_product_kpis[pkpi.user_id][pkpi.year] = {
                    pkpi.month: [ pkpi.product_id ]
                }

            if not pkpi.month in user_product_kpis[pkpi.user_id][pkpi.year]:
                user_product_kpis[pkpi.user_id][pkpi.year][pkpi.month] = [ pkpi.product_id ]
            else:
                user_product_kpis[pkpi.user_id][pkpi.year][pkpi.month] += [ pkpi.product_id ]

        user_unit_product_targets_todo = {}

        for user_id in all_user_ids:
            for month_index, year in month_index_list:
                if user_product_kpis.get(user_id):
                    if user_product_kpis[user_id].get(year):
                        if user_product_kpis[user_id][year].get(month_index):
                            if not user_unit_product_targets_todo.get(user_id):
                                user_unit_product_targets_todo[user_id] = {
                                    year: {
                                        month_index: list(set(product_ids) - set(user_product_kpis[user_id][year][month_index]))
                                    }
                                }
                            else:
                                if not user_unit_product_targets_todo[user_id].get(year):
                                    user_unit_product_targets_todo[user_id][year] = {
                                            month_index: list(set(product_ids) - set(user_product_kpis[user_id][year][month_index]))
                                        }
                                else:
                                    user_unit_product_targets_todo[user_id][year][month_index] = list(set(product_ids) - set(user_product_kpis[user_id][year][month_index]))
                        else:
                            if not user_unit_product_targets_todo.get(user_id):
                                user_unit_product_targets_todo[user_id] = {
                                    year: {
                                        month_index: list(set(product_ids))
                                    }
                                }
                            else:
                                if not user_unit_product_targets_todo[user_id].get(year):
                                    user_unit_product_targets_todo[user_id][year] = {
                                            month_index: list(set(product_ids))
                                        }
                                else:
                                    user_unit_product_targets_todo[user_id][year][month_index] = list(set(product_ids))
                    else:
                        if not user_unit_product_targets_todo.get(user_id):
                            user_unit_product_targets_todo[user_id] = {
                                year: {
                                    month_index: list(set(product_ids))
                                }
                            }
                        else:
                            if not user_unit_product_targets_todo[user_id].get(year):
                                user_unit_product_targets_todo[user_id][year] = {
                                        month_index: list(set(product_ids))
                                    }
                            else:
                                user_unit_product_targets_todo[user_id][year][month_index] = list(set(product_ids))

                else:
                    if not user_unit_product_targets_todo.get(user_id):
                        user_unit_product_targets_todo[user_id] = {
                            year: {
                                month_index: list(set(product_ids))
                            }
                        }
                    else:
                        if not user_unit_product_targets_todo[user_id].get(year):
                            user_unit_product_targets_todo[user_id][year] = {
                                    month_index: list(set(product_ids))
                                }
                        else:
                            user_unit_product_targets_todo[user_id][year][month_index] = list(set(product_ids))


        for each_user in all_users:
            # print("Entering User %s" % str(each_user))

            if user_completed.get(each_user.pk):
                continue

            total_record_count_actual = UnitProductSalesMonthlyKPI.objects.filter(user_id=each_user.pk, product_id__isnull=False).count()

            print(total_record_count_actual)

            if total_record_count_actual == total_record_count_for_two_year:
                user_completed[each_user.pk] = True
                continue

            year_completed = {}
            month_completed = {}

            for month_index, year in month_index_list:
                if year_completed.get(year):
                    continue

                total_record_count_one_year_actual = UnitProductSalesMonthlyKPI.objects.filter(year=year, user_id=each_user.pk, product_id__isnull=False).count()

                if total_record_count_one_year_actual == total_record_count_for_one_year:
                    year_completed[year] = True
                    continue

                if month_completed.get(year) and month_completed[year].get(month_index):
                    continue

                total_record_count_one_month_actual = UnitProductSalesMonthlyKPI.objects.filter(year=year, month=month_index, user_id=each_user.pk, product_id__isnull=False).count()

                if total_record_count_for_one_month == total_record_count_one_month_actual:
                    if not month_completed.get(year):
                        month_completed[year] = { month_index: True }
                    else:
                        month_completed[year][month_index] = True

                    continue

                month_name = month_names[month_index - 1]

                if user_unit_product_targets_todo.get(each_user.pk):
                    if user_unit_product_targets_todo[each_user.pk].get(year):
                        if user_unit_product_targets_todo[each_user.pk][year].get(month_index):
                            product_ids = user_unit_product_targets_todo[each_user.pk][year][month_index]

                            for product_id in product_ids:
                                monthly_kpi = UnitProductSalesMonthlyKPI()
                                monthly_kpi.year = year
                                monthly_kpi.month = month_index
                                monthly_kpi.month_name = month_name
                                monthly_kpi.user_id = each_user.pk
                                monthly_kpi.product_id= product_id
                                monthly_kpi.name = "Monthly KPI for "+month_name
                                monthly_kpi.start_time = month_ts[year][month_index]['start']
                                monthly_kpi.end_time = month_ts[year][month_index]['end']
                                monthly_kpi.organization = organization
                                monthly_kpi.save()

        cls.end_timer()

        cls.print_time_taken()


    @classmethod
    def initialize_product_group_targets(cls, *args, **kwargs):

        cls.start_timer()

        kpi_enabled_models = get_models_with_decorator('enable_kpi', INSTALLED_APPS, include_class=True)

        tday = Clock.now()
        current_year = tday.year
        previous_year = current_year - 1

        organization = Organization.objects.filter(is_master=True).first()

        model_names = [ _m.__name__ for _m in kpi_enabled_models ]

        role_ids = Role.objects.filter(name__in=model_names).values_list('pk', flat=True)

        all_users = ConsoleUser.objects.filter(role_id__in=role_ids)

        all_user_ids = all_users.values_list('pk', flat=True)

        tpg_objects = TargetProductGroup.objects.all()

        tpg_ids = tpg_objects.values_list('pk', flat=True)

        tpg_count = tpg_objects.count()

        month_index_list = [ ( i + 1, previous_year ) for i in range(12) ]
        month_index_list += [ ( i + 1, current_year ) for i in range(12) ]

        total_record_count_for_two_year = tpg_count * 24

        total_record_count_for_one_year = tpg_count * 12

        total_record_count_for_one_month = tpg_count * 1

        month_completed = {}

        year_completed = {}

        user_completed = {}

        user_group_kpis = {}

        completed_group_kpis = cls.objects.filter(user_id__in=all_user_ids, group_id__isnull=False)

        month_names = cls.get_months()

        month_ts = {}

        for month_index, year in month_index_list:
            weekday, end_day = calendar.monthrange(year, month_index)
            start_ts = Clock.get_timestamp_from_date(str(1)+"/"+str(month_index)+"/"+str(year)+" 00:00:00")
            end_ts = Clock.get_timestamp_from_date(str(end_day)+"/"+str(month_index)+"/"+str(year)+" 00:00:00")

            if not month_ts.get(year):
                month_ts[year] = {}

            if not month_ts[year].get(month_index):
                month_ts[year][month_index] = {
                    'start': start_ts,
                    'end': end_ts
                }

        for gk in completed_group_kpis:
            if not gk.user_id in user_group_kpis:
                user_group_kpis[gk.user_id] = {
                  gk.year: {
                      gk.month: [ gk.group_id ]
                  }
                }
            if not gk.year in user_group_kpis[gk.user_id]:
                user_group_kpis[gk.user_id][gk.year] = {
                    gk.month: [ gk.group_id ]
                }

            if not gk.month in user_group_kpis[gk.user_id][gk.year]:
                user_group_kpis[gk.user_id][gk.year][gk.month] = [ gk.group_id ]
            else:
                user_group_kpis[gk.user_id][gk.year][gk.month] += [ gk.group_id ]

        user_group_targets_todo = {}

        for user_id in all_user_ids:
            for month_index, year in month_index_list:
                if user_group_kpis.get(user_id):
                    if user_group_kpis[user_id].get(year):
                        if user_group_kpis[user_id][year].get(month_index):
                            if not user_group_targets_todo.get(user_id):
                                user_group_targets_todo[user_id] = {
                                    year: {
                                        month_index: list(set(tpg_ids) - set(user_group_kpis[user_id][year][month_index]))
                                    }
                                }
                            else:
                                if not user_group_targets_todo[user_id].get(year):
                                    user_group_targets_todo[user_id][year] = {
                                            month_index: list(set(tpg_ids) - set(user_group_kpis[user_id][year][month_index]))
                                        }
                                else:
                                    user_group_targets_todo[user_id][year][month_index] = list(set(tpg_ids) - set(user_group_kpis[user_id][year][month_index]))
                        else:
                            if not user_group_targets_todo.get(user_id):
                                user_group_targets_todo[user_id] = {
                                    year: {
                                        month_index: list(set(tpg_ids))
                                    }
                                }
                            else:
                                if not user_group_targets_todo[user_id].get(year):
                                    user_group_targets_todo[user_id][year] = {
                                            month_index: list(set(tpg_ids))
                                        }
                                else:
                                    user_group_targets_todo[user_id][year][month_index] = list(set(tpg_ids))
                    else:
                        if not user_group_targets_todo.get(user_id):
                            user_group_targets_todo[user_id] = {
                                year: {
                                    month_index: list(set(tpg_ids))
                                }
                            }
                        else:
                            if not user_group_targets_todo[user_id].get(year):
                                user_group_targets_todo[user_id][year] = {
                                        month_index: list(set(tpg_ids))
                                    }
                            else:
                                user_group_targets_todo[user_id][year][month_index] = list(set(tpg_ids))

                else:
                    if not user_group_targets_todo.get(user_id):
                        user_group_targets_todo[user_id] = {
                            year: {
                                month_index: list(set(tpg_ids))
                            }
                        }
                    else:
                        if not user_group_targets_todo[user_id].get(year):
                            user_group_targets_todo[user_id][year] = {
                                    month_index: list(set(tpg_ids))
                                }
                        else:
                            user_group_targets_todo[user_id][year][month_index] = list(set(tpg_ids))


        for each_user in all_users:
            # print("Entering User %s" % str(each_user))

            if user_completed.get(each_user.pk):
                continue

            total_record_count_actual = cls.objects.filter(user_id=each_user.pk, group_id__isnull=False).count()

            print(total_record_count_actual)

            if total_record_count_actual == total_record_count_for_two_year:
                user_completed[each_user.pk] = True
                continue

            year_completed = {}
            month_completed = {}

            for month_index, year in month_index_list:
                if year_completed.get(year):
                    continue

                total_record_count_one_year_actual = cls.objects.filter(year=year, user_id=each_user.pk, group_id__isnull=False).count()

                if total_record_count_one_year_actual == total_record_count_for_one_year:
                    year_completed[year] = True
                    continue

                if month_completed.get(year) and month_completed[year].get(month_index):
                    continue

                total_record_count_one_month_actual = cls.objects.filter(year=year, month=month_index, user_id=each_user.pk, group_id__isnull=False).count()

                if total_record_count_for_one_month == total_record_count_one_month_actual:
                    if not month_completed.get(year):
                        month_completed[year] = { month_index: True }
                    else:
                        month_completed[year][month_index] = True

                    continue

                month_name = month_names[month_index - 1]

                if user_group_targets_todo.get(each_user.pk):
                    if user_group_targets_todo[each_user.pk].get(year):
                        if user_group_targets_todo[each_user.pk][year].get(month_index):
                            group_ids = user_group_targets_todo[each_user.pk][year][month_index]

                            for group_id in group_ids:
                                monthly_kpi = cls()
                                monthly_kpi.year = year
                                monthly_kpi.month = month_index
                                monthly_kpi.month_name = month_name
                                monthly_kpi.user_id = each_user.pk
                                monthly_kpi.group_id= group_id
                                monthly_kpi.name = "Monthly KPI for "+month_name
                                monthly_kpi.start_time = month_ts[year][month_index]['start']
                                monthly_kpi.end_time = month_ts[year][month_index]['end']
                                monthly_kpi.organization = organization
                                monthly_kpi.save()


        cls.end_timer()

        cls.print_time_taken()

    @classmethod
    def calculate_unit_product_kpi(cls, *args, **kwargs):
        kpi_enabled_models = get_models_with_decorator('enable_kpi', INSTALLED_APPS, include_class=True)

        tday = Clock.now()
        current_year = tday.year
        previous_year = current_year - 1

        for _model_class in kpi_enabled_models:
            all_users = _model_class.objects.all()
            for each_user in all_users:

                user_entries = UnitProductSalesMonthlyKPI.objects.filter((Q(last_calculated__isnull=True) |
                                                                          (Q(last_calculated__isnull=False) & Q(last_calculated__lte=Clock.timestamp())))
                                                                         & Q(user_id=each_user.pk) & Q(created_from_group=True))
                for entry in user_entries:
                    cls.calculate_monthly_kpi(entry.pk,entry.month, entry.year, entry.product_id, entry.user_id)

    @classmethod
    def calculate_kpi(cls, *args, **kwargs):
        cls.calculate_unit_product_kpi()

        kpi_enabled_models = get_models_with_decorator('enable_kpi', INSTALLED_APPS, include_class=True)
        for _model_class in kpi_enabled_models:
            all_users = _model_class.objects.all()
            for each_user in all_users:
                user_entries = cls.objects.filter((Q(last_calculated__isnull=True) | (Q(last_calculated__isnull=False)
                                                                                      & Q(last_calculated__lte=Clock.timestamp()))) & Q(user_id=each_user.pk))
                for entry in user_entries:
                    QueryHelper.update_group_kpi_from_unit_product_kpis(entry.group_id, entry.year, entry.month, entry.user_id)
