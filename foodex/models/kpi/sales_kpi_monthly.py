import calendar
from django.db.models.aggregates import Sum
from datetime import datetime
from datetime import timedelta
from django.db.models.query_utils import Q
from blackwidow.core.managers.modelmanager import DomainEntityModelManager
from blackwidow.core.models.contracts.kpi_base import MonthlyKPI
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.decorators.enable_export import enable_export
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, get_models_with_decorator
from blackwidow.engine.extensions.clock import Clock
from blackwidow.sales.models.transactions.transaction import SecondaryTransaction, TransactionType, PrimaryTransaction
from config.apps import INSTALLED_APPS
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum
from foodex.helpers.query_helper import QueryHelper

__author__ = 'Sohel'

@decorate(enable_import, enable_export, route(route='sales_kpi_monthly', module=ModuleEnum.Targets,
                                              display_name='Monthly Sales Value', group='Targets'))
class SalesKPIMonthly(MonthlyKPI):

    objects = DomainEntityModelManager(filter={ "type": "SalesKPIMonthly" })

    @classmethod
    def get_button_title(cls, button=ViewActionEnum.Details):
        if button == ViewActionEnum.AdvancedExport:
            return "Download this table"
        elif button == ViewActionEnum.AdvancedImport:
            return "Upload changes to this table"

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.AdvancedImport, ViewActionEnum.AdvancedExport]

    @classmethod
    def get_queryset(cls, queryset=None, profile_filter=False, **kwargs):
        # dt = Clock.utcnow()
        # if queryset:
        #     return queryset.filter(month=dt.month,year=dt.year)
        # else:
        #     queryset = cls.objects.filter(month=dt.month,year=dt.year)
        return queryset

    @classmethod
    def get_left_cols_fixed(cls):
        return ( "Name", )

    @classmethod
    def table_columns(cls):
        return "code", "user", "month_name", "year", "target", "achieved"

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', "user", 'month_name', 'year', 'target', 'achieved']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        return d

    def tabs_config(self):
        tabs = [TabView(
            title='Sales KPI for %s in %s' % (self.user.name, self.year),
            access_key='sales_kpi_monthly',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='foodex.SalesKPIMonthly',
            queryset=self.get_queryset(),
            queryset_filter=Q(**{'client__id': self.pk, 'transaction_type': TransactionType.StockAdjustment.value, 'infrastructure_unit': not None})
        )]
        return tabs

    @classmethod
    def get_page_title(cls):
        return "Monthly Sales KPI"

    @classmethod
    def fetch_user_kpis(cls, user_ids, previous_year, current_year):
        all_kpi_objects = cls.objects.filter(user_id__in=user_ids, year__in=[ previous_year, current_year ], month__in=[ (i + 1) for i in range(12) ])
        user_kpis = {}
        for kpi in all_kpi_objects:
            if not kpi.user_id in user_kpis:
                user_kpis[kpi.user_id] = {
                    kpi.year: {
                        kpi.month: {
                            "target": kpi.target,
                            "actual": kpi.achieved
                        }
                    }
                }
            else:
                if not kpi.year in user_kpis[kpi.user_id]:
                    user_kpis[kpi.user_id][kpi.year] = {
                        kpi.month: {
                            "target": kpi.target,
                            "actual": kpi.achieved
                        }
                    }
                else:
                    if not kpi.month in user_kpis[kpi.user_id][kpi.year]:
                        user_kpis[kpi.user_id][kpi.year][kpi.month] = {
                            "target": kpi.target,
                            "actual": kpi.achieved
                        }
        return user_kpis

    @classmethod
    def get_kpi_data_rows(cls, **kwargs):
        rows = []
        tday = Clock.now()
        current_year = tday.year
        previous_year = current_year - 1
        current_month = tday.month

        user_kpis = {}

        kpi_enabled_models = get_models_with_decorator('enable_kpi', INSTALLED_APPS, include_class=True)
        for _model_class in kpi_enabled_models:
            all_users = _model_class.objects.all()

            user_ids = all_users.values_list('pk', flat=True)

            user_kpis = cls.fetch_user_kpis(user_ids, previous_year, current_year)

            def find_kpi_for_user(user_id, year, month, target_or_actual):
                try:
                    kpi_value = user_kpis[user_id][year][month][target_or_actual]
                    return kpi_value
                except:
                    pass

            for each_user in all_users:
                row = []
                month_index = 1
                for index in range(49):
                    cell_value = ''
                    if index == 0:
                        cell_value = each_user
                    else:
                        mindex = index - 1
                        if mindex % 4 == 0: ###Previous Year Target
                            cell_value =  find_kpi_for_user(each_user.pk, previous_year, month_index, "target") # cls.get_target(each_user.pk, previous_year, month_index)
                        elif mindex % 4 == 1: ###Previous Year Actual.
                            cell_value =  find_kpi_for_user(each_user.pk, previous_year, month_index, "actual") #cls.get_actual(each_user.pk, previous_year, month_index)
                        elif mindex % 4 == 2: ###Current Year Target
                            if month_index <= current_month:
                                cell_value =  find_kpi_for_user(each_user.pk, current_year, month_index, "target") #cls.get_target(each_user.pk, current_year, month_index)
                            else:

                                cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "target") #cls.get_target(each_user.pk, current_year, month_index)
                        elif mindex % 4 == 3: ###Current Year Actual
                            if month_index <= current_month:
                                cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "actual") #cls.get_actual(each_user.pk, current_year, month_index)
                            else:
                                cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "actual") #cls.get_actual(each_user.pk, current_year, month_index)

                            ###Index Month Index After Each Index.
                            month_index += 1
                    if not cell_value:
                        cell_value = "-"
                    row += [ cell_value ]
                rows += [ row ]
        return rows

    @classmethod
    def calculate_new_kpi(cls, kpi_id, first_datetime,user_id):
        now_datetime = Clock.utcnow()
        point_datetime = first_datetime
        now_timestamp = Clock.timestamp()
        while point_datetime < now_datetime:
            month_number = point_datetime.month
            year_number = point_datetime.year
            week_day, last_day = calendar.monthrange(year_number, month_number)

            start_time = point_datetime.timestamp()*1000 #Clock.get_utc_timestamp_from_date("%s/%s/%s %s:%s:%s", (str(first_datetime.day), str(month_number), str(year_number), str(first_datetime.hour), str(first_datetime.minute), str(first_datetime.second)))
            target_end_datestring = "%s/%s/%s 23:59:59" % (str(last_day), str(month_number), str(year_number))
            target_end_time = Clock.get_utc_timestamp_from_date(target_end_datestring)
            if target_end_time > now_timestamp:
                target_end_time = now_timestamp

            secondary_sales_objects = SecondaryTransaction.objects.filter(created_by_id=user_id)
            secondary_sales_objects = secondary_sales_objects.filter(Q(transaction_time__gte=start_time),Q(transaction_time__lte=target_end_time))
            total_sales = secondary_sales_objects.aggregate(total_sales = Sum('total'))['total_sales']

            if total_sales is not None:
                monthly_kpi = cls.objects.get(pk=kpi_id)
                monthly_kpi.achieved = total_sales
                monthly_kpi.save()
            point_datetime = datetime.fromtimestamp(target_end_time/1000) + timedelta(seconds=1)

    @classmethod
    def calculate_monthly_kpi(cls, kpi_id, date_and_time,user_id):
        point_datetime = date_and_time
        now_timestamp = Clock.timestamp()
        month_number = point_datetime.month
        year_number = point_datetime.year
        week_day, last_day = calendar.monthrange(year_number, month_number)

        start_time_string = "%s/%s/%s 00:00:00" % (1, str(month_number), str(year_number))
        start_time = Clock.get_utc_timestamp_from_date(start_time_string) #point_datetime.timestamp()*1000 #Clock.get_utc_timestamp_from_date("%s/%s/%s %s:%s:%s", (str(first_datetime.day), str(month_number), str(year_number), str(first_datetime.hour), str(first_datetime.minute), str(first_datetime.second)))
        target_end_datestring = "%s/%s/%s 23:59:59" % (str(last_day), str(month_number), str(year_number))
        target_end_time = Clock.get_utc_timestamp_from_date(target_end_datestring)

        if target_end_time > now_timestamp:
            target_end_time = now_timestamp

        total_sales = QueryHelper.get_total_sales_value_for_user(user_id,start_time,target_end_time)
        if total_sales is not None:
            monthly_kpi = cls.objects.get(pk=kpi_id)
            monthly_kpi.achieved = total_sales
            monthly_kpi.last_calculated = Clock.timestamp()
            monthly_kpi.save()

    @classmethod
    def calculate_kpi(cls, *args, **kwargs):
        entries = cls.objects.filter(Q(last_calculated__isnull=True) | (Q(last_calculated__isnull=False) & Q(last_calculated__lte=Clock.timestamp()))).values_list('pk','user_id','year','month','last_calculated')
        for pk, user_id, year, month, last_calculated in entries:
            cls.calculate_monthly_kpi(pk, datetime(year=year, month=month, day=1), user_id)


