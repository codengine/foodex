from datetime import datetime, timedelta
import calendar
from decimal import Decimal
from django.db import models
from django.db.models.query_utils import Q
from openpyxl.styles import Style
from openpyxl.styles.alignment import Alignment
from openpyxl.styles.protection import Protection
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig
from blackwidow.core.models.config.exporter_config import ExporterConfig
from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
from blackwidow.core.models.config.importer_config import ImporterConfig
from blackwidow.core.models.contracts.kpi_base import MonthlyKPI
from blackwidow.core.models.log.logs import ErrorLog
from blackwidow.engine.decorators.enable_export import enable_export
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, get_models_with_decorator
from blackwidow.engine.extensions.clock import Clock
from blackwidow.sales.models.products.product import Product
from config.apps import INSTALLED_APPS
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum
from foodex.helpers.query_helper import QueryHelper
from foodex.models.users.service_persons import ServicePerson

__author__ = 'Sohel'

@decorate(enable_import, enable_export, route(route='unit_product_sales_monthly_kpi', module=ModuleEnum.Targets, display_name='Monthly Unit Red Bull', group='Targets', hide=True))
class UnitProductSalesMonthlyKPI(MonthlyKPI):
    product = models.ForeignKey(Product)
    created_from_group = models.BooleanField(default=False)

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.AdvancedImport, ViewActionEnum.AdvancedExport]

    @classmethod
    def get_button_title(cls, button=ViewActionEnum.Details):
        if button == ViewActionEnum.AdvancedExport:
            return "Download this table"
        elif button == ViewActionEnum.AdvancedImport:
            return "Upload changes to this table"

    @classmethod
    def get_unit_kpi_enabled_products(cls, **kwargs):
        product_names = ['Red Bull']
        products = Product.objects.filter(short_name__in=product_names)
        request = kwargs.get("request")
        if request:
            if request.GET.get("filter") == "true":
                product_id = request.GET.get("product:id")
                try:
                    product_id = int(product_id)
                except:
                    product_id = None
                if product_id:
                    products = products.filter(pk=product_id)
        return products

    @classmethod
    def get_product(cls, user_id, year, month):
        kpi = cls.objects.filter(user_id=user_id, year=year, month=month)
        if kpi.exists():
            kpi = kpi.first()
            return kpi.product
        return None

    @classmethod
    def get_page_title(cls):
        return "Sales KPI Product"

    @classmethod
    def get_left_cols_fixed(cls):
        return ( "Name", )

    @classmethod
    def get_queryset(cls, queryset=None, profile_filter=False, **kwargs):
        dt = Clock.utcnow()
        if queryset:
            return queryset.filter(month=dt.month,year=dt.year)
        else:
            queryset = cls.objects.filter(month=dt.month,year=dt.year)
        return queryset

    @classmethod
    def table_columns(cls):
        return "code", "user", "month_name", "year", "product", "target", "achieved"

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', "user", 'month_name', 'year', 'product', 'target', 'achieved']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        return d

    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        ExporterConfig.objects.filter(model=cls.__name__, organization=organization).delete()
        exporter_config, created = ExporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)

        columns = [
            ExporterColumnConfig(column=0, column_name='ID', property_name='id', ignore=False),
            ExporterColumnConfig(column=1, column_name='Name', property_name='name', ignore=False),
            ExporterColumnConfig(column=2, column_name='Product Id', property_name='product_id', ignore=False),
            ExporterColumnConfig(column=3, column_name='Product Name', property_name='product_name', ignore=False),
        ]
        for i in range(0,48):
            columns += [ExporterColumnConfig(column=i + 4, column_name='value'+str(i), property_name='render_value_'+str(i), ignore=False)]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)

        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):
        # workbook.protection.sheet = True
        return self.pk, row_number + 1

    @classmethod
    def fetch_user_kpis(cls, user_ids, previous_year, current_year, product_ids):
        all_kpi_objects = cls.objects.filter(user_id__in=user_ids, year__in=[ previous_year, current_year ], month__in=[ (i + 1) for i in range(12) ], product_id__in=product_ids)
        user_kpis = {}
        for kpi in all_kpi_objects:
            if not kpi.user_id in user_kpis:
                user_kpis[kpi.user_id] = {
                    kpi.year: {
                        kpi.month: {
                            kpi.product_id: {
                                "target": kpi.target,
                                "actual": kpi.achieved
                            }
                        }
                    }
                }
            else:

                if not kpi.year in user_kpis[kpi.user_id]:
                    user_kpis[kpi.user_id][kpi.year] = {
                        kpi.month: {
                            kpi.product_id: {
                                "target": kpi.target,
                                "actual": kpi.achieved
                            }
                        }
                    }
                else:
                    if not kpi.month in user_kpis[kpi.user_id][kpi.year]:
                        user_kpis[kpi.user_id][kpi.year][kpi.month] = {
                            kpi.product_id: {
                                "target": kpi.target,
                                "actual": kpi.achieved
                            }
                        }
                    else:
                        if not kpi.product_id in user_kpis[kpi.user_id][kpi.year][kpi.month]:
                            user_kpis[kpi.user_id][kpi.year][kpi.month][kpi.product_id] = {
                                "target": kpi.target,
                                "actual": kpi.achieved
                            }

        return user_kpis

    @classmethod
    def get_kpi_data_rows(cls, **kwargs):
        rows = []
        tday = Clock.now()
        current_year = tday.year
        previous_year = current_year - 1

        current_month = tday.month
        product_objects = cls.get_unit_kpi_enabled_products(**kwargs)

        user_kpis = {}

        ###Now get all users with enable_kpi decorator.
        kpi_enabled_models = get_models_with_decorator('enable_kpi', INSTALLED_APPS, include_class=True)
        for _model_class in kpi_enabled_models:
            all_users = _model_class.objects.all()

            user_ids = all_users.values_list('pk', flat=True)

            product_ids = product_objects.values_list('pk', flat=True)

            user_kpis = cls.fetch_user_kpis(user_ids, previous_year, current_year, product_ids)

            def find_kpi_for_user(user_id, year, month, target_or_actual, product_id):
                try:
                    kpi_value = user_kpis[user_id][year][month][product_id][target_or_actual]
                    return kpi_value
                except:
                    pass

            for each_user in all_users:
                for p in product_objects:
                    row = []
                    month_index = 1
                    for index in range(49):
                        cell_value = ''
                        if index == 0:
                            cell_value = each_user
                        else:
                            mindex = index - 1
                            if mindex % 4 == 0: ###Previous Year Target
                                cell_value = find_kpi_for_user(each_user.pk, previous_year, month_index, "target", p.pk)
                            elif mindex % 4 == 1: ###Previous Year Actual.
                                cell_value = find_kpi_for_user(each_user.pk, previous_year, month_index, "actual", p.pk)
                            elif mindex % 4 == 2: ###Current Year Target
                                if month_index <= current_month:
                                    cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "target", p.pk)
                                else:

                                    cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "target", p.pk)
                            elif mindex % 4 == 3: ###Current Year Actual
                                if month_index <= current_month:
                                    cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "actual", p.pk)
                                else:
                                    cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "actual", p.pk)

                                ###Index Month Index After Each Index.
                                month_index += 1
                        if not cell_value:
                            cell_value = '-'
                        row += [ cell_value ]
                    rows += [ row ]
        return rows

    @classmethod
    def initialize_export(cls, workbook=None, columns=None,  query_set=None, **kwargs):
        # workbook.protection.sheet = True
        center_style = Style(alignment=Alignment(horizontal='center', vertical='center'))
        if len(columns) < 50:
            return workbook, 1

        row = 1
        workbook.cell(row=row, column=1).value = "ID"
        workbook.cell(row=row, column=2).value = "Name"
        workbook.cell(row=row, column=3).value = "product ID"
        workbook.cell(row=row, column=4).value = "Product Name"
        start_column = 4
        end_column = 5
        niteration = 0
        for i in range(len(columns) - 4):
            if i % 4 == 0:
                start_column = i + 5
            if i % 4 == 3:
                end_column = i + 5
                niteration += 1
                workbook.cell(row=row, column=start_column).value = cls.map_month_name(niteration)
                workbook.cell(row=row, column=start_column).style = center_style
                workbook.merge_cells(start_row=row, start_column=start_column, end_row=row, end_column=end_column)

        now_dt = Clock.now()
        current_year = now_dt.year
        previous_year = current_year - 1
        row = 2
        for i in range(len(columns) - 4):
            if i % 2 == 0:
                start_column = i + 5
            if i % 2 == 1:
                end_column = i + 5
                if i % 4 == 1:
                    workbook.cell(row=row, column=start_column).value = str(previous_year)
                elif i % 4 == 3:
                    workbook.cell(row=row, column=start_column).value = str(current_year)
                workbook.cell(row=row, column=start_column).style = center_style
                workbook.merge_cells(start_row=row, start_column=start_column, end_row=row, end_column=end_column)

        row = 3
        for i in range(len(columns) - 4):
            if i % 2 == 0:
                workbook.cell(row=row, column= i + 5 ).value = "Target"
            elif i % 2 == 1:
                workbook.cell(row=row, column= i + 5 ).value = "Actual"

        workbook.cell(row=1, column=1).style = center_style
        workbook.merge_cells(start_row=1, start_column=1, end_row=3, end_column=1)
        workbook.cell(row=1, column=2).style = center_style
        workbook.merge_cells(start_row=1, start_column=2, end_row=3, end_column=2)
        workbook.cell(row=1, column=3).style = center_style
        workbook.merge_cells(start_row=1, start_column=3, end_row=3, end_column=3)
        workbook.cell(row=1, column=4).style = center_style
        workbook.merge_cells(start_row=1, start_column=4, end_row=3, end_column=4)

        tday = Clock.now()
        current_year = tday.year
        previous_year = current_year - 1

        current_month = tday.month
        row_number = 4

        product_objects = cls.get_unit_kpi_enabled_products()
        ###Now get all users with enable_kpi decorator.
        kpi_enabled_models = get_models_with_decorator('enable_kpi', INSTALLED_APPS, include_class=True)
        for _model_class in kpi_enabled_models:
            all_users = _model_class.objects.all()

            user_ids = all_users.values_list('pk', flat=True)

            product_ids = product_objects.values_list('pk', flat=True)

            user_kpis = cls.fetch_user_kpis(user_ids, previous_year, current_year, product_ids)

            def find_kpi_for_user(user_id, year, month, target_or_actual, product_id):
                try:
                    kpi_value = user_kpis[user_id][year][month][product_id][target_or_actual]
                    return kpi_value
                except:
                    pass

            for each_user in all_users:
                for p in product_objects:
                    month_index = 1
                    protection = Protection(hidden=True, locked=True)
                    protected_style = Style(protection=protection)
                    for index, column in enumerate(columns):
                        ccolumn = column.column + 1
                        should_protected = False
                        if month_index < current_month:
                            should_protected = True
                        cell_value = ''
                        if index == 0:
                            cell_value = each_user.pk
                            workbook.cell(row=row_number, column=ccolumn).value = cell_value
                            workbook.cell(row=row_number, column=ccolumn).style = protected_style
                        elif index == 1:
                            cell_value = each_user.name
                            workbook.cell(row=row_number, column=ccolumn).value = cell_value
                            workbook.cell(row=row_number, column=ccolumn).style = protected_style
                        elif index == 2:
                            cell_value = p.pk
                            workbook.cell(row=row_number, column=ccolumn).value = cell_value
                            workbook.cell(row=row_number, column=ccolumn).style = protected_style
                        elif index == 3:
                            cell_value = p.name+': '+p.description if p.description else p.name
                            workbook.cell(row=row_number, column=ccolumn).value = cell_value
                            workbook.cell(row=row_number, column=ccolumn).style = protected_style
                        else:
                            mindex = index - 4
                            if mindex % 4 == 0: ###Previous Year Target
                                cell_value = find_kpi_for_user(each_user.pk, previous_year, month_index, "target", p.pk) #cls.get_target(each_user.pk, previous_year, month_index)
                                workbook.cell(row=row_number, column=ccolumn).value = cell_value
                                if should_protected:
                                    workbook.cell(row=row_number, column=ccolumn).style = protected_style
                            elif mindex % 4 == 1: ###Previous Year Actual.
                                cell_value = find_kpi_for_user(each_user.pk, previous_year, month_index, "actual", p.pk) #cls.get_actual(each_user.pk, previous_year, month_index)
                                workbook.cell(row=row_number, column=ccolumn).value = cell_value
                                if should_protected:
                                    workbook.cell(row=row_number, column=ccolumn).style = protected_style
                            elif mindex % 4 == 2: ###Current Year Target
                                if month_index <= current_month:
                                    cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "target", p.pk) #cls.get_target(each_user.pk, current_year, month_index)
                                    workbook.cell(row=row_number, column=ccolumn).value = cell_value
                                    if should_protected:
                                        workbook.cell(row=row_number, column=ccolumn).style = protected_style
                                else:

                                    cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "target", p.pk) #cls.get_target(each_user.pk, current_year, month_index)
                                    workbook.cell(row=row_number, column=ccolumn).value = cell_value
                            elif mindex % 4 == 3: ###Current Year Actual
                                if month_index <= current_month:
                                    cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "actual", p.pk) #cls.get_actual(each_user.pk, current_year, month_index)
                                    workbook.cell(row=row_number, column=ccolumn).value = cell_value
                                    if should_protected:
                                        workbook.cell(row=row_number, column=ccolumn).style = protected_style
                                else:
                                    cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "actual", p.pk) #cls.get_actual(each_user.pk, current_year, month_index)
                                    workbook.cell(row=row_number, column=ccolumn).value = cell_value

                                ###Index Month Index After Each Index.
                                month_index += 1
                    row_number += 1

        return workbook, row_number

    @classmethod
    def finalize_export(cls, workbook=None, row_count=None, **kwargs):
        file_name = '%s_%s' % (cls.__name__, Clock.timestamp())
        return workbook, file_name

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        ImporterConfig.objects.filter(model=cls.__name__, organization=organization).delete()
        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        importer_config.starting_row = 3
        importer_config.starting_column = 0
        importer_config.save(**kwargs)
        columns = [
            ImporterColumnConfig(column=0, column_name='ID', property_name='id', ignore=False),
            ImporterColumnConfig(column=1, column_name='Name', property_name='name', ignore=False),
            ImporterColumnConfig(column=2, column_name='Product Id', property_name='product_id', ignore=False),
            ImporterColumnConfig(column=3, column_name='Product Name', property_name='product_name', ignore=False),
        ]
        for i in range(0,48):
                columns += [ImporterColumnConfig(column=i + 4, column_name='value'+str(i), property_name='render_value', ignore=False)]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        return data

    @classmethod
    def process_user_data(cls,user_id, user_name, product_id, product_name, current_month, previous_month, current_year, previous_year, item, user=None, organization=None, **kwargs):
        editable_from = previous_month * 4
        sorted_items = sorted(item.items(), key=lambda x: int(x[0]))
        start_day = 1
        month_index = 1
        for index, (key, value) in enumerate(sorted_items):
            weekday, end_day = calendar.monthrange(current_year, month_index)
            start_ts = Clock.get_timestamp_from_date(str(start_day)+"/"+str(month_index)+"/"+str(current_year)+" 00:00:00")
            end_ts = Clock.get_timestamp_from_date(str(end_day)+"/"+str(month_index)+"/"+str(current_year)+" 00:00:00")
            key = int(key)
            if index < editable_from - 1:

                if index % 4 == 3:
                    month_index += 1

                continue

            if index % 4 == 2:
                month_name = cls.map_month_name(month_index)
                try:
                    target_value = Decimal(value)
                    monthly_kpi = cls.objects.filter(year=current_year, month = month_index, user_id = user_id, product_id = product_id)
                    if monthly_kpi.exists():
                        monthly_kpi = monthly_kpi.first()
                    else:
                        monthly_kpi = cls()
                    monthly_kpi.year = current_year
                    monthly_kpi.month = month_index
                    monthly_kpi.month_name = month_name
                    monthly_kpi.user_id = user_id
                    monthly_kpi.product_id= product_id
                    monthly_kpi.name = "Monthly KPI for "+month_name
                    monthly_kpi.target = target_value
                    monthly_kpi.start_time = start_ts
                    monthly_kpi.end_time = end_ts
                    monthly_kpi.organization = organization
                    monthly_kpi.save()
                except Exception as exp:
                    ErrorLog.log(exp, organization=organization)
            if index % 4 == 3:
                month_index += 1


    @classmethod
    def post_processing_import_completed(cls,items=[], user=None, organization=None, **kwargs):
        # super().post_processing_import_completed(items,user,organization,**kwargs)
        tday = Clock.now()
        current_year = tday.year
        previous_year = current_year - 1

        current_month = tday.month

        first_day_this_month = datetime.strptime("1/"+str(tday.month)+"/"+str(tday.year),"%d/%m/%Y")
        last_month_data = first_day_this_month - timedelta(days=1)

        previous_month = last_month_data.month

        unit_product_kpi_enabled = cls.get_unit_kpi_enabled_products()

        for item in items:
            user_id = int(item['0'])
            user_name = item['1']
            product_id = int(item['2'])
            product_name = item['3']
            data_item = item
            data_item.pop('0')
            data_item.pop('1')
            data_item.pop('2')
            data_item.pop('3')
            cls.process_user_data(user_id,user_name, product_id, product_name, current_month, previous_month, current_year, previous_year, data_item,user,organization,**kwargs)


    @classmethod
    def calculate_monthly_kpi(cls, pk_val, month, year, product_id, user_id):
        week_day, last_day = calendar.monthrange(year, month)
        now_timestamp = Clock.timestamp()

        start_time_string = "%s/%s/%s 00:00:00" % (1, str(month), str(year))
        start_time = Clock.get_timestamp_from_date(start_time_string) #point_datetime.timestamp()*1000 #Clock.get_timestamp_from_date("%s/%s/%s %s:%s:%s", (str(first_datetime.day), str(month_number), str(year_number), str(first_datetime.hour), str(first_datetime.minute), str(first_datetime.second)))
        target_end_datestring = "%s/%s/%s 23:59:59" % (str(last_day), str(month), str(year))
        target_end_time = Clock.get_timestamp_from_date(target_end_datestring)
        if target_end_time > now_timestamp:
            target_end_time = now_timestamp

        total_sales = QueryHelper.get_total_unit_product_sales_for_user(user_id, product_id, start_time, target_end_time)
        if total_sales is not None:
            monthly_kpi = cls.objects.get(pk=pk_val)
            monthly_kpi.achieved = total_sales
            monthly_kpi.last_calculated = Clock.timestamp()
            monthly_kpi.save()

    @classmethod
    def calculate_kpi(cls, *args, **kwargs):
        kpi_enabled_models = get_models_with_decorator('enable_kpi', INSTALLED_APPS, include_class=True)
        for _model_class in kpi_enabled_models:
            all_users = _model_class.objects.all()
            for each_user in all_users:
                user_entries = cls.objects.filter((Q(last_calculated__isnull=True) | (Q(last_calculated__isnull=False) & Q(last_calculated__lte=Clock.timestamp()))) & Q(user_id=each_user.pk)) # .values_list('pk','user_id','product_id','year','month','last_calculated')
                for entry in user_entries:
                    cls.calculate_monthly_kpi(entry.pk,entry.month, entry.year, entry.product_id, entry.user_id)