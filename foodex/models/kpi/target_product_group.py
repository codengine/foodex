from django.db import models
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from blackwidow.sales.models.products.product import Product
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Sohel'

@decorate(route(route='target-product-group', module=ModuleEnum.Administration,
                                              display_name='Target Products Group', group='Other admin'))
class TargetProductGroup(OrganizationDomainEntity):
    name = models.CharField(max_length=200)
    products = models.ManyToManyField(Product)

    @property
    def render_is_active(self):
        return self.is_active

    @property
    def details_config(self):
        d = super().details_config

        custom_list = [ 'code', "name", "render_is_active", 'date_created', 'last_updated' ]
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        return d

    @property
    def tabs_config(self):
        tabs = [TabView(
            title='Products(s)',
            access_key='products',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            related_model=Product,
            property=self.products
        )]
        return tabs
