from django.core.urlresolvers import reverse
from django.db.models.loading import get_model
from django.db.models.query_utils import Q
from blackwidow.core.managers.modelmanager import DomainEntityModelManager
from blackwidow.core.process.process_breakdown import ProcessBreakdown, ApprovalStatusEnum
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.sales.models.orders.order_history import OrderHistory, OrderHistoryStatusEnum
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.clients.distributor import Distributor
from foodex.models.clients.horeco import HoReCo
from foodex.models.clients.modern_trade import ModernTrade
from foodex.models.orders.pending_order import PendingOrder
from django.db import models

__author__ = 'Sohel'

class ExtendedDomainEntityModelManager(models.Manager):

    def __init__(self, *args, query=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.kwargs = kwargs
        self.query = query

    def get_queryset(self):
        queryset = super().get_queryset()
        filter_query = None
        if self.query:
            for _q in self.query:
                if not filter_query:
                    filter_query = Q(**_q)
                else:
                    filter_query |= Q(**_q)
        if filter_query:
            queryset = queryset.filter(filter_query)
        return queryset

@decorate(save_audit_log, is_object_context,
          route(route='order-rsm-approval', display_name='Awaiting RSM/MTM/HM approval', module=ModuleEnum.Execute, group_order=1, item_order=2, group='Primary Sales(Clients)'))
class OrderRSMMTMHMApproval(PendingOrder):

    objects = ExtendedDomainEntityModelManager(query=[{"type": "OrderRSMMTMHMApproval"}, { "type": "PendingOrder", "client__type__in": [ Distributor.__name__, HoReCo.__name__, ModernTrade.__name__ ]}])

    @classmethod
    def get_manage_buttons(cls):
        return [ ViewActionEnum.Edit, ViewActionEnum.Delete ]

    @classmethod
    def success_url(cls):
        return reverse(cls.get_route_name(ViewActionEnum.Manage))

    def details_link_config(self, **kwargs):

        show_approve_buttons = False

        if self.type == "PendingOrder":
            show_approve_buttons = True

        details_buttons = [
            dict(
                name='Edit',
                action='edit',
                icon='fbx-rightnav-edit',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit)
            ),
            dict(
                name='Delete',
                action='delete',
                title="Click to remove this item",
                icon='icon-remove',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Delete),
                classes='manage-action all-action confirm-action',
                parent=None
            )
            ]
        if show_approve_buttons:
            details_buttons += [
                dict(
                name='Approve',
                action='approve',
                icon='fbx-rightnav-tick',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Approve),
                classes='manage-action all-action confirm-action',
                parent=None
            ),
            dict(
                name='Reject',
                action='reject',
                icon='fbx-rightnav-cancel',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Reject),
                classes='manage-action all-action confirm-action',
                parent=None
            ),
            ]
        return details_buttons

    def approve_to(self, cls=None, *args, **kwargs):
        c_user = kwargs.get('user', None)

        if cls is None:
            cls = get_model('foodex', 'OrderNSMApproval')
        self.type = cls.__name__
        self.__class__ = cls
        if c_user:
            self.last_updated_by = c_user
        self.save()

        OrderHistory.create_from_order(self, action=OrderHistoryStatusEnum.Approved.value)

        process_breakdown = ProcessBreakdown()
        process_breakdown.approval_status = ApprovalStatusEnum.Approved
        process_breakdown.process_level = PendingOrder.__name__
        process_breakdown.description = "order approved by %s" % self.created_by.role.name
        process_breakdown.save()
        self.process_breakdown.add(process_breakdown)

        # sig_order_accepted.send(self.__class__, order=self, user=self.created_by, organization=self.organization)

        return self

    def reject_to(self, cls=None, *args, **kwargs):
        c_user = kwargs.get('user', None)

        if cls is None:
            cls = get_model('foodex', 'RejectedOrder')
        self.type = cls.__name__
        self.__class__ = cls
        if c_user:
            self.last_updated_by = c_user
        self.save()

        OrderHistory.create_from_order(self, action=OrderHistoryStatusEnum.Rejected.value)

        process_breakdown = ProcessBreakdown()
        process_breakdown.approval_status = ApprovalStatusEnum.Rejected
        process_breakdown.process_level = PendingOrder.__name__
        process_breakdown.description = "order rejected by %s" % self.created_by.role.name
        process_breakdown.save()
        self.process_breakdown.add(process_breakdown)

        return self


    class Meta:
        proxy = True
