from django.core.urlresolvers import reverse
from django.dispatch.dispatcher import receiver
from django.utils.safestring import mark_safe
from blackwidow.core.managers.modelmanager import DomainEntityModelManager
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig
from blackwidow.core.models.config.exporter_config import ExporterConfig
from blackwidow.core.models.organizations.organization import Organization
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.core.signals.signals import import_completed
from blackwidow.engine.decorators.enable_export import enable_export
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.extensions.model_descriptor import get_model_by_name
from blackwidow.sales.models.orders.order_breakdown import OrderBreakdown
from blackwidow.sales.models.orders.order_history import OrderHistory
from blackwidow.sales.models.products.product import Product
from foodex.models.infrastructure.warehouse import Warehouse
from blackwidow.engine.decorators.utility import is_object_context
from blackwidow.sales.models.orders.pending_order import *
from django.db import transaction
from decimal import Decimal
from django.db.models import Sum
from django import forms
from django.db.models import Q

__author__ = 'Sohel'


@decorate(is_object_context, enable_export, enable_import, expose_api('pending-orders'),
          route(route='pending-orders', group='Primary Sales(Clients)', group_order=1, item_order=1,
                module=ModuleEnum.Execute, display_name="New & Pending Order"))
class PendingOrder(PendingOrder):
    objects = DomainEntityModelManager(filter={"type": "PendingOrder"})

    class Meta:
        proxy = True

    @classmethod
    def success_url(cls):
        return reverse(cls.get_route_name(ViewActionEnum.Manage))

    @property
    def render_previous_credit(self):
        return self.client.available_credit  # + self.render_total_transaction_value

    @property
    def render_total_transaction_value(self):
        return self.breakdown.all().aggregate(Sum('total'))['total__sum']

    @property
    def render_available_credit(self):
        client_credit = self.client.clientcredit_set.first()
        if client_credit:
            return client_credit.render_current_available_credit
        else:
            return self.client.available_credit

    @property
    def render_over_limit(self):
        if self.client.available_credit >= 0:
            return "OK"
        else:
            return "Over credit limit"

    @property
    def render_area(self):
        return self.client.assigned_to

    @property
    def render_area_manager(self):
        if self.client.assigned_to.consoleuser_set.exists():
            return self.client.assigned_to.consoleuser_set.last()
        else:
            return None

    @property
    def render_client(self):
        if self.client:
            model = get_model_by_name(self.client.type)
            return mark_safe("<a class='inline-link' href='" +
                             reverse(model.get_route_name(ViewActionEnum.Details),
                                     kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")
        else:
            return None

    @classmethod
    def filter_query(cls, query_set, custom_search_fields=[]):
        from django.db.models import Q
        from functools import reduce
        import operator

        for key, value in custom_search_fields:
            if key.startswith("__search__areas"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(client__assigned_to__name__icontains=v))
                    orders = PendingOrder.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=orders.values_list('pk', flat=True))
                except Exception as e:
                    pass
            elif key.startswith("__search__text__over_limit"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        if v in 'OK':
                            or_list.append(Q(client__available_credit__gte=0))
                        elif v in "Over credit limit":
                            or_list.append(Q(client__available_credit__lt=0))
                    if len(or_list) > 0:
                        orders = PendingOrder.objects.filter(reduce(operator.or_, or_list))
                        query_set = query_set.filter(pk__in=orders.values_list('pk', flat=True))
                    else:
                        query_set = query_set.filter(pk__in=[])
                except Exception as e:
                    pass
            elif key.startswith("__search__area-mangers"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(client__contact_person__name__icontains=v))
                    orders = PendingOrder.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=orders.values_list('pk', flat=True))
                except Exception as e:
                    pass
        return query_set

    @classmethod
    def get_url_by_name(cls, name):
        if name == 'render_area':
            return '__search__areas'
        elif name == 'render_over_limit':
            return '__search__text__over_limit'
        elif name == 'render_area_manager':
            return '__search__area-mangers'
        return None

    @classmethod
    def table_columns(cls):
        return 'code', 'render_client', 'render_total_transaction_value', 'render_available_credit', 'render_over_limit', \
               'warehouse:Foodex Warehouse', 'render_area', 'render_area_manager', 'created_by', 'date_created:Created On', 'last_updated_by', 'last_updated'

    @classmethod
    def search_client(cls, queryset, term):
        return queryset.filter(client__name__icontains=term)

    @classmethod
    def exclude_search_fields(cls):
        return [
            "render_total_transaction_value", "render_available_credit", "render_over_limit"
        ]
    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'client', 'warehouse', 'date_created', 'last_updated']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['client_available_credit'] = self.render_available_credit
        d['total_transaction_value'] = self.render_total_transaction_value
        d['over_limit?'] = self.render_over_limit
        d['area'] = self.render_area
        d['area_manager'] = self.render_area_manager
        d['created_by'] = self.created_by
        d['created_on'] = d.pop('date_created')
        d['last_updated_by'] = self.last_updated_by
        d['last_updated_on'] = d.pop('last_updated')
        # d['location'] = self.address.location

        return d

    @classmethod
    def get_export_dependant_fields(self):
        class AdvancedExportDependentForm(GenericFormMixin):
            def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
                super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
                self.fields['infrastructure_unit__1_id'] = GenericModelChoiceField(label='Area',
                                                                                   empty_label="Select Area",
                                                                                   required=False,
                                                                                   queryset=InfrastructureUnit.objects.filter(
                                                                                       Q(type="Area") | Q(
                                                                                           type='ModernTrade') | Q(
                                                                                           type='HoReCo')),
                                                                                   widget=forms.Select(
                                                                                       attrs={'class': 'select2'}))
                self.fields['client:id'] = GenericModelChoiceField(label='Client', empty_label="Select Client",
                                                                   required=False, queryset=Client.objects.all(),
                                                                   widget=forms.TextInput(
                                                                       attrs={'class': 'select2-input', 'width': '220',
                                                                              'data-depends-on': 'infrastructure_unit__1_id',
                                                                              'data-depends-property': 'assigned_to:id',
                                                                              'data-url': reverse(Client.get_route_name(
                                                                                  action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))

        return AdvancedExportDependentForm

    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        if not organization:
            organization = Organization.objects.all().first()
        exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
        if not exporter_configs.exists():
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.starting_row = 1
            exporter_config.save(**kwargs)
        else:
            for e in exporter_configs:
                e.delete()
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.save(**kwargs)

        columns = [
            ExporterColumnConfig(column=1, column_name='Warehouse Id', property_name='get_warehouse_id', ignore=False),
            ExporterColumnConfig(column=2, column_name='Warehouse Name', property_name='get_warehouse_name',
                                 ignore=False),
            ExporterColumnConfig(column=3, column_name='Client Id', property_name='get_client_id', ignore=False),
            ExporterColumnConfig(column=4, column_name='Client Name', property_name='get_client_name', ignore=False),
            ExporterColumnConfig(column=5, column_name='Product Name', property_name='get_product_name', ignore=False),
            ExporterColumnConfig(column=6, column_name='Product Description', property_name='get_product_description',
                                 ignore=False),
            ExporterColumnConfig(column=7, column_name='Total Items', property_name='get_total_items', ignore=False),
            ExporterColumnConfig(column=8, column_name='Discount', property_name='get_discount', ignore=False)
        ]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)
        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):

        for column in columns:
            workbook.cell(row=row_number, column=column.column + 1).value = str(getattr(self, column.property_name))
        return self.pk, row_number + 1

    @classmethod
    def finalize_export(cls, workbook=None, row_number=None, query_set=None, **kwargs):
        # file_name = 'Product_price_config' + (Clock.now().strftime('%d-%m-%Y'))
        return workbook

    @classmethod
    def initialize_export(cls, workbook=None, columns=None, row_number=None, query_set=None, **kwargs):

        for column in columns:
            workbook.cell(row=1, column=column.column + 1).value = column.column_name

        row_number += 1

        query_params = kwargs.get('query_params')
        if query_params:
            client_id = query_params.get('client:id')
        try:
            client_id = int(client_id)

        except Exception:
            client_id = -1

        if client_id > 0:
            warehouse_object = Warehouse.objects.first()
            client_object = Client.objects.get(pk=client_id)

            product_inventory_objects = get_model("foodex", "WarehouseInventory").objects.filter(
                assigned_to_id=int(warehouse_object.pk)).order_by('product__id', '-last_updated').distinct(
                'product__id')
            for pi in product_inventory_objects:
                for column in columns:
                    column_value = ''
                    if column.property_name == 'get_warehouse_id':
                        column_value = str(warehouse_object.pk)
                    elif column.property_name == 'get_warehouse_name':
                        column_value = warehouse_object.name
                    elif column.property_name == 'get_client_id':
                        column_value = str(client_object.pk)
                    elif column.property_name == 'get_client_name':
                        column_value = client_object.name
                    elif column.property_name == 'get_product_name':
                        column_value = pi.product.name
                    elif column.property_name == 'get_product_description':
                        column_value = pi.product.description
                    elif column.property_name == 'get_total_items':
                        column_value = '0'
                    elif column.property_name == 'get_discount':
                        column_value = '0'

                    workbook.cell(row=row_number, column=column.column + 1).value = column_value
                row_number += 1

        return workbook, row_number

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        # ImporterColumnConfig= 'core.ImporterColumnConfig'
        # ImporterConfig = 'core.ImporterConfig'
        from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
        from blackwidow.core.models.config.importer_config import ImporterConfig

        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if result or importer_config.columns.count() == 0:
            importer_config.save(**kwargs)
        else:
            for items in importer_config.columns.all():
                items.delete()
        columns = [
            ImporterColumnConfig(column=1, column_name='Warehouse Id', property_name='get_warehouse_id', ignore=False),
            ImporterColumnConfig(column=2, column_name='Warehouse Name', property_name='get_warehouse_name',
                                 ignore=False),
            ImporterColumnConfig(column=3, column_name='Client Id', property_name='get_client_id', ignore=False),
            ImporterColumnConfig(column=4, column_name='Client Name', property_name='get_client_name', ignore=False),
            ImporterColumnConfig(column=5, column_name='Product Name', property_name='get_product_name', ignore=False),
            ImporterColumnConfig(column=6, column_name='Product Description', property_name='get_product_description',
                                 ignore=False),
            ImporterColumnConfig(column=7, column_name='Total Items', property_name='get_total_items', ignore=False),
            ImporterColumnConfig(column=8, column_name='Discount', property_name='get_discount', ignore=False)

        ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, request=None, **kwargs):
        return data

    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Edit',
                action='edit',
                icon='fbx-rightnav-edit',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit)
            ),
            dict(
                name='Delete',
                action='delete',
                title="Click to remove this item",
                icon='icon-remove',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Delete),
                classes='manage-action all-action confirm-action',
                parent=None
            ),
        ]

    @classmethod
    def get_button_title(cls, button=ViewActionEnum.Details):
        if button == ViewActionEnum.Approve:
            return "Approve"

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create, ViewActionEnum.Edit, ViewActionEnum.Delete, ViewActionEnum.AdvancedExport,
                ViewActionEnum.AdvancedImport]

    @property
    def get_inline_manage_buttons(self):
        return [
            dict(
                name='Details',
                action='view',
                title="Click to view this item",
                icon='icon-eye',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Details),
                classes='all-action ',
                parent=None
            ),
            dict(
                name='Edit',
                action='edit',
                title="Click to edit this item",
                icon='icon-pencil',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit),
                classes='all-action',
                parent=None
            ), dict(
                name='Delete',
                action='delete',
                title="Click to remove this item",
                icon='icon-remove',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Delete),
                classes='manage-action all-action confirm-action',
                parent=None
            ), dict(
                name='Approve',
                action='view',
                title="Click to accept this order",
                icon='icon-arrow-right',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Approve),
                url_params={'pk': self.pk},
                classes='manage-action all-action confirm-action',
                parent=None
            ), dict(
                name='Reject',
                action='view',
                title="Click to reject this order",
                icon='icon-arrow-right',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Reject),
                url_params={'pk': self.pk},
                classes='manage-action all-action confirm-action',
                parent=None
            )]


@receiver(import_completed, sender=PendingOrder)
def pending_order_import_completed(sender, items=None, user=None, organization=None, **kwargs):
    from blackwidow.sales.models import ProductPriceConfig, ProductPriceEnum
    data_items = {}
    for data in items:
        warehouse_id = int(data['1'])
        distributor_id = int(data['3'])
        data_tuple = (warehouse_id, distributor_id)
        if not data_tuple in data_items.keys():
            data_items[data_tuple] = [data]
        else:
            data_items[data_tuple] += [data]

    with transaction.atomic():
        for data_tuple, data_list in data_items.items():

            found_change = False

            data_set = {}
            for data in data_list:
                if int(data['7']) > 0:
                    found_change = True
                    if not data['5'] + data['6'] in data_set.keys():
                        data_set[data['5'] + data['6']] = [data]
                    else:
                        data_set[data['5'] + data['6']] += [data]

            if found_change:

                warehouse_id = data_tuple[0]
                distributor_id = data_tuple[1]
                pending_order = PendingOrder()
                pending_order.organization = organization
                pending_order.created_by = user
                pending_order.warehouse_id = warehouse_id
                pending_order.client_id = distributor_id
                pending_order.save()

                for product, data_list in data_set.items():
                    total_items = 0
                    product_name = None
                    product_description = None
                    discount = 0
                    for data in data_list:
                        product_name = data['5']
                        product_description = data['6']
                        total_items += int(data['7'])
                        discount += int(data['8'])
                    if data_list:
                        product_object = Product.objects.get(name=product_name, description=product_description)
                        ###Get the product price.
                        product_price = 0
                        ppc = ProductPriceConfig.objects.filter(product=product_object, client_id=distributor_id,
                                                                price_type__short_name=
                                                                ProductPriceEnum.gross_cust_price.value["short_name"])
                        if ppc.exists():
                            product_price = ppc[0].value

                        order_breakdown = OrderBreakdown()
                        order_breakdown.organization = organization
                        order_breakdown.created_by = user
                        order_breakdown.product = product_object
                        order_breakdown.total_items = total_items
                        order_breakdown.unit_price = product_price
                        order_breakdown.sub_total = product_price * total_items
                        order_breakdown.discount = order_breakdown.sub_total * Decimal(discount / 100)
                        order_breakdown.total = order_breakdown.sub_total - order_breakdown.discount
                        order_breakdown.save()

                        pending_order.breakdown.add(order_breakdown)
