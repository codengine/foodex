from django.core.urlresolvers import reverse
from django.db.models import Sum
from django.db.models.loading import get_model
from django.utils.safestring import mark_safe
from blackwidow.core.process.process_breakdown import ProcessBreakdown, ApprovalStatusEnum
from blackwidow.engine.decorators.enable_trigger import enable_trigger
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.engine.extensions import Clock
from blackwidow.sales.models.orders.rejected_order import RejectedOrder
from blackwidow.sales.signals.signals import sig_reverse_reject
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from foodex.models.products.approved_damage_products import ApprovedDamageProducts

__author__ = 'Sohel'


@decorate(enable_trigger, save_audit_log, is_object_context,
          route(route='rejected-orders', display_name='Rejected order', module=ModuleEnum.Execute, group_order=1,
                item_order=7, group='Primary Sales(Clients)'))
class RejectedOrder(RejectedOrder):
    class Meta:
        proxy = True

    @classmethod
    def get_button_title(cls, button=ViewActionEnum.Details):
        if button == ViewActionEnum.Approve:
            return "Reverse Rejection"
        return button.value

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Approve]

    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Reverse Rejection',
                action='approve',
                icon='fbx-rightnav-tick',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Approve),
                classes='manage-action all-action confirm-action',
                parent=None
            ), ]

    @classmethod
    def success_url(cls):
        return reverse(cls.get_route_name(ViewActionEnum.Manage))

    @property
    def render_previous_credit(self):
        return self.client.available_credit + self.render_total_transaction_value

    @property
    def render_total_transaction_value(self):
        return self.breakdown.all().aggregate(Sum('total'))['total__sum']

    @property
    def render_available_credit(self):
        client_credit = self.client.clientcredit_set.first()
        if client_credit:
            return client_credit.render_current_available_credit
        else:
            return self.client.available_credit

    @property
    def render_over_limit(self):
        if self.client.available_credit >= 0:
            return "OK"
        else:
            return "Over credit limit"

    @property
    def render_area(self):
        return self.client.assigned_to

    @property
    def render_area_manager(self):
        if self.client.assigned_to.consoleuser_set.exists():
            return self.client.assigned_to.consoleuser_set.last()
        else:
            return None

    @property
    def render_client(self):
        if self.client:
            Distributor = get_model("foodex", "Distributor")
            HoReCo = get_model("foodex", "HoReCo")
            ModernTrade = get_model("foodex", "ModernTrade")
            WholeSaler = get_model("foodex", "WholeSaler")
            if self.client.type == Distributor.__name__:
                return mark_safe(
                    "<a class='inline-link' href='" + reverse(Distributor.get_route_name(ViewActionEnum.Details),
                                                              kwargs={'pk': self.client.pk}) + "' >" + str(
                        self.client) + "</a>")
            elif self.client.type == HoReCo.__name__:
                return mark_safe(
                    "<a class='inline-link' href='" + reverse(HoReCo.get_route_name(ViewActionEnum.Details),
                                                              kwargs={'pk': self.client.pk}) + "' >" + str(
                        self.client) + "</a>")
            elif self.client.type == ModernTrade.__name__:
                return mark_safe(
                    "<a class='inline-link' href='" + reverse(ModernTrade.get_route_name(ViewActionEnum.Details),
                                                              kwargs={'pk': self.client.pk}) + "' >" + str(
                        self.client) + "</a>")
            elif self.client.type == WholeSaler.__name__:
                return mark_safe(
                    "<a class='inline-link' href='" + reverse(WholeSaler.get_route_name(ViewActionEnum.Details),
                                                              kwargs={'pk': self.client.pk}) + "' >" + str(
                        self.client) + "</a>")
        else:
            return None

    @classmethod
    def filter_query(cls, query_set, custom_search_fields=[]):
        from django.db.models import Q
        from functools import reduce
        import operator
        for key, value in custom_search_fields:
            if key.startswith("__search__areas"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(client__assigned_to__name__icontains=v))
                    orders = RejectedOrder.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=orders.values_list('pk', flat=True))
                except Exception as e:
                    pass
            elif key.startswith("__search__text__over_limit"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        if v in 'OK':
                            or_list.append(Q(client__available_credit__gte=0))
                        elif v in "Over credit limit":
                            or_list.append(Q(client__available_credit__lt=0))
                    if len(or_list) > 0:
                        orders = RejectedOrder.objects.filter(reduce(operator.or_, or_list))
                        query_set = query_set.filter(pk__in=orders.values_list('pk', flat=True))
                    else:
                        query_set = query_set.filter(pk__in=[])
                except Exception as e:
                    pass
            elif key.startswith("__search__area-mangers"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(client__contact_person__name__icontains=v))
                    orders = RejectedOrder.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=orders.values_list('pk', flat=True))
                except Exception as e:
                    pass
        return query_set

    @classmethod
    def get_url_by_name(cls, name):
        if name == 'render_area':
            return '__search__areas'
        elif name == 'render_over_limit':
            return '__search__text__over_limit'
        elif name == 'render_area_manager':
            return '__search__area-mangers'
        return None

    @classmethod
    def table_columns(cls):
        return 'code', 'render_client', 'render_total_transaction_value', 'render_available_credit', 'render_over_limit', 'warehouse:Foodex Warehouse', 'render_area', 'render_area_manager', 'created_by:Originally Created By', 'date_created:Originally Created On', 'last_updated_by:Rejected By', 'last_updated:Rejected On'

    @classmethod
    def search_client(cls, queryset, term):
        return queryset.filter(client__name__icontains=term)

    @classmethod
    def exclude_search_fields(cls):
        return [
            'render_total_transaction_value', "render_available_credit", "render_over_limit"
        ]

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'client', 'warehouse', 'date_created', 'last_updated']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['client_available_credit'] = self.render_available_credit
        d['total_transaction_value'] = self.render_total_transaction_value
        d['over_limit?'] = self.render_over_limit
        d['area'] = self.render_area
        d['area_manager'] = self.render_area_manager
        d['created_by'] = self.created_by
        d['created_on'] = d.pop('date_created')
        d['rejected_by'] = self.last_updated_by
        d['rejected_on'] = d.pop('last_updated')

        return d

    def approve_to(self, cls=None, *args, **kwargs):
        from foodex.models import OpenInvoice

        c_user = kwargs.get('user', None)

        if cls is None:
            p_breakdown = self.process_breakdown.all().order_by('-last_updated').first()
            cls_name = p_breakdown.process_level

        self.type = cls_name
        if c_user:
            self.last_updated_by = c_user

        if cls_name == get_model('foodex', 'CompletedOrder').__name__:
            total_transaction_value = self.breakdown.all().aggregate(Sum('total'))['total__sum']
            total_damage_amount = ApprovedDamageProducts.objects.filter(client=self.client).aggregate(Sum('value'))[
                'value__sum']

            open_inv = OpenInvoice()
            open_inv.system_user = c_user
            open_inv.counter_part = self.client
            open_inv.date_of_invoice = Clock.utcnow().timestamp() * 1000
            if total_damage_amount and total_damage_amount > total_transaction_value:
                open_inv.price_total = 0
            else:
                open_inv.price_total = total_transaction_value - total_damage_amount

            open_inv.actual_amount_paid = 0
            open_inv.save()
            self.invoice = open_inv

        self.save()

        process_breakdown = ProcessBreakdown()
        process_breakdown.approval_status = ApprovalStatusEnum.ReverseRejected
        process_breakdown.process_level = RejectedOrder.__name__
        process_breakdown.description = "order reverse rejection"
        process_breakdown.save()
        self.process_breakdown.add(process_breakdown)

        sig_reverse_reject.send(self.__class__, order=self, user=self.created_by, organization=self.organization)

        return self
