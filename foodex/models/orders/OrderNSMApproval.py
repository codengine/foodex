from uuid import uuid4
from django.core.urlresolvers import reverse
from django.db.models import Sum
from django.db.models.loading import get_model
from django.utils.safestring import mark_safe
from blackwidow.core.models import MaxSequence
from blackwidow.core.process.process_breakdown import ProcessBreakdown, ApprovalStatusEnum
from blackwidow.engine.decorators.enable_trigger import enable_trigger
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.engine.extensions import Clock
from blackwidow.sales.models.orders.order_history import OrderHistory, OrderHistoryStatusEnum
from blackwidow.sales.models.orders.pending_order import PendingOrder
from blackwidow.sales.models.transactions.transaction import TransactionType
from blackwidow.sales.signals.signals import sig_order_accepted
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from foodex.models.transactions.delivery_transaction import DeliveryTransaction
from foodex.models.transactions.delivery_transaction_braakdown import DeliveryTransactionBreakDown

__author__ = 'Sohel'


@decorate(enable_trigger, save_audit_log, is_object_context,
          route(route='order-nsm-approval', display_name='Awaiting NSM approval', module=ModuleEnum.Execute, group_order=1, item_order=3, group='Primary Sales(Clients)'))
class OrderNSMApproval(PendingOrder):
    class Meta:
        proxy = True

    @classmethod
    def get_button_title(cls, button=ViewActionEnum.Details):
        if button == ViewActionEnum.Approve:
            return "Approve"

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Edit, ViewActionEnum.Approve, ViewActionEnum.Reject]

    @classmethod
    def get_object_inline_buttons(cls):
        return [ ViewActionEnum.Details, ViewActionEnum.Edit ]

    @classmethod
    def success_url(cls):
        return reverse(cls.get_route_name(ViewActionEnum.Manage))

    @property
    def render_previous_credit(self):
        return self.client.available_credit + self.render_total_transaction_value

    @property
    def render_total_transaction_value(self):
        return self.breakdown.all().aggregate(Sum('total'))['total__sum']

    @property
    def render_available_credit(self):
        client_credit = self.client.clientcredit_set.first()
        if client_credit:
            return client_credit.render_current_available_credit
        else:
            return self.client.available_credit

    @property
    def render_over_limit(self):
        if self.client.available_credit >= 0:
            return "OK"
        else:
            return "Over credit limit"

    @property
    def render_area(self):
        return self.client.assigned_to

    @property
    def render_area_manager(self):
        if self.client.assigned_to.consoleuser_set.exists():
            return self.client.assigned_to.consoleuser_set.last()
        else:
            return None

    @property
    def render_client(self):
        if self.client:
            Distributor = get_model("foodex", "Distributor")
            HoReCo = get_model("foodex", "HoReCo")
            ModernTrade = get_model("foodex", "ModernTrade")
            WholeSaler = get_model("foodex", "WholeSaler")
            if self.client.type == Distributor.__name__:
                return mark_safe("<a class='inline-link' href='" + reverse(Distributor.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")
            elif self.client.type == HoReCo.__name__:
                return mark_safe("<a class='inline-link' href='" + reverse(HoReCo.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")
            elif self.client.type == ModernTrade.__name__:
                return mark_safe("<a class='inline-link' href='" + reverse(ModernTrade.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")
            elif self.client.type == WholeSaler.__name__:
                return mark_safe("<a class='inline-link' href='" + reverse(WholeSaler.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")
        else:
            return None


    @classmethod
    def filter_query(cls,query_set,custom_search_fields=[]):
        from django.db.models import Q
        from functools import reduce
        import operator
        for key, value in custom_search_fields:
            if key.startswith("__search__areas"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(client__assigned_to__name__icontains=v))
                    orders = OrderNSMApproval.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=orders.values_list('pk', flat=True))
                except Exception as e:
                    pass
            elif key.startswith("__search__text__over_limit"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        if v in 'OK':
                            or_list.append(Q(client__available_credit__gte=0))
                        elif v in "Over credit limit":
                            or_list.append(Q(client__available_credit__lt=0))
                    if len(or_list) > 0:
                        orders = OrderNSMApproval.objects.filter(reduce(operator.or_, or_list))
                        query_set = query_set.filter(pk__in=orders.values_list('pk', flat=True))
                    else:
                        query_set = query_set.filter(pk__in=[])
                except Exception as e:
                    pass
            elif key.startswith("__search__area-mangers"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(client__contact_person__name__icontains=v))
                    orders = OrderNSMApproval.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=orders.values_list('pk', flat=True))
                except Exception as e:
                    pass
        return query_set

    @classmethod
    def get_url_by_name(cls, name):
        if name == 'render_area':
            return '__search__areas'
        elif name == 'render_over_limit':
            return '__search__text__over_limit'
        elif name == 'render_area_manager':
            return '__search__area-mangers'
        return None

    @classmethod
    def search_client(cls, queryset, term):
        return queryset.filter(client__name__icontains=term)

    @classmethod
    def table_columns(cls):
        return 'code', 'render_client', 'render_total_transaction_value', 'render_available_credit', 'render_over_limit', 'warehouse:Foodex Warehouse', 'render_area', 'render_area_manager', 'created_by:Originally Created By', 'date_created:Originally Created On', 'last_updated'

    @classmethod
    def exclude_search_fields(cls):
        return [
           'render_total_transaction_value', 'render_available_credit', 'render_over_limit'
        ]

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'client', 'warehouse', 'date_created', 'last_updated']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['client_available_credit'] = self.render_available_credit
        d['total_transaction_value'] = self.render_total_transaction_value
        d['over_limit?'] = self.render_over_limit
        d['area'] = self.render_area
        d['area_manager'] = self.render_area_manager
        d['created_by'] = self.created_by
        d['created_on'] = d.pop('date_created')
        d['last_updated_by'] = self.last_updated_by
        d['last_updated_on'] = d.pop('last_updated')

        return d


    def approve_to(self, cls=None, *args, **kwargs):
        from foodex.models import DraftInvoice

        c_user = kwargs.get('user', None)

        if cls is None:
            cls = get_model('foodex', 'CompletedOrder')
        self.type = cls.__name__
        self.__class__ = cls
        if c_user:
            self.last_updated_by = c_user
        self.save()

        OrderHistory.create_from_order(self, action=OrderHistoryStatusEnum.Approved.value)

        ### create Process breakdown
        process_breakdown = ProcessBreakdown()
        process_breakdown.approval_status = ApprovalStatusEnum.Approved
        process_breakdown.process_level = OrderNSMApproval.__name__
        process_breakdown.description = "order approved by national sales manager"
        process_breakdown.save()
        self.process_breakdown.add(process_breakdown)

        ### create Draft Invoice
        total_transaction_value = self.breakdown.all().aggregate(Sum('total'))['total__sum']
        discount = self.discount if self.discount is not None else 0
        total_discount_value = (total_transaction_value*discount)/100

        gross_discount_value = self.gross_discount if self.gross_discount else 0

        tt_value_with_discount = total_transaction_value - total_discount_value - gross_discount_value


        draft_inv = DraftInvoice()
        draft_inv.system_user = c_user
        draft_inv.counter_part = self.client
        draft_inv.date_of_invoice = Clock.utcnow().timestamp() * 1000
        draft_inv.price_total = tt_value_with_discount
        draft_inv.actual_amount_paid = 0
        draft_inv.save()



        delivery_transaction = DeliveryTransaction()
        delivery_transaction.organization = self.organization
        delivery_transaction.client = self.client
        delivery_transaction.infrastructure_unit = self.warehouse
        delivery_transaction.transaction_type = TransactionType.StockAdjustment.value
        delivery_transaction.location = self.location
        delivery_transaction.transaction_time = Clock.timestamp()
        delivery_transaction.unique_id = str(uuid4())
        delivery_transaction.is_active = False
        delivery_transaction.save()

        tsubtotal = 0
        tdiscount = 0
        ttotal = 0

        for ob in self.breakdown.all():
            txx = DeliveryTransactionBreakDown()
            txx.product = ob.product
            txx.quantity = ob.total_items

            txx.unit_price = ob.unit_price
            txx.sub_total = txx.unit_price * txx.quantity
            txx.discount = ob.discount
            txx.total = txx.sub_total - txx.discount
            txx.save()


            delivery_transaction.breakdown.add(txx)

            tsubtotal += txx.sub_total
            tdiscount += txx.discount
            ttotal += (txx.sub_total - txx.discount)

        delivery_transaction.sub_total = tsubtotal
        delivery_transaction.discount = total_discount_value
        delivery_transaction.gross_discount = gross_discount_value
        delivery_transaction.total = tt_value_with_discount
        delivery_transaction.invoice = draft_inv
        delivery_transaction.save()


        self.invoice = draft_inv
        self.save()
        sig_order_accepted.send(self.__class__, order=self, user=self.created_by, organization=self.organization)
        return self

    def reject_to(self, cls=None, *args, **kwargs):
        c_user = kwargs.get('user', None)

        if cls is None:
            cls = get_model('foodex', 'RejectedOrder')
        self.type = cls.__name__
        self.__class__ = cls
        if c_user:
            self.last_updated_by = c_user
        self.save()

        OrderHistory.create_from_order(self, action=OrderHistoryStatusEnum.Rejected.value)

        process_breakdown = ProcessBreakdown()
        process_breakdown.approval_status = ApprovalStatusEnum.Rejected
        process_breakdown.process_level = OrderNSMApproval.__name__
        process_breakdown.description = "order rejected by national sales manager"
        process_breakdown.save()
        self.process_breakdown.add(process_breakdown)

        return self

    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Approve',
                action='approve',
                icon='fbx-rightnav-tick',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Approve),
                classes='manage-action all-action confirm-action',
                parent=None
            ),
            dict(
                name='Delete',
                action='delete',
                title="Click to remove this item",
                icon='icon-remove',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Delete),
                classes='manage-action all-action confirm-action',
                parent=None
            ),
            dict(
                name='Reject',
                action='reject',
                icon='fbx-rightnav-cancel',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Reject),
                classes='manage-action all-action confirm-action',
                parent=None
            ),
            ]

    @property
    def get_inline_manage_buttons(self):
        return [
            dict(
                name='Details',
                action='view',
                title="Click to view this item",
                icon='icon-eye',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Details),
                classes='all-action ',
                parent=None
            ),  dict(
                name='Approve',
                action='view',
                title="Click to accept this order",
                icon='icon-arrow-right',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Approve),
                url_params={'pk': self.pk},
                classes='manage-action all-action confirm-action',
                parent=None
            ),  dict(
                name='Reject',
                action='view',
                title="Click to reject this order",
                icon='icon-arrow-right',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Reject),
                url_params={'pk': self.pk},
                classes='manage-action all-action confirm-action',
                parent=None
            )]
            