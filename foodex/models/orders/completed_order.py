from uuid import uuid4
from decimal import Decimal
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
import inflect
from django.db.models import Sum
from django.db.models.loading import get_model
from blackwidow.core.process.process_breakdown import ProcessBreakdown, ApprovalStatusEnum
from blackwidow.engine.decorators.enable_trigger import enable_trigger
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.engine.extensions.clock import Clock
from blackwidow.engine.extensions.model_descriptor import get_model_by_name
from blackwidow.sales.models.invoice.invoice import Invoice
from blackwidow.sales.models.orders.completed_order import CompletedOrder
from blackwidow.sales.models.orders.order_history import OrderHistory, OrderHistoryStatusEnum
from blackwidow.sales.models.transactions.transaction import Transaction, PrimaryTransaction, TransactionType
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from blackwidow.sales.models.transactions.transaction_status import TransactionStatus
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from foodex.models.invoice.closed_invoice import ClosedInvoice
from foodex.models.invoice.draft_invoice import DraftInvoice
from foodex.models.inventory.warehouse_inventory import WarehouseInventory
from foodex.models.invoice.open_invoice import OpenInvoice
from foodex.models.products.approved_damage_products import ApprovedDamageProducts
from foodex.models.products.cleared_damage_products import ClearedDamageProducts
from foodex.models.transactions.delivery_transaction import DeliveryTransaction

__author__ = 'Sohel'


@decorate(expose_api('tx-stock-out'),
          enable_trigger, save_audit_log, is_object_context,
          route(route='tx-stock-out', display_name='Stock Out & Invoice', module=ModuleEnum.Execute, group_order=1,
                item_order=4, group='Primary Sales(Clients)'))
class CompletedOrder(CompletedOrder):
    class Meta:
        proxy = True

    @classmethod
    def get_button_title(cls, button=ViewActionEnum.Details):
        if button == ViewActionEnum.Approve:
            return "Stock out & Invoice"
        return button.value


    def details_link_config(self, **kwargs):
        details_buttons = []
        if not self.is_locked:
            details_buttons += [
                dict(
                    name='Edit',
                    action='edit',
                    icon='fbx-rightnav-edit',
                    ajax='0',
                    url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit)
                )
            ]
        details_buttons += [

            dict(
                name='Print Invoice',
                action='print_invoice',
                icon='fbx-rightnav-print',
                ajax='0',
                classes='manage-action popup',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Print)
            ),
            dict(
                name='Stock out & Invoice',
                action='approve',
                icon='fbx-rightnav-tick',
                ajax='0',
                classes='disabled' if self.is_locked else 'manage-action all-action confirm-action',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Approve),
                parent=None
            ),
            dict(
                name='Reject',
                action='reject',
                icon='fbx-rightnav-cancel',
                ajax='0',
                classes='disabled' if self.is_locked else 'manage-action all-action confirm-action',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Reject)
            )
        ]
        return details_buttons

    # @classmethod
    # def success_url(cls):
    # return '/' + cls.route + '/'

    @property
    def render_previous_credit(self):
        return self.client.available_credit + self.render_total_transaction_value

    @property
    def render_open_invoice_total(self):
        total_open_invoice_amount = Invoice.objects.filter(pk=self.invoice.pk).aggregate(Sum('price_total'))[
            'price_total__sum']
        if not total_open_invoice_amount:
            total_open_invoice_amount = 0
        return total_open_invoice_amount

    @property
    def render_discount_amount(self):
        total_transaction_value = self.breakdown.all().aggregate(Sum('total'))['total__sum']
        discount = self.discount if self.discount is not None else 0
        total_discount_value = (total_transaction_value * discount) / 100
        return total_discount_value

    @property
    def render_invoice_due(self):
        try:
            total_open_invoice_amount = Invoice.objects.filter(pk=self.invoice.pk).aggregate(Sum('price_total'))[
                'price_total__sum']
            total_open_invoice_paid = Invoice.objects.filter(pk=self.invoice.pk).aggregate(Sum('actual_amount_paid'))[
                'actual_amount_paid__sum']
            if not total_open_invoice_amount:
                total_open_invoice_amount = 0
            if not total_open_invoice_paid:
                total_open_invoice_paid = 0
            return total_open_invoice_amount - total_open_invoice_paid
        except:
            return 0


    @property
    def render_total_open_invoice_due_client(self):
        from blackwidow.sales.models.payments.payment_methods import Payment

        try:
            payments = Payment.objects.filter(client=self.client, date_created__lt=self.last_updated)
            total_amount_paid = 0
            for payment in payments:
                total_amount_paid = total_amount_paid + payment.amount_paid

            invoice_price_total = \
                Invoice.objects.filter(counter_part_id=self.client.pk, date_created__lt=self.last_updated).exclude(
                    pk=self.invoice_id).aggregate(invoice_price_total=Sum('price_total'))['invoice_price_total']
            if not invoice_price_total:
                invoice_price_total = 0
            return invoice_price_total - total_amount_paid
        except:
            return 0

    @property
    def render_total_invoice_balance(self):
        return self.render_total_open_invoice_due_client + self.render_total_transaction_value

    @property
    def render_total_transaction_value(self):
        return self.invoice.price_total

    @property
    def render_in_words(self):
        p = inflect.engine()
        tmp = p.number_to_words(int(self.render_total_transaction_value))
        tmp = tmp.replace(',', '').replace('-', '')
        tmp = tmp.split()
        tmp = [x.capitalize() for x in tmp]
        tmp = ' '.join(tmp)
        return tmp

    @property
    def render_available_credit(self):
        client_credit = self.client.clientcredit_set.first()
        if client_credit:
            return client_credit.render_current_available_credit
        else:
            return self.client.available_credit

    @property
    def render_over_limit(self):
        if self.client.available_credit >= 0:
            return "OK"
        else:
            return "Over credit limit"

    @property
    def render_area(self):
        return self.client.assigned_to if self.client.assigned_to else ""

    @property
    def render_area_manager(self):
        try:
            return get_model('foodex', 'AreaSalesManager').objects.filter(assigned_to_id=self.render_area.pk).first()
        except:
            return ""

    @property
    def render_status(self):
        return "Stock Out" if self.is_locked else "Pending"

    @property
    def render_client(self):
        if self.client:
            model = get_model_by_name(self.client.type)
            return mark_safe("<a class='inline-link' href='" +
                             reverse(model.get_route_name(ViewActionEnum.Details),
                                     kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")
        else:
            return None

    @property
    def render_invoice(self):
        if self.invoice:
            if self.is_locked:
                model = get_model_by_name(self.invoice.type)
                return mark_safe(
                    "<a class='inline-link' href='" +
                    reverse(model.get_route_name(ViewActionEnum.Details),
                            kwargs={'pk': self.invoice.pk}) + "' >" + str(self.invoice) + "</a>")
            else:
                return self.invoice
        else:
            return None


    @classmethod
    def filter_query(cls, query_set, custom_search_fields=[]):
        from django.db.models import Q
        from functools import reduce
        import operator

        for key, value in custom_search_fields:
            if key.startswith("__search__areas"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(client__assigned_to__name__icontains=v))
                    orders = CompletedOrder.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=orders.values_list('pk', flat=True))
                except:
                    pass
            elif key.startswith("__search__text__over_limit"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        if v in 'OK':
                            or_list.append(Q(client__available_credit__gte=0))
                        elif v in "Over credit limit":
                            or_list.append(Q(client__available_credit__lt=0))
                    if len(or_list) > 0:
                        orders = CompletedOrder.objects.filter(reduce(operator.or_, or_list))
                        query_set = query_set.filter(pk__in=orders.values_list('pk', flat=True))
                    else:
                        query_set = query_set.filter(pk__in=[])
                except Exception as e:
                    pass
        return query_set

    @classmethod
    def get_url_by_name(cls, name):
        if name == 'render_area':
            return '__search__areas'
        elif name == 'render_over_limit':
            return '__search__text__over_limit'
        return None

    @classmethod
    def search_client(cls, queryset, term):
        return queryset.filter(client__name__icontains=term)

    @classmethod
    def table_columns(cls):
        return 'code', 'render_client', 'render_invoice', 'render_total_transaction_value', 'render_available_credit', 'render_over_limit', 'render_area', 'created_by:Originally Created By', 'date_created:Originally Created On', 'last_updated_by:	Approved by NSM', 'last_updated', 'render_status'

    @classmethod
    def exclude_search_fields(cls):
        return [
            'render_total_transaction_value', 'render_available_credit', 'render_over_limit', 'render_status'
        ]

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'client', 'warehouse', 'date_created', 'last_updated']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['client_available_credit'] = self.render_available_credit
        d['total_transaction_value'] = self.render_total_transaction_value
        d['over_limit?'] = self.render_over_limit
        d['area'] = self.render_area
        d['area_manager'] = self.render_area_manager
        d['created_by'] = self.created_by
        d['created_on'] = d.pop('date_created')
        d['last_updated_by'] = self.last_updated_by
        d['last_updated_on'] = d.pop('last_updated')

        return d

    @classmethod
    def get_object_inline_buttons(cls):
        return [ViewActionEnum.Details]


    @classmethod
    def get_manage_buttons(cls):
        return []

    def approve_to(self, cls=None, *args, **kwargs):

        invoice = self.invoice
        # print(invoice)
        delivery_transaction = Transaction.objects.get(type=DeliveryTransaction.__name__, invoice_id=invoice.pk,
                                                       is_active=False)
        # print(delivery_transaction)
        delivery_transaction.is_active = True
        txbreakdown = delivery_transaction.breakdown.all()
        for txbrk in txbreakdown:
            txbrk.status = TransactionStatus.InTransit.value
            txbrk.save()
        delivery_transaction.save()

        c_user = kwargs.get('user', None)
        # sig_stock_out.send(self.__class__, order=self, user=self.created_by, organization=self.organization)

        if c_user:
            self.last_updated_by = c_user
        self.is_locked = True
        self.save()

        OrderHistory.create_from_order(self, action=OrderHistoryStatusEnum.Approved.value)

        process_breakdown = ProcessBreakdown()
        process_breakdown.approval_status = ApprovalStatusEnum.Approved
        process_breakdown.process_level = CompletedOrder.__name__
        process_breakdown.description = "order approved and stocked out"
        process_breakdown.save()
        self.process_breakdown.add(process_breakdown)

        tx = PrimaryTransaction()
        tx.client = self.client
        tx.organization = self.organization
        tx.infrastructure_unit = self.warehouse
        tx.location = self.location
        tx.transaction_time = Clock.timestamp()
        tx.transaction_type = TransactionType.Sale.value
        tx.unique_id = str(uuid4())
        tx.invoice = self.invoice
        tx.save()

        tx_wh_adjustment = PrimaryTransaction()
        tx_wh_adjustment.organization = self.organization
        tx_wh_adjustment.client = self.client
        tx_wh_adjustment.infrastructure_unit = self.warehouse
        tx_wh_adjustment.transaction_type = TransactionType.StockAdjustment.value
        tx_wh_adjustment.location = self.location
        tx_wh_adjustment.transaction_time = Clock.timestamp()
        tx_wh_adjustment.unique_id = str(uuid4())
        tx_wh_adjustment.save()

        tsubtotal = 0
        tdiscount = 0
        ttotal = 0

        for ob in self.breakdown.all():
            txx = TransactionBreakDown()
            txx.product = ob.product
            txx.quantity = ob.total_items

            product_unit_price = getattr(ob.product, 'get_gross_cust_price')

            txx.unit_price = ob.unit_price
            txx.sub_total = txx.unit_price * txx.quantity
            txx.discount = ob.discount
            txx.total = txx.sub_total - txx.discount
            txx.save()

            tx.breakdown.add(txx)
            tx_wh_adjustment.breakdown.add(txx)

            tsubtotal += txx.sub_total
            tdiscount += txx.discount
            ttotal += (txx.sub_total - txx.discount)

            # update warehouse inventory
            whouse_inventories = WarehouseInventory.objects.filter(assigned_to=tx.infrastructure_unit,
                                                                   product=txx.product)
            if whouse_inventories.exists():
                wi = whouse_inventories.first()
                txx.opening_stock = wi.stock
                txx.closing_stock = wi.stock - txx.quantity
                txx.save()

                wi.stock = wi.stock - txx.quantity
                wi.save()
            else:
                ###Alert saying no stock exists in the warehouse inventory.
                pass

        tx.sub_total = tsubtotal
        tx.discount = self.discount if self.discount else 0
        tx.total = ttotal - ((ttotal * tx.discount) / 100)
        tx.save()

        tx_wh_adjustment.sub_total = tsubtotal
        tx_wh_adjustment.discount = self.discount if self.discount else 0
        tx_wh_adjustment.total = ttotal - ((ttotal * tx_wh_adjustment.discount) / 100)
        tx_wh_adjustment.save()

        tx.save()
        tx.client.available_credit -= tx.total
        tx.client.save()

        ### create open invoice
        open_inv = self.invoice
        cls = get_model('foodex', 'OpenInvoice')
        open_inv.type = cls.__name__
        open_inv.__class__ = cls
        open_inv.save()

        return self

    def reject_to(self, cls=None, *args, **kwargs):
        c_user = kwargs.get('user', None)

        if cls is None:
            cls = get_model('foodex', 'RejectedOrder')
        self.type = cls.__name__
        self.__class__ = cls
        if c_user:
            self.last_updated_by = c_user
        invoice = self.invoice
        self.invoice = None
        self.save()

        OrderHistory.create_from_order(self, action=OrderHistoryStatusEnum.Rejected.value)

        if invoice:
            Payment = get_model('sales', 'Payment')
            Payment.objects.filter(invoice_id= invoice.pk).delete();

            tx_objects = Transaction.objects.filter(invoice_id=invoice.pk)
            for tx_object in tx_objects:
                tx_object.invoice = None
                tx_object.save()

        invoice.delete()


        process_breakdown = ProcessBreakdown()
        process_breakdown.approval_status = ApprovalStatusEnum.Rejected
        process_breakdown.process_level = CompletedOrder.__name__
        process_breakdown.description = "order rejected "
        process_breakdown.save()
        self.process_breakdown.add(process_breakdown)

        return self
