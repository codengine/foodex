from decimal import Decimal
from uuid import uuid4
from django.db import transaction
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from blackwidow.core.managers.modelmanager import DomainEntityModelManager
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.engine.extensions.clock import Clock
from blackwidow.sales.models.transactions.transaction import Transaction, PrimaryTransaction, TransactionType
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from blackwidow.sales.models.transactions.transaction_status import TransactionStatus
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.inventory.client_inventory import ClientInventory
from foodex.models.transactions.completed_sales_transaction import CompletedSalesTransaction
from foodex.models.transactions.delivery_transaction_braakdown import DeliveryTransactionBreakDown

__author__ = 'Sohel'


@decorate(save_audit_log, is_object_context,
          expose_api("incoming-inventories"),
          route(route='incoming-inventories', module=ModuleEnum.Execute, group_order=1, item_order=5, display_name='In-Transit Stock', group='Primary Sales(Clients)'))
class DeliveryTransaction(Transaction):

    objects = DomainEntityModelManager(filter={'type': 'DeliveryTransaction','status': TransactionStatus.Open.value, 'is_active': True})

    def save(self, *args, **kwargs):
        self.type = self.__class__.__name__
        self.is_active = False if not kwargs.get('is_active') else True
        super().save(*args,**kwargs)

    @classmethod
    def get_button_title(cls, button=ViewActionEnum.Details):
        if button == ViewActionEnum.Approve:
            return "Receive Order"
        return button.value

    @property
    def render_completed_sales_transaction_ID(self):
        completed_sales_transaction = CompletedSalesTransaction.objects.filter(invoice=self.invoice)
        if completed_sales_transaction.exists():
            completed_sales_transaction = completed_sales_transaction.first()
            return completed_sales_transaction
        return ""

    @property
    def render_client(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.client.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")

    @classmethod
    def search_client(cls, queryset, term):
        return queryset.filter(client__name__icontains=term)


    @classmethod
    def exclude_search_fields(cls):
        return [
            'total', 'render_completed_sales_transaction_ID'
        ]

    @classmethod
    def table_columns(cls):
        return 'code', 'total:Total transaction value', 'render_client', 'render_completed_sales_transaction_ID', 'last_updated', 'last_updated_by'

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['Client'] = self.render_client
        d['Total transaction value'] = self.total
        d['completed_sales_transaction_ID'] = self.render_completed_sales_transaction_ID
        d['Created BY'] = self.created_by
        d['Created On'] = self.render_timestamp(self.date_created)
        d['Last Updated On'] = self.render_timestamp(self.last_updated)
        d['Last Updated By'] = self.last_updated_by

        return d

    @classmethod
    def success_url(cls):
        return reverse(cls.get_route_name(ViewActionEnum.Manage))

    def reverse_calculate_discount_percentage(self):
        total_amount = self.total + self.discount + self.gross_discount

        return (self.discount / total_amount) * 100 if total_amount > 0 else 0

    def approve_to(self, cls=None, *args, **kwargs):

        with transaction.atomic():
            request_data_dict = kwargs.get("data")
            extra = request_data_dict.get("extra")

            if not extra:
                return self

            extra = extra.strip().split("&")
            breakdown_receive_values = {}
            for e in extra:
                if not e:
                    continue
                breakdown_receive_values[int(e.split("=")[0])] = Decimal(e.split("=")[1]) if Decimal(e.split("=")[1]) > 0 else 0

            for brkdown in self.breakdown.all():
                if not breakdown_receive_values.get(brkdown.pk):
                    breakdown_receive_values[brkdown.pk] = 0

            if all(value == 0 for key,value in breakdown_receive_values.items()):
                ###Generate an alert.
                return self

            tx_client_adjustment = PrimaryTransaction()
            tx_client_adjustment.organization = self.organization
            tx_client_adjustment.client = self.client
            tx_client_adjustment.infrastructure_unit = self.infrastructure_unit
            tx_client_adjustment.transaction_type = TransactionType.StockAdjustment.value
            tx_client_adjustment.location = self.location
            tx_client_adjustment.transaction_time = Clock.timestamp()
            tx_client_adjustment.unique_id = str(uuid4())
            tx_client_adjustment.invoice = self.invoice
            tx_client_adjustment.save()

            txx_breakdowns = []

            tsubtotal = 0
            tdiscount = 0
            ttotal = 0

            discount_percentage = self.reverse_calculate_discount_percentage()



            # all_status_closed = True

            total_without_discount = 0

            for ob in self.breakdown.all():
                if ob.status == TransactionStatus.Close.value:
                    continue
                if breakdown_receive_values.get(ob.pk) > 0 and breakdown_receive_values.get(ob.pk) <= (ob.quantity - ob.stock_received):
                    txx = TransactionBreakDown()
                    txx.product = ob.product
                    txx.quantity = breakdown_receive_values.get(ob.pk)

                    if breakdown_receive_values.get(ob.pk) == (ob.quantity - ob.stock_received):
                        ob.status = TransactionStatus.Close.value
                        ob.save()
                    # else:
                    #     all_status_closed = False

                    txx.stock_received = breakdown_receive_values.get(ob.pk)
                    ob.stock_received += breakdown_receive_values.get(ob.pk)
                    ob.save()

                    txx.unit_price = ob.unit_price
                    txx.sub_total = txx.unit_price * txx.quantity
                    txx.discount = ob.discount
                    txx.total = txx.sub_total - txx.discount
                    txx.save()


                    tx_client_adjustment.breakdown.add(txx)

                    tsubtotal += txx.sub_total
                    tdiscount += txx.discount
                    ttotal += (txx.sub_total - txx.discount)

                    total_without_discount += txx.sub_total

                    # update client inventory
                    c_inventories = ClientInventory.objects.filter(assigned_to=self.client, product=txx.product)
                    if c_inventories.exists():
                        ci = c_inventories.first()
                        txx.opening_stock = ci.stock
                        txx.closing_stock = ci.stock + txx.quantity
                        txx.save()

                        #print(ci.stock)

                        ci.stock = ci.stock + txx.quantity
                        ci.save()
                    else:
                        txx.opening_stock = 0
                        txx.closing_stock = txx.quantity
                        txx.save()

                        c_inventory = ClientInventory()
                        c_inventory.assigned_to = self.client
                        c_inventory.product = txx.product
                        c_inventory.organization = self.organization
                        c_inventory.stock = txx.quantity
                        c_inventory.save()

                    txx_breakdowns += [ txx ]

                # if ob.status != TransactionStatus.Close.value:
                #     all_status_closed = False

            total_discount_value = (tsubtotal*discount_percentage)/100

            if txx_breakdowns:
                tx_client_adjustment.sub_total = tsubtotal
                tx_client_adjustment.discount = total_discount_value
                tx_client_adjustment.total = total_without_discount - total_discount_value - self.gross_discount
                tx_client_adjustment.gross_discount = self.gross_discount
                tx_client_adjustment.save()
                all_status_closed = True
                for ob in self.breakdown.all():
                    if ob.status != TransactionStatus.Close.value:
                        all_status_closed = False
                if all_status_closed:
                    self.status = TransactionStatus.Close.value
                self.save(is_active=True)
            else:
                tx_client_adjustment.delete()
        return self

    @classmethod
    def get_object_inline_buttons(cls):
        return [ ViewActionEnum.Details ]

    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Receive Order',
                action='approve',
                icon='fbx-rightnav-tick',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Approve),
                classes='manage-action all-action confirm-action require-post-method require-extra-param',
                parent=None
            )
        ]

    class Meta:
        proxy = True

    @property
    def tabs_config(self):
        delivery_tx_breakdown_set = DeliveryTransactionBreakDown.objects.filter(pk__in=self.breakdown.values_list('id', flat=True))
        tabs = [TabView(
            title='Transaction Details',
            access_key='breakdown',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            related_model=DeliveryTransactionBreakDown,
            property=self.breakdown,
            queryset=delivery_tx_breakdown_set
        )]
        return tabs
