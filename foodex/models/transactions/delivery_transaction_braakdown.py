from unicodedata import decimal
from django.utils.safestring import mark_safe
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from blackwidow.sales.models.transactions.transaction_status import TransactionStatus

__author__ = 'zia'


class DeliveryTransactionBreakDown(TransactionBreakDown):

    class Meta:
        proxy = True

    @property
    def render_receive_stock(self):
        #print(self.stock_received),self.status
        input_field = mark_safe('<input type="number" id="delivery_transaction_breakdown_'+ str(self.pk) +'" class="inline-receive-stock" name="receive_stock" min="0" max="' + str(self.quantity - self.stock_received) + '" value="%s" ></input>' % str(self.quantity - self.stock_received))
        if self.status == TransactionStatus.Close.value:
            input_field = mark_safe('<span class="inline-receive-stock-span" name="receive_stock-span"" >%s</span>' % str(self.quantity))
        return input_field

    @property
    def render_status(self):
        return TransactionStatus.get_name(self.status)

    @classmethod
    def table_columns(cls):
        return 'code', 'product:Product Name', 'quantity:incoming_quantity', 'render_receive_stock','render_status'

