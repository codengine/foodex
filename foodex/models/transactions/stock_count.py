from uuid import uuid4
from django import forms
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from blackwidow.core.managers.modelmanager import DomainEntityModelManager
from blackwidow.core.mixins.fieldmixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin import GenericFormMixin
from blackwidow.core.models import ExporterConfig, ExporterColumnConfig, ContactAddress, Location
from blackwidow.core.models.organizations.organization import Organization
from blackwidow.engine.decorators.utility import decorate, is_object_context
from blackwidow.engine.extensions.clock import Clock
from blackwidow.sales.models.common.product_price import ProductPriceEnum
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.transactions.transaction import TransactionType
from blackwidow.sales.models.transactions.stock_count import StockCount
from blackwidow.engine.decorators.route_partial_routes import route
from config.enums import ViewActionEnum
from blackwidow.engine.decorators.enable_export import enable_export
from config.enums.modules_enum import ModuleEnum
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from foodex.models import Warehouse, WarehouseInventory
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.core.signals.signals import import_completed
from django.dispatch.dispatcher import receiver
from multiprocessing.synchronize import Lock
from django.db import transaction
from threading import Thread

__author__ = 'Sohel'


@decorate(is_object_context, enable_export, enable_import,
          route(route='stock-count-transactions', group='Inventory', module=ModuleEnum.Execute, group_order=4, item_order=2, display_name="Foodex Warehouse Stock Count"))
class StockCountTransaction(StockCount):
    objects = DomainEntityModelManager(filter={'transaction_type': TransactionType.StockCount.value, 'type': 'StockCountTransaction'})

    @property
    def render_warehouse(self):
        return self.infrastructure_unit

    @property
    def render_code(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.pk}) + "' >" + self.code + "</a>")


    @classmethod
    def table_columns(cls):
        return 'render_code', 'render_warehouse', 'created_by', 'date_created:Created On'

    def save(self, *args, **kwargs):
        self.transaction_time = Clock.timestamp()
        super().save(*args, **kwargs)

    class Meta:
        proxy = True


    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'date_created']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]

        d['warehouse'] = self.render_warehouse
        d['created_by'] = self.created_by
        d['Remarks'] = self.remarks if self.remarks else 'N/A'

        return d



    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create, ViewActionEnum.AdvancedExport, ViewActionEnum.AdvancedImport]


    @classmethod
    def get_export_dependant_fields(self):
        class AdvancedExportDependentForm(GenericFormMixin):
            def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
                super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
                self.fields['infrastructure_unit'] = GenericModelChoiceField(label='Warehouse', queryset=Warehouse.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))

        return AdvancedExportDependentForm


    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        if not organization:
            organization = Organization.objects.all().first()
        exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
        if not exporter_configs.exists():
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.starting_row = 1
            exporter_config.save(**kwargs)
        else:
            for e in exporter_configs:
                e.delete()
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.save(**kwargs)

        columns = [
            ExporterColumnConfig(column=0, column_name='Warehouse Id', property_name='get_warehouse_id', ignore=False),
            ExporterColumnConfig(column=1, column_name='Warehouse Name', property_name='get_warehouse_name', ignore=False),
            ExporterColumnConfig(column=2, column_name='Product Id', property_name='get_product_id', ignore=False),
            ExporterColumnConfig(column=3, column_name='Product Name', property_name='get_product_name', ignore=False),
            ExporterColumnConfig(column=4, column_name='Product Description', property_name='get_product_description', ignore=False),
            ExporterColumnConfig(column=5, column_name='Current Stock', property_name='get_previous_stock_quantity', ignore=False),
            ExporterColumnConfig(column=6, column_name='New Physical Stock', property_name='get_incoming_stock_quantity', ignore=False)
        ]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)
        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):

        # for column in columns:
        #     workbook.cell(row=row_number, column=column.column + 1).value = str(getattr(self, column.property_name))
        return self.pk, row_number + 1

    @classmethod
    def finalize_export(cls, workbook=None, row_number=None, query_set=None, **kwargs):
        return workbook

    @classmethod
    def initialize_export(cls, workbook=None, columns=None, row_number=None, query_set=None, **kwargs):

        for column in columns:
            workbook.cell(row=1, column=column.column + 1).value = column.column_name

        row_number += 1

        query_params = kwargs.get('query_params')
        warehouse_id = query_params.get('infrastructure_unit')
        warehouse_object = Warehouse.objects.get(pk=int(warehouse_id))

        # product_inventories_pk = list()
        # for product in Product.objects.all():
        #     product_inventories = InfrastructureUnitInventory.objects.filter(product=product).order_by('-last_updated')
        #     if product_inventories.exists():
        #         product_inventories_pk.append(int(product_inventories.first().pk))
        # product_inventory_objects = InfrastructureUnitInventory.objects.filter(pk__in=product_inventories_pk)

        product_inventory_objects = WarehouseInventory.objects.filter(assigned_to_id=int(warehouse_id))#.order_by('product', '-last_updated').distinct('product')
        for product_inventory in product_inventory_objects:
            for column in columns:
                column_value = ''
                if column.property_name == 'get_warehouse_id':
                    column_value = str(warehouse_id)
                elif column.property_name == 'get_warehouse_name':
                    column_value = warehouse_object.name
                elif column.property_name == 'get_product_id':
                    column_value = product_inventory.product.id
                elif column.property_name == 'get_product_name':
                    column_value = product_inventory.product.name
                elif column.property_name == 'get_product_description':
                    column_value = product_inventory.product.description
                elif column.property_name == 'get_previous_stock_quantity':
                    column_value = product_inventory.stock
                elif column.property_name == 'get_incoming_stock_quantity':
                    column_value = product_inventory.stock


                workbook.cell(row=row_number, column=column.column + 1).value = column_value
            row_number += 1

        return workbook, row_number


    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
        from blackwidow.core.models.config.importer_config import ImporterConfig
        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if result or importer_config.columns.count() == 0:
            importer_config.save(**kwargs)
        else:
            for items in importer_config.columns.all():
                items.delete()
        columns = [
            ImporterColumnConfig(column=0, column_name='Warehouse Id', property_name='get_warehouse_id', ignore=False),
            ImporterColumnConfig(column=1, column_name='Warehouse Name', property_name='get_warehouse_name', ignore=False),
            ImporterColumnConfig(column=2, column_name='Product Id', property_name='get_product_id', ignore=False),
            ImporterColumnConfig(column=3, column_name='Product Name', property_name='get_product_name', ignore=False),
            ImporterColumnConfig(column=4, column_name='Product Description', property_name='get_product_description', ignore=False),
            ImporterColumnConfig(column=5, column_name='Current Stock', property_name='get_previous_stock_quantity', ignore=False),
            ImporterColumnConfig(column=6, column_name='New Physical Stock', property_name='get_incoming_stock_quantity', ignore=False)
        ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config



    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, request=None, **kwargs):
        return data




def save_transaction_details(items,user,organization):
    warehouses = {}
    #print(items)
    for data in items:
        w_id = int(data['0'])
        if not w_id in warehouses.keys():
            warehouses[int(data['0'])] = [data]
        else:
            warehouses[int(data['0'])] += [data]

    with transaction.atomic():
        for warehouse_id, data in warehouses.items():
            custom_data = list()

            for data_row in data:
                if int(data_row['5']) != int(data_row['6']):
                    custom_data.append(data_row)

            if not custom_data:
                continue

            warehouse_objects = Warehouse.objects.filter(pk=warehouse_id)
            if not warehouse_objects.exists():
                warehouse_object = Warehouse()
                warehouse_object.name = data[0]['1']
                warehouse_object.organization = organization
                warehouse_object.created_by = user

                contact_address = ContactAddress()
                contact_address.street = 'Azhar Comfort Complex.'
                contact_address.city = 'Dhaka'
                contact_address.province = 'Dhaka'
                contact_address.save()

                warehouse_object.address = contact_address

                warehouse_object.save()
                warehouse_id = warehouse_object.pk


            tx_stock_count = StockCountTransaction()
            tx_stock_count.organization = organization
            tx_stock_count.infrastructure_unit_id = warehouse_id
            location = Location()
            location.save()
            tx_stock_count.location = location
            tx_stock_count.transaction_time = Clock.timestamp()
            tx_stock_count.transaction_type = TransactionType.StockCount.value
            tx_stock_count.unique_id = str(uuid4())
            tx_stock_count.created_by = user
            tx_stock_count.save()

            tx_subtotal = 0
            tx_total = 0
            tx_discount = 0

            for data_row in custom_data:
                product_object = Product.objects.get(pk=int(data_row['2']))
                product_unit_price = product_object.prices.filter(price__name=ProductPriceEnum.gross_cust_price.value["name"]).first().value

                tx_breakdown = TransactionBreakDown()
                tx_breakdown.product_id = int(data_row['2'])
                tx_breakdown.organization = organization

                whouse_inventories = WarehouseInventory.objects.filter(assigned_to_id=warehouse_id, product_id=int(data_row['2'])).order_by('-last_updated')
                if whouse_inventories.exists():
                    # update transaction breakdown
                    tx_breakdown.opening_stock = whouse_inventories.first().stock
                    tx_breakdown.quantity = int(data_row['6'])
                    tx_breakdown.closing_stock = tx_breakdown.quantity
                    tx_breakdown.unit_price = product_unit_price
                    tx_breakdown.sub_total = tx_breakdown.quantity * tx_breakdown.unit_price
                    tx_breakdown.discount = 0
                    tx_breakdown.total = tx_breakdown.sub_total - tx_breakdown.discount

                    # update warehouse inventory
                    whouse_inventory = whouse_inventories.first()
                    whouse_inventory.stock = tx_breakdown.quantity
                    whouse_inventory.last_updated_by = user
                    whouse_inventory.save()

                tx_breakdown.save()
                tx_stock_count.breakdown.add(tx_breakdown)

                tx_subtotal += tx_breakdown.sub_total
                tx_discount += tx_breakdown.discount
                tx_total += tx_breakdown.total

            tx_stock_count.sub_total = tx_subtotal
            tx_stock_count.discount = tx_discount
            tx_stock_count.total = tx_total
            tx_stock_count.save()

@receiver(import_completed, sender=StockCountTransaction)
def stock_count_import_completed(sender, items=None, user=None, organization=None, **kwargs):
    lock = Lock(ctx=None)
    lock.acquire(True)
    process = Thread(target=save_transaction_details, args=(items,user,organization,))
    process.start()
    lock.release()
