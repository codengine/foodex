from django.core.urlresolvers import reverse
from django.db.models.loading import get_model
from blackwidow.core.managers.modelmanager import DomainEntityModelManager
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.decorators.enable_trigger import enable_trigger
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log
from blackwidow.sales.models.transactions.transaction import PrimaryTransaction, TransactionType
from config.enums.modules_enum import ModuleEnum
from django.utils.safestring import mark_safe
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum
from blackwidow.sales.models.transactions.transaction_break_down import TxTransactionBreakdown, OrderTransactionBreakdown

__author__ = 'Sohel'


@decorate(expose_api('primary-transactions'), enable_trigger, save_audit_log,
          route(route='primary-transactions', display_name='Completed Sales Transaction', module=ModuleEnum.Execute, group_order=1, item_order=6, group='Primary Sales(Clients)'))
class CompletedSalesTransaction(PrimaryTransaction):

    objects = DomainEntityModelManager(filter={'transaction_type': TransactionType.Sale.value})

    def __str__(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.pk}) + "' >" + self.code + "</a>")

    class Meta:
        proxy = True

    @property
    def render_type(self):
        return TransactionType.get_name(self.transaction_type)

    @property
    def render_sold_by(self):
        return self.last_updated_by

    @property
    def render_code(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.pk}) + "' >" + self.code + "</a>")

    @property
    def render_sold_to(self):
        if self.client:
            Distributor = get_model("foodex", "Distributor")
            HoReCo = get_model("foodex", "HoReCo")
            ModernTrade = get_model("foodex", "ModernTrade")
            WholeSaler = get_model("foodex", "WholeSaler")
            if self.client.type == Distributor.__name__:
                return mark_safe("<a class='inline-link' href='" + reverse(Distributor.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")
            elif self.client.type == HoReCo.__name__:
                return mark_safe("<a class='inline-link' href='" + reverse(HoReCo.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")
            elif self.client.type == ModernTrade.__name__:
                return mark_safe("<a class='inline-link' href='" + reverse(ModernTrade.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")
            elif self.client.type == WholeSaler.__name__:
                return mark_safe("<a class='inline-link' href='" + reverse(WholeSaler.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")
        else:
            return None



    @classmethod
    def table_columns(cls):
        return 'render_code', 'infrastructure_unit:From Warehouse', 'render_sold_by', 'render_sold_to', 'total:amount', 'transaction_time', 'last_updated:Sync Time', 'last_updated_by'

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'transaction_time']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['Type'] = self.render_type
        d['From Warehouse'] = self.infrastructure_unit
        d['Sold By'] = self.last_updated_by
        d['Sold To'] = self.client
        d['Amount'] = self.total
        d['Last Updated On'] = self.render_timestamp(self.last_updated)
        d['Last Updated By'] = self.last_updated_by

        return d

    @property
    def tabs_config(self):

        tabs = [TabView(
            title='Order Breakdown(s)',
            access_key='txorderbreakdown',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            related_model=OrderTransactionBreakdown,
            property=self.breakdown
        ),TabView(
            title='Transaction Details(s)',
            access_key='txbreakdown',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            related_model=TxTransactionBreakdown,
            property=self.breakdown
        )]
        return tabs
