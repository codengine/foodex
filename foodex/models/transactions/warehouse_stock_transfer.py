from threading import Thread
from uuid import uuid4
from django.dispatch.dispatcher import receiver
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig
from blackwidow.core.models.config.exporter_config import ExporterConfig
from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
from blackwidow.core.models.config.importer_config import ImporterConfig
from blackwidow.core.signals.signals import import_completed
from blackwidow.engine.decorators.enable_export import enable_export
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.enable_trigger import enable_trigger
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.engine.extensions.clock import Clock
from blackwidow.sales.models.common.product_price import ProductPriceEnum
from blackwidow.sales.models.transactions.transaction import TransactionType
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.transactions.stock_transfer import StockTransfer
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from config.enums.modules_enum import ModuleEnum
from multiprocessing.synchronize import Lock
from django.db import transaction
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.inventory.warehouse_inventory import WarehouseInventory
from foodex.models.infrastructure.warehouse import Warehouse
from django import forms

__author__ = 'Sohel'

@decorate(expose_api('wh-stock-transfer'), enable_export,enable_import,
          enable_trigger, save_audit_log, is_object_context,
          route(route='wh-stock-transfer', display_name='Warehouse Stock Transfer', module=ModuleEnum.Execute, group='Inventory', hide=True))
class WarehouseStockTransfer(StockTransfer):

    @property
    def render_stock_change_quantity(self):
        return self.breakdown.all().first().quantity if self.breakdown.all().exists() else ''

    @classmethod
    def table_columns(cls):
        return 'code', 'infrastructure_unit:From Warehouse', 'infrastructure_unit_2:To Warehouse', 'created_by:Created By', 'date_created:Created On'

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'date_created']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['Created On'] = d.pop('date_created')
        d['created_by'] = self.created_by
        d['From Warehouse'] = self.infrastructure_unit
        d['To Warehouse'] = self.infrastructure_unit_2


        return d

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    @classmethod
    def get_export_dependant_fields(self):
        class AdvancedExportDependentForm(GenericFormMixin):
            def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
                super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
                self.fields['infrastructure_unit'] = GenericModelChoiceField(label='Warehouse From',queryset=Warehouse.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
                self.fields['infrastructure_unit_2'] = GenericModelChoiceField(label='Warehouse To',queryset=Warehouse.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))

        return AdvancedExportDependentForm

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create, ViewActionEnum.AdvancedExport, ViewActionEnum.AdvancedImport]

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        importer_config, created = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if created or importer_config.columns.count() == 0:
            importer_config.starting_row = 1
            importer_config.save(**kwargs)
        else:
            for items in importer_config.columns.all():
                items.delete()
        columns = [
            ImporterColumnConfig(column=1, column_name='Warehouse From Id', property_name='get_warehouse_from_id', ignore=False),
            ImporterColumnConfig(column=2, column_name='Warehouse From Name', property_name='get_warehouse_from_name', ignore=False),
            ImporterColumnConfig(column=3, column_name='Warehouse To Id', property_name='get_warehouse_to_id', ignore=False),
            ImporterColumnConfig(column=4, column_name='Warehouse To Name', property_name='get_warehouse_to_name', ignore=False),
            ImporterColumnConfig(column=5, column_name='Product Name', property_name='get_product_name', ignore=False),
            ImporterColumnConfig(column=6, column_name='Product Description', property_name='get_product_description', ignore=False),
            ImporterColumnConfig(column=7, column_name='Stock Available', property_name='get_stock_available', ignore=False),
            ImporterColumnConfig(column=8, column_name='Stock Transfer Quantity', property_name='get_stock_transfer_quantity', ignore=False)
        ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.starting_row = 1
            importer_config.columns.add(c)
        return importer_config

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        return data

    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
        if not exporter_configs.exists():
            exporter_config = ExporterConfig()
            exporter_config.starting_row = 1
            exporter_config.save(**kwargs)
        else:
            for e in exporter_configs:
                e.delete()
            exporter_config = ExporterConfig()
            exporter_config.save(**kwargs)

        columns = [
            ExporterColumnConfig(column=1, column_name='Warehouse From Id', property_name='get_warehouse_from_id', ignore=False),
            ExporterColumnConfig(column=2, column_name='Warehouse From Name', property_name='get_warehouse_from_name', ignore=False),
            ExporterColumnConfig(column=3, column_name='Warehouse To Id', property_name='get_warehouse_to_id', ignore=False),
            ExporterColumnConfig(column=4, column_name='Warehouse To Name', property_name='get_warehouse_to_name', ignore=False),
            ExporterColumnConfig(column=5, column_name='Product Name', property_name='get_product_name', ignore=False),
            ExporterColumnConfig(column=6, column_name='Product Description', property_name='get_product_description', ignore=False),
            ExporterColumnConfig(column=7, column_name='Stock Available', property_name='get_stock_available', ignore=False),
            ExporterColumnConfig(column=8, column_name='Stock Transfer Quantity', property_name='get_stock_transfer_quantity', ignore=False)
        ]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)
        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):

        # for column in columns:
        #     workbook.cell(row=row_number, column=column.column + 1).value = str(getattr(self, column.property_name))
        return self.pk, row_number + 1

    @classmethod
    def finalize_export(cls, workbook=None, row_number=None, query_set=None, **kwargs):
        #file_name = 'Product_price_config' + (Clock.now().strftime('%d-%m-%Y'))
        return workbook

    @classmethod
    def initialize_export(cls, workbook=None, columns=None, row_number=None, query_set=None, **kwargs):

        for column in columns:
            workbook.cell(row=1, column=column.column + 1).value = column.column_name

        row_number += 1

        query_params = kwargs.get('query_params')
        warehouse_from_id = query_params.get('infrastructure_unit')
        warehouse_to_id = query_params.get('infrastructure_unit_2')
        warehouse_from_object = Warehouse.objects.get(pk=int(warehouse_from_id))
        warehouse_to_object = Warehouse.objects.get(pk=int(warehouse_to_id))

        product_inventory_objects = WarehouseInventory.objects.filter(assigned_to_id=int(warehouse_from_id)).order_by('product', '-last_updated').distinct('product')
        for pi in product_inventory_objects:
            for column in columns:
                column_value = ''
                if column.property_name == 'get_warehouse_from_id':
                    column_value = str(warehouse_from_id)
                elif column.property_name == 'get_warehouse_from_name':
                    column_value = warehouse_from_object.name
                elif column.property_name == 'get_warehouse_to_id':
                    column_value = str(warehouse_to_id)
                elif column.property_name == 'get_warehouse_to_name':
                    column_value = warehouse_to_object.name
                elif column.property_name == 'get_product_name':
                    column_value = pi.product.name
                elif column.property_name == 'get_product_description':
                    column_value = pi.product.description
                elif column.property_name == 'get_stock_available':
                    column_value = str(pi.closing_stock)
                elif column.property_name == 'get_stock_transfer_quantity':
                    column_value = '0'

                workbook.cell(row=row_number, column=column.column + 1).value = column_value
            row_number += 1

        return workbook, row_number

    class Meta:
        proxy = True

def save_transaction_details(items,user,organization):
    warehouses = {}
    for item in items:
        warehouse_tuple = (int(item['1']), int(item['3']))
        if not warehouses.get(warehouse_tuple):
            warehouses[warehouse_tuple] = [item]
        else:
            warehouses[warehouse_tuple] += [item]

    with transaction.atomic():
        for w_tuple,data_items in warehouses.items():

            custom_data = list()
            for data_row in data_items:
                if int(data_row['8']) > 0:
                    custom_data.append(data_row)

            if not custom_data:
                continue

            warehouse_from_id = w_tuple[0]
            warehouse_to_id = w_tuple[1]
            warehouse_stock_transfer = WarehouseStockTransfer()
            warehouse_stock_transfer.infrastructure_unit_id = warehouse_from_id
            warehouse_stock_transfer.infrastructure_unit_2_id = warehouse_to_id
            warehouse_stock_transfer.unique_id = str(uuid4())
            warehouse_stock_transfer.transaction_time = Clock.timestamp()
            warehouse_stock_transfer.sub_total = 0
            warehouse_stock_transfer.discount = 0
            warehouse_stock_transfer.total = warehouse_stock_transfer.sub_total - warehouse_stock_transfer.discount
            warehouse_stock_transfer.organization = organization
            warehouse_stock_transfer.created_by = user
            warehouse_stock_transfer.transaction_type = TransactionType.StockTransfer.value
            warehouse_stock_transfer.save()

            sub_total = 0
            discount = 0
            total = 0

            for data in custom_data:
                product_name = data['5']
                product_description = data['6']
                product_object = Product.objects.get(name=product_name,description=product_description)
                product_unit_price = product_object.prices.filter(price__name=ProductPriceEnum.gross_cust_price.value["name"]).first().value
                product_id = product_object.pk
                quantity = int(data['8'])
                warehouse_1_inventory = WarehouseInventory.objects.filter(assigned_to_id=warehouse_from_id, product_id=product_id).order_by('-last_updated').first()

                warehouse_2_inventory = WarehouseInventory.objects.filter(assigned_to_id=warehouse_to_id, product_id=product_id).order_by('-last_updated')
                if not warehouse_2_inventory.exists():
                    whouse_2_inventory = WarehouseInventory()
                    whouse_2_inventory.assigned_to_id = warehouse_to_id
                    whouse_2_inventory.product_id = product_id
                    whouse_2_inventory.opening_stock = 0
                    whouse_2_inventory.stock_change = quantity
                    whouse_2_inventory.closing_stock = whouse_2_inventory.stock_change
                    whouse_2_inventory.transaction = warehouse_stock_transfer
                    whouse_2_inventory.organization = organization
                    whouse_2_inventory.created_by = user
                    whouse_2_inventory.save()
                else:
                    whouse_2_inventory = WarehouseInventory()
                    whouse_2_inventory.assigned_to_id = warehouse_to_id
                    whouse_2_inventory.product_id = product_id
                    whouse_2_inventory.opening_stock = warehouse_2_inventory.first().closing_stock
                    whouse_2_inventory.stock_change = quantity
                    whouse_2_inventory.closing_stock = whouse_2_inventory.opening_stock + whouse_2_inventory.stock_change
                    whouse_2_inventory.transaction = warehouse_stock_transfer
                    whouse_2_inventory.organization = organization
                    whouse_2_inventory.created_by = user
                    whouse_2_inventory.save()

                whouse_1_inventory = WarehouseInventory()
                whouse_1_inventory.assigned_to_id = warehouse_from_id
                whouse_1_inventory.product_id = product_id
                whouse_1_inventory.opening_stock = warehouse_1_inventory.closing_stock
                whouse_1_inventory.stock_change = -quantity
                whouse_1_inventory.closing_stock = warehouse_1_inventory.closing_stock + whouse_1_inventory.stock_change
                whouse_1_inventory.transaction = warehouse_stock_transfer
                whouse_1_inventory.organization = organization
                whouse_1_inventory.created_by = user
                whouse_1_inventory.save()


                tx_breakdown = TransactionBreakDown()
                tx_breakdown.product_id = product_id
                tx_breakdown.quantity = quantity
                tx_breakdown.unit_price = product_unit_price
                tx_breakdown.sub_total = tx_breakdown.unit_price * tx_breakdown.quantity
                tx_breakdown.discount = 0
                tx_breakdown.total = tx_breakdown.sub_total - tx_breakdown.discount
                tx_breakdown.opening_stock = warehouse_1_inventory.closing_stock
                tx_breakdown.closing_stock = whouse_1_inventory.closing_stock
                tx_breakdown.organization = organization
                tx_breakdown.created_by = user
                tx_breakdown.save()

                sub_total += tx_breakdown.sub_total
                discount += tx_breakdown.discount
                total += tx_breakdown.total

                warehouse_stock_transfer.breakdown.add(tx_breakdown)

            warehouse_stock_transfer.sub_total = sub_total
            warehouse_stock_transfer.discount = discount
            warehouse_stock_transfer.total = total
            warehouse_stock_transfer.save()


@receiver(import_completed, sender=WarehouseStockTransfer)
def warehouse_stock_transfer_import_completed(sender, items=None, user=None, organization=None, **kwargs):
    lock = Lock(ctx=None)
    lock.acquire(True)
    process = Thread(target=save_transaction_details, args=(items,user,organization,))
    process.start()
    lock.release()