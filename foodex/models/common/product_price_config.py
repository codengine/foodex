from blackwidow.core.managers.modelmanager import DomainEntityModelManager
from blackwidow.engine.decorators.enable_bulk_edit import enable_bulk_edit
from blackwidow.engine.decorators.enable_export import enable_export
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context, loads_initial_data
from blackwidow.sales.models.common.product_price import ProductPriceEnum
from blackwidow.sales.models.common.product_price_config import ProductPriceConfig
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.clients.horeco import HoReCo
from foodex.models.clients.modern_trade import ModernTrade

__author__ = 'Sohel'


@decorate(expose_api('product-price-config'),
          enable_bulk_edit(row_pivots=[], column_pivots=[], fallback_default=True),
          enable_export, enable_import, save_audit_log, is_object_context, loads_initial_data,
          route(route='product-price-config', group='Products', module=ModuleEnum.Administration,
                display_name="Product Price"))
class FoodexProductPriceConfig(ProductPriceConfig):

    allowable_price_types = [
        ProductPriceEnum.net_cust_price.value['name'],
        ProductPriceEnum.vat_rate.value['name'],
        ProductPriceEnum.gross_cust_price.value['name']
    ]

    objects = DomainEntityModelManager(
        filter={"type": ProductPriceConfig.__name__, 'client__type__in': [ModernTrade.__name__, HoReCo.__name__], 'price_type__name__in': allowable_price_types })

    class Meta:
        proxy = True

    def save(self, *args, organization=None, **kwargs):
        self.type = ProductPriceConfig.__name__
        super().save(*args, organization=organization, **kwargs)

    @classmethod
    def get_manage_buttons(cls):
        return [ ViewActionEnum.Edit, ViewActionEnum.Delete, ViewActionEnum.AdvancedImport, ViewActionEnum.AdvancedExport]
