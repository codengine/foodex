from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.loading import get_model
from django.db.models.query_utils import Q
from django.utils.safestring import mark_safe
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin import GenericFormMixin
from blackwidow.core.models import ExporterConfig, ExporterColumnConfig
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.organizations.organization import Organization
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum
from blackwidow.engine.decorators.enable_export import enable_export
from blackwidow.engine.decorators.enable_import import enable_import
from django.db import transaction
from django.dispatch.dispatcher import receiver
from blackwidow.core.signals.signals import import_completed
from threading import Thread
from multiprocessing.synchronize import Lock
from foodex.models.invoice.open_invoice import OpenInvoice
from django.db.models import Sum
from decimal import Decimal
from foodex.models.products.approved_damage_products import ApprovedDamageProducts
from django import forms

__author__ = 'Sohel'

@decorate(save_audit_log, is_object_context, enable_export, enable_import,
          route(route='client-credit', module=ModuleEnum.Execute, group_order=3, item_order=11, display_name='Client credit', group='Credits & payments(clients)'))
class ClientCredit(OrganizationDomainEntity):
    client = models.ForeignKey(Client)

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Edit,ViewActionEnum.AdvancedExport, ViewActionEnum.AdvancedImport]

    @property
    def render_area(self):
        return self.client.assigned_to

    @property
    def render_credit_limit(self):
        return self.client.credit_limit

    @property
    def render_current_available_credit(self):
        return self.render_credit_limit - self.render_open_invoice_value + self.render_approved_damage

    @property
    def render_open_invoice_value(self):
        open_invoice_set = self.client.invoice_set.filter(type=OpenInvoice.__name__)
        if open_invoice_set.exists():
            price_total = self.client.invoice_set.filter(type=OpenInvoice.__name__).aggregate(Sum('price_total'))['price_total__sum']
            actual_amount_paid = self.client.invoice_set.filter(type=OpenInvoice.__name__).aggregate(Sum('actual_amount_paid'))['actual_amount_paid__sum']
            return price_total - actual_amount_paid
        else:
            return Decimal(0.0)

    @property
    def render_approved_damage(self):
        if ApprovedDamageProducts.objects.filter(client=self.client).exists():
            return ApprovedDamageProducts.objects.filter(client=self.client).aggregate(Sum('value'))['value__sum']
        else:
            return Decimal(0.0)

    @property
    def render_client(self):
        if self.client:
            return mark_safe("<a class='inline-link' href='" + reverse(self.client.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")
        else:
            return ""

    @classmethod
    def table_columns(cls):
        return 'code', 'render_client', 'render_area', 'render_credit_limit', 'render_current_available_credit', 'render_open_invoice_value', 'render_approved_damage', 'last_updated', 'created_by: Last Updated By'


    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'client', 'last_updated', 'date_created']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['credit_limit'] = self.render_credit_limit
        d['current_available_credit'] = self.render_current_available_credit
        d['open_invoice_value'] = self.render_open_invoice_value
        d['approved_damage'] = self.render_approved_damage
        d['last_updated_by'] = self.last_updated_by
        d['created_by'] = self.created_by

        return d


    @classmethod
    def get_export_dependant_fields(self):
        class AdvancedExportDependentForm(GenericFormMixin):
            def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
                super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
                Area = get_model('foodex','Area')
                queryset =Area.get_role_based_queryset()
                self.fields['infrastructure_unit'] = GenericModelChoiceField(label='Area', queryset=queryset, widget=forms.Select(attrs={'class': 'select2'}))
                self.fields['client'] = GenericModelChoiceField(label='Client', empty_label="Select Client", required=False, queryset=Client.get_role_based_queryset(), widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'infrastructure_unit', 'data-depends-property': 'assigned_to:id', 'data-url': reverse(get_model("core","Client").get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1"}))

        return AdvancedExportDependentForm


    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        if not organization:
            organization = Organization.objects.all().first()
        exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
        if not exporter_configs.exists():
            exporter_config = ExporterConfig(model=cls.__name__)
            exporter_config.organization = organization
            exporter_config.starting_row = 1
            exporter_config.save(**kwargs)
        else:
            for e in exporter_configs:
                e.delete()
            exporter_config = ExporterConfig(model=cls.__name__)
            exporter_config.organization = organization
            exporter_config.save(**kwargs)

        columns = [
            ExporterColumnConfig(column=0, column_name='Area Name', property_name='get_area_name', ignore=False),
            ExporterColumnConfig(column=1, column_name='Client Id', property_name='get_client_id', ignore=False),
            ExporterColumnConfig(column=2, column_name='Client Name', property_name='get_client_name', ignore=False),
            ExporterColumnConfig(column=3, column_name='Credit Limit', property_name='get_credit_limit', ignore=False),
        ]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)
        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):
        #
        # for column in columns:
        #     workbook.cell(row=row_number, column=column.column + 1).value = str(getattr(self, column.property_name))
        return self.pk, row_number + 1

    @classmethod
    def finalize_export(cls, workbook=None, row_number=None, query_set=None, **kwargs):
        return workbook

    @classmethod
    def initialize_export(cls, workbook=None, columns=None, row_number=None, query_set=None, **kwargs):

        for column in columns:
            workbook.cell(row=1, column=column.column + 1).value = column.column_name

        row_number += 1
        #####
        # all_records = ClientCredit.objects.all()
        # for record in all_records:
        #     for column in columns:
        #         column_value = str(getattr(record, column.property_name))
        #
        #         workbook.cell(row=row_number, column=column.column + 1).value = column_value
        #     row_number += 1
        #
        # return workbook, row_number
        #####
        query_params = kwargs.get('query_params')
        area_id = query_params.get('infrastructure_unit')
        client_id = query_params.get('client')

        try:
            area_id = int(area_id)
        except Exception:
            area_id = -1

        try:
            client_id = int(client_id)
        except Exception:
            client_id = -1

        clients = []

        if area_id > 0:
            clients = Client.objects.filter(assigned_to_id=area_id).exclude(type="Retailers")

            if client_id > 0:
                clients = clients.filter(pk=client_id)

        for client in clients:
            for column in columns:
                if column.property_name == 'get_area_name':
                    column_value = str(client.assigned_to.name)
                elif column.property_name == 'get_client_id':
                    column_value = str(client.id)
                elif column.property_name == 'get_client_name':
                    column_value = str(client.name)
                elif column.property_name == 'get_credit_limit':
                    column_value = str(client.credit_limit)

                workbook.cell(row=row_number, column=column.column + 1).value = column_value
            row_number += 1

        return workbook, row_number


    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
        from blackwidow.core.models.config.importer_config import ImporterConfig
        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if result or importer_config.columns.count() == 0:
            importer_config.save(**kwargs)
        else:
            for items in importer_config.columns.all():
                items.delete()
        columns = [
            ImporterColumnConfig(column=0, column_name='Area Name', property_name='get_area_name', ignore=False),
            ImporterColumnConfig(column=1, column_name='Client Id', property_name='get_client_id', ignore=False),
            ImporterColumnConfig(column=2, column_name='Client Name', property_name='get_client_name', ignore=False),
            ImporterColumnConfig(column=3, column_name='Credit Limit', property_name='get_credit_limit', ignore=False),
            ]

        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config


    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, request=None, **kwargs):

        return data


def save_client_credit_limit(items, user, organization):
    clients = {}
    for data in items:
        client_id = int(data['1'])
        if not client_id in clients.keys():
            clients[int(data['1'])] = [data]
        else:
            clients[int(data['1'])] += [data]

    with transaction.atomic():
        for dis_id, data in clients.items():
            credit_limit = float(data[0]['3'])

            clients = get_model("core", "Client").objects.filter(pk=int(dis_id))
            if clients.exists():
                client = clients.first()
                if client.credit_limit != credit_limit:
                    client.credit_limit = credit_limit
                    client.save()

                    # create or update client credit
                    client_credits = ClientCredit.objects.filter(client=client)
                    if client_credits.exists():
                        client_credit = client_credits.first()
                        client_credit.last_updated_by = user
                        client_credit.organization = organization
                        client_credit.save()
                    else:
                        client_credit = ClientCredit()
                        client_credit.client = client
                        client_credit.organization = organization
                        client_credit.created_by = user
                        client_credit.save()


@receiver(import_completed, sender=ClientCredit)
def client_credit_import_completed(sender, items=None, user=None, organization=None, **kwargs):
    lock = Lock(ctx=None)
    lock.acquire(True)
    process = Thread(target=save_client_credit_limit, args=(items,user,organization,))
    process.start()
    lock.release()