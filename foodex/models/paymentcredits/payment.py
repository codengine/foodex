from datetime import datetime
from decimal import Decimal
from django.core.urlresolvers import reverse
from django.db.models.loading import get_model
from django.db.models.query_utils import Q
from django.utils.safestring import mark_safe
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig
from blackwidow.core.models.config.exporter_config import ExporterConfig
from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
from blackwidow.core.models.config.importer_config import ImporterConfig
from blackwidow.core.models.log.logs import ErrorLog
from blackwidow.core.models.organizations.organization import Organization
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.engine.decorators.enable_export import enable_export
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.engine.extensions.model_descriptor import get_model_by_name
from blackwidow.sales.models.payments.payment_methods import Payment, PaymentMethod
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum
from django import forms
from django.db import transaction

__author__ = 'Sohel'


@decorate(save_audit_log, enable_export, enable_import, is_object_context,
          route(route='payments', module=ModuleEnum.Execute, group_order=3, item_order=9, display_name='Payment',
                group='Credits & payments(clients)'))
class Payment(Payment):
    @property
    def render_date_of_invoice(self):
        inv_date = datetime.fromtimestamp(self.invoice.date_of_invoice / 1000).strftime("%d/%m/%Y %H:%M %p")
        return inv_date

    @property
    def render_code(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.get_route_name(ViewActionEnum.Details),
                                                                   kwargs={'pk': self.pk}) + "' >" + str(
            self.code) + "</a>")


    @property
    def render_invoice(self):
        if self.invoice:
            model = get_model_by_name(self.invoice.type)
            return mark_safe("<a class='inline-link' href='" + reverse(model.get_route_name(ViewActionEnum.Details),
                                                                       kwargs={'pk': self.invoice.pk}) + "' >" + str(
                self.invoice) + "</a>")
        else:
            return ""

    @property
    def render_client(self):
        if self.client:
            model = get_model_by_name(self.client.type)
            return mark_safe(
                "<a class='inline-link' href='" + reverse(model.get_route_name(ViewActionEnum.Details),
                                                          kwargs={'pk': self.client.pk}) + "' >" + str(
                    self.client) + "</a>")
        else:
            return None


    @classmethod
    def get_button_title(cls, button=ViewActionEnum.Details):
        if button == ViewActionEnum.AdvancedExport:
            return "Export Open Invoice"
        elif button == ViewActionEnum.AdvancedImport:
            return "Batch Invoice Close"
        return button.value

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create, ViewActionEnum.AdvancedExport, ViewActionEnum.AdvancedImport]

    def details_link_config(self, **kwargs):
        return []

    @classmethod
    def table_columns(cls):
        return 'render_code', 'payment_method', 'render_invoice', 'render_date_of_invoice', 'total_amount', 'amount_paid', 'due_amount', 'apply_damage_amount:Damage Value', 'render_client', 'reference'

    class Meta:
        proxy = True

    @classmethod
    def get_export_dependant_fields(self):
        class AdvancedExportDependentForm(GenericFormMixin):
            def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
                super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
                self.fields['infrastructure_unit__1_id'] = GenericModelChoiceField(label='Area',
                                                                                   empty_label="Select Area",
                                                                                   required=False,
                                                                                   queryset=InfrastructureUnit.objects.filter(
                                                                                       Q(type="Area") | Q(
                                                                                           type='ModernTrade') | Q(
                                                                                           type='HoReCo')),
                                                                                   widget=forms.Select(
                                                                                       attrs={'class': 'select2'}))
                self.fields['client:id'] = GenericModelChoiceField(label='Client', empty_label="Select Client",
                                                                   required=False, queryset=Client.objects.all(),
                                                                   widget=forms.TextInput(
                                                                       attrs={'class': 'select2-input', 'width': '220',
                                                                              'data-depends-on': 'infrastructure_unit__1_id',
                                                                              'data-depends-property': 'assigned_to:id',
                                                                              'data-url': reverse(Client.get_route_name(
                                                                                  action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))

        return AdvancedExportDependentForm

    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        if not organization:
            organization = Organization.objects.all().first()
        exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
        if not exporter_configs.exists():
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.starting_row = 1
            exporter_config.save(**kwargs)
        else:
            exporter_config = exporter_configs.first()
            clmns = exporter_config.columns.all()
            exporter_config.columns.clear()

            for e in clmns:
                e.delete()

        columns = [
            ExporterColumnConfig(column=0, column_name='Invoice No', property_name='code', ignore=False),
            ExporterColumnConfig(column=1, column_name='Distributor', property_name='render_distributor', ignore=True),
            ExporterColumnConfig(column=2, column_name='Invoice Value', property_name='render_invoice_value',
                                 ignore=True),
            ExporterColumnConfig(column=3, column_name='Already Paid', property_name='render_already_paid',
                                 ignore=True),
            ExporterColumnConfig(column=4, column_name='Still To Pay', property_name='render_still_to_pay',
                                 ignore=True),
            ExporterColumnConfig(column=5, column_name='Last Updated', property_name='render_last_updated',
                                 ignore=True),
            ExporterColumnConfig(column=6, column_name='Payment', property_name='render_payment', ignore=False),
            ExporterColumnConfig(column=7, column_name='Payment Method', property_name='render_payment_method',
                                 ignore=False),
            ExporterColumnConfig(column=8, column_name='Reference', property_name='render_reference', ignore=False),
        ]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)
        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):
        # for column in columns:
        # workbook.cell(row=row_number, column=column.column + 1).value = str(getattr(self, column.property_name))
        return self.pk, row_number + 1

    @classmethod
    def finalize_export(cls, workbook=None, row_number=None, query_set=None, **kwargs):
        return workbook

    @classmethod
    def initialize_export(cls, workbook=None, columns=None, row_number=None, query_set=None, **kwargs):
        for column in columns:
            workbook.cell(row=1, column=column.column + 1).value = column.column_name

        row_number = row_number + 1

        OpenInvoice = get_model('foodex', 'OpenInvoice')

        open_invoices = OpenInvoice.objects.all()

        client_id = None
        try:
            if kwargs.get('query_params') and kwargs['query_params'].get('client:id'):
                client_id = int(kwargs['query_params']['client:id'])
        except:
            client_id = None
        if client_id:
            open_invoices = open_invoices.filter(counter_part__id=client_id)

        for open_invoice in open_invoices:
            for column in columns:
                column_value = ''
                if column.property_name == 'code':
                    column_value = str(getattr(open_invoice, "code"))
                elif column.property_name == 'render_distributor':
                    column_value = str(getattr(open_invoice, "render_distributor"))
                elif column.property_name == 'render_invoice_value':
                    column_value = str(getattr(open_invoice, "render_invoice_value"))
                elif column.property_name == 'render_already_paid':
                    column_value = str(getattr(open_invoice, "render_already_paid"))
                elif column.property_name == 'render_still_to_pay':
                    column_value = str(getattr(open_invoice, "render_still_to_pay"))
                elif column.property_name == 'render_last_updated':
                    column_value = str(getattr(open_invoice, "render_last_updated"))
                elif column.property_name == 'render_payment':
                    column_value = '0'

                workbook.cell(row=row_number, column=column.column + 1).value = column_value
            row_number += 1

        return workbook, row_number

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        if not organization:
            organization = Organization.objects.all().first()
        importer_configs = ImporterConfig.objects.filter(model=cls.__name__, organization=organization)
        if not importer_configs.exists():
            importer_config = ImporterConfig()
            importer_config.organization = organization
            importer_config.starting_row = 1
            importer_config.save(**kwargs)
        else:
            importer_config = importer_configs.first()
            clmns = importer_config.columns.all()
            importer_config.columns.clear()

            for e in clmns:
                e.delete()

        columns = [
            ImporterColumnConfig(column=0, column_name='Invoice No', property_name='code', ignore=False),
            ImporterColumnConfig(column=1, column_name='Distributor', property_name='render_distributor', ignore=True),
            ImporterColumnConfig(column=2, column_name='Invoice Value', property_name='render_invoice_value',
                                 ignore=True),
            ImporterColumnConfig(column=3, column_name='Already Paid', property_name='render_already_paid',
                                 ignore=True),
            ImporterColumnConfig(column=4, column_name='Still To Pay', property_name='render_still_to_pay',
                                 ignore=True),
            ImporterColumnConfig(column=5, column_name='Last Updated', property_name='render_last_updated',
                                 ignore=True),
            ImporterColumnConfig(column=6, column_name='Payment', property_name='render_payment', ignore=False),
            ImporterColumnConfig(column=7, column_name='Payment Method', property_name='render_payment_method',
                                 ignore=False),
            ImporterColumnConfig(column=8, column_name='Reference', property_name='render_reference', ignore=False),
        ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):

        if any([not data['0'], not data['6']]):
            ErrorLog.log('Not all data provided. Provided data: %s' % data, stacktrace='')
            return None

        OpenInvoice = get_model('foodex', 'OpenInvoice')
        if not OpenInvoice.objects.filter(code=data['0']).exists():
            ErrorLog.log('No open invoice found with invoice number: %s' % data['0'], stacktrace='')
            return None

        try:
            Decimal(data['6'])
        except:
            ErrorLog.log('Invalid payment amount given. Value given: %s required decimal.' % data['6'], stacktrace='')
            return None

        payment_method_name = 'Cash'
        if data['7']:
            payment_method_name = data['7']

        payment_methods = PaymentMethod.objects.filter(name=payment_method_name)
        if not payment_methods.exists():
            payment_method_options = [pm.name for pm in PaymentMethod.objects.all()].join(',')
            ErrorLog.log(
                "Provided payment method name doesn't exists in the system. Options are: %s" % payment_method_options,
                stacktrace='')
            return None

        organization = kwargs.get('organization') or Organization.objects.all().first()

        if Decimal(data['6']) > 0:
            with transaction.atomic():
                invoice = OpenInvoice.objects.filter(code=data['0']).first()

                payment_due = invoice.price_total - invoice.actual_amount_paid
                invoice.actual_amount_paid += Decimal(data['6'])

                client = invoice.counter_part
                client.available_credit += Decimal(data['6'])
                client.save()

                if invoice.price_total - invoice.actual_amount_paid <= 0:
                    closed_invoice = get_model("foodex", "ClosedInvoice")

                    invoice.type = closed_invoice.__name__
                    invoice.__class__ = closed_invoice
                invoice.save()

                ###Now save payment.
                payment_method = payment_methods.first()

                payment = Payment()
                payment.payment_method_id = payment_method.pk
                payment.total_amount = payment_due
                payment.amount_paid = Decimal(data['6'])
                payment.due_amount = payment.total_amount - payment.amount_paid
                payment.invoice_id = invoice.pk
                if data['8']:
                    payment.reference = data['8']
                else:
                    payment.reference = 'Payment through batch invoice close import.'
                payment.client_id = client.pk
                payment.organization_id = organization.pk
                payment.save()

        return data
