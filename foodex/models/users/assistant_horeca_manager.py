from crequest.middleware import CrequestMiddleware
from django.db.models.query_utils import Q
from rest_framework import serializers

from blackwidow.core.models.roles.role import Role
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.engine.decorators.enable_assignment import enable_assignment
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route, is_business_role
from blackwidow.engine.decorators.utility import decorate, is_role_context, save_audit_log, is_object_context
from blackwidow.sales.models.transactions.transaction import PrimaryTransaction
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum
from foodex.models import PrimarySalesReturn, ClientCredit, ClosedInvoice, OpenInvoice, DraftInvoice, \
    RejectedDamageProducts, ClearedDamage, ApprovedDamageProducts, DamageNSMApproval, PrimaryDamagedProducts, Payment
from foodex.models.clients.horeco import HoReCo
from foodex.models.infrastructure.area import Area
from foodex.models.infrastructure.region import Region
from foodex.models.inventory.client_inventory import ClientInventory
from foodex.models.orders.OrderNSMApproval import OrderNSMApproval
from foodex.models.orders.completed_order import CompletedOrder
from foodex.models.orders.order_rsm_mtm_hm import OrderRSMMTMHMApproval
from foodex.models.orders.pending_order import PendingOrder
from foodex.models.orders.rejected_order import RejectedOrder
from foodex.models.transactions.client_stock_count import ClientStockCount
from foodex.models.transactions.completed_sales_transaction import CompletedSalesTransaction
from foodex.models.transactions.delivery_transaction import DeliveryTransaction

__author__ = 'Sohel'


@decorate(is_role_context, is_business_role, save_audit_log, is_object_context,
          expose_api("assistant-horeca-managers"),
          enable_assignment(targets=['area'], role=['user']),
          route(route='assistant-horeca-mangers', group='Users', module=ModuleEnum.Administration,
                display_name="Assistant Horeca Manager"))
class AssistantHoReCaManager(ConsoleUser):
    class Meta:
        proxy = True

    @property
    def render_client(self):
        return self.infrastructureuserassignment_set.all().first().client_set.all().first() if self.infrastructureuserassignment_set.first() else None

    def get_areas(self):
        region = Region.objects.filter(pk=self.assigned_to.pk).values_list('pk', flat=True)
        return Area.objects.filter(parent__id__in=region).values_list('pk', flat=True)

    def filter_model(self, queryset=None, **kwargs):

        filters = []
        filters += [
            (Area, Q(**{'pk__in': self.get_areas()})),
            (Region, Q(**{'pk': self.assigned_to.pk})),
        ]

        filters += [
            (ClientInventory, Q(**{'assigned_to__assigned_to__parent__id': self.assigned_to.pk})),
            (ClientStockCount, Q(**{'client__assigned_to__parent__id': self.assigned_to.pk})),
            (PendingOrder, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (OrderNSMApproval, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (CompletedOrder, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (RejectedOrder, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (PrimaryTransaction, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (DeliveryTransaction, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (CompletedSalesTransaction, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (OrderRSMMTMHMApproval, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (PrimaryDamagedProducts, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (DamageNSMApproval, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (ApprovedDamageProducts, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (ClearedDamage, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (RejectedDamageProducts, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (DraftInvoice, Q(**{"counter_part__assigned_to__parent__id": self.assigned_to.pk})),
            (OpenInvoice, Q(**{"counter_part__assigned_to__parent__id": self.assigned_to.pk})),
            (ClosedInvoice, Q(**{"counter_part__assigned_to__parent__id": self.assigned_to.pk})),
            (Payment, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (ClientCredit, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (PrimarySalesReturn, Q(**{"invoice__counter_part__assigned_to__parent__id": self.assigned_to.pk})),
        ]
        for f in filters:
            if f[0] == queryset.model:
                return super().filter_model(queryset=queryset.filter(f[1]))
        return super().filter_model(queryset=queryset)

    @property
    def render_phone(self):
        return ','.join(obj.phone for obj in self.phones.all()) if self.phones.all() else ''

    @classmethod
    def table_columns(cls):
        return 'code', 'name', 'render_phone', 'last_updated:created_on', 'last_updated', 'last_updated_by'

    @property
    def render_email(self):
        return ','.join(obj.email for obj in self.emails.all()) if self.emails.all() else ''

    @property
    def details_config(self):
        d = super().details_config
        custom_list = ['code', 'name', 'user', 'assigned_to', 'date_created', 'last_updated']

        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['assigned_area'] = d.pop('assigned_to')
        d['login'] = d.pop('user')
        d['created_on'] = d.pop('date_created')
        d['created_by'] = self.created_by
        d['last_updated_by'] = self.last_updated_by
        d['location'] = self.addresses.all().first().location if self.addresses.all() else ''
        d['phone'] = self.render_phone
        d['email'] = self.render_email
        d['address'] = self.addresses.all().first if self.addresses.all() else ''
        d['image'] = self.image
        # d['contract_address'] = self.render_phone

        return d

    @classmethod
    def get_serializer(cls):
        ss = ConsoleUser.get_serializer()

        class Serializer(ss):
            role = serializers.PrimaryKeyRelatedField(required=False, queryset=Role.objects.get(name=cls.__name__))

            def create(self, attrs, instance=None):
                attrs.update({
                    "role": Role.objects.get(name=cls.__name__)
                })
                obj = super().create(attrs)
                obj.save()
                return obj

            class Meta(ss.Meta):
                model = cls

        return Serializer