from crequest.middleware import CrequestMiddleware
from rest_framework import serializers

from blackwidow.core.models.roles.role import Role
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.engine.decorators.enable_assignment import enable_assignment
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route, is_business_role
from blackwidow.engine.decorators.utility import decorate, is_role_context, save_audit_log, is_object_context
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'Sohel'

@decorate(is_role_context, is_business_role, save_audit_log, is_object_context,
          expose_api("national-sales-managers"),
          enable_assignment(targets=['area'], role=['user']),
          route(route='national-sales-mangers', group='Users', module=ModuleEnum.Administration, display_name="National Sales Manager"))
class NationalSalesManager(ConsoleUser):
    class Meta:
        proxy = True

    def filter_model(self, queryset=None, **kwargs):
        # from jita.models.infrastructure.territory import Territory
        # from jita.models.users.service_persons import ServicePerson
        # from blackwidow.core.models.clients.client_assignment import ClientAssignment

        filters = [
            # (Aparajita, Q(**{'assigned_to__parent__id': self.assigned_to.pk})),
            # # (Hub, Q(**{'parent__id': self.assigned_to.pk}) | Q(**{'assigned_to__id': self.pk})),
            # (Hub, Q(**{'assigned_to__id': self.assigned_to.pk})),
            # (Transaction, Q(**{'infrastructure_unit__parent__id': self.assigned_to.pk})),
            # (HubManager, Q(**{'assigned_to': self.assigned_to.pk})),
            # (Territory, Q(**{'id': self.assigned_to.pk})),
            # (Shop, Q(**{'assigned_to__parent__id': self.assigned_to.pk})),
            # (ProductInventory, Q(**{'assigned_to__parent__id': self.assigned_to.pk})),
            # (ServicePerson, Q(**{'assigned_to__parent__id': self.assigned_to.pk})),
            # # (SaleTargetPerformanceIndex, Q(**{'user__assigned_to__parent__id': self.assigned_to.pk})),
            # (PerformanceIndex, Q(**{'user__assigned_to__parent__id': self.assigned_to.pk})),
            # (TerritoryManager, Q(**{'assigned_to__parent__id': self.assigned_to.pk})),
            #
            # (PreRegisteredAparajita, Q(**{'assigned_to__parent__id': self.assigned_to.pk})),
            # (RegisteredAparajita, Q(**{'assigned_to__parent__id': self.assigned_to.pk})),
            # (SaleTransaction, Q(**{'infrastructure_unit__parent__id': self.assigned_to.pk})),
            # (LiftTransaction, Q(**{'infrastructure_unit__parent__id': self.assigned_to.pk})),
            # (ReverseBuyTransaction, Q(**{'infrastructure_unit__parent__id': self.assigned_to.pk})),
            # (ReverseSaleTransaction, Q(**{'infrastructure_unit__parent__id': self.assigned_to.pk})),
            # (StockAdjustmentTransaction, Q(**{'infrastructure_unit__parent__id': self.assigned_to.pk}))
        ]
        for f in filters:
            if f[0] == queryset.model:
                return super().filter_model(queryset=queryset.filter(f[1]))
        return super().filter_model(queryset=queryset)

    @property
    def render_phone(self):
        return ','.join(obj.phone for obj in self.phones.all()) if self.phones.all() else ''

    @classmethod
    def table_columns(cls):
        return 'code', 'name', 'render_phone', 'last_updated:created_on','last_updated','last_updated_by'

    @property
    def render_email(self):
        return ','.join(obj.email for obj in self.emails.all()) if self.emails.all() else ''

    @property
    def details_config(self):
        d = super().details_config
        custom_list = ['code', 'name', 'user', 'date_created', 'last_updated']

        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['login'] = d.pop('user')
        d['created_on'] = d.pop('date_created')
        d['created_by'] = self.created_by
        d['last_updated_by'] = self.last_updated_by
        d['location'] = self.addresses.all().first().location if self.addresses.all() else ''
        d['phone'] = self.render_phone
        d['email'] = self.render_email
        d['address'] = self.addresses.all().first if self.addresses.all() else ''
        d['image'] = self.image
        #d['contract_address'] = self.render_phone

        return d

    @classmethod
    def get_serializer(cls):
        ss = ConsoleUser.get_serializer()

        class Serializer(ss):
            role = serializers.PrimaryKeyRelatedField(required=False, queryset=Role.objects.get(name=cls.__name__))

            def create(self, attrs, instance=None):
                attrs.update({
                    "role": Role.objects.get(name=cls.__name__)
                })
                obj = super().create(attrs)
                obj.save()
                return obj

            class Meta(ss.Meta):
                model = cls

        return Serializer