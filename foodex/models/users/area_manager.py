from rest_framework import serializers

from blackwidow.core.models.roles.role import Role
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.engine.decorators.enable_assignment import enable_assignment
from blackwidow.engine.decorators.enable_kpi import enable_kpi
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route, is_business_role
from blackwidow.engine.decorators.utility import decorate, is_role_context, save_audit_log, is_object_context
from config.enums.modules_enum import ModuleEnum
from foodex.models.clients.distributor import Distributor
from foodex.models.users.filters.area_manager import get_filters
from foodex.models.users.service_persons import ServicePerson

__author__ = 'Mahmud'

@decorate(is_role_context, is_business_role, enable_kpi, save_audit_log, is_object_context,
          expose_api("area-managers"),
          enable_assignment(targets=['area'], role=['user']),
          route(route='area-mangers', group='Users', module=ModuleEnum.Administration, display_name="Area Sales Manager"))
class AreaSalesManager(ConsoleUser):
    class Meta:
        proxy = True

    def get_all_distributor_under_same_area(self,flat=False):
        distributors = Distributor.objects.filter(assigned_to_id=self.assigned_to.pk)
        if not flat:
            return distributors
        else:
            return distributors.values_list('id', flat=True)

    def get_all_service_persons_under(self,flat=False):
        all_distributors = self.get_all_distributor_under_same_area()
        service_persons = []
        for d in all_distributors:
            iua_list = d.assigned_employee.all()
            for iua in iua_list:
                if not flat:
                    service_persons += list(iua.users.all())
                else:
                    service_persons += iua.users.all().values_list("id",flat=True)
        return service_persons

    def get_all_assigned_units_under(self,flat=False):
        all_service_persons = self.get_all_service_persons_under()
        all_assigned_units = []
        for sp in all_service_persons:
            if not flat:
                all_assigned_units += list(sp.assigned_units.all())
            else:
                all_assigned_units += sp.assigned_units.all().values_list("id",flat=True)
        return all_assigned_units

    def get_service_person_role(self):
        return ServicePerson.objects.all().first().role

    def get_all_retailers_under(self,flat=True, pending=False):
        all_sp = self.get_all_service_persons_under()
        retailers = []
        for sp in all_sp:
            sp_list = ServicePerson.objects.filter(pk=sp.pk)
            if sp_list.exists():
                try:
                    if pending:
                        retailers += sp_list.first().get_pending_retailers()
                    else:
                        retailers += sp_list.first().get_retailers()
                except:
                    pass
        return retailers

    def get_all_service_persons_under_id(self):
        service_persons = self.get_all_service_persons_under(flat=True)
        id_list = []
        for person in service_persons:
            id_list.append(person.pk)
        return id_list

    def filter_model(self, queryset=None, **kwargs):
        filters = get_filters(model_object=self)
        for f in filters:
            if f[0] == queryset.model:
                return super().filter_model(queryset=queryset.filter(f[1]))
        return super().filter_model(queryset=queryset)

    @property
    def render_phone(self):
        return ','.join(obj.phone for obj in self.phones.all()) if self.phones.all() else ''

    @classmethod
    def table_columns(cls):
        return 'code', 'name', 'assigned_to:assigned_area', 'render_phone', 'date_created:created_on','last_updated'

    @property
    def render_email(self):
        return ','.join(obj.email for obj in self.emails.all()) if self.emails.all() else ''

    @property
    def details_config(self):
        d = super().details_config
        custom_list = ['code', 'name', 'user', 'assigned_to', 'date_created', 'last_updated']

        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['assigned_area'] = d.pop('assigned_to')
        d['login'] = d.pop('user')
        d['created_on'] = d.pop('date_created')
        d['created_by'] = self.created_by
        d['last_updated_by'] = self.last_updated_by
        d['location'] = self.addresses.all().first().location if self.addresses.all() else ''
        d['phone'] = self.render_phone
        d['email'] = self.render_email
        d['address'] = self.addresses.all().first if self.addresses.all() else ''
        d['image'] = self.image

        return d

    @classmethod
    def get_serializer(cls):
        ss = ConsoleUser.get_serializer()

        class Serializer(ss):
            role = serializers.PrimaryKeyRelatedField(required=False, queryset=Role.objects.get(name=cls.__name__))

            def create(self, attrs, instance=None):
                attrs.update({
                    "role": Role.objects.get(name=cls.__name__)
                })
                obj = super().create(attrs)
                obj.save()
                return obj

            class Meta(ss.Meta):
                model = cls

        return Serializer