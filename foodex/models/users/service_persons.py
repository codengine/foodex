from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import transaction
from crequest.middleware import CrequestMiddleware
from django.db.models.loading import get_model
from django.db.models.query_utils import Q
from django.utils.safestring import mark_safe
from rest_framework import serializers
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.clientvisit.client_visit import VisitClient

from blackwidow.core.models.common.qr_code import QRCode
from blackwidow.core.viewmodels.tabs_config import TabView, TabViewAction
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.enable_kpi import enable_kpi
from blackwidow.engine.exceptions.exceptions import BWException
from blackwidow.core.models.roles.role import Role
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.engine.decorators.enable_assignment import enable_assignment
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route, is_business_role, partial_route
from blackwidow.engine.decorators.utility import decorate, is_role_context, save_audit_log, is_object_context
from blackwidow.engine.extensions.model_descriptor import get_model_by_name
from blackwidow.sales.decorators.model_decorators import enable_individual_assignment
from blackwidow.sales.models.kpi.sale_target import SaleTargetPerformanceIndex
from blackwidow.sales.models.orders.secondary_completed_order import SecondaryCompletedOrder
from blackwidow.sales.models.orders.secondary_delivered_order import SecondaryDeliveredOrder
from blackwidow.sales.models.orders.secondary_pending_order import SecondaryPendingOrder
from blackwidow.sales.models.transactions.transaction import SecondaryTransaction, PrimaryTransaction
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.transactions.client_stock_count import ClientStockCount
from foodex.models.clients.distributor import Distributor
from foodex.models.clients.retailers import Retailers
from foodex.models.infrastructure.area import Area
from foodex.models.infrastructure.region import Region
from foodex.models.infrastructure.routes import Route
from foodex.models.inventory.client_inventory import ClientInventory
from foodex.models.orders.completed_order import CompletedOrder
from foodex.models.orders.pending_order import PendingOrder
from foodex.models.transactions.delivery_transaction import DeliveryTransaction

__author__ = 'Mahmud'


@decorate(is_role_context, enable_kpi, is_business_role, save_audit_log, is_object_context,
          expose_api('service-persons'), enable_import,
          enable_assignment(targets=['hub'], role=['user']), expose_api('service-persons'),
          route(route='service-persons', group='Users', module=ModuleEnum.Administration, display_name="Sales Officer"),
          partial_route(relation=['normal'], models=[Route]))
class ServicePerson(ConsoleUser):
    class Meta:
        proxy = True

    def get_choice_name(self):
        return self.code + ": "+self.name

    def get_retailers(self):
        result = []
        for route in self.assigned_units.all():
            assigned_clients = route.assigned_clients.all()
            for assigned_client in assigned_clients:
                clients = assigned_client.clients
                if clients.exists():
                    result += clients.values_list('id', flat=True)
        return result

    def get_pending_retailers(self):
        result = []
        for route in self.assigned_units.all():
            assigned_clients = route.assigned_clients.all()
            for assigned_client in assigned_clients:
                clients = assigned_client.clients
                if clients.exists():
                    result += clients.filter(is_approved=False).values_list('id', flat=True)
        return result

    def filter_model(self, queryset=None, **kwargs):
        from blackwidow.core.models.structure.user_assignment import InfrastructureUserAssignment
        from blackwidow.sales.models.common.product_price_config import ProductPriceConfig
        # from jita.models.users.hub_managers import HubManager
        # distributors = [x.clients.values_list('pk') for x in self.assigned_to.assigned_clients.all() if x.client_type == Distributor.__name__][0]

        # userassignment = InfrastructureUserAssignment.objects.filter(role=self.role)

        OrderNSMApproval = get_model("foodex","OrderNSMApproval")
        RejectedOrder = get_model("foodex","RejectedOrder")

        try:
            user_assignment = self.infrastructureuserassignment_set.all().first()
            distributor = user_assignment.client_set.all().first()
            route_set = distributor.infrastructureunit_set.all()
        except Exception:
            route_set = Route.objects.none()

        filters = [
            # (Route, Q(**{'pk__in': self.assigned_units.values_list('id', flat=True)})),
            (Route, Q(**{'pk__in': route_set.values_list('id', flat=True)})),
            (ServicePerson, Q(**{'pk': self.pk})),
            (get_model_by_name('AreaSalesManager'), Q(**{'pk': self.pk})),      # cannot see unless himself
            (get_model_by_name('RegionalSalesManager'), Q(**{'pk': self.pk})),  # cannot see unless himself
            (Area, Q(**{'pk': self.get_distributor.assigned_to.pk if self.get_distributor is not None else -1})),
            (Region, Q(**{'pk': self.get_distributor.assigned_to.parent.pk if self.get_distributor is not None else -1})),
            (ProductPriceConfig, Q(**{'client_type': Distributor.__name__,'client__assigned_employee__role__id': self.role.pk, 'client__assigned_employee__users__id__in':[self.pk]})),
            (Distributor, Q(**{'assigned_employee__role__id': self.role.pk, 'assigned_employee__users__id__in':[self.pk]})),
            (DeliveryTransaction, Q(**{'client__assigned_employee__role__id': self.role.pk, 'client__assigned_employee__users__id__in':[self.pk]})),
            (Retailers, Q(**{'pk__in': self.get_retailers()})),
            (get_model_by_name("PendingRetailerAccount"), Q(**{'pk__in': self.get_pending_retailers()})),
            (SecondaryTransaction,Q(**{"client_id": self.get_distributor.pk if self.get_distributor else -1,"client_2__isnull":False})),
            (SecondaryPendingOrder,Q(**{"from_client_id": self.get_distributor.pk if self.get_distributor else -1})),
            (SecondaryCompletedOrder,Q(**{"from_client_id": self.get_distributor.pk if self.get_distributor else -1})),
            (SecondaryDeliveredOrder,Q(**{"from_client_id": self.get_distributor.pk if self.get_distributor else -1})),
            (VisitClient, Q(**{"client_id__in": self.get_retailers()})),
            (ClientInventory, Q(**{'assigned_to__assigned_employee__role__id': self.role.pk, 'assigned_to__assigned_employee__users__id__in':[self.pk]})),
            (ClientStockCount, Q(**{'client__assigned_employee__role__id': self.role.pk, 'client__assigned_employee__users__id__in':[self.pk]})),
            (PendingOrder,Q(**{"client__type": Distributor.__name__,'client__assigned_employee__role__id': self.role.pk, 'client__assigned_employee__users__id__in':[self.pk]})),
            (OrderNSMApproval,Q(**{"client__type": Distributor.__name__, 'client__assigned_employee__role__id': self.role.pk, 'client__assigned_employee__users__id__in':[self.pk]})),
            (CompletedOrder,Q(**{"client__type": Distributor.__name__, 'client__assigned_employee__role__id': self.role.pk, 'client__assigned_employee__users__id__in':[self.pk]})),
            (RejectedOrder,Q(**{"client__type": Distributor.__name__, 'client__assigned_employee__role__id': self.role.pk, 'client__assigned_employee__users__id__in':[self.pk]})),
            (PrimaryTransaction,Q(**{"client__type": Distributor.__name__, 'client__assigned_employee__role__id': self.role.pk, 'client__assigned_employee__users__id__in':[self.pk], "client_2__isnull": True})),
            ]
        for f in filters:
            if f[0] == queryset.model:
                return super().filter_model(queryset=queryset.filter(f[1]))
        return super().filter_model(queryset=queryset)

    @classmethod
    def get_serializer(cls):
        ss = ConsoleUser.get_serializer()

        class Serializer(ss):
            role = serializers.PrimaryKeyRelatedField(required=False, queryset=Role.objects.get(name=cls.__name__))
            assigned_to = serializers.SerializerMethodField('get_distributor')

            def get_distributor(self,obj):
                distributor = ''
                try:
                    distributor_set = Distributor.objects.filter(assigned_employee__in=[obj])
                    if distributor_set:
                        distributor = distributor_set.first().name
                except:
                    pass
                return distributor


            def create(self, attrs, instance=None):
                attrs.update({
                    "role": Role.objects.get(name=cls.__name__)
                })
                obj = super().create(attrs)
                obj.save()
                return obj

            class Meta(ss.Meta):
                model = cls

        return Serializer

    @property
    def render_full_name(self):
        if self.user.get_full_name():
            return self.user.get_full_name()
        return self.name

    @property
    def render_phone(self):
        return self.phones.all().first().phone if self.phones else ""

    @property
    def render_distributor(self):
        user_assignment = self.infrastructureuserassignment_set.all()
        if user_assignment.exists():
            distributor = user_assignment.first().client_set.all().first()
            model = get_model_by_name(distributor.type)
            return mark_safe("<a class='inline-link' href='" + reverse(model.get_route_name(ViewActionEnum.Details),
                                                                       kwargs={'pk': distributor.pk}) + "' >" + str(
                distributor) + "</a>")
        else:
            return None

    @property
    def get_distributor(self):
        user_assignment = self.infrastructureuserassignment_set.all()
        if user_assignment.exists():
            return user_assignment.first().client_set.all().first()
        else:
            return None

    @property
    def render_email(self):
        return self.emails.all().first().email if self.emails.first() else ""

    @classmethod
    def filter_query(cls,query_set,custom_search_fields=[]):
        from django.db.models import Q
        from functools import reduce
        import operator
        for key, value in custom_search_fields:
            if key.startswith("__search__distributors"):
                try:
                    or_list = []
                    values = value.split(",")
                    for val in values:
                        or_list.append(Q(name__icontains=val))
                    service_persons_pk = []
                    distributors = Distributor.objects.filter(reduce(operator.or_, or_list))
                    for dist in distributors:
                        service_persons = dist.assigned_employee.filter(role__name=ServicePerson.__name__)
                        if service_persons.exists():
                            service_persons_pk += service_persons.first().users.values_list('pk', flat=True)
                    query_set = query_set.filter(pk__in=service_persons_pk)
                except:
                    pass
            elif key.startswith("__search__text__phone"):
                try:
                    or_list = []
                    values = value.split(",")
                    for val in values:
                        or_list.append(Q(phones__phone__icontains=val))
                    service_persons = ServicePerson.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=service_persons.values_list('pk', flat=True))
                except:
                    pass
        return query_set

    @classmethod
    def get_url_by_name(cls, name):
        if name == 'render_distributor':
            return '__search__distributors'
        elif name == 'render_phone':
            return '__search__text__phone'
        return None

    @classmethod
    def table_columns(cls):
        return "code", "name", 'render_distributor', "render_phone", "created_by", "date_created:Created On", "last_updated"


    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'national_id', 'date_of_birth', 'address 1', 'image', 'education', 'date_created', 'last_updated']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['address'] = d.pop('address 1')
        d['location'] = self.addresses.first().location
        d['full_name'] = self.render_full_name
        d['distributor'] = self.render_distributor
        d['login'] = self.user.username
        d[''] = self.render_phone
        d['email'] = self.render_email
        d['created_by'] = self.created_by
        d['last_updated_by'] = self.last_updated_by

        return d

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        with transaction.atomic():
            name = data['3'].strip() + ' ' + data['4'].strip()
            if ServicePerson.objects.filter(name=name).exists():
                hm = ServicePerson.objects.filter(name=name).first()
            else:
                hm = ServicePerson()
                hm.name = name
                hm.organization = user.organization
                hm.role = Role.objects.get(name=ServicePerson.__name__)

                auser = User() if hm.user is None else hm.user
                auser.username = data['2'] if data['2'] is not None else name
                auser.password = data['1'] if data['1'] is not None else "000000"
                auser.save()

                hub = Route.objects.get(name=data['0'])

                qr_code = QRCode.objects.filter(key=data['7'])
                if qr_code.exists():
                    qr_code = qr_code.first()
                    if qr_code.is_used:
                        raise BWException('Duplicate QR Code')
                    hm.qr_code = qr_code
                elif data['7'] is not None:
                    qr_code = QRCode()
                    qr_code.key = data['7']
                    qr_code.value = data['7']
                    qr_code.organization = user.organization
                    qr_code.save()
                    hm.qr_code = qr_code

                hm.user = auser
                hm.assigned_to = hub
                hm.save()

            return hm.pk

    @property
    def tabs_config(self):
        tabs = []

        clients = Client.objects.filter(
            assigned_employee__role__id=self.role.pk,
            assigned_employee__users__id__in=[self.pk]).values_list('id', flat=True)

        tabs.append(TabView(
            title='Routes (s)',
            access_key='assigned_units',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            property=self.assigned_units,
            add_more_queryset=Route.objects.filter(parent_client__in=clients).exclude(pk__in=self.assigned_units.values_list('id', flat=True)),
            related_model=Route,
            actions=[
                TabViewAction(
                    title='Add',
                    action='add',
                    icon='icon-plus',
                    route_name=Route.get_route_name(action=ViewActionEnum.PartialBulkAdd, parent=self.__class__.__name__.lower()),
                    css_class='manage-action load-modal fis-plus-ico',
                    enable_wide_popup=True
                ),
                TabViewAction(
                    title='Remove',
                    action='partial-remove',
                    icon='icon-remove',
                    route_name=Route.get_route_name(action=ViewActionEnum.PartialBulkRemove, parent=self.__class__.__name__.lower()),
                    css_class='manage-action delete-item fis-remove-ico'
                )
            ]))
        return tabs

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
        from blackwidow.core.models.config.importer_config import ImporterConfig

        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if result or importer_config.columns.count() == 0:
            importer_config.save(**kwargs)
        else:
            for items in importer_config.columns.all():
                items.delete()
        columns = [
            ImporterColumnConfig(column=3, column_name='first_name', property_name='first_name', ignore=False),
            ImporterColumnConfig(column=4, column_name='last_name', property_name='last_name', ignore=False),
            ImporterColumnConfig(column=6, column_name='qr_code_id', property_name='qr_code_id', ignore=False),
            ImporterColumnConfig(column=1, column_name='password', property_name='password', ignore=False),
            ImporterColumnConfig(column=2, column_name='username', property_name='username', ignore=False),
            ImporterColumnConfig(column=5, column_name='reference_key', property_name='reference_key', ignore=False),
            ImporterColumnConfig(column=0, column_name='hub', property_name='hub', ignore=False),
            ImporterColumnConfig(column=7, column_name='qr_code', property_name='key', ignore=False),
            ImporterColumnConfig(column=8, column_name='qr_code', property_name='value', ignore=False)
        ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config

    # def save(self, *args, **kwargs):
    #     super(ServicePerson, self).save(*args, **kwargs)
    #     with transaction.atomic():
    #         if self.pk is None:
    #             _kpi, result = SaleTargetPerformanceIndex.objects.get_or_create(user=self, )