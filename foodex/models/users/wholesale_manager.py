from crequest.middleware import CrequestMiddleware
from django.db.models.query_utils import Q
from rest_framework import serializers

from blackwidow.core.models.roles.role import Role
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.engine.decorators.enable_assignment import enable_assignment
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route, is_business_role
from blackwidow.engine.decorators.utility import decorate, is_role_context, save_audit_log, is_object_context
from blackwidow.sales.models.inventory.client_inventory import ClientInventory
from blackwidow.sales.models.transactions.transaction import PrimaryTransaction
from config.enums.modules_enum import ModuleEnum
from foodex.models.transactions.client_stock_count import ClientStockCount
from foodex.models.clients.wholesale import WholeSaler
from foodex.models.orders.OrderNSMApproval import OrderNSMApproval
from foodex.models.orders.completed_order import CompletedOrder
from foodex.models.orders.pending_order import PendingOrder
from foodex.models.orders.rejected_order import RejectedOrder
from foodex.models.transactions.delivery_transaction import DeliveryTransaction

__author__ = 'Sohel'

@decorate(is_role_context, is_business_role, save_audit_log, is_object_context,
          expose_api("wholesale-managers"),
          enable_assignment(targets=['area'], role=['user']),
          route(route='wholesale-managers', group='Users', module=ModuleEnum.Administration, display_name="Wholesale Manager"))
class WholesaleManager(ConsoleUser):
    class Meta:
        proxy = True

    def filter_model(self, queryset=None, **kwargs):
        # from jita.models.infrastructure.territory import Territory
        # from jita.models.users.service_persons import ServicePerson
        # from blackwidow.core.models.clients.client_assignment import ClientAssignment

        filters = [
            (ClientInventory, Q(**{'assigned_to__type': WholeSaler.__name__})),
            (ClientStockCount, Q(**{'client__type': WholeSaler.__name__})),
            (PendingOrder,Q(**{"client__type": WholeSaler.__name__})),
            (OrderNSMApproval,Q(**{"client__type": WholeSaler.__name__})),
            (CompletedOrder,Q(**{"client__type": WholeSaler.__name__})),
            (RejectedOrder,Q(**{"client__type": WholeSaler.__name__})),
            (PrimaryTransaction,Q(**{"client__type": WholeSaler.__name__})),
            (DeliveryTransaction, Q(**{"client__type": WholeSaler.__name__})),
        ]
        for f in filters:
            if f[0] == queryset.model:
                return super().filter_model(queryset=queryset.filter(f[1]))
        return super().filter_model(queryset=queryset)

    @property
    def render_phone(self):
        return ','.join(obj.phone for obj in self.phones.all()) if self.phones.all() else ''

    @classmethod
    def table_columns(cls):
        return 'code', 'name', 'assigned_to:assigned_area', 'render_phone', 'last_updated:created_on','last_updated','last_updated_by'

    @property
    def render_email(self):
        return ','.join(obj.email for obj in self.emails.all()) if self.emails.all() else ''

    @property
    def details_config(self):
        d = super().details_config
        custom_list = ['code', 'name', 'user', 'assigned_to', 'date_created', 'last_updated']

        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['assigned_area'] = d.pop('assigned_to')
        d['login'] = d.pop('user')
        d['created_on'] = d.pop('date_created')
        d['created_by'] = self.created_by
        d['last_updated_by'] = self.last_updated_by
        d['location'] = self.addresses.all().first().location if self.addresses.all() else ''
        d['phone'] = self.render_phone
        d['email'] = self.render_email
        d['address'] = self.addresses.all().first if self.addresses.all() else ''
        d['image'] = self.image
        #d['contract_address'] = self.render_phone

        return d

    @classmethod
    def get_serializer(cls):
        ss = ConsoleUser.get_serializer()

        class Serializer(ss):
            role = serializers.PrimaryKeyRelatedField(required=False, queryset=Role.objects.get(name=cls.__name__))

            def create(self, attrs, instance=None):
                attrs.update({
                    "role": Role.objects.get(name=cls.__name__)
                })
                obj = super().create(attrs)
                obj.save()
                return obj

            class Meta(ss.Meta):
                model = cls

        return Serializer