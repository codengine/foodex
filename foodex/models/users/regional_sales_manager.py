from crequest.middleware import CrequestMiddleware
from django.db.models.query_utils import Q
from rest_framework import serializers
from blackwidow.core.models.clientvisit.client_visit import VisitClient

from blackwidow.core.models.roles.role import Role
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.engine.decorators.enable_assignment import enable_assignment
from blackwidow.engine.decorators.enable_kpi import enable_kpi
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route, is_business_role
from blackwidow.engine.decorators.utility import decorate, is_role_context, save_audit_log, is_object_context
from blackwidow.sales.models.common.product_price_config import ProductPriceConfig
from blackwidow.sales.models.orders.secondary_completed_order import SecondaryCompletedOrder
from blackwidow.sales.models.orders.secondary_delivered_order import SecondaryDeliveredOrder
from blackwidow.sales.models.orders.secondary_pending_order import SecondaryPendingOrder
from blackwidow.sales.models.transactions.transaction import SecondaryTransaction, PrimaryTransaction
from config.enums.modules_enum import ModuleEnum
from foodex.models import PrimarySalesReturn, ClientCredit, ClosedInvoice, OpenInvoice, DraftInvoice, \
    RejectedDamageProducts, ClearedDamage, ApprovedDamageProducts, DamageNSMApproval, PrimaryDamagedProducts, Payment, \
    CompletedSalesTransaction
from foodex.models.clients.distributor import Distributor
from foodex.models.clients.pending_sec_retail import PendingRetailerAccount
from foodex.models.clients.retailers import Retailers
from foodex.models.infrastructure.area import Area
from foodex.models.infrastructure.region import Region
from foodex.models.infrastructure.routes import Route
from foodex.models.inventory.client_inventory import ClientInventory
from foodex.models.orders.OrderNSMApproval import OrderNSMApproval
from foodex.models.orders.completed_order import CompletedOrder
from foodex.models.orders.order_rsm_mtm_hm import OrderRSMMTMHMApproval
from foodex.models.orders.pending_order import PendingOrder
from foodex.models.orders.rejected_order import RejectedOrder
from foodex.models.transactions.delivery_transaction import DeliveryTransaction
from foodex.models.users.area_manager import AreaSalesManager
from foodex.models.users.service_persons import ServicePerson

__author__ = 'Sohel'

@decorate(is_role_context, is_business_role, enable_kpi, save_audit_log, is_object_context,
          expose_api("regional-sales-managers"),
          enable_assignment(targets=['area'], role=['user']),
          route(route='regional-sales-mangers', group='Users', module=ModuleEnum.Administration, display_name="Regional Sales Manager"))
class RegionalSalesManager(ConsoleUser):
    class Meta:
        proxy = True

    def get_all_areas_under(self,flat=False):
        all_areas = Area.objects.filter(parent_id=self.assigned_to.pk)
        if not flat:
            return all_areas
        else:
            return all_areas.values_list("id",flat=True)

    def get_all_distributor_under_same_region(self,flat=False):
        distributors = Distributor.objects.filter(assigned_to_id__in=self.get_all_areas_under(flat=True))
        if not flat:
            return distributors
        else:
            return distributors.values_list('id', flat=True)

    def get_all_service_persons_under(self,flat=False):
        all_distributors = self.get_all_distributor_under_same_region()
        service_persons = []
        for d in all_distributors:
            iua_list = d.assigned_employee.all()
            for iua in iua_list:
                if not flat:
                    service_persons += list(iua.users.all())
                else:
                    service_persons += iua.users.all().values_list("id",flat=True)
        return service_persons

    def get_all_assigned_units_under(self,flat=False):
        all_service_persons = self.get_all_service_persons_under()
        all_assigned_units = []
        for sp in all_service_persons:
            if not flat:
                all_assigned_units += list(sp.assigned_units.all())
            else:
                all_assigned_units += sp.assigned_units.all().values_list("id",flat=True)
        return all_assigned_units

    def get_service_person_role(self):
        return ServicePerson.objects.all().first().role

    def get_all_retailers_under(self,flat=True, pending=False):
        all_sp = self.get_all_service_persons_under()
        retailers = []
        for sp in all_sp:
            sp_list = ServicePerson.objects.filter(pk=sp.pk)
            if sp_list.exists():
                try:
                    if pending:
                        retailers += sp_list.first().get_pending_retailers()
                    else:
                        retailers += sp_list.first().get_retailers()
                except:
                    pass
        return retailers

    def filter_model(self, queryset=None, **kwargs):
        # from jita.models.infrastructure.territory import Territory
        # from jita.models.users.service_persons import ServicePerson
        # from blackwidow.core.models.clients.client_assignment import ClientAssignment

        filters = [
            (Route, Q(**{'pk__in': self.get_all_assigned_units_under(flat=True)})),
            (RegionalSalesManager, Q(**{'pk': self.pk})),
            (AreaSalesManager, Q(**{'assigned_to__parent__pk': self.assigned_to.pk})),
            (ProductPriceConfig, Q(**{'client_type': Distributor.__name__,'client__assigned_employee__role__id': self.get_service_person_role().pk, 'client__assigned_employee__users__id__in':self.get_all_service_persons_under(flat=True)})),
            (Distributor, Q(**{'assigned_to__id__in': self.get_all_areas_under(flat=True)})),
            (DeliveryTransaction, Q(**{'client__assigned_employee__role__id': self.get_service_person_role().pk, 'client__assigned_employee__users__id__in':self.get_all_service_persons_under(flat=True)})),
            (Retailers, Q(**{'pk__in': self.get_all_retailers_under()})),
            (PendingRetailerAccount, Q(**{'pk__in': self.get_all_retailers_under(pending=True)})),
            (SecondaryTransaction,Q(**{"client_id__in": self.get_all_distributor_under_same_region(flat=True),"client_2__isnull":False})),
            (SecondaryPendingOrder,Q(**{"from_client_id__in": self.get_all_distributor_under_same_region(flat=True)})),
            (SecondaryCompletedOrder,Q(**{"from_client_id__in":  self.get_all_distributor_under_same_region(flat=True)})),
            (SecondaryDeliveredOrder,Q(**{"from_client_id__in":  self.get_all_distributor_under_same_region(flat=True)})),
            (VisitClient, Q(**{"client_id__in": self.get_all_retailers_under()})),
            (ClientInventory, Q(**{'assigned_to__assigned_employee__role__id': self.get_service_person_role().pk, 'assigned_to__assigned_employee__users__id__in':self.get_all_service_persons_under(flat=True)})),
            (ServicePerson,Q(**{"pk__in": self.get_all_service_persons_under(flat=True)})),
            (PendingOrder,Q(**{"client__type": Distributor.__name__,'client__assigned_employee__role__id': self.get_service_person_role().pk, 'client__assigned_employee__users__id__in':self.get_all_service_persons_under(flat=True)})),
            (OrderNSMApproval,Q(**{"client__type": Distributor.__name__, 'client__assigned_employee__role__id': self.get_service_person_role().pk, 'client__assigned_employee__users__id__in':self.get_all_service_persons_under(flat=True)})),
            (CompletedOrder,Q(**{"client__type": Distributor.__name__, 'client__assigned_employee__role__id': self.get_service_person_role().pk, 'client__assigned_employee__users__id__in':self.get_all_service_persons_under(flat=True)})),
            (RejectedOrder,Q(**{"client__type": Distributor.__name__, 'client__assigned_employee__role__id': self.get_service_person_role().pk, 'client__assigned_employee__users__id__in':self.get_all_service_persons_under(flat=True)})),
            (PrimaryTransaction,Q(**{"client__type": Distributor.__name__, 'client__assigned_employee__role__id': self.get_service_person_role().pk, 'client__assigned_employee__users__id__in':self.get_all_service_persons_under(flat=True), "client_2__isnull": True})),
            (Region, Q(**{'pk': self.assigned_to.pk})),
            (Area, Q(**{'parent__pk': self.assigned_to.pk})),
            (OrderRSMMTMHMApproval,Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (PrimaryDamagedProducts, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (DamageNSMApproval, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (ApprovedDamageProducts, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (ClearedDamage, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (RejectedDamageProducts, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (DraftInvoice, Q(**{"counter_part__assigned_to__parent__id": self.assigned_to.pk})),
            (OpenInvoice, Q(**{"counter_part__assigned_to__parent__id": self.assigned_to.pk})),
            (ClosedInvoice, Q(**{"counter_part__assigned_to__parent__id": self.assigned_to.pk})),
            (Payment, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (ClientCredit, Q(**{"client__assigned_to__parent__id": self.assigned_to.pk})),
            (PrimarySalesReturn, Q(**{"invoice__counter_part__assigned_to__parent__id": self.assigned_to.pk})),
            (CompletedSalesTransaction, Q(**{'client__assigned_employee__role__id': self.get_service_person_role().pk, 'client__assigned_employee__users__id__in':self.get_all_service_persons_under(flat=True)})),
        ]
        for f in filters:
            if f[0] == queryset.model:
                return super().filter_model(queryset=queryset.filter(f[1]))
        return super().filter_model(queryset=queryset)

    @property
    def render_phone(self):
        return ','.join(obj.phone for obj in self.phones.all()) if self.phones.all() else ''

    @classmethod
    def table_columns(cls):
        return 'code', 'name', 'assigned_to:assigned region', 'render_phone', 'last_updated:created_on','last_updated','last_updated_by'

    @property
    def render_email(self):
        return ','.join(obj.email for obj in self.emails.all()) if self.emails.all() else ''

    @property
    def details_config(self):
        d = super().details_config
        custom_list = ['code', 'name', 'user', 'assigned_to', 'date_created', 'last_updated']

        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['assigned_region'] = d.pop('assigned_to')
        d['login'] = d.pop('user')
        d['created_on'] = d.pop('date_created')
        d['created_by'] = self.created_by
        d['last_updated_by'] = self.last_updated_by
        d['location'] = self.addresses.all().first().location if self.addresses.all() else ''
        d['phone'] = self.render_phone
        d['email'] = self.render_email
        d['address'] = self.addresses.all().first if self.addresses.all() else ''
        d['image'] = self.image
        #d['contract_address'] = self.render_phone

        return d

    @classmethod
    def get_serializer(cls):
        ss = ConsoleUser.get_serializer()

        class Serializer(ss):
            role = serializers.PrimaryKeyRelatedField(required=False, queryset=Role.objects.get(name=cls.__name__))

            def create(self, attrs, instance=None):
                attrs.update({
                    "role": Role.objects.get(name=cls.__name__)
                })
                obj = super().create(attrs)
                obj.save()
                return obj

            class Meta(ss.Meta):
                model = cls

        return Serializer