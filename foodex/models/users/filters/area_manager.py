from django.db.models.query_utils import Q
from blackwidow.engine.extensions.model_descriptor import get_model_by_name
from foodex.models import PrimarySalesReturn, ClientCredit, ClosedInvoice, OpenInvoice, DraftInvoice, \
    RejectedDamageProducts, ClearedDamage, ApprovedDamageProducts, DamageNSMApproval, PrimaryDamagedProducts, Payment
from foodex.models.clients.distributor import Distributor
from foodex.models.orders.order_rsm_mtm_hm import OrderRSMMTMHMApproval

__author__ = 'Machine II'

def get_filters(model_object):
    filters = [
        (get_model_by_name('Route'), Q(**{'pk__in': model_object.get_all_assigned_units_under(flat=True)})),
        (get_model_by_name('ServicePerson'), Q(**{'pk__in': model_object.get_all_service_persons_under(flat=True)})),
        (get_model_by_name('AreaSalesManager'), Q(**{'pk': model_object.pk})),
        (get_model_by_name('RegionalSalesManager'), Q(**{'pk': model_object.pk})),  # cannot see unless himmodel_object
        (get_model_by_name('ProductPriceConfig'), Q(**{'client_type': Distributor.__name__,'client__assigned_employee__role__id': model_object.get_service_person_role().pk, 'client__assigned_employee__users__id__in':model_object.get_all_service_persons_under(flat=True)})),
        (get_model_by_name('Distributor'), Q(**{'assigned_to__id': model_object.assigned_to.pk})),
        (get_model_by_name('DeliveryTransaction'), Q(**{'client__assigned_employee__role__id': model_object.get_service_person_role().pk, 'client__assigned_employee__users__id__in':model_object.get_all_service_persons_under(flat=True)})),
        (get_model_by_name('CompletedSalesTransaction', 'foodex'), Q(**{'client__assigned_employee__role__id': model_object.get_service_person_role().pk, 'client__assigned_employee__users__id__in':model_object.get_all_service_persons_under(flat=True)})),
        (get_model_by_name('Retailers'), Q(**{'pk__in': model_object.get_all_retailers_under()})),
        (get_model_by_name('PendingRetailerAccount'), Q(**{'pk__in': model_object.get_all_retailers_under(pending=True)})),
        (get_model_by_name('SecondaryTransaction'),Q(**{"client_id__in": model_object.get_all_distributor_under_same_area(flat=True),"client_2__isnull":False})),
        (get_model_by_name('SecondaryPendingOrder'),Q(**{"from_client_id__in": model_object.get_all_distributor_under_same_area(flat=True)})),
        (get_model_by_name('SecondaryCompletedOrder'),Q(**{"from_client_id__in":  model_object.get_all_distributor_under_same_area(flat=True)})),
        (get_model_by_name('SecondaryDeliveredOrder'),Q(**{"from_client_id__in":  model_object.get_all_distributor_under_same_area(flat=True)})),
        (get_model_by_name('VisitClient'), Q(**{"client_id__in": model_object.get_all_retailers_under()})),
        # (get_model_by_name('ClientInventory'), Q(**{'assigned_to__assigned_employee__role__id': model_object.get_service_person_role().pk, 'assigned_to__assigned_employee__users__id__in':model_object.get_all_service_persons_under(flat=True)})),
        (get_model_by_name('ClientStockCount'), Q(**{'client__assigned_employee__role__id': model_object.get_service_person_role().pk, 'client__assigned_employee__users__id__in':model_object.get_all_service_persons_under(flat=True)})),
        (get_model_by_name('ServicePerson'),Q(**{"pk__in": model_object.get_all_service_persons_under(flat=True)})),
        (get_model_by_name('PendingOrder', 'foodex'),Q(**{"client__type": Distributor.__name__,'client__assigned_employee__role__id': model_object.get_service_person_role().pk, 'client__assigned_employee__users__id__in':model_object.get_all_service_persons_under(flat=True)})),
        (get_model_by_name('OrderNSMApproval'),Q(**{"client__type": Distributor.__name__, 'client__assigned_employee__role__id': model_object.get_service_person_role().pk, 'client__assigned_employee__users__id__in':model_object.get_all_service_persons_under(flat=True)})),
        (get_model_by_name('CompletedOrder', 'foodex'),Q(**{"client__type": Distributor.__name__, 'client__assigned_employee__role__id': model_object.get_service_person_role().pk, 'client__assigned_employee__users__id__in':model_object.get_all_service_persons_under(flat=True)})),
        (get_model_by_name('RejectedOrder', 'foodex'),Q(**{"client__type": Distributor.__name__, 'client__assigned_employee__role__id': model_object.get_service_person_role().pk, 'client__assigned_employee__users__id__in':model_object.get_all_service_persons_under(flat=True)})),
        (get_model_by_name('PrimaryTransaction'),Q(**{"client__type": Distributor.__name__, 'client__assigned_employee__role__id': model_object.get_service_person_role().pk, 'client__assigned_employee__users__id__in':model_object.get_all_service_persons_under(flat=True), "client_2__isnull": True})),
        (get_model_by_name('Area'), Q(**{'pk': model_object.assigned_to.pk})),
        (get_model_by_name('Region'), Q(**{'pk': model_object.assigned_to.parent.pk})),
        (OrderRSMMTMHMApproval,Q(**{"client__assigned_to__parent__id": model_object.assigned_to.parent.pk})),
        (PrimaryDamagedProducts, Q(**{"client__assigned_to__id": model_object.assigned_to.pk})),
        (DamageNSMApproval, Q(**{"client__assigned_to__id": model_object.assigned_to.pk})),
        (ApprovedDamageProducts, Q(**{"client__assigned_to__id": model_object.assigned_to.pk})),
        (ClearedDamage, Q(**{"client__assigned_to__id": model_object.assigned_to.pk})),
        (RejectedDamageProducts, Q(**{"client__assigned_to__id": model_object.assigned_to.pk})),
        (DraftInvoice, Q(**{"counter_part__assigned_to__id": model_object.assigned_to.pk})),
        (OpenInvoice, Q(**{"counter_part__assigned_to__id": model_object.assigned_to.pk})),
        (ClosedInvoice, Q(**{"counter_part__assigned_to__id": model_object.assigned_to.pk})),
        (Payment, Q(**{"client__assigned_to__id": model_object.assigned_to.pk})),
        (ClientCredit, Q(**{"client__assigned_to__id": model_object.assigned_to.pk})),
        (PrimarySalesReturn, Q(**{"invoice__counter_part__assigned_to__id": model_object.assigned_to.pk})),
        ]
    return filters
