from django.db.models.loading import get_model
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.sales.decorators.model_decorators import enable_individual_assignment
from blackwidow.sales.models.common.product_price_config import ProductPriceConfig
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Sohel'


@decorate(save_audit_log, is_object_context, enable_individual_assignment(models=[ProductPriceConfig]), expose_api('regions'),
          route(route='regions', group='Organizations/Hierarchy', module=ModuleEnum.Administration, display_name="Region"))
class Region(InfrastructureUnit):

    @classmethod
    def table_columns(cls):
        return 'code', 'name:Region Name', 'date_created', 'last_updated'

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'name', 'date_created', 'last_updated']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]

        d['created_by'] = self.created_by
        d['last_updated_by'] = self.last_updated

        return d

    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Edit',
                action='edit',
                icon='fbx-rightnav-edit',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit)
            )
        ]

    @property
    def tabs_config(self):
        tabs = list()
        Area = get_model('foodex', 'Area')
        all_areas_under = Area.objects.filter(parent_id=self.pk)

        tabs.append(TabView(
            title='Area(s)',
            access_key='areas_under',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            property='parent',
            related_model=Area,
            queryset=all_areas_under
        ))

        return tabs

    class Meta:
        proxy = True