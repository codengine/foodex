from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.clients.client_assignment import ClientAssignment
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.core.viewmodels.tabs_config import TabView, TabViewAction
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route, partial_route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.engine.extensions.bw_titleize import bw_titleize
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.clients.retailers import Retailers


@decorate(save_audit_log, is_object_context,
          expose_api('routes'),
          route(route='routes', module=ModuleEnum.Administration, display_name='Route', group='Organizations/Hierarchy',group_order=1,item_order=5),
          partial_route(relation=['normal'], models=[Retailers, Client, ConsoleUser]))
class Route(InfrastructureUnit):

    def save(self, *args, **kwargs):
        from django.db import transaction

        with transaction.atomic():
            super().save(*args, **kwargs)
            if not self.assigned_clients.exists():
                dis_assign = ClientAssignment()
                dis_assign.client_type = 'Retailers'
                dis_assign.organization = self.organization
                dis_assign.save()
                self.assigned_clients.add(dis_assign)

    @property
    def render_service_day(self):
        return self.service_days.all().first() if self.service_days.exists() else ""

    @classmethod
    def filter_query(cls,query_set,custom_search_fields=[]):
        from django.db.models import Q
        from functools import reduce
        import operator
        for key, value in custom_search_fields:
            if key.startswith("__search__areas"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(parent_client__assigned_to__name__icontains=v))
                    routes = Route.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=routes.values_list('pk', flat=True))
                except:
                    pass
            elif key.startswith("__search__distributors"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(parent_client__name__icontains=v))
                    routes = Route.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=routes.values_list('pk', flat=True))
                except:
                    pass
        return query_set

    @classmethod
    def get_url_by_name(cls, name):
        if name == 'render_area':
            return '__search__areas'
        elif name == 'render_distributor':
            return '__search__distributors'
        return None

    @property
    def render_code(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.pk}) + "' >" + self.code + "</a>")


    @property
    def render_distributor(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.parent_client.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.parent_client.pk}) + "' >" + str(self.parent_client) + "</a>")

    @classmethod
    def table_columns(cls):
        return "code", "name", "render_service_day", "parent_client:Distributor", "render_area", "date_created:Created On", "last_updated"

    @property
    def details_config(self):
        dict = super().details_config

        custom_list = ['code', 'name', 'name', 'parent_client', 'date_created', 'last_updated']

        for key in dict:
            if key in custom_list:
                pass
            else:
                del dict[key]

        dict['distributor'] = dict.pop('parent_client')
        dict['created_on'] = dict.pop('date_created')
        dict['created_by'] = self.created_by
        dict['last_updated_by'] = self.last_updated_by
        dict["location"] = self.address.location if self.address else ''
        dict["service_day"] = self.service_days.first() if self.service_days else ""
        dict["area"] = self.parent_client.assigned_to
        dict['location'] = self.address if self.address else ""
        return dict

    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Edit',
                action='edit',
                icon='fbx-rightnav-edit',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit)
            )
        ]

    @property
    def tabs_config(self):
        tabs = []
        clients = self.assigned_clients.all()
        retailers_assigned = []
        all_routes = Route.objects.all()
        for r in all_routes:
            clnts = list(r.assigned_clients.filter(client_type=Retailers.__name__).first().clients.values_list("id"))
            retailers_assigned += [c[0] for c in clnts]
        for c in clients:
            tabs.append(TabView(
                title=bw_titleize(c.client_type) + '(s)',
                access_key=c.client_type.lower(),
                route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
                relation_type=ModelRelationType.NORMAL,
                property=c.clients,
                add_more_queryset=Client.objects.filter(type=c.client_type,is_approved=True).exclude(pk__in=retailers_assigned).exclude(pk__in=c.clients.values('id')),
                related_model=Client,
                actions=[
                    TabViewAction(
                        title='Add',
                        action='add',
                        icon='icon-plus',
                        route_name=Client.get_route_name(action=ViewActionEnum.PartialBulkAdd, parent=self.__class__.__name__.lower()),
                        css_class='manage-action load-modal fis-plus-ico',
                        enable_wide_popup=True
                    ),
                    TabViewAction(
                        title='Remove',
                        action='partial-remove',
                        icon='icon-remove',
                        route_name=Client.get_route_name(action=ViewActionEnum.PartialBulkRemove, parent=self.__class__.__name__.lower()),
                        css_class='manage-action delete-item fis-remove-ico'
                    )
                ]

            ))


        return tabs

    class Meta:
        proxy = True