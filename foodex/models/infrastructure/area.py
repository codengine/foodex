from django.db.models.loading import get_model
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from django.db import transaction
from django.db.models.query_utils import Q
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.sales.decorators.model_decorators import enable_individual_assignment
from blackwidow.sales.models.common.product_price_config import ProductPriceConfig
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.clients.distributor import Distributor

__author__ = 'Mahmud'


@decorate(save_audit_log, is_object_context, enable_individual_assignment(models=[ProductPriceConfig]), expose_api('areas'),
          route(route='areas', group='Organizations/Hierarchy', module=ModuleEnum.Administration, display_name="Area"))
class Area(InfrastructureUnit):

    @classmethod
    def table_columns(cls):
        return 'code', 'name:Area Name', 'parent:Region', 'date_created', 'last_updated'

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'name', 'date_created', 'last_updated']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]

        d['created_by'] = self.created_by
        d['last_updated_by'] = self.last_updated

        return d

    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Edit',
                action='edit',
                icon='fbx-rightnav-edit',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit)
            )
        ]

    @property
    def tabs_config(self):
        tabs = []
        tabs.append(TabView(
            title='Distributors',
            access_key='distributors',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model=Distributor,
            queryset_filter=Q(**{'assigned_to__id': self.pk})
        ))

        tabs.append(TabView(
            title='HoReCas',
            access_key='horecas',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model=get_model('foodex','HoReCo'),
            queryset_filter=Q(**{'assigned_to__id': self.pk})
        ))

        tabs.append(TabView(
            title='Modern Trades',
            access_key='modern_trades',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model=get_model('foodex','ModernTrade'),
            queryset_filter=Q(**{'assigned_to__id': self.pk})
        ))

        tabs.append(TabView(
            title='Wholesalers',
            access_key='wholesalers',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model=get_model('foodex','WholeSaler'),
            queryset_filter=Q(**{'assigned_to__id': self.pk})
        ))

        return tabs


    def save(self, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)

    class Meta:
        proxy = True