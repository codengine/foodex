from django.db import transaction
from django.db.models.loading import get_model
from blackwidow.core.managers.modelmanager import DomainEntityModelManager
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.sales.models.products.product import Product
from config.enums.modules_enum import ModuleEnum
from multiprocessing.synchronize import Lock
from threading import Thread

__author__ = 'Sohel'

@decorate(save_audit_log, is_object_context,  route(route='warehouses', module=ModuleEnum.Administration, display_name='Warehouse', group='Organizations/Hierarchy'))
class Warehouse(InfrastructureUnit):

    objects = DomainEntityModelManager(filter={'type': 'WareHouse'})

    @property
    def get_warehouse_id(self):
        return self.pk

    @property
    def get_warehouse_name(self):
        return self.name

    def initialize_warehouse_inventory(self):
        all_products = Product.objects.all()
        all_warehouses = Warehouse.objects.all()

        WarehouseInventory = get_model("foodex","WarehouseInventory")

        with transaction.atomic():
            for warehouse in all_warehouses:
                for product in all_products:
                    if not WarehouseInventory.objects.filter(product=product,assigned_to=warehouse).exists():
                        product_inventory_object = WarehouseInventory()
                        product_inventory_object.product = product
                        product_inventory_object.assigned_to = warehouse
                        product_inventory_object.stock = 0
                        product_inventory_object.organization = warehouse.organization
                        product_inventory_object.save()

    def save(self, *args, **kwargs):
        self.type = 'WareHouse'
        super().save(*args,**kwargs)

        lock = Lock(ctx=None)
        lock.acquire(True)
        process = Thread(target=self.initialize_warehouse_inventory)
        process.start()
        lock.release()


    class Meta:
        proxy = True

    @property
    def render_phone(self):
        return self.assigned_to.phones.all().first() if self.assigned_to.phones is not None else ''

    @property
    def render_address(self):
        return self.address.location if self.address.location is not None else 'N/A'

    @classmethod
    def table_columns(cls):
        return 'code', 'name:Warehouse Name', 'date_created:Created On', 'created_by', 'last_updated'


    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'name', 'assigned_to', 'address', 'date_created', 'last_updated']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['created_by'] = self.created_by
        d['warehouse_name'] = d.pop('name')
        d['created_on'] = d.pop('date_created')
        d['last_updated_by'] = self.last_updated_by
        d['location'] = self.render_address
        d['phone'] = self.render_phone

        return d
