from datetime import datetime
from decimal import Decimal
from django.core.urlresolvers import reverse
from django import forms
from django.db.models.loading import get_model
from django.forms.formsets import formset_factory, BaseFormSet
from django.utils.safestring import mark_safe
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.organizations.organization import Organization
from blackwidow.core.viewmodels.tabs_config import TabView, TabViewAction
from blackwidow.engine.decorators.route_partial_routes import route, partial_route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.sales.models.invoice.invoice import Invoice
from blackwidow.sales.models.orders.order_breakdown import OrderBreakdown
from blackwidow.sales.models.products.product import Product
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from foodex.models.inventory.client_inventory import ClientInventory
from foodex.models.inventory.warehouse_inventory import WarehouseInventory
from foodex.models.paymentcredits.payment import Payment
from foodex.models.products.primary_sales_return import PrimarySalesReturn
from foodex.models.transactions.completed_sales_transaction import CompletedSalesTransaction

__author__ = 'Sohel'


@decorate(save_audit_log, is_object_context,
          route(route='open-invoices', display_name='Open Invoice', module=ModuleEnum.Execute, group_order=3, item_order=7, group='Credits & payments(clients)'),
          partial_route(relation=['normal'], models=[Payment]))
class OpenInvoice(Invoice):
    class Meta:
        proxy = True

    def partial_tab_redirect_url(self, instance=None):
        try:
            if instance.invoice:
                if instance.invoice.type == 'ClosedInvoice':
                    return reverse(self.get_route_name(ViewActionEnum.Manage))
                else:
                    return ""
        except:
            return ""

    @property
    def render_primary_sales_transaction_id(self):
        primary_sales_transactions = CompletedSalesTransaction.objects.filter(invoice=self)
        if primary_sales_transactions.exists():
            primary_sales_transaction = primary_sales_transactions.first()
            return primary_sales_transaction
        return ""

    @property
    def render_area(self):
        return self.counter_part.assigned_to

    @property
    def render_still_to_pay(self):
        return self.price_total - self.actual_amount_paid

    @property
    def render_distributor(self):
        return self.counter_part.name if self.counter_part else ''

    @property
    def render_invoice_value(self):
        return self.price_total

    @property
    def render_already_paid(self):
        return self.actual_amount_paid

    @property
    def render_last_updated(self):
        return datetime.fromtimestamp(self.last_updated/1000).strftime('%d/%m/%Y %H:%M %p')

    @property
    def render_payment(self):
        return '0'

    @property
    def render_payment_method(self):
        return ""

    @property
    def render_reference(self):
        return ""

    @property
    def render_client(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.counter_part.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.counter_part.pk}) + "' >" + str(self.counter_part) + "</a>")


    @classmethod
    def filter_query(cls,query_set,custom_search_fields=[]):
        from django.db.models import Q
        from functools import reduce
        import operator
        from foodex.models import CompletedSalesTransaction
        # ongoing work
        for key, value in custom_search_fields:
            if key.startswith("__search__text__trans_id"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(code__icontains=v))
                    invoices_pk = []
                    transactions = CompletedSalesTransaction.objects.filter(reduce(operator.or_, or_list))
                    for trans in transactions:
                        invoices_pk.append(trans.invoice.pk)
                    query_set = query_set.filter(pk__in=invoices_pk)
                except Exception as e:
                    pass
            elif key.startswith("__search__areas"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(counter_part__assigned_to__name__icontains=v))
                    invoices = OpenInvoice.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=invoices.values_list('pk', flat=True))
                except Exception as e:
                    pass
        return query_set

    @classmethod
    def get_url_by_name(cls, name):
        if name == 'counter_part':
            return 'distributors'
        elif name == 'render_primary_sales_transaction_id':
            return '__search__text__trans_id'
        elif name == 'render_area':
            return '__search__areas'
        return None

    @classmethod
    def table_columns(cls):
        return 'code', 'render_client', 'render_primary_sales_transaction_id', 'render_area', 'invoice_number', 'price_total:invoice_value', 'actual_amount_paid:paid', 'render_still_to_pay', 'damage_value', 'date_created:created_on', 'last_updated', 'last_updated_by'

    def get_choice_name(self):
        return self.invoice_number

    def __str__(self):
        return self.invoice_number

    @classmethod
    def default_order_by(cls):
        return '-last_updated'



    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'invoice_number', 'date_created', 'last_updated']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['created_by'] = self.created_by
        d['client'] = self.render_client
        d['Invoice Value'] = self.price_total
        d['Paid'] = self.actual_amount_paid
        d['Still to pay'] = self.render_still_to_pay
        d['area'] = self.render_area
        d['primary_sales_transaction_id'] = self.render_primary_sales_transaction_id
        d['last_updated_by'] = self.last_updated_by
        return d


    @property
    def tabs_config(self):
        tabs = list()
        breakdown_set = self.render_primary_sales_transaction_id.breakdown.all() if self.render_primary_sales_transaction_id else None

        tabs.append(TabView(
            title='Payment(s)',
            access_key='payments',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            property=self.payment_set,
            add_more_queryset=Payment.objects.all(),
            queryset=Payment.objects.filter(invoice_id=self.pk),
            related_model=Payment,
            actions=[
                TabViewAction(
                    title='Create',
                    action='create',
                    icon='icon-plus',
                    route_name=Payment.get_route_name(action=ViewActionEnum.PartialCreate, parent=self.__class__.__name__.lower()),
                    css_class='manage-action load-modal fis-plus-ico',

                    )
            ]))

        tabs.append(TabView(
            title='Transaction Details',
            access_key='breakdown',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='sales.OrderTransactionBreakdown',
            queryset=breakdown_set
        ))

        stock_returns = PrimarySalesReturn.objects.filter(invoice_id=self.pk)

        tabs.append(TabView(
            title='Sales Return(s)',
            access_key='sales_returns',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='foodex.PrimarySalesReturn',
            queryset=stock_returns
        ))

        return tabs

    @classmethod
    def get_object_inline_buttons(cls):
        return [ ViewActionEnum.Details ]

    @classmethod
    def get_manage_buttons(cls):
        return [

        ]

    @classmethod
    def get_advanced_edit_form(cls, **kwargs):

        class _SalesReturnForm(GenericFormMixin):
            def __init__(self, data=None, files=None,  **kwargs):
                super().__init__(data=data, files=files, **kwargs)
                initial = kwargs.get("initial")
                if initial:
                    initial_product = Product.objects.filter(pk=initial['product'].pk)
                    initial_total_items =initial['total_items']
                    initial_return_quantity =initial['return_quantity']
                    self.fields['product'] = GenericModelChoiceField(initial=initial_product,queryset=initial_product, label='Product', widget=forms.Select(attrs={'class': 'select2', 'width': '220'}), required=True)
                    self.fields['total_items'] = forms.IntegerField(initial=initial_total_items,min_value=1,required=True)
                    self.fields['return_quantity'] = forms.IntegerField(initial=initial_return_quantity,min_value=1,required=False)

            def show_form_inline(self):
                return True

            def show_header_inline(self):
                return True

            def clean(self):
                data = self.cleaned_data
                return data

            def save(self, commit=True):
                try:
                    if hasattr(self.instance,"pk"):
                        self.instance.save(commit)
                    else:
                        pass
                except:
                    pass
                return self.instance

            class Meta:
                model = OrderBreakdown
                fields = ['product', 'total_items']
                
        class _SalesReturnFormSet(BaseFormSet):
            pass

        class AdvancedEditForm(GenericFormMixin):
            def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
                super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
                invoice_id = kwargs.get("object_id")
                if 'object_id' in kwargs:
                    kwargs.pop('object_id')
                if invoice_id:
                    try:
                        invoice_id = int(invoice_id)
                    except:
                        invoice_id = None
                    if invoice_id:
                        self.fields['invoice'] = GenericModelChoiceField(label='Invoice', empty_label="Select Invoice", initial=OpenInvoice.objects.filter(pk=invoice_id).first(), required=True, queryset=OpenInvoice.objects.filter(pk=invoice_id), widget=forms.Select(attrs={'class': 'select2'}))

                        ###Now add breakdown from CompletedOrder using this invoice.
                        CompletedOrder = get_model('sales','CompletedOrder')
                        stockout_order = CompletedOrder.objects.filter(invoice_id=invoice_id).first()

                        SalesFormSet = formset_factory(_SalesReturnForm, formset=_SalesReturnFormSet, extra=0)
                        initial_data = []
                        for brkdown in stockout_order.breakdown.all():
                            if brkdown.total_items > 0:
                                initial_data += [ {
                                    "product": brkdown.product,
                                    "total_items": brkdown.total_items,
                                    "return_quantity": 0
                                } ]
                        self.add_child_form("breakdown", SalesFormSet(initial=initial_data))

            def save(self, commit=True):
                data = self.data
                invoice_id = data['invoice']
                invoice_object = OpenInvoice.objects.get(pk=invoice_id)
                CompletedOrder = get_model('sales','CompletedOrder')
                stockout_order = CompletedOrder.objects.filter(invoice_id=invoice_id).first()
                item_count = stockout_order.breakdown.filter(total_items__gt=0).count()

                transaction = CompletedSalesTransaction.objects.get(invoice_id=invoice_id)

                original_records = {}
                for brkdown in stockout_order.breakdown.all():
                    original_records[brkdown.product.pk] = {
                        "total": brkdown.total_items,
                        "unit_price": brkdown.unit_price,
                        "order_breakdown": brkdown,
                        "tx_breakdown": transaction.breakdown.filter(product_id=brkdown.product.pk).first()
                    }

                entries = []
                invoice_adjust_amount = 0
                tx_adjust_subtotal = 0
                tx_adjust_total = 0
                for i in range(item_count):
                    product_id = int(data.get('form-%i-product' % i))
                    total_items = int(data.get('form-%i-total_items' % i))
                    return_quantity = int(data.get('form-%i-return_quantity' % i))

                    if return_quantity > 0 and return_quantity <= original_records.get(product_id)['total']:
                        primary_sales_return = PrimarySalesReturn()
                        primary_sales_return.product_id = product_id
                        primary_sales_return.invoice_id = invoice_id
                        primary_sales_return.quantity = return_quantity
                        primary_sales_return.organization_id = Organization.objects.all().first().pk
                        primary_sales_return.save()

                        ###Adjust stockout
                        stockout_brkdown = original_records.get(product_id)['order_breakdown']
                        stockout_brkdown.total_items = stockout_brkdown.total_items - return_quantity
                        stockout_brkdown.sub_total = stockout_brkdown.total_items * stockout_brkdown.unit_price
                        stockout_brkdown.total = stockout_brkdown.sub_total - stockout_brkdown.discount
                        stockout_brkdown.save()

                        ###Adjust transaction.
                        tx_brkdown = original_records.get(product_id)['tx_breakdown']
                        tx_brkdown.quantity = tx_brkdown.quantity - return_quantity
                        tx_brkdown.sub_total = tx_brkdown.quantity * tx_brkdown.unit_price
                        tx_brkdown.total = tx_brkdown.sub_total - tx_brkdown.discount
                        tx_brkdown.closing_stock = tx_brkdown.opening_stock - tx_brkdown.quantity
                        tx_brkdown.save()

                        tx_adjust_subtotal += return_quantity * tx_brkdown.unit_price
                        tx_adjust_total += return_quantity * tx_brkdown.unit_price

                        warehouse = transaction.infrastructure_unit
                        wis = WarehouseInventory.objects.filter(assigned_to_id=warehouse.pk,product_id=product_id)
                        if wis.exists():
                            wi = wis.first()
                            wi.stock = wi.stock + return_quantity
                            wi.save()


                        client = transaction.client
                        cis = ClientInventory.objects.filter(assigned_to_id=client.pk,product_id=product_id)
                        if cis.exists():
                            ci = cis.first()
                            ci.stock = ci.stock - return_quantity
                            ci.save()

                        invoice_adjust_amount += return_quantity * original_records.get(product_id)['unit_price']

                if invoice_adjust_amount > 0:
                    invoice_object.price_total = invoice_object.price_total - Decimal(invoice_adjust_amount)
                    invoice_object.save()

                    invoice_client = invoice_object.counter_part
                    invoice_client.available_credit = invoice_client.available_credit + invoice_adjust_amount
                    invoice_client.save()

                if tx_adjust_subtotal > 0 and tx_adjust_total > 0:
                    transaction.sub_total = transaction.sub_total - tx_adjust_subtotal
                    transaction.total = transaction.total - tx_adjust_total
                    transaction.save()



                return self.instance


        return AdvancedEditForm

    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Sales Return',
                action='return_stock',
                icon='fbx-rightnav-edit',
                ajax='1',
                classes='manage-action all-action load-advanced-edit-form',
                ignore_id=True,
                object_id=self.pk,
                url_name=self.__class__.get_route_name(action=ViewActionEnum.AdvancedEdit)
            )
        ]
