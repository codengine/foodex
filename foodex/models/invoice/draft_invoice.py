from blackwidow.core.models import MaxSequence
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.sales.models.invoice.invoice import Invoice
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Sohel'


@decorate(save_audit_log, is_object_context,
          route(route='draft-invoices', display_name='Draft Invoice', module=ModuleEnum.Execute, group_order=3, item_order=6, group='Credits & payments(clients)'))
class DraftInvoice(Invoice):
    class Meta:
        proxy = True

    @property
    def code_prefix(self):
        return "INV"

    @classmethod
    def get_manage_buttons(cls):
        return []

    @classmethod
    def get_object_inline_buttons(cls):
        return [ ViewActionEnum.Details ]

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # max_seqs = MaxSequence.objects.filter(context=self.__class__.__name__)
        # if max_seqs.exists():
        #     max_seq = max_seqs[0]
        self.invoice_number =  self.code #'INV' + self.code_separator + str(max_seq.value-1).zfill(6)
        super().save()