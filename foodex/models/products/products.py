import os
from threading import Thread
from django.core.files.base import File
from django.db import transaction
from django.db.models.query_utils import Q
from django.dispatch.dispatcher import receiver
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
from blackwidow.core.models.config.importer_config import ImporterConfig
from blackwidow.core.models.file.imagefileobject import ImageFileObject
from blackwidow.core.models.manufacturers.manufacturer import Manufacturer
from blackwidow.core.signals.signals import import_completed
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.extensions.clock import Clock
from blackwidow.engine.managers.filemanager import FileManager
from blackwidow.sales.models.common.product_price import ProductPrice, ProductPriceEnum
from blackwidow.sales.models.common.product_price_config import receive_product_added
from blackwidow.sales.models.products.client_current_inventory import ClientCurrentInventory
from blackwidow.sales.models.products.product import Product, ProductPriceValue
from blackwidow.sales.models.products.product_group import ProductGroup
from foodex.models.clients.distributor import Distributor
from settings import PROJECT_PATH, STATIC_UPLOAD_ROOT
from multiprocessing.synchronize import Lock



@decorate(enable_import)
class FoodexProduct(Product):
    class Meta:
        proxy = True

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if result or importer_config.columns.count() == 0:
            importer_config.save(**kwargs)
        else:
            for items in importer_config.columns.all():
                items.delete()
        columns = [
            ImporterColumnConfig(column=0, column_name='Code', property_name='code', ignore=False),
            ImporterColumnConfig(column=1, column_name='name', property_name='name', ignore=False),
            ImporterColumnConfig(column=2, column_name='short_name', property_name='short_name', ignore=False),
            ImporterColumnConfig(column=3, column_name='description', property_name='description', ignore=False),
            ImporterColumnConfig(column=4, column_name='origin', property_name='origin', ignore=False),
            ImporterColumnConfig(column=5, column_name='Ref cost', property_name='price', ignore=False),
            ImporterColumnConfig(column=6, column_name='Ref price', property_name='price', ignore=False),
            ImporterColumnConfig(column=7, column_name='Rf-NtCu markup/discount', property_name='price', ignore=False),
            ImporterColumnConfig(column=8, column_name='Net Cust price', property_name='price', ignore=False),
            ImporterColumnConfig(column=9, column_name='Vat Rate', property_name='price', ignore=False),
            ImporterColumnConfig(column=10, column_name='Gross  Cust Price', property_name='price', ignore=False),
            ImporterColumnConfig(column=11, column_name='GrCu-Di markup', property_name='price', ignore=False),
            ImporterColumnConfig(column=12, column_name='Dist price', property_name='price', ignore=False),
            ImporterColumnConfig(column=13, column_name='Di-Rt markup', property_name='price', ignore=False),
            ImporterColumnConfig(column=14, column_name='Retail price', property_name='price', ignore=False),
            ImporterColumnConfig(column=15, column_name='Category', property_name='groups:name', ignore=False),
            ImporterColumnConfig(column=16, column_name='Icon', property_name='icon:file', ignore=False)
        ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config


    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        with transaction.atomic():
            org = user.organization
            manufacturer, result = Manufacturer.objects.get_or_create(name='Foodex', organization=org)
            if result:
                manufacturer.created_by = user
                manufacturer.last_updated_by = user
                manufacturer.save()

            products = Product.objects.filter(Q(code=data['0']) | Q(name=data['1'], description=data['3']))
            if products.exists():
                product = products[0]
                product.created_by = user
                product.name = data['1']
                product.short_name = data['2']
                product.description = data['3']
                product.last_updated_by = user
                product.manufacturer = manufacturer
                product.origin = data['4']
                product.save(signal=False)
            else:
                product = Product(name=data['1'], description=data['3'], organization=org)
                product.short_name = data['2']
                product.created_by = user
                product.last_updated_by = user
                product.manufacturer = manufacturer
                product.origin = data['4']
                product.save(signal=False)

            category, result = ProductGroup.objects.get_or_create(name=data['15'], organization=org)
            if result:
                category.created_by = user
                category.last_updated_by = user
                category.save()

            product.categories.add(category)

            prices = [(ProductPriceEnum.ref_cost.value["name"], ProductPriceEnum.ref_cost.value["short_name"], 5, "", tuple(), False),
                      (ProductPriceEnum.ref_price.value["name"], ProductPriceEnum.ref_price.value["short_name"], 6, "", tuple(), False),
                      (ProductPriceEnum.ref_net_cust_markup.value["name"], ProductPriceEnum.ref_net_cust_markup.value["short_name"], 7, "", tuple(), False),
                      (ProductPriceEnum.net_cust_price.value["name"], ProductPriceEnum.net_cust_price.value["short_name"], 8, "[RefP] + [RefP] * [Rf-NtCu-markup]", ("RefP", "Rf-NtCu-markup"), False),
                      (ProductPriceEnum.vat_rate.value["name"], ProductPriceEnum.vat_rate.value["short_name"], 9, "", tuple(), False),
                      (ProductPriceEnum.gross_cust_price.value["name"], ProductPriceEnum.gross_cust_price.value["short_name"], 10, "[Net-CustP] + [Net-CustP] * [VAT]", ("Net-CustP", "VAT"), False),
                      (ProductPriceEnum.gross_cust_distributor_markup.value["name"], ProductPriceEnum.gross_cust_distributor_markup.value["short_name"], 11, "", tuple(), False),
                      (ProductPriceEnum.dist_price.value["name"], ProductPriceEnum.dist_price.value["short_name"], 12, "[Gross-CustP] + [Gross-CustP] * [Gross-CusDi-markup]", ("Gross-CustP", "Gross-CusDi-markup"), False),
                      (ProductPriceEnum.dist_retailer_markup.value["name"], ProductPriceEnum.dist_retailer_markup.value["short_name"], 13, "", tuple(), False),
                      (ProductPriceEnum.retail_price.value["name"], ProductPriceEnum.retail_price.value["short_name"], 14, "[DP] + [DP] * [Di-Rt-markup]", ("DP", "Di-Rt-markup"), True)]

            for p in prices:
                price, result = ProductPrice.objects.get_or_create(name=p[0], short_name=p[1])

                price.equation = p[3]
                price.parents.clear()
                for p_2 in p[4]:
                    _p_2 = ProductPrice.objects.get(short_name=p_2)
                    price.parents.add(_p_2)
                price.save()

                prices_value = product.prices.filter(price=price)
                if prices_value.exists():
                    price_value = prices_value[0]
                    price_value.value = 0.0 if data[str(p[2])] is None else data[str(p[2])]
                    price_value.save()
                else:
                    price_value = ProductPriceValue.objects.create(price=price, value=0.0 if data[str(p[2])] is None else data[str(p[2])])
                    price_value.load_initial_data(org=org, user=user, date=int(Clock.timestamp()), timestamp=int(Clock.timestamp()))
                    price_value.save()
                    product.prices.add(price_value)

            if product.icon is None:
                icon = ImageFileObject()
            else:
                icon = product.icon
            icon_location = os.path.join(PROJECT_PATH, STATIC_UPLOAD_ROOT + 'icons/')
            new_icon_location = os.path.join(PROJECT_PATH, STATIC_UPLOAD_ROOT + 'imported_icons/')
            FileManager.create_dirs(icon_location)
            FileManager.create_dirs(new_icon_location)
            icon_name = data['16']
            icon_name = icon_name if icon_name is not None else ''
            icon.name = icon_name
            icon.extension = icon_name[icon_name.rfind('.'):] if icon_name is not None or icon_name != '' else ''
            icon.organization = org
            try:
                icon_file = open(os.path.join(icon_location, icon_name), 'rb')
                icon.file.save(icon_name, File(icon_file))
                icon.path = icon_file
                icon_file.close()
            except FileNotFoundError as exp:
                icon.file = None
            icon.save()
            product.icon = icon
            product.save(signal=False)

            receive_product_added(product=product)

            return product.pk

def initialize_client_inventory(client,organization,user):
    all_products = Product.objects.all()
    for a_product in all_products:
        client_current_inventory_objects = ClientCurrentInventory.objects.filter(assigned_to=client,product=a_product)
        if not client_current_inventory_objects.exists():
            client_current_inventory_object = ClientCurrentInventory()
            client_current_inventory_object.assigned_to_id = client.pk
            client_current_inventory_object.organization = organization
            client_current_inventory_object.product_id = a_product.pk
            client_current_inventory_object.stock = 0
            client_current_inventory_object.created_by = user
            client_current_inventory_object.organization = organization
            client_current_inventory_object.save()

def initialize_all_clients_inventory(user=None,organization=None):
    all_distributors = Client.objects.filter(type=Distributor.__name__)
    for a_distributor in all_distributors:
        initialize_client_inventory(client=a_distributor,organization=organization,user=user)

@receiver(import_completed, sender=FoodexProduct)
def foodex_product_import_completed(sender, items=None, user=None, organization=None, **kwargs):
    lock = Lock(ctx=None)
    lock.acquire(True)
    process = Thread(target=initialize_all_clients_inventory, args=(user,organization,))
    process.start()
    lock.release()