from collections import OrderedDict
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import save_audit_log, decorate, is_object_context
from blackwidow.sales.models.payments.payment_methods import Payment
from blackwidow.sales.models.invoice.invoice import Invoice
from blackwidow.sales.models.products.primary_damage_products import PrimaryDamagedProducts
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from django.db import models
from blackwidow.core.models.clients.client import Client
from foodex.models.invoice.open_invoice import OpenInvoice


__author__ = 'Sohel'

# @decorate(save_audit_log, is_object_context,
#           expose_api("cleared-damage-products"),
#           route(route='cleared-damage-products', module=ModuleEnum.Execute, group_order=3, item_order=4, display_name='Cleared damage', group='Credits & payments(clients)'))
class ClearedDamageProducts(PrimaryDamagedProducts):
    class Meta:
        proxy = True


    @property
    def render_area(self):
        return self.client.assigned_to


    @classmethod
    def table_columns(cls):
        return 'code', 'render_area', 'client', 'product', 'quantity:quantity_damaged', 'value', 'date_created:Created', 'created_by'

    @classmethod
    def get_manage_buttons(cls):
        return []


    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'client', 'product', 'address', 'value', 'date_created']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['created_on'] = d.pop('date_created')
        d['quantity_damaged'] = self.quantity
        d['area'] = self.render_area
        d['created_by'] = self.created_by

        return d



@decorate(save_audit_log, is_object_context,
          expose_api("cleared-damage"),
          route(route='cleared-damage', module=ModuleEnum.Execute, group_order=3, item_order=4, display_name='Cleared damage', group='Credits & payments(clients)'))
class ClearedDamage(OrganizationDomainEntity):
    client = models.ForeignKey(Client, null=True, default=None)
    total_available_damage = models.DecimalField(decimal_places=2, max_digits=20, default=0)
    total_cleared_damage = models.DecimalField(decimal_places=2, max_digits=20, default=0)
    invoice = models.ForeignKey(Invoice, null=True)
    payment = models.ForeignKey(Payment, null=True)

    @property
    def render_client(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.client.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")

    @property
    def render_invoice(self):
        return mark_safe("<a class='inline-link' href='" + reverse(OpenInvoice.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.invoice.pk}) + "' >" + str(self.invoice) + "</a>")

    @classmethod
    def table_columns(cls):
        return 'code', 'render_client', 'total_available_damage', 'total_cleared_damage', 'last_updated', 'last_updated_by'

    @classmethod
    def get_manage_buttons(cls):
        return []

    @property
    def details_config(self):
        dict = OrderedDict()
        dict['code'] = self.code
        dict['client'] = self.render_client
        dict['total_available_damage'] = self.total_available_damage
        dict['total_cleared_damage'] = self.total_cleared_damage
        dict['invoice'] = self.render_invoice
        dict['payment'] = self.payment
        dict['last_updated'] = self.render_timestamp(self.last_updated)
        dict['last_updated_by'] = self.last_updated_by

        return dict