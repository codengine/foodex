from decimal import Decimal
from django.core.urlresolvers import reverse
from django.db.models.loading import get_model
from django.utils.safestring import mark_safe
from blackwidow.core.process.process_breakdown import ProcessBreakdown, ApprovalStatusEnum
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import save_audit_log, decorate, is_object_context
from blackwidow.sales.models.products.primary_damage_products import PrimaryDamagedProducts
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Sohel'

@decorate(save_audit_log, is_object_context,
          expose_api("damage-nsm-products"),
          route(route='damage-nsm-products', module=ModuleEnum.Execute, group_order=3, item_order=2, display_name='Damages awaiting NSM approval', group='Credits & payments(clients)'))
class DamageNSMApproval(PrimaryDamagedProducts):
    class Meta:
        proxy = True

    @property
    def render_area(self):
        return self.client.assigned_to

    @property
    def render_client(self):
        if self.client:
            return mark_safe("<a class='inline-link' href='" + reverse(self.client.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")
        else:
            return ""

    @classmethod
    def success_url(cls):
        return reverse(cls.get_route_name(ViewActionEnum.Manage))

    def save(self, *args, **kwargs):
        super().save(*args,**kwargs)
        process_breakdown = ProcessBreakdown()
        process_breakdown.approval_status = ApprovalStatusEnum.Created
        process_breakdown.process_level = DamageNSMApproval.__name__
        process_breakdown.description = "Pending damage product created by National Sales Manager"
        process_breakdown.save()
        self.process_breakdown.add(process_breakdown)
        return self

    def approve_to(self, cls=None, *args,**kwargs):
        user = kwargs.get('user')
        damage_approved_model = get_model('foodex','ApprovedDamageProducts')
        worth_value = self.value
        self.client.available_credit += Decimal(worth_value)
        self.client.save()
        self.last_updated_by = user
        self.type = damage_approved_model.__name__
        self.save()
        process_breakdown = ProcessBreakdown()
        process_breakdown.approval_status = ApprovalStatusEnum.Approved
        process_breakdown.process_level = DamageNSMApproval.__name__
        process_breakdown.description = "Pending damage product approved by National Sales Manager"
        process_breakdown.save()
        self.process_breakdown.add(process_breakdown)
        return self

    def reject_to(self, cls=None, *args,**kwargs):
        user = kwargs.get('user')
        rejected_damage_product_model = get_model('foodex','RejectedDamageProducts')
        self.last_updated_by = user
        self.type = rejected_damage_product_model.__name__
        self.save()

        c_inventory_class = get_model('foodex', 'ClientInventory')
        #print(type)
        c_invs = c_inventory_class.objects.filter(product=self.product, assigned_to=self.client)
        if c_invs.exists():
            c_inv = c_invs.first()
            c_inv.stock = c_inv.stock + self.quantity
            c_inv.save()

        process_breakdown = ProcessBreakdown()
        process_breakdown.approval_status = ApprovalStatusEnum.Rejected
        process_breakdown.process_level = DamageNSMApproval.__name__
        process_breakdown.description = "Pending damage product rejected by National Sales Manager"
        process_breakdown.save()
        self.process_breakdown.add(process_breakdown)
        return self

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Approve,ViewActionEnum.Reject]

    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Delete',
                action='delete',
                title="Click to remove this item",
                icon='icon-remove',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Delete),
                classes='manage-action all-action confirm-action',
                parent=None
            ),
            dict(
                name='Approve',
                action='approve',
                icon='fbx-rightnav-tick',
                classes = 'manage-action all-action confirm-action',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Approve)
            ),
            dict(
                name='Reject',
                action='reject',
                icon='fbx-rightnav-cancel',
                classes = 'manage-action all-action confirm-action',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Reject)
            )
        ]

    @classmethod
    def table_columns(cls):
        return 'code', 'render_area', 'render_client', 'product', 'quantity:quantity_damaged', 'value', 'date_created:Created', 'created_by'

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'client', 'product', 'address', 'value', 'date_created']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['created_on'] = d.pop('date_created')
        d['quantity_damaged'] = self.quantity
        d['area'] = self.render_area
        d['approved_by'] = self.last_updated_by
        d['created_by'] = self.created_by

        return d
