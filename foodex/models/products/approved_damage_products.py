from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import save_audit_log, decorate, is_object_context
from blackwidow.sales.models.products.primary_damage_products import PrimaryDamagedProducts
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from foodex.models.products.damage_nsm_approval import DamageNSMApproval

__author__ = 'Sohel'

@decorate(save_audit_log, is_object_context,
          expose_api("approved-damage-products"),
          route(route='approved-damage-products', module=ModuleEnum.Execute, group_order=3, item_order=3, display_name='Approved damage', group='Credits & payments(clients)'))
class ApprovedDamageProducts(PrimaryDamagedProducts):
    class Meta:
        proxy = True

    @classmethod
    def get_manage_buttons(cls):
        return []

    @property
    def render_area(self):
        return self.client.assigned_to

    @property
    def render_damages_approved_by_nsm(self):
        all_process_list = self.process_breakdown.all().reverse()
        #print(all_process_list[len(all_process_list) - 1].process_level)
        #print([p.process_level for p in all_process_list])
        if all_process_list:
            last_process_breakdown = all_process_list[len(all_process_list) - 1]
            if last_process_breakdown.process_level == DamageNSMApproval.__name__:
                return "Approved"
        return "Not Approved"

    @property
    def render_approved_by_nsm(self):
        return self.last_updated_by

    @property
    def render_client(self):
        if self.client:
            return mark_safe("<a class='inline-link' href='" + reverse(self.client.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")
        else:
            return ""

    @classmethod
    def table_columns(cls):
        return 'code', 'render_area', 'render_client', 'product', 'quantity:quantity_damaged', 'value', 'render_approved_by_nsm', 'date_created:Originally Created on', 'created_by:Originally Created by'

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'client', 'product', 'address', 'value']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['quantity_damaged'] = self.quantity
        d['area'] = self.render_area
        d['approved_by_nsm'] = self.render_approved_by_nsm
        d['originally_created_by'] = self.created_by
        d['originally_created_on'] = self.render_timestamp(self.date_created)

        return d
