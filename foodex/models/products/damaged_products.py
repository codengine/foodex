from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from rest_framework import serializers
from django.db.models.loading import get_model
from blackwidow.core.models import Client
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.process.process_breakdown import ProcessBreakdown, ApprovalStatusEnum
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import save_audit_log, decorate, is_object_context
from blackwidow.sales.models.products.primary_damage_products import PrimaryDamagedProducts
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum
from blackwidow.sales.models.products.product import Product

__author__ = 'Sohel'


@decorate(save_audit_log, is_object_context,
          expose_api("primary-damage-products"),
          route(route='primary-damage-products', module=ModuleEnum.Execute, group_order=3, item_order=1,
                display_name='Pending damage', group='Credits & payments(clients)'))
class PrimaryDamagedProducts(PrimaryDamagedProducts):
    class Meta:
        proxy = True

    @property
    def render_area(self):
        return self.client.assigned_to

    @property
    def render_client(self):
        if self.client:
            return mark_safe(
                "<a class='inline-link' href='" + reverse(self.client.get_route_name(ViewActionEnum.Details),
                                                          kwargs={'pk': self.client.pk}) + "' >" + str(
                    self.client) + "</a>")
        else:
            return ""

    @classmethod
    def success_url(cls):
        return reverse(cls.get_route_name(ViewActionEnum.Manage))

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # process_breakdown = ProcessBreakdown()
        # process_breakdown.approval_status = ApprovalStatusEnum.Created
        # process_breakdown.process_level = "created"
        # process_breakdown.description = "Pending damage product created"
        # process_breakdown.save()
        # self.process_breakdown.add(process_breakdown)

    @classmethod
    def get_serializer(cls):
        from blackwidow.sales.models import ProductPriceEnum
        ss = OrganizationDomainEntity.get_serializer()

        class Serializer(ss):
            product = serializers.PrimaryKeyRelatedField(required=False, queryset=Product.objects.all())
            quantity = serializers.IntegerField(default=0)
            client = serializers.PrimaryKeyRelatedField(required=False, queryset=Client.objects.all())

            def create(self, validated_data):
                # print("Inside create")
                product = validated_data['product']
                product_unit_price = product.prices.filter(
                    price__name=ProductPriceEnum.gross_cust_price.value["name"]).first().value
                value = int(validated_data['quantity']) * product_unit_price

                c_inventory_class = get_model('foodex', 'ClientInventory')
                # print(type)
                c_inv, result = c_inventory_class.objects.get_or_create(product=validated_data['product'],
                                                                        assigned_to=validated_data['client'])
                if result:
                    c_inv.stock = 0
                c_inv.stock = c_inv.stock - int(validated_data['quantity'])
                c_inv.save()

                obj = super().create(validated_data)
                obj.value = value
                obj.save()

                process_breakdown = ProcessBreakdown()
                process_breakdown.approval_status = ApprovalStatusEnum.Created
                process_breakdown.process_level = "created"
                process_breakdown.description = "Pending damage product created"
                process_breakdown.save()
                obj.process_breakdown.add(process_breakdown)

                return obj

            class Meta(ss.Meta):
                model = cls
                depth = 1

        return Serializer

    def approve_to(self, *args, **kwargs):
        user = kwargs.get('user')
        damage_nsm_approval_model = get_model('foodex', 'DamageNSMApproval')
        self.last_updated_by = user
        self.type = damage_nsm_approval_model.__name__
        self.save()
        process_breakdown = ProcessBreakdown()
        process_breakdown.approval_status = ApprovalStatusEnum.Approved
        process_breakdown.process_level = PrimaryDamagedProducts.__name__
        process_breakdown.description = "Pending damage product request approved by the area manager"
        process_breakdown.save()
        self.process_breakdown.add(process_breakdown)
        return self

    def reject_to(self, *args, **kwargs):
        user = kwargs.get('user')
        rejected_damage_product_model = get_model('foodex', 'RejectedDamageProducts')
        self.last_updated_by = user
        self.type = rejected_damage_product_model.__name__
        self.save()

        c_inventory_class = get_model('foodex', 'ClientInventory')
        # print(type)
        c_invs = c_inventory_class.objects.filter(product=self.product, assigned_to=self.client)
        if c_invs.exists():
            c_inv = c_invs.first()
            c_inv.stock = c_inv.stock + self.quantity
            c_inv.save()

        process_breakdown = ProcessBreakdown()
        process_breakdown.approval_status = ApprovalStatusEnum.Rejected
        process_breakdown.process_level = PrimaryDamagedProducts.__name__
        process_breakdown.description = "Pending damage product request rejected by the area manager"
        process_breakdown.save()
        self.process_breakdown.add(process_breakdown)
        return self

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create, ViewActionEnum.Approve, ViewActionEnum.Reject]

    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Delete',
                action='delete',
                title="Click to remove this item",
                icon='icon-remove',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Delete),
                classes='manage-action all-action confirm-action',
                parent=None
            ),
            dict(
                name='Approve',
                action='approve',
                icon='fbx-rightnav-tick',
                classes='manage-action all-action confirm-action',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Approve)
            ),
            dict(
                name='Reject',
                action='reject',
                icon='fbx-rightnav-cancel',
                classes='manage-action all-action confirm-action',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Reject)
            )
        ]

    @classmethod
    def table_columns(cls):
        return 'code', 'render_area', 'render_client', 'product', 'quantity:quantity_damaged', 'value', 'date_created:Created On', 'created_by'

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'client', 'product', 'address', 'value', 'date_created']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['created_on'] = d.pop('date_created')
        d['quantity_damaged'] = self.quantity
        d['area'] = self.render_area
        d['created_by'] = self.created_by

        return d
