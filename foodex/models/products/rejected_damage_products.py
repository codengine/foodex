from django.core.urlresolvers import reverse
from django.db.models.loading import get_model
from django.utils.safestring import mark_safe
from blackwidow.core.process.process_breakdown import ProcessBreakdown, ApprovalStatusEnum
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import save_audit_log, decorate, is_object_context
from blackwidow.sales.models.products.primary_damage_products import PrimaryDamagedProducts
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Sohel'

@decorate(save_audit_log, is_object_context,
          expose_api("rejected-damage-products"),
          route(route='rejected-damage-products', module=ModuleEnum.Execute, group_order=3, item_order=5, display_name='Rejected damage', group='Credits & payments(clients)'))
class RejectedDamageProducts(PrimaryDamagedProducts):
    class Meta:
        proxy = True

    @classmethod
    def get_button_title(cls, button=ViewActionEnum.Details):
        if button == ViewActionEnum.Approve:
            return "Reverse Rejection"
        return button.value

    @classmethod
    def success_url(cls):
        return reverse(cls.get_route_name(ViewActionEnum.Manage))

    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Reverse Rejection',
                action='approve',
                icon='fbx-rightnav-tick',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Approve),
                classes='manage-action all-action confirm-action',
                parent=None
            ),]

    def approve_to(self, cls=None, *args, **kwargs):
        c_user = kwargs.get('user', None)

        if cls is None:
            p_breakdown = self.process_breakdown.all().order_by('-last_updated').first()
            cls_name = p_breakdown.process_level

        self.type = cls_name
        if c_user:
            self.last_updated_by = c_user

        self.save()

        c_inventory_class = get_model('foodex', 'ClientInventory')
        #print(type)
        c_invs = c_inventory_class.objects.filter(product=self.product, assigned_to=self.client)
        if c_invs.exists():
            c_inv = c_invs.first()
            c_inv.stock = c_inv.stock - self.quantity
            c_inv.save()

        process_breakdown = ProcessBreakdown()
        process_breakdown.approval_status = ApprovalStatusEnum.ReverseRejected
        process_breakdown.process_level = RejectedDamageProducts.__name__
        process_breakdown.description = "Rejected damage product reverse rejection done"
        process_breakdown.save()
        self.process_breakdown.add(process_breakdown)

        return self

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Approve]

    @property
    def render_area(self):
        return self.client.assigned_to

    @property
    def render_client(self):
        if self.client:
            return mark_safe("<a class='inline-link' href='" + reverse(self.client.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")
        else:
            return ""

    @classmethod
    def table_columns(cls):
        return 'code', 'render_area', 'render_client', 'product', 'quantity:quantity_damaged', 'value', 'date_created:Originally Created on', 'created_by: Originally Created By', 'last_updated_by:Rejected By'


    @property
    def details_config(self):
        dic = super().details_config

        custom_list = ['code', 'client', 'product', 'value']
        for key in dic:
            if key in custom_list:
                pass
            else:
                del dic[key]
        dic['originally_created_on'] = self.render_timestamp(self.date_created)
        dic['quantity_damaged'] = self.quantity
        dic['area'] = self.render_area
        dic['originally_created_by'] = self.created_by
        dic['rejected_by'] = self.last_updated_by

        return dic
