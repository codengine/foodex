from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.loading import get_model
from django.utils.safestring import mark_safe
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, is_object_context
from blackwidow.sales.models.invoice.invoice import Invoice
from blackwidow.sales.models.products.product import Product
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum

__author__ = 'Sohel'


@decorate(is_object_context,
          route(route='primary-sales-return', module=ModuleEnum.Execute, group_order=3, item_order=12, display_name='Sales Return', group='Credits & payments(clients)'))
class PrimarySalesReturn(OrganizationDomainEntity):
    invoice = models.ForeignKey(Invoice, null=True)
    product = models.ForeignKey(Product, null=True)
    quantity = models.IntegerField(default=0)

    @classmethod
    def get_manage_buttons(cls):
        return []

    @classmethod
    def get_object_inline_buttons(cls):
        return []

    @property
    def render_invoice(self):
        if self.invoice:
            OpenInvoice = get_model("foodex", "OpenInvoice")
            ClosedInvoice = get_model("foodex", "ClosedInvoice")
            DraftInvoice = get_model("foodex", "DraftInvoice")
            if self.invoice.type == OpenInvoice.__name__:
                return mark_safe("<a class='inline-link' href='" + reverse(OpenInvoice.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.invoice.pk}) + "' >" + str(self.invoice) + "</a>")
            elif self.invoice.type == ClosedInvoice.__name__:
                return mark_safe("<a class='inline-link' href='" + reverse(ClosedInvoice.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.invoice.pk}) + "' >" + str(self.invoice) + "</a>")
            elif self.invoice.type == DraftInvoice.__name__:
                return mark_safe("<a class='inline-link' href='" + reverse(DraftInvoice.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.invoice.pk}) + "' >" + str(self.invoice) + "</a>")
        else:
            return ""

    @classmethod
    def table_columns(cls):
        return "code", "render_invoice", "product", "quantity:Stock Return","last_updated"
