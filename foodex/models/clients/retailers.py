from django.db import transaction
from django.db.models.loading import get_model
from django.db.models.query_utils import Q
from django import forms
from django.core.urlresolvers import reverse
from rest_framework import serializers
from blackwidow.core.mixins.fieldmixin import GenericModelChoiceField
from blackwidow.core.models import ExporterConfig, ExporterColumnConfig, InfrastructureUnit

from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.clients.client_assignment import ClientAssignment
from blackwidow.core.models.common.contactaddress import ContactAddress
from blackwidow.core.models.common.country import Country
from blackwidow.core.models.common.division import Division
from blackwidow.core.models.common.phonenumber import PhoneNumber
from blackwidow.core.models.common.state import State
from blackwidow.core.models.common.upazila import Upazila
from blackwidow.core.models.common.week_day import WeekDay
from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
from blackwidow.core.models.config.importer_config import ImporterConfig
from blackwidow.core.models.log.logs import ErrorLog
from blackwidow.core.models.organizations.organization import Organization
from blackwidow.core.models.users.user import WebUser
from blackwidow.engine.decorators.enable_export import enable_export
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context, get_models_with_decorator
from blackwidow.core.models.manufacturers.manufacturer import Manufacturer
from blackwidow.sales.models.common.product_price import ProductPrice
from blackwidow.sales.models.products.product import Product
from config.apps import INSTALLED_APPS
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum
from blackwidow.core.models.contracts.base import RequestSource
from blackwidow.core.managers.modelmanager import DomainEntityModelManager
from blackwidow.core.mixins.formmixin import GenericFormMixin
# r
from multiprocessing.synchronize import Lock

__author__ = 'Mahmud'


@decorate(expose_api('retailers'), enable_import, enable_export,
          save_audit_log, is_object_context,
          route(route='retailers', group='Customers', group_order=1, module=ModuleEnum.Administration,
                display_name="Secondary Retailer", item_order=3))
class Retailers(Client):
    objects = DomainEntityModelManager(filter={'type': 'Retailers', 'is_approved': True})


    @property
    def contact_person_name(self):
        return self.contact_person.name

    @property
    def mobile_number(self):
        return self.contact_person.phone

    @property
    def render_distributor(self):
        retailer_routes = get_model("foodex", "Route").objects.filter(
            assigned_clients__in=ClientAssignment.objects.filter(client_type=Retailers.__name__, clients__in=[self]))
        if retailer_routes:
            distributor = retailer_routes[0].parent_client
            return distributor
        return None


    @property
    def retailer_distributor(self):
        retailer_routes = get_model("foodex", "Route").objects.filter(
            assigned_clients__in=ClientAssignment.objects.filter(client_type=Retailers.__name__, clients__in=[self]))
        if retailer_routes:
            distributor = retailer_routes[0].parent_client
            return distributor.name
        return ""

    @property
    def retailer_distributor_code(self):
        retailer_routes = get_model("foodex", "Route").objects.filter(
            assigned_clients__in=ClientAssignment.objects.filter(client_type=Retailers.__name__, clients__in=[self]))
        if retailer_routes:
            distributor = retailer_routes[0].parent_client
            return distributor.code
        return ""

    @property
    def retailer_street(self):
        return self.address.street

    @property
    def retailer_division(self):
        return self.address.division

    @property
    def retailer_district(self):
        return self.address.state

    @property
    def retailer_union_city(self):
        return self.address.city

    @property
    def retailer_province_upazilla(self):
        return self.address.province

    @property
    def retailer_route_name(self):
        retailer_routes = get_model("foodex", "Route").objects.filter(
            assigned_clients__in=ClientAssignment.objects.filter(client_type=Retailers.__name__, clients__in=[self]))
        if retailer_routes.exists():
            return retailer_routes.first().name
        return ""

    @property
    def retailer_service_day(self):
        retailer_routes = get_model("foodex", "Route").objects.filter(
            assigned_clients__in=ClientAssignment.objects.filter(client_type=Retailers.__name__, clients__in=[self]))
        if retailer_routes.exists():
            return retailer_routes.first().service_days.all().first() if retailer_routes.first().service_days.exists() else ""
        return ""

    @property
    def render_distributor_id(self):
        return self.render_distributor.pk if self.render_distributor else -1

    @property
    def render_area(self):
        return self.render_distributor.assigned_to if self.render_distributor else None


    @property
    def render_assign_route(self):
        retailer_routes = get_model("foodex", "Route").objects.filter(
            assigned_clients__in=ClientAssignment.objects.filter(client_type=Retailers.__name__, clients__in=[self]))
        if retailer_routes.exists():
            return retailer_routes.first()
        return None

    @classmethod
    def filter_query(cls, query_set, custom_search_fields=[]):
        from django.db.models import Q
        from functools import reduce
        import operator
        from foodex.models import Route

        for key, value in custom_search_fields:
            if key.startswith("_search_route"):
                try:
                    route_object = get_model("foodex", "Route").objects.get(pk=int(value))
                    query_set = route_object.assigned_clients.first().clients.filter(
                        pk__in=[item.pk for item in query_set])
                except:
                    query_set = query_set.model.objects.none()
            elif key.startswith("__search__areas"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(parent_client__assigned_to__name=v))
                    retailers_pk = []
                    routes = Route.objects.filter(reduce(operator.or_, or_list))
                    for route in routes:
                        client_assigns = route.assigned_clients.filter(client_type=Retailers.__name__)
                        if client_assigns.exists():
                            retailers_pk += client_assigns.first().clients.values_list('pk', flat=True)
                    query_set = query_set.filter(pk__in=retailers_pk)
                except:
                    pass
            elif key.startswith("__search__distributors"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(parent_client__name=v))
                    retailers_pk = []
                    routes = Route.objects.filter(reduce(operator.or_, or_list))
                    for route in routes:
                        client_assigns = route.assigned_clients.filter(client_type=Retailers.__name__)
                        if client_assigns.exists():
                            retailers_pk += client_assigns.first().clients.values_list('pk', flat=True)
                    query_set = query_set.filter(pk__in=retailers_pk)
                except:
                    pass
            elif key.startswith("__search__routes"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(name__icontains=v))
                    retailers_pk = []
                    routes = Route.objects.filter(reduce(operator.or_, or_list))
                    for route in routes:
                        client_assigns = route.assigned_clients.filter(client_type=Retailers.__name__)
                        if client_assigns.exists():
                            retailers_pk += client_assigns.first().clients.values_list('pk', flat=True)
                    query_set = query_set.filter(pk__in=retailers_pk)
                except Exception as e:
                    pass
        return query_set

    @classmethod
    def get_url_by_name(cls, name):
        if name == 'render_area':
            return '__search__areas'
        elif name == 'render_distributor':
            return '__search__distributors'
        elif name == 'render_assign_route':
            return '__search__routes'
        return None


    @classmethod
    def table_columns(cls):
        return 'code', 'name', 'render_area', 'render_distributor', 'render_assign_route', 'created_by', 'date_created', 'last_updated'

    @classmethod
    def get_serializer(cls):
        ss = Client.get_serializer()

        class Serializer(ss):
            assigned_route = serializers.SerializerMethodField("route_assigned")
            manufacturer = serializers.PrimaryKeyRelatedField(required=False, queryset=Manufacturer.objects.all())

            def route_assigned(self, obj):
                retailer_routes = get_model("foodex", "Route").objects.filter(
                    assigned_clients__in=ClientAssignment.objects.filter(client_type=Retailers.__name__,
                                                                         clients__in=[obj]))
                if retailer_routes.exists():
                    return retailer_routes.first().pk
                return 0

            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
                # print(self.fields)
                self.fields.pop("financial_information")
                self.fields.pop("assigned_employee")

            def create(self, validated_data):
                commit = True
                route = validated_data["assigned_to"]
                distributor = route.parent_client
                assigned_clients = route.assigned_clients.filter(client_type=Retailers.__name__)

                validated_data.update({
                    'is_approved': False,
                    'parent': distributor,
                    'type': Retailers.__name__,
                    'request_source': RequestSource.MobileDevice.value
                })
                obj = super().create(validated_data)

                if assigned_clients:
                    route.assigned_clients.all().first().clients.add(obj)
                else:
                    client_assignment = ClientAssignment()
                    client_assignment.client_type = Retailers.__name__
                    client_assignment.save(commit)
                    client_assignment.clients.add(obj)
                    client_assignment.save(commit)
                    route.assigned_clients.add(client_assignment)
                route.save(commit)

                return obj

            class Meta(ss.Meta):
                model = cls

        return Serializer

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'name', 'address', 'credit_limit', 'assigned_to', 'date_created', 'last_updated']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['assign_route'] = d.pop('assigned_to')
        d['distributor'] = self.render_distributor
        d['area'] = self.render_area
        d['created_by'] = self.created_by
        d['last_updated_by'] = self.last_updated_by
        d['location'] = self.address.location if self.address.location else "N/A"
        d['phone'] = self.phone_number.phone
        d['contract_address'] = self.address

        return d


    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Edit',
                action='edit',
                icon='fbx-rightnav-edit',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit)
            )
        ]

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create, ViewActionEnum.Edit, ViewActionEnum.Delete, ViewActionEnum.AdvancedImport,
                ViewActionEnum.AdvancedExport]


    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        importer_config, created = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if created or importer_config.columns.count() == 0:
            importer_config.save(**kwargs)
        else:
            importer_config.columns.all().delete()
        importer_config.starting_row = 4
        importer_config.save()
        columns = [
            ImporterColumnConfig(column=0, column_name='Code', property_name='code', ignore=False),
            ImporterColumnConfig(column=1, column_name='Retailer Name', property_name='name', ignore=False),
            ImporterColumnConfig(column=2, column_name='Contact person', property_name='contact_person_name',
                                 ignore=False),
            ImporterColumnConfig(column=3, column_name='Mobile No', property_name='mobile_number', ignore=False),
            ImporterColumnConfig(column=4, column_name='Dealer Code', property_name='distributor_code', ignore=False),
            ImporterColumnConfig(column=5, column_name='Dealer name', property_name='distributor_name', ignore=False),
            ImporterColumnConfig(column=6, column_name='Route name', property_name='route_name', ignore=False),
            ImporterColumnConfig(column=7, column_name='Street/village', property_name='street_village', ignore=False),
            ImporterColumnConfig(column=8, column_name='Union/city', property_name='union_city', ignore=False),
            ImporterColumnConfig(column=9, column_name='Province/upzila', property_name='province_upazilla',
                                 ignore=False),
            ImporterColumnConfig(column=10, column_name='District', property_name='district_name', ignore=False),
            ImporterColumnConfig(column=11, column_name='Division', property_name='division_name', ignore=False),
            ImporterColumnConfig(column=12, column_name='Service Day', property_name='service_day', ignore=False)
        ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        with transaction.atomic():
            organization = user.organization

            ###Create Country.
            country_name = "Bangladesh"
            country_objects = Country.objects.filter(name=country_name)
            if country_objects.exists():
                country_object = country_objects.first()
            else:
                ErrorLog.log("Exception in retailer import. Country does not exist with name %s. Skipping......Data provided: %s" % (country_name, data), stacktrace='')
                return None

            ###Create Division.
            division_name = data['11']
            division_objects = Division.objects.filter(name=division_name)
            if division_objects.exists():
                division_object = division_objects.first()
            else:
                ErrorLog.log("Exception in retailer import. Division does not exist with name %s. Skipping......Data provided: %s" % (division_name, data), stacktrace='')
                return None

            ###Create District.
            district_name = data['10']
            district_objects = State.objects.filter(name=district_name)
            if district_objects.exists():
                district_object = district_objects.first()
            else:
                ErrorLog.log("Exception in retailer import. District does not exist with name %s. Skipping......Data provided: %s" % (district_name, data), stacktrace='')
                return None

            ###Create Province/Upazilla.
            upazila_name = data['9']
            upazila_objects = Upazila.objects.filter(name=upazila_name)
            if upazila_objects.exists():
                upazila_object = upazila_objects.first()
            else:
                ErrorLog.log("Exception in retailer import. Upazila does not exist with name %s. Skipping......Data provided: %s" % (upazila_name, data), stacktrace='')
                return None

            distributor_objects = get_model('foodex', 'Distributor').objects.filter(
                code=data['4'])  # (Q(code=data['4']) | Q(name=data['5']))
            if distributor_objects.exists():
                distributor_object = distributor_objects.first()
            else:
                ErrorLog.log('Distributor with code %s not found. Skipping...', data['4'])
                return None
            ###Create Retailer.
            retailer_name = data['1']
            retailer_object = None
            contact_address = None
            phone_number = str(data['3'])
            contact_person = None

            phone_number = phone_number.replace(' ', '')

            if not data['0']:
                retailer_object = Retailers()
                contact_address = ContactAddress()
                contact_person = WebUser()
            else:
                retailers = Retailers.objects.filter(code=data[
                    '0'])  # (Q(code=data['0']) | Q(name=retailer_name, parent=distributor_object, contact_person__phone__phone=phone_number))
                if retailers.exists():
                    retailer_object = retailers.first()
                    contact_address = retailer_object.address
                    contact_person = retailer_object.contact_person
                else:
                    ErrorLog.log('Retailer with code %s not found. Skipping...', data['0'])
                    return None

            if not retailer_name:
                ErrorLog.log('No name found in the input for retailer to be created.')
                return None

            retailer_object.name = retailer_name
            retailer_object.parent = distributor_object
            retailer_object.organization = organization
            retailer_object.created_by = user
            retailer_object.is_approved = True

            contact_address.street = data['7']
            contact_address.city = data['8']
            contact_address.province = upazila_object.name
            contact_address.state = district_object
            contact_address.division = division_object
            contact_address.country = country_object
            contact_address.save()
            retailer_object.address = contact_address

            contact_person.name = data['2']
            if not contact_person.name:
                contact_person.name = 'Unknown'
            contact_person.address = contact_address
            if contact_person.phone:
                phone_number_object = contact_person.phone
            else:
                phone_number_object = PhoneNumber()
                phone_number_object.is_primary = True
                phone_number_object.is_own = True
            phone_number_object.phone = data['3']
            phone_number_object.save()
            contact_person.phone = phone_number_object
            contact_person.save()
            retailer_object.contact_person = contact_person
            retailer_object.phone_number = contact_person.phone

            retailer_object.save()

            route_objects = get_model('foodex', 'Route').objects.filter(name=data['6'],
                                                                        parent_client=distributor_object)
            if route_objects.exists():
                service_day = WeekDay.objects.get(name=data['12'])
                route_object = route_objects.first()
                if not route_object.service_days.filter(name=data['12']).exists():
                    route_object.service_days.add(service_day)
                    route_object.save()
            else:
                service_day = WeekDay.objects.get(name=data['12'])
                route_object = get_model('foodex', 'Route')()
                route_object.name = data['6']
                route_object.organization = organization
                route_object.parent_client = distributor_object
                route_object.save()

            route_object.service_days.add(service_day)

            assigned_clients = route_object.assigned_clients.filter(client_type=Retailers.__name__)
            if assigned_clients.exists():
                assigned_clients.all().first().clients.add(retailer_object)
            else:
                client_assignment = ClientAssignment()
                client_assignment.client_type = Retailers.__name__
                client_assignment.organization = organization
                client_assignment.save()
                client_assignment.clients.add(retailer_object)
                client_assignment.save()
                route_object.assigned_clients.add(client_assignment)
            route_object.save()

            return retailer_object.pk

    @classmethod
    def get_export_dependant_fields(self):
        class AdvancedExportDependentForm(GenericFormMixin):
            def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
                super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
                self.fields['assigned_to___1__id'] = GenericModelChoiceField(label='Area', empty_label="Select Area",
                                                                             required=False,
                                                                             queryset=InfrastructureUnit.objects.filter(
                                                                                 type="Area"), widget=forms.Select(
                        attrs={'class': 'select2'}))
                self.fields['parent___1__id'] = GenericModelChoiceField(label='Distributor',
                                                                        empty_label="Select Distributor",
                                                                        required=False, queryset=Client.objects.all(),
                                                                        widget=forms.TextInput(
                                                                            attrs={'class': 'select2-input',
                                                                                   'width': '220',
                                                                                   'data-depends-on': 'assigned_to___1__id',
                                                                                   'data-depends-property': 'assigned_to:'
                                                                                                            'id',
                                                                                   'data-url': reverse(
                                                                                       get_model("foodex",
                                                                                                 "Distributor").get_route_name(
                                                                                           action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))

        return AdvancedExportDependentForm


    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        if not organization:
            organization = Organization.objects.all().first()
        exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
        if not exporter_configs.exists():
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.starting_row = 1
            exporter_config.save(**kwargs)
        else:
            for e in exporter_configs:
                e.delete()
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.save(**kwargs)

        columns = [
            ExporterColumnConfig(column=0, column_name='Code', property_name='code', ignore=False),
            ExporterColumnConfig(column=1, column_name='Retailer Name', property_name='name', ignore=False),
            ExporterColumnConfig(column=2, column_name='Contact person', property_name='contact_person_name',
                                 ignore=False),
            ExporterColumnConfig(column=3, column_name='Mobile No', property_name='mobile_number', ignore=False),
            ExporterColumnConfig(column=4, column_name='Dealer Code', property_name='retailer_distributor_code',
                                 ignore=False),
            ExporterColumnConfig(column=5, column_name='Dealer name', property_name='retailer_distributor',
                                 ignore=False),
            ExporterColumnConfig(column=6, column_name='Route name', property_name='retailer_route_name', ignore=False),
            ExporterColumnConfig(column=7, column_name='Street/village', property_name='retailer_street', ignore=False),
            ExporterColumnConfig(column=8, column_name='Union/city', property_name='retailer_union_city', ignore=False),
            ExporterColumnConfig(column=9, column_name='Province/upzila', property_name='retailer_province_upazilla',
                                 ignore=False),
            ExporterColumnConfig(column=10, column_name='District', property_name='retailer_district', ignore=False),
            ExporterColumnConfig(column=11, column_name='Division', property_name='retailer_division', ignore=False),
            ExporterColumnConfig(column=12, column_name='Service Day', property_name='retailer_service_day',
                                 ignore=False)
        ]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)
        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):
        # for column in columns:
        # workbook.cell(row=row_number, column=column.column + 1).value = str(getattr(self, column.property_name))
        return self.pk, row_number + 1

    @classmethod
    def finalize_export(cls, workbook=None, row_number=None, query_set=None, **kwargs):
        return workbook

    @classmethod
    def initialize_export(cls, workbook=None, columns=None, row_number=None, query_set=None, **kwargs):
        for column in columns:
            workbook.cell(row=1, column=column.column + 1).value = column.column_name
        row_number += 1

        query_params = kwargs.get('query_params')
        if query_params:
            infrastructure_unit_id = query_params.get("assigned_to:id")
            client_id = query_params.get("parent:id")

            try:
                infrastructure_unit_id = int(infrastructure_unit_id)
                client_id = int(client_id)
            except Exception:
                infrastructure_unit_id = -1
                client_id = -1
            if client_id > 0:
                retailer_objects = Retailers.objects.filter(pk__in=[x.id for x in
                                                                    get_model("foodex", "Distributor").objects.get(
                                                                        pk=client_id).client_set.all()], deleted_level=0)
                print(len(retailer_objects))
                for retailer in retailer_objects:
                    for column in columns:
                        try:
                            column_value = ''
                            if column.property_name == 'code':
                                column_value = str(retailer.code)
                            elif column.property_name == 'name':
                                column_value = str(retailer.name)
                            elif column.property_name == 'contact_person_name':
                                column_value = str(retailer.contact_person.name)
                            elif column.property_name == 'mobile_number':
                                column_value = str(retailer.phone_number.phone)
                            elif column.property_name == 'retailer_distributor_code':
                                column_value = str(retailer.parent.code)
                            elif column.property_name == 'retailer_route_name':
                                column_value = str(retailer.retailer_route_name)
                            elif column.property_name == 'retailer_street':
                                column_value = str(retailer.retailer_street)
                            elif column.property_name == 'retailer_union_city':
                                column_value = str(retailer.retailer_union_city)
                            elif column.property_name == 'retailer_province_upazilla':
                                column_value = str(retailer.retailer_province_upazilla)
                            elif column.property_name == 'retailer_district':
                                column_value = str(retailer.retailer_district)
                            elif column.property_name == 'retailer_division':
                                column_value = str(retailer.retailer_division)
                            elif column.property_name == 'retailer_service_day':
                                column_value = str(retailer.retailer_service_day)
                        except:
                            pass
                        finally:
                            workbook.cell(row=row_number, column=column.column + 1).value = column_value
                    row_number += 1

        return workbook, row_number

    @classmethod
    def get_dependent_field_list(cls):
        return ['address', 'financial_information', 'custom_fields']

    def save(self, *args, organization=None, **kwargs):
        self.type = Retailers.__name__
        super().save(*args, **kwargs)

    class Meta:
        proxy = True