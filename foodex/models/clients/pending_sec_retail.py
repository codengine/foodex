from collections import OrderedDict
from django.core.urlresolvers import reverse
from django.db.models.loading import get_model
from blackwidow.core.models import ConsoleUser
from rest_framework import serializers
from blackwidow.core.models.clients.client import Client
from foodex.models.clients.distributor import Distributor
from foodex.models.clients.retailers import Retailers
from foodex.models.users.service_persons import ServicePerson
from foodex.models.infrastructure.routes import Route
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.core.models.manufacturers.manufacturer import Manufacturer
from blackwidow.core.models.clients.client_assignment import ClientAssignment
from blackwidow.core.models.structure.user_assignment import InfrastructureUserAssignment
from blackwidow.sales.decorators.model_decorators import enable_individual_assignment
from blackwidow.sales.models.common.product_price_config import ProductPriceConfig
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum
from blackwidow.core.managers.modelmanager import DomainEntityModelManager

__author__ = 'sohel'


@decorate(expose_api('pending_sec_retailers'),is_object_context,
          save_audit_log, enable_individual_assignment(models=[ProductPriceConfig]),
          route(route='pending_sec_retailers', group='Customers', group_order=1, module=ModuleEnum.Administration, display_name="Pending Sec. Retailer", item_order=7))
class PendingRetailerAccount(Client):

    def retailer_routes(self):
        retailer_routes = Route.objects.filter(assigned_clients__in=ClientAssignment.objects.filter(client_type=Retailers.__name__,clients__in=[self]))
        return retailer_routes

    @property
    def render_assigned_employees(self):
        assigned_employee_str = ''
        retailer_routes = self.retailer_routes()
        if retailer_routes:
            retailer_route = retailer_routes[0]
            distributor = retailer_route.parent_client
            if distributor:
                distributors = Distributor.objects.filter(pk=distributor.pk,assigned_employee__in=InfrastructureUserAssignment.objects.filter(role__name=ServicePerson.__name__))
                distributor = distributors[0]
                assigned_employees = distributor.assigned_employee.filter(role__name=ServicePerson.__name__)
                for assigned_employee in assigned_employees:
                    user_objs = assigned_employee.users.all()
                    user_names = []
                    for user in user_objs:
                        user_names += [user.name]
                    assigned_employee_str = ','.join(user_names)
        return assigned_employee_str

    @property
    def render_distributor(self):
        distributor = ''
        retailer_routes = Route.objects.filter(assigned_clients__in=ClientAssignment.objects.filter(client_type=Retailers.__name__,clients__in=[self]))
        if retailer_routes:
            distributor = retailer_routes[0].parent_client
        return distributor

    # @property
    # def render_assigned_employee(self):
    #     return self.assigned_employee.name

    @property
    def render_retailer_name(self):
        return self.name


    @property
    def render_assign_route(self):
        retailer_routes = get_model("foodex", "Route").objects.filter(assigned_clients__in=ClientAssignment.objects.filter(client_type=Retailers.__name__,clients__in=[self]))
        if retailer_routes:
            return retailer_routes[0]
        return None

    @classmethod
    def filter_query(cls,query_set,custom_search_fields=[]):
        from django.db.models import Q
        from functools import reduce
        import operator
        from foodex.models import Route
        for key, value in custom_search_fields:
            if key.startswith("__search__distributors"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(parent_client__name__icontains=v))
                    retailers_pk = []
                    routes = Route.objects.filter(reduce(operator.or_, or_list))
                    for route in routes:
                        client_assigns = route.assigned_clients.filter(client_type=Retailers.__name__)
                        if client_assigns.exists():
                            retailers_pk += client_assigns.first().clients.values_list('pk', flat=True)
                    query_set = query_set.filter(pk__in=retailers_pk)
                except:
                    pass
            elif key.startswith("__search__routes"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(name__icontains=v))
                    retailers_pk = []
                    routes = Route.objects.filter(reduce(operator.or_, or_list))
                    for route in routes:
                        client_assigns = route.assigned_clients.filter(client_type=Retailers.__name__)
                        if client_assigns.exists():
                            retailers_pk += client_assigns.first().clients.values_list('pk', flat=True)
                    query_set = query_set.filter(pk__in=retailers_pk)
                except Exception as e:
                    pass
        return query_set

    @classmethod
    def get_url_by_name(cls, name):
        if name == 'render_retailer_name':
            return 'name'
        elif name == 'render_distributor':
            return '__search__distributors'
        elif name == 'render_assign_route':
            return '__search__routes'
        return None

    @classmethod
    def table_columns(cls):
        return 'code', 'name', 'render_distributor', 'render_assign_route', 'render_assigned_employees', 'date_created'

    def get_distributor(self):
        distributor = None
        retailer_routes = Route.objects.filter(assigned_clients__in=ClientAssignment.objects.filter(client_type=Retailers.__name__,clients__in=[self]))
        if retailer_routes:
            distributor = retailer_routes[0].parent_client
        return distributor

    def get_delete_log_model(self):
        return Retailers.__name__

    @property
    def details_config(self):
        dict = OrderedDict()
        dict['code'] = self.code
        dict['name'] = self.render_retailer_name
        dict['company_name'] = self.company_name
        dict['address'] = self.address
        dict['phone_number'] = self.phone_number
        dict['assigned_to '] = self.assigned_to
        dict['contact_person'] = self.contact_person if self.contact_person else 'N/A'
        dict['date_created'] = self.render_timestamp(self.date_created)
        dict['date_updated'] = self.render_timestamp(self.last_updated)
        dict["distributor"] = self.render_distributor
        dict["location"] = self.address.location if self.address else 'N/A'
        return dict

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Edit, ViewActionEnum.Approve, ViewActionEnum.Delete]


    def approve_to(self, cls=None, *args, **kwargs):
        user = kwargs.get('user', None)

        self.is_approved = True
        if user:
            console_users = ConsoleUser.objects.filter(user=user)
            if console_users.exists():
                self.approved_by_id = console_users.first().pk
        self.save()
        return self

    @classmethod
    def success_url(cls):
        return reverse(cls.get_route_name(ViewActionEnum.Manage))


    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Approve',
                action='approve',
                icon='fbx-rightnav-tick',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Approve),
                classes='manage-action all-action confirm-action',
                parent=None
            ),
            dict(
                name='Edit',
                action='edit',
                icon='fbx-rightnav-edit',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit)
            ),
            dict(
                name='Print',
                action='print',
                icon='fbx-rightnav-print',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Print)
            )
        ]

    @property
    def get_inline_manage_buttons(self):
        return [
            dict(
                name='Edit',
                action='edit',
                title="Click to edit this item",
                icon='icon-pencil',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit),
                classes='all-action',
                parent=None
            ),
            dict(
                name='Details',
                action='view',
                title="Click to view this item",
                icon='icon-eye',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Details),
                classes='all-action ',
                parent=None
            )]

    objects = DomainEntityModelManager(filter={'type': Retailers.__name__, 'is_approved': False})

    @classmethod
    def get_serializer(cls):
        ss = Client.get_serializer()

        class Serializer(ss):
            manufacturer = serializers.PrimaryKeyRelatedField(required=False, queryset=Manufacturer.objects.all())

            # class Meta(ss.Meta):
            #     model = cls
            #     depth = 1
        return Serializer

    def save(self, *args, organization=None, **kwargs):
        self.type = Retailers.__name__
        #self.is_approved = True
        #self.approved_time = Clock.timestamp()
        #u = self.request.user
        #self.approved_by = self.request.user
        self_obj = super().save(*args, **kwargs)
        return self_obj

    class Meta:
        proxy = True
