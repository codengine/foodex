from django.db import transaction
from django.db.models.aggregates import Sum
from django.db.models.query_utils import Q
from blackwidow.core.models.clients.client import Client
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, loads_initial_data, is_object_context
from blackwidow.sales.decorators.model_decorators import enable_individual_assignment
from blackwidow.sales.models.common.product_price_config import ProductPriceConfig
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.invoice.open_invoice import OpenInvoice
from foodex.models.paymentcredits.client_credit import ClientCredit

__author__ = 'Mahmud'


@decorate(expose_api('wholesalers'),
          save_audit_log, is_object_context,  enable_individual_assignment(models=[ProductPriceConfig]),
          route(route='wholesalers', group='Customers', group_order=1, module=ModuleEnum.Administration, display_name="Wholesaler",item_order=6))
class WholeSaler(Client):

    class Meta:
        proxy = True

    @classmethod
    def table_columns(cls):
        return 'code', 'name', 'company_name', 'credit_limit', 'available_credit', 'assigned_to:Area', 'date_created:Created On', 'last_updated'

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'name', 'company_name', 'address', 'credit_limit', 'available_credit', 'assigned_to', 'date_created', 'last_updated']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['area'] = d.pop('assigned_to')
        d['created_on'] = d.pop('date_created')
        d['created_by'] = self.created_by
        d['last_updated_by'] = self.last_updated_by
        d['location'] = self.address.location
        d['contract_address'] = self.contact_person if self.contact_person else 'N/A'

        return d


    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Edit',
                action='edit',
                icon='fbx-rightnav-edit',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit)
            )
        ]

    def save(self, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)

            client_added_new = True if not self.pk else False
            if client_added_new:
                client_credit = ClientCredit()
                client_credit.client = self
                client_credit.organization = self.organization
                client_credit.save()
            else:
                if not ClientCredit.objects.filter(client=self).exists():
                    client_credit = ClientCredit()
                    client_credit.client = self
                    client_credit.organization = self.organization
                    client_credit.save()

    @property
    def tabs_config(self):
        tabs = [TabView(
            title='Incoming Products',
            access_key='incominginventories',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='sales.ClientOnRouteInventory',
            queryset_filter=Q(**{'assigned_to__id': self.pk})
        ), TabView(
            title='Product Inventory',
            access_key='currentinventories',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='sales.ClientCurrentInventory',
            queryset_filter=Q(**{'assigned_to__id': self.pk})
        )]
        return tabs