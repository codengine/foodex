from uuid import uuid4
from django.core.urlresolvers import reverse
from django.db import transaction
from django.db.models.aggregates import Sum
from django.db.models.loading import get_model
from django.db.models.query_utils import Q
from django.utils.safestring import mark_safe
from rest_framework import serializers
from blackwidow.core.models import InfrastructureUnit, CustomFieldValue

from blackwidow.core.models.common.contactaddress import ContactAddress
from blackwidow.core.models.common.country import Country
from blackwidow.core.models.common.division import Division
from blackwidow.core.models.common.emailaddress import EmailAddress
from blackwidow.core.models.common.location import Location
from blackwidow.core.models.common.phonenumber import PhoneNumber
from blackwidow.core.models.common.state import State
from blackwidow.core.models.common.upazila import Upazila
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig
from blackwidow.core.models.config.exporter_config import ExporterConfig
from blackwidow.core.models.contracts.base import CreationFlag
from blackwidow.core.models.log.logs import ErrorLog
from blackwidow.core.models.organizations.organization import Organization
from blackwidow.core.models.roles.role import Role
from blackwidow.core.models.structure.user_assignment import InfrastructureUserAssignment
from blackwidow.core.models.users.user import WebUser, ConsoleUser
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.clients.client_meta import ClientMeta
from blackwidow.core.viewmodels.tabs_config import TabView, TabViewAction
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route, partial_route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
from blackwidow.core.models.config.importer_config import ImporterConfig
from blackwidow.core.models.manufacturers.manufacturer import Manufacturer
from blackwidow.engine.extensions.bw_titleize import bw_titleize
from blackwidow.engine.extensions.clock import Clock
from blackwidow.sales.decorators.model_decorators import enable_individual_assignment
from blackwidow.sales.models.transactions.transaction import TransactionType, PrimaryTransaction
from blackwidow.sales.models.common.product_price import ProductPriceEnum
from blackwidow.sales.models.common.product_price_config import ProductPriceConfig
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.transactions.stock_adjustment import StockAdjustmentTransaction
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from blackwidow.sales.signals.signals import sig_client_added
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum
from multiprocessing.synchronize import Lock
from threading import Thread
from foodex.models.invoice.open_invoice import OpenInvoice
from foodex.models.paymentcredits.client_credit import ClientCredit
from foodex.models.infrastructure.routes import Route

__author__ = 'Mahmud'


@decorate(expose_api('distributors'),is_object_context,
          enable_import, save_audit_log, enable_individual_assignment(models=[ProductPriceConfig]),
          route(route='distributors', group='Customers', group_order= 1, module=ModuleEnum.Administration, display_name="Distributor", item_order=1),
          partial_route(relation=['normal', 'inverted'], models=[Route, InfrastructureUserAssignment, ConsoleUser, CustomFieldValue]))
class Distributor(Client):

    @classmethod
    def prefix(cls):
        return 'DIST'

    @property
    def render_code(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.pk}) + "' >" + self.code + "</a>")

    @classmethod
    def get_url_by_name(cls, name):
        if name == 'assigned_to':
            return 'areas'
        return None

    @classmethod
    def table_columns(cls):
        return 'render_code', 'name', 'company_name', 'credit_limit', 'available_credit', 'assigned_to:Area', 'date_created:Created On', 'last_updated'

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'name', 'company_name', 'address', 'credit_limit', 'assigned_to', 'date_created', 'last_updated']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['area'] = d.pop('assigned_to')
        d['created_by'] = self.created_by
        d['last_updated_by'] = self.last_updated_by
        d['location'] = self.address.location if self.address.location else 'N/A'
        d['contract_address'] = self.contact_person if self.contact_person else 'N/A'

        return d

    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Edit',
                action='edit',
                icon='fbx-rightnav-edit',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit)
            )
        ]

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create, ViewActionEnum.Edit, ViewActionEnum.Delete, ViewActionEnum.AdvancedImport, ViewActionEnum.AdvancedExport]


    @classmethod
    def get_dependent_field_list(cls):
        return ['custom_fields']

    @classmethod
    def get_serializer(cls):
        ss = Client.get_serializer()

        class Serializer(ss):
            company_name = serializers.CharField(required=False)
            manufacturer = serializers.PrimaryKeyRelatedField(required=True, queryset=Manufacturer.objects.all())
            address = ContactAddress.get_serializer()(required=True)
            contact_person = WebUser.get_serializer()(required=False)
            client_meta = ClientMeta.get_serializer()(required=False)
            phone_number = PhoneNumber.get_serializer()(required=False)

            class Meta(ss.Meta):
                model = cls
        return Serializer

    def save(self, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)

            client_added_new = True if not self.pk else False
            if client_added_new:
                client_credit = ClientCredit()
                client_credit.client = self
                client_credit.organization = self.organization
                client_credit.save()
            else:
                if not ClientCredit.objects.filter(client=self).exists():
                    client_credit = ClientCredit()
                    client_credit.client = self
                    client_credit.organization = self.organization
                    client_credit.save()

            if not self.assigned_employee.exists():
                dis_assign = InfrastructureUserAssignment()
                dis_assign.role = Role.objects.get(name='ServicePerson')
                dis_assign.organization = self.organization
                dis_assign.save()
                self.assigned_employee.add(dis_assign)
    @property
    def contact_person_name(self):
        return self.contact_person.name

    @property
    def distributor_email(self):
        return self.contact_person.email

    @property
    def contact_person_mobile(self):
        return self.contact_person.phone

    @property
    def distributor_area(self):
        return self.assigned_to.name

    @property
    def distributor_street(self):
        return self.address.street

    @property
    def distributor_division(self):
        return self.address.division

    @property
    def distributor_district(self):
        return self.address.state

    @property
    def distributor_union_city(self):
        return self.address.city

    @property
    def distributor_province_upazilla(self):
        return self.address.province

    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        if not organization:
            organization = Organization.objects.all().first()
        exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
        if not exporter_configs.exists():
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.starting_row = 1
            exporter_config.save(**kwargs)
        else:
            for e in exporter_configs:
                e.delete()
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.save(**kwargs)

        columns = [
            ExporterColumnConfig(column=0, column_name='Dealer Code', property_name='code', ignore=False),
            ExporterColumnConfig(column=1, column_name='Dealer Name', property_name='name', ignore=False),
            ExporterColumnConfig(column=2, column_name='Contact person', property_name='contact_person_name', ignore=False),
            ExporterColumnConfig(column=3, column_name='Email', property_name='distributor_email', ignore=False),
            ExporterColumnConfig(column=4, column_name='Mobile No', property_name='contact_person_mobile', ignore=False),
            ExporterColumnConfig(column=5, column_name='Area', property_name='distributor_area', ignore=False),
            ExporterColumnConfig(column=6, column_name='Credit Limit', property_name='credit_limit', ignore=False),
            ExporterColumnConfig(column=7, column_name='Street/Village', property_name='distributor_street', ignore=False),
            ExporterColumnConfig(column=8, column_name='Union/City', property_name='distributor_union_city', ignore=False),
            ExporterColumnConfig(column=9, column_name='Upazilla', property_name='distributor_province_upazilla', ignore=False),
            ExporterColumnConfig(column=10, column_name='District', property_name='distributor_district', ignore=False),
            ExporterColumnConfig(column=11, column_name='Division', property_name='distributor_division', ignore=False)
            ]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)
        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):
        for column in columns:
            workbook.cell(row=row_number, column=column.column + 1).value = str(getattr(self, column.property_name))
        return self.pk, row_number + 1

    @classmethod
    def finalize_export(cls, workbook=None, row_number=None, query_set=None, **kwargs):
        return workbook

    @classmethod
    def initialize_export(cls, workbook=None, columns=None, row_number=None, query_set=None, **kwargs):
        for column in columns:
            workbook.cell(row=1, column=column.column + 1).value = column.column_name
        return workbook, row_number



    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if result or importer_config.columns.count() == 0:
            importer_config.save(**kwargs)
            columns = [
                ImporterColumnConfig(column=0, column_name='Dealer Code', property_name='code', ignore=False),
                ImporterColumnConfig(column=1, column_name='Dealer Name', property_name='name', ignore=False),
                ImporterColumnConfig(column=2, column_name='Contact person', property_name='contact_person_name', ignore=False),
                ImporterColumnConfig(column=3, column_name='Email', property_name='distributor_email', ignore=False),
                ImporterColumnConfig(column=4, column_name='Mobile No', property_name='phone_number', ignore=False),
                ImporterColumnConfig(column=5, column_name='Area', property_name='distributor_area', ignore=False),
                ImporterColumnConfig(column=6, column_name='Credit Limit', property_name='credit_limit', ignore=False),
                ImporterColumnConfig(column=7, column_name='Street/Village', property_name='distributor_street', ignore=False),
                ImporterColumnConfig(column=8, column_name='Union/City', property_name='distributor_union_city', ignore=False),
                ImporterColumnConfig(column=9, column_name='Upazilla', property_name='distributor_province_upazilla', ignore=False),
                ImporterColumnConfig(column=10, column_name='District', property_name='distributor_district', ignore=False),
                ImporterColumnConfig(column=11, column_name='Division', property_name='distributor_division', ignore=False)
                ]
            for c in columns:
                c.save(organization=organization, **kwargs)
                importer_config.columns.add(c)
        return importer_config


    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        with transaction.atomic():
            org = user.organization

            area_name = data['5']
            from foodex.models.infrastructure.area import Area
            area_objects = Area.objects.filter(name=area_name)
            if area_objects.exists():
                area_object = area_objects.first()
            else:
                ErrorLog.log("Exception in distributor import. Area does not exist with name %s. Skipping......Data provided: %s" % (area_name, data), stacktrace='')
                return None

            ###Create Country.
            country_name = "Bangladesh"
            country_objects = Country.objects.filter(name=country_name)
            if country_objects.exists():
                country_object = country_objects.first()
            else:
                ErrorLog.log("Exception in distributor import. Country does not exist with name %s. Skipping......Data provided: %s" % (country_name, data), stacktrace='')
                return None

            ###Create Division.
            division_name = data['11']
            division_objects = Division.objects.filter(name=division_name)
            if division_objects.exists():
                division_object = division_objects.first()
            else:
                ErrorLog.log("Exception in distributor import. Division does not exist with name %s. Skipping......Data provided: %s" % (division_name, data), stacktrace='')
                return None

            ###Create District.
            district_name = data['10']
            district_objects = State.objects.filter(name=district_name)
            if district_objects.exists():
                district_object = district_objects.first()
            else:
                ErrorLog.log("Exception in distributor import. District does not exist with name %s. Skipping......Data provided: %s" % (district_name, data), stacktrace='')
                return None

            ###Create Province/Upazilla.
            upazila_name = data['9']
            upazila_objects = Upazila.objects.filter(name=upazila_name)
            if upazila_objects.exists():
                upazila_object = upazila_objects.first()
            else:
                ErrorLog.log("Exception in distributor import. Upazila does not exist with name %s. Skipping......Data provided: %s" % (upazila_name, data), stacktrace='')
                return None


            contact_address = None
            phone_number = None
            contact_person = None
            distributor_objects = Distributor.objects.filter(Q(code=data['0']) | Q(name=data['1']))
            distributor_available_credit = data['6'];
            if distributor_objects.exists():
                distributor_object = distributor_objects.first()
                distributor_available_credit = distributor_object.available_credit
                contact_address = distributor_object.address
                contact_person = distributor_object.contact_person
            else:
                distributor_object = Distributor()
            distributor_object.name = data['1']
            distributor_object.creation_flag = CreationFlag.Imported
            distributor_object.created_by = user
            distributor_object.type = Distributor.__name__
            distributor_object.credit_limit = data['6']
            distributor_object.available_credit = distributor_available_credit
            distributor_object.organization = org
            distributor_object.assigned_to = area_object
            distributor_object.company_name = data['1']
            if contact_address is None:
                contact_address = ContactAddress()
            else:
                contact_address = distributor_object.address
            contact_address.street = data['7']
            contact_address.city = data['8']
            contact_address.province = upazila_object.name
            contact_address.state = district_object
            contact_address.division = division_object
            contact_address.country = country_object
            contact_address.save()
            distributor_object.address = contact_address
            if contact_person is None:
                contact_person = WebUser()
                contact_person.name = data['2']
                contact_person.address = contact_address
                if len(str(data['3'])) > 5:
                    email = EmailAddress()
                    email.organization = org
                    email.email = data['3']
                    email.save()
                contact_person.email = email
                if len(str(data['4'])) > 4:
                    phone_number = data['4']
                    phone_number_object = PhoneNumber()
                    phone_number_object.phone = phone_number
                    phone_number_object.is_primary = True
                    phone_number_object.is_own = True
                    phone_number_object.save()
                contact_person.phone = phone_number_object
            else:
                contact_person.name = data['2']
                contact_person.address = contact_address
                email = contact_person.email
                email.email = data['3']
                email.save()
                contact_person.email = email
                phone_number_object = contact_person.phone
                phone_number_object.phone = data['4']
                phone_number_object.save()
                contact_person.phone = phone_number_object
            contact_person.save()
            distributor_object.contact_person = contact_person
            distributor_object.phone_number = contact_person.phone
            distributor_object.save()

            return distributor_object.pk

    class Meta:
        proxy = True


    @property
    def tabs_config(self):
        primary_transactions = get_model('foodex','CompletedSalesTransaction').objects.filter(client_id=self.pk,transaction_type=TransactionType.Sale.value,infrastructure_unit__isnull=False)
        tabs = [TabView(
            title='Primary Transaction (s)',
            access_key='primarytrnasaction',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='foodex.CompletedSalesTransaction',
            queryset= primary_transactions
        ), TabView(
            title='Secondary Transaction (s)',
            access_key='secondarytransaction',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='sales.SecondaryTransaction',
            queryset_filter=Q(**{'client__id': self.pk, 'infrastructure_unit': None})
        ), TabView(
            title='Incoming Products',
            access_key='incominginventories',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='foodex.DeliveryTransaction',
            queryset_filter=Q(**{'client__id': self.pk})
        ), TabView(
            title='Inventory',
            access_key='currentinventories',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='foodex.ClientInventory',
            queryset_filter=Q(**{'assigned_to__id': self.pk})
        )]

        clients = self.assigned_employee.all()

        for c in clients:
            users_already_assigned = []
            all_inf_assignment = self.__class__.objects.all()
            for d in all_inf_assignment:
                for e in d.assigned_employee.all():
                    t = list(e.users.values('id'))
                    t = [l['id'] for l in t]
                    users_already_assigned += t


            tabs.append(TabView(
                title=bw_titleize(c.role.get_rename) + '(s)',
                access_key=c.role.name.lower(),
                route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
                relation_type=ModelRelationType.NORMAL,
                property=c.users,
                add_more_queryset=ConsoleUser.objects.filter(type=c.role.name).exclude(pk__in=users_already_assigned),#exclude(pk__in=c.users.values('id')),
                related_model=ConsoleUser,
                queryset=ConsoleUser.objects.filter(pk__in=c.users.values_list('pk', flat=True)),
                actions=[
                    TabViewAction(
                        title='Add',
                        action='add',
                        icon='icon-plus',
                        route_name=ConsoleUser.get_route_name(action=ViewActionEnum.PartialBulkAdd, parent=self.__class__.__name__.lower()),
                        css_class='manage-action load-modal fis-plus-ico'
                    ),
                    TabViewAction(
                        title='Remove',
                        action='partial-remove',
                        icon='icon-remove',
                        route_name=ConsoleUser.get_route_name(action=ViewActionEnum.PartialBulkRemove, parent=self.__class__.__name__.lower()),
                        css_class='manage-action delete-item fis-remove-ico'
                    )
                ]

            ))
            
        tabs.append(TabView(
            title='Route(s)',
            access_key='routes',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            property=self.infrastructureunit_set,
            # queryset=get_model('foodex', 'Route').objects.filter(parent_client=self),
            add_more_queryset=Route.objects.filter(type='Route', parent_client=None),
            related_model=Route,
            actions=[
                TabViewAction(
                    title="Create",
                    action="action",
                    icon="icon-plus",
                    css_class='manage-action load-modal fis-create-ico',
                    route_name=Route.get_route_name(action=ViewActionEnum.PartialCreate, parent=self.__class__.__name__.lower())
                ),
                TabViewAction(
                    title='Add',
                    action='add',
                    icon='icon-plus',
                    route_name=Route.get_route_name(action=ViewActionEnum.PartialBulkAdd, parent=self.__class__.__name__.lower()),
                    css_class='manage-action load-modal fis-plus-ico'
                ),
                TabViewAction(
                    title='Remove',
                    action='partial-remove',
                    icon='icon-remove',
                    route_name=Route.get_route_name(action=ViewActionEnum.PartialBulkRemove, parent=self.__class__.__name__.lower()),
                    css_class='manage-action delete-item fis-remove-ico'
                )
            ]

        ))

        return tabs