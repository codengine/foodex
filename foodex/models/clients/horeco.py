import traceback
from decimal import Decimal
from django.db import transaction
from django.db.models.aggregates import Sum
from django.db.models.loading import get_model
from django.db.models.query_utils import Q
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.common.contactaddress import ContactAddress
from blackwidow.core.models.common.country import Country
from blackwidow.core.models.common.division import Division
from blackwidow.core.models.common.phonenumber import PhoneNumber
from blackwidow.core.models.common.state import State
from blackwidow.core.models.common.upazila import Upazila
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig
from blackwidow.core.models.config.exporter_config import ExporterConfig
from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
from blackwidow.core.models.config.importer_config import ImporterConfig
from blackwidow.core.models.contracts.base import CreationFlag
from blackwidow.core.models.log.logs import ErrorLog
from blackwidow.core.models.organizations.organization import Organization
from blackwidow.core.models.users.user import WebUser
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.decorators.enable_export import enable_export
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, loads_initial_data, is_object_context
from blackwidow.sales.decorators.model_decorators import enable_individual_assignment
from blackwidow.sales.models.common.product_price_config import ProductPriceConfig
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.infrastructure.area import Area
from foodex.models.infrastructure.region import Region
from foodex.models.invoice.open_invoice import OpenInvoice

__author__ = 'Mahmud'


@decorate(is_object_context, enable_import, enable_export, expose_api('horecas'), save_audit_log, enable_individual_assignment(models=[ProductPriceConfig]),
          route(route='horecas', group='Customers', group_order=1, module=ModuleEnum.Administration, display_name="HoReCa", item_order=2))
class HoReCo(Client):

    @classmethod
    def prefix(cls):
        return 'HRC'

    class Meta:
        proxy = True


    @classmethod
    def table_columns(cls):
        return 'code', 'name', 'company_name', 'credit_limit', 'available_credit', 'assigned_to:Area', 'date_created:created_on', 'last_updated'


    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'name', 'company_name', 'address', 'credit_limit', 'available_credit', 'assigned_to', 'date_created', 'last_updated']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['area'] = d.pop('assigned_to')
        d['created_on'] = d.pop('date_created')
        d['created_by'] = self.created_by
        d['last_updated_by'] = self.last_updated_by
        d['location'] = self.address.location
        d['contract_address'] = self.contact_person if self.contact_person else 'N/A'

        return d

    def save(self, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)

            client_added_new = True if not self.pk else False
            ClientCredit = get_model('foodex','ClientCredit')
            if client_added_new:
                client_credit = ClientCredit()
                client_credit.client = self
                client_credit.organization = self.organization
                client_credit.save()
            else:
                if not ClientCredit.objects.filter(client=self).exists():
                    client_credit = ClientCredit()
                    client_credit.client = self
                    client_credit.organization = self.organization
                    client_credit.save()


    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Edit',
                action='edit',
                icon='fbx-rightnav-edit',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit)
            )
        ]

    @property
    def tabs_config(self):
        tabs = [TabView(
            title='Incoming Products',
            access_key='incominginventories',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='sales.ClientOnRouteInventory',
            queryset_filter=Q(**{'assigned_to__id': self.pk})
        ), TabView(
            title='Product Inventory',
            access_key='currentinventories',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='sales.ClientCurrentInventory',
            queryset_filter=Q(**{'assigned_to__id': self.pk})
        )]
        return tabs

    @classmethod
    def get_manage_buttons(cls):
        return [ ViewActionEnum.Create, ViewActionEnum.Edit, ViewActionEnum.Delete, ViewActionEnum.AdvancedExport, ViewActionEnum.AdvancedImport ]

    @property
    def contact_person_name(self):
        return self.contact_person.name if self.contact_person else ""

    @property
    def contact_person_mobile(self):
        return self.contact_person.phone if self.contact_person else ""
    
    @property
    def horeca_area(self):
        return self.assigned_to.name if self.assigned_to else ""

    @property
    def horeca_street(self):
        return self.address.street if self.address else ""

    @property
    def horeca_division(self):
        return self.address.division if self.address else ""

    @property
    def horeca_district(self):
        return self.address.state if self.address else ""

    @property
    def horeca_union_city(self):
        return self.address.city if self.address else ""

    @property
    def horeca_province_upazilla(self):
        return self.address.province if self.address else ""
    
    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        if not organization:
            organization = Organization.objects.all().first()
        exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
        if not exporter_configs.exists():
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.starting_row = 1
            exporter_config.save(**kwargs)
        else:
            for e in exporter_configs:
                e.delete()
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.save(**kwargs)

        columns = [
            ExporterColumnConfig(column=0, column_name='Horeca Code', property_name='code', ignore=False),
            ExporterColumnConfig(column=1, column_name='Horeca Name', property_name='name', ignore=False),
            ExporterColumnConfig(column=2, column_name='Contact person', property_name='contact_person_name', ignore=False),
            ExporterColumnConfig(column=3, column_name='Mobile No', property_name='contact_person_mobile', ignore=False),
            ExporterColumnConfig(column=4, column_name='Area', property_name='horeca_area', ignore=False),
            ExporterColumnConfig(column=5, column_name='Credit Limit', property_name='credit_limit', ignore=False),
            ExporterColumnConfig(column=6, column_name='Street/Village', property_name='horeca_street', ignore=False),
            ExporterColumnConfig(column=7, column_name='Union/City', property_name='horeca_union_city', ignore=False),
            ExporterColumnConfig(column=8, column_name='Upazilla', property_name='horeca_province_upazilla', ignore=False),
            ExporterColumnConfig(column=9, column_name='District', property_name='horeca_district', ignore=False),
            ExporterColumnConfig(column=10, column_name='Division', property_name='horeca_division', ignore=False)
        ]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)
        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):
        for column in columns:
            workbook.cell(row=row_number, column=column.column + 1).value = str(getattr(self, column.property_name))
        return self.pk, row_number + 1

    @classmethod
    def finalize_export(cls, workbook=None, row_number=None, query_set=None, **kwargs):
        return workbook

    @classmethod
    def initialize_export(cls, workbook=None, columns=None, row_number=None, query_set=None, **kwargs):
        for column in columns:
            workbook.cell(row=1, column=column.column + 1).value = column.column_name
        return workbook, row_number

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if result or importer_config.columns.count() == 0:
            importer_config.save(**kwargs)
            columns = [
                ImporterColumnConfig(column=0, column_name='Horeca Code', property_name='code', ignore=False),
                ImporterColumnConfig(column=1, column_name='Horeca Name', property_name='name', ignore=False),
                ImporterColumnConfig(column=2, column_name='Contact person', property_name='contact_person_name', ignore=False),
                ImporterColumnConfig(column=3, column_name='Mobile No', property_name='contact_person_mobile', ignore=False),
                ImporterColumnConfig(column=4, column_name='Area', property_name='horeca_area', ignore=False),
                ImporterColumnConfig(column=5, column_name='Credit Limit', property_name='credit_limit', ignore=False),
                ImporterColumnConfig(column=6, column_name='Street/Village', property_name='horeca_street', ignore=False),
                ImporterColumnConfig(column=7, column_name='Union/City', property_name='horeca_union_city', ignore=False),
                ImporterColumnConfig(column=8, column_name='Upazilla', property_name='horeca_province_upazilla', ignore=False),
                ImporterColumnConfig(column=9, column_name='District', property_name='horeca_district', ignore=False),
                ImporterColumnConfig(column=10, column_name='Division', property_name='horeca_division', ignore=False)
            ]
            for c in columns:
                c.save(organization=organization, **kwargs)
                importer_config.columns.add(c)
        return importer_config

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):

        if data['0']:
            if not HoReCo.objects.filter(code=data['0']).exists():
                ErrorLog.log("Horeca with code %s doesn't exists." % ( data['0'], ), stacktrace="N/A")
                return None

        if any([ not data['%s' % (i + 1)] for i in range(10) ]):
            ErrorLog.log("Not all data provided. Provided data: %s " % (data, ), stacktrace="N/A")
            return None

        try:
            Decimal(data['5'])
        except:
            ErrorLog.log("Invalid credit limit provided. Provided value: %s" % (data['5'], ), stacktrace="N/A")
            return None

        with transaction.atomic():
            org = user.organization

            region_name = "Horeca"
            region_objects = Region.objects.filter(name=region_name)
            if region_objects.exists():
                region_object = region_objects.first()
            else:
                ErrorLog.log("Exception in horeca import. Region does not exist with name %s. Skipping......Data provided: %s" % (region_name, data), stacktrace='')
                return None

            area_name = data['4']
            area_objects = Area.objects.filter(name=area_name)
            if area_objects.exists():
                area_object = area_objects.first()
            else:
                ErrorLog.log("Exception in horeca import. Area does not exist with name %s. Skipping......Data provided: %s" % (area_name, data), stacktrace='')
                return None

            ###Create Country.
            country_name = "Bangladesh"
            country_objects = Country.objects.filter(name=country_name)
            if country_objects.exists():
                country_object = country_objects.first()
            else:
                ErrorLog.log("Exception in horeca import. Country does not exist with name %s. Skipping......Data provided: %s" % (country_name, data), stacktrace='')
                return None

            ###Create Division.
            division_name = data['10']
            division_objects = Division.objects.filter(name=division_name)
            if division_objects.exists():
                division_object = division_objects.first()
            else:
                ErrorLog.log("Exception in horeca import. Division does not exist with name %s. Skipping......Data provided: %s" % (division_name, data), stacktrace='')
                return None

            ###Create District.
            district_name = data['9']
            district_objects = State.objects.filter(name=district_name)
            if district_objects.exists():
                district_object = district_objects.first()
            else:
                ErrorLog.log("Exception in horeca import. District does not exist with name %s. Skipping......Data provided: %s" % (district_name, data), stacktrace='')
                return None

            ###Create Province/Upazilla.
            upazila_name = data['8']
            upazila_objects = Upazila.objects.filter(name=upazila_name)
            if upazila_objects.exists():
                upazila_object = upazila_objects.first()
            else:
                ErrorLog.log("Exception in horeca import. Upazila does not exist with name %s. Skipping......Data provided: %s" % (upazila_name, data), stacktrace='')
                return None


            contact_address = None
            phone_number = None
            contact_person = None
            horeca_available_credit = Decimal(data['5'])
            if data['0']:
                horeca_object = HoReCo.objects.get(code=data['0'])
                horeca_available_credit = horeca_object.available_credit
                contact_address = horeca_object.address
                contact_person = horeca_object.contact_person
            else:
                horeca_object = HoReCo()
            horeca_object.name = data['1']
            horeca_object.creation_flag = CreationFlag.Imported
            horeca_object.created_by = user
            horeca_object.type = HoReCo.__name__
            horeca_object.credit_limit = Decimal(data['5'])
            horeca_object.available_credit = horeca_available_credit
            horeca_object.organization = org
            horeca_object.assigned_to = area_object
            horeca_object.company_name = data['1']
            if not contact_address:
                contact_address = ContactAddress()
            contact_address.street = data['6']
            contact_address.city = data['7']
            contact_address.province = upazila_object.name
            contact_address.state = district_object
            contact_address.division = division_object
            contact_address.country = country_object
            contact_address.save()
            horeca_object.address = contact_address
            if contact_person is None:
                contact_person = WebUser()
                contact_person.name = data['2']
                contact_person.address = contact_address
                if len(str(data['3'])) > 4:
                    phone_number = data['3']
                    phone_number_object = PhoneNumber()
                    phone_number_object.phone = phone_number
                    phone_number_object.is_primary = True
                    phone_number_object.is_own = True
                    phone_number_object.save()
                    contact_person.phone = phone_number_object
            else:
                contact_person.name = data['2']
                contact_person.address = contact_address
                phone_number_object = contact_person.phone
                if not phone_number_object:
                    phone_number_object = PhoneNumber()
                    phone_number_object.is_primary = True
                    phone_number_object.is_own = True
                phone_number_object.phone = data['3']
                phone_number_object.save()
                contact_person.phone = phone_number_object
            contact_person.save()
            horeca_object.contact_person = contact_person
            horeca_object.phone_number = contact_person.phone
            horeca_object.save()

            return data