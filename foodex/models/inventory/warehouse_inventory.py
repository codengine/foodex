from django.db.models.loading import get_model
from openpyxl.styles import Style
from openpyxl.styles.alignment import Alignment
from openpyxl.styles.fonts import Font
from blackwidow.core.managers.modelmanager import DomainEntityModelManager
from blackwidow.core.models.organizations.organization import Organization
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.sales.models.transactions.transaction import TransactionType
from blackwidow.sales.models.inventory.infrastructure_unit_inventory import InfrastructureUnitInventory
from threading import Thread
from uuid import uuid4
from django.db import transaction
from django.dispatch.dispatcher import receiver
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.common.contactaddress import ContactAddress
from blackwidow.core.models.common.location import Location
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig
from blackwidow.core.models.config.exporter_config import ExporterConfig

from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.core.models.contracts.manufacturer_domain_entity import ManufacturerDomainEntity
from blackwidow.core.signals.signals import import_completed
from blackwidow.engine.decorators.enable_export import enable_export
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.engine.extensions.clock import Clock
from blackwidow.sales.models.common.product_price import ProductPriceEnum
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.transactions.stock_adjustment import StockAdjustmentTransaction
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from multiprocessing.synchronize import Lock
from config.enums.tab_view_enum import ModelRelationType
from foodex.models.infrastructure.warehouse import Warehouse
from django import forms
from django.db.models import Count, Q

__author__ = 'Sohel'

@decorate(save_audit_log, is_object_context, enable_export, enable_import, expose_api("inventories"),
          route(route='inventories', module=ModuleEnum.Execute, group_order=4, item_order=3, display_name='Foodex Warehouse Inventory', group='Inventory'))
class WarehouseInventory(InfrastructureUnitInventory):

    objects = DomainEntityModelManager(filter={'type': 'WarehouseInventory'})

    def save(self, *args, **kwargs):
        self.type = self.__class__.__name__
        super().save(*args,**kwargs)

    class Meta:
        proxy = True

    @classmethod
    def get_search_form(cls, search_fields, fields):
        sf_instance = ManufacturerDomainEntity.get_search_form(search_fields, fields)

        i_label,i_value = cls.get_search_value("assigned_to",search_fields,fields)

        sf_instance.fields['assigned_to:id'] = GenericModelChoiceField(label='Warehouse', empty_label="Select Warehouse", required=False,  initial=i_value, queryset=Warehouse.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
        sf_instance.fields['assigned_to:id'].readonly = False

        return sf_instance

    @property
    def render_product(self):
        return self.product.code + ': ' + self.product.name.strip() + ' ' + self.product.description.strip()

    @property
    def render_distributor(self):
        # from blackwidow.core.models.clients.distributor import Distributor
        # _distributor = Distributor.objects.filter(assigned_to=self.assigned_to, manufacturer=self.manufacturer)
        # if _distributor.exists():
        #     return _distributor[0]
        return ''



    @property
    def tabs_config(self):
        transaction_breakdown_set = TransactionBreakDown.objects.filter(product=self.product)
        transaction_set = get_model('sales', 'Transaction').objects.filter(infrastructure_unit=self.assigned_to, breakdown__in=transaction_breakdown_set)
        # stock_management_transaction_set = transaction_set.filter(transaction_type__in=[TransactionType.StockCount.value, TransactionType.StockIn.value])
        transaction_id_list = list()
        for t in transaction_set:
            transaction_id_list.append(t.id)
        #
        # stock_management_transaction_id_list = list()
        # for t in stock_management_transaction_set:
        #     stock_management_transaction_id_list.append(t.id)


        tabs = [TabView(
            title='Primary Sale(s)',
            access_key='transaction',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='foodex.CompletedSalesTransaction',
            queryset_filter=Q(**{'id__in': transaction_id_list, 'transaction_type': TransactionType.Sale.value})
        ),TabView(
            title='Stock Count(s)',
            access_key='stockcount',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='foodex.StockCountTransaction',
            queryset_filter=Q(**{'id__in': [x.id for x in transaction_set]})
        ),TabView(
            title='Stock In(s)',
            access_key='stockin',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='foodex.StockInTransaction',
            queryset_filter=Q(**{'id__in': [x.id for x in transaction_set]})
        )]
        return tabs

    @classmethod
    def get_export_dependant_fields(self):
        class AdvancedExportDependentForm(GenericFormMixin):
            def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
                super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
                self.fields['assigned_to'] = GenericModelChoiceField(label='Select Warehouse', empty_label="Select Warehouse", required=True, queryset=InfrastructureUnit.objects.filter(type='WareHouse'))

        return AdvancedExportDependentForm

    @property
    def get_warehouse_id(self):
        return self.assigned_to.pk if self.assigned_to else ''

    @property
    def get_warehouse_name(self):
        return self.assigned_to.name if self.assigned_to else ''

    @property
    def get_product_id(self):
        return self.product.pk if self.product else ''

    @property
    def get_product_name(self):
        return self.product.name if self.product else ''

    @property
    def get_product_description(self):
        return self.product.name if self.product else ''

    @property
    def get_incoming_stock(self):
        return 0

    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        if not organization:
            organization = Organization.objects.all().first()
        exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
        if not exporter_configs.exists():
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.starting_row = 2
            exporter_config.save(**kwargs)
        else:
            for e in exporter_configs:
                e.delete()
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.save(**kwargs)

        columns = [
            ExporterColumnConfig(column=0, column_name='Warehouse Id', property_name='get_warehouse_id', ignore=False),
            ExporterColumnConfig(column=1, column_name='Warehouse Name', property_name='get_warehouse_name', ignore=False),
            ExporterColumnConfig(column=2, column_name='Product Id', property_name='get_product_id', ignore=False),
            ExporterColumnConfig(column=3, column_name='Product Name', property_name='get_product_name', ignore=False),
            ExporterColumnConfig(column=4, column_name='Product Description', property_name='get_product_description', ignore=False),
            ExporterColumnConfig(column=5, column_name='Previous Stock Quantity', property_name='stock', ignore=False),
            ExporterColumnConfig(column=6, column_name='Stock Change Quantity', property_name='get_incoming_stock', ignore=False)
        ]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)
        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):
        for column in columns:
            workbook.cell(row=row_number, column=column.column + 1).value = str(getattr(self, column.property_name))
        return self.pk, row_number + 1

    @classmethod
    def finalize_export(cls, workbook=None, row_number=None, query_set=None, **kwargs):
        file_name = 'WarehouseInventory' + str(Clock.timestamp(_format='ms'))
        return workbook, file_name

    @classmethod
    def initialize_export(cls, workbook=None, columns=None, row_number=None, query_set=None, **kwargs):
        all_products = Product.objects.all()
        all_warehouses = Warehouse.objects.all()

        with transaction.atomic():
            for warehouse in all_warehouses:
                for product in all_products:
                    if not WarehouseInventory.objects.filter(product=product,assigned_to=warehouse).exists():
                        product_inventory_object = WarehouseInventory()
                        product_inventory_object.product = product
                        product_inventory_object.assigned_to = warehouse
                        product_inventory_object.stock = 0
                        product_inventory_object.organization = warehouse.organization
                        product_inventory_object.save()


        #print(query_set)
        for column in columns:
            workbook.cell(row=1, column=column.column + 1).value = column.column_name
        return workbook, row_number

    def download_item(self, workbook=None, row_number=None, **kwargs):
        return self.pk, row_number + 1

    @classmethod
    def finalize_download(cls, workbook=None, row_number=None, query_set=None, **kwargs):
        return workbook

    @classmethod
    def initialize_download(cls, workbook=None, row_number=None, query_set=None, **kwargs):

        query_params = kwargs.get('query_params')
        warehouse_id = query_params.get('assigned_to')
        warehouse = Warehouse.objects.get(pk=int(warehouse_id))

        workbook.page_setup.paperSize = workbook.PAPERSIZE_A4
        workbook.page_margins.top = 1.5
        workbook.page_margins.header = 0.25
        workbook.page_margins.bottom = 1.0
        workbook.page_margins.footer = 0.25
        workbook.page_margins.left = 0.75
        workbook.page_margins.right = 0.25

        workbook.header_footer.center_header.text = 'Foodex International\nWarehouse Inventory\nWarehouse: '+warehouse.name
        workbook.header_footer.center_footer.text = '     Prepared By:                   '+ \
                                                    '             Checked By                     Approved By\n'+ \
                                                    'This page was generated at '+ \
                                                    Clock.now().strftime('%d-%m-%Y %H:%M:%S') + ''

        workbook.header_footer.right_footer.text = '(Page &[Page]  of &[Pages])'

        column = 0
        workbook.cell(row=row_number, column=column + 1).style = Style(font=Font(bold=True))
        workbook.cell(row=row_number, column=column + 1).value = 'Product Code'
        column += 1
        workbook.cell(row=row_number, column=column + 1).style = Style(font=Font(bold=True))
        workbook.cell(row=row_number, column=column + 1).value = 'Product Name'
        column += 1
        workbook.cell(row=row_number, column=column + 1).style = Style(font=Font(bold=True))
        workbook.cell(row=row_number, column=column + 1).value = 'Description'
        column += 1
        workbook.cell(row=row_number, column=column + 1).style = Style(font=Font(bold=True),
                                                                       alignment=Alignment(horizontal='right'))
        workbook.cell(row=row_number, column=column + 1).value = 'Stock'
        column += 1

        workbook.column_dimensions['A'].width=15
        workbook.column_dimensions['B'].width=36
        workbook.column_dimensions['C'].width=20
        workbook.column_dimensions['D'].width=8

        row_number += 1

        product_objects = get_model("sales", "Product").objects.all()
        for product in product_objects:
            column = 0
            foodex_warehouse_inventory = get_model('foodex','WarehouseInventory').objects.filter(product=product,assigned_to_id=int(warehouse_id))

            workbook.cell(row=row_number, column=column + 1).style = Style(alignment=Alignment(horizontal='center', vertical='top'))
            workbook.cell(row=row_number, column=column + 1).value = product.code
            column += 1
            workbook.cell(row=row_number, column=column + 1).style = Style(alignment=Alignment(wrap_text=True, vertical='top'))
            workbook.cell(row=row_number, column=column + 1).value = product.name
            column += 1
            workbook.cell(row=row_number, column=column + 1).style = Style(alignment=Alignment(wrap_text=True,vertical='top'))
            workbook.cell(row=row_number, column=column + 1).value = product.description
            column += 1
            workbook.cell(row=row_number, column=column + 1).style = Style(alignment=Alignment(horizontal='right',vertical='top'))
            workbook.cell(row=row_number, column=column + 1).value = foodex_warehouse_inventory. \
                order_by('-last_updated').first().stock if foodex_warehouse_inventory.exists() else '0'
            column += 1

            row_number += 1

        return workbook, row_number

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.AdvancedDownload]

    @property
    def get_inline_manage_buttons(self):
        return [

        ]

    @classmethod
    def table_columns(cls):
        return 'code', 'assigned_to:Warehouse', 'product', 'stock:Stock', 'last_updated', 'last_updated_by'

    @property
    def details_config(self):
        dic = super().details_config

        custom_list = ['code', 'product', 'last_updated']
        for key in dic:
            if key in custom_list:
                pass
            else:
                del dic[key]

        dic['Warehouse'] = self.assigned_to
        dic['Updated quantity'] = self.stock
        dic['Last Updated By'] = self.last_updated_by

        return dic

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        # ImporterColumnConfig= 'core.ImporterColumnConfig'
        # ImporterConfig = 'core.ImporterConfig'
        from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
        from blackwidow.core.models.config.importer_config import ImporterConfig
        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if result or importer_config.columns.count() == 0:
            importer_config.save(**kwargs)
        else:
            for items in importer_config.columns.all():
                items.delete()
        columns = [
            ImporterColumnConfig(column=0, column_name='Warehouse Id', property_name='get_warehouse_id', ignore=False),
            ImporterColumnConfig(column=1, column_name='Warehouse Name', property_name='get_warehouse_name', ignore=False),
            ImporterColumnConfig(column=2, column_name='Product Id', property_name='get_product_id', ignore=False),
            ImporterColumnConfig(column=3, column_name='Product Name', property_name='get_product_name', ignore=False),
            ImporterColumnConfig(column=4, column_name='Product Description', property_name='get_product_description', ignore=False),
            ImporterColumnConfig(column=5, column_name='Previous Stock Quantity', property_name='stock', ignore=False),
            ImporterColumnConfig(column=6, column_name='Stock Change Quantity', property_name='get_incoming_stock', ignore=False)
            ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config


    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, request=None, **kwargs):
        # with transaction.atomic():
        #     pi = ProductInventory.objects.filter(assigned_to_id=int(data['0']), product_id=int(data['2']))
        #     if pi.exists():
        #         pi = pi.first()
        #         pi.stock = pi.stock + int(data['6'])
        #         pi.save()
        #         return data
        #     else:
        #         organization = Organization.objects.all().first()
        #         pi = ProductInventory()
        #         pi.assigned_to_id = int(data['0'])
        #         pi.product_id = int(data['2'])
        #         pi.stock = int(data['6'])
        #         pi.organization = organization
        #         pi.save()
        #         return data

        return data

def save_transaction_details(items,user,organization):
    #print("Saving transaction details now.")
    warehouses = {}
    #print(items)
    for data in items:
        w_id = int(data['0'])
        if not w_id in warehouses.keys():
            warehouses[int(data['0'])] = [data]
        else:
            warehouses[int(data['0'])] += [data]

    with transaction.atomic():
        for warehouse_id,data in warehouses.items():

            warehouse_objects = Warehouse.objects.filter(pk=warehouse_id)
            if not warehouse_objects.exists():
                warehouse_object = Warehouse()
                warehouse_object.name = data[0]['1']
                warehouse_object.organization = organization
                warehouse_object.created_by = user

                contact_address = ContactAddress()
                contact_address.street = 'Azhar Comfort Complex.'
                contact_address.city = 'Dhaka'
                contact_address.province = 'Dhaka'
                contact_address.save()

                warehouse_object.address = contact_address

                warehouse_object.save()
                warehouse_id = warehouse_object.pk

            #tx_stock_adjustment = StockAdjustmentTransaction()

            #organization = Organization.objects.all().first()

            tx_stock_adjustment = StockAdjustmentTransaction()
            tx_stock_adjustment.organization = organization
            tx_stock_adjustment.infrastructure_unit_id = warehouse_id
            location = Location()
            location.save()
            tx_stock_adjustment.location = location
            tx_stock_adjustment.transaction_time = Clock.timestamp()
            tx_stock_adjustment.unique_id = str(uuid4())
            tx_stock_adjustment.created_by = user
            tx_stock_adjustment.save()

            tx_subtotal = 0
            tx_total = 0
            tx_discount = 0

            #print(data)

            for data_row in data:
                product_object = Product.objects.get(pk=int(data_row['2']))
                product_unit_price = product_object.prices.filter(price__name=ProductPriceEnum.ref_price.value["name"]).first().value
                tx_breakdown = TransactionBreakDown()
                tx_breakdown.product_id = int(data_row['2'])
                tx_breakdown.quantity = int(data_row['6'])
                tx_breakdown.unit_price = product_unit_price
                tx_breakdown.sub_total = tx_breakdown.quantity * tx_breakdown.unit_price
                tx_breakdown.discount = 0
                tx_breakdown.total = tx_breakdown.sub_total - tx_breakdown.discount

                pi = WarehouseInventory.objects.filter(assigned_to_id=warehouse_id, product_id=int(data_row['2']))
                if pi.exists():
                    pi = pi.first()
                    tx_breakdown.opening_balance = pi.stock
                    pi.stock = pi.stock + int(data_row['6'])
                    pi.save()
                else:
                    tx_breakdown.opening_balance = 0
                    pi = WarehouseInventory()
                    pi.assigned_to_id = warehouse_id
                    pi.product_id = product_object.pk
                    pi.stock = int(data_row['6'])
                    pi.organization = organization
                    pi.save()

                pi.transaction = tx_stock_adjustment

                tx_breakdown.closing_balance = pi.stock
                tx_breakdown.organization = organization
                tx_breakdown.save()
                tx_stock_adjustment.breakdown.add(tx_breakdown)

                tx_subtotal += tx_breakdown.sub_total
                tx_discount += tx_breakdown.discount
                tx_total += tx_breakdown.total

            tx_stock_adjustment.sub_total = tx_subtotal
            tx_stock_adjustment.discount = tx_discount
            tx_stock_adjustment.total = tx_total
            tx_stock_adjustment.save()


@receiver(import_completed, sender=WarehouseInventory)
def retailers_import_completed(sender, items=None, user=None, organization=None, **kwargs):
    #print("Products have been added to warehouses")
    lock = Lock(ctx=None)
    lock.acquire(True)
    process = Thread(target=save_transaction_details, args=(items,user,organization,))
    process.start()
    lock.release()
