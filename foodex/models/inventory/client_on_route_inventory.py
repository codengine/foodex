from django.core.urlresolvers import reverse
from django.db import models, transaction
from django.db.models.loading import get_model
from django.utils.safestring import mark_safe
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import save_audit_log, decorate, is_object_context
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum
from django.db.models import Sum
from blackwidow.sales.models.inventory.client_inventory import ClientInventory

__author__ = 'Sohel'

# @decorate(save_audit_log, is_object_context,
#           expose_api("incoming-inventories"),
#           route(route='incoming-inventories', module=ModuleEnum.Execute, display_name='In-Transit Stock', group='Primary Sales(Clients)'))
class ClientOnRouteInventory(ClientInventory):
    class Meta:
        proxy = True

    # @classmethod
    # def get_queryset(cls, queryset=None, **kwargs):
    #     return ClientOnRouteInventory.objects.order_by('-last_updated').distinct('assigned_to_id','product_id')

    @classmethod
    def get_button_title(cls, button=ViewActionEnum.Details):
        if button == ViewActionEnum.Mutate:
            return "Product Received"
        return button.value

    @property
    def render_manufacturer(self):
        return self.manufacturer

    @property
    def render_total_transaction_value(self):
        return self.transaction.breakdown.all().aggregate(Sum('total'))['total__sum']

    @property
    def render_completed_sales_transaction_ID(self):
        return  mark_safe("<a class='inline-link' href='" + reverse(self.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.transaction.pk}) + "' >" + self.transaction.code+": "+self.transaction.invoice_number + "</a>")

    @classmethod
    def table_columns(cls):
        return 'code', 'assigned_to:Distributor', 'product', 'stock_change:Stock Quantity', 'render_completed_sales_transaction_ID', 'last_updated', 'last_updated_by'


    def mutate_to(self, cls=None):
        with transaction.atomic():

            c_inventory_class = get_model('foodex', 'ClientInventory')
            self.type = c_inventory_class.__name__
            self.__class__ = c_inventory_class
            self.save()
            return self

    @classmethod
    def get_serializer(cls):
        ss = ClientInventory.get_serializer()

        class Serializer(ss):

            def save(self, **kwargs):
                with transaction.atomic():
                    c_inventory_class = get_model('sales', 'ClientCurrentInventory')
                    c_inv, result = c_inventory_class.objects.get_or_create(product=self.product, assigned_to=self.assigned_to)
                    if result:
                        c_inv.stock = 0
                    c_inv.stock += self.stock
                    c_inv.save()
                    self.delete()
                    return c_inv

                    # class Meta(ss.Meta):
                    #     model = cls
                    #     depth = 1
        return Serializer

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Details,ViewActionEnum.Mutate]

    @property
    def get_inline_manage_buttons(self):
        return [dict(
            name='Product Received',
            action='view',
            title="Click to receive products",
            icon='icon-arrow-right',
            ajax='0',
            url_name=self.__class__.get_route_name(action=ViewActionEnum.Mutate),
            url_params= {'pk': self.pk},
            classes='all-action confirm-action',
            parent=None
        )]