from uuid import uuid4
from django.db.models.loading import get_model
from django.dispatch.dispatcher import receiver
from openpyxl.styles import Style
from openpyxl.styles.alignment import Alignment
from openpyxl.styles.fonts import Font
from blackwidow.core.managers.modelmanager import DomainEntityModelManager
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.common.location import Location
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig
from blackwidow.core.models.config.exporter_config import ExporterConfig
from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
from blackwidow.core.models.config.importer_config import ImporterConfig
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.organizations.organization import Organization
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.core.signals.signals import import_completed
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import save_audit_log, decorate, is_object_context
from blackwidow.engine.extensions.clock import Clock
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.transactions.transaction import PrimaryTransaction, TransactionType
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from blackwidow.sales.models.transactions.transaction_status import TransactionStatus
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum
from django import forms
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.utils.safestring import mark_safe
from blackwidow.sales.models.inventory.client_inventory import ClientInventory
from blackwidow.engine.decorators.enable_export import enable_export
from blackwidow.engine.decorators.enable_import import enable_import

__author__ = 'Sohel'

@decorate(save_audit_log, is_object_context,enable_export, enable_import,
          expose_api("client-inventories"),
          route(route='client-inventories', module=ModuleEnum.Execute, group_order=4, item_order=5, display_name='Client Warehouse Inventory', group='Inventory')
          )
class ClientInventory(ClientInventory):

    objects = DomainEntityModelManager(filter={'type': 'ClientInventory'})

    @classmethod
    def get_search_form(cls, search_fields, fields):
        sf_instance = OrganizationDomainEntity.get_search_form(search_fields, fields)

        i_label,i_value = cls.get_search_value("infrastructure_unit",search_fields,fields)

        sf_instance.fields['infrastructure_unit'] = GenericModelChoiceField(label='Area', empty_label="Select Area", required=False,  initial=i_value, queryset=InfrastructureUnit.objects.filter(Q(type="Area")|Q(type='ModernTrade')|Q(type='HoReCo')), widget=forms.Select(attrs={'class': 'select2'}))
        sf_instance.fields['infrastructure_unit'].readonly = False

        c_label, c_value = cls.get_search_value('assigned_to', search_fields, fields)

        client_model = get_model("core", "Client")

        sf_instance.fields['assigned_to:id'] = GenericModelChoiceField(label='Client', empty_label="Select Client", required=False,  initial=c_value, queryset=Client.objects.all(), widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'infrastructure_unit', 'data-depends-property': 'assigned_to:id', 'data-url': reverse(client_model.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
        sf_instance.fields['assigned_to:id'].readonly = False

        return sf_instance

    @property
    def get_product_id(self):
        return self.product.pk if self.product else 0

    @property
    def get_product_name(self):
        return self.product.name if self.product else ''

    @property
    def get_product_description(self):
        return self.product.description if self.product else ''

    @property
    def get_distributor_id(self):
        return self.assigned_to.pk if self.assigned_to else 0

    @property
    def get_distributor_name(self):
        return self.assigned_to.name if self.assigned_to else ''

    @property
    def get_client_id(self):
        return self.assigned_to.pk if self.assigned_to else 0

    @property
    def get_client_name(self):
        return self.assigned_to.name if self.assigned_to else ''

    @classmethod
    def get_export_dependant_fields(self):
        class AdvancedExportDependentForm(GenericFormMixin):
            def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
                super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
                self.fields['infrastructure_unit__1_id'] = GenericModelChoiceField(label='Area', empty_label="Select Area", required=False, queryset=InfrastructureUnit.objects.filter(Q(type="Area")|Q(type='ModernTrade')|Q(type='HoReCo')), widget=forms.Select(attrs={'class': 'select2'}))
                self.fields['assigned_to___1__id'] = GenericModelChoiceField(label='Client', empty_label="Select Client", required=False, queryset=Client.objects.all(), widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'infrastructure_unit__1_id', 'data-depends-property': 'assigned_to:id', 'data-url': reverse(Client.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
        return AdvancedExportDependentForm

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.AdvancedDownload, ViewActionEnum.AdvancedImport, ViewActionEnum.AdvancedExport]

    @property
    def tabs_config(self):
        transaction_breakdown_set = TransactionBreakDown.objects.filter(product=self.product)
        transaction_set = get_model('sales', 'Transaction').objects.filter(client_id=self.assigned_to.pk, breakdown__in=transaction_breakdown_set)

        tabs = [TabView(
            title='Secondary Sale(s)',
            access_key='secondarysale',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='sales.SecondaryTransaction',
            queryset_filter=Q(**{'id__in': [x.id for x in transaction_set], 'transaction_type': TransactionType.Sale.value})
        ),TabView(
            title='Stock Count(s)',
            access_key='clientstockcount',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='foodex.ClientStockCount',
            queryset_filter=Q(**{'id__in': [x.id for x in transaction_set], 'transaction_type': TransactionType.StockCount.value})
        ),TabView(
            title='Stock In(s)',
            access_key='clientstockin',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='sales.PrimaryTransaction',
            queryset_filter=Q(**{'id__in': [x.id for x in transaction_set], 'transaction_type': TransactionType.StockAdjustment.value})
        )]
        return tabs

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        importer_config, created = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if created or importer_config.columns.count() == 0:
            importer_config.save(**kwargs)
        else:
            for items in importer_config.columns.all():
                items.delete()
        columns = [
            ImporterColumnConfig(column=0, column_name='Client Id', property_name='get_client_id', ignore=False),
            ImporterColumnConfig(column=1, column_name='Client Name', property_name='get_client_name', ignore=False),
            ImporterColumnConfig(column=2, column_name='Product Id', property_name='get_product_id', ignore=False),
            ImporterColumnConfig(column=3, column_name='Product Name', property_name='get_product_name', ignore=False),
            ImporterColumnConfig(column=4, column_name='Product Description', property_name='get_product_description', ignore=False),
            ImporterColumnConfig(column=5, column_name='Previous Stock Quantity', property_name='get_previous_stock_quantity', ignore=False),
            ImporterColumnConfig(column=6, column_name='Incoming Stock Quantity', property_name='get_incoming_stock_quantity', ignore=False),
        ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):

        client_id = int(data['0'])
        product_id = int(data['2'])
        incoming_stock_quantity = int(data['6'])
        product_objects = Product.objects.filter(pk=product_id)

        client_objects = Client.objects.filter(pk=client_id)

        if incoming_stock_quantity > 0 and product_objects.exists() and client_objects.exists():
            return data
        return []

    @property
    def render_code(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.pk}) + "' >" + self.code + "</a>")

    @property
    def render_distributor(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.assigned_to.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.assigned_to.pk}) + "' >" + str(self.assigned_to) + "</a>")


    @classmethod
    def table_columns(cls):
        return 'render_code', 'render_distributor', 'product', 'stock:Stock Quantity','last_updated', 'last_updated_by'

    @property
    def details_config(self):
        dic = super().details_config

        custom_list = ['code', 'product', 'last_updated']
        for key in dic:
            if key in custom_list:
                pass
            else:
                del dic[key]

        dic['Distributor'] = self.assigned_to
        dic['Stock quantity'] = self.stock
        dic['Last Updated By'] = self.last_updated_by

        return dic

    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        if not organization:
            organization = Organization.objects.all().first()
        exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
        if not exporter_configs.exists():
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.starting_row = 1
            exporter_config.save(**kwargs)
        else:
            for e in exporter_configs:
                e.delete()
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.save(**kwargs)

        columns = [
            ExporterColumnConfig(column=0, column_name='Client Id', property_name='get_client_id', ignore=False),
            ExporterColumnConfig(column=1, column_name='Client Name', property_name='get_client_name', ignore=False),
            ExporterColumnConfig(column=2, column_name='Product Id', property_name='get_product_id', ignore=False),
            ExporterColumnConfig(column=3, column_name='Product Name', property_name='get_product_name', ignore=False),
            ExporterColumnConfig(column=4, column_name='Product Description', property_name='get_product_description', ignore=False),
            ExporterColumnConfig(column=5, column_name='Previous Stock Quantity', property_name='get_previous_stock_quantity', ignore=False),
            ExporterColumnConfig(column=6, column_name='Incoming Stock Quantity', property_name='get_incoming_stock_quantity', ignore=False),
        ]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)
        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):
        # for column in columns:
        #     workbook.cell(row=row_number, column=column.column + 1).value = str(getattr(self, column.property_name))
        return self.pk, row_number + 1

    @classmethod
    def finalize_export(cls, workbook=None, row_number=None, query_set=None, **kwargs):
        #file_name = 'Product_price_config' + (Clock.now().strftime('%d-%m-%Y'))
        return workbook

    @classmethod
    def initialize_export(cls, workbook=None, columns=None, row_number=None, query_set=None, **kwargs):

        for column in columns:
            workbook.cell(row=1, column=column.column + 1).value = column.column_name

        row_number += 1

        query_params = kwargs.get('query_params')
        if query_params:
            client_id = query_params.get('assigned_to:id')
        try:
            client_id = int(client_id)
        except Exception:
            client_id = -1

        if client_id > 0:
            client_object = Client.objects.get(pk=client_id)
            product_objects = get_model("sales", "Product").objects.all()
            for product in product_objects:
                foodex_client_inventory = get_model('foodex','ClientInventory').objects.filter(product=product,assigned_to_id=int(client_id))
                for column in columns:
                    column_value = ''
                    if column.property_name == 'get_client_id':
                        column_value = str(client_id)
                    elif column.property_name == 'get_client_name':
                        column_value = client_object.name
                    if column.property_name == 'get_product_id':
                        column_value = product.pk
                    elif column.property_name == 'get_product_name':
                        column_value = product.name
                    elif column.property_name == 'get_product_description':
                        column_value = product.description
                    elif column.property_name == 'get_previous_stock_quantity':
                        column_value = foodex_client_inventory.order_by('-last_updated').first().stock if foodex_client_inventory.exists() else '0'
                    elif column.property_name == 'get_incoming_stock_quantity':
                        column_value = '0'

                    workbook.cell(row=row_number, column=column.column + 1).value = column_value
                row_number += 1

        return workbook, row_number

    def download_item(self, workbook=None, row_number=None, **kwargs):
        return self.pk, row_number + 1

    @classmethod
    def finalize_download(cls, workbook=None, row_number=None, query_set=None, **kwargs):
        return workbook

    @classmethod
    def initialize_download(cls, workbook=None, row_number=None, query_set=None, **kwargs):

        query_params = kwargs.get('query_params')
        client_id = query_params.get('assigned_to')

        query_params = kwargs.get('query_params')
        if query_params:
            client_id = query_params.get('assigned_to:id')
        try:
            client_id = int(client_id)
            client_object = Client.objects.get(pk=int(client_id))
        except Exception:
            client_id = -1
        if client_id > 0:
            workbook.page_setup.paperSize = workbook.PAPERSIZE_A4
            #ws.page_setup.orientation = ws.ORIENTATION_LANDSCAPE
            workbook.page_margins.top = 1.5
            workbook.page_margins.header = 0.25
            workbook.page_margins.bottom = 1.0
            workbook.page_margins.footer = 0.25
            workbook.page_margins.left = 0.75
            workbook.page_margins.right = 0.25

            # workbook.header_footer.center_header.style = Style(font=Font(bold=True, sz=24))
            # workbook.header_footer.center_footer.style = Style(font=Font(bold=True))
            workbook.header_footer.center_header.text = 'Foodex International\nClient Inventory\nClient: '+client_object.name
            workbook.header_footer.center_footer.text = '     Prepared By:                   '+\
                                                        '             Checked By                     Approved By\n'+\
                                                        'This page was generated at '+\
                                                        Clock.now().strftime('%d-%m-%Y %H:%M:%S') + ''

            workbook.header_footer.right_footer.text = '(Page &[Page]  of &[Pages])'

            column = 0
            workbook.cell(row=row_number, column=column + 1).style = Style(font=Font(bold=True))
            workbook.cell(row=row_number, column=column + 1).value = 'Product Code'
            column += 1
            workbook.cell(row=row_number, column=column + 1).style = Style(font=Font(bold=True))
            workbook.cell(row=row_number, column=column + 1).value = 'Product Name'
            column += 1
            workbook.cell(row=row_number, column=column + 1).style = Style(font=Font(bold=True))
            workbook.cell(row=row_number, column=column + 1).value = 'Description'
            column += 1
            workbook.cell(row=row_number, column=column + 1).style = Style(font=Font(bold=True),
                                                                           alignment=Alignment(horizontal='right'))
            workbook.cell(row=row_number, column=column + 1).value = 'Stock'
            column += 1

            workbook.column_dimensions['A'].width=15
            workbook.column_dimensions['B'].width=36
            workbook.column_dimensions['C'].width=20
            workbook.column_dimensions['D'].width=8

            row_number += 1

            product_objects = get_model("sales", "Product").objects.all()
            for product in product_objects:
                column = 0
                foodex_client_inventory = get_model('foodex','ClientInventory').objects.filter(product=product,assigned_to_id=int(client_id))

                workbook.cell(row=row_number, column=column + 1).style = Style(alignment=Alignment(horizontal='center', vertical='top'))
                workbook.cell(row=row_number, column=column + 1).value = product.code
                column += 1
                workbook.cell(row=row_number, column=column + 1).style = Style(alignment=Alignment(wrap_text=True, vertical='top'))
                workbook.cell(row=row_number, column=column + 1).value = product.name
                column += 1
                workbook.cell(row=row_number, column=column + 1).style = Style(alignment=Alignment(wrap_text=True,vertical='top'))
                workbook.cell(row=row_number, column=column + 1).value = product.description
                column += 1
                workbook.cell(row=row_number, column=column + 1).style = Style(alignment=Alignment(horizontal='right',vertical='top'))
                workbook.cell(row=row_number, column=column + 1).value = foodex_client_inventory.\
                    order_by('-last_updated').first().stock if foodex_client_inventory.exists() else '0'
                column += 1

                row_number += 1

        return workbook, row_number

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        self.type = self.__class__.__name__
        super().save(*args,**kwargs)

    class Meta:
        proxy = True

@receiver(import_completed, sender=ClientInventory)
def client_inventory_import_completed(sender, product=None, items=None, user=None, organization=None, **kwargs):

    #print("Import started...")
    organization = organization if organization else Organization.objects.all().first()
    client_id = -1
    for item in items:
        if item:
            try:
                client_id = int(item['0'])
                break
            except:
                client_id = -1

    if items and client_id > 0:
        tx_client_adjustment = PrimaryTransaction()
        tx_client_adjustment.organization = organization
        tx_client_adjustment.client_id = client_id
        tx_client_adjustment.transaction_type = TransactionType.StockAdjustment.value
        location = Location()
        location.save()
        tx_client_adjustment.location = location
        tx_client_adjustment.transaction_time = Clock.timestamp()
        tx_client_adjustment.unique_id = str(uuid4())
        tx_client_adjustment.save()


        tsubtotal = 0
        tdiscount = 0
        ttotal = 0

        for data in items:
            if not data:
                continue
            client_id = int(data['0'])
            product_id = int(data['2'])
            incoming_stock_quantity = int(data['6'])
            organization = Organization.objects.all().first()
            ClientInventory = get_model('foodex','ClientInventory')
            product_object = Product.objects.get(pk=product_id)

            txx = TransactionBreakDown()
            txx.product_id = product_id
            txx.quantity = incoming_stock_quantity

            txx.stock_received = 0

            product_unit_price = getattr(product_object,'get_gross_cust_price')

            txx.unit_price = product_unit_price
            txx.sub_total = txx.unit_price * txx.quantity
            txx.discount = 0
            txx.total = txx.sub_total - txx.discount
            txx.organization = organization
            txx.save()


            tx_client_adjustment.breakdown.add(txx)

            tsubtotal += txx.sub_total
            tdiscount += txx.discount
            ttotal += (txx.sub_total - txx.discount)

            client_inventory_objects = ClientInventory.objects.filter(assigned_to_id=client_id, product_id=product_id)

            if client_inventory_objects.exists():
                client_inventory_object = client_inventory_objects.first()
                client_inventory_object.stock += incoming_stock_quantity
                client_inventory_object.last_updated_by = user
                client_inventory_object.save()

            else:
                client_inventory_object = ClientInventory()
                client_inventory_object.assigned_to_id = client_id
                client_inventory_object.organization = organization
                client_inventory_object.product_id = product_id
                client_inventory_object.stock = incoming_stock_quantity
                client_inventory_object.created_by = user
                client_inventory_object.last_updated_by = user
                client_inventory_object.save()

        tx_client_adjustment.sub_total = tsubtotal
        tx_client_adjustment.discount = tdiscount
        tx_client_adjustment.total = ttotal
        tx_client_adjustment.save()
