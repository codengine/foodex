from django.conf.urls import patterns, url

from blackwidow.core.generics.views.dashboard_view import GenericDashboardView

urlpatterns = patterns('',
                       # -------------------- Dashboards ---------------------------------#
                       url(r'^execute/dashboard', GenericDashboardView.as_view(), name="admin_dashboard"),
                       url(r'^communication/dashboard', GenericDashboardView.as_view(), name="communication_dashboard"),
                       url(r'^reports/dashboard', GenericDashboardView.as_view(), name="reports_dashboard"),
                       )

