from blackwidow.core.mixins.viewmixin.protected_view_mixin import ProtectedViewMixin
from django.views.generic.base import TemplateView

__author__ = 'shamil'


class ErrorView404(ProtectedViewMixin, TemplateView):
    template_name = "templates/error/404.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get(self, request, *args, **kwargs):
        return super(ErrorView404, self).get(request, *args, **kwargs)

    def get_template_names(self):
        return ['templates/error/404.html']

