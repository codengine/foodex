__author__ = "auto generated"

from foodex.views.kpi.kpi_base_view import KPIBaseView
from foodex.views.kpi.monthly_sales_kpi_view import MonthlySalesKPIView
from foodex.views.kpi.monthly_sales_product_group_kpi_view import MonthlySalesProductGroupKPIView
from foodex.views.kpi.monthly_sales_product_kpi_view import MonthlySalesProductKPIView
from foodex.views.orders.create_order import CreateOrderWizardView
from foodex.views.orders.primary_pending_order import OrderCreateView


__all__ = ['MonthlySalesProductKPIView']
__all__ += ['MonthlySalesKPIView']
__all__ += ['CreateOrderWizardView']
__all__ += ['KPIBaseView']
__all__ += ['MonthlySalesProductGroupKPIView']
__all__ += ['OrderCreateView']
