from blackwidow.core.generics.views.create_view import GenericCreateView
from blackwidow.core.generics.views.create_wizard_view import GenericCreateWizardView
from blackwidow.engine.decorators.view_decorators import override_view
from blackwidow.sales.forms.orders.order_form import OrderForm
from blackwidow.sales.models.orders.order import Order
from config.enums.view_action_enum import ViewActionEnum


# @override_view(view=ViewActionEnum.Manage, model=Order)
class CreateOrderWizardView(GenericCreateWizardView):
    form_list = [OrderForm]
