from blackwidow.core.generics.views.create_view import GenericCreateView
from blackwidow.engine.decorators.view_decorators import override_view
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.orders.pending_order import PendingOrder

__author__="Sohel"

@override_view(view=ViewActionEnum.Create, model=PendingOrder)
class OrderCreateView(GenericCreateView):
    pass
