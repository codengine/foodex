from foodex.models.kpi.product_group_sales_kpi_monthly import ProductGroupSalesMonthlyKPI
from foodex.views.kpi.kpi_base_view import KPIBaseView
from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Sohel'

@decorate(override_view(model=ProductGroupSalesMonthlyKPI, view=ViewActionEnum.Manage))
class MonthlySalesProductGroupKPIView(KPIBaseView):

    def get_template_names(self):
        return ['kpi/product_group_kpi.html']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        kpi_filter_groups = self.model.get_unit_kpi_enabled_product_groups()
        context["kpi_filter_groups"] = kpi_filter_groups
        filter_group = None
        try:
            filter_group = int(self.request.GET.get("group:id")) if self.request.GET.get("filter") == "true" else None
        except:
            pass
        if not self.request.GET.get("group:id") and self.request.GET.get("filter") != "true":
            filter_group_object = kpi_filter_groups.first() if kpi_filter_groups.exists() else None
        else:
            kpi_filter_groups = kpi_filter_groups.filter(pk=filter_group)
            filter_group_object = kpi_filter_groups.first() if kpi_filter_groups.exists() else None

        context["filter_group"] = filter_group
        if filter_group_object:
            context["sub_heading"] = "Sales KPI for %s" % filter_group_object.name
        return context
