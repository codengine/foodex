from foodex.views.kpi.kpi_base_view import KPIBaseView
from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.kpi.sales_kpi_monthly import SalesKPIMonthly

__author__ = 'Sohel'

@decorate(override_view(model=SalesKPIMonthly, view=ViewActionEnum.Manage))
class MonthlySalesKPIView(KPIBaseView):
    pass

