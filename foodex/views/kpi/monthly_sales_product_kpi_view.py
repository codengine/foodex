from foodex.models.kpi.unit_product_sales_monthly_kpi import UnitProductSalesMonthlyKPI
from foodex.views.kpi.kpi_base_view import KPIBaseView
from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Sohel'

@decorate(override_view(model=UnitProductSalesMonthlyKPI, view=ViewActionEnum.Manage))
class MonthlySalesProductKPIView(KPIBaseView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        kpi_enabled_products = self.model.get_unit_kpi_enabled_products()
        context["kpi_filter_products"] = kpi_enabled_products
        filter_product = None
        try:
            filter_product = int(self.request.GET.get("product:id")) if self.request.GET.get("filter") == "true" else None
        except:
            pass
        if not self.request.GET.get("product:id") and self.request.GET.get("filter") != "true":
            filter_product = kpi_enabled_products.first() if kpi_enabled_products.exists() else None
        context["filter_product"] = filter_product
        if filter_product:
            context["sub_heading"] = "Sales KPI for %s" % filter_product.name
        return context
