from datetime import datetime
from blackwidow.core.generics.views.list_view import GenericListView

__author__ = 'Sohel'

class KPIBaseView(GenericListView):

    def get_template_names(self):
        return ['kpi/kpi_base.html']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["manage_buttons"] = self.get_manage_buttons()
        context["header_fixed_left_cols"] = self.model.get_left_cols_fixed()
        context["months"] = self.model.get_months()
        dt = datetime.utcnow()
        context["current_year"] = dt.year
        context["previous_year"] = dt.year - 1
        context["data_rows"] = self.model.get_kpi_data_rows(request=self.request)
        context["display_model"] = self.model.get_page_title();
        return context
