from django.db.models.aggregates import Sum
from django.db.models.loading import get_model
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.sales.models.transactions.transaction import PrimaryTransaction, SecondaryTransaction, TransactionType
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from foodex.models.clients.distributor import Distributor
from foodex.models.clients.horeco import HoReCo
from foodex.models.clients.modern_trade import ModernTrade
from foodex.models.kpi.target_product_group import TargetProductGroup

__author__ = 'Sohel'

class QueryHelper(object):
    @classmethod
    def get_total_sales_value_for_user(cls,user_id,start_time,end_time):
        user = ConsoleUser.objects.filter(pk=user_id)
        AreaSalesManager = get_model("foodex", "AreaSalesManager")
        HoReCaManager = get_model("foodex", "HoReCaManager")
        AssistantHoReCaManager = get_model("foodex", "AssistantHoReCaManager")
        ModernTradeManager = get_model("foodex", "ModernTradeManager")
        SeniorAccountsOfficer = get_model("foodex", "SeniorAccountsOfficer")
        ServicePerson = get_model("foodex", "ServicePerson")
        RegionalSalesManager = get_model("foodex", "RegionalSalesManager")
        if user.exists():
            user = user.first()
            if user.type == HoReCaManager.__name__ or user.type == AssistantHoReCaManager.__name__:
                if user.type == HoReCaManager.__name__:
                    user = HoReCaManager.objects.get(pk=user_id)
                else:
                    user = AssistantHoReCaManager.objects.get(pk=user_id)
                ###Get primary transaction.
                transactions = PrimaryTransaction.objects.filter(client__type=HoReCo.__name__,client_2__isnull=True,transaction_type=TransactionType.Sale.value)
                transactions = transactions.filter(transaction_time__gte=start_time,transaction_time__lte=end_time)
                total_sales = transactions.aggregate(total_sales = Sum('total'))['total_sales']
                return total_sales
            elif user.type == ModernTradeManager.__name__:
                user = ModernTradeManager.objects.get(pk=user_id)
                transactions = PrimaryTransaction.objects.filter(client__type=ModernTrade.__name__,client_2__isnull=True,transaction_type=TransactionType.Sale.value)
                transactions = transactions.filter(transaction_time__gte=start_time,transaction_time__lte=end_time)
                total_sales = transactions.aggregate(total_sales = Sum('total'))['total_sales']
                return total_sales
            elif user.type == SeniorAccountsOfficer.__name__:
                user = SeniorAccountsOfficer.objects.get(pk=user_id)
                transactions = PrimaryTransaction.objects.filter(client__type=ModernTrade.__name__,
                                                                 client__assigned_to__id=user.assigned_to_id,client_2__isnull=True,transaction_type=TransactionType.Sale.value)
                transactions = transactions.filter(transaction_time__gte=start_time,transaction_time__lte=end_time)
                total_sales = transactions.aggregate(total_sales = Sum('total'))['total_sales']
                return total_sales
            elif user.type == ServicePerson.__name__:
                user = ServicePerson.objects.get(pk=user_id)
                distributor_ids = Distributor.objects.filter(assigned_employee__role__id=user.role.pk,assigned_employee__users__id__in=[ user.pk ]).values_list('pk', flat=True)
                transactions = SecondaryTransaction.objects.filter(client__type=Distributor.__name__,
                                                                   client_id__in=distributor_ids,client_2__isnull=False,transaction_type=TransactionType.Sale.value)
                transactions = transactions.filter(transaction_time__gte=start_time,transaction_time__lte=end_time)
                total_sales = transactions.aggregate(total_sales = Sum('total'))['total_sales']
                return total_sales
            elif user.type == AreaSalesManager.__name__:
                user = AreaSalesManager.objects.get(pk=user_id)
                ###Read Primary Transactions.
                transactions = PrimaryTransaction.objects.filter(client__type=Distributor.__name__,client__assigned_to__id=user.assigned_to_id,
                                                                 client_2__isnull=True,transaction_type=TransactionType.Sale.value)
                transactions = transactions.filter(transaction_time__gte=start_time,transaction_time__lte=end_time)
                total_primary_sales = transactions.aggregate(total_sales = Sum('total'))['total_sales']
                dist_ids = user.get_all_distributor_under_same_area(flat=True)
                transactions = SecondaryTransaction.objects.filter(client_id__in=dist_ids,client_2__isnull=False,transaction_type=TransactionType.Sale.value)
                transactions = transactions.filter(transaction_time__gte=start_time,transaction_time__lte=end_time)
                total_secondary_sales = transactions.aggregate(total_sales = Sum('total'))['total_sales']
                total_sales = 0
                if total_primary_sales:
                    total_sales += total_primary_sales
                if total_secondary_sales:
                    total_sales += total_secondary_sales
                return total_sales
            elif user.type == RegionalSalesManager.__name__:
                user = RegionalSalesManager.objects.get(pk=user_id)
                ###Read Primary Transactions.
                transactions = PrimaryTransaction.objects.filter(client__type=Distributor.__name__,client__assigned_to__parent__id=user.assigned_to_id,
                                                                 client_2__isnull=True,transaction_type=TransactionType.Sale.value)
                transactions = transactions.filter(transaction_time__gte=start_time,transaction_time__lte=end_time)
                total_primary_sales = transactions.aggregate(total_sales = Sum('total'))['total_sales']

                dist_ids = user.get_all_distributor_under_same_region(flat=True)
                transactions = SecondaryTransaction.objects.filter(client_id__in=dist_ids,client_2__isnull=False,transaction_type=TransactionType.Sale.value)
                transactions = transactions.filter(transaction_time__gte=start_time,transaction_time__lte=end_time)
                total_secondary_sales = transactions.aggregate(total_sales = Sum('total'))['total_sales']
                total_sales = 0
                if total_primary_sales:
                    total_sales += total_primary_sales
                if total_secondary_sales:
                    total_sales += total_secondary_sales
                return total_sales

    @classmethod
    def get_total_unit_product_sales_for_user(cls, user_id, product_id, start_time, end_time):
        user = ConsoleUser.objects.filter(pk=user_id)
        AreaSalesManager = get_model("foodex", "AreaSalesManager")
        HoReCaManager = get_model("foodex", "HoReCaManager")
        AssistantHoReCaManager = get_model("foodex", "AssistantHoReCaManager")
        ModernTradeManager = get_model("foodex", "ModernTradeManager")
        SeniorAccountsOfficer = get_model("foodex", "SeniorAccountsOfficer")
        ServicePerson = get_model("foodex", "ServicePerson")
        RegionalSalesManager = get_model("foodex", "RegionalSalesManager")
        if user.exists():
            user = user.first()
            if user.type == HoReCaManager.__name__ or user.type == AssistantHoReCaManager.__name__:
                if user.type == HoReCaManager.__name__:
                    user = HoReCaManager.objects.get(pk=user_id)
                else:
                    user = AssistantHoReCaManager.objects.get(pk=user_id)
                ###Get primary transaction.
                transactions = PrimaryTransaction.objects.filter(client__type=HoReCo.__name__,client_2__isnull=True,
                                                                 transaction_type=TransactionType.Sale.value)
                transactions = transactions.filter(transaction_time__gte=start_time,transaction_time__lte=end_time)

                tx_ids = transactions.values_list('pk',flat=True)

                tx_breakdown = TransactionBreakDown.objects.filter(product_id=product_id,transaction__id__in=tx_ids).aggregate(Sum('total'))

                total_sales = tx_breakdown['total__sum']
                return total_sales

            elif user.type == ModernTradeManager.__name__:
                user = ModernTradeManager.objects.get(pk=user_id)
                transactions = PrimaryTransaction.objects.filter(client__type=ModernTrade.__name__,client_2__isnull=True,
                                                                 transaction_type=TransactionType.Sale.value)
                transactions = transactions.filter(transaction_time__gte=start_time,transaction_time__lte=end_time)
                tx_ids = transactions.values_list('pk',flat=True)
                tx_breakdown = TransactionBreakDown.objects.filter(product_id=product_id,transaction__id__in=tx_ids).aggregate(Sum('total'))
                total_sales = tx_breakdown['total__sum']
                return total_sales
            elif user.type == SeniorAccountsOfficer.__name__:
                user = SeniorAccountsOfficer.objects.get(pk=user_id)
                transactions = PrimaryTransaction.objects.filter(client__type=ModernTrade.__name__,
                                                                 client__assigned_to__id=user.assigned_to_id,client_2__isnull=True,transaction_type=TransactionType.Sale.value)
                transactions = transactions.filter(transaction_time__gte=start_time,transaction_time__lte=end_time)
                tx_ids = transactions.values_list('pk',flat=True)
                tx_breakdown = TransactionBreakDown.objects.filter(product_id=product_id,transaction__id__in=tx_ids).aggregate(Sum('total'))
                total_sales = tx_breakdown['total__sum']
                return total_sales
            elif user.type == ServicePerson.__name__:
                user = ServicePerson.objects.get(pk=user_id)
                distributor_ids = Distributor.objects.filter(assigned_employee__role__id=user.role.pk,assigned_employee__users__id__in=[ user.pk ]).values_list('pk', flat=True)

                transactions = SecondaryTransaction.objects.filter(client__type=Distributor.__name__,
                                                                   client_id__in=distributor_ids,client_2__isnull=False,transaction_type=TransactionType.Sale.value)
                transactions = transactions.filter(transaction_time__gte=start_time,transaction_time__lte=end_time)
                tx_ids = transactions.values_list('pk',flat=True)
                tx_breakdown = TransactionBreakDown.objects.filter(product_id=product_id,transaction__id__in=tx_ids).aggregate(Sum('total'))
                total_sales = tx_breakdown['total__sum']
                return total_sales
            elif user.type == AreaSalesManager.__name__:
                user = AreaSalesManager.objects.get(pk=user_id)
                ###Read Primary Transactions.
                transactions = PrimaryTransaction.objects.filter(client__type=Distributor.__name__,
                                                                 client__assigned_to__id=user.assigned_to_id,client_2__isnull=True,transaction_type=TransactionType.Sale.value)
                transactions = transactions.filter(transaction_time__gte=start_time,transaction_time__lte=end_time)
                tx_ids = transactions.values_list('pk',flat=True)
                tx_breakdown = TransactionBreakDown.objects.filter(product_id=product_id,transaction__id__in=tx_ids).aggregate(Sum('total'))
                total_primary_sales = tx_breakdown['total__sum']

                dist_ids = user.get_all_distributor_under_same_area(flat=True)
                transactions = SecondaryTransaction.objects.filter(client_id__in=dist_ids,client_2__isnull=False,
                                                                   transaction_type=TransactionType.Sale.value)
                transactions = transactions.filter(transaction_time__gte=start_time,transaction_time__lte=end_time)
                tx_ids = transactions.values_list('pk',flat=True)
                tx_breakdown = TransactionBreakDown.objects.filter(product_id=product_id,transaction__id__in=tx_ids).aggregate(Sum('total'))
                total_secondary_sales = tx_breakdown['total__sum']
                total_sales = 0
                if total_primary_sales:
                    total_sales += total_primary_sales
                if total_secondary_sales:
                    total_sales += total_secondary_sales
                return total_sales
            elif user.type == RegionalSalesManager.__name__:
                user = RegionalSalesManager.objects.get(pk=user_id)
                ###Read Primary Transactions.
                transactions = PrimaryTransaction.objects.filter(client__type=Distributor.__name__,client__assigned_to__parent__id=user.assigned_to_id,
                                                                 client_2__isnull=True,transaction_type=TransactionType.Sale.value)
                transactions = transactions.filter(transaction_time__gte=start_time,transaction_time__lte=end_time)
                tx_ids = transactions.values_list('pk',flat=True)
                tx_breakdown = TransactionBreakDown.objects.filter(product_id=product_id,transaction__id__in=tx_ids).aggregate(Sum('total'))
                total_primary_sales = tx_breakdown['total__sum']

                dist_ids = user.get_all_distributor_under_same_region(flat=True)
                transactions = SecondaryTransaction.objects.filter(client_id__in=dist_ids,client_2__isnull=False,
                                                                   transaction_type=TransactionType.Sale.value)
                transactions = transactions.filter(transaction_time__gte=start_time,transaction_time__lte=end_time)
                tx_ids = transactions.values_list('pk',flat=True)
                tx_breakdown = TransactionBreakDown.objects.filter(product_id=product_id,transaction__id__in=tx_ids).aggregate(Sum('total'))
                total_secondary_sales = tx_breakdown['total__sum']
                total_sales = 0
                if total_primary_sales:
                    total_sales += total_primary_sales
                if total_secondary_sales:
                    total_sales += total_secondary_sales
                return total_sales

    @classmethod
    def update_group_kpi_from_unit_product_kpis(cls, group_id, year, month, user_id, **kwargs):

        UnitProductSalesMonthlyKPI = get_model("foodex", "UnitProductSalesMonthlyKPI")
        ProductGroupSalesMonthlyKPI = get_model("foodex", "ProductGroupSalesMonthlyKPI")

        product_group = TargetProductGroup.objects.get(pk=group_id)
        product_ids = product_group.products.values_list('pk', flat=True)

        unit_product_achieved = UnitProductSalesMonthlyKPI.objects.filter(user_id=user_id, month=month, product_id__in=product_ids, created_from_group=True).aggregate(total_achieved=Sum('achieved'))

        product_group_sales_kpis = ProductGroupSalesMonthlyKPI.objects.filter(user_id=user_id, year=year, month=month,group_id=group_id)
        if product_group_sales_kpis.exists():
            product_group_sales_kpi = product_group_sales_kpis.first()
            product_group_sales_kpi.achieved = unit_product_achieved['total_achieved'] if unit_product_achieved['total_achieved'] else 0
            product_group_sales_kpi.save()


