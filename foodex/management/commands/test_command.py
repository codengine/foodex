import copy
import os
import re

from django.core.management.base import BaseCommand

from config.apps import INSTALLED_APPS
from foodex.models.kpi.product_group_sales_kpi_monthly import ProductGroupSalesMonthlyKPI

PROJECT_PATH = os.path.abspath(".")


class Command(BaseCommand):
    def handle(self, *args, **options):
        print("Starting...")
        ProductGroupSalesMonthlyKPI.initialize_product_group_targets()
        # ProductGroupSalesMonthlyKPI.initialize_unit_product_targets()
        # ProductGroupSalesMonthlyKPI.objects.all().delete()
        # print(ProductGroupSalesMonthlyKPI.objects.all().count())
        print("Ended")
