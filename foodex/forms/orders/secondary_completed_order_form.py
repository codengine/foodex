from collections import OrderedDict
from django.core.urlresolvers import reverse
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.sales.forms.orders.secondary_order_form import SecondaryOrderForm
from blackwidow.sales.models.orders.secondary_completed_order import SecondaryCompletedOrder
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.clients.distributor import Distributor
from django import forms
from foodex.models.clients.retailers import Retailers
from foodex.models.infrastructure.area import Area
from foodex.models.infrastructure.routes import Route


class SecondaryCompletedOrderForm(SecondaryOrderForm):
    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        kwargs["depends_on"] = "from_client"
        kwargs["depends_property"] = "_search_client_inventory:assigned_to:id"
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)
        area_initial = None
        try:
            area_initial = instance.from_client.assigned_to
        except:
            area_initial = None

        from_client_initial = None
        try:
            from_client_initial = instance.from_client
        except:
            from_client_initial = None

        route_initial = None
        try:
            route_initial = Retailers.objects.get(pk=instance.to_client.pk).render_assign_route
        except:
            route_initial = None

        to_client_initial = None
        try:
            to_client_initial = instance.to_client
        except:
            to_client_initial = None

        self.fields['area'] = GenericModelChoiceField(label='Area', queryset=Area.objects.all(), widget=forms.Select(attrs={'class': 'select2'}), initial=area_initial)
        self.fields['from_client'] = GenericModelChoiceField(label='From Distributor',queryset=Distributor.objects.all(), initial=from_client_initial, widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'area', 'data-depends-property': 'assigned_to:id', 'data-url': reverse(Distributor.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
        self.fields['route'] = GenericModelChoiceField(label='Route', queryset=Route.objects.all(), initial= route_initial, widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'from_client', 'data-depends-property': 'parent_client:id', 'data-url': reverse(Route.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
        self.fields['to_client'] = GenericModelChoiceField(label='To Retailer',queryset=Retailers.objects.all(), initial=to_client_initial, widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'route', 'data-depends-property': '_search_route:id', 'data-url': reverse(Retailers.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))

        original_fields = self.fields
        ordered_fields = ['area', 'from_client', 'route', 'to_client']
        new_order = OrderedDict()
        for key in ordered_fields:
            new_order[key] = original_fields[key]
        self.fields = new_order

    class Meta(SecondaryOrderForm.Meta):
        model = SecondaryCompletedOrder