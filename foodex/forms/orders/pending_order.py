__author__ = 'Sohel'

from blackwidow.sales.forms.orders.order_form import OrderForm
from blackwidow.sales.models.orders.pending_order import PendingOrder
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.models.clients.client import Client
from django import forms
from blackwidow.sales.forms.orders.pending_order_form import *


class PendingOrderForm(PendingOrderForm):
    class Meta(OrderForm.Meta):
        model = PendingOrder
