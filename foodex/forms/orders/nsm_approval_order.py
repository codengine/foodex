from foodex.models.orders.OrderNSMApproval import OrderNSMApproval

__author__ = 'Sohel'

from blackwidow.sales.forms.orders.pending_order_form import *


class NSMApprovalOrderForm(PendingOrderForm):

    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)

    class Meta(PendingOrderForm.Meta):
        model = OrderNSMApproval

