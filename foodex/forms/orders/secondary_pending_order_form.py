from collections import OrderedDict
from django.core.urlresolvers import reverse
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.sales.forms.orders.secondary_order_form import SecondaryOrderForm
from blackwidow.sales.models.orders.order_history import OrderHistory
from blackwidow.sales.models.orders.order_history_breakdown import OrderHistoryBreakdown
from blackwidow.sales.models.orders.secondary_pending_order import SecondaryPendingOrder
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.clients.distributor import Distributor
from django import forms
from foodex.models.clients.retailers import Retailers
from foodex.models.infrastructure.area import Area
from foodex.models.infrastructure.routes import Route
from django.contrib import messages
from django import forms


class SecondaryPendingOrderForm(SecondaryOrderForm):
    route = forms.IntegerField()

    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        kwargs["depends_on"] = "from_client"
        kwargs["depends_property"] = "_search_client_inventory:assigned_to:id"
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)

        from_client_initial = instance.from_client if instance is not None else None

        area_initial = instance.from_client.assigned_to if from_client_initial is not None else None

        to_client_initial = instance.to_client if instance is not None else None

        route_initial = Route.objects.filter(
            assigned_clients__clients__pk=to_client_initial.pk).first() if to_client_initial is not None else None

        area_queryset = Area.get_role_based_queryset(queryset=Area.objects.all())
        from_client_queryset = Distributor.get_role_based_queryset(queryset=Distributor.objects.all())
        route_queryset = Route.objects.all()
        to_client_queryset = Retailers.objects.all()

        area_queryset = area_queryset if area_initial is None else area_queryset.filter(pk=area_initial.pk)
        from_client_queryset = from_client_queryset if from_client_initial is None else from_client_queryset.filter(
            pk=from_client_initial.pk)
        route_queryset = route_queryset if route_initial is None else route_queryset.filter(pk=route_initial.pk)
        to_client_queryset = to_client_queryset if to_client_initial is None else to_client_queryset.filter(
            pk=to_client_initial.pk)

        if instance:
            self.fields['area'] = GenericModelChoiceField(label='Area', queryset=area_queryset,
                                                          widget=forms.Select(attrs={'class': 'select2'}),
                                                          initial=area_initial)

            self.fields['from_client'] = GenericModelChoiceField(label='From Distributor',
                                                                 queryset=from_client_queryset,
                                                                 initial=from_client_initial,
                                                                 widget=forms.Select(attrs={'class': 'select2'}))

            self.fields['route'] = GenericModelChoiceField(label='Route', queryset=route_queryset,
                                                           initial=route_initial,
                                                           widget=forms.Select(attrs={'class': 'select2'}))

            self.fields['to_client'] = GenericModelChoiceField(label='To Retailer', queryset=to_client_queryset,
                                                               initial=to_client_initial,
                                                               widget=forms.Select(attrs={'class': 'select2'}))

        else:
            self.fields['area'] = GenericModelChoiceField(label='Area', queryset=area_queryset,
                                                          widget=forms.Select(attrs={'class': 'select2'}),
                                                          initial=area_initial)

            self.fields['from_client'] = GenericModelChoiceField(label='From Distributor',
                                                                 queryset=from_client_queryset,
                                                                 initial=from_client_initial, widget=forms.TextInput(
                    attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'area',
                           'data-depends-property': 'assigned_to:id', 'data-url': reverse(Distributor.get_route_name(
                            ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))

            self.fields['route'] = GenericModelChoiceField(label='Route', queryset=route_queryset,
                                                           initial=route_initial,
                                                           widget=forms.TextInput(
                                                               attrs={'class': 'select2-input', 'width': '220',
                                                                      'data-depends-on': 'from_client',
                                                                      'data-depends-property': 'parent_client:id',
                                                                      'data-url': reverse(Route.get_route_name(
                                                                          action=ViewActionEnum.Manage)) +
                                                                                  "?format=json&search=1&disable_pagination=1"}))

            self.fields['to_client'] = GenericModelChoiceField(label='To Retailer',
                                                               queryset=to_client_queryset,
                                                               initial=to_client_initial,
                                                               widget=forms.TextInput(
                                                                   attrs={'class': 'secondary_client select2-input',
                                                                          'width': '220', 'data-depends-on': 'route',
                                                                          'data-depends-property': '_search_route:id',
                                                                          'data-url': reverse(Retailers.get_route_name(
                                                                              action=ViewActionEnum.Manage)) +
                                                                                      "?format=json&search=1&disable_pagination=1"}))

        original_fields = self.fields
        ordered_fields = ['area', 'from_client', 'route', 'to_client']
        new_order = OrderedDict()
        for key in ordered_fields:
            new_order[key] = original_fields[key]
        self.fields = new_order

    def clean(self):
        cleaned_data = super().clean()
        breakdown_form = self.child_forms[0][1]
        breakdown_set = breakdown_form.cleaned_data
        product_flag = False
        for breakdown in breakdown_set:
            if 'product' in breakdown:
                product_flag = True

        if not product_flag:
            message = 'Minimum one order breakdown entry is required'
            messages.error(self.request, message)
            raise forms.ValidationError(message)
        return cleaned_data

    def add_breakdowns(self, blist):
        b_obj = blist[0]
        tnumber_of_packet = 0
        titems_per_packet = 0
        tloose_items = 0
        ttotal_items = 0
        tunit_price = 0
        tsub_total = 0
        tdiscount = 0
        ttotal = 0
        exclude_list = []
        for b in blist:
            tnumber_of_packet += b.number_of_packet
            titems_per_packet += b.items_per_packet
            tloose_items += b.loose_items
            ttotal_items += b.total_items
            tunit_price += b.unit_price
            tsub_total += b.sub_total
            tdiscount += b.discount
            ttotal += b.total
            if b.pk != b_obj.pk:
                exclude_list += [b]
        b_obj.number_of_packet = tnumber_of_packet
        b_obj.items_per_packet = titems_per_packet
        b_obj.loose_items = tloose_items
        b_obj.total_items = ttotal_items
        b_obj.unit_price = tunit_price
        b_obj.sub_total = tsub_total
        b_obj.discount = tdiscount
        b_obj.total = ttotal
        b_obj.save()
        return b_obj, exclude_list

    def save(self, commit=True):
        if self.instance is not None:
            has_instance = True
        else:
            has_instance = False
        super().save(commit)
        breakdown_set = self.instance.breakdown.all()
        b_groups = {}
        for b in breakdown_set:
            if not b.product in b_groups.keys():
                b_groups[b.product] = [b]
            else:
                b_groups[b.product] += [b]

        for key, items in b_groups.items():
            b_obj, exclude_list = self.add_breakdowns(items)
            for e_item in exclude_list:
                self.instance.breakdown.remove(e_item)
                e_item.delete()

        order_history = OrderHistory.objects.filter(object_id=self.instance.pk).order_by('-date_created').first()
        if has_instance:
            order_history.breakdown.clear()
        for breakdown in self.instance.breakdown.all():
            order_history_breakdown = OrderHistoryBreakdown()
            order_history_breakdown.product = breakdown.product
            order_history_breakdown.organization_id = breakdown.organization_id

            order_history_breakdown.number_of_packet = breakdown.number_of_packet
            order_history_breakdown.items_per_packet = breakdown.items_per_packet
            order_history_breakdown.loose_items = breakdown.loose_items
            order_history_breakdown.total_items = breakdown.total_items
            order_history_breakdown.unit_price = breakdown.unit_price
            order_history_breakdown.sub_total = breakdown.sub_total
            order_history_breakdown.discount = breakdown.discount
            order_history_breakdown.total = breakdown.total
            order_history_breakdown.date_created = breakdown.date_created
            order_history_breakdown.last_updated = breakdown.last_updated
            order_history_breakdown.save()

            order_history.breakdown.add(order_history_breakdown)
        return self.instance

    class Meta(SecondaryOrderForm.Meta):
        model = SecondaryPendingOrder
