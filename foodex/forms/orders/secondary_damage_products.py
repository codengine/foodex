
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.sales.models.products.secondary_damage_products import SecondaryDamagedProducts
from django import forms


class SecondaryDamagedProductsForm(GenericFormMixin):
    class Meta:
        model = SecondaryDamagedProducts
        fields = ['from_client', 'to_client', 'product', 'quantity']
        widgets = {
            'from_client': forms.Select(attrs={'class': 'select2'}),
            'to_client': forms.Select(attrs={'class': 'select2'}),
            'product': forms.Select(attrs={'class': 'select2'})
        }

