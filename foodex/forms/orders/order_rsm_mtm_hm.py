from blackwidow.sales.forms.orders.pending_order_form import PendingOrderForm
from foodex.models.orders.order_rsm_mtm_hm import OrderRSMMTMHMApproval

__author__ = 'Sohel'

class RSMApprovalOrderForm(PendingOrderForm):
    class Meta(PendingOrderForm.Meta):
        model = OrderRSMMTMHMApproval

