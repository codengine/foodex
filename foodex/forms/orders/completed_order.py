from blackwidow.sales.models.transactions.transaction import Transaction
from foodex.models.transactions.delivery_transaction import DeliveryTransaction
from foodex.models.transactions.delivery_transaction_braakdown import DeliveryTransactionBreakDown

__author__ = 'Sohel'

from foodex.models.orders.completed_order import CompletedOrder
from blackwidow.sales.forms.orders.pending_order_form import *


class CompletedOrderForm(PendingOrderForm):
    class Meta(OrderForm.Meta):
        model = CompletedOrder

    def save(self, commit=True):
        super().save(commit)

        if self.instance.invoice:
            total_transaction_value = self.instance.breakdown.all().aggregate(Sum('total'))['total__sum']
            discount = self.instance.discount if self.instance.discount is not None else 0
            total_discount_value = (total_transaction_value*discount)/100

            gross_discount_value = self.instance.gross_discount if self.instance.gross_discount else 0

            tt_value_with_discount = total_transaction_value - total_discount_value - gross_discount_value

            invoice = self.instance.invoice
            invoice.price_total = tt_value_with_discount
            invoice.save()

            delivery_transactions = Transaction.objects.filter(invoice_id=invoice.pk)
            if delivery_transactions.exists():
                delivery_transaction = delivery_transactions.first()


                tsubtotal = 0
                tdiscount = 0
                ttotal = 0

                noted_entries = []

                for ob in self.instance.breakdown.all():

                    already_added = False

                    if delivery_transaction.breakdown.filter(product_id=ob.product.pk):
                        txx = delivery_transaction.breakdown.filter(product_id=ob.product.pk).first()
                        already_added = True
                    else:
                        txx = DeliveryTransactionBreakDown()
                        txx.product = ob.product
                    txx.quantity = ob.total_items

                    txx.unit_price = ob.unit_price
                    txx.sub_total = txx.unit_price * txx.quantity
                    txx.discount = ob.discount
                    txx.total = txx.sub_total - txx.discount
                    txx.save()

                    if not already_added:
                        delivery_transaction.breakdown.add(txx)

                    tsubtotal += txx.sub_total
                    tdiscount += txx.discount
                    ttotal += (txx.sub_total - txx.discount)

                    noted_entries += [ txx.pk ]

                delivery_transaction.sub_total = tsubtotal
                delivery_transaction.discount = total_discount_value
                delivery_transaction.gross_discount = gross_discount_value
                delivery_transaction.total = tt_value_with_discount
                delivery_transaction.save()

                remaining_breakdown = delivery_transaction.breakdown.all().exclude(pk__in=noted_entries)
                for rm_entry in remaining_breakdown:
                    delivery_transaction.breakdown.remove(rm_entry)
                    rm_entry.delete()

        return self.instance

