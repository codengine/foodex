from django.forms.models import modelformset_factory
from blackwidow.core.forms.common.email_address_form import EmailAddressForm
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from django import forms
from blackwidow.core.forms.users.form import ConsoleUserForm
from blackwidow.core.mixins.formmixin.form_mixin import GenericModelFormSetMixin
from blackwidow.core.models.common.emailaddress import EmailAddress
from blackwidow.core.models.roles.role import Role
from foodex.models.infrastructure.area import Area
from foodex.models.infrastructure.region import Region
from foodex.models.users.horeca_manager import HoReCaManager

__author__ = 'Sohel'

email_form_set = modelformset_factory(EmailAddress, form=EmailAddressForm, formset=GenericModelFormSetMixin, extra=0, min_num=1, validate_min=False)


class HoReCaManagerForm(ConsoleUserForm):
    def save(self, commit=True):
        role = Role.objects.filter(name=HoReCaManager.__name__)[0]
        self.instance.role = role
        return super().save(commit)

    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)
        prefix = prefix + '-' if prefix != '' else prefix
        self.fields['role'].initial = Role.objects.filter(name=HoReCaManager.__name__)[0].id

    class Meta(ConsoleUserForm.Meta):
        model = HoReCaManager
        fields = ['name', 'role']
        widgets = {
            'role': forms.HiddenInput()
        }
        labels = {
            'name': 'Full name',
        }