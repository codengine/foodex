from django.forms.models import modelformset_factory
from blackwidow.core.forms.common.email_address_form import EmailAddressForm
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from django import forms
from blackwidow.core.forms.users.form import ConsoleUserForm
from blackwidow.core.mixins.formmixin.form_mixin import GenericModelFormSetMixin
from blackwidow.core.models.common.emailaddress import EmailAddress
from blackwidow.core.models.roles.role import Role
from foodex.models.infrastructure.area import Area
from foodex.models.users.area_manager import AreaSalesManager

__author__ = 'Sohel'

email_form_set = modelformset_factory(EmailAddress, form=EmailAddressForm, formset=GenericModelFormSetMixin, extra=0, min_num=1, validate_min=False)


class AreaManagerForm(ConsoleUserForm):
    def save(self, commit=True):
        role = Role.objects.filter(name=AreaSalesManager.__name__)[0]
        self.instance.role = role
        return super().save(commit)

    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)
        prefix = prefix + '-' if prefix != '' else prefix
        #self.add_child_form("emails", email_form_set(data=data, queryset=instance.emails.all() if instance is not None else EmailAddress.objects.none(),files=files, header='Email', prefix=prefix + 'suffix-' + str(len(self.suffix_child_forms)), **kwargs))
        self.fields['role'].initial = Role.objects.filter(name=AreaSalesManager.__name__)[0].id
        self.fields['assigned_to'] = GenericModelChoiceField(queryset=Area.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
        self.fields['assigned_to'].label = "Assigned area"

    class Meta(ConsoleUserForm.Meta):
        model = AreaSalesManager
        fields = ['name', 'role', 'assigned_to']
        widgets = {
            'role': forms.HiddenInput()
        }
        labels = {
            'name': 'Full name',
            'assigned_to': 'Assigned area',
        }