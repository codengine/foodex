from django.db import transaction
from blackwidow.core.forms.common.education_form import EQualificationForm
from blackwidow.core.forms.common.email_address_form import EmailAddressForm
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from django import forms
from blackwidow.core.forms.users.form import ConsoleUserForm
from blackwidow.core.models import InfrastructureUserAssignment
from blackwidow.core.models.roles.role import Role
from foodex.models.clients.distributor import Distributor
from foodex.models.infrastructure.routes import Route
from foodex.models.users.service_persons import ServicePerson

__author__ = 'Mahmud'


class ServicePersonForm(ConsoleUserForm):

    def save(self, commit=True):
        with transaction.atomic():
            role = Role.objects.filter(name=ServicePerson.__name__)[0]
            distributor = self.cleaned_data['distributor']
            self.instance.role = role
            service_person_instance = super().save(commit)

            ###Now check if any distributor has assigned this service person as assigned employee.
            all_dist = Distributor.objects.all()
            for dist in all_dist:
                assigned_employees = dist.assigned_employee.filter(role=role)
                if assigned_employees.exists():
                    sps = dist.assigned_employee.all().first().users.filter(pk=self.instance.pk)
                    if sps.exists():
                        dist.assigned_employee.all().first().users.remove(self.instance)
                        break

            assigned_employee = distributor.assigned_employee.filter(role=role)
            if assigned_employee.exists():
                distributor.assigned_employee.all().first().users.add(service_person_instance)
            else:
                user_assignment = InfrastructureUserAssignment()
                user_assignment.role = role
                user_assignment.save(commit)
                user_assignment.users.add(service_person_instance)
                user_assignment.save(commit)
                distributor.assigned_employee.add(user_assignment)

            return service_person_instance



    def __init__(self, data=None, files=None, instance=None, **kwargs):
        super().__init__(data=data, files=files, instance=instance, **kwargs)
        self.fields['role'].initial = Role.objects.filter(name=ServicePerson.__name__)[0].id
        self.add_child_form("education", EQualificationForm(data=data, files=files, instance=instance.education if instance is not None else None, form_header='Educational Qualification', **kwargs))
        self.fields["distributor"] = GenericModelChoiceField(label='Distributor', initial=self.instance.render_distributor if self.instance.pk else None, queryset=Distributor.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))

    class Meta(ConsoleUserForm.Meta):
        model = ServicePerson
        fields = ['name','date_of_birth','role']
        widgets = {
            'role': forms.HiddenInput(),
            'date_of_birth': forms.TextInput(attrs={'class': 'date-time-picker', 'data-format': "dd/MM/yyyy"}),
        }