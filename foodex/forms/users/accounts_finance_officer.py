from django.forms.models import modelformset_factory
from blackwidow.core.forms.common.email_address_form import EmailAddressForm
from django import forms
from blackwidow.core.forms.users.form import ConsoleUserForm
from blackwidow.core.mixins.formmixin.form_mixin import GenericModelFormSetMixin
from blackwidow.core.models.common.emailaddress import EmailAddress
from blackwidow.core.models.roles.role import Role
from foodex.models.users.accounts_finance_officer import AccountsFinanceOfficer

__author__ = 'bahar'

email_form_set = modelformset_factory(EmailAddress, form=EmailAddressForm, formset=GenericModelFormSetMixin, extra=0, min_num=1, validate_min=False)


class AccountsFinanceOfficerForm(ConsoleUserForm):
    def save(self, commit=True):
        role = Role.objects.filter(name=AccountsFinanceOfficer.__name__)[0]
        self.instance.role = role
        return super().save(commit)

    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)
        prefix = prefix + '-' if prefix != '' else prefix
        #self.add_child_form("emails", email_form_set(data=data, queryset=instance.emails.all() if instance is not None else EmailAddress.objects.none(),files=files, header='Email', prefix=prefix + 'suffix-' + str(len(self.suffix_child_forms)), **kwargs))
        self.fields['role'].initial = Role.objects.filter(name=AccountsFinanceOfficer.__name__)[0].id

    class Meta(ConsoleUserForm.Meta):
        model = AccountsFinanceOfficer
        fields = ['name', 'role']
        widgets = {
            'role': forms.HiddenInput()
        }
        labels = {
            'name': 'Full name'
        }