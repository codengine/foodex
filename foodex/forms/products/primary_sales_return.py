from collections import OrderedDict
from django import forms
from django.db.models.query_utils import Q
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.clients.client import Client
from foodex.models.clients.distributor import Distributor
from foodex.models.clients.horeco import HoReCo
from foodex.models.clients.modern_trade import ModernTrade
from foodex.models.clients.wholesale import WholeSaler
from foodex.models.products.primary_sales_return import PrimarySalesReturn

__author__ = 'Sohel'

class PrimarySalesReturnForm(GenericFormMixin):
    def __init__(self, data=None, files=None,  **kwargs):
        super().__init__(data=data, files=files, **kwargs)
        self.fields['client'] = GenericModelChoiceField(label='Distributor',queryset=Client.objects.filter(Q(type=Distributor.__name__) | Q(type=HoReCo.__name__) | Q(type=ModernTrade.__name__) | Q(type=WholeSaler.__name__)), widget=forms.Select(attrs={'class': 'select2'}))

        original_fields = self.fields
        ordered_fields = ['client']
        new_order = OrderedDict()
        for key in ordered_fields:
            new_order[key] = original_fields[key]
        self.fields = new_order

    def save(self, commit=True):
        super().save(commit)
        return self.instance

    class Meta:
        model = PrimarySalesReturn
        fields = ['invoice','product','quantity']
        widgets = {
            'product': forms.Select(attrs={'class': 'select2'}),
        }
