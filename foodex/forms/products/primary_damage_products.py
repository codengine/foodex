from collections import OrderedDict
from django.core.urlresolvers import reverse
from django.db.models.loading import get_model
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from django import forms
from blackwidow.core.models.clients.client import Client
from blackwidow.core.process.process_breakdown import ProcessBreakdown, ApprovalStatusEnum
from blackwidow.sales.models.common.product_price import ProductPriceEnum
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.clients.distributor import Distributor
from foodex.models.infrastructure.area import Area
from foodex.models.products.damaged_products import PrimaryDamagedProducts

__author__ = 'Sohel'


class PrimaryDamagedProductsForm(GenericFormMixin):

    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)
        self.fields['infrastructure_unit'] = GenericModelChoiceField(label='Area',queryset=Area.objects.all(), widget=forms.Select(attrs={'class': 'select2'}), initial=instance.client.assigned_to if instance is not None else None)
        #self.fields['client'] = GenericModelChoiceField(label='Distributor',queryset=Distributor.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
        self.fields['client'] = GenericModelChoiceField(label='Client', queryset=Client.objects.filter(type=Distributor.__name__), widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'infrastructure_unit', 'data-depends-property': 'assigned_to:id', 'data-url': reverse(Distributor.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}), initial=instance.client if instance is not None else None)
        self.fields['quantity'] = forms.IntegerField(widget=forms.NumberInput(attrs={'min': '0', 'step': '1'}), initial=0)

        original_fields = self.fields
        ordered_fields = ['infrastructure_unit', 'client', 'product', 'quantity']
        new_order = OrderedDict()
        for key in ordered_fields:
            new_order[key] = original_fields[key]
        self.fields = new_order

    def save(self, commit=True):
        product = self.cleaned_data['product']
        product_unit_price = product.prices.filter(price__name=ProductPriceEnum.gross_cust_price.value["name"]).first().value
        value = self.cleaned_data['quantity'] * product_unit_price
        self.instance.value = value
        super().save(commit)

        c_inventory_class = get_model('foodex', 'ClientInventory')
        #print(type)
        c_inv, result = c_inventory_class.objects.get_or_create(product=product, assigned_to=self.cleaned_data['client'])
        if result:
            c_inv.stock = 0
        c_inv.stock = c_inv.stock - int(self.cleaned_data['quantity'])
        c_inv.save()

        # create process breakdown
        process_breakdown = ProcessBreakdown()
        process_breakdown.approval_status = ApprovalStatusEnum.Created
        process_breakdown.process_level = "created"
        process_breakdown.description = "Pending damage product created"
        process_breakdown.save()
        self.instance.process_breakdown.add(process_breakdown)

        return self.instance

    class Meta:
        model = PrimaryDamagedProducts
        fields = ['client', 'product', 'quantity']
        widgets = {
            'client': forms.Select(attrs={'class': 'select2'}),
            'product': forms.Select(attrs={'class': 'select2'}),
        }

