from crequest.middleware import CrequestMiddleware
from django import forms
from django.core.urlresolvers import reverse
from django.db import transaction
from blackwidow.core.mixins.fieldmixin import GenericModelChoiceField

from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.sales.models import ProductPrice, ProductPriceEnum
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from config.enums.view_action_enum import ViewActionEnum
from foodex.models import WarehouseInventory


__author__ = 'Sohel'


class StockInTransactionBreakDownForm(GenericFormMixin):
    def __init__(self, data=None, files=None, instance=None, **kwargs):
        super().__init__(data=data, files=files, instance=instance, **kwargs)
        self.fields['product'] = GenericModelChoiceField(queryset=Product.objects.all(), label='Product Name', widget=forms.Select(attrs={'class': 'select2 transaction-select2'}), required=True)
        self.fields['quantity'] = forms.IntegerField(label='Incoming Quantity', widget=forms.NumberInput(attrs={'min': '0'}), initial=0)

    def save(self, commit=True):
        with transaction.atomic():
            super().save(commit)

            unit_price = self.instance.product.prices.filter(price=ProductPrice.objects.get(name=ProductPriceEnum.gross_cust_price.value['name'])).first().value
            # whouse_inventories = WarehouseInventory.objects.filter(product=self.instance.product, assigned_to=self.instance.transaction_set.all().first().infrastructure_unit).order_by('-last_updated')
            #
            # if whouse_inventories.exists():
            #     self.instance.opening_stock = whouse_inventories.first().stock
            #     self.instance.closing_stock = self.instance.opening_stock + self.instance.quantity
            #     super().save(commit)
            # else:
            #     self.instance.opening_stock = 0
            #     self.instance.closing_stock = self.instance.quantity
            #     super().save(commit)

            self.instance.unit_price = unit_price
            self.instance.sub_total = self.instance.quantity * self.instance.unit_price
            self.instance.discount = 0
            self.instance.total = self.instance.sub_total - self.instance.discount
            super().save(commit)

            return self.instance

    class Meta:
        model = TransactionBreakDown
        fields = ['product', 'quantity']

