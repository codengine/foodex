from uuid import uuid4
from django.db import transaction
from django.forms.models import modelformset_factory
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericModelFormSetMixin
from blackwidow.engine.extensions.clock import Clock
from blackwidow.sales.forms.transactions.stock_transfer_form import StockTransferForm
from blackwidow.sales.models.common.product_price import ProductPrice, ProductPriceEnum
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from foodex.forms.transactions.stock_transfer_transaction_breakdown_form import StockTransferTransactionBreakDownForm
from foodex.models.infrastructure.warehouse import Warehouse
from foodex.models.inventory.warehouse_inventory import WarehouseInventory
from foodex.models.transactions.warehouse_stock_transfer import WarehouseStockTransfer
from django import forms

__author__ = 'Sohel'

products_form_set = modelformset_factory(TransactionBreakDown, form=StockTransferTransactionBreakDownForm, formset=GenericModelFormSetMixin, extra=0, min_num=1)

class WarehouseStockTransferForm(StockTransferForm):
    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)
        self.fields['infrastructure_unit'] = GenericModelChoiceField(label='Warehouse From',queryset=Warehouse.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
        #self.fields["product"] = GenericModelChoiceField(label='Product', queryset=Product.objects.all(), widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'product', 'data-depends-property': 'product__product_inventory__assigned_to:id', 'data-url': reverse(Product.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1"}))
        # self.fields['product'] = GenericModelChoiceField(label='Product',queryset=Product.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
        # self.fields['stock'] = forms.IntegerField(label='Incoming Quantity', required=True)
        self.fields['infrastructure_unit_2'] = GenericModelChoiceField(label='Warehouse To',queryset=Warehouse.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
        self.add_child_form("breakdown", products_form_set(data=data, files=files, header='Products', prefix= prefix + str(len(self.suffix_child_forms)), queryset=TransactionBreakDown.objects.none() if instance is None else instance.breakdown.all(), add_more=True, **kwargs))

    def save(self, commit=True):
        with transaction.atomic():
            self.instance.unique_id = str(uuid4())
            self.instance.transaction_time = Clock.timestamp()
            super().save(commit)
            transaction_breakdown_set = self.instance.breakdown
            from_warehouse = self.instance.infrastructure_unit
            to_warehouse = self.instance.infrastructure_unit_2

            for breakdown in transaction_breakdown_set.all():
                from_warehouse_inventories = WarehouseInventory.objects.filter(assigned_to=from_warehouse,product=breakdown.product).order_by('-last_updated')
                to_warehouse_inventories = WarehouseInventory.objects.filter(assigned_to=to_warehouse,product=breakdown.product).order_by('-last_updated')

                ### save to warehouse inventory
                if not to_warehouse_inventories.exists():
                    whouse_2_inventory = WarehouseInventory()
                    whouse_2_inventory.assigned_to = to_warehouse
                    whouse_2_inventory.product = breakdown.product
                    whouse_2_inventory.opening_stock = 0
                    whouse_2_inventory.stock_change = breakdown.quantity
                    whouse_2_inventory.closing_stock = whouse_2_inventory.stock_change
                    whouse_2_inventory.transaction = self.instance
                    whouse_2_inventory.save(commit)
                else:
                    whouse_2_inventory = WarehouseInventory()
                    whouse_2_inventory.assigned_to = to_warehouse
                    whouse_2_inventory.product = breakdown.product
                    whouse_2_inventory.opening_stock = to_warehouse_inventories.first().closing_stock
                    whouse_2_inventory.stock_change = breakdown.quantity
                    whouse_2_inventory.closing_stock = whouse_2_inventory.opening_stock + whouse_2_inventory.stock_change
                    whouse_2_inventory.transaction = self.instance
                    whouse_2_inventory.save(commit)

                ### save from warehouse inventory
                whouse_1_inventory = WarehouseInventory()
                whouse_1_inventory.assigned_to = from_warehouse
                whouse_1_inventory.product = breakdown.product
                whouse_1_inventory.opening_stock = from_warehouse_inventories.first().closing_stock
                whouse_1_inventory.stock_change = -breakdown.quantity
                whouse_1_inventory.closing_stock = whouse_1_inventory.opening_stock + whouse_1_inventory.stock_change
                whouse_1_inventory.transaction = self.instance
                whouse_1_inventory.save(commit)

    class Meta(StockTransferForm.Meta):
        model = WarehouseStockTransfer
        fields = ['infrastructure_unit','infrastructure_unit_2']
