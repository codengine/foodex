from django import forms
from django.db import transaction
from blackwidow.core.mixins.fieldmixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.sales.models import ProductPrice, ProductPriceEnum, Product
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from foodex.models import WarehouseInventory


__author__ = 'Sohel'


class StockCountTransactionBreakDownForm(GenericFormMixin):
    def __init__(self, data=None, files=None, instance=None, **kwargs):
        super().__init__(data=data, files=files, instance=instance, **kwargs)
        self.fields['product'] = GenericModelChoiceField(queryset=Product.objects.all(), label='Product Name', widget=forms.Select(attrs={'class': 'select2 transaction-select2'}), required=True)
        self.fields['quantity'] = forms.IntegerField(label='Counted Quantity', widget=forms.NumberInput(attrs={'min': '0'}), initial=0)

    def save(self, commit=True):
        with transaction.atomic():

            unit_price = self.instance.product.prices.filter(price=ProductPrice.objects.get(name=ProductPriceEnum.gross_cust_price.value['name'])).first().value
            whouse_inventories = WarehouseInventory.objects.filter(product=self.instance.product).order_by('-last_updated')

            if whouse_inventories.exists():
                self.instance.unit_price = unit_price
                self.instance.sub_total = self.instance.quantity * self.instance.unit_price
                self.instance.discount = 0
                self.instance.total = self.instance.sub_total - self.instance.discount
                super().save(commit)

            return self.instance

    class Meta:
        model = TransactionBreakDown
        fields = ['product', 'quantity']

