from django.db import transaction
from blackwidow.core.mixins.fieldmixin import GenericModelChoiceField
from blackwidow.sales.forms.transactions.stock_count_form import StockCountForm
from foodex.forms.transactions.stock_count_transaction_breakdown_form import StockCountTransactionBreakDownForm
from foodex.models.transactions.stock_count import StockCountTransaction
from django.forms.models import modelformset_factory
from blackwidow.core.mixins.formmixin.form_mixin import GenericModelFormSetMixin, GenericFormMixin
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from foodex.models import Warehouse, WarehouseInventory
from django import forms

__author__ = 'Sohel'

products_form_set = modelformset_factory(TransactionBreakDown, form=StockCountTransactionBreakDownForm, formset=GenericModelFormSetMixin, extra=0, min_num=1)

class StockCountTransactionForm(GenericFormMixin):

    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)

        self.fields['infrastructure_unit'] = GenericModelChoiceField(label='Warehouse Name', queryset=Warehouse.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
        self.add_child_form("breakdown", products_form_set(data=data, files=files, header='Products', prefix= prefix + str(len(self.suffix_child_forms)), queryset=TransactionBreakDown.objects.none() if instance is None else instance.breakdown.all(), add_more=True, **kwargs))
        #self.add_child_form("location", LocationForm(data=data, files=files, form_header='Location', instance=instance.location if instance is not None else None, prefix='suffix' + str(len(self.suffix_child_forms)), **kwargs))

    class Meta(StockCountForm.Meta):
        model = StockCountTransaction
        fields = ['infrastructure_unit', 'remarks']

        widgets = {
            'remarks': forms.Textarea(attrs={'class': 'description'})
        }

    def save(self, commit=True):
        with transaction.atomic():
            super().save(commit)

            product_breakdown_set = self.instance.breakdown.all()
            for product_breakdown in product_breakdown_set:
                tx_subtotal = 0
                tx_discount = 0
                tx_total = 0

                whouse_inventories = WarehouseInventory.objects.filter(product=product_breakdown.product, assigned_to=self.instance.infrastructure_unit).order_by('-last_updated')
                if whouse_inventories.exists():

                    # update transaction breakdown
                    product_breakdown.opening_stock = whouse_inventories.first().stock
                    product_breakdown.closing_stock = product_breakdown.quantity
                    product_breakdown.save(commit)

                    # update warehouse inventory
                    whouse_inventory = whouse_inventories.first()
                    whouse_inventory.stock = product_breakdown.quantity
                    whouse_inventory.save()

                tx_subtotal += product_breakdown.sub_total
                tx_discount += product_breakdown.discount
                tx_total += product_breakdown.total

            self.instance.sub_total = tx_subtotal
            self.instance.discount = tx_discount
            self.instance.total = tx_total
            self.instance.save(commit)

            return self.instance
