from crequest.middleware import CrequestMiddleware
from django import forms
from django.core.urlresolvers import reverse
from django.db import transaction
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField

from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'Sohel'


class StockTransferTransactionBreakDownForm(GenericFormMixin):
    def __init__(self, data=None, files=None, instance=None, **kwargs):
        super().__init__(data=data, files=files, instance=instance, **kwargs)
        self.fields['product'] = GenericModelChoiceField(queryset=Product.objects.all(), label='Product', widget=forms.Select(attrs={'class': 'select2 transaction-select2'}), required=True)
        self.fields['quantity'] = forms.IntegerField(widget=forms.NumberInput(attrs={'min': '0'}), initial=0)


    def save(self, commit=True):
        with transaction.atomic():
            return super().save(commit)

    class Meta:
        model = TransactionBreakDown
        fields = ['product', 'quantity']
