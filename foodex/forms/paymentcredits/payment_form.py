from collections import OrderedDict
from decimal import Decimal
from django.core.urlresolvers import reverse
from django.db import transaction
from django.db.models.loading import get_model
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.models.clients.client import Client
from blackwidow.sales.forms.payments.payment_form import PaymentForm
from blackwidow.sales.models.payments.payment_methods import PaymentMethod
from config.enums import ViewActionEnum
from foodex.models.products.cleared_damage_products import ClearedDamage
from foodex.models.invoice.open_invoice import OpenInvoice
from django import forms
from foodex.models.paymentcredits.payment import Payment

__author__ = 'Sohel'


class PaymentForm(PaymentForm):

    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)

        STATUS_CHOICES = (
            ('open', 'Open'),
            ('close', 'Close'),
        )

        if 'parent_id' in kwargs:
            invoice_id = kwargs['parent_id']
            invoice_objects = OpenInvoice.objects.filter(pk=invoice_id)
            if invoice_objects.exists():
                inv_obj = invoice_objects.first()
                client_obj = inv_obj.counter_part

                self.fields['client'] = GenericModelChoiceField(queryset=Client.objects.filter(pk=client_obj.id), widget=forms.Select(attrs={'class': 'select2 '}), initial=client_obj)
                self.fields['invoice'] = GenericModelChoiceField(label='Invoice', queryset=OpenInvoice.objects.filter(type=OpenInvoice.__name__, pk=inv_obj.id), required=False, widget=forms.TextInput(attrs={'class': 'select2-input require-invoice', 'width': '220', 'data-depends-on': 'client', 'data-depends-property': 'counter_part:id', 'data-url': reverse(OpenInvoice.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}), initial=inv_obj)
                self.fields['payment_method'] = GenericModelChoiceField(queryset=PaymentMethod.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
                self.fields['total_amount'] = forms.DecimalField(label='Total Amount', widget=forms.NumberInput(attrs={'min': '0', 'step': '.01'}), initial=0.0)
                self.fields['due_amount'] = forms.DecimalField(label='Due Amount', widget=forms.NumberInput(attrs={'min': '0', 'step': '.01'}), initial=0.0)
                self.fields['amount_paid'] = forms.DecimalField(label='Pay Amount', widget=forms.NumberInput(attrs={'min': '0', 'step': '.01'}), initial=0.0)
                self.fields['total_available_damage_amount'] = forms.DecimalField(label='Total Available Damage ', widget=forms.NumberInput(attrs={'min': '0', 'step': '.01'}), initial=0.0)
                self.fields['apply_damage_amount'] = forms.DecimalField(label='Apply Damage', widget=forms.NumberInput(attrs={'min': '0', 'step': '.01'}), initial=0.0)
                self.fields['status'] = forms.ChoiceField(widget=forms.Select(attrs={'class': 'select2'}), choices=STATUS_CHOICES, initial='Open', required=True,)

        else:
            self.fields['client'] = GenericModelChoiceField(queryset=Client.objects.all().exclude(type='Retailers'), widget=forms.Select(attrs={'class': 'select2 require-client'}))
            # self.fields['invoice'] = GenericModelChoiceField(queryset=OpenInvoice.objects.all(),initial='khkkhkhkh', widget=forms.Select(attrs={'class': 'select2 require-invoice'}), required=False)
            self.fields['invoice'] = GenericModelChoiceField(label='Invoice', queryset=OpenInvoice.objects.filter(type=OpenInvoice.__name__), required=False, widget=forms.TextInput(attrs={'class': 'select2-input require-invoice', 'width': '220', 'data-depends-on': 'client', 'data-depends-property': 'counter_part:id', 'data-url': reverse(OpenInvoice.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}), initial=instance.invoice if instance is not None else None)
            self.fields['payment_method'] = GenericModelChoiceField(queryset=PaymentMethod.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
            self.fields['total_amount'] = forms.DecimalField(label='Total Amount', widget=forms.NumberInput(attrs={'min': '0', 'step': '.01'}), initial=0.0)
            self.fields['due_amount'] = forms.DecimalField(label='Due Amount', widget=forms.NumberInput(attrs={'min': '0', 'step': '.01'}), initial=0.0)
            self.fields['amount_paid'] = forms.DecimalField(label='Pay Amount', widget=forms.NumberInput(attrs={'min': '0', 'step': '.01'}), initial=0.0)
            self.fields['total_available_damage_amount'] = forms.DecimalField(label='Total Available Damage ', widget=forms.NumberInput(attrs={'min': '0', 'step': '.01'}), initial=0.0)
            self.fields['apply_damage_amount'] = forms.DecimalField(label='Apply Damage', widget=forms.NumberInput(attrs={'min': '0', 'step': '1'}), initial=0.0)
            self.fields['status'] = forms.ChoiceField(widget=forms.Select(attrs={'class': 'select2'}), choices=STATUS_CHOICES, initial='Open', required=True,)

        # sorting payment_form fields
        original_fields = self.fields
        ordered_fields = ['client', 'invoice', 'total_amount', 'due_amount', 'amount_paid', 'total_available_damage_amount', 'apply_damage_amount', 'payment_method', 'reference', 'status']
        new_order = OrderedDict()
        for key in ordered_fields:
            new_order[key] = original_fields[key]
        self.fields = new_order

    class Meta(PaymentForm.Meta):
        model = Payment
        fields = ['client', 'invoice', 'total_amount', 'amount_paid', 'due_amount', 'total_available_damage_amount', 'apply_damage_amount', 'payment_method', 'reference']
        widgets = {
            'reference': forms.Textarea(attrs={'class': 'description', 'rows': 2})
        }

    def save(self, commit=True):
        with transaction.atomic():
            super().save(commit)
            self.instance.amount_paid = self.instance.amount_paid + self.instance.apply_damage_amount
            super().save(commit)

            ### create cleared damage
            clear_damage = ClearedDamage()
            clear_damage.client = self.instance.client
            clear_damage.total_available_damage = self.instance.total_available_damage_amount
            clear_damage.total_cleared_damage = self.instance.apply_damage_amount
            clear_damage.payment = self.instance
            clear_damage.save()

            if self.instance.invoice is not None:
                self.instance.due_amount = self.instance.due_amount-self.instance.amount_paid
                super().save(commit)

                self.instance.invoice.actual_amount_paid += self.instance.amount_paid
                self.instance.client.available_credit += self.instance.amount_paid
                self.instance.client.save()
                if self.instance.invoice.price_total - self.instance.invoice.actual_amount_paid <= 0 or self.cleaned_data['status'] =="close":
                    cls = get_model("foodex", "ClosedInvoice")

                    self.instance.invoice.type = cls.__name__
                    self.instance.invoice.__class__ = cls
                self.instance.invoice.damage_value = self.instance.invoice.damage_value + self.instance.apply_damage_amount
                self.instance.invoice.save()
                clear_damage.invoice = self.instance.invoice
                clear_damage.save()
            return self.instance

