from collections import OrderedDict
from django.core.urlresolvers import reverse
from django.db import transaction
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.clients.client import Client
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.clients.distributor import Distributor
from foodex.models.infrastructure.area import Area
from foodex.models.paymentcredits.client_credit import ClientCredit
from django import forms

__author__ = 'Sohel'


class ClientCreditForm(GenericFormMixin):
    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)
        self.fields['infrastructure_unit'] = GenericModelChoiceField(label='Area',queryset=Area.objects.all(), widget=forms.Select(attrs={'class': 'select2'}), initial=instance.client.assigned_to if instance is not None else None)
        #self.fields['client'] = GenericModelChoiceField(label='Distributor',queryset=Distributor.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
        self.fields['client'] = GenericModelChoiceField(label='Client', queryset=Client.objects.all(), widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'infrastructure_unit', 'data-depends-property': 'assigned_to:id', 'data-url': reverse(Client.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}), initial=instance.client if instance is not None else None)
        self.fields['credit_limit'] = forms.DecimalField(label='Credit Limit', widget=forms.NumberInput(attrs={'min': '0', 'step': '1'}), initial=instance.client.credit_limit if instance is not None else 0.0)

        # sorting client_credit_form fields
        original_fields = self.fields
        ordered_fields = ['infrastructure_unit', 'client', 'credit_limit']
        new_order = OrderedDict()
        for key in ordered_fields:
            new_order[key] = original_fields[key]
        self.fields = new_order

    def save(self, commit=True):
        with transaction.atomic():
            super().save(commit)
            self.cleaned_data['client'].credit_limit = self.cleaned_data['credit_limit']
            self.cleaned_data['client'].available_credit = self.cleaned_data['credit_limit']
            self.cleaned_data['client'].save()
            return self.instance

    class Meta:
        model = ClientCredit
        fields = ['client', ]

