from django.db import transaction
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.common.week_day import WeekDay
from foodex.models.clients.distributor import Distributor
from foodex.models.infrastructure.routes import Route
from django import forms


class RouteForm(GenericFormMixin):

    def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)

        if 'parent_id' in kwargs:
            distributor_id = kwargs['parent_id']
            dis_objects = Distributor.objects.filter(pk=distributor_id)
            if dis_objects.exists():
                distributor_obj = dis_objects.first()
                self.fields['parent_client'] = GenericModelChoiceField(label='Assigned Distributor ',queryset=Distributor.objects.filter(pk=distributor_obj.id), widget=forms.Select(attrs={'class': 'select2'}), initial=distributor_obj)
                self.fields['service_day'] = GenericModelChoiceField(label='Service Day ', queryset=WeekDay.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))

        else:
            self.fields['parent_client'] = GenericModelChoiceField(label='Assigned Distributor ',queryset=Distributor.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
            self.fields['service_day'] = GenericModelChoiceField(label='Service Day ', queryset=WeekDay.objects.all(), widget=forms.Select(attrs={'class': 'select2'}), initial=instance.service_days.all().first() if instance is not None else None)

    def save(self, commit=True):
        with transaction.atomic():
            service_day = self.cleaned_data["service_day"]
            self.instance.save(commit)
            service_days = self.instance.service_days.all()
            if service_days:
                for day in service_days:
                    self.instance.service_days.remove(day)
                self.instance.service_days.add(service_day)
            else:
                self.instance.service_days.add(service_day)
            return self.instance

    class Meta:
        model = Route
        fields = ['name', 'parent_client']