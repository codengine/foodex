from blackwidow.core.forms import WareHouseForm
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models import ConsoleUser
from foodex.models.clients.distributor import Distributor
from foodex.models.infrastructure.routes import Route
from django import forms
from foodex.models.infrastructure.warehouse import Warehouse

__author__ = 'Sohel'

class WarehouseForm(WareHouseForm):

    def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
        self.fields['assigned_to'] = GenericModelChoiceField(label='Assign to', queryset=ConsoleUser.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
        # self.fields['parent_client'] = GenericModelChoiceField(label='Assigned to ',queryset=Distributor.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))

    class Meta(WareHouseForm.Meta):
        model = Warehouse
        fields = ['name', 'assigned_to']