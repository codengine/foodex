
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from foodex.models.infrastructure.region import Region

__author__ = 'Sohel'


class RegionForm(GenericFormMixin):
    def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)

    class Meta(GenericFormMixin.Meta):
        model = Region
        fields = ['name']