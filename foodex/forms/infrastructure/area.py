from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from foodex.models.infrastructure.area import Area
from foodex.models.infrastructure.region import Region
from django import forms

__author__ = 'Mahmud'


class AreaForm(GenericFormMixin):
    def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
        self.fields['parent'] = GenericModelChoiceField(label='Assigned To',queryset=Region.objects.all(), widget=forms.Select(attrs={'class':'select2'}))

    class Meta(GenericFormMixin.Meta):
        model = Area
        fields = ['parent','name']