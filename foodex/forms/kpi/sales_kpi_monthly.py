import calendar
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.organizations.organization import Organization
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.engine.extensions.clock import Clock
from foodex.models.kpi.sales_kpi_monthly import SalesKPIMonthly
from django import forms

__author__ = 'Sohel'


class SalesKPIMonthlyForm(GenericFormMixin):
    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)
        self.fields['user'] = GenericModelChoiceField(label='Select User', queryset=ConsoleUser.objects.all(), widget=forms.Select(attrs={'class': 'select2'})) #initial=instance.warehouse if instance is not None else None
        self.fields['name'] =  forms.CharField( label='Name', widget=forms.TextInput())
        self.fields['year'] =  forms.CharField( label='Year', widget=forms.NumberInput())
        self.fields['january'] =  forms.CharField( label='January', widget=forms.NumberInput())
        self.fields['february'] =  forms.CharField( label='February', widget=forms.NumberInput())
        self.fields['march'] =  forms.CharField( label='March', widget=forms.NumberInput())
        self.fields['april'] =  forms.CharField( label='April', widget=forms.NumberInput())
        self.fields['may'] =  forms.CharField( label='May', widget=forms.NumberInput())
        self.fields['june'] =  forms.CharField( label='June', widget=forms.NumberInput())
        self.fields['july'] =  forms.CharField( label='July', widget=forms.NumberInput())
        self.fields['august'] =  forms.CharField( label='August', widget=forms.NumberInput())
        self.fields['september'] =  forms.CharField( label='September', widget=forms.NumberInput())
        self.fields['october'] =  forms.CharField( label='October', widget=forms.NumberInput())
        self.fields['november'] =  forms.CharField( label='November', widget=forms.NumberInput())
        self.fields['december'] =  forms.CharField( label='December', widget=forms.NumberInput())

    @classmethod
    def map_month_name(cls,number):
        months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        if number >= 1 and number <= 12:
            return months[number - 1]
        return None

    def create_or_update_monthly_kpi(self, name, year, month, user_id, target_value,organization):
        year = int(year)
        month = int(month)
        weekday, end_day = calendar.monthrange(year, month)
        start_day = 1
        start_ts = Clock.get_timestamp_from_date(str(start_day)+"/"+str(month)+"/"+str(year)+" 00:00:00")
        end_ts = Clock.get_timestamp_from_date(str(end_day)+"/"+str(month)+"/"+str(year)+" 00:00:00")

        month_name = self.__class__.map_month_name(month)
        monthly_kpi = SalesKPIMonthly.objects.filter(year=year, month = month, user_id = month)
        if monthly_kpi.exists():
            monthly_kpi = monthly_kpi.first()
        else:
            monthly_kpi = SalesKPIMonthly()
            monthly_kpi.name = name
        monthly_kpi.year = year
        monthly_kpi.month = month
        monthly_kpi.month_name = month_name
        monthly_kpi.user_id = user_id
        monthly_kpi.name = "Monthly KPI for "+month_name
        monthly_kpi.target = target_value
        monthly_kpi.start_time = start_ts
        monthly_kpi.end_time = end_ts
        monthly_kpi.organization = organization
        monthly_kpi.save()
        return self.instance

    def save(self, commit=True, *args, **kwargs):
        name = self.cleaned_data['name']
        year = self.cleaned_data['year']
        user = self.cleaned_data['user']
        organization = kwargs['organization'] if kwargs.get('organization') else Organization.objects.all().first()
        self.create_or_update_monthly_kpi(name, year, 1, user.pk, self.cleaned_data['january'],organization)
        self.create_or_update_monthly_kpi(name, year, 2, user.pk, self.cleaned_data['february'],organization)
        self.create_or_update_monthly_kpi(name, year, 3, user.pk, self.cleaned_data['march'],organization)
        self.create_or_update_monthly_kpi(name, year, 4, user.pk, self.cleaned_data['april'],organization)
        self.create_or_update_monthly_kpi(name, year, 5, user.pk, self.cleaned_data['may'],organization)
        self.create_or_update_monthly_kpi(name, year, 6, user.pk, self.cleaned_data['june'],organization)
        self.create_or_update_monthly_kpi(name, year, 7, user.pk, self.cleaned_data['july'],organization)
        self.create_or_update_monthly_kpi(name, year, 8, user.pk, self.cleaned_data['august'],organization)
        self.create_or_update_monthly_kpi(name, year, 9, user.pk, self.cleaned_data['september'],organization)
        self.create_or_update_monthly_kpi(name, year, 10, user.pk, self.cleaned_data['october'],organization)
        self.create_or_update_monthly_kpi(name, year, 11, user.pk, self.cleaned_data['november'],organization)
        self.create_or_update_monthly_kpi(name, year, 12, user.pk, self.cleaned_data['december'],organization)
        return self.instance

    class Meta(GenericFormMixin.Meta):
        model = SalesKPIMonthly
        fields = ( 'name', 'year', )

