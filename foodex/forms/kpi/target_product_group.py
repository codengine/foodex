from django import forms
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.fieldmixin.multiple_select_field_mixin import GenericModelMultipleChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin, GenericModelFormSetMixin
from blackwidow.sales.forms.products.product_form import ProductsForm
from blackwidow.sales.models.products.product import Product
from foodex.models.kpi.target_product_group import TargetProductGroup

__author__ = 'Sohel'


class TargetProductGroupForm(GenericFormMixin):

    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)

        self.fields['products'] = GenericModelMultipleChoiceField(initial=instance.products.all() if instance else None, queryset=Product.objects.all(),
                                                             label='Select Products', widget=forms.SelectMultiple(attrs={ 'class': 'select2' }),
                                                             required=False)

        self.fields['is_active'] = forms.BooleanField(initial=True if instance and instance.is_active else False, widget=forms.CheckboxInput(attrs={}), required=False)

    class Meta(GenericFormMixin.Meta):
        model = TargetProductGroup
        fields = ( 'name', )
        labels = {
            'name': 'Group Name',
            'is_active': 'Is Active(Mark Check if active)'
        }

    def save(self, commit=True):

        self.instance.is_active = self.cleaned_data['is_active']

        super(TargetProductGroupForm, self).save(commit=commit)

        self.instance.products.clear()

        for product_id in self.data.getlist('products'):
            product_instance = Product.objects.get(pk=int(product_id))
            self.instance.products.add(product_instance)
        return self.instance
