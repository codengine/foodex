from collections import OrderedDict
from django.db import transaction
from django import forms
from django.core.urlresolvers import reverse
from blackwidow.core.forms import WebUserForm

from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.forms.clients.client_form import ClientForm
from blackwidow.core.models import ConsoleUser
from foodex.models import Area
from foodex.models.clients.distributor import Distributor
from foodex.models.clients.retailers import Retailers
from foodex.models.infrastructure.routes import Route
from blackwidow.core.models.clients.client_assignment import ClientAssignment
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'Mahmud'


class RetailersForm(ClientForm):
    def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
        _prefix = prefix
        if _prefix != '':
            _prefix += '-'
        if self.request.method == "GET":
            self.fields['area'] = GenericModelChoiceField(label='Area', queryset=Area.objects.all(), widget=forms.Select(attrs={'class': 'select2'}), initial=instance.render_area if instance is not None else None)
            self.fields["parent"] = GenericModelChoiceField(label='Distributor', queryset=Distributor.objects.all(), widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'area', 'data-depends-property': 'assigned_to:id', 'data-url': reverse(Distributor.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
            self.fields["assigned_to"] = GenericModelChoiceField(label='Assigned Route', queryset=Route.objects.all(), initial=instance.render_assign_route if instance is not None else None, widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'parent', 'data-depends-property': 'parent_client:id', 'data-url': reverse(Route.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
            self.add_child_form("contact_person", WebUserForm(data=data, files=files, instance=instance.contact_person if instance is not None else None, form_header='Contact Person', prefix=_prefix + str(len(self.suffix_child_forms)), **kwargs))

        else:
            self.fields['area'] = GenericModelChoiceField(label='Area', queryset=Area.objects.all(), widget=forms.Select(attrs={'class': 'select2'}), initial=instance.render_area if instance is not None else None)
            self.fields["parent"] = GenericModelChoiceField(label='Distributor', queryset=Distributor.objects.all(), widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'area', 'data-depends-property': 'assigned_to:id', 'data-url': reverse(Distributor.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
            self.fields["assigned_to"] = GenericModelChoiceField(label='Assigned Route', queryset=Route.objects.all(),initial=instance.render_assign_route if instance is not None else None, widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'parent', 'data-depends-property': 'parent_client:id', 'data-url': reverse(Route.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
            self.add_child_form("contact_person", WebUserForm(data=data, files=files, instance=instance.contact_person if instance is not None else None, form_header='Contact Person', prefix=_prefix + str(len(self.suffix_child_forms)), **kwargs))

            #self.fields['assigned_to'] = GenericModelChoiceField(queryset=Route.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))


        contact_person_form_object = self.suffix_child_forms[2]
        contact_address_form_object = contact_person_form_object.suffix_child_forms[2]
        contact_person_form_object.child_forms.pop(1)
        del contact_person_form_object.fields['designation']
        contact_address_form_object.child_forms.pop(0)

        original_fields = self.fields
        ordered_fields = ['name', 'area', 'parent', 'assigned_to']
        new_order = OrderedDict()
        for key in ordered_fields:
            new_order[key] = original_fields[key]
        self.fields = new_order



    def find_initial_pk(self,instance=None, dpk = -1):
        initial_pk = -1
        if dpk != -1:
            initial_pk = dpk
        elif instance:
            retailer_routes = Route.objects.filter(assigned_clients__in=ClientAssignment.objects.filter(client_type=Retailers.__name__,clients__in=[instance]))
            if retailer_routes:
                distributor = retailer_routes[0].parent_client
                initial_pk = distributor.pk

        return initial_pk

    # @classmethod
    # def get_dependent_field_list(cls):
    #         return ['address', 'financial_information', 'custom_fields']

    def save(self, commit=True):

        with transaction.atomic():

            route = self.cleaned_data["assigned_to"]
            super().save(commit)
            assigned_clients = route.assigned_clients.filter(client_type=Retailers.__name__)
            if assigned_clients:
                route.assigned_clients.all().first().clients.add(self.instance)
            else:
                client_assignment = ClientAssignment()
                client_assignment.client_type = Retailers.__name__
                client_assignment.save(commit)
                client_assignment.clients.add(self.instance)
                client_assignment.save(commit)
                route.assigned_clients.add(client_assignment)
            route.save(commit)

            super().save(commit)

            self.instance.is_approved = True
            self.instance.approved_by = ConsoleUser.objects.get(user=self.request.user)
            self.instance.save()

            return self.instance
    
    class Meta:
        model = Retailers
        fields = ['name', 'parent']
        # widgets = {
        #     'assigned_to': forms.Select(attrs={'class': 'select2'})
        # }
        #exclude=['parent']
