from django.db import transaction
from django import forms
from django.core.urlresolvers import reverse

from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.forms.clients.client_form import ClientForm
from foodex.models.clients.distributor import Distributor
from foodex.models.clients.pending_sec_retail import PendingRetailerAccount
from foodex.models.clients.retailers import Retailers
from foodex.models.infrastructure.routes import Route
# from foodex.forms.partials.route_option_choice_form import RoutesForm
from blackwidow.core.models.users.user import ConsoleUser
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.infrastructure.routes import Route
from blackwidow.core.models.clients.client_assignment import ClientAssignment


__author__ = 'Mahmud'


class PendingRetailerAccountForm(ClientForm):
    def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
        # self.fields['parent'] = GenericModelChoiceField(label = 'Distributor',queryset=Distributor.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
        if self.request.method == "GET":
            self.fields['parent'] = GenericModelChoiceField(label='Distributor',queryset=Distributor.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
            self.fields["assigned_to"] = GenericModelChoiceField(label='Assign Route', queryset=Route.objects.all(), widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'parent', 'data-depends-property': 'parent_client:id', 'data-url': reverse(Route.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
        else:
            self.fields['parent'] = GenericModelChoiceField(label='Distributor',queryset=Distributor.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
            self.fields["assigned_to"] = GenericModelChoiceField(label='Assign Route', queryset=Route.objects.all(), widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'parent', 'data-depends-property': 'parent_client:id', 'data-url': reverse(Route.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))


    def find_initial_pk(self,instance=None,dpk = -1):
        initial_pk = -1
        if dpk != -1:
            initial_pk = dpk
        elif instance:
            retailer_routes = Route.objects.filter(assigned_clients__in=ClientAssignment.objects.filter(client_type=Retailers.__name__,clients__in=[instance]))
            if retailer_routes:
                distributor = retailer_routes[0].parent_client
                initial_pk = distributor.pk

        return initial_pk

    def save(self, commit=True):
        #print(self.fields)
        with transaction.atomic():
            #self.fields["is_approved"] = True
            route = self.cleaned_data["assigned_to"]
            # client_assignment = ClientAssignment.objects.get(pk=3)
            # client_assignment.client_type = Retailers.__name__
            # client_assignment.save(commit)
            # client_assignment.clients.add(self.instance)
            # client_assignment.save(commit)
            super().save(commit)
            assigned_clients = route.assigned_clients.filter(client_type=Retailers.__name__)
            if assigned_clients:
                route.assigned_clients.all().first().clients.add(self.instance)
            else:
                client_assignment = ClientAssignment()
                client_assignment.client_type = Retailers.__name__
                client_assignment.save(commit)
                clients = client_assignment.clients.filter(pk=self.instance.pk)
                if clients.exists():
                    print("Already added")
                else:
                    client_assignment.clients.add(self.instance)
                    client_assignment.save(commit)
                    route.assigned_clients.add(client_assignment)
            route.save(commit)
            #super().save(commit)
            self.instance.is_approved = False
            # self.instance.approved_by = ConsoleUser.objects.get(user=self.request.user)
            self.instance.save()
            return self.instance

    class Meta:
        model = PendingRetailerAccount
        fields = ['name','parent','assigned_to']
        widgets = {
            'assigned_to': forms.Select(attrs={'class': 'select2'})
        }
        #exclude = ["parent"]
