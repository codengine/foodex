from django.db import transaction
from django import forms
from blackwidow.core.forms import WebUserForm

from blackwidow.core.forms.clients.client_form import ClientForm
from blackwidow.core.mixins.fieldmixin import GenericModelChoiceField
from foodex.models import Area
from foodex.models.clients.wholesale import WholeSaler


__author__ = 'Mahmud'


class WholeSalerForm(ClientForm):
    def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
        _prefix = prefix
        if _prefix != '':
            _prefix += '-'
        self.fields['assigned_to'] = GenericModelChoiceField(label='Area',queryset=Area.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
        self.add_child_form("contact_person", WebUserForm(data=data, files=files, instance=instance.contact_person if instance is not None else None, form_header='Contact Person', prefix=_prefix + str(len(self.suffix_child_forms)), **kwargs))


        contact_person_form_object=self.suffix_child_forms[2]
        contact_address_form_object=contact_person_form_object.suffix_child_forms[2]
        contact_person_form_object.child_forms.pop(1)
        del contact_person_form_object.fields['designation']
        contact_address_form_object.child_forms.pop(0)


    def save(self, commit=True):
        with transaction.atomic():
            return super().save(commit)
    
    class Meta:
        model = WholeSaler
        fields = ['name', 'company_name', 'credit_limit', 'assigned_to']
        widgets = {
            'assigned_to': forms.Select(attrs={'class': 'select2'})
        }

        label = {
            'credit_limit': 'Credit Limit (BDT)'
        }
