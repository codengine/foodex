from django import forms

from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.forms.clients.client_form import ClientForm
from blackwidow.core.models.clients.dealer import Dealer
from foodex.models.clients.distributor import Distributor


__author__ = 'Mahmud'


class DealerForm(ClientForm):
    def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
        self.fields['parent'] = GenericModelChoiceField(widget=forms.Select(attrs={'class': 'select2'}), queryset=Distributor.objects.all(), initial=instance.parent if instance is not None else None)

    class Meta:
        model = Dealer
        fields = ['name', 'parent']