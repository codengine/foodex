from django import forms
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.sales.models.common.product_price import ProductPrice
from blackwidow.sales.models.products.product import Product
from foodex.models.common.product_price_config import FoodexProductPriceConfig
from foodex.models.clients.horeco import HoReCo
from foodex.models.clients.modern_trade import ModernTrade
from foodex.models.infrastructure.area import Area

__author__ = 'Sohel'


class FoodexProductPriceConfigForm(GenericFormMixin):
    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)

        initial_product, initial_client, initial_iu, initial_price_type, initial_value = None, None, None, None, None

        product_queryset = Product.objects.all()
        client_queryset = Client.objects.filter(type__in=[HoReCo.__name__, ModernTrade.__name__])
        iu_queryset = InfrastructureUnit.objects.filter(type=Area.__name__)
        price_type_queryset = ProductPrice.objects.all()

        if instance:
            initial_product = instance.product
            initial_iu = instance.infrastructure_unit
            initial_client = instance.client
            initial_price_type = instance.price_type
            initial_value = instance.value

            product_queryset = product_queryset.filter(pk=initial_product.pk)
            client_queryset = client_queryset.filter(pk=initial_client.pk)
            iu_queryset = iu_queryset.filter(pk=initial_iu.pk)
            price_type_queryset = price_type_queryset.filter(pk=initial_price_type.pk)

        self.fields['product'] = GenericModelChoiceField(queryset=product_queryset, initial=initial_product,
                                                         widget=forms.Select(attrs={'class': 'select2'}), required=True)
        self.fields['client'] = GenericModelChoiceField(label='HoReCa / ModernTrade', initial=initial_client,
                                                        queryset=client_queryset,
                                                        widget=forms.Select(attrs={'class': 'select2'}), required=True)
        self.fields['infrastructure_unit'] = GenericModelChoiceField(queryset=iu_queryset, initial=initial_iu,
                                                                     widget=forms.Select(attrs={'class': 'select2'}),
                                                                     required=True,
                                                                     label="Area")
        self.fields['price_type'] = GenericModelChoiceField(queryset=price_type_queryset, initial=initial_price_type,
                                                            widget=forms.Select(attrs={'class': 'select2'}),
                                                            required=True)
        self.fields['value'] = forms.DecimalField(min_value=0, initial=initial_value)

    class Meta(GenericFormMixin.Meta):
        model = FoodexProductPriceConfig
        fields = ['product', 'infrastructure_unit', 'client', 'price_type', 'value']

    def save(self, commit=True):
        super().save(commit)
        return self.instance