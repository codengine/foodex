from blackwidow.core.mixins.fieldmixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin import GenericFormMixin
from blackwidow.core.models import DeviceCategory, Organization
from django import forms
__author__ = 'zia ahmed'


class DeviceCategoryForm(GenericFormMixin):

    def __init__(self, data=None, files=None, instance=None, prefix='', form_header='',  **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, form_header=form_header,  **kwargs)
        # self.fields['organization'] = GenericModelChoiceField(widget=forms.Select(attrs={'class': 'select2', 'multiple': 'multiple'}), queryset=Organization.objects.all().exclude(pk=1), initial=instance.organization if instance is not None else None)


    class Meta(GenericFormMixin.Meta):
        model = DeviceCategory
        fields = ['name', ]
        labels = {
            'name': 'Category Name'
        }

