from django.db import transaction
from blackwidow.core.forms.files.apkfileobject_form import ApplicationFileObjectForm
from blackwidow.core.mixins.fieldmixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin import GenericFormMixin
from blackwidow.core.models import Organization, FieldBuzzApplication
from django import forms

__author__ = 'zia ahmed'


class FieldbuzzApplicationForm(GenericFormMixin):

    def __init__(self, data=None, files=None, instance=None, prefix='', form_header='',  **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, form_header=form_header,  **kwargs)
        _prefix = prefix
        if _prefix != '':
            _prefix += '-'
        else:
            _prefix = 'suffix-'


        self.add_child_form("file", ApplicationFileObjectForm(data=data, files=files, form_header='Upload Apk', instance=instance.file if instance is not None else None, prefix=_prefix + str(len(self.suffix_child_forms)), **kwargs))
        self.fields['status'] = forms.ChoiceField(label='Application Status', widget=forms.Select(attrs={'class': 'select2'}), choices=FieldBuzzApplication.STATUS_CHOICES, initial=instance.status if instance is not None else 1)
        # self.fields['organization'] = GenericModelChoiceField(label='Assign Organization', widget=forms.Select(attrs={'class': 'select2'}), queryset=Organization.objects.all().exclude(pk=1), initial=instance.organization if instance is not None else None)


    class Meta(GenericFormMixin.Meta):
        model = FieldBuzzApplication
        fields = ['name', 'package_name', 'version_name', 'version_code', 'status']

    def save(self, commit=True):
        with transaction.atomic():
            # organization = self.cleaned_data['organization']
            # self.instance.organization = organization
            super().save(commit)
            return self.instance