from unicodedata import decimal
from django.db import transaction
from blackwidow.core.forms.users.web_user_form import WebUserForm
from blackwidow.core.mixins.fieldmixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin import GenericFormMixin
from blackwidow.core.models import Device, Manufacturer, Organization, DeviceCategory, WebUser, DeviceSettings, \
    DeviceLocation, DeviceApplication, ConsoleUser
from django import forms
from blackwidow.core.models.device.device_access_control import DeviceWipeData, DeviceLock
from blackwidow.core.models.device.device_status import DeviceUpdateStatus

__author__ = 'zia ahmed'


class DeviceForm(GenericFormMixin):

    def __init__(self, data=None, files=None, instance=None, prefix='', form_header='',  **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, form_header=form_header,  **kwargs)
        _prefix = prefix
        if _prefix != '':
            _prefix += '-'
        else:
            _prefix = 'suffix-'

        s_orientation = 1
        if instance is not None:
            device_settings = DeviceSettings.objects.filter(device=self.instance)
            if device_settings.exists():
                s_orientation = device_settings.first().screen_orientation


        # self.add_child_form("assigned_user", WebUserForm(data=data, files=files, form_header='Assigned User', instance=instance.contact_person if instance is not None else None, prefix=_prefix + str(len(self.suffix_child_forms)), **kwargs))
        self.fields['screen_orientation'] = forms.ChoiceField(widget=forms.Select(attrs={'class': 'select2'}), choices=DeviceSettings.ORIENTATION_CHOICES, initial=s_orientation)
        self.fields['manufacturer'] = GenericModelChoiceField(widget=forms.Select(attrs={'class': 'select2'}), required=False, queryset=Manufacturer.objects.all(), initial=instance.manufacturer if instance is not None else None)
        # if instance is None:
        #     self.fields['organization'] = GenericModelChoiceField(widget=forms.Select(attrs={'class': 'select2'}), queryset=Organization.objects.all().exclude(pk=1), initial=instance.organization if instance is not None else None)
        self.fields['category'] = GenericModelChoiceField(widget=forms.Select(attrs={'class': 'select2'}), queryset=DeviceCategory.objects.all(), initial=instance.category if instance is not None else None)
        self.fields['service_person'] = GenericModelChoiceField(widget=forms.Select(attrs={'class': 'select2'}), queryset=ConsoleUser.objects.all(), initial=instance.service_person if instance is not None else None)
        self.fields['sim_serial_number'].required = False
        self.fields['sim_number'].required = False
        self.fields['pin_code'].required = False
        self.fields['puk_code'].required = False
        self.fields['name'].required = False


    class Meta(GenericFormMixin.Meta):
        model = Device
        fields = ['uuid', 'name', 'sim_serial_number', 'sim_number', 'pin_code', 'puk_code',]

    def save(self, commit=True):
        with transaction.atomic():
            category = self.cleaned_data['category']
            service_person = self.cleaned_data['service_person']

            self.instance.category = category
            self.instance.service_person = service_person
            # if self.instance.pk is None:
            #     organization = self.cleaned_data['organization']
            #     self.instance.organization = organization
            #     super().save(commit)

            super().save(commit)

            # Device settings
            device_settings, created = DeviceSettings.objects.get_or_create(device=self.instance)
            device_settings.screen_orientation = self.cleaned_data['screen_orientation']
            device_settings.save(commit)

            # Device location
            device_location, created = DeviceLocation.objects.get_or_create(device=self.instance)
            if not created:
                pass

            # Device wipe data
            wipedata, created = DeviceWipeData.objects.get_or_create(device=self.instance)
            if not created:
                pass

            # Device lock
            passcode, created = DeviceLock.objects.get_or_create(device=self.instance)
            if not created:
                pass  # Passcode generation method will be implemented

            # Device update status
            update_status, created = DeviceUpdateStatus.objects.get_or_create(device=self.instance)
            if not created:
                pass

            # Assigned allow applications
            # allow_applications = DeviceApplication.objects.filter(organization=self.instance.organization)
            allow_applications = list(service_person.role.allowed_apps.all())
            if len(allow_applications) > 0:
                self.instance.applications.add(*allow_applications)

            return self.instance




