from blackwidow.core.forms.files.fileobject_form import FileObjectForm
from blackwidow.core.models.file.imagefileobject import ImageFileObject

__author__ = 'ruddra'


class ImageFileObjectForm(FileObjectForm):

    class Meta:
        model = ImageFileObject
        fields = ['file', 'description']
