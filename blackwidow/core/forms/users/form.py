from django import forms
from django.contrib.auth.models import User
from django.db import transaction
from django.forms.models import modelformset_factory
from rest_framework.authtoken.models import Token
from blackwidow.core.forms import EmailAddressForm
from blackwidow.core.models import EmailAddress

from blackwidow.core.models.common.custom_field import CustomFieldValue
from blackwidow.engine.decorators.utility import get_models_with_decorator
from config.apps import INSTALLED_APPS
from blackwidow.core.forms.common.contact_address_form import ContactAddressForm
from blackwidow.core.forms.common.phone_number_form import PhoneNumberForm
from blackwidow.core.forms.files.imagefileobject_form import ImageFileObjectForm
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin, GenericModelFormSetMixin
from blackwidow.core.models.common.contactaddress import ContactAddress
from blackwidow.core.models.common.phonenumber import PhoneNumber
from blackwidow.core.models.roles.role import Role
from blackwidow.core.models.users.user import ConsoleUser, SettingsItemValue
from blackwidow.core.models.users.user_settings import TimeZoneSettingsItem
from blackwidow.engine.extensions.generate_form_fields import genarate_form_field
from blackwidow.engine.extensions.list_extensions import inserted_list


__author__ = 'mahmudul'


class AccountInlineForm(GenericFormMixin):
    username = forms.CharField(label="Login", max_length=200)
    password = forms.CharField(label="Password", widget=forms.PasswordInput())
    confirm_password = forms.CharField(label="Confirm Password", widget=forms.PasswordInput())

    def __init__(self, data=None, files=None, instance=None, **kwargs):
        super().__init__(data=data, files=files, instance=instance, **kwargs)
        if instance:
            self.fields['username'].widget.attrs['readonly'] = True
            self.fields['password'].widget.attrs['readonly'] = True
            self.fields['confirm_password'].widget.attrs['readonly'] = True

    def clean(self):
        if self.cleaned_data['password'] != self.cleaned_data['confirm_password']:
            self.add_error('confirm_password', 'Password & Confirm Password must match.')
        return super().clean()

    def save(self, commit=True):
        with transaction.atomic():
            self.instance.save()
            self.instance.set_password(self.instance.password)
            self.instance = super().save(commit)
            Token.objects.create(user=self.instance)
            return self.instance

    def validate_unique(self):
        super().validate_unique()
        users = User.objects.filter(username=self.cleaned_data['username'])
        if len(users) > 0:
            self.add_error('username', 'User with id \'' + self.cleaned_data['username'] + '\' already exists. Please choose a different login id.')

    class Meta:
        model = User
        fields = ['username', 'password', 'confirm_password']
        widgets = {
            'password': forms.PasswordInput(),
            'confirm_password': forms.PasswordInput(),
        }


class CustomFieldValueForm(GenericFormMixin):
    def __init__(self, data=None, files=None, instance=None, **kwargs):
        super().__init__(data=data, files=files, instance=instance, **kwargs)

    class Meta:
        model = CustomFieldValue
        fields = []


contact_address_formset = modelformset_factory(ContactAddress, form=ContactAddressForm, formset=GenericModelFormSetMixin, extra=0,  min_num=1, validate_min=True, can_delete=True )
phone_number_formset = modelformset_factory(PhoneNumber, form=PhoneNumberForm, formset=GenericModelFormSetMixin, extra=0, min_num=1, validate_min=True, can_delete=True )
email_formset = modelformset_factory(EmailAddress, form=EmailAddressForm, formset=GenericModelFormSetMixin, extra=0, min_num=1, validate_min=True, can_delete=True )


class ConsoleUserForm(GenericFormMixin):
    def __init__(self, data=None, files=None, instance=None, allow_login=True, **kwargs):
        super().__init__(data=data, files=files, instance=instance, **kwargs)
        if self.fields['role'].initial is None:
            self.fields['role'].initial = ('', '-')

        if not self.request.c_user.is_super:
            if 'is_super' in self.fields:
                self.fields['is_super'].widget = forms.HiddenInput()
                self.fields['is_super'].initial = False

        # check if role context or not
        roles = Role.objects.filter(name=self.Meta.model.__name__)
        if 'is_role_context' in [x.__name__ for x in self.Meta.model._decorators]:
            self.fields['role'].choices = [(x.id, x.name) for x in roles]
        else:
            roles = Role.objects.all()
            business_roles = get_models_with_decorator('is_business_role', INSTALLED_APPS)
            self.fields['role'].choices = inserted_list([(x.id, x.name) for x in Role.objects.all() if x.name not in business_roles], 0, ('', ' --'))

        if allow_login and instance is None:
            kwargs.update({
                'prefix': 'account'
            })
            self.add_child_form("user", AccountInlineForm(data, files, form_header='Login Information', instance=instance.user if instance else None, **kwargs), is_prefix_form=True)

        if self.instance is not None and self.instance.pk is not None:
            addresses = self.instance.addresses.all()
            phones = self.instance.phones.all()
            emails = self.instance.emails.all()
            custom_fields = self.instance.custom_fields.all()
        else:
            addresses = ContactAddress.objects.none()
            phones = PhoneNumber.objects.none()
            emails = EmailAddress.objects.none()
            custom_fields = CustomFieldValue.objects.none()
        kwargs.update({
            'prefix': 'address'
        })
        self.add_child_form("addresses", contact_address_formset(data=data, files=files, queryset=addresses, header='Address', add_more=True, **kwargs))
        kwargs.update({
            'prefix': 'phone'
        })
        self.add_child_form("phones", phone_number_formset(data=data, files=files, queryset=phones, header='Phone', add_more=False, **kwargs))
        kwargs.update({
            'prefix': 'email'
        })
        self.add_child_form("emails", email_formset(data=data, files=files, queryset=emails, header='Email', add_more=False, **kwargs))
        kwargs.update({
            'prefix': 'image'
        })
        self.add_child_form("image", ImageFileObjectForm(data=data, files=files, instance=instance.image if instance is not None else None, form_header='Profile Picture', **kwargs))

        if len(roles) > 0 and 'is_role_context' in [x.__name__ for x in self.Meta.model._decorators]:
            kwargs.update({
                'prefix': 'meta'
            })
            custom_field_form = self
            _index = 0
            for x in roles[0].custom_fields.all().order_by('name'):
                custom_field_form.fields['field_' + str(_index)] = genarate_form_field(x.field_type)
                custom_field_form.fields['field_' + str(_index)].label = x.name
                if x.name not in [f.field.name for f in custom_fields]:
                    custom_field_form.fields['field_' + str(_index)].initial = ""
                else:
                    custom_field_form.fields['field_' + str(_index)].initial = [f.value for f in custom_fields if f.field.name == x.name][0]
                _index += 1

    def validate_unique(self):
        super().validate_unique()

    def save(self, commit=True):
        with transaction.atomic():
            self.instance = super().save(commit)

            roles = Role.objects.filter(name=self.Meta.model.__name__)
            self.instance.custom_fields.clear()
            if len(roles) > 0 and 'is_role_context' in [x.__name__ for x in self.Meta.model._decorators]:
                custom_field_form = self
                index = 0
                for x in roles[0].custom_fields.all().order_by('name'):
                    if ('field_' + str(index)) in custom_field_form.fields:
                        value = custom_field_form.cleaned_data['field_' + str(index)]
                    else:
                        value = ""
                    f_value = CustomFieldValue()
                    f_value.organization = self.instance.organization
                    f_value.value = value
                    f_value.field = x
                    f_value.save()
                    self.instance.custom_fields.add(f_value)
                    index += 1

            tz_settings, result = TimeZoneSettingsItem.objects.get_or_create(organization=self.request.c_organization)
            if result:
                tz_settings.save()
            tz_value, result = SettingsItemValue.objects.get_or_create(organization=self.request.c_organization, consoleuser__id=self.instance.pk, settings_item=tz_settings)
            if result:
                tz_value.value = str(-360)
                tz_value.save()
            return self.instance

    class Meta(GenericFormMixin.Meta):
        model = ConsoleUser
        fields = ['name', 'role', 'is_super']
        labels = {
            'name': 'Full Name',
            'male_or_female': 'Male or female?'
        }
        widgets = {
            'role': forms.Select(attrs={'class': 'select2'}),
            'organization': forms.HiddenInput(attrs={'class': 'select2'})
        }
