from blackwidow.core.forms.users.form import ConsoleUserForm
from blackwidow.core.models.users.employee import Employee

__author__ = 'Mahmud'


class EmployeeForm(ConsoleUserForm):

    class Meta(ConsoleUserForm.Meta):
        model = Employee
        # fields = ConsoleUserForm.Meta + ['reference_id']