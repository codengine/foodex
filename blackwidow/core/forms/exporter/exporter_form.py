from django.db import transaction
from django import forms
from django.forms.fields import ChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.config.exporter_config import ExporterConfig
from blackwidow.engine.decorators.utility import get_models_with_decorator
from config.apps import INSTALLED_APPS

__author__ = 'Mahmud'


class ExporterConfigForm(GenericFormMixin):
    def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
        _prefix = prefix
        if _prefix != '':
            _prefix += '-'
        exportable_models = get_models_with_decorator('enable_export', INSTALLED_APPS, include_class=True)
        exportable_models = [em.__name__ for em in exportable_models]
        existing_models = ExporterConfig.objects.values_list('model', flat=True)
        for model_name in existing_models:
            if model_name in exportable_models:
                exportable_models.remove(model_name)
        export_models = [ (name,name) for name in exportable_models ]

        self.fields['model'] = ChoiceField(choices=export_models, widget=forms.Select(attrs={'class': 'select2'}))
        self.fields['model'].label = 'Select Model'
        self.fields['model'].help_text = 'Select Model'


    def save(self, commit=True):
        with transaction.atomic():
            model = self.cleaned_data['model']
            self.instance.model = model
            super().save(commit)
            return self.instance

    class Meta:
        model = ExporterConfig
        fields = ['model','starting_row', 'starting_column']
        widgets = {
            'model': forms.Select(attrs={'class': 'select2'})
        }