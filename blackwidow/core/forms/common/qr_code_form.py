from django import forms
from django.db import transaction

from blackwidow.core.models.common.qr_code import QRCode
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.engine.exceptions.exceptions import BWException


__author__ = 'Mahmud'


class QRCodeForm(GenericFormMixin):
    value = forms.CharField(required=False, max_length=200)
    reference_id = forms.CharField(required=False)

    def clean(self):
        ref_id = self.cleaned_data['reference_id']
        value = self.cleaned_data['value']
        if ref_id == '':
           if value == '':
               raise BWException('Please enter either reference id or value of the QR Code.')
        return super().clean()


    def save(self, commit=True):
        with transaction.atomic():
            ref_id = self.cleaned_data['reference_id']
            value = self.cleaned_data['value']

            if ref_id != '' and value != '':
                instance = super().save(commit)
                instance.key = instance.value
                instance.save()
                self.instance = instance
                return self.instance

            if ref_id != '':
                qrcodes = QRCode.objects.filter(reference_id=ref_id)
                if qrcodes.exists():
                    self.instance =  qrcodes.first()
                else:
                    try:
                        qrcodes = QRCode.objects.filter(id=int(ref_id))
                    except:
                        if isinstance(ref_id, int):
                            qrcodes = QRCode.objects.filter(id=ref_id)
                        else:
                            qrcodes = QRCode.objects.none()

                    if qrcodes.exists():
                        self.instance = qrcodes.first()
                    else:
                        qrcodes = QRCode.objects.filter(code=ref_id)
                        if qrcodes.exists():
                            self.instance = qrcodes.first()
            if value != '':
                qrcodes = QRCode.objects.filter(value=value)
                if qrcodes.exists():
                    if qrcodes.first().is_used:
                        raise ValueError("QR Code already used.")
                    self.instance =  qrcodes.first()
                else:
                    self.instance = super().save(commit)
                    self.instance.key = self.instance.value
                    self.instance.save()
            return self.instance

    class Meta(GenericFormMixin.Meta):
        model = QRCode
        fields = ['value', 'reference_id']