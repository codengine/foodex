from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.common.location import Location


__author__ = 'mahmudul'


class LocationForm(GenericFormMixin):
    
    class Meta:
        model = Location
        fields = ['latitude', 'longitude']
