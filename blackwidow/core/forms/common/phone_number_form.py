from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.common.phonenumber import PhoneNumber

__author__ = 'ruddra'

from django import forms


class PhoneNumberForm(GenericFormMixin):
    phone = forms.CharField(max_length=100, required=False)
    class Meta(GenericFormMixin.Meta):
        model = PhoneNumber
        fields = ['phone', 'is_own']

