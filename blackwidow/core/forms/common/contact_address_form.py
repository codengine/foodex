from django import forms
from django.core.urlresolvers import reverse
from django.db import transaction

from blackwidow.core.models.common.division import Division
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.common.contactaddress import ContactAddress
from blackwidow.core.models.common.country import Country
from blackwidow.core.models.common.state import State
from blackwidow.core.forms.common.location_form import LocationForm
from blackwidow.core.models.common.upazila import Upazila
from config.enums.view_action_enum import ViewActionEnum


class ContactAddressForm(GenericFormMixin):

    def __init__(self, data=None, files=None, form_header='', instance=None, prefix='', **kwargs):
        super().__init__(data=data, files=files, form_header=form_header, instance=instance, prefix=prefix, **kwargs)
        _prefix = prefix + '-' if prefix != '' else ''
        self.fields['address_type'].choices = [('Home Address', 'Home Address'), ('Work Address', 'Work Address')]
        self.fields['province'] = GenericModelChoiceField(queryset=Upazila.objects.all(), widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'state', 'data-depends-property': 'parent:id', 'data-url': reverse(Upazila.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
        self.fields['state'] = GenericModelChoiceField(label='District', queryset=State.objects.all(), widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'division', 'data-depends-property': 'parent:id', 'data-url': reverse(State.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
        self.fields['division'] = GenericModelChoiceField(label='Division', queryset=Division.objects.all(), widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'country', 'data-depends-property': 'parent:id', 'data-url': reverse(Division.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
        self.fields['country'] = GenericModelChoiceField(queryset=Country.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
        self.add_child_form("location", LocationForm(data, files, form_header='Location', instance=instance.location if instance is not None else None, prefix= _prefix + str(len(self.suffix_child_forms)), **kwargs))

    def save(self, commit=True):
        with transaction.atomic():
            try:
                self.instance.province = Upazila.objects.get(pk=int(self.instance.province)).name
            except:
                pass
            return super().save(commit)

    class Meta:
        model = ContactAddress
        fields = ["address_type", 'street', 'city', 'country', 'division', 'state', 'province']
        widgets = {
            'street': forms.Textarea,
            'address_type': forms.Select(attrs={'class': 'select2'}, choices=[('Home Address', 'Home Address'), ('Work Address', 'Work Address')])
        }
        labels = {
            'state': 'District',
            'street': 'Street/Village',
            'province': 'Province/Upazila',
            'city': 'Union/City',
        }
