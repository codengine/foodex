from blackwidow.core.mixins.formmixin.form_mixin import GenericDefaultFormMixin

__author__ = 'ruddra'
from django import forms


class ConsoleUserResetPasswordForm(GenericDefaultFormMixin):
    password = forms.CharField(widget=forms.PasswordInput)
    retype_password = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data['password']
        re_password = cleaned_data['retype_password']
        if password != re_password:
            self.add_error('password', "passwords don't match")
        return cleaned_data


    def save(self, **kwargs):
        password = self.cleaned_data['password']
        user = kwargs['user']
        user.set_password(password)
        user.save()
