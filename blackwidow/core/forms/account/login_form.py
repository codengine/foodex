__author__ = 'mahmudul'

from django import forms
from django.contrib.auth import authenticate, login


class LoginForm(forms.Form):
    username = forms.CharField(required=True, label="Username")
    password = forms.CharField(required=True, widget=forms.PasswordInput)
    timezone = forms.CharField(required=False, widget=forms.HiddenInput)
    remember_me = forms.BooleanField(required=False, widget=forms.CheckboxInput, label="Remember Me?")

    def authenticate(self, request):
        data = self.cleaned_data
        user = authenticate(username=data["username"], password=data["password"])
        if user is not None:
            if user.is_active:
                login(request, user)
                if not data['remember_me']:
                    request.session.set_expiry(0)
                return True
            else:
                self.add_error(None, "Your account has been disabled!")
        else:
            self.add_error(None, "Your username and password were incorrect.")
        return False


