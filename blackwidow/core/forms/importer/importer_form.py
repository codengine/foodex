from django.db import transaction
from django import forms
from django.forms.fields import ChoiceField

from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.config.importer_config import ImporterConfig
from blackwidow.engine.decorators.utility import get_models_with_decorator
from config.apps import INSTALLED_APPS

__author__ = 'Mahmud'


class ImporterConfigForm(GenericFormMixin):
    def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
        _prefix = prefix
        if _prefix != '':
            _prefix += '-'
        importable_models = get_models_with_decorator('enable_import', INSTALLED_APPS, include_class=True)
        importable_models = [em.__name__ for em in importable_models]
        existing_models = ImporterConfig.objects.values_list('model', flat=True)
        for model_name in existing_models:
            if model_name in importable_models:
                importable_models.remove(model_name)
        import_models = [ (name,name) for name in importable_models ]

        self.fields['model'] = ChoiceField(choices=import_models, widget=forms.Select(attrs={'class': 'select2'}))
        self.fields['model'].label = 'Select Model'
        self.fields['model'].help_text = 'Select Model'


    def save(self, commit=True):
        with transaction.atomic():
            model = self.cleaned_data['model']
            self.instance.model = model
            super().save(commit)
            return self.instance

    class Meta:
        model = ImporterConfig
        fields = ['model','starting_row', 'starting_column']
        widgets = {
            'model': forms.Select(attrs={'class': 'select2'})
        }