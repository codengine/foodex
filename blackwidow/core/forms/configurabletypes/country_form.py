from django.db import transaction

from blackwidow.core.forms.configurabletypes.configurabletype_form_mixin import ConfigurableTypeFormMixin
from blackwidow.core.models.common.country import Country
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin


class CountryForm(ConfigurableTypeFormMixin):

    class Meta(GenericFormMixin.Meta):
        model = Country
        fields = ['name']