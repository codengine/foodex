from django.db import transaction
from django.forms.util import ErrorList

from blackwidow.core.forms.configurabletypes.configurabletype_form_mixin import ConfigurableTypeFormMixin
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.common.division import Division
from blackwidow.core.models.common.state import State


__author__ = 'ruddra'

from django import forms


class StateForm(ConfigurableTypeFormMixin):
    # parent = forms.ModelChoiceField(queryset=Country.objects.all(), label='Select Country', widget=forms.Select(attrs={'class': 'select1'}), required=True)

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None,
                 initial=None, error_class=ErrorList, label_suffix=None,
                 empty_permitted=False, **kwargs):
        super(StateForm, self).__init__(data=data, files=files, auto_id=auto_id, prefix=prefix, initial=initial, error_class=error_class, label_suffix=label_suffix, empty_permitted=empty_permitted, **kwargs)
        self.fields['parent'] = GenericModelChoiceField(queryset=Division.objects.all(), label='Select Division', widget=forms.Select(attrs={'class': 'select2'}), required=True)

    class Meta(GenericFormMixin.Meta):
        model = State
        fields = ['name', 'parent']
        labels = {
            'parent': 'Country'
        }
