from django.db import transaction
from django.forms.util import ErrorList

from blackwidow.core.forms.configurabletypes.configurabletype_form_mixin import ConfigurableTypeFormMixin
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.common.state import State
from blackwidow.core.models.common.upazila import Upazila


__author__ = 'ruddra'

from django import forms


class UpazilaForm(ConfigurableTypeFormMixin):
    # parent = forms.ModelChoiceField(queryset=Country.objects.all(), label='Select Country', widget=forms.Select(attrs={'class': 'select1'}), required=True)

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None,
                 initial=None, error_class=ErrorList, label_suffix=None,
                 empty_permitted=False, **kwargs):
        super().__init__(data=data, files=files, auto_id=auto_id, prefix=prefix, initial=initial, error_class=error_class, label_suffix=label_suffix, empty_permitted=empty_permitted, **kwargs)
        self.fields['parent'] = GenericModelChoiceField(queryset=State.objects.all(), label='Select District', widget=forms.Select(attrs={'class': 'select2'}), required=True)

    class Meta(GenericFormMixin.Meta):
        model = Upazila
        fields = ['name', 'parent']
        labels = {
            'parent': 'District'
        }
