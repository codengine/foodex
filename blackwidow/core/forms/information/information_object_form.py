from django import forms

from blackwidow.core.models.information.information_object import InformationObject
from blackwidow.core.mixins.fieldmixin.multiple_select_field_mixin import GenericModelMultipleChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.roles.role import Role
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.engine.exceptions.exceptions import BWException
from blackwidow.engine.extensions.clock import Clock


__author__ = 'Mahmud'


class InformationObjectForm(GenericFormMixin):
    start_time = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M:%S'], widget=forms.DateTimeInput(attrs={'data-format':"dd/MM/yyyy hh:mm:ss", 'class': 'date-time-picker', 'readonly': 'True'}, format='%d/%m/%Y %H:%M:%S'))
    end_time = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M:%S'], widget=forms.DateTimeInput(attrs={'data-format':"dd/MM/yyyy hh:mm:ss", 'class': 'date-time-picker', 'readonly': 'True'}, format='%d/%m/%Y %H:%M:%S'))

    def clean(self):
        if self.cleaned_data['start_time'] >= self.cleaned_data['end_time']:
            raise BWException("End time must be greater than start time.")
        return super().clean()

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, instance=None, **kwargs):
        super().__init__(data=data, files=files, auto_id=auto_id, prefix=prefix, instance=instance, **kwargs)
        self.fields['recipient_roles'] = GenericModelMultipleChoiceField(required=False, queryset=Role.objects.all(), widget=forms.SelectMultiple(attrs={'class': 'select2'}), initial=instance.recipient_roles.all() if instance is not None else None)
        self.fields['recipient_users'] = GenericModelMultipleChoiceField(required=False, queryset=ConsoleUser.objects.all(), widget=forms.SelectMultiple(attrs={'class': 'select2'}), initial=instance.recipient_users.all() if instance is not None else None)
        if instance:
            self.fields['start_time'].initial = Clock.get_user_local_time(instance.start_time)
            self.fields['end_time'].initial = Clock.get_user_local_time(instance.end_time)

    def save(self, commit=True):
        self.instance.start_time = (self.cleaned_data['start_time']).timestamp() * 1000
        self.instance.end_time = (self.cleaned_data['end_time']).timestamp() * 1000
        instance = super().save(commit=False)
        for item in self.cleaned_data['recipient_roles']:
            instance.recipient_roles.add(item)
        for item in self.cleaned_data['recipient_users']:
            instance.recipient_users.add(item)
        return instance

    class Meta:
            model = InformationObject
            fields = ['name', 'details', 'notify_by_email', 'recipient_roles', 'recipient_users']
            labels = {
                'name': 'Subject'
            }
            widgets = {
                'details': forms.Textarea(attrs={'class': 'description'}),
                # 'start_time': forms.TextInput(attrs={'data-format':"dd/MM/yyyy hh:mm:ss", 'class': 'date-time-picker'}),
                # 'end_time': forms.TextInput(attrs={'data-format':"dd/MM/yyyy hh:mm:ss", 'class': 'date-time-picker'})
            }