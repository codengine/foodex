from blackwidow.core.forms.information.information_object_form import InformationObjectForm
from blackwidow.core.models.information.news import News

__author__ = 'Mahmud'


class NewsForm(InformationObjectForm):
    class Meta(InformationObjectForm.Meta):
        model = News