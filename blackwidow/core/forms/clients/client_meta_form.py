from blackwidow.core.forms.files.imagefileobject_form import ImageFileObjectForm
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.clients.client_meta import ClientMeta

__author__ = 'ruddra'


class ClientMetaForm(GenericFormMixin):

    def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
        self.add_child_form("image", ImageFileObjectForm(data=data, files=files, instance=instance.image if instance is not None else None, header='Image', prefix=prefix + str(len(self.suffix_child_forms)), **kwargs))

    class Meta(GenericFormMixin.Meta):
        model = ClientMeta
        fields = ['client_male_or_female', 'guardian_name', 'guardian_choice_type', 'selling_area']
        labels = {
            'client_male_or_female': 'Male or female?'
        }
