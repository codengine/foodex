from django.db.models.query_utils import Q

__author__ = 'Mahmud'

from django.db import models

class DomainEntityModelManager(models.Manager):
    _filter = None

    def __init__(self, *args, filter=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._filter = filter

    def get_queryset(self):
        if self._filter is not None:
            return super().get_queryset().filter(Q(**self._filter))
        if self.model._meta.proxy:
            return super().get_queryset().filter(Q(**{self.model.discriminator_property(): self.model.__name__}))
        return super().get_queryset()


class DomainEntityQuerySet(models.QuerySet):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__setattr__('base_count', super().count)
        self.__setattr__('base_first', super().first)
        self.__setattr__('base_last', super().last)
        self.__setattr__('base_filter', super().filter)
        self.__setattr__('base_all', super().all)
        self.__setattr__('base_latest', super().latest)

    def _filter_or_exclude(self, negate, *args, **kwargs):
        if self.model._meta.proxy:
            # kwargs.update({
            #     self.model.discriminator_property(): self.model.__name__
            # })
            self.query.add_filter((self.model.discriminator_property(), self.model.__name__))
        return super()._filter_or_exclude(negate, *args, **kwargs)

    def exists(self):
        if self.model._meta.proxy:
            self.query.add_filter((self.model.discriminator_property(), self.model.__name__))
        return super().exists()

    def all(self):
        if self.model._meta.proxy:
            self.query.add_filter((self.model.discriminator_property(), self.model.__name__))
        return self.base_all()

    def count(self):
        if self.model._meta.proxy:
            self.query.add_filter((self.model.discriminator_property(), self.model.__name__))
        return self.base_count()

    def first(self):
        if self.model._meta.proxy:
            self.query.add_filter((self.model.discriminator_property(), self.model.__name__))
        return self.base_first()

    def latest(self, field_name=None):
        if self.model._meta.proxy:
            self.query.add_filter((self.model.discriminator_property(), self.model.__name__))
        return self.base_latest(field_name=field_name)

    def has_organization(self):
        return False


class OrganizationDomainEntityQuerySet(DomainEntityQuerySet):

    def has_organization(self):
        return True