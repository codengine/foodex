from django.db.models.fields import FieldDoesNotExist
from django.db import models


class ProtectedQuerySetMixin(object):
    model = None
    request = None

    @classmethod
    def build_search_fields(self, model=None, columns=None, **kwargs):
        filters = tuple()
        all_props = dir(model)
        exclude_list = model.exclude_search_fields()
        for c in columns:
            try:
                field_name = c.name
                if c.name.startswith('render_'):
                    field_name = c.name.replace('render_', '')

                if c.name in exclude_list:
                    continue

                url = ''
                route = model.get_url_by_name(c.name)
                if route is not None and "__search__" in route:
                    route_split = route.split("__")
                    if route_split[2] == 'text':
                        filters += ((route, c.verbose_name, False, url), )
                        continue
                    filters += ((route, c.verbose_name, False, url), )
                    continue
                field = model._meta.get_field_by_name(c.name)
                if isinstance(field[0], (models.CharField, models.OneToOneField, models.ForeignKey)):
                    # if isinstance(field[0], models.ForeignKey):
                    #     if route is None:
                    #         c_model = getattr(model, field[0].name).field.rel.to
                    #         route = c_model.get_model_meta('route', 'route')
                    #     url = '/' + route + '/?format=json&search=1&disable_pagination=1'
                    filters += ((field[0].name, c.verbose_name, False, url), )
                elif isinstance(field[0], (models.DateTimeField, models.DateField, )):
                    filters += ((field[0].name, c.verbose_name, True, url), )
                elif field[0].name in model.get_datetime_fields():
                    filters += ((field[0].name, c.verbose_name, True, url), )
            except FieldDoesNotExist:
                if c.name in all_props and c.name.startswith("render_"):
                    if field_name in model.get_datetime_fields():
                        filters += ((c.name.replace("render_", "__search_"), field_name, True, "" ), )
                    else:
                        filters += ((c.name.replace("render_", "__search_"), field_name, False, "" ), )
        return filters

    def get_queryset(self, **kwargs):
        _user = self.request.c_user.to_business_user()
        _queryset = self.model.get_queryset(queryset=self.model.objects.all(), user=_user, profile_filter=not (_user.is_super))
        _queryset = _user.filter_model(request=self.request, queryset=_queryset)
        queryset = self.model.apply_search_filter(self.request, queryset=_queryset, **kwargs)
        try:
            queryset = queryset.order_by(self.model.default_order_by())
        except:
            pass

        return queryset


