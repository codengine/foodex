from django.contrib import messages
from django.core.urlresolvers import reverse, NoReverseMatch
from django.shortcuts import render
from django.template.context import RequestContext
from django.utils.safestring import mark_safe
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from blackwidow.core.mixins.viewmixin.protected_details_link_config_view_mixin import \
    ProtectedDetailsLinkConfigViewMixin
from blackwidow.core.mixins.viewmixin.protected_inline_manage_button_view_mixin import \
    ProtectedInlineManageButtonDecisionViewMixin

from blackwidow.core.mixins.viewmixin.protected_manage_button_view_mixin import ProtectedManageButtonViewMixin
from blackwidow.core.mixins.viewmixin.protected_queryset_mixin import ProtectedQuerySetMixin
from blackwidow.engine.extensions.pluralize import pluralize
from config.enums.view_action_enum import ViewActionEnum
from blackwidow.core.managers.contextmanager import ContextManager
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.engine.extensions import bw_titleize
from blackwidow.engine.mixins.viewmixin.json_view_mixin import JsonMixin
from blackwidow.engine.templatetags.blackwidow_filter import remove_quote


class ProtectedViewMixin(ProtectedQuerySetMixin, ProtectedManageButtonViewMixin, ProtectedDetailsLinkConfigViewMixin, ProtectedInlineManageButtonDecisionViewMixin, JsonMixin):
    form_class = GenericFormMixin
    model = None
    initial = None
    success_url_name = ''
    success_url = ''
    model_name = ''
    module_name = ''
    template_name = ''
    action = ViewActionEnum.Details
    object = None

    @classmethod
    def get_view_meta(cls, decorator_name, name):
        try:
            if name in cls._registry[cls.__name__][decorator_name]:
                return cls._registry[cls.__name__][decorator_name][name]
        except Exception as exp:
            if name == 'display_name':
                return bw_titleize(cls.__name__)

            if name == 'route':
                return pluralize(cls.__name__.lower())

            return None

    def get_success_url(self):
        if self.success_url != '':
            return self.success_url
        if self.object is not None:
            try:
                return reverse(self.success_url_name, kwargs={'pk': self.object.id})
            except NoReverseMatch:
                if self.request.is_ajax() or self.is_json_request(self.request):
                    return ''
                return reverse(self.success_url_name)
        return reverse(self.success_url_name)

    def is_json_request(self, request):
        if request.GET.get('format', 'html') == 'json' \
                or request.POST.get('format', 'html') == 'json':
            return True
        return False

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProtectedViewMixin, self).dispatch(*args, **kwargs)

    def get_urls(self, urllist, depth=0):
        urls = list()
        for entry in urllist:
            urls.append("  " * depth + entry.regex.pattern)
            if hasattr(entry, 'url_patterns'):
                urls += self.get_urls(entry.url_patterns, depth + 1)
        return urls

    def get_context_data(self, **kwargs):
        context = super(ProtectedViewMixin, self).get_context_data(**kwargs)
        n_context = ContextManager.get_current_context(self.request)
        if n_context is None and self.request.user is not None:
            c_users = ConsoleUser.objects.filter(user__id=self.request.user.id)
            if len(c_users) > 0:
                ContextManager.initialize_context(self.request, {'user': c_users[0].id})

        context['context'] = ContextManager.get_current_context(self.request)
        if self.model is not None:
            context['model_meta'] = {
                'model_name': bw_titleize(self.model.__name__ if self.model_name == '' or self.model_name is None else self.model_name),
            }
        return context

    def form_invalid(self, form):
        if self.is_json_request(self.request) or self.request.is_ajax():
            storage = messages.get_messages(self.request)
            return self.render_json_response({
                'success': False,
                'message': mark_safe(',<br/> '.join([x + ': ' + remove_quote(form.errors[x][0]) for x in form.errors]))
            })
        view_context = self.get_context_data(form=form)
        bw_context = ContextManager.get_current_context(self.request)
        return render(self.request, self.get_template_names(), {
            'form': form,
            'context': bw_context,
            'model_meta': view_context['model_meta'],
            'context_instance': RequestContext(self.request)
        })

    def form_valid(self, form):
        result = super().form_valid(form)
        if self.is_json_request(self.request) or self.request.is_ajax():
            storage = messages.get_messages(self.request)
            storage.used = True
            return self.render_json_response({
                'success': True,
                'message': 'Operation completed successfully.',
                'load': 'ajax'
            })
        return result

    def get_prefix(self):
        return ''

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        kwargs.update({
            'request': self.request
        })
        return kwargs

    def get_initial(self):
        if self.initial is None:
            self.initial = {}
        return self.initial
