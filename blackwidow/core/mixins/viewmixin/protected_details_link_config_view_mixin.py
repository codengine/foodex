from blackwidow.core.mixins.modelmixin import RoutedModelMixin
from blackwidow.engine.managers.bwpermissionmanager import BWPermissionManager
from blackwidow.sales.models import Order
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'zia'


class ProtectedDetailsLinkConfigViewMixin(object):
    def details_link_config(self, **kwargs):
        self.detail_links = self.object.details_link_config(**kwargs)

        m_buttons = list()

        for detail_link in self.detail_links:
            url_name = detail_link["url_name"]
            if url_name.endswith(ViewActionEnum.Approve.value):
                if BWPermissionManager.has_edit_permission(self.request, self.model):
                    m_buttons += [dict(detail_link),]
            elif url_name.endswith(ViewActionEnum.Reject.value):
                if BWPermissionManager.has_edit_permission(self.request, self.model):
                    m_buttons += [dict(detail_link),]
            elif url_name.endswith(ViewActionEnum.Edit.value):
                if BWPermissionManager.has_edit_permission(self.request, self.model):
                    m_buttons += [dict(detail_link),]
            elif url_name.endswith(ViewActionEnum.AdvancedEdit.value):
                if BWPermissionManager.has_edit_permission(self.request, self.model):
                    m_buttons += [dict(detail_link),]
            elif url_name.endswith(ViewActionEnum.Mutate.value):
                if BWPermissionManager.has_edit_permission(self.request, self.model):
                    m_buttons += [dict(detail_link),]
            elif url_name.endswith(ViewActionEnum.Print.value):
                if BWPermissionManager.has_view_permission(self.request, self.model):
                    m_buttons += [dict(detail_link),]
            elif url_name.endswith("consoleuser_reset_password"):
                m_buttons += [dict(detail_link),]

        return m_buttons