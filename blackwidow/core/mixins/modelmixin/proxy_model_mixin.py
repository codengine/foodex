__author__ = 'Mahmud'


class ProxyModelMixin(object):

    @classmethod
    def discriminator_property(cls):
        return 'type'

    @classmethod
    def get_crud_model(cls):
        return cls

    def to_subclass(self):
        for x in self.__class__.__subclasses__():
            if x.__name__ == self.type:
                self.__class__ = x
        return self