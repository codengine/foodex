from collections import OrderedDict

from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe

from blackwidow.engine.extensions.bw_titleize import bw_titleize
from blackwidow.engine.extensions.clock import Clock
from config.enums.view_action_enum import ViewActionEnum
from settings import LANGUAGES


__author__ = 'Mahmud'


class DetailsViewModelMixin(object):
    @property
    def tabs_config(self):
        return [
        ]

    def __str__(self):
        fields = self._meta.fields
        name_field = [x for x in fields if x.name == 'name']

        text = ''
        if len(name_field) > 0:
            if self.reference_id is not None and self.reference_id != '' and len(self.reference_id) <= 10:
                text = self.reference_id + ': ' + str(self.name)
            else:
                text = str(self.code) + ": " + str(self.name)
        else:
            if self.reference_id is not None:
                text = self.reference_id
            else:
                text = self.code
        try:
            _c = self.__class__
            for x in self.__class__.__subclasses__():
                if x.__name__ == self.type:
                    _c = x
                    break
            if ViewActionEnum.Details in _c.get_routes():
                return mark_safe("<a class='inline-link' href='" + reverse(_c.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.pk}) + "' >" + text + "</a>")
        except:
            return text
        return text

    @classmethod
    def get_datetime_fields(cls):
        return ['date_created', 'last_updated']

    def render_timestamp(self, value):
        _d = Clock.get_user_local_time(value).strftime("%d/%m/%Y - %I:%M %p")
        return _d

    @property
    def details_config(self):
        fields = self._meta.fields

        name_field = [x for x in fields if x.name == 'name']
        datetime_fields = self.__class__.get_datetime_fields()

        a = OrderedDict()
        a['code'] = self.code
        a['name'] = getattr(self, 'name') if len(name_field) > 0 else bw_titleize(self.__class__.__name__)

        _f = ['code', 'name', 'creation_flag'] + datetime_fields + list(self.__class__.get_system_fields())

        try:
            for l in LANGUAGES:
                t_options = self.__class__.get_translator_options()
                for f in t_options.fields:
                    a[f + ' (' + l[1] + ')'] = getattr(self, f + '_' + l[0])
                    _f.append(f + '_' + l[0])
        except Exception as exp:
            pass

        for f in fields:
            if f.name not in _f:
                value = getattr(self, f.name)
                if isinstance(value, list):
                    a[f.name] = mark_safe('<br/>'.join([str(x) for x in value.all()]))
                else:
                    a[f.name] = value if value is not None else ''

        for f in datetime_fields:
            value = getattr(self, f)
            a[f] = self.render_timestamp(value)
        return a