

class MutableModelMixin(object):

    def mutate_to(self, **kwargs):
        return self


class ApprovableModelMixin(object):

    def approve_to(self, **kwargs):
        return self


class RejectableModelMixin(object):

    def reject_to(self, **kwargs):
        return self
