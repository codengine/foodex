from modeltranslation.translator import TranslationOptions

__author__ = 'Mahmud'


class SerializableModelMixin(object):

    @property
    def serializable_fields(self):
        return ()

    @classmethod
    def get_system_fields(cls):
        return 'date_created', 'last_updated', 'is_active', 'is_deleted', 'is_locked', \
               'created_by', 'last_updated_by', 'type', 'context', 'reference_id'

    @classmethod
    def intermediate_models(cls):
        return tuple()

    @classmethod
    def get_custom_serializers(cls):
        return tuple()

    @classmethod
    def serialize_fields(cls):
        return []

    @classmethod
    def get_translator_options(cls):
        class DETranslationOptions(TranslationOptions):
            pass
        return DETranslationOptions