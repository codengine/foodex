from blackwidow.engine.extensions.bw_titleize import bw_titleize
from blackwidow.engine.extensions.pluralize import pluralize
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Mahmud'


class RoutedModelMixin(object):
    @classmethod
    def get_form(cls, parent, **kwargs):
        class DynamicForm(parent):
            class Meta:
                model = cls
                fields = []
        return DynamicForm

    @classmethod
    def get_model_meta(cls, decorator_name, name):
        try:
            if name in cls._registry[cls.__name__][decorator_name]:
                return cls._registry[cls.__name__][decorator_name][name]
        except Exception as exp:
            if name == 'display_name':
                return bw_titleize(cls.__name__)

            if name == 'route':
                return pluralize(cls.__name__.lower())

            return None

    @classmethod
    def get_route_name(cls, action=ViewActionEnum.Details, parent=''):
        return parent + ('_' if parent != '' else '') + cls.__name__.lower() + '_' + str(action)

    @classmethod
    def get_routes(cls, partial=False, **kwargs):
        if partial:
            return [ViewActionEnum.PartialEdit, ViewActionEnum.PartialCreate, ViewActionEnum.PartialBulkAdd,
                    ViewActionEnum.PartialBulkRemove, ViewActionEnum.PartialDelete]
        return [ViewActionEnum.Create, ViewActionEnum.Edit, ViewActionEnum.Details, ViewActionEnum.Delete,
                ViewActionEnum.Tab, ViewActionEnum.Manage, ViewActionEnum.Mutate, ViewActionEnum.Print, ViewActionEnum
                .Export, ViewActionEnum.Download, ViewActionEnum.Approve, ViewActionEnum.Reject, ViewActionEnum.
                AdvancedImport, ViewActionEnum.AdvancedExport, ViewActionEnum.Restore, ViewActionEnum.AdvancedEdit,
                ViewActionEnum.AdvancedDownload]


    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Edit',
                action='edit',
                icon='fbx-rightnav-edit',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit)
            ),
            dict(
                name='Delete',
                action='delete',
                title="Click to remove this item",
                icon='fbx-rightnav-delete',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Delete),
                classes='manage-action all-action confirm-action',
                parent=None
            ),
        ]

    @classmethod
    def get_optional_routes(cls, base_url=ViewActionEnum.Create, model_name='', parent=''):
        options = cls.get_model_meta('route', 'options')
        model_name = model_name if model_name != '' else cls.__name__
        if options is not None:
            option_urls = []
            for _op in options:
                option_urls.append(dict(
                    name=bw_titleize(_op[1]) + bw_titleize(model_name),
                    url_name=cls.get_route_name(base_url, parent=parent),
                    parameters='?type=' + _op[1]
                ))
            return option_urls
        return []
