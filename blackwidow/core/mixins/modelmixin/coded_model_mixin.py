import re

__author__ = 'Mahmud'


class CodedModelMixin(object):
    @classmethod
    def prefix(cls):
        return re.sub(r'[a-z\d]+|(?<=[A-Z])[A-Z\d]+', r'', cls.__name__)

    @classmethod
    def get_class_name(cls):
        return cls.__name__

    @property
    def code_prefix(self):
        return self.__class__.prefix()

    @property
    def code_separator(self):
        return "-"