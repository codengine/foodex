from crequest.middleware import CrequestMiddleware
from django.db import transaction
from django.forms.models import ModelForm, BaseModelFormSet
from django.forms.util import ErrorList

from blackwidow.core.models.contracts.base import DomainEntity
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.log.logs import ErrorLog
from blackwidow.engine.extensions.bw_titleize import bw_titleize
import uuid


__author__ = 'mahmudul'

from django import forms


class GenericModelFormSetMixin(BaseModelFormSet):
    request = None
    header = ''
    add_more = True
    inline = False
    partial_view = False
    tab = ''
    render_table = False

    def __init__(self, request=None, instance=None, header='', render_table=False, form_header='', display_inline=False, add_more=True, partial_view=False, tab='', **kwargs):
        super().__init__(**kwargs)
        self.request = request
        self.header = header
        self.add_more = add_more
        self.inline = display_inline
        self.partial_view = partial_view
        self.tab = tab
        self.render_table = render_table

    def _construct_form(self, i, **kwargs):
        kwargs.update({
            'request': self.request,
            'tab': self.tab,
            'partial_view': self.partial_view,
            'render_table': self.render_table
        })
        return super()._construct_form(i, **kwargs)


class GenericFormMixin(ModelForm):
    id = forms.CharField(required=False, widget=forms.HiddenInput)
    step = forms.CharField(required=False, widget=forms.HiddenInput)
    total_steps = forms.CharField(required=False, widget=forms.HiddenInput)
    child_forms = list()
    header = ''
    encryption_enabled = None
    request = None
    inline = False
    partial_view = False
    render_table = False

    @classmethod
    def get_template(cls):
        return ''

    @property
    def prefix_child_forms(self):
        return [x[1] for x in self.child_forms if x[2]]

    @property
    def suffix_child_forms(self):
        return [x[1] for x in self.child_forms if not x[2]]

    def add_child_form(self, key, form, is_prefix_form=False):
        if key is None:
            key = form.__class__.__name__.lower()
        self.child_forms.append((key, form, is_prefix_form))

    def get_wizard_form(self, step=0):
        steps = self.prefix_child_forms + [self] + self.suffix_child_forms
        return steps[step]

    def show_form_inline(self):
        return False

    @property
    def field_count(self):
        return len(self.fields) + (1 if self.render_table else 0)

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix="",
                 initial=None, error_class=ErrorList, label_suffix=None, render_table=False,
                 empty_permitted=False, request=None, partial_view=False, instance=None, generate_childforms=False, childform_dict={}, display_inline=False,  **kwargs):
        super(). __init__(data=data, files=files, auto_id=auto_id, prefix=prefix,
                                                initial=initial, error_class=error_class, label_suffix=label_suffix,
                                                empty_permitted=empty_permitted, instance=instance)
        self.request = request
        self.child_forms = list()

        self.partial_view = partial_view
        self.encryption_enabled = False
        self.header = kwargs.get('form_header', '')
        self.inline = display_inline
        self.render_table = render_table
        if self.header == '':
            self.header = bw_titleize(self.Meta.model.__name__) + ' form'

    def is_valid(self):
        valid = True
        for f in self.prefix_child_forms:
            valid &= f.is_valid()
        for f in self.suffix_child_forms:
            valid &= f.is_valid()
        return super().is_valid() and valid

    def load_default_fields(self):
        request = CrequestMiddleware.get_request()
        if isinstance(self.instance, OrganizationDomainEntity):
            self.instance.organization = request.c_organization

        if isinstance(self.instance, DomainEntity) and not self.instance.created_by:
            self.instance.last_updated_by = request.c_user
            self.instance.created_by = request.c_user
            self.instance.tsync_id = uuid.uuid4()
        else:
            self.instance.last_updated_by = request.c_user

    def save(self, commit=True):
        from django.db import models

        f_models = dict()
        m2m_models = dict()
        self.load_default_fields()
        for (k, f, p) in self.child_forms:
            try:
                field = self.instance._meta.get_field(k)
                if isinstance(field, (models.OneToOneField, models.ForeignKey)):
                    f_models[k] = f
                elif isinstance(field, models.ManyToManyField):
                    m2m_models[k] = f
            except Exception as exp:
                ErrorLog.log(exp)
        with transaction.atomic():
            for k in f_models.keys():
                setattr(self.instance, k, f_models[k].save(commit))
            self.instance = super().save(commit)
            for k in m2m_models.keys():
                _items = getattr(self.instance, k)
                _item_dict = dict()

                for _f in m2m_models[k]:
                    try:
                        _item = _f.save(commit)
                        _item_dict[_item.pk] = _item

                        if not _items.filter(pk=_item.pk).exists():
                            _items.add(_item)
                    except:
                        pass

                for _item in _items.all():
                    if _item.pk not in _item_dict:
                        _items.remove(_item)

            return self.instance

    @classmethod
    def get_step_count(cls):
        return 1

    class Meta:
        model = DomainEntity
        fields = []
        exclude = ['date_created', 'last_updated', 'created_by', 'last_updated_by']


class GenericDefaultFormMixin(forms.Form):
    id = forms.CharField(required=False, widget=forms.HiddenInput)
    step = forms.CharField(required=False, widget=forms.HiddenInput)
    total_steps = forms.CharField(required=False, widget=forms.HiddenInput)

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None,
                 initial=None, error_class=ErrorList, label_suffix=None,
                 empty_permitted=False, request=None, partial_view=False, generate_childforms=False, childform_dict={}, display_inline=False,  **kwargs):
        super(). __init__(data=data, files=files, auto_id=auto_id, prefix=prefix,
                          initial=initial, error_class=error_class, label_suffix=label_suffix,
                          empty_permitted=empty_permitted)

    def save(self, **kwargs):
        pass





