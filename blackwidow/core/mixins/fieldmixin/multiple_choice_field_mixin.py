from django.forms.models import ModelChoiceField

__author__ = 'Mahmud'


class GenericModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        ch_name = obj.get_choice_name()
        return obj.get_choice_name()