from django.forms.models import ModelMultipleChoiceField

__author__ = 'Mahmud'


class GenericModelMultipleChoiceField(ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.get_choice_name()