from django.conf.urls import patterns, url

from blackwidow.core.generics.views.dashboard_view import GenericDashboardView
from blackwidow.core.generics.views.model_descriptor_view import GenericModelDescriptorView
from blackwidow.core.generics.views.notification_view import GenericNotificationView
from blackwidow.core.models.information.news import News
from blackwidow.core.models.information.notification import Notification
from blackwidow.core.views import *
from blackwidow.core.generics.views.error_view import GenericErrorView
from blackwidow.core.views.account.reset_password_view import ResetPasswordRequestView
from blackwidow.core.views.users.consoleuser_reset_password_view import ConsoleUserResetPasswordView
import settings
urlpatterns = patterns('',
                       url(r'^$', HomeView.as_view(), name="bw_home"),
                       url(r'^account/login', LoginView.as_view(), name="bw_login"),
                       url(r'^account/reset_password_confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', PasswordResetConfirmView.as_view(),name='bw_reset_password_confirm'),
                       url(r'^account/reset_password', ResetPasswordRequestView.as_view(), name="bw_reset_password"),
                       url(r'^consoleuser/reset_password/(?P<id>\d+)/', ConsoleUserResetPasswordView.as_view(), name= "consoleuser_reset_password"),
                       url(r'^account/logout', LogoutView.as_view(), name="bw_logout"),
                       url(r'^account/register', RegisterView.as_view(), name="bw_registration"),
                       # url(r'^account/login', include('allauth.urls')),
                       url(r'^no-access', GenericErrorView.as_view(template_name='shared/no-access.html'), name="no_access"),
                       url(r'^error', GenericErrorView.as_view(template_name='shared/error.html'), name="error"),
                       # -------------------- Test purpose --------
                       url(r'^inprogress', GenericDashboardView.as_view(), name="coming_soon"),


                       # -------------------- Dashboards ---------------------------------#
                       url(r'^administration/dashboard', GenericDashboardView.as_view(), name="admin_dashboard"),
                       url(r'^settings/dashboard', GenericDashboardView.as_view(), name="config_dashboard"),
                       url(r'^settings', GenericDashboardView.as_view(), name="settings_coming_soon"),
                       url(r'^help/dashboard', GenericDashboardView.as_view(), name="help_dashboard"),
                       url(r'^help/about', GenericDashboardView.as_view(), name="help_about"),
                       url(r'^users/dashboard', GenericDashboardView.as_view(), name="user_dashboard"),
                       url(r'^maintenance/dashboard', GenericDashboardView.as_view(), name="maintenance_dashboard"),
                       url(r'^kpi/dashboard', GenericDashboardView.as_view(), name="kpi_dashboard"),

                       # -------------------- Model Descriptor ------------------------#
                       url(r'^diagnostics/model_descriptor/', GenericModelDescriptorView.as_view(), name="model_descriptor"),

                       url(r'^_profile/news/count/', GenericNotificationView.as_view(model=News), name='news_count_view'),
                       url(r'^_profile/news/', GenericNotificationView.as_view(model=News), name='news_list_view'),
                       url(r'^_profile/notifications/count/', GenericNotificationView.as_view(model=Notification), name='notification_count_view'),
                       url(r'^_profile/notifications/', GenericNotificationView.as_view(model=Notification), name='notification_list_view')


)

#static file urls
urlpatterns += patterns('',
                        url(r'^static/' + '(.*)$', 'django.views.static.serve'),
                        url(r'^static-media/'+ '(.*)$', 'django.views.static.serve', {
                                    'document_root': settings.STATIC_ROOT,
                                }),
                        )


