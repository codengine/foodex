from blackwidow.core.models.email.email_template import EmailTemplate
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import save_audit_log, decorate, is_object_context
from config.enums.modules_enum import ModuleEnum

__author__ = 'ruddra'

@decorate(save_audit_log, is_object_context, route(route='alert-email-template', display_name='Alert email template', module=ModuleEnum.Administration, group='Other admin'))
class AlertEmailTemplate(EmailTemplate):

    class Meta:
        proxy = True