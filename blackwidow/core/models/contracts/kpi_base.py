import calendar
import collections
from datetime import datetime, timedelta
from decimal import Decimal
from django.db import models
from django.dispatch.dispatcher import receiver
from openpyxl.styles import Style
from openpyxl.styles.alignment import Alignment
from openpyxl.styles.protection import Protection
from blackwidow.core.mixins.modelmixin.export_model_mixin import ExportModelMixin
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig
from blackwidow.core.models.config.exporter_config import ExporterConfig
from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
from blackwidow.core.models.config.importer_config import ImporterConfig

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.engine.decorators.utility import get_models_with_decorator
from blackwidow.engine.extensions.clock import Clock
from config.apps import INSTALLED_APPS
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'activehigh'


class PerformanceIndex(ExportModelMixin,OrganizationDomainEntity):
    user = models.ForeignKey(ConsoleUser)
    start_time = models.BigIntegerField(default=0, null=True)
    end_time = models.BigIntegerField(default=0, null=True)
    name = models.CharField(max_length=500)
    target = models.DecimalField(max_digits=20, decimal_places=2,null=True)
    achieved = models.DecimalField(max_digits=20, decimal_places=2,null=True)
    last_calculated = models.BigIntegerField(null=True)

    @classmethod
    def get_datetime_fields(cls):
        return ['start_time', 'end_time'] + super().get_datetime_fields()

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create, ViewActionEnum.Edit, ViewActionEnum.Delete, ViewActionEnum.Export]

    @classmethod
    def get_queryset(cls, queryset=None, profile_filter=False, **kwargs):
        if profile_filter:
            _timestamp = Clock.timestamp()
            return queryset.filter(start_time__lte=_timestamp, end_time__gte=_timestamp)
        return queryset

    @classmethod
    def table_columns(cls):
        return 'code', 'user', 'start_time', 'end_time', 'target', 'achieved', 'is_active', 'last_updated'

    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        """
        :param organization: the reference organization
        :param kwargs:
        :return: returns the exporter configuration for this class
        """
        exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
        if not exporter_configs.exists():
            exporter_config = ExporterConfig()
            exporter_config.save(**kwargs)
        else:
            for e in exporter_configs:
                e.delete()
            exporter_config = ExporterConfig()
            exporter_config.save(**kwargs)
        '''
        We are keeping the columns blank for now
        TO DO: will be replaced with actual dynamic codes to load columns from database configuration
        '''
        columns = []
        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)
        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):
        """
        Called for each item, this function should write the rows directly into the work book
        :param workbook: the workbook to write to
        :param columns: columns returned from export_config
        :param row_number: the row index of the worksheet
        :return:
        """

        return 0, row_number + 1

    @classmethod
    def initialize_export(cls, workbook=None, columns=None,  row_count=None, **kwargs):
        return workbook, 1

    @classmethod
    def finalize_export(cls, workbook=None, row_count=None, **kwargs):
        return workbook

    @classmethod
    def calculate_kpi(cls, *args, **kwargs):
        pass

    @classmethod
    def map_month_name(cls,number):
        months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        if number >= 1 and number <= 12:
            return months[number - 1]
        return None

    class Meta:
        abstract = True

class MonthlyKPI(PerformanceIndex):
    year = models.IntegerField(default=2015)
    month = models.IntegerField(default=1)
    month_name = models.CharField(default="January", max_length=200)

    @classmethod
    def set_actual(cls, user_id, year, month, actual_value):
        kpi = cls.objects.filter(user_id=user_id, year=year, month=month)
        if kpi.exists():
            kpi = kpi.first()
            kpi.achieved = Decimal(str(actual_value))
            kpi.save()

    @classmethod
    def get_target(cls, user_id, year, month):
        kpi = cls.objects.filter(user_id=user_id, year=year, month=month)
        if kpi.exists():
            kpi = kpi.first()
            return kpi.target
        return None

    @classmethod
    def get_page_title(cls):
        return ""

    @classmethod
    def get_actual(cls, user_id, year, month):
        kpi = cls.objects.filter(user_id=user_id, year=year, month=month)
        if kpi.exists():
            kpi = kpi.first()
            return kpi.achieved
        return None

    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        ExporterConfig.objects.filter(model=cls.__name__, organization=organization).delete()
        exporter_config, created = ExporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)

        columns = [
            ExporterColumnConfig(column=0, column_name='ID', property_name='id', ignore=False),
            ExporterColumnConfig(column=1, column_name='Name', property_name='name', ignore=False),
        ]
        for i in range(0,48):
            columns += [ExporterColumnConfig(column=i + 2, column_name='value'+str(i), property_name='render_value_'+str(i), ignore=False)]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)

        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):
        # workbook.protection.sheet = True
        return self.pk, row_number + 1

    @classmethod
    def get_left_cols_fixed(cls):
        return []

    @classmethod
    def get_months(cls):
        return ( "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" )

    @classmethod
    def get_kpi_data_rows(cls, **kwargs):
        return []

    @classmethod
    def initialize_export(cls, workbook=None, columns=None,  query_set=None, **kwargs):
        # workbook.protection.sheet = True
        center_style = Style(alignment=Alignment(horizontal='center', vertical='center'))
        if len(columns) < 50:
            return workbook, 1

        row = 1
        workbook.cell(row=row, column=1).value = "ID"
        workbook.cell(row=row, column=2).value = "Name"
        start_column = 3
        end_column = 3
        niteration = 0
        for i in range(len(columns) - 2):
            if i % 4 == 0:
                start_column = i + 3
            if i % 4 == 3:
                end_column = i + 3
                niteration += 1
                workbook.cell(row=row, column=start_column).value = cls.map_month_name(niteration)
                workbook.cell(row=row, column=start_column).style = center_style
                workbook.merge_cells(start_row=row, start_column=start_column, end_row=row, end_column=end_column)


        now_dt = datetime.utcnow()

        current_year = now_dt.year
        previous_year = now_dt.year - 1
        row = 2
        for i in range(len(columns) - 2):
            if i % 2 == 0:
                start_column = i + 3
            if i % 2 == 1:
                end_column = i + 3
                if i % 4 == 1:
                    workbook.cell(row=row, column=start_column).value = str(previous_year)
                elif i % 4 == 3:
                    workbook.cell(row=row, column=start_column).value = str(current_year)
                workbook.cell(row=row, column=start_column).style = center_style
                workbook.merge_cells(start_row=row, start_column=start_column, end_row=row, end_column=end_column)

        row = 3
        for i in range(len(columns) - 2):
            if i % 2 == 0:
                workbook.cell(row=row, column= i + 3 ).value = "Target"
            elif i % 2 == 1:
                workbook.cell(row=row, column= i + 3 ).value = "Actual"

        workbook.cell(row=1, column=1).style = center_style
        workbook.merge_cells(start_row=1, start_column=1, end_row=3, end_column=1)
        workbook.cell(row=1, column=2).style = center_style
        workbook.merge_cells(start_row=1, start_column=2, end_row=3, end_column=2)

        tday = Clock.now()
        current_year = tday.year
        previous_year = current_year - 1

        current_month = tday.month
        row_number = 4
        ###Now get all users with enable_kpi decorator.
        kpi_enabled_models = get_models_with_decorator('enable_kpi', INSTALLED_APPS, include_class=True)
        for _model_class in kpi_enabled_models:
            all_users = _model_class.objects.all()

            user_ids = all_users.values_list('pk', flat=True)

            user_kpis = cls.fetch_user_kpis(user_ids, previous_year, current_year)

            def find_kpi_for_user(user_id, year, month, target_or_actual):
                try:
                    kpi_value = user_kpis[user_id][year][month][target_or_actual]
                    return kpi_value
                except:
                    pass

            for each_user in all_users:
                month_index = 1
                protection = Protection(hidden=True, locked=True)
                protected_style = Style(protection=protection)
                for index, column in enumerate(columns):
                    ccolumn = column.column + 1
                    should_protected = False
                    if month_index < current_month:
                        should_protected = True
                    cell_value = ''
                    if index == 0:
                        cell_value = each_user.pk
                        workbook.cell(row=row_number, column=ccolumn).value = cell_value
                        workbook.cell(row=row_number, column=ccolumn).style = protected_style
                    elif index == 1:
                        cell_value = each_user.name
                        workbook.cell(row=row_number, column=ccolumn).value = cell_value
                        workbook.cell(row=row_number, column=ccolumn).style = protected_style
                    else:
                        mindex = index - 2
                        if mindex % 4 == 0: ###Previous Year Target
                            cell_value = find_kpi_for_user(each_user.pk, previous_year, month_index, "target")
                            workbook.cell(row=row_number, column=ccolumn).value = cell_value
                            if should_protected:
                                workbook.cell(row=row_number, column=ccolumn).style = protected_style
                        elif mindex % 4 == 1: ###Previous Year Actual.
                            cell_value = find_kpi_for_user(each_user.pk, previous_year, month_index, "actual")
                            workbook.cell(row=row_number, column=ccolumn).value = cell_value
                            if should_protected:
                                workbook.cell(row=row_number, column=ccolumn).style = protected_style
                        elif mindex % 4 == 2: ###Current Year Target
                            #print(month_index)
                            if month_index <= current_month:
                                cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "target")
                                workbook.cell(row=row_number, column=ccolumn).value = cell_value
                                if should_protected:
                                    workbook.cell(row=row_number, column=ccolumn).style = protected_style
                            else:

                                cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "target")
                                workbook.cell(row=row_number, column=ccolumn).value = cell_value
                        elif mindex % 4 == 3: ###Current Year Actual
                            if month_index <= current_month:
                                cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "actual")
                                workbook.cell(row=row_number, column=ccolumn).value = cell_value
                                if should_protected:
                                    workbook.cell(row=row_number, column=ccolumn).style = protected_style
                            else:
                                cell_value = find_kpi_for_user(each_user.pk, current_year, month_index, "actual")
                                workbook.cell(row=row_number, column=ccolumn).value = cell_value

                            ###Index Month Index After Each Index.
                            month_index += 1
                row_number += 1

        return workbook, row_number

    @classmethod
    def finalize_export(cls, workbook=None, row_count=None, **kwargs):
        file_name = '%s_%s' % (cls.__name__, Clock.timestamp())
        return workbook, file_name

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        ImporterConfig.objects.filter(model=cls.__name__, organization=organization).delete()
        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        importer_config.starting_row = 3
        importer_config.starting_column = 0
        importer_config.save(**kwargs)
        columns = [
            ImporterColumnConfig(column=0, column_name='ID', property_name='id', ignore=False),
            ImporterColumnConfig(column=1, column_name='Name', property_name='name', ignore=False),
        ]
        for i in range(0,48):
                columns += [ImporterColumnConfig(column=i + 2, column_name='value'+str(i), property_name='render_value', ignore=False)]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        return data

    @classmethod
    def process_user_data(cls,user_id, user_name, current_month, previous_month, current_year, previous_year, item, user=None, organization=None, **kwargs):
        editable_from = previous_month * 4
        sorted_items = sorted(item.items(), key=lambda x: int(x[0]))
        start_day = 1
        month_index = 1
        for index, (key, value) in enumerate(sorted_items):
            weekday, end_day = calendar.monthrange(current_year, month_index)
            start_ts = Clock.get_timestamp_from_date(str(start_day)+"/"+str(month_index)+"/"+str(current_year)+" 00:00:00")
            end_ts = Clock.get_timestamp_from_date(str(end_day)+"/"+str(month_index)+"/"+str(current_year)+" 00:00:00")
            key = int(key)
            if index < editable_from - 1:

                if index % 4 == 3:
                    month_index += 1

                continue

            if index % 4 == 2:
                month_name = cls.map_month_name(month_index)
                try:
                    target_value = Decimal(value)
                    monthly_kpi = cls.objects.filter(year=current_year, month = month_index, user_id = user_id)
                    if monthly_kpi.exists():
                        monthly_kpi = monthly_kpi.first()
                    else:
                        monthly_kpi = cls()
                    monthly_kpi.year = current_year
                    monthly_kpi.month = month_index
                    monthly_kpi.month_name = month_name
                    monthly_kpi.user_id = user_id
                    monthly_kpi.name = "Monthly KPI for "+month_name
                    monthly_kpi.target = target_value
                    monthly_kpi.start_time = start_ts
                    monthly_kpi.end_time = end_ts
                    monthly_kpi.organization = organization
                    monthly_kpi.save()
                except Exception as exp:
                    print(str(exp))
                    pass
            if index % 4 == 3:
                month_index += 1


    @classmethod
    def post_processing_import_completed(cls,items=[], user=None, organization=None, **kwargs):
        super().post_processing_import_completed(items,user,organization,**kwargs)
        tday = Clock.now()
        current_year = tday.year
        previous_year = current_year - 1

        current_month = tday.month

        first_day_this_month = datetime.strptime("1/"+str(tday.month)+"/"+str(tday.year),"%d/%m/%Y")
        last_month_data = first_day_this_month - timedelta(days=1)

        previous_month = last_month_data.month

        for item in items:
            user_id = int(item['0'])
            user_name = item['1']
            data_item = item
            data_item.pop('0')
            data_item.pop('1')
            cls.process_user_data(user_id,user_name, current_month, previous_month, current_year, previous_year, data_item,user,organization,**kwargs)


    class Meta:
        abstract = True


class WeeklyKPI(PerformanceIndex):
    week = models.IntegerField(default=1)

    class Meta:
        abstract = True


class YearlyKPI(PerformanceIndex):
    year = models.IntegerField(default=2000)

    class Meta:
        abstract = True
