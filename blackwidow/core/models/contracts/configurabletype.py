from django.db import models

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity


__author__ = 'ruddra'


class ConfigurableType(OrganizationDomainEntity):
    context = models.CharField(max_length=255, null=True)
    key = models.CharField(max_length=255, null=True)
    name = models.CharField(max_length=8000, null=True)
    short_name = models.CharField(max_length=200, default='')
    short_code = models.CharField(max_length=200, default='')

    def clean(self):
        self.type = self.__class__.__name__

    @classmethod
    def table_columns(cls):
        return "code", "name", "short_name", "last_updated"

    def load_initial_data(self, **kwargs):
        super().load_initial_data(**kwargs)
        self.context = ''
        self.key = ''

    def __str__(self):
        if self.short_name:
            return self.short_name + " ( " + self.name + " ) "
        return self.name

    class Meta:
        abstract = True
