import traceback

from django.db import models

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.organizations.organization import Organization
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, is_object_context, direct_delete
from config.constants.access_permissions import BW_ACCESS_NO_ACCESS, BW_ACCESS_READ_ONLY
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'Mahmud'


class SystemLog(OrganizationDomainEntity):
    message = models.CharField(max_length=500)

    # @classmethod
    # def get_inline_manage_buttons_decision(cls):
    #     return BW_ACCESS_READ_ONLY

    @classmethod
    def get_object_inline_buttons(cls):
        return [ ViewActionEnum.Details, ViewActionEnum.Delete ]

    @classmethod
    def get_routes(cls, **kwargs):
        return [ViewActionEnum.Details, ViewActionEnum.Edit, ViewActionEnum.Delete, ViewActionEnum.Manage]

    @classmethod
    def table_columns(cls):
        return 'code', 'message', 'date_created'

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Delete]

    @property
    def get_inline_manage_buttons(self):
        return [dict(
            name='Details',
            action='view',
            title="Click to view this item",
            icon='icon-eye',
            ajax='0',
            url_name=self.__class__.get_route_name(action=ViewActionEnum.Details),
            classes='all-action ',
            parent=None
        )]

    @classmethod
    def default_order_by(cls):
        return '-date_created'


    def details_link_config(self, **kwargs):
        return []

    @classmethod
    def log(cls, exp, *args, **kwargs):
        log = cls()
        log.message = str(exp)
        log.organization = kwargs.get('organization', Organization.objects.filter(is_master=True)).first()
        log.error_code = ""
        log.stacktrace = "<hr/>".join(traceback.format_exc().splitlines()) if kwargs.get("stacktrace") is None else kwargs.get("stacktrace")
        log.save()

    class Meta:
        abstract = True


@decorate(is_object_context,direct_delete,route(route='system-logs', group='Logs', display_name='Error Log', module=ModuleEnum.Administration))
class ErrorLog(SystemLog):
    error_code = models.CharField(max_length=200, default='')
    stacktrace = models.CharField(max_length=8000)

    @classmethod
    def table_columns(cls):
        return 'code', 'message','created_by', 'date_created'

    @property
    def details_config(self):
        dic = super().details_config
        dic['Created By'] = self.created_by if self.created_by else "N/A"
        return dic


@decorate(is_object_context,route(route='api-logs', group='Logs', display_name='Api Log', module=ModuleEnum.Administration))
class ApiCallLog(OrganizationDomainEntity):
    url = models.URLField()
    request_type = models.CharField(max_length=500)
    token = models.CharField(max_length=200)
    request_data = models.CharField(max_length=8000, default='')
    response_data = models.CharField(max_length=8000, default='')

    @classmethod
    def table_columns(cls):
        return 'code', 'url', 'request_type', 'token', 'created_by', 'last_updated'

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Delete]

    @property
    def get_inline_manage_buttons(self):
        return [dict(
            name='Details',
            action='view',
            title="Click to view this item",
            icon='icon-eye',
            ajax='0',
            url_name=self.__class__.get_route_name(action=ViewActionEnum.Details),
            classes='all-action ',
            parent=None
        )]

    def details_link_config(self, **kwargs):
        return []

    @classmethod
    def default_order_by(cls):
        return "-date_created"

    # @classmethod
    # def get_routes(cls, **kwargs):
    #     return [ViewActionEnum.Details, ViewActionEnum.Delete, ViewActionEnum.Manage]

    @classmethod
    def get_object_inline_buttons(cls):
        return [ ViewActionEnum.Details, ViewActionEnum.Delete, ViewActionEnum.Manage ]

    @classmethod
    def log(cls, *args, url='', type='', token='', request=None, response=None, **kwargs):
        log = cls()
        log.request_type = type if type != '' else request._method
        log.url = url if url != '' else request.META['PATH_INFO']
        log.organization = kwargs.get('organization', Organization.objects.get(is_master=True))
        log.token = token
        log.request_data = request.DATA
        if response is not None:
            log.response_data = str(response.status_code) + ' ' + response.status_text
        log.save()

@decorate(is_object_context,route(route='scheduler-logs', group='Logs', display_name='Scheduler Log', module=ModuleEnum.Administration))
class SchedulerLog(SystemLog):
    status = models.CharField(max_length=200, default='')

    @classmethod
    def table_columns(cls):
        return 'code', 'status', 'message', 'date_created'


@decorate(is_object_context,route(route='email-logs', group='Logs', display_name='Email Log', module=ModuleEnum.Administration))
class EmailLog(SystemLog):
    from blackwidow.core.models.users.user import ConsoleUser
    
    status = models.CharField(max_length=200, default='')
    recipient_user = models.ForeignKey(ConsoleUser, null=True, default=None)

    @classmethod
    def table_columns(cls):
        return 'code', 'status', 'message', 'date_created', 'recipient_user:Recipient'

