from enum import Enum
from django.db import models

from blackwidow.core.models.contracts.base import DomainEntity
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, is_object_context
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum

class DeletedEntityEnum(Enum):
    SoftDeleted = 0
    Restored = 1
    HardDeleted = 2
    def __str__(self):
        return self.value



class AuditLog(OrganizationDomainEntity):
    model_name = models.CharField(max_length=200)
    model_pk = models.BigIntegerField(default=0)

    @classmethod
    def log(cls, model=DomainEntity):
        try:
            _log = cls()
            _log.model_name = model.get_delete_log_model()
            _log.model_pk = model.pk
            _log.save()
        except:
            pass


@decorate(is_object_context, route(route='delete-logs', group='Audit Logs', display_name='Delete Log', module=ModuleEnum.Administration))
class DeleteLog(AuditLog):
    deleted_status = models.IntegerField(default=DeletedEntityEnum.SoftDeleted.value)
    display_name = models.CharField(max_length=128, default='Unspecified')

    @classmethod
    def get_object_inline_buttons(cls):
        return [ ViewActionEnum.Details, ViewActionEnum.Delete ]

    @classmethod
    def table_columns(cls):
        return 'code', 'model_pk', 'model_name', 'display_name', 'created_by', 'last_updated'

    @property
    def get_inline_manage_buttons(self):
        inline_buttons = [dict(
            name='Details',
            action='view',
            title="Click to view this item",
            icon='icon-eye',
            ajax='0',
            url_name=self.__class__.get_route_name(action=ViewActionEnum.Details),
            classes='all-action ',
            parent=None
        )]
        if self.deleted_status == DeletedEntityEnum.SoftDeleted.value:
            inline_buttons.append(dict(
                name='Restore',
                action='restore',
                title="Click to restore this item",
                icon='icon-redo icon-safe',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Restore),
                classes='all-action confirm-action',
                parent=None
            ))
            inline_buttons.append(dict(
                name='Delete',
                action='delete',
                title="Click to delete this item permanently",
                icon='icon-remove icon-danger',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Delete),
                classes='all-action confirm-action',
                parent=None,
                params='hard_delete=1'
            ))
        return inline_buttons

    @classmethod
    def log(cls, model=DomainEntity, name='Unspecified'):
        try:
            _log = cls()
            _log.model_name = model.get_delete_log_model()
            _log.model_pk = model.pk
            _log.display_name = name
            _log.save()
        except:
            pass

    @classmethod
    def get_routes(cls, **kwargs):
        return [ViewActionEnum.Details, ViewActionEnum.Delete, ViewActionEnum.Manage,ViewActionEnum.Restore]


@decorate(route(route='restore-logs', group='Audit Logs', display_name='Restore Log', module=ModuleEnum.Administration))
class RestoreLog(AuditLog):
    display_name = models.CharField(max_length=128, default='Unspecified')

    @classmethod
    def table_columns(cls):
        return 'code', 'model_pk', 'model_name', 'display_name', 'created_by', 'last_updated'

    @property
    def get_inline_manage_buttons(self):
        return [dict(
            name='Details',
            action='view',
            title="Click to view this item",
            icon='icon-eye',
            ajax='0',
            url_name=self.__class__.get_route_name(action=ViewActionEnum.Details),
            classes='all-action ',
            parent=None
        )]

    @classmethod
    def log(cls, model=DomainEntity, name='Unspecified'):
        try:
            _log = cls()
            _log.model_name = model.get_delete_log_model()
            _log.model_pk = model.pk
            _log.display_name = name
            _log.save()
        except:
            pass

    @classmethod
    def get_routes(cls, **kwargs):
        return [ViewActionEnum.Details, ViewActionEnum.Delete, ViewActionEnum.Manage]


class UpdateLog(AuditLog):

    class Meta:
        proxy = True


class CreateLog(AuditLog):

    class Meta:
        proxy = True

