from blackwidow.engine.decorators.utility import decorate, is_object_context
from blackwidow.engine.decorators.route_partial_routes import partial_route

__author__ = 'ruddra'
from django.db import models

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity

class ExporterColumnConfig(OrganizationDomainEntity):
    column = models.IntegerField(default=0)
    column_name = models.CharField(default='', max_length=200)
    property_name = models.CharField(default='', max_length=500)
    ignore = models.BooleanField(default=0)
    children = models.ManyToManyField('self', symmetrical=False)

    @classmethod
    def get_default_ignored_columns(cls):
        return ['is_locked', 'last_updated', 'last_updated_by', 'last_updated_by_id', 'organization', 'organization_id',
                'reference_id', 'tsync_id', 'type', 'created_by', 'date_created', 'creation_flag', 'deleted_level', 'id',
                'is_active', 'is_deleted', 'created_by_id'
        ]

    @classmethod
    def table_columns(cls):
        return 'column_name', 'property_name'
