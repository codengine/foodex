from django.db import models
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
from blackwidow.core.viewmodels.tabs_config import TabView, TabViewAction
from blackwidow.engine.decorators.route_partial_routes import route, partial_route
from blackwidow.engine.decorators.utility import decorate, is_object_context
from blackwidow.engine.extensions.bw_titleize import bw_titleize
from blackwidow.engine.extensions.model_descriptor import get_model_by_name
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum
from django.utils.safestring import mark_safe
from functools import reduce
from django.db.models.query_utils import Q
from django.core.urlresolvers import reverse
import operator


__author__ = 'Mahmud'


@decorate(is_object_context,route(route='create-importer', group='Import/Export', module=ModuleEnum.Administration, display_name="Create New Importer"),
          partial_route(relation=['normal'], models=[ImporterColumnConfig]))
class ImporterConfig(OrganizationDomainEntity):
    columns = models.ManyToManyField(ImporterColumnConfig)
    model = models.CharField(max_length=200)
    starting_row = models.IntegerField(default=1)       # 1 indexed system
    starting_column = models.IntegerField(default=0)    # 0 indexed system

    @property
    def render_code(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.pk}) + "' >" + self.code + "</a>")

    @property
    def render_columns(self):
        columns = self.columns.all()
        value = ''
        for column in columns:
            value += column.column_name if len(value)==0 else ', ' + column.column_name
        return value


    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create, ViewActionEnum.Edit, ViewActionEnum.Delete, ViewActionEnum.Manage]

    @classmethod
    def get_dependent_field_list(cls):
        return ['columns']

    @classmethod
    def table_columns(cls):
        return 'render_code', 'model', 'render_columns', 'starting_row', 'starting_column'

    @property
    def tabs_config(self):
        for_model = get_model_by_name(model_name=self.model)
        all_fields = for_model._meta.get_all_field_names()
        ignored_fields = ImporterColumnConfig.get_default_ignored_columns()

        fields = list()
        column_index = 0
        for f in all_fields:
            if f not in ignored_fields:
                fields.append((f, column_index))
                column_index+=1
        for field in fields:
            ImporterColumnConfig.objects.update_or_create(column=field[1], column_name=bw_titleize(field[0]), property_name=field[0])
        properties = list()
        all_properties = [name for name in dir(for_model) if isinstance(getattr(for_model, name), property)]
        for f in all_properties:
            properties.append((f, column_index))
            column_index+=1
        for p in properties:
            ImporterColumnConfig.objects.update_or_create(column=p[1], column_name=bw_titleize(p[0]), property_name=p[0])


        field_query = reduce(operator.or_, (Q(column=c, property_name=p) for p,c in fields))
        field_query = ImporterColumnConfig.objects.filter(field_query).exclude(pk__in=self.columns.values_list('id',flat=True))
        property_query = reduce(operator.or_, (Q(column=c, property_name=p) for p,c in properties))
        property_query = ImporterColumnConfig.objects.filter(property_query).exclude(pk__in=self.columns.values_list('id',flat=True))
        tabs = [TabView(
            title='Import Column (s)',
            access_key='columns',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            add_more_queryset=field_query,#exclude(pk__in=c.users.values('id')),
            related_model=ImporterColumnConfig,
            property=self.columns,
            actions=[
                TabViewAction(
                    title='Add',
                    action='add',
                    icon='icon-plus',
                    route_name=ImporterColumnConfig.get_route_name(action=ViewActionEnum.PartialBulkAdd, parent=self.__class__.__name__.lower()),
                    css_class='manage-action load-modal fis-plus-ico'
                ),
                TabViewAction(
                    title='Remove',
                    action='partial-remove',
                    icon='icon-remove',
                    route_name=ImporterColumnConfig.get_route_name(action=ViewActionEnum.PartialBulkRemove, parent=self.__class__.__name__.lower()),
                    css_class='manage-action delete-item fis-remove-ico'
                )
            ]
        ),TabView(
            title='Import Property (s)',
            access_key='properties',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            add_more_queryset=property_query,#exclude(pk__in=c.users.values('id')),
            related_model=ImporterColumnConfig,
            property=self.columns,
            actions=[
                TabViewAction(
                    title='Add',
                    action='add',
                    icon='icon-plus',
                    route_name=ImporterColumnConfig.get_route_name(action=ViewActionEnum.PartialBulkAdd, parent=self.__class__.__name__.lower()),
                    css_class='manage-action load-modal fis-plus-ico'
                ),
                TabViewAction(
                    title='Remove',
                    action='partial-remove',
                    icon='icon-remove',
                    route_name=ImporterColumnConfig.get_route_name(action=ViewActionEnum.PartialBulkRemove, parent=self.__class__.__name__.lower()),
                    css_class='manage-action delete-item fis-remove-ico'
                )
            ]
        )]

        return tabs
