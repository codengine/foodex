from django.db import models

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity


__author__ = 'Mahmud'


class ImporterColumnConfig(OrganizationDomainEntity):
    column = models.IntegerField(default=0)
    column_name = models.CharField(default='', max_length=200)
    property_name = models.CharField(default='', max_length=500)
    ignore = models.BooleanField(default=0)
    is_unique = models.NullBooleanField(default=False, null=True)
    children = models.ManyToManyField('self', symmetrical=False)

    @classmethod
    def get_default_ignored_columns(cls):
        return ['is_locked', 'last_updated', 'last_updated_by', 'last_updated_by_id', 'organization', 'organization_id',
                'reference_id', 'tsync_id', 'type', 'created_by', 'date_created', 'creation_flag', 'deleted_level', 'id',
                'is_active', 'is_deleted', 'created_by_id'
        ]

    @classmethod
    def table_columns(cls):
        return 'column_name', 'property_name'

