from django.db import models
from rest_framework import serializers

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from config.enums.modules_enum import ModuleEnum


__author__ = 'activehigh'


@decorate(save_audit_log, is_object_context, expose_api('other-activities'))
class OtherActivity(OrganizationDomainEntity):
    subject = models.CharField(max_length=200, default='')
    description = models.CharField(max_length=500, default='')

    @classmethod
    def table_columns(cls):
        return 'code', 'subject', 'description'

    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class ODESerializer(ss):
            name = serializers.CharField(required=False)

            class Meta:
                model = cls
                read_only_fields = ss.Meta.read_only_fields

        return ODESerializer