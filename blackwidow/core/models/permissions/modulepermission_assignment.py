from django.utils.safestring import mark_safe

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.modules.module import BWModule
from blackwidow.core.models.roles.role import Role
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'mahmudul'

from django.db import models


class ModulePermissionAssignment(OrganizationDomainEntity):
    module = models.ForeignKey(BWModule)
    role = models.ForeignKey(Role)
    access = models.IntegerField(default=0)
    visibility = models.IntegerField(default=0)

    @classmethod
    def default_order_by(cls):
        return 'module'

    @property
    def get_inline_manage_buttons(self, **kwargs):
        return [
            dict(
                name='Edit',
                action='edit',
                icon='icon-pencil',
                ajax='1',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.PartialEdit,
                                                       parent=self.role_set.model.__name__.lower()),
                classes='manage-action load-modal',
                parent=self.role_set.all()[0]
            )
        ]

    def get_simplified_dict(self):
        contexts = self.module.name
        return {
            'access': self.access,
            'visibility': self.visibility,
            'app': contexts[0] if len(contexts) > 1 else '',
            'name': contexts[1] if len(contexts) > 1 else contexts[0]
        }

    def render_allow(self):
        return mark_safe('<input type="radio" class="inline-edit-input" name="' + self.module.name + '" value="' + str(1) + '" ' + (' checked="checked"' if self.access == 1 else '') + ' ></input>')

    def render_deny(self):
        return mark_safe('<input type="radio" class="inline-edit-input" name="' + self.module.name + '" value="' + str(0) + '" ' + (' checked="checked"' if self.access == 0 else '') + ' ></input>')

    @classmethod
    def table_columns(cls):
        return 'module', 'render_allow', 'render_deny'

    class Meta:
        app_label = 'core'