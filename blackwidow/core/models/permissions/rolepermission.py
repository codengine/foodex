from django.contrib.contenttypes.models import ContentType
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from django.apps import apps
from blackwidow.core.models.modules.module import BWModule

__author__ = 'mahmudul'

from django.db import models

class RolePermission(OrganizationDomainEntity):
    context = models.CharField(max_length=200)
    display_name = models.CharField(max_length=500, default='')

    @classmethod
    def default_order_by(cls):
        return 'display_name'

    def __str__(self):
        model_object = ContentType.objects.filter(model=self.context.lower()).first()
        model = apps.get_model(model_object.app_label, self.context)
        group = model.get_model_meta('route', 'group')
        if group is not None:
            module = BWModule.objects.filter(name=group).first()
            if module.parent is not None:
                return module.parent.name + ' -> ' + module.name + ' -> ' + self.display_name
            return group + ' -> ' + self.display_name
        return self.display_name
