from django.db.models.query_utils import Q
from blackwidow.core.models.contracts.base import DomainEntity
from blackwidow.core.viewmodels.tabs_config import TabView
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'mahmudul'

from django.db import models


# @decorate(save_audit_log, route(route='organizations', group='Organizations', module=ModuleEnum.Administration, display_name='Organization'))
class Organization(DomainEntity):
    name = models.CharField(max_length=255, unique=False)

    is_master = models.BooleanField(default=0)
    is_test = models.BooleanField(default=0)

    addresses = models.ManyToManyField('core.ContactAddress', null=True, related_name="addresses")
    emails = models.ManyToManyField('core.EmailAddress', null=True, related_name="emails")
    phones = models.ManyToManyField('core.PhoneNumber', null=True, related_name="phones")

    trade_license_number = models.CharField(max_length=200, default='')
    registration_date = models.BigIntegerField(null=True)
    date_joined = models.BigIntegerField(null=True)

    # @property
    # def details_config(self):
    #     data = super().details_config
    #     data['email'] = self.emails.first()
    #     data['phone'] = self.phones.first()
    #     data['address'] = self.addresses.first()
    #     return data
    @classmethod
    def get_datetime_fields(cls):
        return ['date_created', 'last_updated', 'date_joined', 'registration_date']

    @classmethod
    def get_serializer(cls):
        ss = DomainEntity.get_serializer()

        class Serializer(ss):
            class Meta(ss.Meta):
                model = cls

        return Serializer

    @classmethod
    def get_dependent_field_list(cls):
        return ['addresses', 'emails', 'phones']

    def get_choice_name(self):
        return self.name

    @property
    def tabs_config(self):
        from blackwidow.core.models.common.contactaddress import ContactAddress
        from blackwidow.core.models.common.emailaddress import EmailAddress
        from blackwidow.core.models.common.phonenumber import PhoneNumber

        tabs = [TabView(
            title='Addresses',
            access_key='addresses',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            related_model=ContactAddress,
            property=self.addresses
        ), TabView(
            title='Emails',
            access_key='emails',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            related_model=EmailAddress,
            property=self.emails
        ), TabView(
            title='Phones',
            access_key='phones',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            related_model=PhoneNumber,
            property=self.phones
        )]
        return tabs

    class Meta:
        app_label = 'core'
