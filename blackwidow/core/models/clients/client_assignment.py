from django.db import models

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity


__author__ = 'Mahmud'

from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.enable_m2m_tracking import enable_m2m_tracking

#@decorate(enable_m2m_tracking('clients'))
class ClientAssignment(OrganizationDomainEntity):
    clients = models.ManyToManyField('core.Client')
    client_type = models.CharField(max_length=200, default='')

