from django.utils.safestring import mark_safe
from django.db import models
from rest_framework import serializers

from blackwidow.core.models.file.imagefileobject import ImageFileObject
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity


__author__ = 'ruddra'

_CHOICES_HF = (
    ('H', 'Husband'),
    ('F', 'Father'),
)

_CHOICES_MF = (
    ('M', 'Male'),
    ('F', 'Female'),
)

class ClientMeta(OrganizationDomainEntity):
    client_male_or_female = models.CharField(max_length=2, choices=_CHOICES_MF, null=True, default='M')
    guardian_name = models.CharField(max_length=255, verbose_name="Husband or Father's Name", null= True, default=None)
    guardian_choice_type = models.CharField(max_length=2, choices=_CHOICES_HF, verbose_name='Husband or Father', null=True, default='H')
    selling_area = models.CharField(max_length=255, null= True, default=None, verbose_name='Selling area or selling village')
    date_of_birth = models.CharField(max_length=200, null=True)
    national_id = models.CharField(max_length=200, null=True)
    image = models.ForeignKey(ImageFileObject, null=True, default=None)

    def __str__(self):
        return mark_safe("<b>Husband or Father's name: </b>"+self.guardian_name if self.guardian_name is not None else ''+"<br/>"
                         + '<b>Husband or Father:</b> '+self.guardian_choice_type if self.guardian_name is not None else ''+'<br/>'
                         + '<b>Date of Birth:</b> '+self.date_of_birth if self.guardian_name is not None else ''+'<br/>'
                         + '<b>National Id:</b> '+self.national_id if self.guardian_name is not None else ''+'<br/>'
                         + "<b>Selling Area or Village:</b> "+self.selling_area if self.guardian_name is not None else ''+'<br/>')

    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class Serializer(ss):
            image = serializers.PrimaryKeyRelatedField(required=False, queryset=ImageFileObject.objects.all())

            class Meta(ss.Meta):
                model = cls

        return Serializer