from rest_framework import serializers
from django.db import models

from blackwidow.core.models.common.contactaddress import ContactAddress
from blackwidow.core.models.common.custom_field import CustomFieldValue
from blackwidow.core.models.common.phonenumber import PhoneNumber
from blackwidow.core.models.common.qr_code import QRCode
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.core.models.manufacturers.manufacturer import Manufacturer
from blackwidow.core.models.finanical.financial_information import FinancialInformation
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.structure.user_assignment import InfrastructureUserAssignment
from blackwidow.core.models.users.user import ConsoleUser, WebUser
from blackwidow.core.models.contracts.base import RequestSource
from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.route_partial_routes import route
from config.enums.modules_enum import ModuleEnum

__author__ = 'Mahmud'


@decorate(route(route='clients', module=ModuleEnum.Administration, display_name='Client', group='Customers', hide=True))
class Client(OrganizationDomainEntity):
    name = models.CharField(max_length=200)
    company_name = models.CharField(max_length=200, null=True)
    manufacturer = models.ForeignKey(Manufacturer, null=True)
    address = models.ForeignKey(ContactAddress, null=True)
    phone_number = models.ForeignKey(PhoneNumber, null=True)
    parent = models.ForeignKey('self', null=True, default=None)
    group = models.CharField(max_length=255, null=True, default='')
    assigned_employee = models.ManyToManyField(InfrastructureUserAssignment)
    assigned_to = models.ForeignKey(InfrastructureUnit, null=True)
    contact_person = models.ForeignKey(WebUser, null=True)
    financial_information = models.ForeignKey(FinancialInformation, null=True, default=None)
    remark = models.CharField(max_length=500, default='')
    qr_code = models.ForeignKey(QRCode, null=True)
    pre_registered = models.BooleanField(default=0)
    is_approved = models.BooleanField(default=False)
    approved_by = models.ForeignKey(ConsoleUser,null=True,blank=True)
    approved_time = models.BigIntegerField(default=0,null=True,blank=True)
    request_source = models.CharField(max_length=20,default=RequestSource.WebBrowser.value,null=True)
    custom_fields = models.ManyToManyField(CustomFieldValue)
    credit_limit = models.DecimalField(decimal_places=2, max_digits=20, default=0)
    available_credit = models.DecimalField(decimal_places=2, max_digits=20, default=0)


    def delete(self, *args, **kwargs):
        if self.qr_code:
            self.qr_code.is_used = False
            self.qr_code.save()
        super().delete(*args, **kwargs)

    @classmethod
    def get_trigger_properties(cls, prefix='', expand=['address']):
        return super().get_trigger_properties(prefix=prefix, expand=expand)

    @property
    def details_config(self):
        details_config = super().details_config

        for x in self.custom_fields.all():
            details_config[x.field.name] = x.value

        return details_config

    def __str__(self):
        return self.code + ": " + self.name

    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class Serializer(ss):
            financial_information = FinancialInformation.get_serializer()(required=False)
            assigned_employee = InfrastructureUserAssignment.get_serializer()(required=False, many=True)
            address = ContactAddress.get_serializer()(required=False)
            phone_number = PhoneNumber.get_serializer()(required=False)
            assigned_to = serializers.PrimaryKeyRelatedField(required=False, queryset=InfrastructureUnit.objects.all())
            parent = serializers.PrimaryKeyRelatedField(required=False, queryset=Client.objects.all())
            remark = serializers.CharField(max_length=500, required=False)
            qr_code = QRCode.get_serializer()(required=False)
            pre_registered = serializers.BooleanField(required=False, default=0)
            custom_fields = CustomFieldValue.get_serializer()(many=True, required=False)

            class Meta(ss.Meta):
                model = cls
        return Serializer

    def load_initial_data(self, **kwargs):
        super().load_initial_data(**kwargs)
        self.name = "Client " + str(kwargs['index'])

        self.save()

    @classmethod
    def get_dependent_field_list(cls):
            return ['address', 'financial_information', 'custom_fields']

    def save(self, *args, signal=True, **kwargs):
        if self.qr_code is None:
            self.pre_registered = True
        else:
            self.pre_registered = False
            self.qr_code.is_used = True
            self.qr_code.save()

        super().save(*args, **kwargs)

        # if signal:
        #     sig_client_added.send(sender=self.__class__, client=self, user=self.created_by, organization=self.organization)

    def get_choice_name(self):
        return self.code + ': ' + self.name
