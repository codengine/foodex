from django.contrib.auth.models import User
from django.db import transaction

from blackwidow.core.models.common.contactaddress import ContactAddress
from blackwidow.core.models.common.phonenumber import PhoneNumber
from blackwidow.core.models.contracts.base import CreationFlag
from blackwidow.core.models.roles.role import Role
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.stores.store import Store
from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
from blackwidow.core.models.config.importer_config import ImporterConfig
from blackwidow.core.models.users.employee import Employee
from blackwidow.core.viewmodels.tabs_config import TabView
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'Mahmud'

# @decorate(enable_import, is_object_context, expose_api('dealers'), route(route='dealers', group='Client/Suppliers', module=ModuleEnum.Administration, display_name="Dealer"),
#           partial_route(relation='normal', models=[Client]))
class Dealer(Client):
    @classmethod
    def prefix(cls):
        return 'DL'

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if result or importer_config.columns.count() == 0:
            importer_config.save(**kwargs)
            columns = [
                ImporterColumnConfig(column=0, column_name='Dealer Id', property_name='reference_id', ignore=False),
                ImporterColumnConfig(column=1, column_name='Dealer Name', property_name='name', ignore=False),
                ImporterColumnConfig(column=2, column_name='Address', property_name='addresses:street', ignore=False),
                ImporterColumnConfig(column=3, column_name='Mobile No', property_name='phone_numbers:phone', ignore=False),
                ImporterColumnConfig(column=4, column_name='Dealer Group', property_name='group', ignore=False),
                ImporterColumnConfig(column=5, column_name='Sales Person ID', property_name='sales_person', ignore=False),
                ]
            for c in columns:
                c.save(organization=organization, **kwargs)
                importer_config.columns.add(c)
        return importer_config

    @property
    def tabs_config(self):
        tabs = [
            # dict(
            #     name='Retailers',
            #     id='reatielrs',
            #     load_ajax='1',
            #     url_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            #     relation='inverted',
            #     model_name=Client,
            #     property='parent'
            # ),
            TabView(title='Retailers',
                    access_key='retailers',
                    enable_ajax=True,
                    relation_type=ModelRelationType.INVERTED,
                    queryset=Client.objects.filter(parent=self),
                    queryset_filter=None)
        ]
        return tabs

    @classmethod
    def get_serializer(cls):
        ss = Client.get_serializer()

        class Serializer(ss):
            class Meta(ss.Meta):
                model = cls
        return Serializer

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        with transaction.atomic():
            org = user.organization
            dealers= Dealer.objects.filter(reference_id= data['0'])
            if dealers.exists():
                dealer = dealers[0]
            else:
                dealer= Dealer()
            dealer.organization=org
            dealer.reference_id= data['0']
            dealer.name= data['1']
            dealer.group= data['4']
            dealer.creation_flag = CreationFlag.Imported.value
            assigned_employee = Employee.objects.filter(reference_id=data['5'])
            if assigned_employee.exists():
                dealer.assigned_employee = assigned_employee[0]
            else:
                employee = Employee()
                employee.organization = org
                employee.role = Role.objects.filter(name__iexact='territory officer')[0]
                employee.code = data['5']
                employee.name = data['5']
                employee.reference_id = data['5']
                employee.save()
                dealer.assigned_employee = employee
            if dealer.assigned_employee.user is None:
                _obj = dealer.assigned_employee
                _user = User()
                _user.username = dealer.assigned_employee.code
                _user.save()
                _user.set_password(dealer.assigned_employee.code)
                _user.save()
                _obj.user = _user
                _obj.save()
                dealer.assigned_employee = _obj
            dealer.save()
            if not dealer.stores.exists():
                new_address = ContactAddress()
                store = Store()
            else:
                new_address = dealer.stores.all()[0].address
                store = dealer.stores.all()[0]

            new_address.organization= org
            address_data=str(data['2'])
            new_address.street= address_data
            new_address.city= ''
            new_address.province= ''
            new_address.is_primary= True
            new_address.save()
            if len(str(data['3']))>4:
                ph_number= PhoneNumber()
                ph_number.organization=org
                ph_number.phone= str(data['3']).strip('.0')
                ph_number.save()
                store.phone_number = ph_number
            store.address = new_address
            store.organization = org
            store.name = data['1']
            store.save()

            if not dealer.stores.exists():
                dealer.stores.add(store)
            return dealer.pk

    class Meta:
        proxy = True