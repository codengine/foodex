from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, loads_initial_data, is_object_context
from config.enums.modules_enum import ModuleEnum

# @decorate(is_object_context,expose_api('warehouses'),
#           route(route='warehouses', module=ModuleEnum.Administration, group='Other Admin', display_name='Warehouse'))
class WareHouse(InfrastructureUnit):
    class Meta:
        proxy = True