from django.db.models.query_utils import Q

from blackwidow.core.models.information.information_object import InformationObject
from blackwidow.core.models.roles.role import Role
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route, partial_route, is_profile_content
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from config.enums.modules_enum import ModuleEnum


__author__ = 'Mahmud'

@decorate(is_profile_content, save_audit_log, is_object_context, expose_api('news'),
          route(route='news', display_name='News', group='News', module=ModuleEnum.Alert, hide=True),
          partial_route(relation='normal', models=[Role, ConsoleUser]))
class News(InformationObject):

    @classmethod
    def get_queryset(cls, queryset=None, user=None, profile_filter=False, **kwargs):
        if profile_filter:
            _query = Q(is_active=True, recipient_roles__in=[user.role.pk]) | Q(is_active=True, recipient_users__in=[user.pk])
            return queryset.filter(_query)
        return queryset

    class Meta:
        proxy = False