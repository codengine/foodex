from django.db.models.query_utils import Q

from blackwidow.core.models.information.information_object import InformationObject
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'Mahmud'


# @decorate(is_profile_content, is_object_context, expose_api('notifications'),
#           route(route='notifications', display_name='Notification', group='Alert', module=ModuleEnum.Alert),
#           partial_route(relation='normal', models=[Role, ConsoleUser]))
class Notification(InformationObject):

    @classmethod
    def get_queryset(cls, queryset=None, user=None, profile_filter=False, **kwargs):
        if profile_filter:
            _query = Q(is_active=True, recipient_roles__in=[user.role.pk]) | Q(is_active=True, recipient_users__in=[user.pk])
            return queryset.filter(_query)
        return queryset

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Delete]

    class Meta:
        proxy = False