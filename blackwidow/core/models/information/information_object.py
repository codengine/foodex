from django.db import models
from django.db.models.query_utils import Q

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.roles.role import Role
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.extensions.clock import Clock
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'Mahmud'


class InformationObject(OrganizationDomainEntity):
    name = models.CharField(max_length=500)
    recipient_roles = models.ManyToManyField(Role)
    recipient_users = models.ManyToManyField(ConsoleUser)
    start_time = models.BigIntegerField(null=True)
    end_time = models.BigIntegerField(null=True)
    details = models.TextField(max_length=8000)
    notify_by_email = models.BooleanField(default=0)


    @classmethod
    def get_datetime_fields(cls):
        return ['date_created', 'last_updated', 'start_time', 'end_time']

    @classmethod
    def get_dependent_field_list(cls):
        return []

    def process(self):
        # c_date = Clock.now()
        if self.start_time is None:
            pass
        elif self.end_time is None or (self.start_time <= Clock.utcnow() <= self.end_time):
            pass

    @classmethod
    def table_columns(cls):
        return 'code', 'name', 'details:description', 'start_time', 'end_time'

    @property
    def tabs_config(self):
        return [
            TabView(
                title='Recipient Role(s)',
                access_key='roles',
                route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
                relation_type=ModelRelationType.NORMAL,
                related_model=Role,
                property=self.recipient_roles,
                queryset_filter=Q(**{'role__id': self.pk})
            ),
            TabView(
                title='Recipient User(s)',
                access_key='users',
                route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
                relation_type=ModelRelationType.NORMAL,
                related_model=ConsoleUser,
                property=self.recipient_users,
                queryset_filter=None
            )
        ]

    class Meta:
        abstract = True