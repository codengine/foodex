from blackwidow.core.models.contracts.base import DomainEntity
from blackwidow.engine.decorators.enable_trigger import enable_trigger
from blackwidow.engine.decorators.utility import decorate

__author__ = 'Mahmud'

from django.db import models

@decorate(enable_trigger)
class PhoneNumber(DomainEntity):
    phone = models.CharField(max_length=100, null=True, default=None)
    is_primary = models.BooleanField(default=0)
    is_own = models.BooleanField(default=1)

    def load_initial_data(self, **kwargs):
        super().load_initial_data(**kwargs)
        self.phone = "0123456789"

    def __str__(self):
        return self.phone


