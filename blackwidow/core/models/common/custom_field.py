from django.db import models, transaction
from rest_framework import serializers

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'Mahmud'

_field_choices=(
    ('char', 'Character Field'),
    ('datetime', 'Date Time Field'),
    ('int', 'Integer Field'),
    ('big_int', 'BigIntegerField Field'),
    ('bool', 'Boolean Field'),
    ('decimal', 'DecimalField Field'),
    ('email', 'Email Field'),
    ('float', 'FloatField Field'),
    ('text', 'TextField Field'),
    ('url', 'URLField Field'),  # Extend if necessary. For usage, generate field type in blackwidow > engine > extentions
)

# @decorate(is_object_context, route(route='custom-fields', display_name='Custom Field', module=ModuleEnum.Administration, group='Common'))
class CustomField(OrganizationDomainEntity):
    name = models.CharField(max_length=200)
    field_type = models.CharField(max_length=500, choices= _field_choices, default= None)

    @property
    def get_inline_manage_buttons(self, **kwargs):
        return [
            dict(
                name='Edit',
                action='edit',
                icon='icon-pencil',
                ajax='1',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.PartialEdit, parent=self.role_set.model.__name__.lower()),
                classes='manage-action load-modal',
                parent=self.role_set.all()[0]
            )
        ]


class CustomFieldValue(OrganizationDomainEntity):
    value = models.CharField(max_length=8000, null=True)
    field = models.ForeignKey(CustomField)

    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class Serializer(ss):
            name = serializers.CharField(required=True, write_only=True)
            value = serializers.CharField(required=True)

            def to_representation(self, obj):
                value = super().to_representation(obj)
                value['name'] = obj.field.name
                return value

            def create(self, validated_attrs):
                with transaction.atomic():
                    cfield = CustomField.objects.filter(name=validated_attrs['name'])
                    if cfield.exists():
                        cfield = cfield.first()
                    else:
                        cfield = CustomField()
                        cfield.organization = self.context['request'].c_organization
                        cfield.name = validated_attrs['name']
                        cfield.field_type = 'char'
                        cfield.save()

                    validated_attrs.pop('name', None)
                    validated_attrs['field'] = cfield
                    return super().create(validated_attrs)

            def update(self, instance, validated_attrs):
                with transaction.atomic():
                    cfield = CustomField.objects.filter(name=validated_attrs['name'])
                    if cfield.exists():
                        cfield = cfield.first()
                    else:
                        cfield = CustomField()
                        cfield.organization = self.context['request'].c_organization
                        cfield.name = validated_attrs['name']
                        cfield.save()

                    validated_attrs['field'] = cfield
                    return super().update(instance, validated_attrs)

            class Meta(ss.Meta):
                model = cls
                fields = ('name', 'value')

        return Serializer

