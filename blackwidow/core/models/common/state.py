from django.db import transaction, models
from blackwidow.core.models.common.division import Division
from blackwidow.core.models.contracts.configurabletype import ConfigurableType
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'Mahmud'


@decorate(expose_api("states"),is_object_context, enable_import,
          save_audit_log, route(route='districts', group='Common',group_order=7,item_order=3, module=ModuleEnum.Administration, display_name="District"))
class State(ConfigurableType):
    parent = models.ForeignKey(Division, null=True)

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create, ViewActionEnum.Edit, ViewActionEnum.Delete, ViewActionEnum.AdvancedImport]


    def get_choice_name(self):
        return self.name

    @classmethod
    def table_columns(cls):
        return 'code', 'name', 'parent:Division', 'last_updated'

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        from blackwidow.core.models.common.division import Division
        from blackwidow.core.models.common.country import Country

        country = Country.objects.filter(organization=user.organization).first()
        with transaction.atomic():
            if Division.objects.filter(name=data['0'], organization=user.organization).exists():
                div = Division.objects.filter(name=data['0'], organization=user.organization).first()
            else:
                div = Division()
                div.name = data['0']
                div.parent = country
                div.organization = user.organization
                div.save()

            if State.objects.filter(name=data['1'], organization=user.organization).exists():
                st = State.objects.filter(name=data['1'], organization=user.organization).first()
            else:
                st = State()
                st.name = data['1']
                st.organization = user.organization
                st.parent = div
                st.save()
            return st.pk

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
        from blackwidow.core.models.config.importer_config import ImporterConfig

        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if result or importer_config.columns.count() == 0:
            importer_config.save(**kwargs)
        else:
            for items in importer_config.columns.all():
                items.delete()
        columns = [
            ImporterColumnConfig(column=0, column_name='division', property_name='parent:name', ignore=False),
            ImporterColumnConfig(column=1, column_name='district', property_name='name', ignore=False),
        ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config
