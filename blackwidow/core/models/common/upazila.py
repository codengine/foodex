from django.db import transaction, models

from blackwidow.core.models.common.state import State
from blackwidow.core.models.contracts.configurabletype import ConfigurableType
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, loads_initial_data, is_object_context
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'Mahmud'


@decorate(expose_api('upazilas'),is_object_context, enable_import, loads_initial_data,
          save_audit_log, route(route='upazilas', group='Common',group_order=7,item_order=4, module=ModuleEnum.Administration, display_name="Upazila"))
class Upazila(ConfigurableType):
    parent = models.ForeignKey(State, null=True)

    def get_choice_name(self):
        return self.name


    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create, ViewActionEnum.Edit, ViewActionEnum.Delete, ViewActionEnum.AdvancedImport]

    @classmethod
    def table_columns(cls):
        return 'code', 'name', 'parent:District', 'last_updated'

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        from blackwidow.core.models.common.state import State

        with transaction.atomic():
            if State.objects.filter(name=data['0'], organization=user.organization).exists():
                district = State.objects.filter(name=data['0'], organization=user.organization).first()
            else:
                district = State()
                district.name = data['0']
                district.organization = user.organization
                district.save()

            if Upazila.objects.filter(name=data['1'], organization=user.organization).exists():
                upazila = Upazila.objects.filter(name=data['1'], organization=user.organization).first()
            else:
                upazila = Upazila()
                upazila.name = data['1']
                upazila.organization = user.organization
                upazila.parent = district
                upazila.save()
            return upazila.pk

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
        from blackwidow.core.models.config.importer_config import ImporterConfig

        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if result or importer_config.columns.count() == 0:
            importer_config.save(**kwargs)
        else:
            for items in importer_config.columns.all():
                items.delete()
        columns = [
            ImporterColumnConfig(column=0, column_name='district', property_name='parent:name', ignore=False),
            ImporterColumnConfig(column=1, column_name='upazila', property_name='name', ignore=False),
            ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config
