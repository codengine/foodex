from blackwidow.engine.decorators.expose_model import expose_api

from blackwidow.core.models.contracts.configurabletype import ConfigurableType
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.engine.managers.bwpermissionmanager import BWPermissionManager
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum


__author__ = 'Mahmud'

@decorate(expose_api('countries'),is_object_context, save_audit_log, route(route='countries', group='Localization', module=ModuleEnum.Administration, display_name="Country"))
class Country(ConfigurableType):

    def get_choice_name(self):
        return self.name