from decimal import Decimal

from django.db import models
from django.utils.safestring import mark_safe

from blackwidow.core.models.contracts.base import DomainEntity


class Location(DomainEntity):
    latitude = models.DecimalField(max_digits=16, decimal_places=6, default=Decimal("000.00"), null=True)
    longitude = models.DecimalField(max_digits=16, decimal_places=6, default=Decimal("000.00"), null=True)
    accuracy = models.CharField(max_length=20, default='0', null=True, blank=True)

    def load_initial_data(self, **kwargs):
        super().load_initial_data(**kwargs)
        self.latitude = 23
        self.longitude = 90
        self.accuracy = ''

    @classmethod
    def get_serializer(cls):
        ss = DomainEntity.get_serializer()

        class ODESerializer(ss):
            class Meta(ss.Meta):
                model = cls
                depth = 0
        return ODESerializer

    def __str__(self):
        if self.latitude == 0 or self.longitude == 0:
            return "N/A"
        return mark_safe('<a class="inline-link" href="http://maps.google.com/maps?z=17&q='
                         + str(self.latitude) + ','
                         + str(self.longitude) + '" target="_blank">'
                         + str(self.latitude) + ', ' + str(self.longitude) + '</a>')

    def clean(self):
        if self.latitude is None:
            self.latitude = Decimal(0.0)

        if self.longitude is None:
            self.longitude = Decimal(0.0)

        if self.accuracy is None:
            self.accuracy = '100%'