from django.db import models

from blackwidow.core.models.common.country import Country
from blackwidow.core.models.contracts.configurabletype import ConfigurableType
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from config.enums.modules_enum import ModuleEnum


__author__ = 'Mahmud'


@decorate(expose_api('divisions'),is_object_context, save_audit_log, route(route='divisions', group='Localization', module=ModuleEnum.Administration, display_name="Division"))
class Division(ConfigurableType):
    parent = models.ForeignKey(Country, null=True)

    @classmethod
    def table_columns(cls):
        return 'code', 'name', 'parent:Country', 'last_updated'

    def get_choice_name(self):
        return self.name

    def __str__(self):
        return self.name

