from django.core.exceptions import ValidationError
from django.db import models, transaction
from rest_framework import serializers

from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from config.enums.modules_enum import ModuleEnum


__author__ = 'Mahmud'


@decorate(expose_api('qrcodes'),save_audit_log, is_object_context, enable_import)
class QRCode(OrganizationDomainEntity):
    key = models.CharField(max_length=200, unique=True)
    value = models.CharField(max_length=1000)
    is_used = models.BooleanField(default=False)

    def __str__(self):
        return self.reference_id+': '+self.value

    @classmethod
    def table_columns(cls):
        return 'code', 'key', 'reference_id', 'is_used', 'last_updated'

    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class Serializer(ss):
            key = serializers.CharField(max_length=500, required=False)
            value = serializers.CharField(max_length=500, required=False)
            is_used = serializers.BooleanField(required=False, default=False)

            def create(self, attrs):
                attrs.update({
                    'reference_id': '' if attrs.get('reference_id', '') == '' else attrs.get('reference_id', '0')
                })
                if QRCode.objects.filter(key=attrs.get('key', '')).exists():
                    qr_code = QRCode.objects.filter(key=attrs.get('key', '')).first()
                    if qr_code.is_used:
                        raise ValidationError('QR Code already exists and assigned to another user/client.')
                    return qr_code
                elif QRCode.objects.filter(value=attrs.get('value', '')).exists():
                    qr_code = QRCode.objects.filter(value=attrs.get('value', '')).first()
                    if qr_code.is_used:
                        raise ValidationError('QR Code already exists and assigned to another user/client.')
                    return qr_code
                elif attrs.get('reference_id', '') != '' and QRCode.objects.filter(reference_id=int(attrs.get('reference_id', ''))).exists():
                    qr_code = QRCode.objects.filter(reference_id=int(attrs.get('reference_id', ''))).first()
                    if qr_code.is_used:
                        raise ValidationError('QR Code already exists and assigned to another user/client.')
                    return qr_code
                return super().create(attrs)

            class Meta(ss.Meta):
                model = cls

        return Serializer

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        # ImporterColumnConfig= 'core.ImporterColumnConfig'
        # ImporterConfig = 'core.ImporterConfig'
        from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
        from blackwidow.core.models.config.importer_config import ImporterConfig
        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if result or importer_config.columns.count() == 0:
            importer_config.save(**kwargs)
        else:
            for items in importer_config.columns.all():
                items.delete()
        columns = [
            ImporterColumnConfig(column=0, column_name='key', property_name='key', ignore=False),
            ImporterColumnConfig(column=1, column_name='value', property_name='value', ignore=False),
            ImporterColumnConfig(column=2, column_name='reference_id', property_name='value', ignore=False),
            ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config


    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        with transaction.atomic():
            org = user.organization
            qr_codes = QRCode.objects.filter(key=data['0'])
            if qr_codes.exists():
                qr_code = qr_codes[0]
            else:
                qr_code = QRCode()
                qr_code.organization = org
                qr_code.key = data['0']
                qr_code.value = data['1']
                if data['2']:
                    qr_code.reference_id = str(int(data['2']))
                qr_code.save()
            return qr_code.pk



