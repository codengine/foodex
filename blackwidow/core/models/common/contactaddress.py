from rest_framework import serializers

from blackwidow.core.models.common.country import Country
from blackwidow.core.models.common.location import Location
from blackwidow.core.models.contracts.base import DomainEntity
from blackwidow.engine.decorators.utility import loads_initial_data


__author__ = 'Mahmud'

from blackwidow.core.models.common.state import *


@decorate(loads_initial_data)
class ContactAddress(DomainEntity):
    street = models.CharField(default='', max_length=500)
    city = models.CharField(default='', max_length=200)
    province = models.CharField(default='', max_length=200)
    division = models.ForeignKey(Division, null=True)
    state = models.ForeignKey(State, null=True) #district
    country = models.ForeignKey(Country, null=True)
    is_primary = models.BooleanField(default=0)
    location = models.ForeignKey(Location, null=True)
    address_type = models.CharField(max_length=200, default='Home Address')

    @classmethod
    def get_dependent_field_list(cls):
        return ['location']

    @classmethod
    def get_serializer(cls):
        ss = DomainEntity.get_serializer()

        class ODESerializer(ss):
            country = serializers.PrimaryKeyRelatedField(required=False, queryset=Country.objects.all())
            state = serializers.PrimaryKeyRelatedField(required=False, queryset=State.objects.all())
            division = serializers.PrimaryKeyRelatedField(required=False, queryset=Division.objects.all())
            city = serializers.CharField(required=False)
            province = serializers.CharField(required=False)
            location = Location.get_serializer()()

            class Meta:
                model = cls
                read_only_fields = ss.Meta.read_only_fields + ('organization', )

        return ODESerializer

    def load_initial_data(self, **kwargs):
        super().load_initial_data(**kwargs)
        self.street = "Street Address " + str(kwargs['index'])
        self.city = "City " + str(kwargs['index'])

        country = Country.objects.first() if Country.objects.count() > 0 else Country()
        if country.pk is None:
            country.name = "Bangladesh"
            country.organization = kwargs['org']
            country.save()

        state = State.objects.filter(parent=country)[0] if State.objects.filter(parent=country).count() > 0 else State()
        if state.pk is None:
            state.name = "Dhaka"
            state.organization = kwargs['org']
            state.parent = country
            state.save()

        self.country = country
        self.state = state
        self.save()

    def __str__(self):
        result = ''
        if self.street:
            result = self.street
            result += ', '
        if self.city:
            result += self.city
            result += ', '

        if self.province:
            result += self.province
            result += ', '

        if self.state:
            result += str(self.state)
            result += ', '
        if self.division:
            result += str(self.division)
            result += ', '
        if self.country:
            result += str(self.country)

        return result









