from django.contrib.contenttypes.models import ContentType
from django.apps import apps
from django.db.models.query_utils import Q
from rest_framework import serializers
from blackwidow.core.models.device.applications import DeviceApplication
from blackwidow.core.models.device.organization_applications import FieldBuzzApplication
from blackwidow.core.models.device.devices import Device

from blackwidow.core.models.common.custom_field import CustomField
from blackwidow.core.models.modules.module import BWModule
from blackwidow.core.viewmodels.tabs_config import TabView, TabViewAction
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.core.models.permissions.query_filter import QueryFilter
from blackwidow.engine.decorators.route_partial_routes import route, partial_route
from blackwidow.engine.exceptions.exceptions import EntityNotDeletableException, BWException
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'mahmudul'

from django.db import models
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.permissions.rolepermission import RolePermission


@decorate(save_audit_log, is_object_context, expose_api('roles'),
          route(route='roles', display_name='User Role', group='Other admin', module=ModuleEnum.Administration),
          partial_route(relation='normal',
                        models=[RolePermission, QueryFilter, CustomField, BWModule, DeviceApplication,
                                FieldBuzzApplication]))
class Role(OrganizationDomainEntity):
    priority = models.IntegerField(default=0)
    name = models.CharField(max_length=254, default='', unique=True)
    landing_url = models.CharField(max_length=254, default='')
    permissions = models.ManyToManyField(RolePermission, through='core.RolePermissionAssignment')
    modules = models.ManyToManyField(BWModule, through='core.ModulePermissionAssignment')
    is_implemented = models.BooleanField(default=0)
    filters = models.ManyToManyField(QueryFilter)
    parent = models.ManyToManyField('self', symmetrical=False)
    custom_fields = models.ManyToManyField(CustomField)
    get_alert = models.BooleanField(default=0)
    allowed_apps = models.ManyToManyField('core.DeviceApplication')
    fieldbuzz_apps = models.ManyToManyField('core.FieldBuzzApplication')

    # @classmethod
    # def get_manage_buttons(cls):
    # return []

    @classmethod
    def get_dependent_field_list(cls):
        return ['custom_fields']

    @property
    def get_rename(self):
        if self.name == 'ServicePerson':
            return 'Sales Officer'
        return self.name

    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class Serializer(ss):
            custom_fields = CustomField.get_serializer()(many=True, required=False)
            name = serializers.CharField(max_length=254, default='')

            class Meta(ss.Meta):
                model = Role
                fields = ('id', 'name', 'custom_fields', 'reference_id')
                read_only_fields = ss.Meta.read_only_fields + ('organization', 'created_by', 'last_updated_by')

        return Serializer

    @property
    def tabs_config(self):
        tabs = [
            TabView(
                title='User(s) in Role',
                access_key='users',
                route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
                relation_type=ModelRelationType.INVERTED,
                related_model='core.ConsoleUser',
                queryset=None,
                queryset_filter=Q(**{'role__pk': self.pk})
            ),
            TabView(
                title='Custom Field(s)',
                access_key='custom_fields',
                route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
                relation_type=ModelRelationType.NORMAL,
                related_model=CustomField,
                queryset=self.custom_fields.all(),
                queryset_filter=None,
                property=self.custom_fields,
                actions=[
                    TabViewAction(
                        title="Add",
                        action="action",
                        icon="icon-plus",
                        css_class='manage-action load-modal fis-plus-ico',
                        route_name=CustomField.get_route_name(action=ViewActionEnum.PartialCreate,
                                                              parent=self.__class__.__name__.lower())
                    ),
                    TabViewAction(
                        title="Delete",
                        action="close",
                        icon="icon-remove",
                        css_class='manage-action delete-item fis-remove-ico',
                        route_name=CustomField.get_route_name(action=ViewActionEnum.PartialDelete,
                                                              parent=self.__class__.__name__.lower())
                    )
                ]
            ),
            TabView(
                title='Top Menu Permission(s)',
                access_key='top_menus',
                route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
                relation_type=ModelRelationType.INVERTED,
                related_model='core.ModulePermissionAssignment',
                child_tabs='modules,permissions',
                queryset=None,
                enable_inline_edit=True,
                queryset_filter=Q(**{'pk__in': self.get_parent_module_permission()})
            ),
            TabView(
                title='Module Permission(s)',
                access_key='modules',
                route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
                relation_type=ModelRelationType.INVERTED,
                related_model='core.ModulePermissionAssignment',
                child_tabs='permissions',
                queryset=None,
                enable_inline_edit=True,
                queryset_filter=Q(**{'pk__in': self.get_child_module_permissions()})
            ),
            TabView(
                title='Object Permission(s)',
                access_key='permissions',
                route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
                relation_type=ModelRelationType.INVERTED,
                related_model='core.RolePermissionAssignment',
                queryset=None,
                enable_inline_edit=True,
                queryset_filter=Q(**{'pk__in': self.get_object_permissions_names()})
            ),
            TabView(
                title='Allowed App Permission(s)',
                access_key='allowedappinrole',
                route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
                relation_type=ModelRelationType.NORMAL,
                property=self.allowed_apps,
                add_more_queryset=DeviceApplication.objects.filter().exclude(
                    pk__in=self.allowed_apps.values_list('id', flat=True)),
                related_model=DeviceApplication,
                actions=[
                    TabViewAction(
                        title='Add',
                        action='add',
                        icon='icon-plus',
                        route_name=DeviceApplication.get_route_name(action=ViewActionEnum.PartialBulkAdd,
                                                                    parent=self.__class__.__name__.lower()),
                        css_class='manage-action load-modal fis-plus-ico',

                    ),
                    TabViewAction(
                        title='Remove',
                        action='partial-remove',
                        icon='icon-remove',
                        route_name=DeviceApplication.get_route_name(action=ViewActionEnum.PartialBulkRemove,
                                                                    parent=self.__class__.__name__.lower()),
                        css_class='manage-action delete-item fis-remove-ico'
                    )
                ])
        ]
        return tabs

    def get_parent_module_permission(self):
        from blackwidow.core.models.permissions.modulepermission_assignment import ModulePermissionAssignment

        parent_module_permissions = ModulePermissionAssignment.objects.filter(
            Q(module__parent=None) & Q(role_id=self.pk))
        module_permissions = parent_module_permissions.values_list('pk', flat=True)
        return module_permissions

    def get_child_module_permissions(self):
        from blackwidow.core.models.permissions.modulepermission_assignment import ModulePermissionAssignment

        parent_module_permissions = ModulePermissionAssignment.objects.filter(
            Q(module__parent=None) & Q(role_id=self.pk) & Q(access__gt=0))
        parent_module_list = parent_module_permissions.values_list('module__pk', flat=True)
        child_module_permissions = ModulePermissionAssignment.objects.filter(
            ~Q(module__parent=None) & Q(role_id=self.pk))
        child_module_permissions = child_module_permissions.filter(
            Q(module__parent__id__in=parent_module_list))
        module_permissions = child_module_permissions.values_list('pk', flat=True)
        return module_permissions

    def get_object_permissions_names(self):
        from blackwidow.core.models.permissions.modulepermission_assignment import ModulePermissionAssignment
        from blackwidow.core.models.permissions.rolepermission_assignment import RolePermissionAssignment

        child_module_permissions = ModulePermissionAssignment.objects.filter(
            ~Q(module__parent=None) & Q(role_id=self.pk) & Q(access__gt=0))
        child_module_name_list = child_module_permissions.values_list('module__name', flat=True)
        object_permissions = RolePermissionAssignment.objects.filter(Q(role_id=self.pk))
        object_permissions_list = []
        for object_permission in object_permissions:
            model_name = object_permission.permission.context
            model_objects = ContentType.objects.filter(model=model_name.lower())
            for model_object in model_objects:
                model = apps.get_model(model_object.app_label, model_name)
                group = model.get_model_meta('route', 'group')
                if group in child_module_name_list:
                    object_permissions_list += [object_permission.pk]
        object_permissions = object_permissions.filter(
            Q(pk__in=object_permissions_list)).values_list('pk', flat=True)
        return object_permissions

    def save(self, *args, **kwargs):
        return super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        user = kwargs.get('user', None)
        if user:
            if user.role.id == self.id:
                raise EntityNotDeletableException("You cannot delete your own role.")

            if self.is_implemented:
                raise EntityNotDeletableException(
                    'This role is mandatory for running the system. Please contact system administrator to modify/delete this role.')
        else:
            raise BWException("You must provide user performing the operation as named parameter.")
        super().delete(*args, **kwargs)

    def get_choice_name(self):
        return self.name

    def app_assignment(self, **kwargs):
        tab = [x for x in self.tabs_config if x.access_key == kwargs['tab']][0]
        console_user_set = self.consoleuser_set.all()
        device_set = Device.objects.filter(service_person=console_user_set)

        if tab.access_key == 'allowedappinrole' and '/bulk-add/' in kwargs['path']:
            allowed_app_set = list(DeviceApplication.objects.filter(pk__in=[_pk for _pk in kwargs['ids'].split(',')]))
            if len(allowed_app_set) > 0:
                for device in device_set:
                    device.applications.add(*allowed_app_set)

        elif tab.access_key == 'allowedappinrole' and '/bulk-remove/' in kwargs['path']:
            allowed_app_set = list(DeviceApplication.objects.filter(pk__in=[_pk for _pk in kwargs['ids'].split(',')]))
            if len(allowed_app_set) > 0:
                for device in device_set:
                    device.applications.remove(*allowed_app_set)


    class Meta:
        app_label = 'core'