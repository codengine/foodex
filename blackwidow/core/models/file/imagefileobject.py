import re

from django.utils.safestring import mark_safe
from rest_framework import serializers

from blackwidow.core.models.file.fileobject import FileObject
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from settings import STATIC_UPLOAD_ROOT, PROJECT_PATH


__author__ = 'ruddra'


@decorate(is_object_context,expose_api('image-uploads'), save_audit_log)
class ImageFileObject(FileObject):
    def __str__(self):
        return mark_safe('<img width="100" height="100" src="' + re.sub('.*' + STATIC_UPLOAD_ROOT.replace(PROJECT_PATH, '').replace('/', '\\/'), '/static-media/uploads/', str(self.file)) + '" />')

    @property
    def relative_url(self):
        return re.sub('(.)*' + STATIC_UPLOAD_ROOT.replace(PROJECT_PATH, '').replace('/', '\\/'), '/static-media/uploads/', str(self.file))

    @classmethod
    def get_serializer(cls):
        ss = FileObject.get_serializer()

        class Serializer(ss):
            relative_url = serializers.CharField(read_only=True)

            class Meta(ss.Meta):
                model = cls

        return Serializer

    class Meta:
        proxy = True