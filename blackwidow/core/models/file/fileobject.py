import os

from rest_framework import serializers

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from settings import PROJECT_PATH, STATIC_UPLOAD_ROOT


__author__ = 'ActiveHigh'

from django.db import models


class FileObject(OrganizationDomainEntity):
    name = models.CharField(max_length=8000, null=True, default=None)
    path = models.CharField(max_length=8000, null=True, default=None)
    extension = models.CharField(max_length=10, null=True, default=None)
    description = models.CharField(max_length=8000, null=True, default=None)
    file = models.FileField(default=None, max_length=8000, upload_to=os.path.join(PROJECT_PATH, STATIC_UPLOAD_ROOT), null=True)

    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class Serializer(ss):
            path = serializers.CharField(required=False, max_length=8000)
            extension = serializers.CharField(required=False, max_length=10)
            class Meta(ss.Meta):
                model = cls

        return Serializer


    def clean(self):
        self.type = self.__class__.__name__
        super().clean()

    def __str__(self):
        if self.code and self.name:
            return self.code + " : " + self.name
        elif self.code and not self.name:
            return self.code
        elif not self.code and self.name:
            return self.name
        else:
            return ''

    def get_choice_name(self):
        if self.code and self.name:
            return self.code + " : " + self.name
        elif self.code and not self.name:
            return self.code
        elif not self.code and self.name:
            return self.name
        else:
            return ''

    @property
    def serializable_fields(self):
        return ('name', ) + super().serializable_fields

    def save(self, *args, **kwargs):
        self.file_type = self.__class__.__name__
        super().save(*args, **kwargs)

    class Meta:
        app_label = 'core'