from blackwidow.core.models.file.fileobject import FileObject
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from config.enums.modules_enum import ModuleEnum

__author__ = 'ruddra'


@decorate(save_audit_log, is_object_context, route(route='import-files', group='Import/Export', module=ModuleEnum.Administration, display_name="Data File"))
class ImportFileObject(FileObject):

    @classmethod
    def all(cls):
        return FileObject.objects.filter(file_type=cls.__name__)

    class Meta:
        proxy = True