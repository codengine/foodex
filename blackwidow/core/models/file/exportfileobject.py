from config.enums.view_action_enum import ViewActionEnum

__author__ = 'ruddra'
from blackwidow.core.models.file.fileobject import FileObject
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, is_object_context
from config.enums.modules_enum import ModuleEnum


@decorate(is_object_context,route(route='export-files', group='Import/Export', module=ModuleEnum.Administration, display_name="Exported File"))
class ExportFileObject(FileObject):

    @classmethod
    def all(cls):
        return FileObject.objects.filter(file_type=cls.__name__)

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Download, ViewActionEnum.Delete]

    class Meta:
        proxy = True

