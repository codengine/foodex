from crequest.middleware import CrequestMiddleware
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models, transaction
from django.utils.safestring import mark_safe
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from blackwidow.core.models.common.bank_info import BankAccountDetails, MobileBankingDetails
from blackwidow.core.models.common.educational_qualification import EducationalQualification
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.core.models.common.custom_field import CustomFieldValue
from blackwidow.core.models.file.imagefileobject import ImageFileObject
from blackwidow.core.models.common.phonenumber import PhoneNumber
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.users.user_settings import SettingsItem
from blackwidow.engine.decorators.enable_assignment import enable_assignment
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import save_audit_log, decorate, is_role_context, is_object_context
from blackwidow.core.models.common.contactaddress import ContactAddress
from blackwidow.core.models.common.emailaddress import EmailAddress
from blackwidow.core.models.contracts.base import DomainEntity
from blackwidow.core.models.roles.role import Role
from blackwidow.core.models.common.qr_code import QRCode
from blackwidow.engine.extensions.bw_titleize import bw_titleize
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'mahmudul'

class SettingsItemValue(OrganizationDomainEntity):
    value = models.CharField(max_length=500)
    settings_item = models.ForeignKey(SettingsItem)

    @classmethod
    def get_dependent_field_list(cls):
        return []

    @property
    def render_settings(self):
        return bw_titleize(self.settings_item.type.replace('SettingsItem', ''))

    @property
    def render_value(self):
        return self.settings_item.format_value(self.value)

    @classmethod
    def table_columns(cls):
        return 'render_settings', 'render_value'


class UserSerializer(serializers.ModelSerializer):

    def is_valid(self, raise_exception=False):
        return True

    def update(self, instance, attrs):
        instance = super().update(instance, attrs)
        if attrs.get('password', None):
            instance.set_password(attrs.get('password'))
        instance.save()
        return instance

    def create(self, attrs):
        obj = super().create(attrs)
        obj.set_password(obj.password)
        obj.save()
        return obj

    def validate_username(self, value):
        """
        Check duplicate entry for username
        """
        _user = User.objects.filter(username=value)
        if _user.exists():
            # raise APIException( 'Cannot login with provided credentials.')
            raise serializers.ValidationError("Username already exists.")

        return value

    class Meta:
        model = User
        fields = ('id', 'username', 'password')
        read_only_fields = ('id',)

_CHOICES_MF = (
    ('M', 'Male'),
    ('F', 'Female'),
)

@decorate(save_audit_log,is_object_context, expose_api('users'),
          route(route='users', group='Users', module=ModuleEnum.Administration, display_name='All Users', hide=True),
          enable_assignment(targets=['route'], role=['user']))
class ConsoleUser(OrganizationDomainEntity):
    user = models.OneToOneField(User, null=True)
    is_super = models.BooleanField(default=False)
    name = models.CharField(max_length=200)
    addresses = models.ManyToManyField(ContactAddress, null=True)
    emails = models.ManyToManyField(EmailAddress, null=True)
    phones = models.ManyToManyField(PhoneNumber, null=True)
    role = models.ForeignKey(Role, null=False)
    qr_code = models.ForeignKey(QRCode, null=True)
    male_or_female = models.CharField(max_length=2, choices=_CHOICES_MF, null=True, default='M')
    device_id = models.CharField(max_length=200, default=None, null=True)
    settings_value = models.ManyToManyField(SettingsItemValue)
    custom_fields = models.ManyToManyField(CustomFieldValue)
    bank_details = models.OneToOneField(BankAccountDetails, null=True)
    mobile_banking_details = models.OneToOneField(MobileBankingDetails, null=True)
    education = models.OneToOneField(EducationalQualification, null=True)
    image = models.ForeignKey(ImageFileObject, null=True, default=None)
    assigned_to = models.ForeignKey(InfrastructureUnit, null=True, default=None)
    assigned_units = models.ManyToManyField(InfrastructureUnit, related_name="assigned_units")
    date_of_birth = models.CharField(max_length=200, default='')
    national_id = models.CharField(max_length=200, default='')


    @property
    def details_config(self):
        d = super().details_config

        for x in self.custom_fields.all():
            d[x.field.name] = x.value

        index = 1
        for x in self.addresses.all():
            d['address ' + str(index)] = str(x)
            index += 1

        return d

    def details_link_config(self, **kwargs):
        consoleUserDetailLinkConfig = super().details_link_config(**kwargs)
        _request = CrequestMiddleware.get_request()
        consoleUserDetailLinkConfig.append(
            dict(
                name='Reset Password',
                title='Reset Password',
                action='print',
                icon='icon-key',
                ajax='0',
                url_name='consoleuser_reset_password'
            ),
        )

        return consoleUserDetailLinkConfig

    @property
    def tabs_config(self):
        tabs = [TabView(
            title='Settings',
            access_key='settings_value',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model=SettingsItemValue,
            property=self.settings_value
        )]
        return tabs

    @classmethod
    def get_custom_serializers(cls):
        return ('user', UserSerializer),

    def to_business_user(self):
        for x in ConsoleUser.__subclasses__():
            if x.__name__ == self.type:
                self.__class__ = x
        return self

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @classmethod
    def serialize_fields(self, prefix=''):
        return [
            {
                "controlType": "inputText",
                "textBoxType": "singline",
                "story": "Name",
                "textBoxInputType": "alphanumeric",
                "name": "name"
            }
        ]

    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class Serializer(ss):
            role = serializers.PrimaryKeyRelatedField(required=True, queryset=Role.objects.all())
            addresses = ContactAddress.get_serializer()(many=True, required=False)
            emails = EmailAddress.get_serializer()(many=True, required=False)
            phones = PhoneNumber.get_serializer()(many=True, required=False)
            qr_code = QRCode.get_serializer()(many=False, required=False)
            device_id = serializers.CharField(max_length=200, required=False, default="")
            male_or_female = serializers.CharField(max_length=200, required=False, default="M")
            custom_fields = CustomFieldValue.get_serializer()(many=True, required=False)
            bank_details = BankAccountDetails.get_serializer()(required=False)
            mobile_banking_details = MobileBankingDetails.get_serializer()(required=False)
            education = EducationalQualification.get_serializer()(required=False)
            image = serializers.PrimaryKeyRelatedField(required=False, queryset=ImageFileObject.objects.all())
            user = UserSerializer(required=True)
            settings_value = serializers.PrimaryKeyRelatedField(required=False, many=True, queryset=SettingsItemValue.objects.none())

            def validate_user(self, user):
                if user is not None:
                    if 'username' not in user:
                        return user
                    if self.instance and User.objects.filter(username=user['username']).exclude(pk=self.instance.user.id).exists():
                        raise ValidationError('Username already exists.')
                return user

            def update(self, instance, validated_data):
                with transaction.atomic():
                    user = validated_data.pop('user', None)
                    # custom_fields = validated_data.pop('custom_fields', None)

                    instance = super().update(instance, validated_data)

                    if user is not None:
                        if 'username' in user.keys():
                            instance.user.username = user['username']
                        if 'password' in user.keys():
                            instance.user.set_password(user['password'])
                        instance.user.save()

                    # if custom_fields is not None:
                    #     for f in custom_fields:
                    #         field = self.custom_fields.filter(field__name=f['name'])
                    #         if field.exists():
                    #             field = field.first()
                    #             CustomFieldValue.get_serializer()(self.context).update(field, f)

                    return instance

            def create(self, validated_data):
                with transaction.atomic():
                    instance = super().create(validated_data)
                    if instance.custom_fields.exists():
                        for x in instance.custom_fields.all():
                            if x.pk not in [instance.role.custom_fields.values_list('pk')]:
                                instance.role.custom_fields.add(x.field)
                    return instance

            class Meta(ss.Meta):
                model = cls

        return Serializer

    # def details_link_config(self, **kwargs):
    #     return super().details_link_config(**kwargs)


    def save(self, *args, **kwargs):
        with transaction.atomic():
            if self.qr_code is not None:
                self.qr_code.is_used = True
                self.qr_code.save()
            super().save(*args, **kwargs)

    @classmethod
    def get_dependent_field_list(cls):
        return ['addresses', 'emails', 'phones', 'settings_value', 'user', 'custom_fields', 'bank_details', 'education']

    def get_choice_name(self):
        return self.name

    @property
    def render_phone(self):
        return ', '.join([str(x) for x in self.phones.all()])

    @property
    def render_address(self):
        return ', '.join([str(x) for x in self.addresses.all()])
    
    @property
    def render_code(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.pk}) + "' >" + self.code + "</a>")

    @classmethod
    def table_columns(cls):
        return "render_code", "name", "render_address", "render_phone", "last_updated"

    def filter_model(self, queryset=None, **kwargs):
        return queryset

    def delete(self, *args, **kwargs):
        if self.qr_code:
            self.qr_code.is_used = False
            self.qr_code.save()
        super().delete(*args, **kwargs)

@decorate(is_role_context,is_object_context, save_audit_log, expose_api('system-admins'),
    route(route='system-admins', group='Users', module=ModuleEnum.Administration, display_name='Other User'))
class SystemAdmin(ConsoleUser):

    class Meta:
        proxy = True


class WebUser(DomainEntity):
    name = models.CharField(max_length=200)
    address = models.ForeignKey(ContactAddress, null=True, on_delete=models.SET_NULL)
    email = models.ForeignKey(EmailAddress, null=True, on_delete=models.SET_NULL)
    phone = models.ForeignKey(PhoneNumber, null=True, on_delete=models.SET_NULL)
    designation = models.CharField(max_length=200, default='')

    @classmethod
    def get_serializer(cls):
        ss = DomainEntity.get_serializer()

        class Serializer(ss):
            address = ContactAddress.get_serializer()(required=False)
            email = EmailAddress.get_serializer()(required=False)
            phone = PhoneNumber.get_serializer()(required=False)

            class Meta(ss.Meta):
                model = cls
        return Serializer

    @classmethod
    def get_dependent_field_list(cls):
        return ['address', 'email', 'phone']

    def load_initial_data(self, **kwargs):
        super().load_initial_data(**kwargs)
        self.name = "Web user " + str(kwargs['index'])

        address = ContactAddress()
        address.load_initial_data(**kwargs)
        self.address = address

        email = EmailAddress()
        email.load_initial_data(**kwargs)
        self.email = email
        self.save()


    def __str__(self):
        result = self.name
        result += ",<br/><em>address: </em>" + str(self.address)
        result += ',<br/><em>email: </em>' + str(self.email)
        return result



