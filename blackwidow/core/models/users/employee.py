from django.contrib.auth.models import User
from django.db import transaction

from blackwidow.core.models.common.phonenumber import PhoneNumber
from blackwidow.core.models.roles.role import Role
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.utility import decorate
from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
from blackwidow.core.models.config.importer_config import ImporterConfig


__author__ = 'Mahmud'


@decorate(enable_import,
          # is_role_context, is_object_context, route(route='employees', display_name='Employee', group='Employee', module=ModuleEnum.Administration)
)
class Employee(ConsoleUser):

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if result or importer_config.columns.count() == 0:
            importer_config.save(**kwargs)
            columns = [
                ImporterColumnConfig(column=0, column_name='Employee ID', property_name='reference_id', ignore=False),
                ImporterColumnConfig(column=1, column_name='Name', property_name='name', ignore=False),
                ImporterColumnConfig(column=2, column_name='Designation', property_name='role:name', ignore=False),
                ImporterColumnConfig(column=3, column_name='Phone', property_name='phone_numbers:phone', ignore=False),
                ]
            for c in columns:
                c.save(organization=organization, **kwargs)
                importer_config.columns.add(c)
        return importer_config


    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        with transaction.atomic():
            org = user.organization
            employees= Employee.objects.filter(reference_id=data['0'])
            if employees.exists():
                employee = employees[0]
            else:
                employee = Employee()

            employee.organization = org
            employee.reference_id = data['0']
            employee.name = data['1']

            e_role_name=str(data['2']).strip()
            e_role= Role.objects.filter(name__iexact= e_role_name)
            if e_role.exists():
                employee.role= e_role[0]
            else:
                new_role= Role.objects.create(organization=org, name=e_role_name)
                employee.role= new_role
                employee.save()
            if len(str(data['3'])) > 4:
                ph_number = PhoneNumber()
                ph_number.organization = org
                ph_number.phone = str(data['3']).strip('.0')
                ph_number.save()
                employee.phone_number = ph_number
                employee.save()

            if employee.user is None:
                _user = User()
                _user.username = employee.reference_id if employee.reference_id is not None or employee.reference_id != '' else employee.code
                _user.save()
                _user.set_password(_user.username)
                _user.save()
                employee.user = _user
                employee.save()

            return employee.pk

    def get_queryset(self, queryset=None, **kwargs):
        return super().get_queryset(queryset=queryset, **kwargs)

    class Meta:
        proxy = True