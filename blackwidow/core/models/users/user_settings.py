from blackwidow.core.models.common.settings_item import SettingsItem

__author__ = 'Mahmud'


class TimeZoneSettingsItem(SettingsItem):
    def format_value(self, value):
        v = (-1) * int(value)/60
        return "GMT " + ('' if v == 0 else ('+' if v > 0 else '-')) + ' ' + str(v)

    class Meta:
        proxy = True