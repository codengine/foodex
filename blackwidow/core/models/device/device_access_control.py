from django.db import transaction
from blackwidow.core.models.contracts.base import DomainEntity
from django.db import models

__author__ = 'zia ahmed'


def generate_passcode(uuid):
    uuid_length = len(uuid)
    custom_length = int(uuid_length/2)
    my_list = []
    for x in range(0, custom_length):
        my_list.append(uuid[x])
        my_list.append(uuid[uuid_length-(x+1)])

    passcode = ''.join(my_list)
    return passcode


class DeviceWipeData(DomainEntity):
    device = models.OneToOneField('core.Device', related_name='wipe_data')
    wipe_data_flag = models.BooleanField(default=False)

    def __str__(self):
        return str('Wiped' if self.wipe_data_flag is True else 'Not wiped')


class DeviceLock(DomainEntity):
    device = models.OneToOneField('core.Device', related_name='device_lock')
    lock_flag = models.BooleanField(default=False)
    pass_code = models.CharField(max_length=100, default='abc123456789')

    def __str__(self):
        return str('Locked' if self.lock_flag is True else 'Unlocked')

    def save(self, *args, **kwargs):
        with transaction.atomic():
            self.type = DeviceLock.__name__
            super().save(*args, **kwargs)
            uuid = str(self.device.uuid)
            if uuid:
                self.pass_code = generate_passcode(uuid)
            super().save(*args, **kwargs)

            return self