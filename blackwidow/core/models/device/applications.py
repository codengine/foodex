from blackwidow.core.models.contracts.base import DomainEntity
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from django.db import models
from django.db import transaction
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, is_object_context, save_audit_log
from config.enums.modules_enum import ModuleEnum

__author__ = 'zia ahmed'


# @decorate(is_object_context, save_audit_log,
#           route(route='device-applications', group='Application', module=ModuleEnum.DeviceManager, display_name='Device Applicaiton'))
class DeviceApplication(OrganizationDomainEntity):
    name = models.CharField(max_length=200)

    def get_choice_name(self):
        return self.code+": "+self.name

    def save(self, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)


class ApplicationPackage(OrganizationDomainEntity):
    name = models.CharField(max_length=200)
    application = models.ForeignKey(DeviceApplication, related_name='packages')

    def get_choice_name(self):
        return self.code+": "+self.name

    def save(self, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)