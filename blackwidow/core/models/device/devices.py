from django.db import transaction
from django.db.models.loading import get_model
from blackwidow.core.models.device.applications import DeviceApplication
from django.db import models
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.engine.extensions.clock import Clock
from django.db.models.signals import m2m_changed

__author__ = 'zia ahmed'

# @decorate(is_role_context, is_object_context, save_audit_log,
#           route(route='devices', group='Device', module=ModuleEnum.DeviceManager, display_name='Organization Device'))
class Device(OrganizationDomainEntity):
    uuid = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200, null=True)
    manufacturer = models.ForeignKey('core.Manufacturer', null=True)
    sim_serial_number = models.CharField(max_length=200, null=True)
    sim_number = models.IntegerField(null=True)
    pin_code = models.IntegerField(null=True)
    puk_code = models.IntegerField(null=True)
    category = models.ForeignKey('core.DeviceCategory', related_name='category_wise_devices',null=True)
    assigned_user = models.OneToOneField('core.WebUser', null=True)
    service_person = models.OneToOneField('core.ConsoleUser',null=True)
    applications = models.ManyToManyField('core.DeviceApplication', null=True)
    fieldbuzz_applications = models.ManyToManyField('core.FieldBuzzApplication', null=True)
    app_assignment_time = models.BigIntegerField(editable=False, default=0)

    def get_choice_name(self):
        return self.code+": "+self.name

    def save(self, organization=None, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)


def applications_changed(sender, **kwargs):
    device_obj = kwargs['instance']
    app_model = kwargs['model']

    if device_obj.__class__ == get_model('device_manager', 'OrganizationDevice') and app_model == DeviceApplication:
        if kwargs['action'] == "post_add" or kwargs['action'] == "post_remove":
            device_obj.app_assignment_time = Clock.timestamp()
            device_obj.save()
            # print(sender, kwargs['model'])


m2m_changed.connect(applications_changed, sender=Device.applications.through)