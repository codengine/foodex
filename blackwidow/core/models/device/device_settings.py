from django.db import transaction
from django.db import models
from django.utils.safestring import mark_safe
from blackwidow.core.models.contracts.base import DomainEntity

__author__ = 'zia ahmed'




class DeviceSettings(DomainEntity):
    AUTO_ORIENTATION = 1
    PORTRAIT_ORIENTATION = 2
    LANDSCAPE_ORIENTATION = 3

    ORIENTATION_CHOICES = (
        (AUTO_ORIENTATION, 'Auto'),
        (PORTRAIT_ORIENTATION, 'Portrait'),
        (LANDSCAPE_ORIENTATION, 'Landscape'),
    )
    device = models.OneToOneField('core.Device', related_name='device_settings')
    gps = models.BooleanField(default=True)
    wifi = models.BooleanField(default=True)
    bluetooth = models.BooleanField(default=False)
    mobile_data = models.BooleanField(default=True)
    screen_orientation = models.IntegerField(choices=ORIENTATION_CHOICES, default=AUTO_ORIENTATION)

    def save(self, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)

    def __str__(self):
        screen_orientation = self.screen_orientation
        if screen_orientation == self.AUTO_ORIENTATION:
            orientation = "Auto"
        elif screen_orientation == self.PORTRAIT_ORIENTATION:
            orientation = "Portrait"
        elif screen_orientation == self.LANDSCAPE_ORIENTATION:
            orientation = "Landscape"

        return mark_safe("<p>" + "Gps: " + str(self.gps) + "</br>"
                         + "Wifi: " + str(self.wifi) + "</br>"
                         + "Bluetooth: " + str(self.bluetooth) + "</br>"
                         + "Mobile Data: " + str(self.mobile_data) + "</br>"
                         + "Screen Orientation: " + orientation + "</br>"
                         + "</p>")
