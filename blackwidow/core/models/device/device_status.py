from blackwidow.core.models.contracts.base import DomainEntity
from django.db import models
from django.db import transaction

__author__ = 'zia ahmed'


class DeviceUpdateStatus(DomainEntity):

    UPDATED = 1
    NOT_UPDATED = 2

    device = models.OneToOneField('core.Device')
    status = models.CharField(max_length=1, default=2)

    def __str__(self):
        if self.status == str(self.UPDATED):
            return "Updated"
        elif self.status == str(self.NOT_UPDATED):
            return "Not Updated"

    def save(self, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)