from django.db import models, transaction
from blackwidow.core.models.contracts.base import DomainEntity
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, is_role_context, is_object_context, save_audit_log
from config.enums.modules_enum import ModuleEnum


__author__ = 'zia ahmed'

# @decorate(is_role_context, is_object_context, save_audit_log,
#           route(route='device-categories', group='Device', module=ModuleEnum.DeviceManager, display_name='Device Category'))
class DeviceCategory(OrganizationDomainEntity):
    name = models.CharField(max_length=200,null=True,blank=True)

    def __str__(self):
        return self.name

    def get_choice_name(self):
        return self.code + ": "+self.name

    def save(self, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)