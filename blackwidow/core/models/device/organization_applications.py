from django.db import transaction
from blackwidow.core.models.contracts.base import DomainEntity
from django.db import models
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import is_object_context, decorate, save_audit_log
from config.enums.modules_enum import ModuleEnum

__author__ = 'zia ahmed'


# @decorate(is_object_context, save_audit_log,
#           route(route='fieldbuzz-applications', group='Application', module=ModuleEnum.DeviceManager, display_name='Fieldbuzz Applicaiton'))
class FieldBuzzApplication(OrganizationDomainEntity):

    INSTALL = 1
    UPDATE = 2

    STATUS_CHOICES = (
        (INSTALL, 'Install'),
        (UPDATE, 'Update'),
    )

    name = models.CharField(max_length=200)
    package_name = models.CharField(max_length=200, null=True)
    version_name = models.CharField(max_length=200, null=True)
    version_code = models.IntegerField()
    status = models.IntegerField(choices=STATUS_CHOICES, default=INSTALL)
    file = models.ForeignKey('core.ApplicationFileObject', null=True, default=None)
    hashcode = models.CharField(max_length=5000, blank=True, editable=False)

    def get_choice_name(self):
        return self.code+": "+self.name

    @classmethod
    def hashcode_generator(cls, file_content):
        import hashlib
        hashcode = hashlib.md5(file_content.read()).hexdigest()
        return hashcode

    @property
    def download(self):
        apk_url = '/static-media/fieldbuzz_applications'
        s = str(self.file)
        try:
            apk_name = s.split('/static_media/fieldbuzz_applications/')[1]
            return apk_url + '/' + apk_name
        except:
            return ""

    @property
    def relative_url(self):
        return str(self.file.relative_url)

    def save(self, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)
            file_content = self.file.file
            hash_code = FieldBuzzApplication.hashcode_generator(file_content)
            self.hashcode = hash_code
            super().save(*args, **kwargs)

