from django.db import transaction
from blackwidow.core.models.contracts.base import DomainEntity
from django.db import models
from decimal import Decimal
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from config.enums.modules_enum import ModuleEnum
from django.utils.safestring import mark_safe

__author__ = 'zia ahmed'

@decorate(route(route='device-location', group='Device', module=ModuleEnum.DeviceManager, display_name='Device Location', hide=True))
class DeviceLocation(DomainEntity):
    device = models.OneToOneField('core.Device', related_name='device_location')
    location_flag = models.BooleanField(default=False)
    latitude = models.DecimalField(max_digits=20, decimal_places=10, default=Decimal("000.00"), null=True, blank=True)
    longitude = models.DecimalField(max_digits=20, decimal_places=10, default=Decimal("000.00"), null=True, blank=True)
    accuracy = models.DecimalField(max_digits=20, decimal_places=10, default=0.0, null=True, blank=True)

    def save(self, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)

    def __str__(self):
        if self.latitude == 0 or self.longitude == 0:
            return "N/A"
        return mark_safe('<a class="inline-link" href="http://maps.google.com/maps?z=17&q='
                         + str(self.latitude) + ','
                         + str(self.longitude) + '" target="_blank">'
                         + str(self.latitude) + ', ' + str(self.longitude) + '</a>')