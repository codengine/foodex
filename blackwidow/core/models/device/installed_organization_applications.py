from blackwidow.core.models.contracts.base import DomainEntity
from django.db import models
from django.db import transaction
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity

__author__ = 'zia ahmed'


class InstalledFieldbuzzApplication(OrganizationDomainEntity):
    device = models.ForeignKey('core.Device')
    application_name = models.CharField(max_length=200)
    package_name = models.CharField(max_length=200, null=True, blank=True)
    version_name = models.CharField(max_length=200, null=True, blank=True)
    version_code = models.IntegerField()

    def save(self, organization=None, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)


    @classmethod
    def table_columns(cls):
        return "code", "application_name:Name", "package_name", "version_name", "version_code", "date_created:Created On", "last_updated"


