from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.common.location import Location
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from rest_framework import serializers
from django.db import models

__author__ = 'activehigh'


@decorate(save_audit_log, is_object_context, expose_api('visit-client'),
          route(route="visit-client", module=ModuleEnum.Execute, group_order=2, item_order=5, group="Secondary Sales", display_name="Visit Retailer"))
class VisitClient(OrganizationDomainEntity):
    client = models.ForeignKey(Client, null=True)
    location = models.OneToOneField(Location)
    visit_time = models.BigIntegerField(default=0)
    description = models.CharField(max_length=8000, default='')

    @property
    def render_distributor(self):
        try:
            distributor = self.created_by.infrastructureuserassignment_set.all().first().client_set.all().first()
            return mark_safe("<a class='inline-link' href='" + reverse(distributor.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.distributor.pk}) + "' >" + str(self.distributor) + "</a>")
        except:
            return ""

    @classmethod
    def filter_query(cls,query_set,custom_search_fields=[]):
        from django.db.models import Q
        from functools import reduce
        import operator
        from foodex.models import Route, Retailers
        for key,value in custom_search_fields:
            if key.startswith("__search__distributors"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(parent_client__name=v))
                    retailers_pk = []
                    routes = Route.objects.filter(reduce(operator.or_, or_list))
                    for route in routes:
                        client_assigns = route.assigned_clients.filter(client_type=Retailers.__name__)
                        if client_assigns.exists():
                            retailers_pk += client_assigns.first().clients.values_list('pk', flat=True)
                    query_set = query_set.filter(client__pk__in=retailers_pk)
                except:
                    pass
        return query_set

    @classmethod
    def get_url_by_name(cls, name):
        if name == 'render_distributor':
            return '__search__distributors'
        return None

    @classmethod
    def table_columns(cls):
        return 'code', 'client:Retailer', 'created_by:Visited By', 'render_distributor', 'location', 'visit_time', 'last_updated:Sync Time'
        # return 'code', 'client', 'description', 'location', 'last_updated'

    @classmethod
    def exclude_search_fields(cls):
        return [
            "render_distributor", "location", "visit_time"
        ]

    @classmethod
    def get_manage_buttons(cls):
        return []

    def details_link_config(self, **kwargs):
        return []

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'location', 'visit_time']
        for key in d:
            if not key in custom_list:
                del d[key]
        d['Retailer'] = self.client
        d['Visited By'] = self.created_by
        d['Distributor'] = self.render_distributor
        d['Location'] = self.location
        d['Visit Time'] = self.visit_time
        d['Sync Time'] = self.last_updated

        return d

    @classmethod
    def default_order_by(cls):
        return "-code"

    @classmethod
    def get_datetime_fields(cls):
        return super().get_datetime_fields() + ['visit_time']

    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class Serializer(ss):
            client = serializers.PrimaryKeyRelatedField(required=False, queryset=Client.objects.all())
            location = Location.get_serializer()()
            description = serializers.CharField(required=False)
            visit_time = serializers.IntegerField(required=False)

            class Meta(ss.Meta):
                model = cls

        return Serializer
