from blackwidow.core.generics.views.details_view import GenericDetailsView
from blackwidow.sales.models.orders.completed_order import CompletedOrder

__author__ = 'sifat'


class GenericPrintableContentView(GenericDetailsView):
    template_name = 'shared/printable_details.html'

    def get_template_names(self):
        template_names = super().get_template_names()
        if self.model.__name__ == CompletedOrder.__name__:
            template_names = ['shared/invoice.html']
        return template_names