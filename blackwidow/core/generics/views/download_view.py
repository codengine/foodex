import os

from django.shortcuts import redirect

from blackwidow.core.generics.views.list_view import GenericListView
from blackwidow.core.managers.archivemanager import ArchiveManager
from blackwidow.core.models.file.exportfileobject import ExportFileObject
from blackwidow.engine.extensions.clock import Clock
from settings import EXPORT_FILE_ROOT


class GenericDownloadView(GenericListView):
    model_name = ExportFileObject

    def get(self, request, *args, **kwargs):
        models = list()
        path = os.path.join(EXPORT_FILE_ROOT)
        if not os.path.exists(path):
            os.makedirs(path)
        download_url = '/static-media/exported-files/'
        if request.GET.get("names"):
            name_split = request.GET["names"].split(",")
            for name in name_split:
                export_file_objects = ExportFileObject.objects.filter(name=name)
                for export_file_object in export_file_objects:
                    export_file_object = export_file_objects.first()
                    if not export_file_object in models:
                        models += [export_file_object]
        else:
            if "pks" in kwargs:
                ids = kwargs["pks"].split(',')
                for _id in ids:
                    models += ExportFileObject.objects.filter(id=_id)
        if len(models) > 1:
            file_name = str(Clock.timestamp())
            ArchiveManager.zip_files([(x.path, x.name + x.extension) for x in models], os.path.join(path ,file_name))
            file_to_download = download_url + file_name + '.zip'
        else:
            file_name = models[0].name + models[0].extension
            file_to_download = download_url + file_name
        return redirect(file_to_download)