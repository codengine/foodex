from django.views.generic.base import TemplateView

from blackwidow.core.mixins.viewmixin.protected_view_mixin import ProtectedViewMixin
from blackwidow.engine.mixins.viewmixin.json_view_mixin import JsonMixin


__author__ = 'ActiveHigh'


class GenericErrorView(JsonMixin, TemplateView):
    template_name = "shared/error.html"
    model = None


class GenericInfoView(ProtectedViewMixin, TemplateView):
    template_name = "shared/work_in_progress.html"
    model = None