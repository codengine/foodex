from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.db.models.loading import get_model
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.detail import DetailView

from blackwidow.core.managers.contextmanager import ContextManager
from blackwidow.core.mixins.viewmixin.protected_view_mixin import ProtectedViewMixin
from blackwidow.engine.exceptions.exceptions import NotEnoughPermissionException
from blackwidow.engine.managers.bwpermissionmanager import BWPermissionManager
from config.apps import INSTALLED_APPS

__author__ = 'mahmudul'

class GenericDetailsView(ProtectedViewMixin, DetailView):
    template_name = ""
    success_url = "/"
    model_name = ''
    model = None
    object = None

    @method_decorator(login_required)
    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        if self.model is not None:
            if not BWPermissionManager.has_view_permission(self.request, self.model):
                user = ContextManager.get_current_user(self.request)
                if 'pk' in kwargs and not user['id'] == int(kwargs.get("pk", "0")) \
                        and self.model.__name__ != 'ConsoleUser':
                    raise NotEnoughPermissionException("You do not have enough permission to view this item.")

                if 'id' in kwargs and not user['id'] == int(kwargs.get("id", "0")) \
                        and self.model.__name__ != 'ConsoleUser':
                    raise NotEnoughPermissionException("You do not have enough permission to view this item.")
        return super(ProtectedViewMixin, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):

        def is_proxy(model):
            if 'proxy' in model._meta.__dict__:
                return model._meta.__dict__.get('proxy')

        try:
            obj = super(GenericDetailsView, self).get_object(queryset)
            return obj
        except:
            max_step = 20
            parent_found = False
            parent_model = self.model
            current_step = 1
            while not parent_found and current_step < max_step:
                parent_model = parent_model.__base__
                if is_proxy(parent_model):
                    current_step += 1
                    continue
                else:
                    parent_found = True
            parent_object = parent_model.objects.get(pk=self.kwargs[self.pk_url_kwarg])
            target_model_type = parent_object.type
            target_object = None

            app_models = reversed(INSTALLED_APPS)
            for app in app_models:
                try:
                    app = app[app.rindex('.') + 1:] if '.' in app else app
                    target_object = get_model(app, target_model_type).objects.get(pk=self.kwargs[self.pk_url_kwarg])
                    break
                except Exception as exp:
                    pass

            if not target_object:
                raise ObjectDoesNotExist("Object does not exist with this id: %s" % self.kwargs[self.pk_url_kwarg])

            return target_object



    def get_context_data(self, **kwargs):
        context = super(GenericDetailsView, self).get_context_data(**kwargs)
        context['data'] = self.object

        if any(filter(lambda x: x.__name__ == 'is_object_context', self.model._decorators)):
            context['details_link_config'] = self.details_link_config()
        else:
            context['details_link_config'] = self.object.details_link_config(user=self.request.c_user)

        # context['details_link_config'] = self.object.details_link_config(user=self.request.c_user)
        # context['details_link_config'] = self.details_link_config()

        context['model_meta']['tabs'] = self.object.tabs_config
        context['model_meta']['properties'] = self.object.details_config
        return context

    def get_template_names(self):
        if self.template_name != '':
            return [self.template_name]
        self.model_name = self.model_name if self.model_name else self.model.__name__.lower()
        return [
            self.model_name + '/details.jinja',
            self.model_name + '/details.html',
            "shared/display-templates/details.jinja",
            "shared/display-templates/details.html"]

    def render_to_response(self, context, *args, **response_kwargs):
        if self.request.GET.get('format', 'html') == 'json':
            data = self.json_serialize_object(context['data'])
            return self.render_json_response(data)
        else:
            if 'template_name' in response_kwargs:
                return self.response_class(
                    request=self.request,
                    template=response_kwargs['template_name'],
                    context=context,
                    **response_kwargs
                )

            return self.response_class(
                request=self.request,
                template=self.get_template_names(),
                context=context,
                **response_kwargs
            )
