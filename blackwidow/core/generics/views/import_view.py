from multiprocessing.synchronize import Lock
import os
from threading import Thread

from django.db import transaction
from django.db.models.loading import get_model
from django.shortcuts import redirect

from blackwidow.core.generics.views.list_view import GenericListView
from blackwidow.core.models.config.importer_config import ImporterConfig
from blackwidow.core.models.log.logs import ErrorLog
from blackwidow.core.generics.importer.generic_importer import GenericImporter
from blackwidow.core.models.config.importer_lock import ImporterLock
from blackwidow.core.models.queue.import_queue import ImportFileQueue
from blackwidow.core.models.queue.queue import FileQueueEnum
from blackwidow.core.signals.signals import import_completed


__author__ = 'Mahmud'


class GenericImportRunnerView(GenericListView):
    @classmethod
    def run_import(cls, queue, lock_object, request):
        user = request.c_user
        try:
            model = get_model(queue.model.split('.')[0], queue.model.split('.')[1])
            importer_config = model.importer_config(organization=user.organization)
            imported_items = GenericImporter.import_from_excel(filename=os.path.abspath(queue.file.file.path), model=model, user=queue.created_by, importer_config=importer_config, request=request)
            import_completed.send(model, items=imported_items, user=queue.created_by, organization=user.organization)
            model.post_processing_import_completed(items=imported_items, user=queue.created_by, organization=user.organization)
            queue.status = FileQueueEnum.COMPLETED
        except Exception as exp:
            ErrorLog.log(exp, organization=user.organization)
            queue.status = FileQueueEnum.ERROR
        with transaction.atomic():
            lock_object.is_locked = False
            lock_object.save()
            queue.save()

    def start_background_worker(self, request=None, **kwargs):
        queue = ImportFileQueue.objects.filter(status=FileQueueEnum.SCHEDULED, organization=request.c_user.organization)
        for q in queue:
            lock = Lock(ctx=None)
            lock.acquire(True)
            lock_object, result = ImporterLock.objects.get_or_create(model=q.model, organization=request.c_user.organization)
            if ~lock_object.is_locked:
                with transaction.atomic():
                    lock_object.is_locked = True
                    lock_object.save()
                    q.status = FileQueueEnum.PROCESSING
                    q.save()
                    process = Thread(target=GenericImportRunnerView.run_import, args=(q, lock_object, request))
                    process.start()
            lock.release()

    def get(self, request, *args, **kwargs):
        self.start_background_worker(request=request, **kwargs)
        return redirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['import'] = True
        return context