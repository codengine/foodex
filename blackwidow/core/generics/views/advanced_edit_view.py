from django.http.response import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.template.context import Context
from blackwidow.core.generics.views.edit_view import GenericEditView

__author__ = 'Sohel'

class AdvancedGenericEditView(GenericEditView):
    def get_template_names(self):
        return ['shared/display-templates/_advanced_edit_form.html']

    def get_context_data(self, **kwargs):
        advanced_edit_form = self.model.get_advanced_edit_form()(**kwargs) if self.model.get_advanced_edit_form() else None
        context = Context({ "form": advanced_edit_form })
        return context

    def get_form_class(self):
        return self.model.get_advanced_edit_form()

    def get(self, request, *args, **kwargs):
        template = loader.get_template(self.get_template_names()[0])
        context = Context(self.get_context_data(**{ "object_id": request.GET.get("object_id") }))
        return HttpResponse(template.render(context))

    def get_success_url(self):
        return "/"+self.model.get_model_meta('route', 'route')+"/"

    def post(self, request, *args, **kwargs):
        advanced_edit_form = self.model.get_advanced_edit_form()(request.POST) if self.model.get_advanced_edit_form() else None
        if advanced_edit_form.is_valid():
            advanced_edit_form.save()
        return HttpResponseRedirect(self.get_success_url())
