__author__ = "auto generated"

from blackwidow.core.generics.views.list_view import *
from blackwidow.core.generics.views.chart_data_view import * 
from blackwidow.core.generics.views.create_view import * 
from blackwidow.core.generics.views.details_view import * 
from blackwidow.core.generics.views.error_view import *


__all__ = ['GenericListView']
__all__ += ['GenericChartDataView']
__all__ += ['GenericCreateView']
__all__ += ['GenericDetailsView']
__all__ += ['GenericErrorView']
