__author__ = 'Machine II'

from datetime import datetime
import os

from openpyxl import Workbook

from blackwidow.core.models.file.exportfileobject import ExportFileObject
from blackwidow.core.models.log.logs import ErrorLog
from blackwidow.core.models.organizations.organization import Organization
from blackwidow.engine.extensions.clock import Clock
from settings import EXPORT_FILE_ROOT

class GenericDownloader(object):
    success_url = '/'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @classmethod
    def download_to_excel(cls, queryset=None, model=None, filename=None, user=None, **kwargs):
        downloadable_item = []
        path = os.path.join(EXPORT_FILE_ROOT)
        if not os.path.exists(path):
            os.makedirs(path)
        wb = Workbook(encoding='utf-8')
        if filename is None:
            dttime = datetime.fromtimestamp(Clock.timestamp(_format='s')).strftime('%d%m%Y_%H%M%S')
            dest_filename = str(model.__name__)+'_'+ dttime
        else:
            dest_filename = filename

        file_path = path + os.sep + dest_filename + '.xlsx'
        ws = wb.active
        ws.title = str(model.__name__)

        ws, row_number = queryset.model.initialize_download(workbook=ws, row_number=1, query_set=queryset, **kwargs)

        row_number = 2
        for obj in queryset:
            try:
                _pk, row_number = obj.download_item(workbook=ws, row_number=row_number, **kwargs)
                downloadable_item.append(_pk)
            except Exception as exp:
                ErrorLog.log(exp)
        response = queryset.model.finalize_download(workbook=ws, row_number=row_number, query_set=queryset, **kwargs)
        if type(response) is tuple:
            ws = response[0]
            file_name = response[1]
        else:
            ws = response
            file_name = None
        # if file_name is None:
        #     dest_filename = str(queryset.model.__name__)+'_'+ user.code +'_'+str(Clock.timestamp(_format='ms'))
        # else:
        dest_filename = filename
        generated_file_name = str(dest_filename) + '.xlsx'
        file_path = path + os.sep + generated_file_name
        ws.title = str(model.__name__)
        wb.save(filename=file_path)
        export_file_object = ExportFileObject()
        export_file_object.path = file_path
        export_file_object.name = dest_filename
        export_file_object.file = file_path
        export_file_object.extension = '.xlsx'
        export_file_object.organization = Organization.objects.all().first()
        export_file_object.save()

        return dest_filename, file_path
