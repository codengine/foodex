from django.contrib.contenttypes.models import ContentType
from django.apps import apps
from django.db import transaction
from django.db.models.query_utils import Q

from blackwidow.core.generics.views.partial_views.partial_tab_list_view import PartialGenericTabListView
from blackwidow.core.models.permissions.modulepermission_assignment import ModulePermissionAssignment
from blackwidow.core.models.permissions.rolepermission_assignment import RolePermissionAssignment
from blackwidow.core.models.roles.role import Role
from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from blackwidow.engine.exceptions.exceptions import NotEnoughPermissionException
from blackwidow.engine.managers.bwpermissionmanager import BWPermissionManager
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'Mahmud'


@decorate(override_view(model=Role, view=ViewActionEnum.Tab))
class PartialRoleTabListView(PartialGenericTabListView):

    def post(self, request, *args, pk=0, **kwargs):
        if not BWPermissionManager.has_view_permission(self.request, self.model):
            raise NotEnoughPermissionException("You do not have enough permission to view this content.")

        model = self.model.objects.filter(id=int(pk))[0]
        tab = [x for x in model.tabs_config if x.access_key == kwargs['tab']][0]
        for key in request.POST:
            with transaction.atomic():
                if tab.access_key == "top_menus":
                    items = tab.get_queryset().filter(module__name=key)
                elif tab.access_key == "modules":
                    items = tab.get_queryset().filter(module__name=key)
                elif tab.access_key == "permissions":
                    items = tab.get_queryset().filter(permission__context=key)
                if items.exists():
                    item = items.first()
                    values = request.POST.get(key, None)
                    if values is not None:
                        _v = max([int(v) for v in values.split(',')])
                        if item.access == _v:
                            continue
                        item.access = _v
                        item.save()
                        if tab.access_key == "top_menus":
                            self.sub_module_permission_update(role=model, parent_module_permission=item, access=_v)
                        elif tab.access_key == "modules":
                            self.object_permission_update(role=model, module_permission=item, access=_v)
                else:
                    pass
        return self.get(request, *args, pk=pk, **kwargs)

    def sub_module_permission_update(self, role=None, parent_module_permission=None, access=0):
        if role is not None and parent_module_permission is not None:
            sub_module_permissions = ModulePermissionAssignment.objects.filter(
                Q(role__id=role.id) & Q(module__parent__name=parent_module_permission.module.name))
            if len(sub_module_permissions) < 1:
                self.object_permission_update(role=role, module_permission=parent_module_permission, access=access)
                return
            for sub_module_permission in sub_module_permissions:
                if sub_module_permission.access == access:
                    continue
                sub_module_permission.access = access
                sub_module_permission.save()
                self.object_permission_update(role=role, module_permission=sub_module_permission, access=access)

    def object_permission_update(self, role=None, module_permission=None, access=0):
        object_permissions = RolePermissionAssignment.objects.filter(Q(role__id=role.id))
        for object_model in object_permissions:
            try:
                model_name = object_model.permission.context
                model_objects = ContentType.objects.filter(model=model_name.lower())
                for model_object in model_objects:
                    model = apps.get_model(model_object.app_label, model_name)
                    group = model.get_model_meta('route', 'group')
                    if module_permission.module.name == group:
                        object_model.access = access
                        object_model.save()
            except:
                continue
