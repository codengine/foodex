__author__ = "auto generated"

from blackwidow.core.views.account.login_view import LoginView
from blackwidow.core.views.account.logout_view import LogoutView
from blackwidow.core.views.account.register_view import RegisterView
from blackwidow.core.views.account.reset_password_view import ResetPasswordRequestView, PasswordResetConfirmView
from blackwidow.core.views.models.error_details_view import ErrorDetailsView
from blackwidow.core.views.models.role_tab_list_view import PartialRoleTabListView
from blackwidow.core.views.search.list_view import SearchView
from blackwidow.core.views.shared.home_view import HomeView
from blackwidow.core.views.shared.settings_view import SettingsView
from blackwidow.core.views.users.consoleuser_reset_password_view import ConsoleUserResetPasswordView


__all__ = ['LogoutView']
__all__ += ['RegisterView']
__all__ += ['HomeView']
__all__ += ['LoginView']
__all__ += ['SettingsView']
__all__ += ['ResetPasswordRequestView']
__all__ += ['PasswordResetConfirmView']
__all__ += ['ConsoleUserResetPasswordView']
__all__ += ['PartialRoleTabListView']
__all__ += ['SearchView']
__all__ += ['ErrorDetailsView']
