from crequest.middleware import CrequestMiddleware
from django.contrib import messages
from django.shortcuts import redirect
from django import http

from blackwidow.core.managers.contextmanager import ContextManager
from blackwidow.core.models.log.logs import ErrorLog
from blackwidow.engine.encoders.bw_json_encoder import DynamicJsonEncoder
from blackwidow.engine.exceptions.exceptions import NotEnoughPermissionException
from blackwidow.core.models.common.sessionkey import SessionKey
from blackwidow.core.models.users.user import ConsoleUser


__author__ = 'mahmudul'


class ProtectedFormPostMiddleWare(object):
    def is_json_request(self, request):
        if request.GET.get('format', None) == 'json' \
                or request.POST.get('format', None) == 'json':
            return True
        return False

    def has_authkey(self, request):
        if request.GET.get('authkey', None) is not None:
            return True
        if request.POST.get('authkey', None) is not None:
            return True
        return False

    def get_authkey(self, request):
        if request.GET.get('authkey', None) is not None:
            return request.GET.get('authkey', None)
        if request.POST.get('authkey', None) is not None:
            return request.POST.get('authkey', None)
        return False

    def process_exception(self, request, exception):
        if hasattr(request, 'c_user'):
            ErrorLog.log(exception, organization=request.c_user.organization)
        message = str(exception)

        if self.is_json_request(request) \
                or request.is_ajax() \
                or ('CONTENT_TYPE' in request.META and request.META['CONTENT_TYPE'] == 'application/json'):
            encoder = DynamicJsonEncoder()
            return http.HttpResponse(encoder.encode({
                'message': message,
                'success': False
            }), content_type='application/json')

        if exception.__class__ == NotEnoughPermissionException:
            return redirect("/no-access")

        messages.error(request, message)
        if request.META['PATH_INFO'] != '/error':
            return redirect('/error')

    def process_request(self, request):
        if request.META['PATH_INFO'] != '/error':
            if not request.user.is_anonymous():
                if ~hasattr(request, 'c_user'):
                    c_user = ConsoleUser.objects.filter(user__id=request.user.id)[0]
                    ContextManager.initialize_context(request, {'user': c_user, 'org': c_user.organization})
                    CrequestMiddleware.set_request(request)
            elif self.is_json_request(request) and self.has_authkey(request):  #legacy support
                authkey = self.get_authkey(request)
                sess_key = SessionKey.objects.filter(ses_key=authkey)[0]
                c_user = sess_key.user
                ContextManager.initialize_context(request, {'user': c_user, 'org': c_user.organization})
