from blackwidow.core.api.models.news.news_viewmodel import NewsViewModel
from blackwidow.core.api.serializers.newsviewmodel_serializer import NewsViewModelSerializer
from blackwidow.core.api.mixins.viewsetmixin.viewset_mixin import GenericApiReadOnlyViewSetMixin

__author__ = 'Mahmud'


class NewsViewSet(GenericApiReadOnlyViewSetMixin):
    serializer_class = NewsViewModelSerializer

    def get_queryset(self, **kwargs):
        return [NewsViewModel(request=self.request, **kwargs)]
