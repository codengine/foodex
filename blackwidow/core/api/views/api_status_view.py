from django.core.urlresolvers import Resolver404
from django.db.models.aggregates import Max
from rest_framework.response import Response
from rest_framework.views import APIView

from blackwidow.core.api.athorization.is_authorized import IsAuthorized
from blackwidow.core.api.athorization.token_authentication import BWTokenAuthentication
from blackwidow.core.api.renderers.generic_renderer import GenericJsonRenderer
from blackwidow.core.models.file.imagefileobject import ImageFileObject
from blackwidow.core.models.log.audit_log import DeleteLog
from blackwidow.core.models.log.logs import ApiCallLog
from blackwidow.engine.decorators.utility import get_models_with_decorator
from config.apps import INSTALLED_APPS
from blackwidow.core.models.track_change.TrackModelM2MChangeModel import TrackModelM2MChangeModel
from blackwidow.engine.extensions.clock import Clock


__author__ = 'Mahmud'


class ApiStatusView(APIView):
    authentication_classes = (BWTokenAuthentication, )
    permission_classes = (IsAuthorized,)
    renderer_classes = (GenericJsonRenderer, )

    def finalize_response(self, request, response, *args, **kwargs):
        ApiCallLog.log(request=request, response=response)
        return super().finalize_response(request, response, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        data = dict()
        models = get_models_with_decorator('save_audit_log', INSTALLED_APPS, include_class=True)
        m2m_tracking_models = get_models_with_decorator('enable_m2m_tracking', INSTALLED_APPS, include_class=True)
        for m2m_model in m2m_tracking_models:
            if not m2m_model in models:
                models += [m2m_model]
        m_models = get_models_with_decorator('is_profile_content', INSTALLED_APPS, include_class=True)
        for m in models:
            if m in m_models:
                try:
                    if m.objects.exists():
                        data[m.__name__.lower()] = m.get_queryset(queryset=m.objects.all(), user=request.c_user).aggregate(last_updated=Max('last_updated'))
                    else:
                        data[m.__name__.lower()] = dict(
                            last_updated=0
                        )
                    # data[m.__name__.lower()]['deleted'] = [x['model_pk'] for x in DeleteLog.objects.filter(model_name=m.__name__).values('model_pk')]
                except Resolver404 as exp:
                    pass
            else:
                try:
                    if m.objects.exists():
                        data[m.__name__.lower()] = m.objects.aggregate(last_updated=Max('last_updated'))
                        m2m_objs = TrackModelM2MChangeModel.objects.filter(model_name=m.__name__)
                        if m2m_objs.exists():
                            m2m_change_time = m2m_objs.first().modified_time
                            if m2m_change_time > data[m.__name__.lower()]:
                                data[m.__name__.lower()] = m2m_change_time
                    else:
                        data[m.__name__.lower()] = dict(
                            last_updated=0
                        )
                    one_month = 30 * 24 * 60 * 60 * 1000
                    data[m.__name__.lower()]['deleted'] = [x['model_pk'] for x in DeleteLog.objects.filter(model_name=m.__name__,date_created__gte=(Clock.timestamp() - (2 * one_month))).values('model_pk')]
                    _max_delete = DeleteLog.objects.filter(model_name=m.__name__).aggregate(last_updated=Max('last_updated'))['last_updated']
                    if _max_delete is None:
                        _max_delete = 0
                    if _max_delete > data[m.__name__.lower()]['last_updated']:
                        data[m.__name__.lower()]['last_updated'] = _max_delete
                except Resolver404 as exp:
                    pass
        if not request.c_user.image:
            request.c_user.image = ImageFileObject.objects.filter().first()
        data['logged_in_information'] = request.c_user.to_json()
        return Response({'data': data, 'success': True})