from django.db import transaction
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response

from blackwidow.core.api.serializers.auth_token_serializer import BWAuthTokenSerializer
from blackwidow.core.models.users.user import ConsoleUser


__author__ = 'Mahmud'


class ApiLoginView(ObtainAuthToken):
    serializer_class = BWAuthTokenSerializer

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    @csrf_exempt
    def post(self, request):
        serializer = self.serializer_class(data=request.DATA)
        if serializer.is_valid():
            user = ConsoleUser.objects.get(user=serializer.validated_data['user'])
            user.device_id = serializer.validated_data['device_id']
            with transaction.atomic():
                user.save()
                token, created = Token.objects.get_or_create(user=serializer.validated_data['user'])
                # ApiCallLog.log(url=token.user.username + ": " + request._request.META['PATH_INFO'], type='POST', token=token.key)
                return Response({'token': token.key, 'success': True})
        return Response({'message': 'Cannot login with provided credentials.', 'success': False}, status=status.HTTP_400_BAD_REQUEST)