from rest_framework import serializers

from blackwidow.core.models.information.alert import Alert
from blackwidow.core.models.information.news import News
from blackwidow.core.models.information.notification import Notification


__author__ = 'Mahmud'


class NewsViewModelSerializer(serializers.Serializer):
    news = News.get_serializer()()
    alerts = Alert.get_serializer()()
    notifications = Notification.get_serializer()()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
