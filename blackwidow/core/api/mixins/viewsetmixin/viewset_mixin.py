from django.core.exceptions import ObjectDoesNotExist
from django.db.models.loading import get_model
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import MultiPartParser, FormParser, JSONParser
from rest_framework.renderers import BrowsableAPIRenderer
from rest_framework import viewsets

from blackwidow.core.api.athorization.is_authorized import IsAuthorized
from blackwidow.core.api.athorization.token_authentication import BWTokenAuthentication
from blackwidow.core.api.mixins.viewsetmixin.approve_view_set import GenericApiApproveViewSetMixin
from blackwidow.core.api.mixins.viewsetmixin.mutation_view_set import GenericApiMutableViewSetMixin
from blackwidow.core.api.mixins.viewsetmixin.reject_view_set import GenericApiRejectViewSetMixin
from blackwidow.core.api.renderers.generic_renderer import GenericJsonRenderer
from blackwidow.core.mixins.viewmixin.protected_queryset_mixin import ProtectedQuerySetMixin
from blackwidow.core.models.log.logs import ApiCallLog
from config.apps import INSTALLED_APPS

__author__ = 'Mahmud'


class GenericApiViewSetMixin(ProtectedQuerySetMixin, GenericApiMutableViewSetMixin,GenericApiApproveViewSetMixin, GenericApiRejectViewSetMixin, viewsets.ModelViewSet):
    authentication_classes = (BWTokenAuthentication, )
    permission_classes = (IsAuthorized,)
    renderer_classes = (GenericJsonRenderer, BrowsableAPIRenderer)
    parser_classes = (MultiPartParser, FormParser, JSONParser, )

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def finalize_response(self, request, response, *args, **kwargs):
        ApiCallLog.log(request=request, response=response)
        return super().finalize_response(request, response, *args, **kwargs)

    def metadata(self, request):
        ret = super().metadata(request)
        # ret['fields'] = self.model.serialize_fields()
        return ret

    def update(self, request, *args, partial=True, **kwargs):
        return super().update(request, *args, partial=partial, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    def options(self, request, *args, **kwargs):
        return super().options(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    def get_object(self):
        def is_proxy(model):
            if 'proxy' in model._meta.__dict__:
                return model._meta.__dict__.get('proxy')

        try:
            obj = super(GenericApiViewSetMixin, self).get_object()
            return obj
        except:
            max_step = 20
            parent_found = False
            parent_model = self.model
            current_step = 1
            while not parent_found and current_step < max_step:
                parent_model = parent_model.__base__
                if is_proxy(parent_model):
                    current_step += 1
                    continue
                else:
                    parent_found = True
            parent_object = parent_model.objects.get(pk=self.kwargs[self.lookup_field])
            target_model_type = parent_object.type
            target_object = None

            app_models = reversed(INSTALLED_APPS)
            for app in app_models:
                try:
                    app = app[app.rindex('.') + 1:] if '.' in app else app
                    target_object = get_model(app, target_model_type).objects.get(pk=self.kwargs[self.lookup_field])
                    break
                except Exception as exp:
                    pass

            if not target_object:
                # raise ObjectDoesNotExist("Object does not exist with this id: %s" % self.kwargs[self.lookup_field])
                return parent_object

            return target_object

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        # response.data['fields'] = self.model.serialize_fields()
        # response.data['fields_hierarchical'] = SortedDict(
        #     [(field_name, field.metadata())
        #      for field_name, field in six.iteritems(self.model._meta.fields)]
        # )
        return response




class GenericApiReadOnlyViewSetMixin(ProtectedQuerySetMixin, viewsets.ReadOnlyModelViewSet):
    authentication_classes = (BWTokenAuthentication, )
    permission_classes = (IsAuthorized,)
    renderer_classes = (GenericJsonRenderer, BrowsableAPIRenderer)

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def finalize_response(self, request, response, *args, **kwargs):
        ApiCallLog.log(request=request, response=response)
        return super().finalize_response(request, response, *args, **kwargs)
