from blackwidow.core.models.log.logs import ErrorLog

__author__ = 'Mahmud'

from rest_framework.views import exception_handler


def BWApiExceptionHandler(exc):
    ErrorLog.log(exc)
    response = exception_handler(exc)
    if response is not None:
        response.data['status_code'] = response.status_code

    return response