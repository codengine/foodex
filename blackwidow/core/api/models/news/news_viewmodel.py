from blackwidow.core.models.information.alert import Alert
from blackwidow.core.models.information.news import News
from blackwidow.core.models.information.notification import Notification

__author__ = 'Mahmud'


class NewsViewModel(object):
    alerts = Alert.objects.none()
    notifications = Notification.objects.none()
    news = News.objects.none()

    def __init__(self, request=None, **kwargs):
        super().__init__()

        user = request.c_user
            
        self.alerts = Alert.apply_search_filter(request, queryset=Alert.get_queryset(queryset=Alert.objects.all(), user=user), **kwargs)
        self.notifications = Notification.apply_search_filter(request, queryset=Notification.get_queryset(queryset=Notification.objects.all(), user=user), **kwargs)
        self.news = News.apply_search_filter(request, queryset=News.get_queryset(queryset=News.objects.all(), user=user), **kwargs)
        # self.kpi = KeyPerformanceIndex.apply_search_filter(request, queryset=KeyPerformanceIndex.get_queryset(queryset=KeyPerformanceIndex.objects.all(), user=user), **kwargs)