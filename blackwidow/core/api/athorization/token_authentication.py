from crequest.middleware import CrequestMiddleware
from rest_framework import exceptions
from rest_framework.authentication import TokenAuthentication, get_authorization_header
from rest_framework.exceptions import APIException

from blackwidow.core.managers.contextmanager import ContextManager
from blackwidow.core.models.users.user import ConsoleUser


__author__ = 'Mahmud'


class BWTokenAuthentication(TokenAuthentication):
    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or len(auth) == 0 or auth[0].lower() != b'token':
            return None

        if len(auth) <= 1:
            msg = 'Invalid token header. No credentials provided.'
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = 'Invalid token header. Token string should not contain spaces.'
            raise exceptions.AuthenticationFailed(msg)

        try:
            auth_user, token = self.authenticate_credentials(auth[1])
            c_user = ConsoleUser.objects.get(user=auth_user)
            ContextManager.initialize_context(request, {'user': c_user, 'org': c_user.organization})
            CrequestMiddleware.set_request(request)
        except Exception as exp:
            raise APIException(str(exp))
        return (auth_user, token)