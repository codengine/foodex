__author__ = 'ruddra'
from django import forms
from rest_framework import serializers

def genarate_form_field(type):

    '''
    ('char', 'Character Field'),
    ('int', 'Integer Field'),
    ('big_int', 'BigIntegerField Field'),
    ('bool', 'Boolean Field'),
    ('decimal', 'DecimalField Field'),
    ('email', 'Email Field'),
    ('float', 'FloatField Field'),
    ('text', 'TextField Field'),
    ('url', 'URLField Field'),
'''

    if type == 'datetime':
        return forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M:%S', '%Y/%m/%d %H:%M:%S'], widget=forms.DateTimeInput(attrs={'data-format':"dd/MM/yyyy hh:mm:ss", 'class': 'date-time-picker'}, format='%d/%m/%Y %H:%M:%S'))

    elif type == 'int':
        return forms.IntegerField()

    elif type == 'big_int':
        return forms.IntegerField()

    elif type == 'bool':
        return forms.BooleanField()

    elif type == 'decimal':
        return forms.DecimalField()

    elif type == 'email':
        return forms.EmailField()

    elif type == 'float':
        return forms.FloatField()

    elif type == 'text':
        return forms.CharField(widget=forms.TextInput)

    elif type == 'url':
        return forms.URLField()

    else:
        #if x.field_type == 'char':
        return forms.CharField(max_length=8000, required= False)


def genarate_serializer_field(type):
    if type == 'datetime':
        return serializers.DateTimeField(input_formats=['%d/%m/%Y %H:%M:%S', '%Y/%m/%d %H:%M:%S'])
    elif type == 'int':
        return serializers.IntegerField()
    elif type == 'big_int':
        return serializers.IntegerField()
    elif type == 'bool':
        return serializers.BooleanField()
    elif type == 'decimal':
        return serializers.DecimalField()
    elif type == 'email':
        return serializers.EmailField()
    elif type == 'float':
        return serializers.FloatField()
    elif type == 'text':
        return serializers.CharField(widget=forms.TextInput)
    elif type == 'url':
        return serializers.URLField()
    else:
        return serializers.CharField(max_length=8000, required= False)
