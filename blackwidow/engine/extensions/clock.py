import time
import datetime

from django.utils import timezone
from crequest.middleware import CrequestMiddleware


now = timezone.now()

class Clock():
    '''
    System Clock
    '''
    def __init__(self):
        pass

    @classmethod
    def get_timestamp_from_date(cls, datetime_string=''):
        '''
        for getting timestamp from date use (datetimeobj).timestamp()*1000
        :param datetime_string:
        :return:
        '''
        return datetime.datetime.strptime(datetime_string, "%d/%m/%Y %H:%M:%S").timestamp() * 1000

    @classmethod
    def get_utc_timestamp_from_date(cls, datetime_string):
        return datetime.datetime.strptime(datetime_string, "%d/%m/%Y %H:%M:%S").replace(tzinfo=timezone.utc).timestamp() * 1000

    @staticmethod
    def now():
        # return datetime.datetime(2014, 12, 31)
        return timezone.now()

    @staticmethod
    def utcnow():
        # return datetime.datetime(2014, 12, 31)
        return datetime.datetime.utcnow()

    @staticmethod
    def timestamp(_format='ms'):
        if _format == 'ms':
            return int(time.time() * 1000)
        return int(time.time())

    @staticmethod
    def localTzname():
        if time.daylight:
            offsetHour = time.altzone / 3600
        else:
            offsetHour = time.timezone / 3600
        return 'Etc/GMT%+d' % offsetHour

    @staticmethod
    def get_utc_from_local_time(value=None):
        return datetime.datetime.utcfromtimestamp(value/1000)

    @staticmethod
    def get_user_local_time(value=None):
        request = CrequestMiddleware.get_request()
        user_tz_offset = request.c_tz_offset
        if value is None:
            return Clock.utcnow() - datetime.timedelta(minutes=int(user_tz_offset))
        else:
            time_value = datetime.datetime.utcfromtimestamp(value/1000)
            return time_value - datetime.timedelta(minutes=int(user_tz_offset))

    @staticmethod
    def get_user_universal_time(value=None):
        request = CrequestMiddleware.get_request()
        user_tz_offset = request.c_tz_offset
        if value is None:
            return Clock.utcnow() + datetime.timedelta(minutes=int(user_tz_offset))
        else:
            time_value = datetime.datetime.utcfromtimestamp(value/1000)
            return time_value + datetime.timedelta(minutes=int(user_tz_offset))


