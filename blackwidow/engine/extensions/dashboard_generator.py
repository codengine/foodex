from blackwidow.engine.decorators.utility import get_models_with_decorator
from blackwidow.sales.models.transactions.transaction import SecondaryTransaction
from config.apps import INSTALLED_APPS
from config.enums.modules_enum import ModuleEnum
from device_manager.models.device.devices import OrganizationDevice
from foodex.models.clients.pending_sec_retail import PendingRetailerAccount
from foodex.models.clients.retailers import Retailers
from foodex.models.infrastructure.routes import Route
from foodex.models.inventory.client_inventory import ClientInventory
from foodex.models.inventory.warehouse_inventory import WarehouseInventory
from foodex.models.invoice.open_invoice import OpenInvoice
from foodex.models.kpi.sales_kpi_monthly import SalesKPIMonthly
from foodex.models.transactions.completed_sales_transaction import CompletedSalesTransaction

__author__ = 'Imtiaz'


def dashboard_generator(module=None):
    all_models = get_models_with_decorator('route', INSTALLED_APPS, include_class=True)
    all_models = [x for x in all_models if not bool(x.get_model_meta('route', 'hide'))]

    #modules = [ModuleEnum.Reports, ModuleEnum.Alert, ModuleEnum.Targets, ModuleEnum.Execute, ModuleEnum.Administration, ModuleEnum.DeviceManager]

    modulesForDashboard = dict()
    modulesForDashboard[ModuleEnum.Administration.value['route']] = [Retailers, Route, PendingRetailerAccount]
    modulesForDashboard[ModuleEnum.Execute.value['route']] = [CompletedSalesTransaction, SecondaryTransaction, WarehouseInventory, ClientInventory, OpenInvoice]
    modulesForDashboard[ModuleEnum.Targets.value['route']] = [SalesKPIMonthly]
    modulesForDashboard[ModuleEnum.Alert.value['route']] = []
    modulesForDashboard[ModuleEnum.DeviceManager.value['route']] = [OrganizationDevice]

    return modulesForDashboard[module]

    # for _m in modules:
    #     if _m.value['route'] == module:
    #         return [x for x in all_models if x.get_model_meta('route', 'module') is not None and x.get_model_meta('route', 'module').value['route'] == module]
    # return []