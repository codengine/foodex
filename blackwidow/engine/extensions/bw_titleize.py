__author__ = 'mahmudul'

import re

BW_first_cap_re = re.compile('(.)([A-Z][a-z]+)')
BW_all_cap_re = re.compile('([a-z0-9])([A-Z])')


def bw_titleize(name):
    s1 = BW_first_cap_re.sub(r'\1_\2', name)
    return BW_all_cap_re.sub(r'\1_\2', s1).title().replace(r'_', r' ')