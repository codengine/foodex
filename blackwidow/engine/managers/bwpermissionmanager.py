from blackwidow.core.models.permissions.modulepermission_assignment import ModulePermissionAssignment
from blackwidow.core.models.permissions.rolepermission_assignment import RolePermissionAssignment
from config.constants.access_permissions import BW_ACCESS_CREATE_MODIFY, BW_ACCESS_MODIFY_ONLY, \
    BW_ACCESS_READ_ONLY, BW_ACCESS_CREATE_MODIFY_DELETE, BW_ACCESS_NO_ACCESS

__author__ = 'ActiveHigh'


class BWPermissionManager(object):
    @staticmethod
    def __has_permission(request, model, permission):
        all_objects = list(RolePermissionAssignment.objects.filter(role=request.c_user.role))
        if model is None or all(map(lambda x: x.__name__ != 'is_object_context', model._decorators)):
            return True

        for decorator in model._decorators:
            if decorator.__name__ == "is_object_context":
                if request.user is not None:
                    return any(filter(lambda x: x.permission.context == model.__name__ and x.access >= int(permission['value']), all_objects))
                return False

        _module = model.get_model_meta('route', 'module')
        _sub_modules = model.get_model_meta('route', 'group')

        all_modules = list(ModulePermissionAssignment.objects.filter(role=request.c_user.role))
        if any(filter(lambda x: x.module.name == _module.value['title'], all_modules)):
            module = list(filter(lambda x: x.module.name == _module.value['title'], all_modules))[0]
            if module.access < int(permission['value']):
                return False
            if isinstance(_sub_modules, (list, tuple)):
                for _s in _sub_modules:
                    if any(filter(lambda x: x.module.name == _s and x.module.parent is not None and x.module.parent.id == module.module.id and x.access < int(permission['value']), all_modules)):
                        return False
            else:
                if any(filter(lambda x: x.module.name == _sub_modules and x.module.parent is not None and x.module.parent.id == module.module.id and x.access < int(permission['value']), all_modules)):
                    return False
            return True
        return False

    @staticmethod
    def has_create_permission(request, model):
        return BWPermissionManager.__has_permission(request, model, BW_ACCESS_CREATE_MODIFY)

    @staticmethod
    def has_edit_permission(request, model):
        return BWPermissionManager.__has_permission(request, model, BW_ACCESS_MODIFY_ONLY)

    @staticmethod
    def has_view_permission(request, model):
        return BWPermissionManager.__has_permission(request, model, BW_ACCESS_READ_ONLY)

    @staticmethod
    def has_delete_permission(request, model):
        return BWPermissionManager.__has_permission(request, model, BW_ACCESS_CREATE_MODIFY_DELETE)

    @staticmethod
    def has_no_permission(request, model):
        return BWPermissionManager.__has_permission(request, model, BW_ACCESS_NO_ACCESS)
