from django.db.models.loading import get_app, get_models

__author__ = 'mahmudul'


def is_role_context(original_class):
    return original_class


def is_query_context(original_class):
    return original_class


def enable_wizard(original_class):
    return original_class


def save_audit_log(original_class):
    return original_class


def is_object_context(original_class):
    return original_class

def direct_delete(origina_class):
    return origina_class


def loads_initial_data(original_class):
    load_data = getattr(original_class, "load_initial_data", None)
    if not callable(load_data):
        raise Exception(str(original_class) + " is decorated with 'loads_initial_data' but does not implement 'load_initial_data'")
    return original_class


def is_queryable(original_class):
    return original_class


def decorate(*decorators):
    def register_wrapper(func):
        if '_decorators' not in dir(func):
            func._decorators = tuple()
        for deco in decorators[::-1]:
            func = deco(func)
        func._decorators = decorators
        return func

    return register_wrapper


def get_models_with_decorator_in_app(_app, decorator_name='', app_name=False, include_class=False, **kwargs):
    app = get_app(_app)
    m = list()
    for model in get_models(app):
        if model._decorators is not None:
            for decorator in model._decorators:
                if decorator.__name__ == decorator_name:
                    if include_class:
                        _m = model
                    else:
                        _m = model.__name__

                    if app_name:
                        m.append((_app, _m))
                    else:
                        m.append(_m)
                    break
    return m


def get_models_with_decorator(decorator_name, apps, app_name=False, include_class=False, **kwargs):
    m = list()
    for app in apps:
        try:
            appname = app[app.rfind(".") + 1:]
            m += get_models_with_decorator_in_app(appname, decorator_name, app_name=app_name, include_class=include_class, **kwargs)
        except:
            pass
    return m
