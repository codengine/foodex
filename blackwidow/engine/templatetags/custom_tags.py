from django import template
from datetime import datetime
from itertools import chain
from copy import deepcopy
import re

register = template.Library()


@register.filter(name='date')
def timestamp_to_date(timestamp):
    return datetime.fromtimestamp(timestamp).strftime("%d-%m-%y")


@register.filter(name="make_list")
def make_list(objects):

    max_items_single_page = 42
    single_item_height_on_print = 13
    normal_page_items = 23
    first_page_items = normal_page_items
    last_page_threshold = 16
    objects = objects.order_by(objects.model.default_order_by())
    objects = list(chain(objects))
    objects_set = []
    count = 1
    for item in objects:
        item.index = count
        objects_set.append(deepcopy(item))
        count += 1
    final_set = []
    if len(objects_set) >= first_page_items:
        final_set.append(objects_set[:first_page_items])
        objects_set = objects_set[first_page_items:]
    n = max(1, normal_page_items)
    final_set += [objects_set[i:i + n] for i in range(0, len(objects_set), n)]
    last_header = False
    if len(final_set[len(final_set) - 1]) > last_page_threshold:
        custom_list = final_set[len(final_set) - 1]
        l_portion = custom_list[-5:]
        f_portion = custom_list[:len(custom_list)-len(l_portion)]
        final_set.remove(custom_list)
        final_set += [f_portion, l_portion]
        last_header = False
    if last_header is True:
        return {
            'sub_lists': final_set,
            'first_page_footer_margin':
                (max_items_single_page - first_page_items - 7) * single_item_height_on_print + 10,
            'normal_page_footer_margin':
                (max_items_single_page - normal_page_items) * single_item_height_on_print,
            'last_page_footer_margin_1':
                (max_items_single_page - len(final_set[len(final_set) - 1])) * single_item_height_on_print,
            'last_page_footer_margin_2':
                (max_items_single_page - 5) * single_item_height_on_print,
            'last_header': last_header
        }
    elif len(final_set) == 1:
        return {
            'sub_lists': final_set,
            'first_page_footer_margin':
                (max_items_single_page - first_page_items - 7) * single_item_height_on_print + 10,
            'normal_page_footer_margin':
                (max_items_single_page - normal_page_items) * single_item_height_on_print,
            'last_page_footer_margin_2':
                (max_items_single_page - len(final_set[len(final_set) - 1]) - 13) * single_item_height_on_print + 10,
            'last_header': last_header
        }
    else:
        return {
            'sub_lists': final_set,
            'first_page_footer_margin':
                (max_items_single_page - first_page_items - 7) * single_item_height_on_print,
            'normal_page_footer_margin':
                (max_items_single_page - first_page_items - 7) * single_item_height_on_print,
            'last_page_footer_margin_2':
                (max_items_single_page - len(final_set[len(final_set) - 1]) - 20) * single_item_height_on_print + 10,
            'last_header': last_header
        }


@register.filter(name="make_pack_size")
def make_pack_size(string=""):
    digit_set = re.findall("(\d+)", string)
    if len(digit_set) > 0:
        return string[string.rfind(digit_set[0]):]
    else:
        return string


