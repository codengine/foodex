from django.db.models.loading import get_model
from widget_tweaks.templatetags.widget_tweaks import append_attr
from blackwidow.core.models.log.audit_log import DeletedEntityEnum

from blackwidow.engine.extensions import bw_titleize
from blackwidow.engine.extensions.list_extensions import inserted_list
from blackwidow.engine.managers.bwpermissionmanager import BWPermissionManager
from config.constants.access_permissions import BW_ACCESS_CREATE_MODIFY_DELETE, BW_ACCESS_CREATE_MODIFY, \
    BW_ACCESS_MODIFY_ONLY, BW_ACCESS_READ_ONLY, BW_ACCESS_NO_ACCESS
from config.enums import ViewActionEnum


__author__ = 'ruddra'

from django import template
from django.forms import HiddenInput

register = template.Library()


@register.filter(name='is_hidden')
def filter_hidden(field):
    return field.field.widget.__class__.__name__ == HiddenInput().__class__.__name__

@register.filter(name='is_visible')
def filter_visible(field):
    hidden_class_found = field.field.widget.attrs.get('class') and 'hidden' in field.field.widget.attrs['class'].split(' ')
    return hidden_class_found

@register.filter(name='is_datetime')
def filter_datetime(field):
    return 'date-time-picker' in field.field.widget.attrs.get('class', '').split(' ')


@register.filter(name='get_property')
def filter_search_property(s, args):
    property = args.get('property', '')
    if property != '':
        return property
    return ''


@register.filter(name='get_prefix')
def get_prefix(prefix):
    if prefix == "" or prefix == '-' or prefix is None:
        return ""
    return prefix + '-'


@register.filter(name='replace_with_underscore')
def replace_prefix(s, r):
    if s == "" or s == r or s is None:
        return "_"
    return s.replace(r, '_')


@register.filter(name='name_value_pair')
def filter_dictionary(s):
    items = []
    for k in s:
        items += [[bw_titleize(k), s[k]]]
    return items


@register.filter(name='get_name')
def filter_dictionary_name(s, prop=None):
    if prop:
        return s[prop]
    if 'name' in s:
        return s['name']
    if 'full_name' in s:
        return s['full_name']
    return ''


@register.filter(name='is_selected')
def select_search_property(s, args):
    if s == args:
        return "selected=selected"
    return ''


@register.filter(name='get_search_value')
def get_search_value(s, args):
    property = args.get('query_1', '')
    if property != '':
        return "value=" + property + ""

    property = args.get('query_2', '')
    if property != '':
        return "value=" + property + ""

    return 'All'


@register.filter(name='add_prefix')
def add_prefix(field, args):
    return append_attr(field, 'data-prefix:' + str(args) + '-')


@register.filter(name='make_search_list')
def make_search_list(s):
    a = []
    for field, name, is_date, link in s:
        a.append((bw_titleize(name), field, is_date, link))
    return a #inserted_list(a, 0, ('All', 'All', False, ''))


@register.filter(name='remove_quote')
def remove_quote(s):
    return str(s).strip('\'')


@register.filter(name='append_request')
def append_request(object,request):
    return {
        "request": request,
        "object": object
    }

@register.filter(name="filter_inline_object_permission")
def filter_inline_object_permission(object, buttons):
    # inline_buttons = object.get_inline_manage_buttons
    # model = get_model(object._meta.app_label, object.__class__.__name__)
    # buttons = model.get_manage_buttons()
    object_inline_buttons = buttons['object']
    request = buttons['request']
    inline_buttons = list()
    if not object_inline_buttons:
        return []
    for btn in object_inline_buttons:
        if btn == ViewActionEnum.Details:
            inline_buttons += [ dict(
                name='Details',
                action='view',
                title="Click to view this item",
                icon='icon-eye',
                ajax='0',
                url_name=object.__class__.get_route_name(action=ViewActionEnum.Details),
                classes='all-action ',
                parent=None
            )]
        if btn == ViewActionEnum.Edit:
            inline_buttons += [ dict(
                    name='Edit',
                    action='edit',
                    title="Click to edit this item",
                    icon='icon-pencil',
                    ajax='0',
                    url_name=object.__class__.get_route_name(action=ViewActionEnum.Edit),
                    classes='all-action',
                    parent=None
                )]
        if btn == ViewActionEnum.Delete:
            inline_buttons += [ dict(
                    name='Delete',
                    action='delete',
                    title="Click to remove this item",
                    icon='icon-remove',
                    ajax='0',
                    url_name=object.__class__.get_route_name(action=ViewActionEnum.Delete),
                    classes='manage-action all-action confirm-action',
                    parent=None
                )]
        if btn == ViewActionEnum.Mutate:
            inline_buttons += [ dict(
                    name='Accept',
                    action='view',
                    title="Click to accept this order",
                    icon='icon-arrow-right',
                    ajax='0',
                    url_name=object.__class__.get_route_name(action=ViewActionEnum.Mutate),
                    url_params={'pk': object.pk},
                    classes='all-action confirm-action',
                    parent=None
                )]
        if btn == ViewActionEnum.Approve:
            inline_buttons += [ dict(
                    name='Approve',
                    action='approve',
                    title="Click to approve this order",
                    icon='icon-arrow-right',
                    ajax='0',
                    url_name=object.__class__.get_route_name(action=ViewActionEnum.Approve),
                    url_params={'pk': object.pk},
                    classes='all-action confirm-action',
                    parent=None
                )]
        if btn == ViewActionEnum.Reject:
            inline_buttons += [ dict(
                    name='Reject',
                    action='reject',
                    title="Click to reject this order",
                    icon='icon-arrow-right',
                    ajax='0',
                    url_name=object.__class__.get_route_name(action=ViewActionEnum.Reject),
                    url_params={'pk': object.pk},
                    classes='all-action confirm-action',
                    parent=None
                )]

    if request.c_user.is_super:
        if hasattr(object, 'deleted_status') and object.deleted_status == DeletedEntityEnum.SoftDeleted.value:
            inline_buttons.append(dict(
                name='Restore',
                action='restore',
                title="Click to restore this item",
                icon='icon-redo icon-safe',
                ajax='0',
                url_name=object.__class__.get_route_name(action=ViewActionEnum.Restore),
                classes='all-action confirm-action',
                parent=None
            ))
            inline_buttons.append(dict(
                name='Delete',
                action='delete',
                title="Click to delete this item permanently",
                icon='icon-remove icon-danger',
                ajax='0',
                url_name=object.__class__.get_route_name(action=ViewActionEnum.Delete),
                classes='all-action confirm-action',
                parent=None,
                params='hard_delete=1'
            ))

    return inline_buttons


@register.filter(name="sorted_search_form")
def sorted_search_form(object):
    search_list = []

    for field in object:
        if not field.field.readonly:
            search_list.append(field)
    sorted_items = sorted(search_list, key=lambda a: a.label)
    return sorted_items

@register.filter(name='times')
def times(number):
    return range(number)

@register.filter(name='mod')
def mod_(number, divisible_by):
    return number % divisible_by
