from blackwidow.core.models.file.fileobject import FileObject
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Mahmud'


# @decorate(is_object_context, route(route='back-ups', group='BackUp', module=ModuleEnum.Administration, display_name="Back Up"))
class BackUpInformation(FileObject):

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.BackUp, ViewActionEnum.Delete]

    class Meta:
        proxy = True