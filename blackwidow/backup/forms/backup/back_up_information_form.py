from blackwidow.backup.models.backup.back_up_information import BackUpInformation
from blackwidow.core.forms.files.fileobject_form import FileObjectForm

__author__ = 'Mahmud'


class BackUpInformationForm(FileObjectForm):
    class Meta(FileObjectForm.Meta):
        model = BackUpInformation
        fields = ['name']