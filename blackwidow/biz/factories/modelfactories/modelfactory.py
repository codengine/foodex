from django.db.models.loading import get_model
from south.db import db
from django.db import transaction

from blackwidow.engine.exceptions.exceptions import BWException
from blackwidow.engine.factories.modelfactories.modelfactory import ModelFactory


__author__ = 'Mahmud'


class BizModelFactory(ModelFactory):

    def generate_model(self, data=None, **kwargs):
        attrs = {
            '__module__': 'blackwidow.biz.models'
        }
        model_parent = get_model('biz', 'BizModel')
        model = type(data.name, (model_parent,), attrs)
        return model

    def generate_storage(self, data=None, model=None):
        if model is None:
            raise BWException('Null model provided')
        fields = [(f.name, f) for f in model._meta.local_fields]
        table_name = data.db_table

        with transaction.atomic():
            db.create_table(table_name, fields)
            db.execute_deferred_sql()

    def generate_model_storage(self, model=None):
        if model is None:
            raise BWException('Null model provided')
        d_model = self.generate_model(data=model)
        self.generate_storage(model, d_model)

    def delete_model_storage(self, model=None):
        if model is None:
            raise BWException('Null model provided')
        # d_model = self.generate_model(data=model)
        table_name = model.db_table
        with transaction.atomic():
            db.delete_table(table_name)
            db.execute_deferred_sql()




