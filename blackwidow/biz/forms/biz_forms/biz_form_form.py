import re

from django import forms
from django.db import transaction

from blackwidow.biz.models.biz_forms.biz_form import BizForm
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin


__author__ = 'Mahmud'


class BizFormForm(GenericFormMixin):
    name = forms.CharField(required=False, widget=forms.TextInput)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def clean_name(self):
        name = self.cleaned_data['name'].strip()
        match = re.match(r'^[a-zA-z]+((\s)+([a-zA-Z])*)*$', self.cleaned_data['name'])
        if match is None:
            self.add_error('name', 'Name can only contain Alphabetical Characters and \' \'.')
            return name
        return self.cleaned_data['name']

    def save(self, commit=True):
        self.instance.db_table = 'biz_dynamic_' + re.sub(r'(\s)+', '_', self.instance.name).lower()
        with transaction.atomic():
            self.model_factory.generate_model_storage(self.instance)
            return super().save(commit)

    class Meta(GenericFormMixin.Meta):
        model = BizForm
        fields = ['name']