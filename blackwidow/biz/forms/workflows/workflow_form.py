from blackwidow.biz.models.workflows.workflow import Workflow
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin

__author__ = 'Mahmud'


class WorkFlowForm(GenericFormMixin):

    class Meta(GenericFormMixin.Meta):
        model = Workflow
        fields = ['name']