from blackwidow.biz.models.workflows.workflow import WorkflowStep
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin

__author__ = 'Mahmud'


class WorkflowStepForm(GenericFormMixin):
    
    class Meta(GenericFormMixin.Meta):
        model = WorkflowStep
        fields = ['name']