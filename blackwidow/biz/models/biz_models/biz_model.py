from django.db import models, transaction

from blackwidow.biz.factories.modelfactories.modelfactory import BizModelFactory
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity


__author__ = 'Mahmud'


class BizModelField(OrganizationDomainEntity):
    name = models.CharField(max_length=200)
    field_type = models.BinaryField()


class BizModel(OrganizationDomainEntity):
    name = models.CharField(max_length=200, unique=True)
    fields = models.ManyToManyField(BizModelField)
    db_table = models.CharField(max_length=200)
    model_factory = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.model_factory = BizModelFactory()

    def get_choice_name(self):
        return self.name

    def delete(self, *args, **kwargs):
        with transaction.atomic():
            self.model_factory.delete_model_storage(model=self)
            all_fields = list(self.fields.all())
            for x in all_fields:
                self.fields.remove(x)
                x.delete()
            super().delete(*args, **kwargs)
