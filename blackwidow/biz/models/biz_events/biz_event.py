from django.db import models

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity


__author__ = 'Mahmud'


class BizEvent(OrganizationDomainEntity):
    name = models.CharField(max_length=200)

    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class Serializer(ss):
            class Meta(ss.Meta):
                model = cls
                depth = 1
        return Serializer
