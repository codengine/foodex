from django.db import models

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.engine.decorators.utility import decorate, save_audit_log


__author__ = 'Mahmud'


class WorkflowStep(OrganizationDomainEntity):
    name = models.CharField(max_length=200)
    order = models.IntegerField(default=0)

@decorate(save_audit_log)
class Workflow(OrganizationDomainEntity):
    name = models.CharField(max_length=200)
    steps = models.ManyToManyField(WorkflowStep)

    def delete(self, *args, **kwargs):
        all_steps = self.steps.all()
        if len(all_steps) > 0:
            for x in all_steps:
                self.steps.remove(x)
                x.delete(**kwargs)
            super().delete(**kwargs)

