from django.db import models, transaction

from blackwidow.biz.models.biz_models.biz_model import BizModelField
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity


__author__ = 'Mahmud'


class BizFormField(OrganizationDomainEntity):
    name = models.CharField(max_length=200)
    widget = models.CharField(max_length=200)
    max_length = models.IntegerField(default=0)
    default = models.BinaryField(null=True)


class BizForm(OrganizationDomainEntity):
    name = models.CharField(max_length=200)
    fields = models.ManyToManyField(BizModelField)

    def get_choice_name(self):
        return self.name


    def delete(self, *args, **kwargs):
        with transaction.atomic():
            all_fields = list(self.fields.all())
            for x in all_fields:
                self.fields.remove(x)
                x.delete()
            super().delete(*args, **kwargs)
