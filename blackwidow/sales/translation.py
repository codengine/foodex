from modeltranslation.translator import translator

from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.products.product_group import ProductGroup


__author__ = 'Mahmud'

translator.register(ProductGroup, ProductGroup.get_translator_options())
translator.register(Product, Product.get_translator_options())