from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.clients.dealer import Dealer


class DealerSerializer(OrganizationDomainEntity.get_serializer()):
    
    class Meta(OrganizationDomainEntity.get_serializer().Meta):
        model = Dealer