from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from foodex.models.clients.distributor import Distributor


class DistributorSerializer(OrganizationDomainEntity.get_serializer()):
    
    class Meta(OrganizationDomainEntity.get_serializer().Meta):
        model = Distributor