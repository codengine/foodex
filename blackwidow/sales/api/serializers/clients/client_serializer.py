from rest_framework import serializers

from blackwidow.sales.api.serializers.clients.dealer_serializer import DealerSerializer
from blackwidow.sales.api.serializers.clients.distributor_serializer import DistributorSerializer


class ClientSerializer(serializers.Serializer):
    dealers = DealerSerializer()
    distributors = DistributorSerializer()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
