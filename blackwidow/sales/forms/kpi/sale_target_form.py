from blackwidow.core.forms.kpi.kpi_form import PerformanceIndexForm
from blackwidow.sales.models.kpi.sale_target import SaleTargetPerformanceIndex

__author__ = 'activehigh'


class SaleTargetPerformanceIndexForm(PerformanceIndexForm):

    class Meta(PerformanceIndexForm.Meta):
        model = SaleTargetPerformanceIndex