from django import forms

from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.clients.client import Client
from blackwidow.sales.models.invoice.invoice import Invoice
from blackwidow.sales.models.payments.payment_methods import Payment, PaymentMethod


__author__ = 'Mahmud'


class PaymentForm(GenericFormMixin):
    def __init__(self, data=None, files=None, instance=None,  **kwargs):
        super(). __init__(data=data, files=files, instance=instance, **kwargs)

    class Meta(GenericFormMixin.Meta):
        model = Payment

