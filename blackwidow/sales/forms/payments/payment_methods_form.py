from blackwidow.core.forms.configurabletypes.configurabletype_form_mixin import ConfigurableTypeFormMixin
from blackwidow.sales.models.payments.payment_methods import PaymentMethod

__author__ = 'Mahmud'


class PaymentMethodForm(ConfigurableTypeFormMixin):

    class Meta(ConfigurableTypeFormMixin.Meta):
        model = PaymentMethod
        fields = ['name']

