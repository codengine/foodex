from django.forms.models import modelformset_factory
from django import forms
from django.forms.utils import ErrorList

from blackwidow.core.forms.files.imagefileobject_form import ImageFileObjectForm
from blackwidow.sales.forms.common.product_price_value_form import ProductPriceValueForm
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.fieldmixin.multiple_select_field_mixin import GenericModelMultipleChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin, GenericModelFormSetMixin
from blackwidow.sales.models.common.product_price import ProductPrice
from blackwidow.core.models.manufacturers.manufacturer import Manufacturer
from blackwidow.sales.models.products.product import Product, ProductPriceValue
from blackwidow.sales.models.products.product_group import ProductGroup

product_price_form_set = modelformset_factory(ProductPriceValue, form=ProductPriceValueForm,
                                              formset=GenericModelFormSetMixin, extra=0, min_num=1)


class ProductsForm(GenericFormMixin):
    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, instance=None, **kwargs):
        super().__init__(data=data, files=files, auto_id=auto_id, prefix=prefix, instance=instance, **kwargs)
        prefix = prefix + '-' if prefix == '' else prefix
        # self.fields['unit'] = GenericModelChoiceField(queryset=ProductUnit.objects.all(), label='Unit', initial=instance.unit if instance is not None else None, widget=forms.Select(attrs={'class': 'select2'}), required=True)
        self.fields['manufacturer'] = GenericModelChoiceField(queryset=Manufacturer.objects.all(),
                                                              initial=instance.manufacturer if instance is not None else None,
                                                              label='Manufacturer',
                                                              widget=forms.Select(attrs={'class': 'select2'}),
                                                              required=True)
        self.fields['categories'] = GenericModelMultipleChoiceField(queryset=ProductGroup.objects.all(),
                                                                    initial=instance.categories.all() if instance is not None else None,
                                                                    label='Category', widget=forms.SelectMultiple(
                attrs={'class': 'select2'}), required=True)
        # self.fields['brand'] = GenericModelChoiceField(queryset=Brand.objects.all(), label='Brand', initial=instance.brand if instance is not None else None, widget=forms.Select(attrs={'class': 'select2'}), required=False)
        self.fields['origin'].required = True
        self.add_child_form("prices", product_price_form_set(data=data, files=files, header='Price Matrix',
                                                             prefix=prefix + 'suffix-' + str(
                                                                 len(self.suffix_child_forms)),
                                                             queryset=instance.prices.all() if instance is not None else ProductPrice.objects.none(),
                                                             **kwargs))
        kwargs.update({
            'prefix': 'icon'
        })
        self.add_child_form("icon", ImageFileObjectForm(data=data, files=files,
                                                        instance=instance.icon if instance is not None else None,
                                                        form_header='Icon', **kwargs))

    def clean(self):
        attrs = [at for at in self.data if '-price' in at]
        prices = ProductPrice.objects.all().values('pk', 'name')
        prices_pk_list = [price['pk'] for price in prices]

        added_list = list()
        for attr in attrs:
            price_pk = int(self.data[attr])
            added_list.append(price_pk)

        prices_count = len(self.child_forms[0][1])
        for pk in prices_pk_list:
            if pk not in added_list:
                self.errors['price'] = ErrorList()
                self.errors['price'].append(
                    '%s is not provided' % [price['name'] for price in prices if price['pk'] == pk][0])
                self.child_forms[0][1][prices_count - 1].add_error('price',
                                                                   '%s is not provided' %
                                                                   [price['name'] for price in prices if
                                                                    price['pk'] == pk][0])
                break
        return super().clean()

    class Meta(GenericFormMixin.Meta):
        model = Product
        fields = ['name', 'short_name', 'categories', 'description', 'manufacturer', 'origin']
        widgets = {
            'description': forms.Textarea(attrs={'class': 'description'}),
            'description_bd': forms.Textarea(attrs={'class': 'description'})
        }
