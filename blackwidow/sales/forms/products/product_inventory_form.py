from django import forms

from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.sales.models.products.product_inventory import ProductInventory

__author__ = 'Mahmud'


class ProductInventoryForm(GenericFormMixin):
    adjust_stock = forms.CharField(max_length=10, required=True)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields['stock'].widget.attrs['readonly'] = 'readonly'

    def save(self, commit=True):
        i_stock = int(self.cleaned_data.get('adjust_stock', '0'))
        self.instance.stock += i_stock
        #self.instance.save()
        super().save(commit)
        # sale = WarehouseTransaction()
        # sale.organization = self.instance.organization
        # sale.infrastructure_unit = self.instance.assigned_to
        # location = Location()
        # location.save()
        # sale.location = location
        # sale.transaction_time = Clock.timestamp()
        # sale.transaction_type = TransactionType.StockAdjustment.value
        # sale.save()

        #return super().save(commit)
    
    class Meta(GenericFormMixin.Meta):
        model = ProductInventory
        fields = ['product','stock']
        labels = {
            'adjust_stock': 'Adjust Stock',
            'stock': 'Current Stock'
        }
        widgets = {
            'product': forms.Select(attrs={'class': 'select2'}),
            }
