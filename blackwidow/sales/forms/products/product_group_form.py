from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.sales.models.products.product_group import ProductGroup

__author__ = 'ruddra'


class ProductGroupForm(GenericFormMixin):
    # def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None,
    #              initial=None, error_class=ErrorList, label_suffix=None,
    #              empty_permitted=False, **kwargs):
    #     super(). __init__(data=data, files=files, auto_id=auto_id, prefix=prefix,
    #                       initial=initial, error_class=error_class, label_suffix=label_suffix,
    #                       empty_permitted=empty_permitted, **kwargs)

    def validate_unique(self):
        super().validate_unique()
        users = ProductGroup.objects.filter(name=self.cleaned_data['name'])
        if len(users) > 0:
            self.add_error('name', 'Category with name \'' + self.cleaned_data['name'] + '\' already exists.')

    class Meta(GenericFormMixin.Meta):
        model = ProductGroup
        fields = ['name']