from django import forms
from django.db import transaction
from django.forms.models import modelformset_factory

from blackwidow.core.forms.common.location_form import LocationForm
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin, GenericModelFormSetMixin
from blackwidow.core.models.clients.client import Client
from blackwidow.sales.forms.orders.order_breakdown_form import OrderBreakdownForm
from blackwidow.sales.forms.orders.order_breakdown_formset import make_order_formset
from blackwidow.sales.forms.payments.payment_form import PaymentForm
from blackwidow.sales.models.orders.order_breakdown import OrderBreakdown
from blackwidow.sales.models.orders.order import Order
from blackwidow.sales.models.orders.secondary_order import SecondaryOrder
from blackwidow.sales.models.payments.payment_methods import Payment

__author__ = 'Mahmud'


class SecondaryOrderForm(GenericFormMixin):
    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        depends_on = kwargs.get("depends_on")
        depends_property = kwargs.get("depends_property")
        kwargs.pop("depends_on")
        kwargs.pop("depends_property")
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)
        prefix = prefix + '-' if prefix != '' else ''
        if self.instance.pk is not None:
            orders = self.instance.breakdown.all()
            breakdown_formset = make_order_formset(max_num=orders.count() if orders.exists else 0,
                                                   **{"depends_on": depends_on, "depends_property": depends_property,
                                                      "require_price_type": "require-dist-price"})
            self.add_child_form("breakdown", breakdown_formset(add_more=True, data=data, files=files, queryset=orders,
                                                               prefix=prefix + 'suffix-' + str(
                                                                   len(self.suffix_child_forms)),
                                                               header='Order Breakdown', render_table=False, **kwargs))

        else:
            orders = OrderBreakdown.objects.filter(id=0)
            breakdown_formset = make_order_formset(extra=9,
                                                   **{"depends_on": depends_on, "depends_property": depends_property,
                                                      "require_price_type": "require-dist-price"})
            self.add_child_form("breakdown", breakdown_formset(add_more=True, data=data, files=files, queryset=orders,
                                                               prefix=prefix + 'suffix-' + str(
                                                                   len(self.suffix_child_forms)),
                                                               header='Order Breakdown', render_table=False, **kwargs))
            # self.add_child_form("location", LocationForm(data=data, files=files, instance=instance.location if instance is not None else None, prefix=prefix + 'suffix-' + str(len(self.suffix_child_forms)), form_header='Location', **kwargs))

    class Meta(GenericFormMixin.Meta):
        model = SecondaryOrder
        fields = ['from_client', 'to_client']
