from django import forms
from django.core.urlresolvers import reverse

from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.orders.order_breakdown import OrderBreakdown
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Mahmud'


class OrderBreakdownForm(GenericFormMixin):
    def __init__(self, data=None, files=None,  **kwargs):
        super().__init__(data=data, files=files, **kwargs)
        self.fields['product'] = GenericModelChoiceField(queryset=Product.objects.none(), label='Select Product', widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'warehouse', 'data-depends-property': '_search_warehouse_inventory:assigned_to:id', 'data-url': reverse(Product.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}), required=True)
        self.fields['total_items'] = forms.IntegerField(min_value=1,required=False)

    def show_form_inline(self):
        return True

    def clean(self):
        data = self.cleaned_data
        return data

    def save(self, commit=True):
        try:
            if hasattr(self.instance,"pk"):
                self.instance.save(commit)
        except:
            pass
        return self.instance

    class Meta:
        model = OrderBreakdown
        fields = ['product', 'total_items', 'unit_price', 'total']
