from collections import OrderedDict
from django.db import transaction
from django.core.urlresolvers import reverse
from django.db.models.query_utils import Q
from blackwidow.core.models import WareHouse
from blackwidow.core.process.process_breakdown import ProcessBreakdown, ApprovalStatusEnum
from blackwidow.sales.forms.orders.order_form import OrderForm
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.models.clients.client import Client
from blackwidow.sales.models.orders.order_history import OrderHistory, OrderHistoryStatusEnum
from config.enums import ViewActionEnum
from foodex.models import Area
from foodex.models.clients.horeco import HoReCo
from foodex.models.clients.modern_trade import ModernTrade
from foodex.models.clients.wholesale import WholeSaler
from foodex.models.orders.pending_order import PendingOrder
from django import forms
from django.db.models import Sum
from foodex.models.clients.distributor import Distributor
from django.contrib import messages


class PendingOrderForm(OrderForm):
    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        kwargs["depends_on"] = "warehouse"
        kwargs["depends_property"] = "_search_warehouse_inventory:assigned_to:id"
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)

        area_qryset = Area.objects.all()
        area_qryset = Area.get_role_based_queryset(area_qryset)
        client_qryset = Client.objects.filter(
            Q(type=Distributor.__name__) | Q(type=HoReCo.__name__) | Q(type=ModernTrade.__name__) | Q(
                type=WholeSaler.__name__))
        client_qryset = Client.get_role_based_queryset(client_qryset)
        if instance:
            area_qryset = area_qryset.filter(pk__in=[instance.client.assigned_to.pk])
            client_qryset = client_qryset.filter(pk__in=[instance.client.pk])

        self.fields['warehouse'] = GenericModelChoiceField(label='From Warehouse', queryset=WareHouse.objects.all(),
                                                           widget=forms.Select(attrs={'class': 'select2'}),
                                                           initial=WareHouse.objects.all().first())
        self.fields['area'] = GenericModelChoiceField(label='Area', queryset=area_qryset,
                                                      widget=forms.Select(attrs={'class': 'select2'}),
                                                      initial=instance.client.assigned_to if instance is not None else None)
        if not instance:
            self.fields['client'] = GenericModelChoiceField(label='To Client', queryset=client_qryset,
                                                            widget=forms.TextInput(
                                                                attrs={'class': 'select2-input toggle_po_number',
                                                                       'width': '220', 'data-depends-on': 'area',
                                                                       'data-depends-property': 'assigned_to:id',
                                                                       'data-url': reverse(Client.get_route_name(
                                                                           action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
            self.fields['reference_po_number'] = forms.CharField(label='Ref PO Number',
                                                                 widget=forms.TextInput(attrs={'class': 'hidden'}),
                                                                 required=False)
        else:
            hidden_class = ''
            if instance.client.type == Distributor.__name__:
                hidden_class = 'hidden'
            self.fields['client'] = GenericModelChoiceField(label='To Client', queryset=client_qryset,
                                                            widget=forms.Select(
                                                                attrs={'class': 'select2 toggle_po_number'}),
                                                            initial=instance.client.assigned_to if instance is not None else None)
            self.fields['reference_po_number'] = forms.CharField(label='Ref PO Number',
                                                                 initial=instance.reference_po_number,
                                                                 widget=forms.TextInput(attrs={'class': hidden_class}),
                                                                 required=False)
        self.fields['discount'] = forms.DecimalField(label='Discount(%)',
                                                     initial=instance.discount if instance and instance.discount else 0.0,
                                                     widget=forms.NumberInput(attrs={'class': ''}), min_value=0,
                                                     max_digits=10, decimal_places=2, required=False)
        self.fields['gross_discount'] = forms.DecimalField(label='Gross Discount(BDT)',
                                                           initial=instance.gross_discount if instance and instance.gross_discount else 0.0,
                                                           widget=forms.NumberInput(attrs={'class': ''}), min_value=0,
                                                           max_digits=10, decimal_places=2, required=False)
        self.fields['remarks'] = forms.CharField(label='Remarks', max_length=100,
                                                 initial=instance.remarks if instance else '', widget=forms.Textarea(),
                                                 required=False)

        original_fields = self.fields
        ordered_fields = ['warehouse', 'area', 'client', 'discount', 'gross_discount', 'reference_po_number', 'remarks']
        new_order = OrderedDict()
        for key in ordered_fields:
            new_order[key] = original_fields[key]
        self.fields = new_order

    def clean(self):
        cleaned_data = super().clean()
        breakdown_form = self.child_forms[0][1]
        breakdown_set = breakdown_form.cleaned_data
        product_flag = False
        for breakdown in breakdown_set:
            if 'product' in breakdown:
                product_flag = True

        if not product_flag:
            message = 'Minimum one order breakdown entry is required'
            messages.error(self.request, message)
            raise forms.ValidationError(message)
        return cleaned_data

    class Meta(OrderForm.Meta):
        model = PendingOrder

    def save(self, commit=True):
        with transaction.atomic():
            pending_order_instance = self.instance.pk

            action = OrderHistoryStatusEnum.Created.value
            if pending_order_instance:
                action = OrderHistoryStatusEnum.Updated.value

            warehouse = self.cleaned_data["warehouse"]
            self.instance.warehouse = warehouse
            super().save(commit)

            OrderHistory.create_from_order(self.instance, action=action)

            if pending_order_instance is None:
                process_breakdown = ProcessBreakdown()
                process_breakdown.approval_status = ApprovalStatusEnum.Created
                process_breakdown.process_level = "created"
                process_breakdown.description = "pending order created"
                process_breakdown.save(commit)
                self.instance.process_breakdown.add(process_breakdown)
            return self.instance
