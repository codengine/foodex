from django.core.urlresolvers import reverse
from django.forms.models import modelformset_factory
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericModelFormSetMixin
from blackwidow.sales.forms.orders.order_breakdown_form import OrderBreakdownForm
from blackwidow.sales.models.orders.order_breakdown import OrderBreakdown
from blackwidow.sales.models.products.product import Product
from django import forms
from config.enums.view_action_enum import ViewActionEnum


def init_with_extra_parameters(**kwargs):
    def wrapper(cls_name):
        cls_name.kwargs = kwargs
        return cls_name

    return wrapper


def make_order_formset(extra=14, max_num=20, min_num=1, validate_min=True, can_delete=True, **kwargs):
    @init_with_extra_parameters(**kwargs)
    class _OrderBreakdownForm(OrderBreakdownForm):
        def __init__(self, data=None, files=None, instance=None, **kwargs):
            super().__init__(data=data, files=files, **kwargs)
            product_queryset = Product.objects.all()
            product_required = False
            initial_product = None
            initial_total_items = 0
            initial_unit_price = 0
            initial_total = 0
            if instance:
                initial_product = instance.product
                initial_total_items = instance.total_items
                initial_unit_price = instance.unit_price
                initial_total = instance.total

            edit_mode = True if instance else False

            self.fields['product'] = GenericModelChoiceField(initial=initial_product, queryset=product_queryset,
                                                             label='Select Product', widget=forms.TextInput(attrs={
                    'class': 'select2-input require-unitprice ' + ((self.__class__.kwargs.get(
                        "require_price_type") if self.__class__.kwargs.get(
                        "require_price_type") else '') + (' edit-mode' if edit_mode else '')), 'width': '220',
                    'data-depends-on': self.__class__.kwargs.get("depends_on"),
                    'data-depends-property': self.__class__.kwargs.get("depends_property"), 'data-url': reverse(
                        Product.get_route_name(
                            action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}),
                                                             required=False)
            self.fields['total_items'] = forms.IntegerField(initial=initial_total_items, required=True,
                                                            widget=forms.NumberInput(
                                                                attrs={'class': 'order-breakdown', 'min': 0}))
            self.fields['unit_price'] = forms.DecimalField(initial=initial_unit_price, required=True,
                                                           widget=forms.NumberInput(attrs={'class': 'order-breakdown'}))
            self.fields['total'] = forms.DecimalField(initial=initial_total, required=True,
                                                      widget=forms.NumberInput(attrs={'class': 'order-breakdown'}))

            self.empty_permitted = True

    form_set = modelformset_factory(OrderBreakdown, form=_OrderBreakdownForm, formset=GenericModelFormSetMixin,
                                    extra=extra, max_num=max_num, min_num=min_num, validate_min=validate_min,
                                    can_delete=can_delete, )

    class modified_formset(form_set):
        def save(self):
            super().save()
            return self.instance

    return modified_formset
