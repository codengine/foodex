from django import forms
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.clients.client import Client
from blackwidow.sales.forms.orders.order_breakdown_formset import make_order_formset
from blackwidow.sales.models.orders.order_breakdown import OrderBreakdown
from blackwidow.sales.models.orders.order import Order
__author__ = 'Mahmud'

#demand_estimation_formset = modelformset_factory(OrderBreakdown, form=OrderBreakdownForm, formset=GenericModelFormSetMixin, extra=1, max_num=10, min_num=1, validate_min=False, can_delete=True, )
#demand_estimation_formset_for_edit = modelformset_factory(OrderBreakdown, form=OrderBreakdownForm, formset=GenericModelFormSetMixin, extra=0, max_num=10, min_num=1, validate_min=False, can_delete=True, )
# payment_form = modelformset_factory(Payment, form=PaymentForm, formset=GenericModelFormSetMixin, extra=0, min_num=1, validate_min=False, can_delete=True, )

class OrderForm(GenericFormMixin):
    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        depends_on = kwargs.get("depends_on")
        depends_property = kwargs.get("depends_property")
        kwargs.pop("depends_on")
        kwargs.pop("depends_property")
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)
        prefix = prefix + '-' if prefix != '' else ''

        if self.instance.pk is not None:
            orders = self.instance.breakdown.all()
            breakdown_formset = make_order_formset(max_num=orders.count() if orders.exists else 0,**{"depends_on":depends_on,"depends_property":depends_property, "require_price_type":"require-gross-price"})
            self.add_child_form("breakdown", breakdown_formset(add_more=True,data=data, files=files, queryset=orders, prefix=prefix + 'suffix-' + str(len(self.suffix_child_forms)), header='Order Breakdown', render_table=False, **kwargs))

        else:
            orders = OrderBreakdown.objects.filter(id=0)
            breakdown_formset = make_order_formset(extra=9,**{"depends_on":depends_on,"depends_property":depends_property, "require_price_type":"require-gross-price"})
            self.add_child_form("breakdown", breakdown_formset(add_more=True,data=data, files=files, queryset=orders, prefix=prefix + 'suffix-' + str(len(self.suffix_child_forms)), header='Order Breakdown', render_table=False, **kwargs))

        # self.add_child_form("location", LocationForm(data=data, files=files, instance=instance.location if instance is not None else None, prefix=prefix + 'suffix-' + str(len(self.suffix_child_forms)), form_header='Location', **kwargs))
        self.fields['client'] = GenericModelChoiceField(queryset=Client.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))

    def add_breakdowns(self,blist):
        b_obj = blist[0]
        tnumber_of_packet = 0
        titems_per_packet = 0
        tloose_items = 0
        ttotal_items = 0
        tunit_price = 0
        tsub_total = 0
        tdiscount = 0
        ttotal = 0
        exclude_list = []
        for b in blist:
            tnumber_of_packet += b.number_of_packet
            titems_per_packet += b.items_per_packet
            tloose_items += b.loose_items
            ttotal_items += b.total_items
            tunit_price  += b.unit_price
            tsub_total  += b.sub_total
            tdiscount  += b.discount
            ttotal  += b.total
            if b.pk != b_obj.pk:
                exclude_list += [b]
        b_obj.number_of_packet = tnumber_of_packet
        b_obj.items_per_packet = titems_per_packet
        b_obj.loose_items = tloose_items
        b_obj.total_items = ttotal_items
        #b_obj.unit_price = tunit_price
        b_obj.sub_total = tsub_total
        b_obj.discount = tdiscount
        b_obj.total = ttotal
        b_obj.save()
        return b_obj,exclude_list

    def save(self, commit=True):
        super().save(commit)
        breakdown_set = self.instance.breakdown.all()
        b_groups = {}
        for b in breakdown_set:
            if not b.product in b_groups.keys():
                b_groups[b.product] = [b]
            else:
                b_groups[b.product] += [b]

        for key,items in b_groups.items():
            b_obj, exclude_list = self.add_breakdowns(items)
            for e_item in exclude_list:
                self.instance.breakdown.remove(e_item)
                e_item.delete()
        return self.instance

    class Meta:
        model = Order
        fields = ['client', 'discount', 'gross_discount', 'reference_po_number', 'remarks']

