from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.engine.extensions.bw_titleize import bw_titleize
from blackwidow.sales.models.settings.price_settings import TransactionPriceSettings
from blackwidow.sales.models.transactions.transaction import TransactionType, Transaction
from django import forms


class TransactionPriceSettingsForm(GenericFormMixin):

    def __init__(self, *args,  **kwargs):
        super(). __init__(*args, **kwargs)

        self.fields['transaction_type'].widget.choices = TransactionType.get_enum_list()
        self.fields['transaction_class'].widget.choices = [(x.__name__, bw_titleize(x.__name__)) for x in Transaction.__subclasses__()]


    class Meta:
        model = TransactionPriceSettings
        fields = ['transaction_type', 'transaction_class', 'price_type']
        widgets = {
            'transaction_type': forms.Select(attrs={'class': 'select2'}),
            'transaction_class': forms.Select(attrs={'class': 'select2'}),
            'price_type': forms.Select(attrs={'class': 'select2'})
        }
