from django import forms
from django.db import transaction
from django.core.urlresolvers import reverse
from django.db.transaction import _transaction_func
from blackwidow.core.forms.configurabletypes.configurabletype_form_mixin import ConfigurableTypeFormMixin
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.engine.exceptions.exceptions import BWException
from blackwidow.sales.models.common.product_price import ProductPrice, ProductPriceEnum
from blackwidow.sales.models.common.product_price_config import ProductPriceConfig
from blackwidow.sales.models.products.product import Product
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.clients.distributor import Distributor
from foodex.models.infrastructure.area import Area

__author__ = 'Rajib'

class ProductPriceConfigForm(GenericFormMixin):
    ref_cost = forms.CharField(label="Ref Cost", required=False)
    ref_price = forms.CharField(label="Ref Price")
    ref_net_cust_markup = forms.CharField(label="Ref Net Customer markup/discount")
    net_cust_price = forms.CharField(label="Net Customer price")
    vat_rate = forms.CharField(label="Vat Rate")
    gross_cust_price = forms.CharField(label="Gross Customer Price")
    gross_cust_distributor_markup = forms.CharField(label="Gross Customer Distributor markup")
    dist_price = forms.CharField(label="Dist price")
    dist_retailer_markup = forms.CharField(label="Distributor Retailer markup")
    retail_price = forms.CharField(label="Retail Price")

    def __init__(self , *args, **kwargs):
        super(). __init__(*args, **kwargs)
        self.fields['product'] = GenericModelChoiceField(queryset=Product.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
        self.fields['client'] = GenericModelChoiceField(label='Distributor', queryset=Distributor.objects.all(), widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'infrastructure_unit', 'data-depends-property': 'assigned_to:id', 'data-url': reverse(Distributor.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1"}))
        self.fields['infrastructure_unit'] = GenericModelChoiceField(queryset=Area.objects.all(), widget=forms.Select(attrs={'class': 'select2'}),
                                                                     label="Area")
        if self.instance:
            price_config = ProductPriceConfig.objects.filter(organization=self.instance.product.organization,
                                            is_default=True,
                                            infrastructure_unit__id=self.instance.infrastructure_unit.pk,
                                            product=self.instance.product)


    def save(self, commit=True):
        with transaction.atomic():
            ref_cost = self.cleaned_data["ref_cost"]
            ref_price = self.cleaned_data["ref_price"]
            ref_net_cust_markup = self.cleaned_data["ref_net_cust_markup"]
            net_cust_price = self.cleaned_data["net_cust_price"]
            vat_rate = self.cleaned_data["vat_rate"]
            gross_cust_price  = self.cleaned_data["gross_cust_price"]
            gross_cust_distributor_markup  = self.cleaned_data["gross_cust_distributor_markup"]
            dist_price = self.cleaned_data["dist_price"]
            dist_retailer_markup = self.cleaned_data["dist_retailer_markup"]
            retail_price = self.cleaned_data["retail_price"]

            if ref_cost:
                price_type = ProductPrice.objects.filter(name=ProductPriceEnum.ref_cost.value["name"])[0]
                self.create_price_config(self.instance,price_type,ref_cost)
            if ref_price:
                price_type = ProductPrice.objects.filter(name=ProductPriceEnum.ref_price.value["name"])[0]
                self.create_price_config(self.instance,price_type,ref_price)
            if ref_net_cust_markup:
                price_type = ProductPrice.objects.filter(name=ProductPriceEnum.ref_net_cust_markup.value["name"])[0]
                self.create_price_config(self.instance,price_type,ref_net_cust_markup)
            if net_cust_price:
                price_type = ProductPrice.objects.filter(name=ProductPriceEnum.net_cust_price.value["name"])[0]
                self.create_price_config(self.instance,price_type,net_cust_price)
            if vat_rate:
                price_type = ProductPrice.objects.filter(name=ProductPriceEnum.vat_rate.value["name"])[0]
                self.create_price_config(self.instance, price_type, vat_rate)
            if gross_cust_price:
                price_type = ProductPrice.objects.filter(name=ProductPriceEnum.gross_cust_price.value["name"])[0]
                self.create_price_config(self.instance, price_type, gross_cust_price)
            if gross_cust_distributor_markup:
                price_type = ProductPrice.objects.filter(name=ProductPriceEnum.gross_cust_distributor_markup.value["name"])[0]
                self.create_price_config(self.instance, price_type, gross_cust_distributor_markup)
            if dist_price:
                price_type = ProductPrice.objects.filter(name=ProductPriceEnum.dist_price.value["name"])[0]
                self.create_price_config(self.instance, price_type, dist_price)
            if dist_retailer_markup:
                price_type = ProductPrice.objects.filter(name=ProductPriceEnum.dist_retailer_markup.value["name"])[0]
                self.create_price_config(self.instance, price_type, dist_retailer_markup)
            if retail_price:
                price_type = ProductPrice.objects.filter(name=ProductPriceEnum.retail_price.value["name"])[0]
                self.create_price_config(self.instance, price_type, retail_price)

    def create_price_config(self, form_instance, price_type, value):
        try:
            ppc = ProductPriceConfig.objects.get(id = form_instance.id)
        except:
            ppc = ProductPriceConfig()
        ppc.organization = form_instance.product.organization
        ppc.price_type = price_type
        ppc.product = form_instance.product
        ppc.value = value
        ppc.is_default = True
        ppc.infrastructure_unit_id = form_instance.infrastructure_unit.pk
        ppc.client_id = form_instance.client.pk
        ppc.save()

    class Meta(GenericFormMixin.Meta):
        model = ProductPriceConfig
        fields = ['product', 'infrastructure_unit', 'client']