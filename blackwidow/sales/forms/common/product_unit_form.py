from blackwidow.core.forms.configurabletypes.configurabletype_form_mixin import ConfigurableTypeFormMixin
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.sales.models.common.product_unit import ProductUnit

__author__ = 'ruddra'


class ProductUnitForm(ConfigurableTypeFormMixin):

    def save(self, commit=True):
        self.instance.context = 'Product'
        return super().save(commit)

    class Meta(GenericFormMixin.Meta):
        model = ProductUnit
        fields = ['name']