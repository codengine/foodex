from django import forms
from django.forms.utils import ErrorList

from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.sales.models.common.product_price import ProductPrice
from blackwidow.sales.models.products.product import ProductPriceValue


__author__ = 'ruddra'


class ProductPriceValueForm(GenericFormMixin):

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None,
                 initial=None, error_class=ErrorList, label_suffix=None,
                 empty_permitted=False, **kwargs):
        super().__init__(data=data, files=files, auto_id=auto_id, prefix=prefix, initial=initial, error_class=error_class, label_suffix=label_suffix, empty_permitted=empty_permitted, **kwargs)
        self.fields['price'] = GenericModelChoiceField(queryset=ProductPrice.objects.all(), label='Price type', widget=forms.Select(attrs={'class': 'select2'}), required=True)

    def save(self, commit=True):
        return super().save(commit)

    class Meta(GenericFormMixin.Meta):
        model = ProductPriceValue
        fields = ['price', 'value']