from blackwidow.core.forms.configurabletypes.configurabletype_form_mixin import ConfigurableTypeFormMixin
from blackwidow.core.mixins.fieldmixin.multiple_select_field_mixin import GenericModelMultipleChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.engine.exceptions.exceptions import BWException
from blackwidow.sales.models.common.product_price import ProductPrice
from django import forms

__author__ = 'ruddra'


class ProductPriceForm(ConfigurableTypeFormMixin):

    def __init__(self, *args, instance=None, **kwargs):
        super().__init__(*args, instance=instance, **kwargs)
        self.fields['parents'] = GenericModelMultipleChoiceField(
            queryset=ProductPrice.objects.all() if instance is None else ProductPrice.objects.exclude(pk=instance.pk),
            help_text="If this price depends on another price, then select hose price types </br>",
            widget=forms.SelectMultiple(attrs={'class': 'select2', 'multiple': 'multiple'}))
        self.fields['equation'].help_text = "If this price depends on one or more prices, then describe the relation with a algebraic equation. <br/> " \
                                            + "Use [<short name>] for price type reference."\
                                            + "Supported operators are - '+ - / % *' "
        self.fields['equation'].required= False

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.context = 'Product'
        if not self.instance.id and self.instance.__class__.objects.filter(short_name=self.instance.short_name, name=self.instance.name).exists():
            raise BWException(message='This price type already exists')
        instance.save(commit)
        return instance

    class Meta(GenericFormMixin.Meta):
        model = ProductPrice
        fields = ['name', 'short_name', 'parents', 'equation', 'is_mobile_sale_price']