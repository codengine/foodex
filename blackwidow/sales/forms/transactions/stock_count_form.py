__author__ = 'zia'


from blackwidow.core.forms.common.location_form import LocationForm
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.sales.models.transactions.stock_count import StockCount


class StockCountForm(GenericFormMixin):

    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)

        self.add_child_form("location", LocationForm(data=data, files=files, form_header='Location', instance=instance.location if instance is not None else None, prefix='suffix' + str(len(self.suffix_child_forms)), **kwargs))

    class Meta(GenericFormMixin.Meta):
        model = StockCount
        fields = GenericFormMixin.Meta.fields + ['infrastructure_unit']


