from django.forms.models import modelformset_factory
from blackwidow.core.forms.common.location_form import LocationForm
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin, GenericModelFormSetMixin
from blackwidow.core.models.structure.warehouse import WareHouse
from django import forms
from blackwidow.sales.forms.transactions.transaction_break_down import BuySaleTransactionBreakDownForm
from blackwidow.sales.models.transactions.stock_in import StockIn
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown

products_form_set = modelformset_factory(TransactionBreakDown, form=BuySaleTransactionBreakDownForm, formset=GenericModelFormSetMixin, extra=0, min_num=1)

class StockInForm(GenericFormMixin):

    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)

        self.add_child_form("breakdown", products_form_set(data=data, files=files, header='Products', prefix= prefix + str(len(self.suffix_child_forms)), queryset=TransactionBreakDown.objects.none() if instance is None else instance.breakdown.all(), add_more=True, **kwargs))
        self.add_child_form("location", LocationForm(data=data, files=files, form_header='Location', instance=instance.location if instance is not None else None, prefix='suffix' + str(len(self.suffix_child_forms)), **kwargs))

    class Meta(GenericFormMixin.Meta):
        model = StockIn
        fields = GenericFormMixin.Meta.fields + ['infrastructure_unit']


