from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.sales.models.transactions.stock_transfer import StockTransfer

__author__ = 'Sohel'

class StockTransferForm(GenericFormMixin):
    def __init__(self, data=None, files=None, prefix='', instance=None, **kwargs):
        super().__init__(data=data, files=files, prefix=prefix, instance=instance, **kwargs)
        
        class Meta:
            model = StockTransfer
