from crequest.middleware import CrequestMiddleware
from django import forms
from django.core.urlresolvers import reverse
from django.db import transaction

from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'Mahmud'


class BuySaleTransactionBreakDownForm(GenericFormMixin):
    def __init__(self, data=None, files=None, instance=None, **kwargs):
        super().__init__(data=data, files=files, instance=instance, **kwargs)
        self.fields['unit_price'].widget = forms.TextInput(attrs={'style': 'width: 90%;', 'data-depends-on': 'product', 'data-depends-property': 'id', 'data-named-parameters': 'product:__product__','data-url': reverse(Product.get_route_name(action=ViewActionEnum.Details), kwargs={'pk': '__product__'})})

    def clean_product(self):
        if isinstance(self.cleaned_data['product'], Product):
            return self.cleaned_data['product']

        product = Product.objects.get(pk=int(self.cleaned_data['product']))
        return product

    def save(self, commit=True):
        with transaction.atomic():
            return super().save(commit)

    @property
    def field_count(self):
        return 7
    
    class Meta:
        model = TransactionBreakDown
        fields = ['product', 'quantity', 'unit_price', 'sub_total', 'discount', 'total']
        widgets = {
            'product': forms.Select(attrs={'class': 'select2', 'style': 'width: 90%;' }),
            'quantity': forms.TextInput(attrs={'style': 'width: 90%;'}),
            'sub_total': forms.TextInput(attrs={'style': 'width: 90%;', 'readonly': 'readonly', 'data-calculation': '[quantity] * [unit_price]'}),
            'discount': forms.TextInput(attrs={'style': 'width: 90%;'}),
            'total': forms.TextInput(attrs={'style': 'width: 90%;', 'readonly': 'readonly', 'data-calculation': '[sub_total] - [discount]'})
        }
