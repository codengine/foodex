from uuid import uuid4

from crequest.middleware import CrequestMiddleware
from django import forms
from django.forms.models import modelformset_factory
from django.db import transaction

from blackwidow.core.forms.common.location_form import LocationForm
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin, GenericModelFormSetMixin
from blackwidow.core.models.clients.client import Client
from blackwidow.engine.extensions.clock import Clock
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.sales.forms.transactions.transaction_break_down import BuySaleTransactionBreakDownForm
from blackwidow.sales.models.transactions.transaction import Transaction, TransactionType
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown


__author__ = 'Mahmud'

products_form_set = modelformset_factory(TransactionBreakDown, form=BuySaleTransactionBreakDownForm, formset=GenericModelFormSetMixin, extra=0, min_num=1)


class BuySaleTransactionForm(GenericFormMixin):
    # transaction_counter_part = forms.ChoiceField()
    transaction_time = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M:%S'], widget=forms.DateTimeInput(attrs={'data-format':"dd/MM/yyyy hh:mm:ss", 'class': 'date-time-picker', 'readonly': 'readonly'}, format='%d/%m/%Y %H:%M:%S'))

    def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)

        self.fields['unique_id'].initial = uuid4()
        self.fields['transaction_time'].initial = Clock.now().strftime("%d/%m/%Y %H:%M:%S")
        self.add_child_form("breakdown", products_form_set(data=data, files=files, header='Products', prefix= prefix + str(len(self.suffix_child_forms)), queryset=TransactionBreakDown.objects.none() if instance is None else instance.breakdown.all(), add_more=True, **kwargs))
        self.add_child_form("location", LocationForm(data=data, files=files, form_header='Location', instance=instance.location if instance is not None else None, prefix='suffix' + str(len(self.suffix_child_forms)), **kwargs))

    def clean_transaction_time(self):
        return self.cleaned_data['transaction_time'].timestamp()*1000

    def save(self, commit=True):
        with transaction.atomic():
            super().save(commit)

            self.instance.sub_total = 0
            self.instance.discount = 0
            self.instance.total = 0
            for x in self.instance.breakdown.all():
                self.instance.sub_total += x.sub_total
                self.instance.discount += x.discount
                self.instance.total += x.total
            self.instance.save(commit)
            return self.instance
            
    @classmethod
    def default_order_by(cls):
        return '-transaction_time'

    class Meta:
        model = Transaction
        fields = ['unique_id', 'transaction_time']
        labels = {
            'infrastructure_unit': 'Transaction For',
            'unique_id': 'Unique ID'
        }
        widgets = {
            'transaction_type': forms.Select(attrs={'readonly': 'readonly', 'class': 'select2'}),
            'unique_id': forms.TextInput(attrs={'readonly': 'readonly'}),
            # 'transaction_counter_part': forms.Select(attrs={'class': 'select2'}),
            'infrastructure_unit': forms.Select(attrs={'class': 'select2'})
        }