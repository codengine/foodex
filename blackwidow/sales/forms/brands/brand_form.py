from django import forms

from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.sales.models.brands.brand import Brand
from blackwidow.core.models.manufacturers.manufacturer import Manufacturer


__author__ = 'Mahmud'


class BrandForm(GenericFormMixin):
    def __init__(self, **kwargs):
        super(). __init__(**kwargs)
        self.fields['manufacturer'] = GenericModelChoiceField(queryset=Manufacturer.objects.all(), label='Manufacturer', widget=forms.Select(attrs={'class': 'select2'}), required=True)

    def save(self, commit=True):
        return super().save(commit)
    
    class Meta(GenericFormMixin.Meta):
        model = Brand
        fields = ['name', 'manufacturer']