from django.utils.safestring import mark_safe
from django.db import models

from blackwidow.core.models.common.contactaddress import ContactAddress
from blackwidow.core.models.common.emailaddress import EmailAddress
from blackwidow.core.models.common.phonenumber import PhoneNumber
from blackwidow.core.models.users.user import WebUser
from blackwidow.core.models.contracts.manufacturer_domain_entity import ManufacturerDomainEntity


__author__ = 'Mahmud'


class Lifters(ManufacturerDomainEntity):
    contact_persons = models.ManyToManyField(WebUser, name='contact_persons')
    addresses = models.ManyToManyField(ContactAddress, name='addresses')
    emails = models.ManyToManyField(EmailAddress, name='emails')
    phone_numbers = models.ManyToManyField(PhoneNumber, name="phone_numbers")

    @classmethod
    def get_dependent_field_list(cls):
        return ['addresses', 'emails', 'phone_numbers']

    @property
    def render_phone(self):
        return ', '.join([str(x) for x in self.phone_numbers.all()])

    @property
    def render_address(self):
        return ', '.join([str(x) for x in self.addresses.all()])

    @property
    def render_contact_person(self):
        return mark_safe(', '.join([str(x) for x in self.contact_persons.all()]))

    @classmethod
    def table_columns(cls):
        return 'code', 'name', 'manufacturer', 'render_contact_person', 'render_address', 'render_phone'