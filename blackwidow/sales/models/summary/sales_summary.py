from decimal import Decimal

from django.db import models

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.engine.extensions.clock import Clock

__author__ = 'activehigh'

# @decorate(is_summary_model)
class SalesSummary(OrganizationDomainEntity):
    hub = models.ForeignKey(InfrastructureUnit)
    week = models.IntegerField()
    sum_of_total_all = models.DecimalField(max_digits=16, decimal_places=6, default=Decimal("000.00"), null=True)
    sum_of_total_aparajita = models.DecimalField(max_digits=16, decimal_places=6, default=Decimal("000.00"), null=True)

    @classmethod
    def process(cls, transaction_queryset=None, current_run_time=None):
        iso_calender = Clock.get_utc_from_local_time(current_run_time).isocalendar()
        if transaction_queryset:
            for item in transaction_queryset:
                sales_summary, status = cls.objects.get_or_create(week=iso_calender[1], organization_id=item.organization_id,
                                                                  created_by=item.created_by, hub_id=item.infrastructure_unit_id)
                sales_summary.reference_id = 'Week '+str(iso_calender[1])+' '+str(iso_calender[0])
                sales_summary.sum_of_total_all += Decimal(item.total)
                sales_summary.sum_of_total_aparajita += Decimal(item.sum_of_total_aparajita)
                sales_summary.save()
            return True
        else:
            return None

    #     pass
        # get all data
        #  calculate
        #   save



# @periodic_task(run_every=datetime.timedelta(minutes=30))
# def trigger_alert():
    # for f,  all models decorated with 'is_suumary_model'
    # f.process()