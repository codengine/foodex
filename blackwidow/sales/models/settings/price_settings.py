from blackwidow.core.models.contracts.settings import BusinessSettingsItem
from django.db import models
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, is_object_context
from blackwidow.sales.models.common.product_price import ProductPrice
from blackwidow.sales.models.transactions.transaction import TransactionType
from config.enums.modules_enum import ModuleEnum


@decorate(expose_api('tx-price-settings'),is_object_context,
          route(route='tx-price-settings', display_name='Transaction Price Settings', module=ModuleEnum.Administration, group='Other admin'))
class TransactionPriceSettings(BusinessSettingsItem):
    transaction_type = models.CharField(max_length=500, default="")
    transaction_class = models.CharField(max_length=500, default="")
    price_type = models.ForeignKey(ProductPrice)

    @property
    def render_transaction_type(self):
        for key, value in TransactionType.get_enum_list():
            if key == str(self.transaction_type):
                return value

    @classmethod
    def table_columns(cls):
        return "code", "render_transaction_type", "price_type", "transaction_class",

    class Meta:
        unique_together = ('transaction_type', 'transaction_class',)