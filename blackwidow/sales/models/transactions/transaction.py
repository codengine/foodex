from enum import Enum
from uuid import uuid4
from django.core.urlresolvers import reverse

from django.db import models
from django.db import transaction
from django.db.models.loading import get_model
from django.utils.safestring import mark_safe

from blackwidow.core.models.clients.client import Client
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.decorators.enable_trigger import enable_trigger
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.extensions.clock import Clock
from blackwidow.sales.models.common.product_price import ProductPrice
from blackwidow.sales.models.common.product_price_config import ProductPriceConfig
from blackwidow.sales.models.invoice.invoice import Invoice
from blackwidow.sales.models.orders.secondary_order import SecondaryOrder
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from blackwidow.sales.models.transactions.transaction_status import TransactionStatus
from blackwidow.sales.signals.signals import sig_stock_out
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum
from blackwidow.core.models.common.location import Location
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.sales.models.payments.payment_methods import Payment
from config.enums.modules_enum import ModuleEnum
from blackwidow.sales.models.transactions.transaction_break_down import TxTransactionBreakdown, OrderTransactionBreakdown

__author__ = 'Mahmud'


class TransactionType(Enum):
    Buy = 0
    Sale = 1
    ReverseBuy = 10
    ReverseSale = 11
    StockAdjustment = 20
    ReverseStockAdjustment = 21
    StockIn = 22
    ReverseStockIn = 23
    StockCount = 24
    ReverseStockCount = 25
    StockTransfer = 26
    ReverseStockTransfer = 27
    StockOut = 28
    ReverseStockOut = 29

    @classmethod
    def get_enum_list(cls):
        enums = [(str(cls.Buy.value), cls.Buy.name),
                 (str(cls.Sale.value), cls.Sale.name),
                 (str(cls.ReverseBuy.value), cls.ReverseBuy.name),
                 (str(cls.ReverseSale.value), cls.ReverseSale.name),
                 (str(cls.StockAdjustment.value), cls.StockAdjustment.name),
                 (str(cls.ReverseStockAdjustment.value), cls.ReverseStockAdjustment.name),
                 (str(cls.StockIn.value), cls.StockIn.name),
                 (str(cls.ReverseStockIn.value), cls.ReverseStockIn.name),
                 (str(cls.StockCount.value), cls.StockCount.name),
                 (str(cls.ReverseStockCount.value), cls.ReverseStockCount.name),
                 (str(cls.StockTransfer.value), cls.StockTransfer.name),
                 (str(cls.ReverseStockTransfer.value), cls.ReverseStockTransfer.name)]
        return enums

    @classmethod
    def get_name(cls, value):
        if value == cls.Buy.value:
            return "Buy"
        if value == cls.Sale.value:
            return "Sale"
        if value == cls.ReverseBuy.value:
            return "ReverseBuy"
        if value == cls.ReverseSale.value:
            return "ReverseSale"
        if value == cls.StockAdjustment.value:
            return "StockAdjustment"
        if value == cls.ReverseStockAdjustment.value:
            return "ReverseStockAdjustment"
        if value == cls.StockIn.value:
            return "StockIn"
        if value == cls.ReverseStockIn.value:
            return "ReverseStockIn"
        if value == cls.StockCount.value:
            return "StockCount"
        if value == cls.ReverseStockCount.value:
            return "ReverseStockCount"
        if value == cls.StockTransfer.value:
            return "StockTransfer"
        if value == cls.ReverseStockTransfer.value:
            return "ReverseStockTransfer"
        return ""

@decorate(expose_api('transactions'), enable_trigger, save_audit_log)
class Transaction(OrganizationDomainEntity):
    infrastructure_unit = models.ForeignKey(InfrastructureUnit, null=True)
    infrastructure_unit_2 = models.ForeignKey(InfrastructureUnit,related_name="tx_infrastructure_unit_2", null=True)
    client = models.ForeignKey(Client, related_name='+', null=True)
    client_2 = models.ForeignKey(Client, related_name='tx_client_2', null=True)
    payments = models.ManyToManyField(Payment)
    transaction_time = models.BigIntegerField(default=None)
    breakdown = models.ManyToManyField(TransactionBreakDown)
    unique_id = models.CharField(max_length=40, unique=True, default='')
    location = models.ForeignKey(Location, null=True)
    transaction_type = models.IntegerField(default=0)
    sub_total = models.DecimalField(decimal_places=2, max_digits=18, default=0)
    total = models.DecimalField(decimal_places=2, max_digits=18, default=0)
    discount = models.DecimalField(decimal_places=2, max_digits=18, default=0)
    gross_discount = models.DecimalField(decimal_places=2, max_digits=18, default=0)
    special = models.BooleanField(default=False)
    invoice = models.ForeignKey(Invoice,null=True)
    status = models.IntegerField(default=TransactionStatus.Open.value)
    remarks = models.CharField(max_length=512, default='')

    def __str__(self):
        return self.code

    @classmethod
    def get_trigger_properties(cls, prefix='', expand=['counter_part']):
        return super().get_trigger_properties(prefix=prefix, expand=expand)

    @property
    def code_prefix(self, transaction_type=None):
        if transaction_type is None:
            transaction_type = self.transaction_type

        if transaction_type == TransactionType.Buy.value:
            return 'buy'
        if transaction_type == TransactionType.Sale.value:
            return 'sell'
        if transaction_type == TransactionType.ReverseBuy.value:
            return 'reverse-buy'
        if transaction_type == TransactionType.ReverseSale.value:
            return 'reverse-sale'
        if transaction_type == TransactionType.StockIn.value:
            return 'stock-in'
        if transaction_type == TransactionType.StockCount.value:
            return 'stock-count'
        if transaction_type == TransactionType.StockTransfer.value:
            return 'stock-transfer'
        return 'stock-adjustment'


    # @property
    # def sum_of_breakdown_sub_total(self):
    #     return self.breakdown.all().aggregate(Avg('sub_total'))['sub_total__avg']


    @property
    def sum_of_total_aparajita(self):
        if not self.counter_part or not self.counter_part.pre_registered:
            return self.total
        return 0


    # @property
    # def sum_of_breakdown_total(self):
    #     sum = 0
    #     for items in  self.breakdown.all():
    #         sum += items.total
    #     return sum

    @property
    def details_config(self):
        a = super().details_config
        a['transaction_type'] = TransactionType.get_name(self.transaction_type)
        return a

    @classmethod
    def get_datetime_fields(cls):
        return super().get_datetime_fields() + ['transaction_time']

    @property
    def tabs_config(self):


        tabs = [TabView(
            title='Transaction Details(s)',
            access_key='breakdown',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            related_model=TransactionBreakDown,
            property=self.breakdown
        )]
        return tabs

    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class ODESerializer(ss):
            location = Location.get_serializer()(required=True)
            breakdown = TransactionBreakDown.get_serializer()(many=True, required=True)

            def is_valid(self, raise_exception=True):
                if 'breakdown' in self.initial_data:
                    breakdown_initial_set = self.initial_data['breakdown']
                    modified_breakdown_set = []
                    for breakdown in breakdown_initial_set:
                        total_type = type(breakdown['total'])
                        subtotal_type = type(breakdown['sub_total'])
                        breakdown['sub_total'] = subtotal_type('%.2f' % breakdown['sub_total'])
                        breakdown['total'] = total_type('%.2f' % breakdown['total'])
                        modified_breakdown_set += [breakdown]

                    self.initial_data['breakdown'] = modified_breakdown_set

                return super().is_valid(raise_exception)

            def create(self, attrs, instance=None):
                attrs.update({
                    'unique_id': attrs.pop('unique_id', str(uuid4()))
                })
                return super().create(attrs)

            class Meta(ss.Meta):
                model = cls
                read_only_fields = ss.Meta.read_only_fields + ('organization', )

        return ODESerializer

    @classmethod
    def get_dependent_field_list(cls):
        return ['payments', 'breakdown']

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Details]


    @property
    def render_type(self):
        for key, value in TransactionType.get_enum_list():
            if key == str(self.transaction_type):
                return value

    @classmethod
    def default_order_by(cls):
        return '-transaction_time'

    @property
    def render_sold_to(self):
        if self.client_2:
            return self.client_2
        return self.client

    @classmethod
    def table_columns(cls):
        return 'code', 'render_type','total:amount', 'created_by', 'render_sold_to', 'transaction_time'

    def save(self, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)

    # class Meta:
    #     abstract = True


class PrimaryTransaction(Transaction):
    @classmethod
    def table_columns(cls):
        return 'code', 'infrastructure_unit:From Warehouse', 'created_by:Sold By', 'client:Sold To', 'total:amount', 'transaction_time', 'last_updated:Sync Time'

    @classmethod
    def search_client(cls, queryset, term):
        return queryset.filter(client__name__icontains=term)


    @classmethod
    def exclude_search_fields(cls):
        return [
            'total'
        ]


@decorate(expose_api('secondary-transactions'), enable_trigger, save_audit_log,
          route(route='secondary-transactions', display_name='Transaction', module=ModuleEnum.Execute, group_order=2, item_order=4, group='Secondary Sales'))
class SecondaryTransaction(Transaction):

    @classmethod
    def get_url_by_name(cls, name):
        if name == 'client' :
            return 'distributors'
        if name == 'client_2' :
            return 'clients'
        return None


    @property
    def render_code(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.pk}) + "' >" + self.code + "</a>")

    @property
    def render_from_distributor(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.client.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.client.pk}) + "' >" + str(self.client) + "</a>")

    @classmethod
    def search_from_distributor(cls, queryset, term):
        return queryset.filter(client__name__icontains=term)

    @classmethod
    def table_columns(cls):
        return 'render_code', 'render_from_distributor', 'created_by:Sold By', 'client_2:Sold To', 'total:amount', 'transaction_time', 'last_updated:Sync Time'

    @classmethod
    def exclude_search_fields(cls):
        return [
            'total', "render_available_credit", "transaction_time"
        ]

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'transaction_time']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['From Distributor'] = self.client
        d['Sold By'] = self.created_by
        d['Sold To'] = self.client_2
        d['Amount'] = self.total
        d['Sync Time'] = self.last_updated
        d['remarks'] = self.remarks

        return d

    @property
    def tabs_config(self):

        tabs = [TabView(
            title='Order Breakdown(s)',
            access_key='txorderbreakdown',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            related_model=OrderTransactionBreakdown,
            property=self.breakdown
        ), TabView(
            title='Transaction Details(s)',
            access_key='txbreakdown',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            related_model=TxTransactionBreakdown,
            property=self.breakdown
        )]
        return tabs



def receive_stock_out(sender=None, order=None, **kwargs):
    if not isinstance(order, SecondaryOrder):
        return

    from django.db import transaction
    from blackwidow.sales.models.settings.price_settings import TransactionPriceSettings

    tsettings = TransactionPriceSettings.objects.filter(transaction_class=SecondaryTransaction.__name__, transaction_type=TransactionType.Sale.value)
    if tsettings.exists():
        tsettings = tsettings.first()
    else:
        tsettings, result = TransactionPriceSettings.objects.get_or_create(transaction_class=SecondaryTransaction.__name__,
                                                                           transaction_type=TransactionType.Buy.value,
                                                                           price_type=ProductPrice.objects.first())

    with transaction.atomic():
        tx = SecondaryTransaction()
        tx.client = order.from_client
        tx.client_2 = order.to_client
        # tx.infrastructure_unit = WareHouse.objects.first()
        tx.location = order.location
        tx.transaction_time = Clock.timestamp()
        tx.transaction_type = TransactionType.Sale.value
        tx.unique_id = str(uuid4())
        tx.save()
        for ob in order.breakdown.all():
            txx = TransactionBreakDown()
            txx.product = ob.product
            txx.quantity = ob.total_items

            price, result = ProductPriceConfig.objects.get_or_create(product=txx.product, organization=order.organization, client=order.to_client, price_type=tsettings.price_type)
            if result:
                price.value = txx.product.get_price_value(tsettings.price_type.short_name)
                price.save()

            # txx.unit_price = price.value
            txx.unit_price = ob.unit_price
            txx.sub_total = ob.sub_total
            txx.discount = ob.discount
            txx.total = ob.total
            # txx.sub_total = txx.unit_price * txx.quantity
            # txx.discount = 0
            # txx.total = txx.sub_total - txx.discount


            # update client inventory
            c_inv, result = get_model("foodex","ClientInventory").objects.get_or_create(product=txx.product, assigned_to=order.from_client)
            if result:
                c_inv.stock = 0

            txx.opening_stock = c_inv.stock
            txx.closing_stock = c_inv.stock - txx.quantity
            txx.save()
            c_inv.stock = c_inv.stock - txx.quantity
            c_inv.save()


            tx.breakdown.add(txx)
            tx.discount += txx.discount
            tx.sub_total += txx.sub_total
            tx.total += txx.total

        tx.save()


sig_stock_out.connect(receive_stock_out, dispatch_uid="stock_out_secondary")