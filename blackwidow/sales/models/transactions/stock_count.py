import uuid
from blackwidow.core.managers.modelmanager import DomainEntityModelManager
from blackwidow.sales.models.transactions.transaction import Transaction, TransactionType
from django.db import transaction


class StockCount(Transaction):
    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        with transaction.atomic():
            self.unique_id = uuid.uuid4()
            self.transaction_type = TransactionType.StockCount.value
            super().save(*args, **kwargs)


