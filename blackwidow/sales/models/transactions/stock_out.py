from uuid import uuid4
from django.db.models.loading import get_model
from blackwidow.core.models.common.location import Location
from blackwidow.core.models.structure.warehouse import WareHouse
from blackwidow.engine.extensions.clock import Clock
from blackwidow.sales.models.common.product_price import ProductPrice, ProductPriceEnum
from blackwidow.sales.models.common.product_price_config import ProductPriceConfig
from blackwidow.sales.models.inventory.client_inventory import ClientInventory
from blackwidow.sales.models.invoice.invoice import Invoice
from blackwidow.sales.models.orders.order import Order
from blackwidow.sales.models.products.client_onroute_inventory import ClientOnRouteInventory
from blackwidow.sales.models.products.product_inventory import ProductInventory
from blackwidow.sales.models.settings.price_settings import TransactionPriceSettings
from blackwidow.sales.models.transactions.stock_adjustment import StockAdjustmentTransaction
from blackwidow.sales.models.transactions.transaction import TransactionType, PrimaryTransaction, Transaction
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from blackwidow.sales.signals.signals import sig_stock_out
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.inventory.warehouse_inventory import WarehouseInventory
from foodex.models.invoice.open_invoice import  OpenInvoice
from foodex.models.products.approved_damage_products import ApprovedDamageProducts
from foodex.models.products.cleared_damage_products import ClearedDamageProducts


class StockOut(PrimaryTransaction):
    def details_link_config(self, **kwargs):
        return PrimaryTransaction.details_link_config() + [ViewActionEnum.Print]

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Details]

    class Meta:
        proxy = True


def receive_stock_out(sender=None, order=None, **kwargs):
    from django.db import transaction

    if not isinstance(order, Order):
        return

    tsettings = TransactionPriceSettings.objects.filter(transaction_class=StockOut.__name__, transaction_type=TransactionType.Sale.value)
    if tsettings.exists():
        tsettings = tsettings.first()
    else:
        tsettings, result = TransactionPriceSettings.objects.get_or_create(transaction_class=StockOut.__name__,
                                                                    transaction_type=TransactionType.Buy.value,
                                                                    price_type=ProductPrice.objects.first())

    with transaction.atomic():
        # tx = StockOut()
        # tx.client = order.client
        # tx.infrastructure_unit = order.warehouse
        # tx.location = order.location
        # tx.transaction_time = Clock.timestamp()
        # tx.transaction_type = TransactionType.Sale.value
        # tx.unique_id = str(uuid4())
        # tx.save()

        tx = PrimaryTransaction()
        tx.client = order.client
        tx.infrastructure_unit = order.warehouse
        tx.location = order.location
        tx.transaction_time = Clock.timestamp()
        tx.transaction_type = TransactionType.Sale.value
        tx.unique_id = str(uuid4())
        tx.invoice = order.invoice
        tx.created_by = kwargs.get('user')
        tx.save()


        tx_wh_adjustment = PrimaryTransaction()
        tx_wh_adjustment.organization = kwargs.get("organization")
        tx_wh_adjustment.client = order.client
        tx_wh_adjustment.infrastructure_unit = order.warehouse
        tx_wh_adjustment.transaction_type = TransactionType.StockAdjustment.value
        tx_wh_adjustment.location = order.location
        tx_wh_adjustment.transaction_time = Clock.timestamp()
        tx_wh_adjustment.unique_id = str(uuid4())
        tx_wh_adjustment.created_by = kwargs.get("user")
        tx_wh_adjustment.save()

        tx_client_adjustment = PrimaryTransaction()
        tx_client_adjustment.organization = kwargs.get("organization")
        tx_client_adjustment.client = order.client
        tx_client_adjustment.infrastructure_unit = order.warehouse
        tx_client_adjustment.transaction_type = TransactionType.StockAdjustment.value
        tx_client_adjustment.location = order.location
        tx_client_adjustment.transaction_time = Clock.timestamp()
        tx_client_adjustment.unique_id = str(uuid4())
        tx_client_adjustment.created_by = kwargs.get("user")
        tx_client_adjustment.is_active = False
        tx_client_adjustment.save()

        tsubtotal = 0
        tdiscount = 0
        ttotal = 0


        for ob in order.breakdown.all():
            txx = TransactionBreakDown()
            txx.product = ob.product
            txx.quantity = ob.total_items

            product_unit_price = getattr(ob.product,'get_gross_cust_price')

            txx.unit_price = product_unit_price
            txx.sub_total = txx.unit_price * txx.quantity
            txx.discount = ob.discount
            txx.total = txx.sub_total - txx.discount
            txx.save()

            txx_client = TransactionBreakDown()
            txx_client.product = ob.product
            txx_client.quantity = ob.total_items
            txx_client.unit_price = product_unit_price
            txx_client.sub_total = txx_client.unit_price * txx_client.quantity
            txx_client.discount = ob.discount
            txx_client.total = txx_client.sub_total - txx_client.discount
            txx_client.save()


            tx.breakdown.add(txx)
            tx_wh_adjustment.breakdown.add(txx)
            tx_client_adjustment.breakdown.add(txx_client)

            tsubtotal += txx.sub_total
            tdiscount += txx.discount
            ttotal += (txx.sub_total - txx.discount)

            # update warehouse inventory
            whouse_inventories = WarehouseInventory.objects.filter(assigned_to=tx.infrastructure_unit, product=txx.product)
            if whouse_inventories.exists():
                wi = whouse_inventories.first()
                txx.opening_stock = wi.stock
                txx.closing_stock = wi.stock - txx.quantity
                txx.save()

                wi.stock = wi.stock - txx.quantity
                wi.save()
            else:
                txx.opening_stock = 0
                txx.closing_stock = txx.quantity
                txx.save()

                whouse_inventory = WarehouseInventory()
                whouse_inventory.assigned_to = wi.assigned_to
                whouse_inventory.product = txx.product
                whouse_inventory.organization = kwargs.get("organization")
                whouse_inventory.created_by = kwargs.get("user")
                whouse_inventory.stock = txx.quantity
                whouse_inventory.save()

            client_or_inventory = ClientOnRouteInventory()
            client_or_inventory.assigned_to = tx.client
            client_or_inventory.product = txx.product
            client_or_inventory.stock = txx.quantity
            client_or_inventory.save()

            client_inventories = ClientInventory.objects.filter(assigned_to=order.client,product=ob.product)
            if client_inventories.exists():
                ci = client_inventories.first()
                txx_client.opening_stock = ci.stock
                txx_client.closing_stock = ci.stock + txx_client.quantity
                txx_client.save()
            else:
                txx_client.opening_stock = 0
                txx_client.closing_stock = txx_client.quantity
                txx_client.save()

        tx.sub_total = tsubtotal
        tx.discount = tdiscount
        tx.total = ttotal
        tx.save()

        tx_wh_adjustment.sub_total = tsubtotal
        tx_wh_adjustment.discount = tdiscount
        tx_wh_adjustment.total = ttotal
        tx_wh_adjustment.save()

        tx_client_adjustment.sub_total = tsubtotal
        tx_client_adjustment.discount = tdiscount
        tx_client_adjustment.total = ttotal
        tx_client_adjustment.save()

        tx.save()
        tx.client.available_credit -= tx.total
        tx.client.save()


        ### create open invoice
        open_inv = order.invoice
        cls = get_model('foodex', 'OpenInvoice')
        open_inv.type = cls.__name__
        open_inv.__class__ = cls
        open_inv.save()
        # approved_damage_products = ApprovedDamageProducts.objects.filter(client=order.client)
        # for damage_product in approved_damage_products:
        #     damage_product.type = ClearedDamageProducts.__name__
        #     damage_product.__class__ = ClearedDamageProducts
        #     damage_product.save()



sig_stock_out.connect(receive_stock_out, dispatch_uid="stock_out")