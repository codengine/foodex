from django.db import models
from rest_framework import serializers

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.transactions.transaction_status import TransactionStatus

__author__ = 'Mahmud'


class TransactionBreakDown(OrganizationDomainEntity):
    product = models.ForeignKey(Product)
    quantity = models.BigIntegerField()
    unit_price = models.DecimalField(decimal_places=2, max_digits=18,default=0)
    sub_total = models.DecimalField(decimal_places=2, max_digits=18,default=0)
    total = models.DecimalField(decimal_places=2, max_digits=18,default=0)
    discount = models.DecimalField(decimal_places=2, max_digits=18,default=0)
    opening_stock = models.BigIntegerField(default=0)
    closing_stock = models.BigIntegerField(default=0)
    stock_received = models.IntegerField(default=0)
    # change_stock_quantity = models.BigIntegerField(default=0,null=True,blank=True)
    special = models.BooleanField(default=False)
    status = models.IntegerField(default=TransactionStatus.Open.value)

    @property
    def render_product_code(self):
        return self.product.code

    @property
    def render_type(self):
        from blackwidow.sales.models.transactions.transaction import Transaction
        return Transaction.objects.get(breakdown=self).render_type

    @property
    def render_bangla_name(self):
        return self.product.name_bd

    @property
    def render_manufacturer(self):
        return self.product.manufacturer

    @property
    def render_brand(self):
        return self.product.brand

    @property
    def render_special(self):
        return 'True' if self.special else ''

    @classmethod
    def table_columns(cls):
        return 'code', 'product:Product Name', 'opening_stock:Previous quantity', 'quantity:Change of quantity', 'closing_stock:Updated quantity'

    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class ODESerializer(ss):
            product = serializers.PrimaryKeyRelatedField(required=True, queryset=Product.objects.all())
            quantity = serializers.IntegerField(required=True)
            unit_price = serializers.DecimalField(decimal_places=2, max_digits=18, required=True)
            sub_total = serializers.DecimalField(decimal_places=2, max_digits=18, required=True)
            total = serializers.DecimalField(decimal_places=2, max_digits=18, required=True)
            discount = serializers.DecimalField(decimal_places=2, max_digits=18, required=True)
            opening_stock = serializers.IntegerField(default=0, required=False)
            closing_stock = serializers.IntegerField(default=0, required=False)
            stock_received = serializers.IntegerField(default=0, required=False)
            special = serializers.BooleanField(default=False, required=False)

            def is_valid(self, raise_exception=True):
                total_type = type(self.initial_data['total'])
                subtotal_type = type(self.initial_data['sub_total'])
                self.initial_data['sub_total'] = subtotal_type('%.2f' % self.initial_data['sub_total'])
                self.initial_data['total'] = total_type('%.2f' % self.initial_data['total'])
                return super().is_valid(raise_exception)

            class Meta(ss.Meta):
                model = cls

        return ODESerializer

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)


class TxTransactionBreakdown(TransactionBreakDown):
    class Meta:
        proxy = True

    @classmethod
    def table_columns(cls):
        return 'code', 'product:Product Name', 'opening_stock:Previous quantity', 'quantity:Change of quantity', 'closing_stock:Updated quantity'


class OrderTransactionBreakdown(TransactionBreakDown):
    class Meta:
        proxy = True

    @classmethod
    def table_columns(cls):
        return 'code', 'product:Product Name', 'quantity:Total Items', 'unit_price', 'sub_total', 'total'

