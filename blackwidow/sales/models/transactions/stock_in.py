import uuid
from blackwidow.core.managers.modelmanager import DomainEntityModelManager
from blackwidow.engine.extensions import Clock
from blackwidow.sales.models.transactions.transaction import Transaction, TransactionType
from django.db import transaction


class StockIn(Transaction):
    objects = DomainEntityModelManager(filter={'transaction_type': TransactionType.StockIn.value})

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        with transaction.atomic():
            self.unique_id = uuid.uuid4()
            self.transaction_type = TransactionType.StockIn.value
            self.transaction_time = Clock.timestamp()
            super().save(*args, **kwargs)


