from django.db import models
from rest_framework import serializers

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.sales.models.products.product import Product


__author__ = 'Sohel'


class StockTransferTransactionBreakDown(OrganizationDomainEntity):
    product = models.ForeignKey(Product)
    quantity = models.BigIntegerField()
    unit_price = models.DecimalField(decimal_places=2, max_digits=18)
    sub_total = models.DecimalField(decimal_places=2, max_digits=18)
    total = models.DecimalField(decimal_places=2, max_digits=18)
    discount = models.DecimalField(decimal_places=2, max_digits=18)
    opening_balance = models.BigIntegerField(default=0, null=True)
    closing_balance = models.BigIntegerField(default=0, null=True)
    special = models.BooleanField(default=False)

    @property
    def render_product_code(self):
        return self.product.code

    @property
    def render_type(self):
        from blackwidow.sales.models.transactions.transaction import Transaction
        return Transaction.objects.get(breakdown=self).render_type

    @property
    def render_bangla_name(self):
        return self.product.name_bd

    @property
    def render_manufacturer(self):
        return self.product.manufacturer

    @property
    def render_brand(self):
        return self.product.brand

    @property
    def render_special(self):
        return 'True' if self.special else ''

    @classmethod
    def table_columns(cls):
        return 'code', 'render_product_code', 'render_manufacturer', 'product', 'quantity'

    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class ODESerializer(ss):
            product = serializers.PrimaryKeyRelatedField(required=True, queryset=Product.objects.all())
            quantity = serializers.IntegerField(required=True)
            unit_price = serializers.DecimalField(decimal_places=2, max_digits=18, required=True)
            sub_total = serializers.DecimalField(decimal_places=2, max_digits=18, required=True)
            total = serializers.DecimalField(decimal_places=2, max_digits=18, required=True)
            discount = serializers.DecimalField(decimal_places=2, max_digits=18, required=True)
            opening_balance = serializers.IntegerField(default=0, required=False)
            closing_balance = serializers.IntegerField(default=0, required=False)
            special = serializers.BooleanField(default=False, required=False)

            def is_valid(self, raise_exception=True):
                total_type = type(self.initial_data['total'])
                subtotal_type = type(self.initial_data['sub_total'])
                self.initial_data['sub_total'] = subtotal_type('%.2f' % self.initial_data['sub_total'])
                self.initial_data['total'] = total_type('%.2f' % self.initial_data['total'])
                return super().is_valid(raise_exception)

            class Meta(ss.Meta):
                model = cls

        return ODESerializer
