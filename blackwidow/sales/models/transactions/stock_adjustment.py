__author__ = 'Sohel'

from blackwidow.core.managers.modelmanager import DomainEntityModelManager
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, is_object_context
from blackwidow.sales.models.transactions.transaction import Transaction, TransactionType
from config.enums.modules_enum import ModuleEnum
from django.db import transaction


@decorate(is_object_context)
class StockAdjustmentTransaction(Transaction):
    objects = DomainEntityModelManager(filter={'transaction_type': TransactionType.StockAdjustment.value})


    @property
    def render_transaction_qty(self):
        if self.breakdown.exists():
            return self.breakdown.first().quantity

    @property
    def render_product_name(self):
        if self.breakdown.exists():
            return self.breakdown.first().product.name + " " + self.breakdown.first().product.description

    @property
    def render_stock_before(self):
        return self.breakdown.first().opening_balance

    @property
    def render_stock_after(self):
        return self.breakdown.first().closing_balance

    @classmethod
    def table_columns(cls):
        # return 'code', 'infrastructure_unit:hub', 'created_by:Transaction By', 'render_product_name', 'render_stock_before','render_transaction_qty','render_stock_after', 'last_updated:Sync Time'
        return 'code', 'infrastructure_unit:Warehouse', 'created_by:Transaction By', 'last_updated:Sync Time'

    def save(self, *args, **kwargs):
        with transaction.atomic():
            self.transaction_type = TransactionType.StockAdjustment.value
            super().save(*args, **kwargs)

    class Meta:
        proxy = True