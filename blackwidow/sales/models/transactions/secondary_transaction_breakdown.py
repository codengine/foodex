from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown

__author__ = 'Sohel'

class SecondaryTransactionBreakdown(TransactionBreakDown):
    class Meta:
        proxy = True
