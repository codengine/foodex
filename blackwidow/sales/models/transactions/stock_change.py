from blackwidow.engine.decorators.enable_trigger import enable_trigger
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import save_audit_log, decorate, is_object_context
from blackwidow.sales.models.transactions.transaction import PrimaryTransaction
from config.enums.modules_enum import ModuleEnum


@decorate(expose_api('tx-stock-change'), is_object_context,
          enable_trigger, save_audit_log)
class StockChange(PrimaryTransaction):
    class Meta:
        proxy = True