from enum import Enum

__author__ = 'Sohel'

class TransactionStatus(Enum):
    Open = 1
    Close = 2
    InTransit = 3

    @classmethod
    def get_enum_list(cls):
        enums = [(str(cls.Open.value), cls.Open.name),
                 (str(cls.Close.value), cls.Close.name),
                 (str(cls.InTransit.value), cls.InTransit.name)]
        return enums

    @classmethod
    def get_name(cls, value):
        if value == cls.Open.value:
            return "Open"
        if value == cls.Close.value:
            return "Close"
        if value == cls.InTransit.value:
            return "InTransit"
        return ""
