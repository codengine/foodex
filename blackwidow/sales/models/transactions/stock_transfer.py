import uuid
from blackwidow.core.managers.modelmanager import DomainEntityModelManager
from blackwidow.sales.models.transactions.transaction import Transaction, TransactionType
from django.db import transaction


class StockTransfer(Transaction):

    objects = DomainEntityModelManager(filter={'transaction_type': TransactionType.StockTransfer.value})

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        with transaction.atomic():
            self.unique_id = uuid.uuid4()
            self.transaction_type = TransactionType.StockTransfer.value
            super().save(*args, **kwargs)
