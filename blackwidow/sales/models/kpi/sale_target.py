import datetime
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField

from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.engine.decorators.enable_bulk_edit import enable_bulk_edit
from blackwidow.engine.decorators.enable_export import enable_export
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, is_object_context
from blackwidow.core.models.contracts.kpi_base import PerformanceIndex, MonthlyKPI
from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
from blackwidow.core.models.config.importer_config import ImporterConfig
from config.enums.modules_enum import ModuleEnum
from django.db import models


__author__ = 'activehigh'


# @decorate(enable_import, enable_export, is_object_context,
#           expose_api('kpi'),
#           route(route='kpi', display_name='KPI', module=ModuleEnum.Targets, group='Targets'))
class SaleTargetPerformanceIndex(MonthlyKPI):

    @classmethod
    def get_search_form(cls, search_fields, fields):
        from django import forms

        sf_instance = PerformanceIndex.get_search_form(search_fields, fields)

        c_label, c_value = cls.get_search_value('user:id', search_fields, fields)

        sf_instance.fields['user:id'] = GenericModelChoiceField(label='User', empty_label="( -- Default -- )", required=False,  initial=c_value, queryset=ConsoleUser.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
        sf_instance.fields['user:id'].readonly = False

        return sf_instance

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if result or importer_config.columns.count() == 0:
            importer_config.starting_row = 2
            importer_config.save(**kwargs)
        else:
            for items in importer_config.columns.all():
                items.delete()
        columns = [
            ImporterColumnConfig(column=0, column_name='Year', property_name='start_time_year', ignore=False),
            ImporterColumnConfig(column=1, column_name='Reference ID', property_name='reference_id', ignore=False, is_unique=True),
            ImporterColumnConfig(column=2, column_name='Code', property_name='code', ignore=False, is_unique=True),
            ImporterColumnConfig(column=3, column_name='Name', property_name='name', ignore=False),

            ImporterColumnConfig(column=4, column_name='January Target', property_name='target', ignore=False),
            ImporterColumnConfig(column=5, column_name='January Achieved', property_name='achieved', ignore=False),

            ImporterColumnConfig(column=6, column_name='February Target', property_name='target', ignore=False),
            ImporterColumnConfig(column=7, column_name='February Achieved', property_name='achieved', ignore=False),

            ImporterColumnConfig(column=8, column_name='March Target', property_name='target', ignore=False),
            ImporterColumnConfig(column=9, column_name='March Achieved', property_name='achieved', ignore=False),

            ImporterColumnConfig(column=10, column_name='April Target', property_name='target', ignore=False),
            ImporterColumnConfig(column=11, column_name='April Achieved', property_name='achieved', ignore=False),

            ImporterColumnConfig(column=12, column_name='May Target', property_name='target', ignore=False),
            ImporterColumnConfig(column=13, column_name='May Achieved', property_name='achieved', ignore=False),

            ImporterColumnConfig(column=14, column_name='June Target', property_name='target', ignore=False),
            ImporterColumnConfig(column=15, column_name='June Achieved', property_name='achieved', ignore=False),

            ImporterColumnConfig(column=16, column_name='July Target', property_name='target', ignore=False),
            ImporterColumnConfig(column=17, column_name='July Achieved', property_name='achieved', ignore=False),

            ImporterColumnConfig(column=18, column_name='August Target', property_name='target', ignore=False),
            ImporterColumnConfig(column=19, column_name='August Achieved', property_name='achieved', ignore=False),

            ImporterColumnConfig(column=20, column_name='September Target', property_name='target', ignore=False),
            ImporterColumnConfig(column=21, column_name='September Achieved', property_name='achieved', ignore=False),

            ImporterColumnConfig(column=22, column_name='October Target', property_name='target', ignore=False),
            ImporterColumnConfig(column=23, column_name='October Achieved', property_name='achieved', ignore=False),

            ImporterColumnConfig(column=24, column_name='November Target', property_name='target', ignore=False),
            ImporterColumnConfig(column=25, column_name='November Achieved', property_name='achieved', ignore=False),

            ImporterColumnConfig(column=26, column_name='December Target', property_name='target', ignore=False),
            ImporterColumnConfig(column=27, column_name='December Achieved', property_name='achieved', ignore=False),
        ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config

    @classmethod
    def importer_get_user(cls, reference_id=None, code=None, name=None):
        if reference_id is None or reference_id == '':
            if code is None or code == '':
                if name is None or name == '':
                    return None
                else:
                    _users = ConsoleUser.objects.filter(name=name.strip())
            else:
                _users = ConsoleUser.objects.filter(code=code.strip())
        else:
            _users = ConsoleUser.objects.filter(reference_id=reference_id.strip())

        if _users.exists():
            return _users.first()
        return None

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        _start_month_index = 4
        _next_achieved_index_offset = 1
        _user = cls.importer_get_user(reference_id=data['1'], code=data['2'], name=data['3'])

        if _user is None:
            return []

        items = []
        for x in range(0, 12):
            _target = data[str((x * 2) + _start_month_index)]
            _achieved = data[str((x * 2) + _start_month_index + _next_achieved_index_offset)]

            _next_month = 1 + x + 1
            _next_year = int(data['0'])

            if _next_month > 12:
                _next_month = 1
                _next_year = _next_year + 1

            _start_time = datetime.datetime(int(data['0']), 1 + x, 1, 0, 0, 0).timestamp() * 1000
            _end_time = datetime.datetime(_next_year, _next_month, 1, 0, 0, 0).timestamp() * 1000
            kpi = SaleTargetPerformanceIndex.objects.filter(user=_user, start_time=_start_time, end_time=_end_time)
            if kpi.exists():
                _kpi = kpi.first()
            else:
                _kpi = SaleTargetPerformanceIndex()
                _kpi.start_time = _start_time
                _kpi.end_time = _end_time
                _kpi.user = _user
            _kpi.target = _target
            _kpi.month = _next_month
            _kpi.achieved = _achieved
            _kpi.is_active = True
            _kpi.save(organization=config.organization)
            items.append(_kpi.pk)