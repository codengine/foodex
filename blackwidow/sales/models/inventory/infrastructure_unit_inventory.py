from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.sales.models.inventory.inventory import Inventory
from django.db import models

__author__ = 'Sohel'

class InfrastructureUnitInventory(Inventory):
    assigned_to = models.ForeignKey(InfrastructureUnit)


