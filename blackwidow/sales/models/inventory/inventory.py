from blackwidow.core.models.contracts.manufacturer_domain_entity import ManufacturerDomainEntity
from django.db import models
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.transactions.transaction import Transaction

__author__ = 'Sohel'

class Inventory(ManufacturerDomainEntity):
    product = models.ForeignKey(Product)
    stock = models.BigIntegerField(default=0)

    class Meta:
        abstract = True
