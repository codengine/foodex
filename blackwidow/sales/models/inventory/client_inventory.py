from blackwidow.core.models.clients.client import Client
from blackwidow.sales.models.inventory.inventory import Inventory
from django.db import models

__author__ = 'Sohel'

class ClientInventory(Inventory):
    assigned_to = models.ForeignKey(Client)


