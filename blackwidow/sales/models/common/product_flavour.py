from blackwidow.core.models.contracts.configurabletype import ConfigurableType

__author__ = 'Mahmud'

# @decorate(route(route='flavours', group='Products', module=ModuleEnum.Administration, display_name="Flavour"))
class ProductFlavour(ConfigurableType):

    def get_choice_name(self):
        return self.name
