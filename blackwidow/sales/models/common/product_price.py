from enum import Enum
from django.db import models

from blackwidow.core.models.contracts.base import DomainEntity
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from config.enums.modules_enum import ModuleEnum


__author__ = 'Mahmud'

class ProductPriceEnum(Enum):
    def make_price_couple(name,short_name):
        return {
            "name":name,
            "short_name":short_name
        }
    ref_cost = make_price_couple("Ref Cost","RefC")
    ref_price = make_price_couple("Ref Price","RefP")
    ref_net_cust_markup = make_price_couple("Ref Net Customer markup/discount","Rf-NtCu-markup")
    net_cust_price = make_price_couple("Net Customer price","Net-CustP")
    vat_rate = make_price_couple("Vat Rate","VAT")
    gross_cust_price = make_price_couple("Gross Customer Price","Gross-CustP")
    gross_cust_distributor_markup = make_price_couple("Gross Customer Distributor markup","Gross-CusDi-markup")
    dist_price = make_price_couple("Dist price","DP")
    dist_retailer_markup = make_price_couple("Distributor Retailer markup","Di-Rt-markup")
    retail_price = make_price_couple("Retail Price","RP")

@decorate(expose_api('price-types'), save_audit_log, is_object_context,
          route(route='price-types', group='Other admin', module=ModuleEnum.Administration, display_name="Price Type"))
class ProductPrice(DomainEntity):
    context = models.CharField(max_length=255, null=True)
    key = models.CharField(max_length=255, null=True)
    name = models.CharField(max_length=8000, null=True)
    short_name = models.CharField(max_length=200, default='')
    short_code = models.CharField(max_length=200, default='')
    parents = models.ManyToManyField('self', related_name='+')
    equation = models.CharField(default='', max_length=500)
    is_mobile_sale_price = models.BooleanField(default=0)

    def get_choice_name(self):
        return self.short_name + "(" + self.name + ")"


