from blackwidow.core.models.contracts.configurabletype import ConfigurableType
from blackwidow.engine.decorators.utility import decorate, loads_initial_data

__author__ = 'Mahmud'


@decorate(loads_initial_data)
class ProductUnit(ConfigurableType):

    def get_choice_name(self):
        return self.name
