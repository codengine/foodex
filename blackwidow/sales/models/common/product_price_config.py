from django.core.urlresolvers import reverse
from django.db.models.loading import get_model
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.mixins.modelmixin.export_model_mixin import ExportModelMixin
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig
from blackwidow.core.models.config.exporter_config import ExporterConfig
from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
from blackwidow.core.models.config.importer_config import ImporterConfig
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from django.db import models, transaction
from blackwidow.core.models.organizations.organization import Organization
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.engine.decorators import enable_import
from blackwidow.engine.decorators.enable_bulk_edit import enable_bulk_edit
from blackwidow.engine.decorators.enable_export import enable_export
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, get_models_with_decorator, save_audit_log, loads_initial_data, \
    is_object_context
from blackwidow.engine.extensions.clock import Clock
from blackwidow.sales.models.common.product_price import ProductPrice, ProductPriceEnum
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.signals.signals import sig_product_added, sig_client_added, sig_iunit_added
from config.apps import INSTALLED_APPS
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum
from django import forms


class ProductPriceConfig(OrganizationDomainEntity, ExportModelMixin):
    product = models.ForeignKey(Product)
    value = models.DecimalField(decimal_places=2, max_digits=20, default=0)
    price_type = models.ForeignKey(ProductPrice)
    is_default = models.BooleanField(default=True)
    client = models.ForeignKey(Client, null=True)
    client_type = models.CharField(default='', max_length=200,null=True,blank=True)
    infrastructure_unit = models.ForeignKey(InfrastructureUnit,null=True)
    infrastructure_unit_type = models.CharField(default='',max_length=200,null=True,blank=True)


    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Delete, ViewActionEnum.AdvancedImport, ViewActionEnum.AdvancedExport]

    @classmethod
    def get_export_dependant_fields(self):
        class AdvancedExportDependentForm(GenericFormMixin):
            def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
                super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
                self.fields['infrastructure_unit___1__id'] = GenericModelChoiceField(label='Area', empty_label="Select Area", required=False,  queryset=InfrastructureUnit.objects.filter(type="Area"), widget=forms.Select(attrs={'class': 'select2'}))
                self.fields['client___1__id'] = GenericModelChoiceField(label='Client', empty_label="Select Client", required=False, queryset=Client.objects.all(), widget=forms.TextInput(attrs={'class': 'select2-input', 'width': '220', 'data-depends-on': 'infrastructure_unit___1__id', 'data-depends-property': 'assigned_to:'
                                                                                                                                                                                                                                                                                                                       'id', 'data-url': reverse(get_model("core","Client").get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))

        return AdvancedExportDependentForm

    @property
    def render_distributor(self):
        return self.client

    @property
    def render_client(self):
        return self.client

    @property
    def render_area(self):
        return self.infrastructure_unit

    @classmethod
    def filter_query(cls,query_set,custom_search_fields=[]):
        from django.db.models import Q
        from functools import reduce
        import operator
        client_id = 0
        for key, value in custom_search_fields:
            if key.startswith("__search__text__client"):
                try:
                    or_list = []
                    values = value.split(",")
                    for v in values:
                        or_list.append(Q(client__name__icontains=v))
                    pp_config = ProductPriceConfig.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=pp_config.values_list('pk', flat=True))
                except:
                    pass
            elif key.startswith("_search_gross_unit_price:product:id"):
                try:
                    product_object = Product.objects.get(pk=int(value))
                    product_price_config = ProductPriceConfig.objects.filter(product=product_object, price_type__short_name=ProductPriceEnum.gross_cust_price.value["short_name"])
                    query_set = query_set.filter(pk__in=product_price_config.values_list('pk', flat=True))
                except:
                    query_set = cls.objects.none()
            elif key.startswith("_search_gross_unit_price:client:id"):
                try:
                    client_object = Client.objects.get(pk=int(value))
                    query_set = query_set.filter(client=client_object, price_type__short_name=ProductPriceEnum.gross_cust_price.value["short_name"])
                except:
                    query_set = cls.objects.none()
            elif key.startswith("_search_dist_unit_price:product:id"):
                try:
                    product_object = Product.objects.get(pk=int(value))
                    product_price_config = ProductPriceConfig.objects.filter(product=product_object, price_type__short_name=ProductPriceEnum.dist_price.value["short_name"])
                    query_set = query_set.filter(pk__in=product_price_config
.values_list('pk', flat=True))
                except:
                    query_set = cls.objects.none()
            elif key.startswith("_search_dist_unit_price:client:id"):
                try:
                    client_object = Client.objects.get(pk=int(value))
                    query_set = query_set.filter(client=client_object, price_type__short_name=ProductPriceEnum.dist_price.value["short_name"])
                except:
                    query_set = cls.objects.none()

        return query_set

    @classmethod
    def get_url_by_name(cls, name):
        if name == 'infrastructure_unit' :
            return 'areas'
        if name == 'client' :
            return '__search__text__client'
        return None

    @classmethod
    def table_columns(cls):
        return 'code', 'product', 'infrastructure_unit:Area', 'client', 'price_type', 'is_default', 'value', \
               'date_created', 'last_updated'

    @property
    def product_price_config_id(self):
        return self.pk

    @property
    def infrastructure_unit_name(self):
        return self.infrastructure_unit.name if self.infrastructure_unit else ""

    @property
    def client_name(self):
        return self.client.name if self.client else ""

    @property
    def price_type_name(self):
        return self.price_type.name if self.price_type else ""

    @property
    def product_name(self):
        return self.product.name+' '+self.product.description

    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        if not organization:
            organization = Organization.objects.all().first()
        exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
        if not exporter_configs.exists():
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.save(**kwargs)
        else:
            for e in exporter_configs:
                e.delete()
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.save(**kwargs)

        columns = [
            ExporterColumnConfig(column=0, column_name='Product Price Config Id', property_name='product_price_config_id', ignore=False),
            ExporterColumnConfig(column=1, column_name='Product', property_name='product_name', ignore=False),
            ExporterColumnConfig(column=2, column_name='Price Type', property_name='price_type_name', ignore=False),
            ExporterColumnConfig(column=3, column_name='Area', property_name='infrastructure_unit_name', ignore=False),
            ExporterColumnConfig(column=4, column_name='Distributor', property_name='client_name', ignore=False),
            ExporterColumnConfig(column=5, column_name='Value', property_name='value', ignore=False)
        ]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)
        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):
        for column in columns:
            workbook.cell(row=row_number, column=column.column + 1).value = str(getattr(self, column.property_name))
        return self.pk, row_number + 1

    @classmethod
    def finalize_export(cls, workbook=None, row_number=None, query_set=None, **kwargs):
        file_name = 'ProductPriceConfig' + str(Clock.timestamp(_format='ms'))
        return workbook, file_name

    @classmethod
    def initialize_export(cls, workbook=None, columns=None, row_number=None, query_set=None, **kwargs):

        query_params = kwargs.get("query_params")
        if query_params:
            infrastructure_unit_id =  query_params.get("infrastructure_unit:id")
            client_id = query_params.get("client:id")
            try:
                infrastructure_unit_id = int(infrastructure_unit_id)
                client_id = int(client_id)
            except Exception:
                infrastructure_unit_id = -1
                client_id = -1
            if infrastructure_unit_id > 0:
                iunit_objects = InfrastructureUnit.objects.filter(pk=infrastructure_unit_id)
                if iunit_objects.exists():
                    iunit = iunit_objects.first()
                    price_types = list(ProductPrice.objects.values_list('pk', 'short_name'))
                    all_products = Product.objects.all()
                    receive_infrastructureunit_added(iunit=iunit,products=all_products,price_types=price_types)
                    if client_id > 0:
                        client_objects = Client.objects.filter(pk=client_id)
                        if client_objects.exists():
                            client = client_objects.first()
                            receive_client_added(client=client,products=all_products,price_types=price_types)

        for column in columns:
            workbook.cell(row=1, column=column.column + 1).value = column.column_name
        return workbook, row_number

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        importer_config, created = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if created or importer_config.columns.count() == 0:
            importer_config.starting_row = 1
            importer_config.save(**kwargs)
        else:
            for items in importer_config.columns.all():
                items.delete()
        columns = [
            ImporterColumnConfig(column=0, column_name='Product Price Config Id', property_name='product_price_config_id', ignore=False),
            ImporterColumnConfig(column=1, column_name='Product', property_name='product_name', ignore=True),
            ImporterColumnConfig(column=2, column_name='Price Type', property_name='price_type_name', ignore=True),
            ImporterColumnConfig(column=3, column_name='Area', property_name='infrastructure_unit_name', ignore=True),
            ImporterColumnConfig(column=4, column_name='Distributor', property_name='client_name', ignore=True),
            ImporterColumnConfig(column=5, column_name='Value', property_name='value', ignore=False)
        ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.starting_row = 1
            importer_config.columns.add(c)
        return importer_config

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        ppc_objs = ProductPriceConfig.objects.filter(pk=int(data['0']))
        if ppc_objs.exists():
            with transaction.atomic():
                ppc = ppc_objs.first()
                ppc.value = float(data['5'])
                ppc.save()
            return ppc.pk
        return 0


    @classmethod
    def init_default_objects(cls, user=None, organization=None):
        all_prices = ProductPrice.objects.values_list('pk', 'short_name')
        all_products = Product.objects.all()
        all_clients = Client.objects.values_list('pk', 'type')

        for c, ct in all_clients:
            for p in all_products:
                for pp, pn in all_prices:
                    if ProductPriceConfig.objects.filter(
                            organization=organization,
                            is_default=True,
                            client__id=c,
                            product__id=p.pk,
                            price_type__id=pp).exists():
                        pass
                    else:
                        ppc = ProductPriceConfig()
                        ppc.organization = organization
                        ppc.price_type_id = pp
                        ppc.product = p
                        ppc.value = p.get_price_value(pn)
                        ppc.is_default = True
                        ppc.client_id = c
                        ppc.client_type = ct
                        ppc.save()

def receive_product_added(sender=None, product=None, **kwargs):
    if not product:
        return
    ip_models = get_models_with_decorator('enable_individual_assignment', INSTALLED_APPS, include_class=True)
    price_types = list(ProductPrice.objects.values_list('pk', 'short_name'))
    for ip_model in ip_models:
        _models = ip_model.get_model_meta('enable_individual_assignment', 'models')
        if _models is not None and ProductPriceConfig in _models:
            all_objects = ip_model.objects.all() #list(ip_model.objects.values_list('pk', flat=True))
            for object in all_objects:
                for p_type_pk, p_type_name in price_types:
                    with transaction.atomic():
                        if object.__class__.__name__ == InfrastructureUnit.__name__:
                            if ProductPriceConfig.objects.filter(
                                    organization=product.organization,
                                    is_default=True,
                                    infrastructure_unit__id=object.pk,
                                    product=product,
                                    client__isnull=True,
                                    price_type__id=p_type_pk).exists():
                                pass
                            else:
                                ppc = ProductPriceConfig()
                                ppc.organization = product.organization
                                ppc.price_type_id = p_type_pk
                                ppc.product = product
                                ppc.value = product.get_price_value(p_type_name)
                                ppc.is_default = True
                                ppc.infrastructure_unit_id = object.pk
                                ppc.infrastructure_unit_type = object.__class__.__name__
                                ppc.save()

                        elif object.__class__.__name__ == Client.__name__:
                            if ProductPriceConfig.objects.filter(
                                    organization=product.organization,
                                    is_default=True,
                                    client__id=object.pk,
                                    infrastructure_unit__id=object.assigned_to.pk,
                                    product=product,
                                    price_type__id=p_type_pk).exists():
                                pass
                            else:
                                if object.assigned_to:
                                    config_objects = ProductPriceConfig.objects.filter(organization=product.organization,
                                                                                       is_default=True,
                                                                                       infrastructure_unit__id=object.assigned_to.pk,
                                                                                       client__isnull=True,
                                                                                       product=product,price_type__id=p_type_pk)
                                    if config_objects.exists():
                                        config_object = config_objects.first()
                                        ppc = ProductPriceConfig()
                                        ppc.organization = config_object.organization
                                        ppc.price_type_id = config_object.price_type_id
                                        ppc.product = config_object.product
                                        ppc.value = config_object.value
                                        ppc.is_default = True
                                        ppc.client_id = object.pk
                                        ppc.infrastructure_unit_id = config_object.infrastructure_unit_id
                                        ppc.infrastructure_unit_type = config_object.infrastructure_unit_type
                                        ppc.client_type = object.__class__.__name__
                                        ppc.save()
                                    else:
                                        ppc = ProductPriceConfig()
                                        ppc.organization = product.organization
                                        ppc.price_type_id = p_type_pk
                                        ppc.product = product
                                        ppc.value = product.get_price_value(p_type_name)
                                        ppc.is_default = True
                                        ppc.infrastructure_unit_id = object.assigned_to.pk
                                        ppc.infrastructure_unit_type = object.assigned_to.__class__.__name__
                                        ppc.save()

                                        ppc_client = ProductPriceConfig()
                                        ppc_client.organization = ppc.organization
                                        ppc_client.price_type_id = ppc.price_type_id
                                        ppc_client.product = ppc.product
                                        ppc_client.value = ppc.value
                                        ppc_client.is_default = ppc.is_default
                                        ppc_client.client_id = object.pk
                                        ppc_client.client_type = object.__class__.__name__
                                        ppc_client.infrastructure_unit_id = ppc.infrastructure_unit_id
                                        ppc_client.infrastructure_unit_type = ppc.infrastructure_unit_type
                                        ppc_client.save()

                                else:
                                    ppc = ProductPriceConfig()
                                    ppc.organization = product.organization
                                    ppc.price_type_id = p_type_pk
                                    ppc.product = product
                                    ppc.value = product.get_price_value(p_type_name)
                                    ppc.is_default = True
                                    ppc.client_id = object.pk
                                    ppc.client_type = object.__class__.__name__
                                    ppc.save()


def receive_client_added(sender=None, client=None, **kwargs):
    if client is None:
        return
    price_types = list(ProductPrice.objects.values_list('pk', 'short_name')) if not kwargs.get("price_types") else kwargs["price_types"]
    all_products = Product.objects.all() if not kwargs.get("products") else kwargs["products"]
    Area = get_model('foodex','Area')
    for product in all_products:
        for p_type_pk, p_type_name in price_types:
            with transaction.atomic():
                product_price_config = ProductPriceConfig.objects.filter(
                        organization=product.organization,
                        is_default=True,
                        infrastructure_unit__id=client.assigned_to.pk,
                        client__id=client.pk,
                        product=product,
                        price_type__id=p_type_pk)
                if product_price_config.exists():
                    product_price_config = product_price_config.first()
                    product_price_config.infrastructure_unit_type = Area.__name__
                    product_price_config.client_type = client.__class__.__name__
                    product_price_config.save()

                else:
                    if client.assigned_to:
                        config_objects = ProductPriceConfig.objects.filter(organization=product.organization,
                                                                           is_default=True,
                                                                           infrastructure_unit__id=client.assigned_to.pk,
                                                                           client__isnull=True,
                                                                           product=product,price_type__id=p_type_pk)
                        if config_objects.exists():
                            config_object = config_objects.first()

                            config_object.infrastructure_unit_type = Area.__name__
                            config_object.save()

                            ppc = ProductPriceConfig()
                            ppc.organization = config_object.organization
                            ppc.price_type_id = config_object.price_type_id
                            ppc.product = config_object.product
                            ppc.value = config_object.value
                            ppc.is_default = True
                            ppc.client_id = client.pk
                            ppc.infrastructure_unit_id = config_object.infrastructure_unit_id
                            ppc.infrastructure_unit_type = Area.__name__
                            ppc.client_type = client.__class__.__name__
                            ppc.save()
                        else:
                            ppc = ProductPriceConfig()
                            ppc.organization = product.organization
                            ppc.price_type_id = p_type_pk
                            ppc.product = product
                            ppc.value = product.get_price_value(p_type_name)
                            ppc.is_default = True
                            ppc.infrastructure_unit_id = client.assigned_to.pk
                            ppc.infrastructure_unit_type = Area.__name__
                            ppc.save()

                            ppc_client = ProductPriceConfig()
                            ppc_client.organization = ppc.organization
                            ppc_client.price_type_id = ppc.price_type_id
                            ppc_client.product = ppc.product
                            ppc_client.value = ppc.value
                            ppc_client.is_default = ppc.is_default
                            ppc_client.client_id = client.pk
                            ppc_client.infrastructure_unit_id = ppc.infrastructure_unit_id
                            ppc_client.infrastructure_unit_type = Area.__name__
                            ppc_client.client_type = client.__class__.__name__
                            ppc_client.save()

                    else:
                        ppc = ProductPriceConfig()
                        ppc.organization = product.organization
                        ppc.price_type_id = p_type_pk
                        ppc.product = product
                        ppc.value = product.get_price_value(p_type_name)
                        ppc.is_default = True
                        ppc.client_id = client.pk
                        ppc.client_type = client.__class__.__name__
                        ppc.save()

def receive_infrastructureunit_added(sender=None, iunit=None, **kwargs):
    if iunit is None:
        return
    price_types = list(ProductPrice.objects.values_list('pk', 'short_name')) if not kwargs.get("price_types") else kwargs["price_types"]
    all_products = Product.objects.all() if not kwargs.get("products") else kwargs["products"]
    Area = get_model('foodex','Area')
    for product in all_products:
        for p_type_pk, p_type_name in price_types:
            with transaction.atomic():
                product_price_config =  ProductPriceConfig.objects.filter(
                        organization=product.organization,
                        is_default=True,
                        infrastructure_unit__id=iunit.pk,
                        client__isnull=True,
                        product=product,
                        price_type__id=p_type_pk)
                if product_price_config.exists():
                    product_price_config = product_price_config.first()
                    product_price_config.infrastructure_unit_type = Area.__name__
                    product_price_config.save()
                    pass
                else:
                    ppc = ProductPriceConfig()
                    ppc.organization = product.organization
                    ppc.price_type_id = p_type_pk
                    ppc.product = product
                    ppc.value = product.get_price_value(p_type_name)
                    ppc.is_default = True
                    ppc.infrastructure_unit_id = iunit.pk
                    ppc.infrastructure_unit_type = Area.__name__
                    ppc.save()


sig_product_added.connect(receive_product_added, dispatch_uid="product_added")
sig_client_added.connect(receive_client_added, dispatch_uid="client_added")
sig_iunit_added.connect(receive_infrastructureunit_added, dispatch_uid="infrastructure_unit_added")

def special_price_config_init_func(area_id=-1, dist_id=-1, horeca=-1, modern_trade=-1, wholesale=-1):
    infrastructure_unit_id =  area_id
    try:
        infrastructure_unit_id = int(infrastructure_unit_id)
        dist_id = int(dist_id)
    except Exception:
        infrastructure_unit_id = -1
        dist_id = -1
    price_types = list(ProductPrice.objects.values_list('pk', 'short_name'))
    all_products = Product.objects.all()

    if infrastructure_unit_id == 0 and dist_id > 0 and horeca < 0 and modern_trade < 0 and wholesale < 0:
        from foodex.models.clients.distributor import Distributor
        all_dist = Distributor.objects.filter(pk=dist_id)
        for dist in all_dist:
            receive_client_added(client=dist,products=all_products,price_types=price_types)
            print(ProductPriceConfig.objects.count())

    if infrastructure_unit_id > 0 and dist_id < 0 and horeca < 0 and modern_trade < 0 and wholesale < 0:
        iunit_objects = InfrastructureUnit.objects.filter(pk=infrastructure_unit_id)
        if iunit_objects.exists():
            iunit = iunit_objects.first()
            receive_infrastructureunit_added(iunit=iunit,products=all_products,price_types=price_types)
            print(ProductPriceConfig.objects.count())

    if infrastructure_unit_id < 0 and dist_id < 0 and horeca < 0 and modern_trade < 0 and wholesale < 0:
        from foodex.models.infrastructure.area import Area
        all_iunits = Area.objects.filter(is_deleted=False)
        for iunit in all_iunits:
            print('Unit ID: '+str(iunit))
            receive_infrastructureunit_added(iunit=iunit,products=all_products,price_types=price_types)
            print(ProductPriceConfig.objects.count())

    if infrastructure_unit_id < 0 and dist_id > 0 and horeca < 0 and modern_trade < 0 and wholesale < 0:
        from foodex.models.clients.distributor import Distributor
        all_dist = Distributor.objects.filter(is_deleted=False)
        for dist in all_dist:
            print('Dist ID: '+str(dist))
            receive_client_added(client=dist,products=all_products,price_types=price_types)
            print(ProductPriceConfig.objects.count())

    if infrastructure_unit_id < 0 and dist_id < 0 and horeca > 0 and modern_trade < 0 and wholesale < 0:
        from foodex.models.clients.horeco import HoReCo
        all_horeca = HoReCo.objects.filter(is_deleted=False, pk__gt=int(horeca))
        for client in all_horeca:
            print('Horeca ID: '+str(client))
            receive_client_added(client=client,products=all_products,price_types=price_types)
            print(ProductPriceConfig.objects.count())

    if infrastructure_unit_id < 0 and dist_id < 0 and horeca < 0 and modern_trade > 0 and wholesale < 0:
        from foodex.models.clients.modern_trade import ModernTrade
        all_mt = ModernTrade.objects.filter(is_deleted=False, pk__gt=int(modern_trade))
        for client in all_mt:
            print('Motern Trade ID: '+str(client))
            receive_client_added(client=client,products=all_products,price_types=price_types)
            print(ProductPriceConfig.objects.count())

    if infrastructure_unit_id < 0 and dist_id < 0 and horeca < 0 and modern_trade < 0 and wholesale > 0:
        from foodex.models.clients.wholesale import WholeSaler

        all_ws = WholeSaler.objects.filter(is_deleted=False, pk__gt=int(wholesale))
        for client in all_ws:
            print('Wholesaler ID: ' + str(client))
            receive_client_added(client=client, products=all_products, price_types=price_types)
            print(ProductPriceConfig.objects.count())

def special_distributor_inventory_init_after_product_added():
    all_products = Product.objects.all()
    all_distributors = Client.objects.filter(type=get_model("foodex","Distributor").__name__)
    for product in all_products:
        for a_distributor in all_distributors:
            print("current dist: "+str(a_distributor))
            client_current_inventory_objects = get_model("foodex","ClientInventory").objects.filter(assigned_to=a_distributor,product=product)
            if not client_current_inventory_objects.exists():
                client_current_inventory_object = get_model("foodex","ClientInventory")()
                client_current_inventory_object.assigned_to_id = a_distributor.pk
                client_current_inventory_object.organization = product.organization,
                client_current_inventory_object.product_id = product.pk
                client_current_inventory_object.stock = 0
                client_current_inventory_object.organization = product.organization,
                client_current_inventory_object.created_by = product.created_by
                client_current_inventory_object.save()
                print('Inventory created for: '+str(product))