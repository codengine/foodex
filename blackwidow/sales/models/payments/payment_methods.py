from django.core.urlresolvers import reverse
from django.db import models
from django.utils.safestring import mark_safe
from blackwidow.core.models.clients.client import Client

from blackwidow.core.models.contracts.configurabletype import ConfigurableType
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, loads_initial_data, is_object_context
from blackwidow.sales.models.invoice.invoice import Invoice
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum


__author__ = 'Mahmud'


@decorate(save_audit_log,is_object_context, loads_initial_data,
          expose_api('payment-methods'), route(route='payment-methods', module=ModuleEnum.Execute, group_order=3, item_order=10, display_name='Payment Method', group='Credits & payments(clients)'))
class PaymentMethod(ConfigurableType):

    @classmethod
    def table_columns(cls):
        return "code", "name", "last_updated"


class Payment(OrganizationDomainEntity):
    payment_method = models.ForeignKey(PaymentMethod)
    total_amount = models.DecimalField(decimal_places=2, max_digits=14, default=0)
    amount_paid = models.DecimalField(decimal_places=2, max_digits=14, default=0)
    due_amount = models.DecimalField(decimal_places=2, max_digits=14, default=0)
    total_available_damage_amount = models.DecimalField(decimal_places=2, max_digits=14, default=0)
    apply_damage_amount = models.DecimalField(decimal_places=2, max_digits=14, default=0)
    invoice = models.ForeignKey(Invoice,null=True)
    infrastructure_unit = models.ForeignKey(InfrastructureUnit,null=True)
    client = models.ForeignKey(Client,null=True)
    reference = models.CharField(max_length=500,null=True,blank=True)

    def __str__(self):
        return 'payment method: '+ str(self.payment_method) +' & total amount: '+ str(self.total_amount) +' & amount paid: '+str(self.amount_paid)


    @property
    def render_code(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.pk}) + "' >" + self.code + "</a>")


    @classmethod
    def table_columns(cls):
        return 'render_code', 'amount_paid:Paid', 'due_amount:Still To Pay', 'last_updated'