from blackwidow.core.models.contracts.manufacturer_domain_entity import ManufacturerDomainEntity
from blackwidow.engine.decorators.utility import decorate, loads_initial_data

__author__ = 'Mahmud'


@decorate(loads_initial_data)
class Brand(ManufacturerDomainEntity):

    def get_choice_name(self):
        return self.code + " : " + self.name