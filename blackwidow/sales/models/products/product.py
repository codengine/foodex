from decimal import Decimal
from collections import OrderedDict
from threading import Thread

from django.db.models.loading import get_model
from django.dispatch.dispatcher import receiver
from django.utils.safestring import mark_safe
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig
from blackwidow.core.models.config.exporter_config import ExporterConfig
from blackwidow.core.models.config.importer_config import ImporterConfig

from blackwidow.core.models.file.imagefileobject import ImageFileObject
from blackwidow.core.models.contracts.base import DomainEntity
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.core.signals.signals import import_completed
from blackwidow.engine.decorators.enable_export import enable_export
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, get_models_with_decorator, \
    is_object_context, direct_delete
from blackwidow.sales.models.brands.brand import Brand
from blackwidow.sales.models.common.product_flavour import ProductFlavour
from blackwidow.sales.models.common.product_price import ProductPrice, ProductPriceEnum
from blackwidow.core.models.contracts.manufacturer_domain_entity import ManufacturerDomainEntity
from blackwidow.sales.models.products.product_group import ProductGroup
from config.apps import INSTALLED_APPS
from config.constants.access_permissions import BW_ACCESS_READ_ONLY, BW_ACCESS_CREATE_MODIFY
from config.enums.modules_enum import ModuleEnum
from multiprocessing.synchronize import Lock
from django.db import models, transaction
from config.enums.view_action_enum import ViewActionEnum


@decorate(is_object_context,
          route(route='product-price-value', group='Products', hide=True, module=ModuleEnum.Administration,
                display_name="Product Price Value"))
class ProductPriceValue(DomainEntity):
    price = models.ForeignKey(ProductPrice)
    value = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    client = models.ForeignKey(Client, null=True)

    @classmethod
    def filter_query(cls, query_set, custom_search_fields=[]):
        for key, value in custom_search_fields:
            if key.startswith("_search_gross_unit_price:product:id"):
                try:
                    product_object = Product.objects.get(pk=int(value))
                    product_prices = product_object.prices.filter(
                        price__short_name=ProductPriceEnum.gross_cust_price.value["short_name"])
                    query_set = query_set.filter(pk__in=product_prices.values_list('pk', flat=True))
                except:
                    query_set = cls.objects.none()
                    pass
            elif key.startswith("_search_dist_unit_price:product:id"):
                try:
                    product_object = Product.objects.get(pk=int(value))
                    product_prices = product_object.prices.filter(
                        price__short_name=ProductPriceEnum.dist_price.value["short_name"])
                    query_set = query_set.filter(pk__in=product_prices.values_list('pk', flat=True))
                except:
                    query_set = cls.objects.none()
        return query_set


@decorate(is_object_context, enable_export, enable_import, expose_api('products'), save_audit_log,
          route(route='products', group='Products', module=ModuleEnum.Administration, display_name="Product"))
class Product(ManufacturerDomainEntity):
    categories = models.ManyToManyField(ProductGroup)
    description = models.CharField(max_length=500, null=True, default='')
    weight = models.CharField(max_length=80, null=True, default="0")
    flavour = models.ForeignKey(ProductFlavour, null=True)
    brand = models.ForeignKey(Brand, null=True)
    prices = models.ManyToManyField(ProductPriceValue)
    icon = models.ForeignKey(ImageFileObject, null=True, default=None)
    origin = models.CharField(max_length=200, default="")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.pk is not None:
            for p in self.prices.all():
                self.__setattr__('price_' + p.price.short_name.lower(), p.value)
            if not hasattr(self, 'price_dp'):
                self.__setattr__('price_dp', 0)
            if not hasattr(self, 'price_rp'):
                self.__setattr__('price_rp', 0)
            if not hasattr(self, 'price_di-rt-markup'):
                self.__setattr__('price_di-rt-markup', 0)
            if not hasattr(self, 'price_refc'):
                self.__setattr__('price_refc', 0)
            if not hasattr(self, 'price_refp'):
                self.__setattr__('price_refp', 0)
            if not hasattr(self, 'price_rf-ntcu-markup'):
                self.__setattr__('price_rf-ntcu-markup', 0)
            if not hasattr(self, 'price_net-custp'):
                self.__setattr__('price_net-custp', 0)
            if not hasattr(self, 'price_vat'):
                self.__setattr__('price_vat', 0)
            if not hasattr(self, 'price_gross-custp'):
                self.__setattr__('price_gross-custp', 0)
            if not hasattr(self, 'price_gross-cusdi-markup'):
                self.__setattr__('price_gross-cusdi-markup', 0)

    def __str__(self):
        return self.short_name + ", " + self.description if self.short_name else self.name + self.description

    @classmethod
    def get_dependent_field_list(cls):
        return ['prices']

    @classmethod
    def get_import_model(cls):
        return get_model("foodex", "FoodexProduct")

    def get_price_value(self, name):
        try:
            return self.prices.get(price__short_name=name).value
        except:
            return Decimal(0)

    @classmethod
    def default_order_by(cls):
        return '-last_updated'

    @classmethod
    def get_translator_options(cls):
        class DETranslationOptions(ManufacturerDomainEntity.get_translator_options()):
            fields = ('description',)

        return DETranslationOptions

    def get_price_value(self, price_type_name):
        try:
            return self.prices.get(price__short_name=price_type_name).value
        except Exception:
            return Decimal(0)

    @property
    def get_ref_cost(self):
        return self.get_price_value(ProductPriceEnum.ref_cost.value["short_name"])

    @property
    def get_ref_price(self):
        return self.get_price_value(ProductPriceEnum.ref_price.value["short_name"])

    @property
    def get_ref_net_cust_markup(self):
        return self.get_price_value(ProductPriceEnum.ref_net_cust_markup.value["short_name"])

    @property
    def get_net_cust_price(self):
        return self.get_price_value(ProductPriceEnum.net_cust_price.value["short_name"])

    @property
    def get_vat_rate(self):
        return self.get_price_value(ProductPriceEnum.vat_rate.value["short_name"])

    @property
    def get_gross_cust_price(self):
        return self.get_price_value(ProductPriceEnum.gross_cust_price.value["short_name"])

    @property
    def get_gross_cust_dist_markup(self):
        return self.get_price_value(ProductPriceEnum.gross_cust_distributor_markup.value["short_name"])

    @property
    def get_dist_price(self):
        return self.get_price_value(ProductPriceEnum.dist_price.value["short_name"])

    @property
    def get_dist_retailer_markup(self):
        return self.get_price_value(ProductPriceEnum.dist_retailer_markup.value["short_name"])

    @property
    def get_retailer_price(self):
        return self.get_price_value(ProductPriceEnum.retail_price.value["short_name"])

    @property
    def get_product_category(self):
        try:
            return self.categories.all().first().name
        except Exception:
            return ""

    @property
    def get_product_icon_name(self):
        try:
            return self.icon.name
        except Exception:
            return ""

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        importer_config, created = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if created or importer_config.columns.count() == 0:
            importer_config.starting_row = 1
            importer_config.save(**kwargs)
        else:
            for items in importer_config.columns.all():
                items.delete()
        columns = [

        ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.starting_row = 1
            importer_config.columns.add(c)
        return importer_config

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        return data

    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
        if not exporter_configs.exists():
            exporter_config = ExporterConfig()
            exporter_config.organization = organization
            exporter_config.starting_row = 1
            exporter_config.save(**kwargs)
        else:
            for e in exporter_configs:
                e.delete()
            exporter_config = ExporterConfig()
            exporter_config.save(**kwargs)

        columns = [
            ExporterColumnConfig(column=0, column_name='Code', property_name='code', ignore=False),
            ExporterColumnConfig(column=1, column_name='Name', property_name='name', ignore=False),
            ExporterColumnConfig(column=2, column_name='Short name', property_name='short_name', ignore=False),
            ExporterColumnConfig(column=3, column_name='Description', property_name='description', ignore=False),
            ExporterColumnConfig(column=4, column_name='Origin', property_name='origin', ignore=False),
            ExporterColumnConfig(column=5, column_name='Ref cost', property_name='get_ref_cost', ignore=False),
            ExporterColumnConfig(column=6, column_name='Ref price', property_name='get_ref_price', ignore=False),
            ExporterColumnConfig(column=7, column_name='Rf-NtCu markup/discount',
                                 property_name='get_ref_net_cust_markup', ignore=False),
            ExporterColumnConfig(column=8, column_name='Net Cust price', property_name='get_net_cust_price',
                                 ignore=False),
            ExporterColumnConfig(column=9, column_name='Vat Rate', property_name='get_vat_rate', ignore=False),
            ExporterColumnConfig(column=10, column_name='Gross Cust Price', property_name='get_gross_cust_price',
                                 ignore=False),
            ExporterColumnConfig(column=11, column_name='GrCu-Di markup', property_name='get_gross_cust_dist_markup',
                                 ignore=False),
            ExporterColumnConfig(column=12, column_name='Dist price', property_name='get_dist_price', ignore=False),
            ExporterColumnConfig(column=13, column_name='Di-Rt markup', property_name='get_dist_retailer_markup',
                                 ignore=False),
            ExporterColumnConfig(column=14, column_name='Retail price', property_name='get_retailer_price',
                                 ignore=False),
            ExporterColumnConfig(column=15, column_name='Category', property_name='get_product_category', ignore=False),
            ExporterColumnConfig(column=16, column_name='Icon', property_name='get_product_icon_name', ignore=False)
        ]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)
        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):
        for column in columns:
            workbook.cell(row=row_number, column=column.column + 1).value = str(getattr(self, column.property_name))
        return self.pk, row_number + 1

    @classmethod
    def finalize_export(cls, workbook=None, row_number=None, query_set=None, **kwargs):
        # file_name = 'Product_price_config' + (Clock.now().strftime('%d-%m-%Y'))
        return workbook

    @classmethod
    def initialize_export(cls, workbook=None, columns=None, row_number=None, query_set=None, **kwargs):
        for column in columns:
            workbook.cell(row=1, column=column.column + 1).value = column.column_name
        return workbook, row_number

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create, ViewActionEnum.Edit, ViewActionEnum.Delete, ViewActionEnum.AdvancedExport,
                ViewActionEnum.AdvancedImport]

    @property
    def details_config(self):
        d = super().details_config
        custom_list = ['code', 'name', 'description (English)', 'origin', 'categories', 'manufacturer', 'icon',
                       'date_created', 'last_updated']

        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]

        d['created_on'] = d.pop('date_created')
        d['description'] = d.pop('description (English)')
        d['created_by'] = self.created_by
        d['last_updated_by'] = self.last_updated_by

        a = OrderedDict()
        if self.pk is not None:
            for p in self.prices.all():
                a[p.price.name.lower()] = str(p.value)

        return OrderedDict(d, **a)

    @classmethod
    def get_serializer(cls):
        ss = ManufacturerDomainEntity.get_serializer()

        class Serializer(ss):
            def __init__(self, **kwargs):
                super().__init__(**kwargs)

                from rest_framework import serializers
                prices = list(ProductPrice.objects.all())

                for p in prices:
                    self.fields['price_' + p.short_name.lower()] = serializers.DecimalField(required=False,
                                                                                            read_only=True,
                                                                                            max_digits=20,
                                                                                            decimal_places=2)
                self.fields['icon'] = ImageFileObject.get_serializer()(read_only=True)

            class Meta(ss.Meta):
                model = cls

        return Serializer

    @property
    def render_categories(self):
        return mark_safe(', '.join([str(x) for x in self.categories.all()]))

    @classmethod
    def sortable_columns(cls):
        return tuple(x for x in cls.table_columns() if x not in ['price_pp', 'price_tp', 'price_mrp'])

    @property
    def render_manufacturer(self):
        return self.manufacturer

    @classmethod
    def filter_query(cls, query_set, custom_search_fields=[]):
        from django.db.models import Q
        from functools import reduce
        import operator
        for key, value in custom_search_fields:

            if key.startswith("_search_client_inventory:assigned_to:id"):
                try:
                    client_inventory = get_model("foodex", "ClientInventory").objects.filter(assigned_to_id=int(value),
                                                                                             stock__gt=0).values_list(
                        'product__id', flat=True)
                    query_set = query_set.filter(pk__in=client_inventory)
                except:
                    query_set = cls.objects.none()
            elif key.startswith("_search_warehouse_inventory:assigned_to:id"):
                try:
                    wis = get_model("foodex", "WarehouseInventory").objects.filter(assigned_to_id=int(value),
                                                                                   stock__gt=0).values_list(
                        'product__id', flat=True)
                    query_set = query_set.filter(pk__in=wis)
                except:
                    query_set = cls.objects.none()

            elif key.startswith("__search__categories"):
                try:
                    or_list = []
                    values = value.split(",")
                    for val in values:
                        or_list.append(Q(categories__name=val))
                    product = Product.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=product.values_list('pk', flat=True))
                except:
                    pass
            elif key.startswith("__search__manufacturers"):
                try:
                    or_list = []
                    values = value.split(",")
                    for val in values:
                        or_list.append(Q(manufacturer__name=val))
                    product = Product.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=product.values_list('pk', flat=True))
                except:
                    pass

            elif key.startswith("_search_categories:id"):
                try:
                    or_list = []
                    values = value.split(",")
                    for val in values:
                        or_list.append(Q(categories__pk=val))
                    product = Product.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=product.values_list('pk', flat=True))
                except:
                    pass
            elif key.startswith("_search_manufacturer:id"):
                try:
                    or_list = []
                    values = value.split(",")
                    for val in values:
                        or_list.append(Q(manufacturer__pk=val))
                    product = Product.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=product.values_list('pk', flat=True))
                except:
                    pass

        return query_set

    @classmethod
    def get_url_by_name(cls, name):
        if name == 'render_categories':
            return '__search__categories'
        elif name == 'render_manufacturer':
            return '__search__manufacturers'
        return None

    @classmethod
    def table_columns(cls):
        prices = list(ProductPrice.objects.all())
        _price_columns = list()
        for p in prices:
            if p.name == ProductPriceEnum.dist_price.value['name'] or p.name == ProductPriceEnum.gross_cust_price.value[
                'name'] or p.name == ProductPriceEnum.retail_price.value['name']:
                _price_columns.append('price_' + p.short_name.lower())
        product_columns = ['code', 'name', 'description', 'origin', 'render_categories',
                           'render_manufacturer'] + _price_columns
        return product_columns + ['last_updated']

    def get_choice_name(self):
        return self.name + ", " + self.description

    def save(self, *args, organization=None, signal=True, **kwargs):
        super().save(*args, organization=organization, **kwargs)
