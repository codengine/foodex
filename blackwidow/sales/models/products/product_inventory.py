from threading import Thread
from uuid import uuid4
from django.db import models, transaction
from django.db.models import Q
from django.dispatch.dispatcher import receiver
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.common.contactaddress import ContactAddress
from blackwidow.core.models.common.location import Location
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig
from blackwidow.core.models.config.exporter_config import ExporterConfig
from blackwidow.sales.models.transactions.stock_transfer_transaction_breakdown import StockTransferTransactionBreakDown

from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.core.models.contracts.manufacturer_domain_entity import ManufacturerDomainEntity
from blackwidow.core.signals.signals import import_completed
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.extensions.clock import Clock
from blackwidow.sales.models.common.product_price import ProductPriceEnum
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.transactions.stock_adjustment import StockAdjustmentTransaction
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from multiprocessing.synchronize import Lock
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.transactions.warehouse_stock_transfer import WarehouseStockTransfer
from foodex.models.infrastructure.warehouse import Warehouse
from django import forms

__author__ = 'Mahmud'

# @decorate(save_audit_log, is_object_context, enable_export, enable_import, expose_api("inventories"),
#           route(route='inventories', module=ModuleEnum.Execute, display_name='Warehouse Inventory', group='Inventory'))
class ProductInventory(ManufacturerDomainEntity):
    product = models.ForeignKey(Product)
    stock = models.BigIntegerField(default=0)
    assigned_to = models.ForeignKey(InfrastructureUnit, null=True, default=None)

    @classmethod
    def get_search_form(cls, search_fields, fields):
        sf_instance = ManufacturerDomainEntity.get_search_form(search_fields, fields)

        i_label,i_value = cls.get_search_value("assigned_to",search_fields,fields)

        sf_instance.fields['assigned_to:id'] = GenericModelChoiceField(label='Warehouse', empty_label="Select Warehouse", required=False,  initial=i_value, queryset=Warehouse.objects.all(), widget=forms.Select(attrs={'class': 'select2'}))
        sf_instance.fields['assigned_to:id'].readonly = False

        return sf_instance

    @property
    def render_product(self):
        return self.product.code + ': ' + self.product.name.strip() + ' ' + self.product.description.strip()

    @property
    def render_distributor(self):
        # from blackwidow.core.models.clients.distributor import Distributor
        # _distributor = Distributor.objects.filter(assigned_to=self.assigned_to, manufacturer=self.manufacturer)
        # if _distributor.exists():
        #     return _distributor[0]
        return ''

    @classmethod
    def get_export_dependant_fields(self):
        class AdvancedExportDependentForm(GenericFormMixin):
            def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
                super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
                self.fields['assigned_to:id'] = GenericModelChoiceField(label='Select Warehouse', empty_label="Select Warehouse", required=True, queryset=InfrastructureUnit.objects.filter(type='WareHouse'))

        return AdvancedExportDependentForm

    @property
    def get_warehouse_id(self):
        return self.assigned_to.pk if self.assigned_to else ''

    @property
    def get_warehouse_name(self):
        return self.assigned_to.name if self.assigned_to else ''

    @property
    def get_product_id(self):
        return self.product.pk if self.product else ''

    @property
    def get_product_name(self):
        return self.product.name if self.product else ''

    @property
    def get_product_description(self):
        return self.product.name if self.product else ''

    @property
    def get_incoming_stock(self):
        return 0

    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
        if not exporter_configs.exists():
            exporter_config = ExporterConfig()
            exporter_config.starting_row = 2
            exporter_config.save(**kwargs)
        else:
            for e in exporter_configs:
                e.delete()
            exporter_config = ExporterConfig()
            exporter_config.save(**kwargs)

        columns = [
            #ExporterColumnConfig(column=0, column_name='Product Price Config Id', property_name='product_price_config_id', ignore=False),
            ExporterColumnConfig(column=0, column_name='Warehouse Id', property_name='get_warehouse_id', ignore=False),
            ExporterColumnConfig(column=1, column_name='Warehouse Name', property_name='get_warehouse_name', ignore=False),
            ExporterColumnConfig(column=2, column_name='Product Id', property_name='get_product_id', ignore=False),
            ExporterColumnConfig(column=3, column_name='Product Name', property_name='get_product_name', ignore=False),
            ExporterColumnConfig(column=4, column_name='Product Description', property_name='get_product_description', ignore=False),
            ExporterColumnConfig(column=5, column_name='Previous Stock Quantity', property_name='stock', ignore=False),
            ExporterColumnConfig(column=6, column_name='Incoming Stock Quantity', property_name='get_incoming_stock', ignore=False)
        ]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)
        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):
        for column in columns:
            workbook.cell(row=row_number, column=column.column + 1).value = str(getattr(self, column.property_name))
        return self.pk, row_number + 1

    @classmethod
    def finalize_export(cls, workbook=None, row_number=None, query_set=None, **kwargs):
        file_name = 'WarehouseInventory' + str(Clock.timestamp(_format='ms'))
        return workbook, file_name

    @classmethod
    def initialize_export(cls, workbook=None, columns=None, row_number=None, query_set=None, **kwargs):
        ###Initialize the product Inventory.
        all_products = Product.objects.all()
        all_warehouses = Warehouse.objects.all()

        with transaction.atomic():
            for warehouse in all_warehouses:
                for product in all_products:
                    if not ProductInventory.objects.filter(product=product,assigned_to=warehouse).exists():
                        product_inventory_object = ProductInventory()
                        product_inventory_object.product = product
                        product_inventory_object.assigned_to = warehouse
                        product_inventory_object.stock = 0
                        product_inventory_object.organization = warehouse.organization
                        product_inventory_object.save()

        #print(query_set)
        for column in columns:
            workbook.cell(row=1, column=column.column + 1).value = column.column_name
        return workbook, row_number

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    @classmethod
    def get_manage_buttons(cls):
        return []

    @property
    def get_inline_manage_buttons(self):
        return [

        ]

    @classmethod
    def table_columns(cls):
        return 'code', 'assigned_to:Warehouse', 'product', 'stock', 'last_updated', 'last_updated_by'

    @classmethod
    def get_queryset(cls, queryset=None, **kwargs):
        return queryset
        # return queryset.exclude(product__manufacturer__name__icontains='JITA')

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        # ImporterColumnConfig= 'core.ImporterColumnConfig'
        # ImporterConfig = 'core.ImporterConfig'
        from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
        from blackwidow.core.models.config.importer_config import ImporterConfig
        importer_config, result = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if result or importer_config.columns.count() == 0:
            importer_config.save(**kwargs)
        else:
            for items in importer_config.columns.all():
                items.delete()
        columns = [
            ImporterColumnConfig(column=0, column_name='Warehouse Id', property_name='get_warehouse_id', ignore=False),
            ImporterColumnConfig(column=1, column_name='Warehouse Name', property_name='get_warehouse_name', ignore=False),
            ImporterColumnConfig(column=2, column_name='Product Id', property_name='get_product_id', ignore=False),
            ImporterColumnConfig(column=3, column_name='Product Name', property_name='get_product_name', ignore=False),
            ImporterColumnConfig(column=4, column_name='Product Description', property_name='get_product_description', ignore=False),
            ImporterColumnConfig(column=5, column_name='Previous Stock Quantity', property_name='stock', ignore=False),
            ImporterColumnConfig(column=6, column_name='Incoming Stock Quantity', property_name='get_incoming_stock', ignore=False)
            ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.columns.add(c)
        return importer_config


    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, request=None, **kwargs):
        # with transaction.atomic():
        #     pi = ProductInventory.objects.filter(assigned_to_id=int(data['0']), product_id=int(data['2']))
        #     if pi.exists():
        #         pi = pi.first()
        #         pi.stock = pi.stock + int(data['6'])
        #         pi.save()
        #         return data
        #     else:
        #         organization = Organization.objects.all().first()
        #         pi = ProductInventory()
        #         pi.assigned_to_id = int(data['0'])
        #         pi.product_id = int(data['2'])
        #         pi.stock = int(data['6'])
        #         pi.organization = organization
        #         pi.save()
        #         return data

        return data




    @property
    def tabs_config(self):

        def get_transaction_breakdown():
            stock_transfers = WarehouseStockTransfer.objects.filter(product=self.product)
            breakdown_pk = list()
            for stock in stock_transfers:
                breakdowns = stock.breakdown.all()
                for breakdown in breakdowns:
                    breakdown_pk.append(int(breakdown.pk))
            breakdown_object = StockTransferTransactionBreakDown.objects.filter(pk__in=breakdown_pk)
            return breakdown_object


        tabs = [TabView(
            title='Stock In',
            access_key='breakdown',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='sales.ProductInventory',
            queryset_filter=Q(**{})
        ), TabView(
            title='Stock Count',
            access_key='incominginventories',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='sales.ClientOnRouteInventory',
            queryset_filter=Q(**{})
        ), TabView(
            title='Stock Transfer',
            access_key='stocktransferbreakdown',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            related_model=StockTransferTransactionBreakDown,
            property=get_transaction_breakdown()
        )]

        return tabs


def save_transaction_details(items,user,organization):
    #print("Saving transaction details now.")
    warehouses = {}
    #print(items)
    for data in items:
        w_id = int(data['0'])
        if not w_id in warehouses.keys():
            warehouses[int(data['0'])] = [data]
        else:
            warehouses[int(data['0'])] += [data]

    with transaction.atomic():
        for warehouse_id,data in warehouses.items():

            warehouse_objects = Warehouse.objects.filter(pk=warehouse_id)
            if not warehouse_objects.exists():
                warehouse_object = Warehouse()
                warehouse_object.name = data[0]['1']
                warehouse_object.organization = organization
                warehouse_object.created_by = user

                contact_address = ContactAddress()
                contact_address.street = 'Azhar Comfort Complex.'
                contact_address.city = 'Dhaka'
                contact_address.province = 'Dhaka'
                contact_address.save()

                warehouse_object.address = contact_address

                warehouse_object.save()
                warehouse_id = warehouse_object.pk

            #tx_stock_adjustment = StockAdjustmentTransaction()

            #organization = Organization.objects.all().first()

            tx_stock_adjustment = StockAdjustmentTransaction()
            tx_stock_adjustment.organization = organization
            tx_stock_adjustment.infrastructure_unit_id = warehouse_id
            location = Location()
            location.save()
            tx_stock_adjustment.location = location
            tx_stock_adjustment.transaction_time = Clock.timestamp()
            tx_stock_adjustment.unique_id = str(uuid4())
            tx_stock_adjustment.created_by = user
            tx_stock_adjustment.save()

            tx_subtotal = 0
            tx_total = 0
            tx_discount = 0

            #print(data)

            for data_row in data:
                product_object = Product.objects.get(pk=int(data_row['2']))
                product_unit_price = product_object.prices.filter(price__name=ProductPriceEnum.ref_price.value["name"]).first().value
                tx_breakdown = TransactionBreakDown()
                tx_breakdown.product_id = int(data_row['2'])
                tx_breakdown.quantity = int(data_row['6'])
                tx_breakdown.unit_price = product_unit_price
                tx_breakdown.sub_total = tx_breakdown.quantity * tx_breakdown.unit_price
                tx_breakdown.discount = 0
                tx_breakdown.total = tx_breakdown.sub_total - tx_breakdown.discount

                pi = ProductInventory.objects.filter(assigned_to_id=warehouse_id, product_id=int(data_row['2']))
                if pi.exists():
                    pi = pi.first()
                    tx_breakdown.opening_balance = pi.stock
                    pi.stock = pi.stock + int(data_row['6'])
                    pi.save()
                else:
                    tx_breakdown.opening_balance = 0
                    pi = ProductInventory()
                    pi.assigned_to_id = warehouse_id
                    pi.product_id = product_object.pk
                    pi.stock = int(data_row['6'])
                    pi.organization = organization
                    pi.save()

                tx_breakdown.closing_balance = pi.stock
                tx_breakdown.organization = organization
                tx_breakdown.save()
                tx_stock_adjustment.breakdown.add(tx_breakdown)

                tx_subtotal += tx_breakdown.sub_total
                tx_discount += tx_breakdown.discount
                tx_total += tx_breakdown.total

            tx_stock_adjustment.sub_total = tx_subtotal
            tx_stock_adjustment.discount = tx_discount
            tx_stock_adjustment.total = tx_total
            tx_stock_adjustment.save()


@receiver(import_completed, sender=ProductInventory)
def retailers_import_completed(sender, items=None, user=None, organization=None, **kwargs):
    #print("Products have been added to warehouses")
    lock = Lock(ctx=None)
    lock.acquire(True)
    process = Thread(target=save_transaction_details, args=(items,user,organization,))
    process.start()
    lock.release()
