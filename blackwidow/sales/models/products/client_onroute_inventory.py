from django.db import models, transaction
from django.db.models.loading import get_model
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.contracts.manufacturer_domain_entity import ManufacturerDomainEntity
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import save_audit_log, decorate, is_object_context
from blackwidow.sales.models.inventory.client_inventory import ClientInventory
from blackwidow.sales.models.products.product import Product
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'Mahmud'

# @decorate(save_audit_log, is_object_context,
#           expose_api("incoming-inventories"),
#           route(route='incoming-inventories', module=ModuleEnum.Execute, display_name='In-Transit Stock', group='Inventory'))
class ClientOnRouteInventory(ClientInventory):
    class Meta:
        proxy = True

    @classmethod
    def get_button_title(cls, button=ViewActionEnum.Details):
        if button == ViewActionEnum.Mutate:
            return "Product Received"
        return button.value

    # def mutate_to(self, cls=None):
    #     with transaction.atomic():
    #         c_inventory_class = get_model('sales', 'ClientCurrentInventory')
    #         c_inv, result = c_inventory_class.objects.get_or_create(product=self.product, assigned_to=self.assigned_to)
    #         if result:
    #             c_inv.stock = 0
    #         c_inv.stock += self.stock
    #         c_inv.save()
    #         self.delete()
    #         return c_inv

    # @classmethod
    # def get_serializer(cls):
    #     ss = ClientInventory.get_serializer()
    #
    #     class Serializer(ss):
    #
    #         def save(self, **kwargs):
    #             with transaction.atomic():
    #                 c_inventory_class = get_model('sales', 'ClientCurrentInventory')
    #                 c_inv, result = c_inventory_class.objects.get_or_create(product=self.product, assigned_to=self.assigned_to)
    #                 if result:
    #                     c_inv.stock = 0
    #                 c_inv.stock += self.stock
    #                 c_inv.save()
    #                 self.delete()
    #                 return c_inv
    #
    #                 # class Meta(ss.Meta):
    #                 #     model = cls
    #                 #     depth = 1
    #     return Serializer

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Details]

    @property
    def get_inline_manage_buttons(self):
        return [dict(
            name='Product Received',
            action='view',
            title="Click to accept products",
            icon='icon-arrow-right',
            ajax='0',
            url_name=self.__class__.get_route_name(action=ViewActionEnum.Mutate),
            url_params= {'pk': self.pk},
            classes='all-action confirm-action',
            parent=None
        )]