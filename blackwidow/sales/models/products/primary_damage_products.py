from django.db import models, transaction
from django.db.models.loading import get_model
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.contracts.manufacturer_domain_entity import ManufacturerDomainEntity
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.process.process_breakdown import ProcessBreakdown
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import save_audit_log, decorate, is_object_context
from blackwidow.sales.models.products.product import Product
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'Mahmud'

# @decorate(save_audit_log, is_object_context,
#           expose_api("primary-damage-products"),
#           route(route='primary-damage-products', module=ModuleEnum.Execute, display_name='Damaged Product', group='Primary Sales'))
class PrimaryDamagedProducts(OrganizationDomainEntity):
    product = models.ForeignKey(Product)
    quantity = models.BigIntegerField(default=0)
    value = models.DecimalField(decimal_places=2, max_digits=20, default=0)
    client = models.ForeignKey(Client, null=True, default=None)
    process_breakdown = models.ManyToManyField(ProcessBreakdown)

