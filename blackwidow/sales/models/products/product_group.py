from django.db import models
from modeltranslation.translator import TranslationOptions

from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, loads_initial_data, is_object_context
from blackwidow.engine.extensions.model_descriptor import get_model_by_name
from config.enums.modules_enum import ModuleEnum


__author__ = 'ruddra'


@decorate(save_audit_log, is_object_context,
          expose_api('categories'),
          loads_initial_data,
          route(route='categories', group='Products', module=ModuleEnum.Administration, display_name="Category"),
          )
class ProductGroup(OrganizationDomainEntity):
    name = models.CharField(max_length=800, null=True, default='')

    def get_choice_name(self):
        return self.name

    @classmethod
    def get_translator_options(cls):
        class DETranslationOptions(TranslationOptions):
            fields = ('name', )
        return DETranslationOptions

    @classmethod
    def table_columns(cls):
        return 'code', 'name_en:Category', 'last_updated'

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'name', 'organization', 'date_created', 'last_updated']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]

        return d

    @property
    def render_manufacturer(self):
        product_model = get_model_by_name('Product')
        product = product_model.objects.filter(categories__icontains = self)[0]
        return product.manufacturer

    def load_initial_data(self, **kwargs):
        super().load_initial_data(**kwargs)
        self.name = "Category " + str(kwargs['index'])
        self.save()