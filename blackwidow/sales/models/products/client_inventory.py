from django.core.urlresolvers import reverse
from django.db import models, transaction
from django.db.models.loading import get_model
from django.utils.safestring import mark_safe
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.contracts.manufacturer_domain_entity import ManufacturerDomainEntity
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import save_audit_log, decorate
from blackwidow.sales.models.products.product import Product
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum


__author__ = 'Mahmud'

class ClientInventoryOld(ManufacturerDomainEntity):
    product = models.ForeignKey(Product)
    stock = models.BigIntegerField(default=0)
    assigned_to = models.ForeignKey(Client, null=True, default=None)

    @property
    def render_product(self):
        return self.product.code + ': ' + self.product.name.strip() + ' ' + self.product.description.strip()

    # @property
    # def get_inline_manage_buttons(self):
    #     return [
    #         dict(
    #             name='Adjust Stock',
    #             action='edit',
    #             icon='icon-pencil',
    #             ajax='1',
    #             title='Stock Adjustment',
    #             url_name=self.__class__.get_route_name(action=ViewActionEnum.PartialEdit),
    #             classes='manage-action load-modal',
    #             parent=self.infrastructureunit_set.all()[0]
    #         )
    #     ]


    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Details]
    
    @property
    def render_code(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.pk}) + "' >" + self.code + "</a>")

    @classmethod
    def table_columns(cls):
        return 'render_code', 'product', 'manufacturer', 'assigned_to:Client', 'stock', 'last_updated', 'last_updated_by'

    @classmethod
    def get_queryset(cls, queryset=None, **kwargs):
        return queryset
