from rest_framework import serializers
from django.db import models
from django.db.models.loading import get_model
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import save_audit_log, decorate, is_object_context
from blackwidow.sales.models.products.product import Product
from config.enums.modules_enum import ModuleEnum


__author__ = 'Mahmud'

# @decorate(save_audit_log, is_object_context,
#           expose_api("secondary-damage-products"),
#           route(route='secondary-damage-products', module=ModuleEnum.Execute, display_name='Damaged Product', group='Secondary Sales'))
class SecondaryDamagedProducts(OrganizationDomainEntity):
    product = models.ForeignKey(Product)
    quantity = models.BigIntegerField(default=0)
    from_client = models.ForeignKey(Client, null=True, related_name='+', default=None)
    to_client = models.ForeignKey(Client, null=True, related_name='+', default=None)

    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class Serializer(ss):
            product = serializers.PrimaryKeyRelatedField(required=False, queryset=Product.objects.all())
            quantity = serializers.IntegerField(default=0)
            from_client = serializers.PrimaryKeyRelatedField(required=False, queryset=Client.objects.all())

            def create(self, validated_data):
                c_inventory_class = get_model('sales', 'ClientCurrentInventory')
                c_inv, result = c_inventory_class.objects.get_or_create(product=validated_data['product'], assigned_to=validated_data['from_client'])
                if result:
                    c_inv.stock = 0
                c_inv.stock = c_inv.stock - int(validated_data['quantity'])
                c_inv.save()
                obj = super().create(validated_data)
                return obj

            class Meta(ss.Meta):
                model = cls
                depth = 1
        return Serializer

    def save(self, *args, organization=None, **kwargs):
        super().save(*args, **kwargs)


