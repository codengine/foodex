from django.db import models, transaction
from django.db.models.loading import get_model
from blackwidow.core.mixins.fieldmixin.multiple_choice_field_mixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin.form_mixin import GenericFormMixin
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig
from blackwidow.core.models.config.exporter_config import ExporterConfig
from blackwidow.core.models.config.importer_column_config import ImporterColumnConfig
from blackwidow.core.models.config.importer_config import ImporterConfig
from blackwidow.core.models.contracts.manufacturer_domain_entity import ManufacturerDomainEntity
from blackwidow.core.models.organizations.organization import Organization
from blackwidow.core.models.roles.role import Role
from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.engine.decorators import enable_import
from blackwidow.engine.decorators.enable_export import enable_export
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route, partial_route
from blackwidow.engine.decorators.utility import save_audit_log, decorate, is_object_context
from blackwidow.sales.models.inventory.client_inventory import ClientInventory
from blackwidow.sales.models.products.product import Product
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum
from django import forms


__author__ = 'Mahmud'

# @decorate(save_audit_log, is_object_context, enable_export,enable_import,
#           expose_api("client-inventories"),
#           route(route='client-inventories', module=ModuleEnum.Execute, display_name='Client Inventory', group='Inventory')
#           )
class ClientCurrentInventory(ClientInventory):

    @property
    def get_product_id(self):
        return self.product.pk if self.product else 0

    @property
    def get_product_name(self):
        return self.product.name if self.product else ''

    @property
    def get_product_description(self):
        return self.product.description if self.product else ''

    @property
    def get_client_id(self):
        return self.assigned_to.pk if self.assigned_to else 0

    @property
    def get_client_name(self):
        return self.assigned_to.name if self.assigned_to else ''

    @classmethod
    def get_export_dependant_fields(self):
        class AdvancedExportDependentForm(GenericFormMixin):
            def __init__(self, data=None, files=None, instance=None, prefix='', **kwargs):
                super().__init__(data=data, files=files, instance=instance, prefix=prefix, **kwargs)
                self.fields['assigned_to:id'] = GenericModelChoiceField(label='Select Distributor', empty_label="Select Distributor", required=True, queryset=Client.objects.filter(type="Distributor"))

        return AdvancedExportDependentForm

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.AdvancedExport, ViewActionEnum.AdvancedImport]

    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        importer_config, created = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        if created or importer_config.columns.count() == 0:
            importer_config.starting_row = 1
            importer_config.save(**kwargs)
        else:
            for items in importer_config.columns.all():
                items.delete()
        columns = [
            ImporterColumnConfig(column=0, column_name='Client Id', property_name='get_client_id', ignore=False),
            ImporterColumnConfig(column=1, column_name='Client Name', property_name='get_client_name', ignore=False),
            ImporterColumnConfig(column=2, column_name='Product Id', property_name='get_product_id', ignore=False),
            ImporterColumnConfig(column=3, column_name='Product Name', property_name='get_product_name', ignore=False),
            ImporterColumnConfig(column=4, column_name='Product Description', property_name='get_product_description', ignore=False),
            ImporterColumnConfig(column=5, column_name='Stock Quantity', property_name='stock', ignore=False),
        ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            importer_config.starting_row = 1
            importer_config.columns.add(c)
        return importer_config

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        client_id = int(data['0'])
        product_id = int(data['2'])
        stock_quantity = int(data['5'])
        client_objects = Client.objects.filter(pk=client_id)
        product_objects = Product.objects.filter(pk=product_id)
        organization = Organization.objects.all().first()

        all_products = Product.objects.all()
        for a_product in all_products:
            client_current_inventory_objects = ClientCurrentInventory.objects.filter(assigned_to=client_objects.first(),product=a_product)
            if not client_current_inventory_objects.exists():
                client_current_inventory_object = ClientCurrentInventory()
                client_current_inventory_object.assigned_to_id = client_id
                client_current_inventory_object.organization = organization
                client_current_inventory_object.product_id = a_product.pk
                client_current_inventory_object.stock = 0
                client_current_inventory_object.save()


        if client_objects.exists() and product_objects.exists():
            client_current_inventory_objects = ClientCurrentInventory.objects.filter(assigned_to=client_objects.first(),product=product_objects.first())
            if client_current_inventory_objects.exists():
                client_current_inventory_object = client_current_inventory_objects.first()
                client_current_inventory_object.stock = stock_quantity
                client_current_inventory_object.save()
            else:
                client_current_inventory_object = ClientCurrentInventory()
                client_current_inventory_object.assigned_to_id = client_id
                client_current_inventory_object.organization = organization
                client_current_inventory_object.product_id = product_id
                client_current_inventory_object.stock = stock_quantity
                client_current_inventory_object.save()
        return 0

    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
        exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
        if not exporter_configs.exists():
            exporter_config = ExporterConfig()
            exporter_config.starting_row = 2
            exporter_config.save(**kwargs)
        else:
            for e in exporter_configs:
                e.delete()
            exporter_config = ExporterConfig()
            exporter_config.save(**kwargs)

        columns = [
            #ExporterColumnConfig(column=0, column_name='Product Price Config Id', property_name='product_price_config_id', ignore=False),
            ExporterColumnConfig(column=0, column_name='Client Id', property_name='get_client_id', ignore=False),
            ExporterColumnConfig(column=1, column_name='Client Name', property_name='get_client_name', ignore=False),
            ExporterColumnConfig(column=2, column_name='Product Id', property_name='get_product_id', ignore=False),
            ExporterColumnConfig(column=3, column_name='Product Name', property_name='get_product_name', ignore=False),
            ExporterColumnConfig(column=4, column_name='Product Description', property_name='get_product_description', ignore=False),
            ExporterColumnConfig(column=5, column_name='Stock Quantity', property_name='stock', ignore=False),
        ]

        for c in columns:
            c.save(organization=organization, **kwargs)
            exporter_config.columns.add(c)
        return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):
        for column in columns:
            workbook.cell(row=row_number, column=column.column + 1).value = str(getattr(self, column.property_name))
        return self.pk, row_number + 1

    @classmethod
    def finalize_export(cls, workbook=None, row_number=None, query_set=None, **kwargs):
        #file_name = 'Product_price_config' + (Clock.now().strftime('%d-%m-%Y'))
        return workbook

    @classmethod
    def initialize_export(cls, workbook=None, columns=None, row_number=None, query_set=None, **kwargs):
        for column in columns:
            workbook.cell(row=1, column=column.column + 1).value = column.column_name
        return workbook, row_number

    class Meta:
        proxy = True
