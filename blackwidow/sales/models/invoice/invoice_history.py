from django.db import models
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.contracts.manufacturer_domain_entity import ManufacturerDomainEntity
from blackwidow.core.models.users.user import ConsoleUser

__author__ = 'Sohel'

class InvoiceHistory(ManufacturerDomainEntity):
    system_user = models.ForeignKey(ConsoleUser, null=True)
    counter_part = models.ForeignKey(Client, null=True)
    date_of_invoice = models.BigIntegerField(default=0)
    invoice_number = models.CharField(max_length=200, default='')
    price_total = models.DecimalField(decimal_places=2, max_digits=20)
    actual_amount_paid = models.DecimalField(decimal_places=2, max_digits=20)
    damage_value = models.DecimalField(decimal_places=2, max_digits=20, default=0.0)
