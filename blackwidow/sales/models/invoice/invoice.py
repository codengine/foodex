from django.db import models
from django.db.models.loading import get_model
from rest_framework import serializers
from blackwidow.engine.extensions.clock import Clock

from blackwidow.core.models.users.user import ConsoleUser
from blackwidow.core.models.clients.client import Client
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.core.models.contracts.manufacturer_domain_entity import ManufacturerDomainEntity
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum
import inflect

__author__ = 'activehigh'


# @decorate(save_audit_log, is_object_context,  expose_api('invoices'),
#           partial_route(relation=['normal'], models=[Payment]))
class Invoice(ManufacturerDomainEntity):
    system_user = models.ForeignKey(ConsoleUser, null=True)
    counter_part = models.ForeignKey(Client, null=True)
    date_of_invoice = models.BigIntegerField(default=0)
    invoice_number = models.CharField(max_length=200, default='')
    price_total = models.DecimalField(decimal_places=2, max_digits=20)
    actual_amount_paid = models.DecimalField(decimal_places=2, max_digits=20)
    damage_value = models.DecimalField(decimal_places=2, max_digits=20, default=0.0)
    #payments = models.ManyToManyField(Payment)

    def __str__(self):
        return self.invoice_number

    def get_choice_name(self):
        return self.invoice_number

    @property
    def code_prefix(self):
        return "INV"

    @classmethod
    def table_columns(cls):
        return 'code', 'invoice_number', 'system_user', 'counter_part', 'price_total', 'actual_amount_paid', 'last_updated'

    @classmethod
    def get_dependent_field_list(cls):
        return []

    @property
    def render_invoice_date(self):
        return Clock.get_user_local_time(self.date_of_invoice).strftime("%d-%m-%Y")

    @classmethod
    def get_serializer(cls):
        ss = ManufacturerDomainEntity.get_serializer()

        class ODESerializer(ss):
            name = serializers.CharField(required=False)
            Payment = get_model('sales', 'Payment')
            payments = Payment.get_serializer()(many=True, required=False)

            class Meta:
                model = cls
                read_only_fields = ss.Meta.read_only_fields

        return ODESerializer

    @property
    def render_in_words(self):
        p=inflect.engine()
        tmp=p.number_to_words(int(self.price_total))
        tmp=tmp.replace(',','').replace('-','')
        tmp=tmp.split()
        tmp=[x.capitalize() for x in tmp]
        tmp=' '.join(tmp)
        return tmp

    @property
    def render_footer_date_time(self):
        return Clock.get_user_local_time(self.date_of_invoice).strftime("%H:%M:%S in %d-%m-%Y")