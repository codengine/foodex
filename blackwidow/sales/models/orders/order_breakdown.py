from crequest.middleware import CrequestMiddleware
from django.db import models
from rest_framework import serializers
from django.db import transaction
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.sales.models.common.product_unit import ProductUnit
from blackwidow.sales.models.products.product import Product


__author__ = 'Mahmud'


class OrderBreakdown(OrganizationDomainEntity):
    product = models.ForeignKey(Product)
    number_of_packet = models.IntegerField(default=0)
    items_per_packet = models.IntegerField(default=0)
    loose_items = models.IntegerField(default=0)
    total_items = models.IntegerField(default=0)
    unit_price = models.DecimalField(max_digits=20, decimal_places=2, default=0.0)
    sub_total = models.DecimalField(max_digits=20, decimal_places=2, default=0.0)
    discount = models.DecimalField(max_digits=20, decimal_places=2, default=0.0)
    total = models.DecimalField(max_digits=20, decimal_places=2, default=0.0)


    # unit = models.ForeignKey(ProductUnit)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @property
    def field_count(self):
        return 4

    def __str__(self):
        return 'Product Code: '+self.product.code + ', Quantity: ' + str(self.total_items)

    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class Serializer(ss):
            product = serializers.PrimaryKeyRelatedField(required=True, queryset=Product.objects.all())
            number_of_packet = serializers.IntegerField(default=0)
            items_per_packet = serializers.IntegerField(default=0)
            loose_items = serializers.IntegerField(default=0)
            total_items = serializers.IntegerField(default=0)
            # unit = serializers.PrimaryKeyRelatedField(required=False, queryset=ProductUnit.objects.all())
            unit_price = serializers.DecimalField(max_digits=20, decimal_places=2, default=0.0)
            sub_total = serializers.DecimalField(max_digits=20, decimal_places=2, default=0.0)
            discount = serializers.DecimalField(max_digits=20, decimal_places=2, default=0.0)
            total = serializers.DecimalField(max_digits=20, decimal_places=2, default=0.0)

            class Meta(ss.Meta):
                model = cls

        return Serializer

    @classmethod
    def table_columns(cls):
        return 'code', 'product', 'total_items', 'unit_price', 'sub_total', 'total', 'last_updated'

    def save(self, *args, **kwargs):
        with transaction.atomic():
            if self.product_id is None:
                pass
            else:
                super().save(*args, **kwargs)

    @classmethod
    def default_order_by(cls):
        return "code"