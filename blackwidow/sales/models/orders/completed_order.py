from blackwidow.engine.decorators.enable_trigger import enable_trigger
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.sales.models.orders.order import Order
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum


# @decorate(expose_api('tx-stock-out'),
#           enable_trigger, save_audit_log, is_object_context,
#           route(route='tx-stock-out', display_name='Stock Out & Invoice', module=ModuleEnum.Execute, group='Primary Sales'))
class CompletedOrder(Order):

    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Print',
                action='print',
                icon='fbx-rightnav-print',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Print)
            ),
            dict(
                name='Stock out & Invoice',
                action='mutate',
                icon='fbx-rightnav-tick',
                ajax='0',
                classes='disabled' if self.is_locked else 'manage-action all-action confirm-action',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Mutate),
                parent=None
            ),
            dict(
                name='Print Invoice',
                action='print_invoice',
                icon='icon-arrow-right',
                ajax='0',
                classes='disabled',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Print)
            )
        ]

    def get_choice_name(self):
        return self.code

    # @classmethod
    # def success_url(cls):
    #     return '/incoming-inventories/'

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Details]

    # @classmethod
    # def get_button_title(cls, button=ViewActionEnum.Details):
    #     if button == ViewActionEnum.Mutate:
    #         return "Stock out & Invoice"
    #     return button.value

    class Meta:
        proxy = True


    # def mutate_to(self, cls=None):
    #     #sig_stock_out.send(self.__class__, order=self, user=self.created_by, organization=self.organization)
    #     ##self.is_locked = True
    #     self.save()
    #     return self

    @property
    def get_inline_manage_buttons(self):
        return [dict(
            name='Details',
            action='view',
            title="Click to view this item",
            icon='icon-eye',
            ajax='0',
            url_name=self.__class__.get_route_name(action=ViewActionEnum.Details),
            classes='all-action ',
            parent=None
        )]