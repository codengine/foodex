from django.db.models.loading import get_model
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from blackwidow.sales.models.orders.order import Order
from blackwidow.sales.signals.signals import sig_order_accepted
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum


#@decorate(expose_api('pending-orders'), route(route='pending-orders', group='Primary Sales', module=ModuleEnum.Execute, display_name="Pending Order"))
class PendingOrder(Order):

    class Meta:
        proxy = True

    @classmethod
    def get_button_title(cls, button=ViewActionEnum.Details):
        if button == ViewActionEnum.Mutate:
            return "Accept"
        return button.value

    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Edit',
                action='edit',
                icon='icon-pencil',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit)
            ),
            dict(
                name='Print',
                action='print',
                icon='icon-print',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Print)
            ),
            dict(
                name='Accept',
                action='mutate',
                icon='icon-arrow-right',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Mutate)
            )
        ]

    def mutate_to(self, cls=None):
        if cls is None:
            cls = get_model('sales', 'CompletedOrder')
        self.type = cls.__name__
        self.__class__ = cls
        self.save()

        sig_order_accepted.send(self.__class__, order=self, user=self.created_by, organization=self.organization)

        return self

    @property
    def get_inline_manage_buttons(self):
        return [
            dict(
                name='Edit',
                action='edit',
                title="Click to edit this item",
                icon='icon-pencil',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit),
                classes='all-action',
                parent=None
            ), dict(
                name='Delete',
                action='delete',
                title="Click to remove this item",
                icon='icon-remove',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Delete),
                classes='manage-action all-action confirm-action',
                parent=None
            ), dict(
                name='Details',
                action='view',
                title="Click to view this item",
                icon='icon-eye',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Details),
                classes='all-action ',
                parent=None
            ),  dict(
                name='Accept',
                action='view',
                title="Click to accept this order",
                icon='icon-arrow-right',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Mutate),
                url_params= {'pk': self.pk},
                classes='all-action confirm-action',
                parent=None
            )]