from django.db import models
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.sales.models.products.product import Product

__author__ = 'Sohel'

class OrderHistoryBreakdown(OrganizationDomainEntity):
    product = models.ForeignKey(Product)
    number_of_packet = models.IntegerField(default=0)
    items_per_packet = models.IntegerField(default=0)
    loose_items = models.IntegerField(default=0)
    total_items = models.IntegerField(default=0)
    unit_price = models.DecimalField(max_digits=20, decimal_places=2, default=0.0)
    sub_total = models.DecimalField(max_digits=20, decimal_places=2, default=0.0)
    discount = models.DecimalField(max_digits=20, decimal_places=2, default=0.0)
    total = models.DecimalField(max_digits=20, decimal_places=2, default=0.0)


    @classmethod
    def table_columns(cls):
        return 'code', 'product', 'total_items', 'unit_price', 'sub_total', 'total', 'last_updated'
