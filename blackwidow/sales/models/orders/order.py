from django.db import models
from blackwidow.core.mixins.modelmixin.export_model_mixin import ExportModelMixin

from blackwidow.core.models.common.location import Location
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig
from blackwidow.core.models.config.exporter_config import ExporterConfig
from blackwidow.core.models.structure.warehouse import WareHouse
from blackwidow.core.process.process_breakdown import ProcessBreakdown
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, is_object_context
from blackwidow.sales.models.invoice.invoice import Invoice
from blackwidow.sales.models.orders.order_breakdown import OrderBreakdown
from blackwidow.sales.models.payments.payment_methods import Payment
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum
from django.db.models.signals import m2m_changed


__author__ = 'Mahmud'


@decorate(is_object_context,expose_api('orders'))
class Order(OrganizationDomainEntity, ExportModelMixin):
    process_breakdown = models.ManyToManyField(ProcessBreakdown)
    breakdown = models.ManyToManyField(OrderBreakdown)
    warehouse = models.ForeignKey(WareHouse, null=True)
    client = models.ForeignKey(Client)
    invoice = models.ForeignKey(Invoice, null=True)
    location = models.ForeignKey(Location, null=True)
    discount = models.DecimalField(max_digits=10,decimal_places=2,default=0,null=True)
    gross_discount = models.DecimalField(max_digits=10,decimal_places=2,default=0,null=True)
    reference_po_number = models.CharField(max_length=120, default='', null=True)
    remarks = models.CharField(max_length=500,default='', null=True)

    def on_add_breakdown(self,sender,**kwargs):
        pass

    def __init__(self,*args,**kwargs):
        #m2m_changed.connect(self.on_add_breakdown, sender=self.breakdown.through)
        super().__init__(*args,**kwargs)

    @classmethod
    def default_order_by(cls):
        return "-last_updated"

    @classmethod
    def get_dependent_field_list(cls):
        return ['breakdown']

    @property
    def export_payments(self):
        return ','.join([str(x) for x in self.payments.all()])

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create, ViewActionEnum.Edit, ViewActionEnum.Delete, ViewActionEnum.Export, ViewActionEnum.Mutate]


    @classmethod
    def exporter_config(cls, organization=None, **kwargs):
            exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
            if not exporter_configs.exists():
                exporter_config = ExporterConfig()
                exporter_config.save(**kwargs)
            else:
                for e in exporter_configs:
                    e.delete()
                exporter_config = ExporterConfig()
                exporter_config.save(**kwargs)
            columns = [
                ExporterColumnConfig(column=0, column_name='Retailer Id', property_name='reference_id', ignore=False),
                ExporterColumnConfig(column=1, column_name='Type', property_name='type', ignore=False),
                ExporterColumnConfig(column=3, column_name='Code', property_name='code', ignore=False),
                ExporterColumnConfig(column=4, column_name='Last Updated', property_name='last_updated', ignore=False),
                ExporterColumnConfig(column=5, column_name='Client', property_name='client', ignore=False),
                ExporterColumnConfig(column=6, column_name='Location', property_name='location', ignore=False),
                # ExporterColumnConfig(column=7, column_name='Payments', property_name='export_payments', ignore=False),
                ExporterColumnConfig(column=7, column_name='delivery_orders', property_name='delivery_orders', ignore=False),
                ]
            for c in columns:
                c.save(organization=organization, **kwargs)
                exporter_config.columns.add(c)
            return exporter_config

    def export_item(self, workbook=None, columns=None, row_number=None):
        orders = self.breakdown.all()
        if orders.exists():
            row = row_number
            for order in orders:
                for column in columns:
                    if column.property_name == 'breakdown':
                        workbook.cell(row=row, column=column.column + 1).value = str(order)
                    else:
                        workbook.cell(row=row, column=column.column + 1).value = str(getattr(self, column.property_name))
                row += 1
            return self.pk, row
        else:
            for column in columns:
                workbook.cell(row=row_number, column=column.column + 1).value = str(getattr(self, column.property_name))
        return self.pk, row_number + 1

    @classmethod
    def finalize_export(cls, workbook=None, row_count=None):
        return workbook

    @classmethod
    def initialize_export(cls, workbook=None, columns=None, row_count=None, **kwargs):
        return workbook, 1


    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class Serializer(ss):
            breakdown = OrderBreakdown.get_serializer()(required=True, many=True)
            # payments = Payment.get_serializer()(many=True, required=False)
            location = Location.get_serializer()(required=True)

            class Meta(ss.Meta):
                model = cls

        return Serializer

    @property
    def tabs_config(self):
        tabs = [TabView(
            title='Order Breakdown(s)',
            access_key='breakdown',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            related_model=OrderBreakdown,
            property=self.breakdown
        )]
        return tabs

    @classmethod
    def table_columns(cls):
        return 'code', 'client', 'location', 'created_by', 'last_updated'