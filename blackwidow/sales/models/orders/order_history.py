from enum import Enum
from django.db import models, transaction
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.common.location import Location
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.structure.infrastructure_unit import InfrastructureUnit
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from blackwidow.sales.models.invoice.invoice_history import InvoiceHistory
from blackwidow.sales.models.orders.order_history_breakdown import OrderHistoryBreakdown
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Sohel'


class OrderHistoryStatusEnum(Enum):
    Created = "Created"
    Updated = "Updated"
    Approved = "Approved"
    Rejected = "Rejected"
    Deleted = "Deleted"
    ReverseRejected = "Reverse Rejected"


@decorate(route(route='order-history', group='Other admin', group_order=1, item_order=1,
                module=ModuleEnum.Administration, display_name="Order History"))
class OrderHistory(OrganizationDomainEntity):
    object_id = models.BigIntegerField()
    object_type = models.CharField(max_length=150)
    object_code = models.CharField(max_length=100, blank=True, default='')
    action = models.CharField(max_length=50, blank=False, default=OrderHistoryStatusEnum.Created.value)
    infrastructure_unit = models.ForeignKey(InfrastructureUnit, null=True)
    from_client = models.ForeignKey(Client, related_name='+', null=True)
    to_client = models.ForeignKey(Client, related_name='+', null=True)
    invoice = models.ForeignKey(InvoiceHistory, null=True)
    discount = models.DecimalField(max_digits=10, decimal_places=2, default=0, null=True)
    gross_discount = models.DecimalField(max_digits=10, decimal_places=2, default=0, null=True)
    reference_po_number = models.CharField(max_length=120, default='', null=True)
    remarks = models.CharField(max_length=500, default='', null=True)
    breakdown = models.ManyToManyField(OrderHistoryBreakdown)

    @classmethod
    def table_columns(cls):
        return 'code', 'object_id', 'object_type', "object_code", "action", 'infrastructure_unit', 'from_client', 'to_client', 'created_by', 'date_created'

    @property
    def details_config(self):
        d = super().details_config

        custom_list = ['code', 'object_id', 'object_type', "object_code", "action", 'infrastructure_unit',
                       'from_client', 'to_client', 'created_by', 'date_created']
        for key in d:
            if key in custom_list:
                pass
            else:
                del d[key]
        d['created_by'] = self.created_by
        d['last_updated_by'] = self.last_updated_by

        return d

    @classmethod
    def create_from_order(cls, order_object, action=OrderHistoryStatusEnum.Created.value, **kwargs):

        with transaction.atomic():
            order_history = OrderHistory()
            order_history.object_id = order_object.pk
            order_history.organization_id = order_object.organization_id
            order_history.object_type = order_object._meta.object_name
            order_history.object_code = order_object.code
            order_history.action = action
            order_history.infrastructure_unit = order_object.warehouse
            order_history.reference_id = order_object.reference_id
            order_history.is_active = order_object.is_active
            order_history.is_deleted = order_object.is_deleted
            order_history.created_by = order_object.created_by
            order_history.date_created = order_object.date_created
            order_history.last_updated_by = order_object.last_updated_by
            order_history.last_updated = order_object.last_updated

            if hasattr(order_object, "client"):
                order_history.to_client = order_object.client
            else:
                order_history.from_client = order_object.from_client
                order_history.to_client = order_object.to_client

            if hasattr(order_object, "discount"):
                order_history.discount = order_object.discount
                order_history.gross_discount = order_object.gross_discount
                order_history.reference_po_number = order_object.reference_po_number
                order_history.remarks = order_object.remarks

            order_history.save()

            for breakdown in order_object.breakdown.all():
                order_history_breakdown = OrderHistoryBreakdown()
                order_history_breakdown.product = breakdown.product
                order_history_breakdown.organization_id = breakdown.organization_id

                order_history_breakdown.number_of_packet = breakdown.number_of_packet
                order_history_breakdown.items_per_packet = breakdown.items_per_packet
                order_history_breakdown.loose_items = breakdown.loose_items
                order_history_breakdown.total_items = breakdown.total_items
                order_history_breakdown.unit_price = breakdown.unit_price
                order_history_breakdown.sub_total = breakdown.sub_total
                order_history_breakdown.discount = breakdown.discount
                order_history_breakdown.total = breakdown.total
                order_history_breakdown.date_created = breakdown.date_created
                order_history_breakdown.last_updated = breakdown.last_updated
                order_history_breakdown.save()

                order_history.breakdown.add(order_history_breakdown)

    @classmethod
    def clone_from_order(cls, order_object, action=OrderHistoryStatusEnum.Created.value, **kwargs):

        with transaction.atomic():
            order_history = OrderHistory()
            updated_order_history = OrderHistory()
            order_history.object_id = updated_order_history.object_id = order_object.pk
            order_history.organization_id = updated_order_history.organization_id = order_object.organization_id
            order_history.object_type = updated_order_history.object_type = order_object._meta.object_name
            order_history.object_code = updated_order_history.object_code = order_object.code
            order_history.action = action
            updated_order_history.action = OrderHistoryStatusEnum.Updated.value
            order_history.infrastructure_unit = updated_order_history.infrastructure_unit = order_object.warehouse
            order_history.reference_id = updated_order_history.reference_id = order_object.reference_id
            order_history.is_active = updated_order_history.is_active = order_object.is_active
            order_history.is_deleted = updated_order_history.is_deleted = order_object.is_deleted
            order_history.created_by = updated_order_history.created_by = order_object.created_by
            order_history.last_updated_by = updated_order_history.last_updated_by = order_object.last_updated_by
            order_history.date_created = order_object.date_created
            updated_order_history.date_created = order_object.last_updated

            if hasattr(order_object, "client"):
                order_history.to_client = updated_order_history.to_client = order_object.client
            else:
                order_history.from_client = updated_order_history.from_client = order_object.from_client
                order_history.to_client = updated_order_history.to_client = order_object.to_client

            if hasattr(order_object, "discount"):
                order_history.discount = updated_order_history.discount = order_object.discount
                order_history.gross_discount = updated_order_history.gross_discount = order_object.gross_discount
                order_history.reference_po_number = updated_order_history.reference_po_number = order_object.reference_po_number
                order_history.remarks = updated_order_history.remarks = order_object.remarks

            order_history.save()
            updated_order_history.save()

            for breakdown in order_object.breakdown.all():
                order_history_breakdown = OrderHistoryBreakdown()
                updated_order_history_breakdown = OrderHistoryBreakdown()
                order_history_breakdown.product = updated_order_history_breakdown.product = breakdown.product
                order_history_breakdown.organization_id = updated_order_history_breakdown.organization_id = breakdown.organization_id

                order_history_breakdown.number_of_packet = updated_order_history_breakdown.number_of_packet = breakdown.number_of_packet
                order_history_breakdown.items_per_packet = updated_order_history_breakdown.items_per_packet = breakdown.items_per_packet
                order_history_breakdown.loose_items = updated_order_history_breakdown.loose_items = breakdown.loose_items
                order_history_breakdown.total_items = updated_order_history_breakdown.total_items = breakdown.total_items
                order_history_breakdown.unit_price = updated_order_history_breakdown.unit_price = breakdown.unit_price
                order_history_breakdown.sub_total = updated_order_history_breakdown.sub_total = breakdown.sub_total
                order_history_breakdown.discount = updated_order_history_breakdown.discount = breakdown.discount
                order_history_breakdown.total = updated_order_history_breakdown.total = breakdown.total
                order_history_breakdown.date_created = breakdown.date_created
                order_history_breakdown.date_created = breakdown.last_updated
                order_history_breakdown.save()
                updated_order_history_breakdown.save()

                order_history.breakdown.add(order_history_breakdown)
                updated_order_history.breakdown.add(updated_order_history_breakdown)

    @property
    def tabs_config(self):
        tabs = [TabView(
            title='Order History Breakdown(s)',
            access_key='breakdown',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            related_model=OrderHistoryBreakdown,
            property=self.breakdown
        )]
        return tabs
