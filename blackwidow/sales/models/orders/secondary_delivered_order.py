from django.core.urlresolvers import reverse
from django.db.models.loading import get_model
from django.utils.safestring import mark_safe
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.sales.models.orders.secondary_order import SecondaryOrder
from blackwidow.sales.signals.signals import sig_order_accepted
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum


@decorate(expose_api('secondary-delivered-orders'), save_audit_log, is_object_context,
          route(route='secondary-delivered-orders', group='Secondary Sales', module=ModuleEnum.Execute, group_order=2, item_order=3, display_name="Delivered and Sold"))
class SecondaryDeliveredOrder(SecondaryOrder):

    def details_link_config(self, **kwargs):
        return [
        ]


    def mutate_to(self, *args, **kwargs):
        return self

    @classmethod
    def get_serializer(cls):
        ss = SecondaryOrder.get_serializer()

        class Serializer(ss):

            def create(self, attrs):
                from_client_id = attrs.get("from_client").pk
                obj = super().create(attrs)
                breakdown_set = obj.breakdown.all()
                for breakdown in breakdown_set:
                    c_inventory_class = get_model('foodex', 'ClientInventory')
                    c_inv = c_inventory_class.objects.get(product_id=breakdown.product.pk, assigned_to_id=from_client_id)
                    c_inv.stock = c_inv.stock - breakdown.total_items
                    c_inv.save()
                return obj

            class Meta(ss.Meta):
                model = cls

        return Serializer

    @classmethod
    def default_order_by(cls):
        return "-code"

    @classmethod
    def get_manage_buttons(cls):
        return [

        ]

    class Meta:
        proxy = True

    @property
    def get_inline_manage_buttons(self):
        return [dict(
            name='Details',
            action='view',
            title="Click to view this item",
            icon='icon-eye',
            ajax='0',
            url_name=self.__class__.get_route_name(action=ViewActionEnum.Details),
            classes='all-action ',
            parent=None
        ),  dict(
            name='Complete',
            action='view',
            title="Click to complete this order & mark as delivered",
            icon='icon-arrow-right',
            ajax='0',
            url_name=self.__class__.get_route_name(action=ViewActionEnum.Mutate),
            url_params= {'pk': self.pk},
            classes='all-action confirm-action',
            parent=None
        )]


    @property
    def render_from_distributor(self):
        if self.from_client:
            distributor = get_model("foodex", "Distributor")
            return mark_safe("<a class='inline-link' href='" + reverse(distributor.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.from_client.pk}) + "' >" + str(self.from_client) + "</a>")
        else:
            return None

    @classmethod
    def search_from_distributor(cls, queryset, term):
        return queryset.filter(from_client__name__icontains=term)

    @property
    def render_to_retailer(self):
        if self.to_client:
            retailer = get_model("foodex", "Retailers")
            return mark_safe("<a class='inline-link' href='" + reverse(retailer.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.to_client.pk}) + "' >" + str(self.to_client) + "</a>")
        else:
            return None

    @classmethod
    def search_to_retailer(cls, queryset, term):
        return queryset.filter(to_client__name__icontains=term)

    @classmethod
    def table_columns(cls):
        return 'code', 'render_from_distributor', 'render_to_retailer', 'location', 'created_by:Sold by', 'render_transaction_time', 'last_updated:Sync Time'

    @classmethod
    def exclude_search_fields(cls):
        return [
            'location', "render_transaction_time"
        ]




