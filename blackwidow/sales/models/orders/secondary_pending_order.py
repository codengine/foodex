from django.core.urlresolvers import reverse
from django.db.models import Sum
from django.db.models.loading import get_model
from django.utils.safestring import mark_safe
from blackwidow.core.models.common.location import Location
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.sales.models.orders.order_breakdown import OrderBreakdown
from blackwidow.sales.models.orders.order_history import OrderHistoryStatusEnum, OrderHistory
from blackwidow.sales.models.orders.secondary_order import SecondaryOrder
from blackwidow.sales.signals.signals import sig_order_accepted, sig_stock_out
from config.enums.modules_enum import ModuleEnum
from config.enums.view_action_enum import ViewActionEnum


@decorate(expose_api('secondary-pending-orders'), save_audit_log, is_object_context,
          route(route='secondary-pending-orders', group='Secondary Sales', module=ModuleEnum.Execute, group_order=2, item_order=1, display_name="New Order"))
class SecondaryPendingOrder(SecondaryOrder):

    class Meta:
        proxy = True

    @classmethod
    def get_button_title(cls, button=ViewActionEnum.Details):
        if button == ViewActionEnum.Mutate:
            return "Approve"
        return button.value

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create,ViewActionEnum.Edit,ViewActionEnum.Mutate,ViewActionEnum.Delete]

    def details_link_config(self, **kwargs):
        return [
            dict(
                name='Edit',
                action='edit',
                icon='fbx-rightnav-edit',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit)
            ),
            dict(
                name='Approve',
                action='mutate',
                icon='fbx-rightnav-tick',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Mutate)
            )
        ]

    def mutate_to(self, cls=None):
        if cls is None:
            cls = get_model('sales', 'SecondaryCompletedOrder')
        self.type = cls.__name__
        self.__class__ = cls
        self.save(keep_no_history=True)

        OrderHistory.create_from_order(self, action=OrderHistoryStatusEnum.Approved.value)

        return self

    @classmethod
    def default_order_by(cls):
        return "-code"

    @property
    def get_inline_manage_buttons(self):
        return [
            dict(
                name='Edit',
                action='edit',
                title="Click to edit this item",
                icon='icon-pencil',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Edit),
                classes='all-action',
                parent=None
            ), dict(
                name='Delete',
                action='delete',
                title="Click to remove this item",
                icon='icon-remove',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Delete),
                classes='manage-action all-action confirm-action',
                parent=None
            ), dict(
                name='Details',
                action='view',
                title="Click to view this item",
                icon='icon-eye',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Details),
                classes='all-action ',
                parent=None
            ),  dict(
                name='Accept',
                action='view',
                title="Click to accept this order",
                icon='icon-arrow-right',
                ajax='0',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Mutate),
                url_params= {'pk': self.pk},
                classes='all-action confirm-action',
                parent=None
            )]
    
    @property
    def render_total_amount(self):
        return self.breakdown.all().aggregate(Sum('total'))['total__sum']

    @property
    def render_from_distributor(self):
        if self.from_client:
            distributor = get_model("foodex", "Distributor")
            return mark_safe("<a class='inline-link' href='" + reverse(distributor.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.from_client.pk}) + "' >" + str(self.from_client) + "</a>")
        else:
            return None

    @classmethod
    def search_from_distributor(cls, queryset, term):
        return queryset.filter(from_client__name__icontains=term)

    @property
    def render_to_retailer(self):
        if self.to_client:
            retailer = get_model("foodex", "Retailers")
            return mark_safe("<a class='inline-link' href='" + reverse(retailer.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.to_client.pk}) + "' >" + str(self.to_client) + "</a>")
        else:
            return None

    @classmethod
    def search_to_retailer(cls, queryset, term):
        return queryset.filter(to_client__name__icontains=term)

    @classmethod
    def table_columns(cls):
        return 'code', 'render_from_distributor', 'render_to_retailer', 'location', 'created_by:Sold by', 'render_transaction_time', 'last_updated:Sync Time'

    @classmethod
    def exclude_search_fields(cls):
        return [
            'location', "render_transaction_time"
        ]

    @property
    def details_config(self):
        dic = super().details_config
        dic['Total Amount'] = self.render_total_amount
        return dic

