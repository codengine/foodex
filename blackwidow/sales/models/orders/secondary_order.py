from collections import OrderedDict
from django.db import models
from django.db.models.aggregates import Sum
from django.db import transaction

from blackwidow.core.models.common.location import Location
from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from blackwidow.core.models.clients.client import Client
from blackwidow.core.models.config.exporter_column_config import ExporterColumnConfig
from blackwidow.core.models.config.exporter_config import ExporterConfig
from blackwidow.core.models.structure.warehouse import WareHouse
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, save_audit_log, is_object_context
from blackwidow.sales.models.orders.order_breakdown import OrderBreakdown
from blackwidow.sales.models.orders.order_history import OrderHistoryStatusEnum, OrderHistory
from blackwidow.sales.models.orders.order_history_breakdown import OrderHistoryBreakdown
from blackwidow.sales.models.payments.payment_methods import Payment
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Mahmud'


@decorate(is_object_context, expose_api('secondary-orders'))
class SecondaryOrder(OrganizationDomainEntity):
    breakdown = models.ManyToManyField(OrderBreakdown)
    warehouse = models.ForeignKey(WareHouse, null=True)
    from_client = models.ForeignKey(Client, related_name='+', null=True)
    to_client = models.ForeignKey(Client, related_name='+', null=True)
    location = models.ForeignKey(Location, null=True)

    def save(self, *args, organization=None, **kwargs):

        keep_no_history = False
        if "keep_no_history" in kwargs:
            keep_no_history = kwargs.pop('keep_no_history')

        if not keep_no_history:
            action = OrderHistoryStatusEnum.Created.value
            if self.pk:
                action = OrderHistoryStatusEnum.Updated.value

        super(SecondaryOrder, self).save(*args, organization=organization, **kwargs)

        if not keep_no_history:
            OrderHistory.create_from_order(self, action=action)

    def delete(self, *args, force_delete=False, user=None, **kwargs):
        OrderHistory.create_from_order(self, action=OrderHistoryStatusEnum.Deleted.value)
        super(SecondaryOrder, self).delete(*args, force_delete=force_delete, user=user, **kwargs)

    @classmethod
    def get_dependent_field_list(cls):
        return ['breakdown']

    @property
    def export_payments(self):
        return ','.join([str(x) for x in self.payments.all()])

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create, ViewActionEnum.Edit, ViewActionEnum.Delete, ViewActionEnum.Export,
                ViewActionEnum.Mutate]

    @classmethod
    def get_serializer(cls):
        ss = OrganizationDomainEntity.get_serializer()

        class Serializer(ss):
            breakdown = OrderBreakdown.get_serializer()(required=True, many=True)
            location = Location.get_serializer()(required=True)

            def is_valid(self, raise_exception=True):
                if 'breakdown' in self.initial_data:
                    breakdown_initial_set = self.initial_data['breakdown']
                    modified_breakdown_set = []
                    for breakdown in breakdown_initial_set:
                        total_type = type(breakdown['total'])
                        subtotal_type = type(breakdown['sub_total'])
                        breakdown['sub_total'] = subtotal_type('%.2f' % breakdown['sub_total'])
                        breakdown['total'] = total_type('%.2f' % breakdown['total'])
                        modified_breakdown_set += [breakdown]

                    self.initial_data['breakdown'] = modified_breakdown_set

                return super().is_valid(raise_exception)

            def create(self, attrs):
                with transaction.atomic():
                    instance = super().create(attrs)

                    order_history = OrderHistory.objects.filter(object_id=instance.pk).order_by('-date_created').first()
                    for breakdown in instance.breakdown.all():
                        order_history_breakdown = OrderHistoryBreakdown()
                        order_history_breakdown.product = breakdown.product
                        order_history_breakdown.organization_id = breakdown.organization_id

                        order_history_breakdown.number_of_packet = breakdown.number_of_packet
                        order_history_breakdown.items_per_packet = breakdown.items_per_packet
                        order_history_breakdown.loose_items = breakdown.loose_items
                        order_history_breakdown.total_items = breakdown.total_items
                        order_history_breakdown.unit_price = breakdown.unit_price
                        order_history_breakdown.sub_total = breakdown.sub_total
                        order_history_breakdown.discount = breakdown.discount
                        order_history_breakdown.total = breakdown.total
                        order_history_breakdown.date_created = breakdown.date_created
                        order_history_breakdown.last_updated = breakdown.last_updated
                        order_history_breakdown.save()

                        order_history.breakdown.add(order_history_breakdown)

                    return instance

            class Meta(ss.Meta):
                model = cls

        return Serializer

    @property
    def tabs_config(self):
        tabs = [TabView(
            title='Order Breakdown(s)',
            access_key='breakdown',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            related_model=OrderBreakdown,
            property=self.breakdown
        )]
        return tabs

    @property
    def render_transaction_time(self):
        return self.render_timestamp(self.date_created)

    @classmethod
    def default_order_by(cls):
        return "date_created"

    @property
    def render_sync_time(self):
        return self.render_timestamp(self.last_updated)

    @classmethod
    def table_columns(cls):
        return 'code', 'from_client:From Distributor', 'to_client:To Retailer', 'location', 'created_by:Sold by', 'render_transaction_time', 'last_updated:Sync Time'

    @property
    def render_total_transaction_value(self):
        return self.breakdown.all().aggregate(Sum('total'))['total__sum']

    @property
    def details_config(self):
        dic = OrderedDict()

        dic['code'] = self.code
        dic['From Distributor'] = self.from_client
        dic['To Retailer'] = self.to_client
        dic['location'] = self.location
        dic['Sold by'] = self.created_by
        dic['transaction_time'] = self.render_transaction_time
        dic['sync_time'] = self.render_sync_time
        dic['Total Amount'] = self.render_total_transaction_value

        return dic
