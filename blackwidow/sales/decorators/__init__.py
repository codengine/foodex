__author__ = "auto generated"

from blackwidow.sales.decorators.model_decorators import is_transaction_host


__all__ = ['is_transaction_host']
