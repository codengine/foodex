__author__ = 'Mahmud'


def is_transaction_host(original_class):
    return original_class


def enable_individual_assignment(models=[], **kwargs):
    registry = {}
    kwargs.update({
        'models': models
    })

    def enable_individual_assignment(original_class):
        if '_registry' in dir(original_class):
            if original_class.__name__ not in original_class._registry:
                original_class._registry[original_class.__name__] = dict()

            if 'enable_individual_assignment' in original_class._registry[original_class.__name__]:
                original_class._registry[original_class.__name__]['enable_individual_assignment'].update(**kwargs)
            else:
                original_class._registry[original_class.__name__]['enable_individual_assignment'] = dict(**kwargs)
        else:
            registry[original_class.__name__] = dict()
            registry[original_class.__name__]['enable_individual_assignment'] = dict(**kwargs)
            original_class._registry = registry
        return original_class
    return enable_individual_assignment



