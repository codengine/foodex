import django.dispatch

sig_product_added = django.dispatch.Signal(providing_args=["product"]) #signal raised at product creation, accepted at product_price_config
sig_product_deleted = django.dispatch.Signal(providing_args=["product"])
sig_order_accepted = django.dispatch.Signal(providing_args=["order"])
sig_stock_out = django.dispatch.Signal(providing_args=["order"])
sig_client_added = django.dispatch.Signal(providing_args=["client_added"]) #signal raised at any client creation, accepted at product_price_config
sig_iunit_added = django.dispatch.Signal(providing_args=["infrastructure_unit_added"]) #signal raised at infrastructure creation, accepted at product_price_config
sig_reverse_reject = django.dispatch.Signal(providing_args=["reverse_reject"])