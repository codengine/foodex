from __future__ import absolute_import
from celery.schedules import crontab
from celery.task import periodic_task
from blackwidow.core.models.log.logs import ErrorLog, SchedulerLog
from foodex.models.kpi.product_group_sales_kpi_monthly import ProductGroupSalesMonthlyKPI

@periodic_task(run_every=crontab(minute=30, hour=8)) #Run at 2:30 PM daily
def initialize_missing_target_unit_product(*args, **kwargs):
    try:
        ProductGroupSalesMonthlyKPI.initialize_unit_product_targets()
        SchedulerLog.log(exp='Scheduled task executed for initialize_missing_target_unit_product',stacktrace='Scheduled task executed for initialize_missing_target_unit_product')
    except Exception as exp:
        ErrorLog.log(exp, stacktrace=str(exp))

@periodic_task(run_every=crontab(minute=30, hour=7)) #Run at 1:30 PM daily
def initialize_missing_target_product_group(*args, **kwargs):
    try:
        ProductGroupSalesMonthlyKPI.initialize_product_group_targets()
        SchedulerLog.log(exp='Scheduled task executed for initialize_missing_target_product_group',stacktrace='Scheduled task executed for initialize_missing_target_product_group')
    except Exception as exp:
        ErrorLog.log(exp, stacktrace=str(exp))

@periodic_task(run_every=crontab(minute=0, hour=21)) #Run at 3 AM daily
def trigger_product_group_kpi_calculation(*args, **kwargs):
    try:
        # ProductGroupSalesMonthlyKPI.calculate_kpi()
        SchedulerLog.log(exp='Scheduled task executed for trigger_product_group_kpi_calculation',stacktrace='Scheduled task executed for trigger_product_group_kpi_calculation')
    except Exception as exp:
        ErrorLog.log(exp, stacktrace=str(exp))

@periodic_task(run_every=crontab(minute=0, hour=17)) #Run at 2:00 AM daily
def trigger_sales_kpi_monthly_calculation(*args, **kwargs):
    try:
        # SalesKPIMonthly.calculate_kpi()
        SchedulerLog.log(exp='Scheduled task executed for trigger_sales_kpi_monthly_calculation', stacktrace='Scheduled task executed for trigger_sales_kpi_monthly_calculation')
    except Exception as exp:
        ErrorLog.log(exp, stacktrace=str(exp))

