import operator
from functools import reduce

__author__ = 'Machine II'


def validated_report_param(model, query, text):
    record_count = model.objects.filter(reduce(operator.and_,query)).count()
    if record_count > 0:
        return text
    else:
        return ''