from settings import PROJECT_PATH
import os

__author__ = 'Machine II'

REPORT_SERVER = 'http://myfoodex.info:8000'
# REPORT_SERVER = 'http://localhost:8080'

DEFAULT_USER = 'admin'
DEFAULT_KEY = 'admin'
DEFAULT_AUTH = 'username=' + DEFAULT_USER + '&password=' + DEFAULT_KEY

PARAM_BEGIN = '/?'
PARAM_JOIN = '&'
EMBED_MODE = 'mode=embed&plugin=true'
QUERY_OPEN = '#query/open//homes/home:' + DEFAULT_USER + '/'

QUERY_OPEN_MANUAL_PARAM_URL = REPORT_SERVER + PARAM_BEGIN + DEFAULT_AUTH + PARAM_JOIN + EMBED_MODE
QUERY_OPEN_URL = REPORT_SERVER + PARAM_BEGIN + DEFAULT_AUTH + PARAM_JOIN + EMBED_MODE + QUERY_OPEN

# REPORT_SERVER_ABS_PATH = os.path.abspath(os.path.join(PROJECT_PATH, '..','..','OLAP','saiku-latest','saiku-server','tomcat','webapps','ROOT'))
REPORT_SERVER_ABS_PATH = os.path.abspath(os.path.join(PROJECT_PATH, '..','..','OLAP','saiku-latest','saiku-server','tomcat','webapps','ROOT'))
TARGET_HTML_PATH = os.path.join(REPORT_SERVER_ABS_PATH, 'index.html')
ADMIN_HTML_PATH = os.path.join(REPORT_SERVER_ABS_PATH, 'index-admin.html')
PRODUCTION_HTML_PATH = os.path.join(REPORT_SERVER_ABS_PATH, 'index-production.html')