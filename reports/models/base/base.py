from blackwidow.core.models.contracts.organizationdomainentity import OrganizationDomainEntity
from config.constants.reporting_server import DEFAULT_DIR, DEFAULT_USER, DEFAULT_PASSWORD

__author__ = 'Machine II'

class Report(OrganizationDomainEntity):
    @classmethod
    def manage_action_config(cls):
        return []

    def get_filtered_data(self,_request,_model,_queryset=None):
        _user = _request.c_user.to_business_user()
        if _queryset is None:
            _queryset = _model.objects.all()
        _queryset = _model.get_queryset(queryset=_queryset,user=_user,profile_filter=not(_user.is_super))
        _queryset=_user.filter_model(request=_request, queryset=_queryset)
        return _model.apply_search_filter(request=_request, queryset=_queryset)

class OlapReport(Report):

    @classmethod
    def get_report_url(cls, report_file, directory = DEFAULT_DIR, params = [], plugin_mode=True, username=DEFAULT_USER,
                       password=DEFAULT_PASSWORD, **kwargs):
        url = REPORTING_SERVER
        url += '?'
        if plugin_mode:
            url += 'plugin=true&'
        params.append({'key': 'username', 'value': username})
        params.append({'key': 'password', 'value': password})
        params.append({'key': 'mode', 'value': 'view'})
        for param in params:
            url += param['key'] + '=' + param['value'] + '&'
        url += '#query/open//' + directory + report_file

        return url

    class Meta:
        proxy=True
