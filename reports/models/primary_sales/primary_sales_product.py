from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from config.enums.modules_enum import ModuleEnum
from reports.models.base.base import OlapReport

__author__ = 'Machine II'

@decorate(route(route='primary-sales-product', group='Primary Sales', group_order=3, item_order=2, module=ModuleEnum.Reports, display_name="Products"))
class PrimarySalesReportByProduct(OlapReport):
    class Meta:
        proxy = True
