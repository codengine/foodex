from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from blackwidow.core.models import Client
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from blackwidow.sales.models.invoice.invoice import Invoice
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from reports.models import Report
from datetime import datetime, timedelta
from django.db.models.aggregates import Sum
from blackwidow.engine.extensions.model_descriptor import get_model_by_name

__author__ = 'Mahmud, Ziaul Haque'


@decorate(route(route='client-invoice-report', group='Primary Sales', group_order=3, module=ModuleEnum.Reports,
                display_name="Clientwise Total Invoice Value"))
class ClientInvoiceReport(Report):
    class Meta:
        proxy = True

    def build_report(self, request=None, report_type=None, styled=False, **kwargs):
        if report_type is not None:
            invoices = Invoice.objects.exclude(counter_part__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100'])
            if report_type == 'Daily':
                today_begin = datetime.now().replace(hour=0, minute=0, second=0).timestamp() * 1000
                client_invoices = invoices.filter(date_created__gte=today_begin).values(
                    'counter_part').annotate(
                    Sum('price_total'))

            elif report_type == 'Weekly':
                today = datetime.now().replace(hour=0, minute=0, second=0)
                if today.weekday() == 6:
                    week_begin = today - timedelta(days=1)
                elif today.weekday() < 6:
                    week_begin = today - timedelta(days=(today.weekday() + 1))
                week_begin = week_begin.timestamp() * 1000
                client_invoices = invoices.filter(date_created__gte=week_begin).values(
                    'counter_part').annotate(Sum('price_total'))

            elif report_type == 'Monthly':
                month_begin = datetime.now().replace(day=1, hour=0, minute=0, second=0).timestamp() * 1000
                client_invoices = invoices.filter(date_created__gte=month_begin).values(
                    'counter_part').annotate(Sum('price_total'))

            elif report_type == 'Yearly':
                year_begin = datetime.now().replace(month=1, day=1, hour=0, minute=0, second=0).timestamp() * 1000
                client_invoices = invoices.filter(date_created__gte=year_begin).values(
                    'counter_part').annotate(Sum('price_total'))

        report = []
        if styled:
            report.append(('Client', 'Total Value Invoiced'))
        else:
            report.append(['Client', 'Total Value Invoiced'])

        for invoice_value in client_invoices:
            clients = Client.objects.filter(pk=invoice_value['counter_part'])
            if clients.exists():
                client_obj = clients.first()
                model = get_model_by_name(client_obj.type)
                client = mark_safe("<a class='inline-link' target='_blank' href='" + reverse(
                    model.get_route_name(ViewActionEnum.Details),
                    kwargs={'pk': client_obj.pk}) + "' >" + client_obj.__str__() + "</a>")
            else:
                client = 'No Reference'

            if styled:
                _item = (client, str(invoice_value['price_total__sum']))
            else:
                _item = [client, str(invoice_value['price_total__sum'])]
            report.append(_item)

        if not styled:
            report = sorted(report)
        return report
