from django.db import models
from datetime import datetime, timedelta
from django.db.models.aggregates import Sum
from blackwidow.sales.models.invoice.invoice import Invoice

__author__ = 'Mahmud'


class DailyClientInvoiceModelManager(models.Manager):
    def get_queryset(self):
        today_begin = datetime.now().replace(hour=0,minute=0,second=0).timestamp()*1000
        return Invoice.objects.filter(date_created__gte=today_begin).values('counter_part').annotate(Sum('price_total'))


class WeeklyClientInvoiceModelManager(models.Manager):
    def get_queryset(self):
        week_begin = datetime.now().replace(hour=0, minute=0, second=0)
        if week_begin.weekday() == 7:
            today = week_begin - timedelta(days=1)
        elif week_begin.weekday() < 6:
            today = week_begin - timedelta(days=(week_begin.weekday()+1))
        week_begin = week_begin.timestamp()*1000
        return Invoice.objects.filter(date_created__gte=week_begin).values('counter_part').annotate(Sum('price_total'))


class MonthlyClientInvoiceModelManager(models.Manager):
    def get_queryset(self):
        month_begin = datetime.now().replace(day=1,hour=0,minute=0,second=0).timestamp()*1000
        return Invoice.objects.filter(date_created__gte=month_begin).values('counter_part').annotate(Sum('price_total'))


class YearlyClientInvoiceModelManager(models.Manager):
    def get_queryset(self):
        year_begin = datetime.now().replace(month=1,day=1,hour=0,minute=0,second=0).timestamp()*1000
        return Invoice.objects.filter(date_created__gte=year_begin).values('counter_part').annotate(Sum('price_total'))
