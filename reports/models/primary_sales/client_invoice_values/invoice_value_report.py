from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from blackwidow.sales.models.invoice.invoice import Invoice
from config.enums.modules_enum import ModuleEnum
from reports.models import Report
from datetime import datetime, timedelta
from django.db.models.aggregates import Sum

__author__ = 'Ziaul Haque'


@decorate(route(route='total-invoice-value-report', group='Primary Sales', group_order=3, module=ModuleEnum.Reports,
                display_name="Total Invoice Value"))
class TotalInvoiceValueReport(Report):
    class Meta:
        proxy = True

    def build_report(self, request=None, **kwargs):
        invoices = Invoice.objects.exclude(counter_part__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100'])
        today_begin = datetime.now().replace(hour=0, minute=0, second=0).timestamp() * 1000
        daily_total_invoice = invoices.filter(date_created__gte=today_begin).aggregate(
            Sum('price_total'))

        today = datetime.now().replace(hour=0, minute=0, second=0)
        if today.weekday() == 6:
            week_begin = today - timedelta(days=1)
        elif today.weekday() < 6:
            week_begin = today - timedelta(days=(today.weekday() + 1))
        week_begin = week_begin.timestamp() * 1000
        weekly_total_invoice = invoices.filter(date_created__gte=week_begin).aggregate(
            Sum('price_total'))

        month_begin = datetime.now().replace(day=1, hour=0, minute=0, second=0).timestamp() * 1000
        monthly_total_invoice = invoices.filter(date_created__gte=month_begin).aggregate(
            Sum('price_total'))

        year_begin = datetime.now().replace(month=1, day=1, hour=0, minute=0, second=0).timestamp() * 1000
        yearly_total_invoice = invoices.filter(date_created__gte=year_begin).aggregate(
            Sum('price_total'))

        report = list()
        report.append(
            ('Total Invoice Value of the Day', str(daily_total_invoice['price_total__sum'] if daily_total_invoice[
                                                                                                  'price_total__sum'] is not None else 0.0) + " TK"),
        )
        report.append(
            ('Total Invoice Value of the Week', str(weekly_total_invoice['price_total__sum'] if weekly_total_invoice[
                                                                                                    'price_total__sum'] is not None else 0.0) + " TK")
        )
        report.append(
            ('Total Invoice Value of the Month', str(monthly_total_invoice['price_total__sum'] if monthly_total_invoice[
                                                                                                      'price_total__sum'] is not None else 0.0) + "TK")
        )
        report.append(
            ('Total Invoice Value of the Year', str(yearly_total_invoice['price_total__sum'] if yearly_total_invoice[
                                                                                                    'price_total__sum'] is not None else 0.0) + "TK")
        )
        return report
