from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from blackwidow.core.models import Client
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from reports.models import Report
from datetime import datetime
from django.db.models.aggregates import Sum
from foodex.models.transactions.completed_sales_transaction import CompletedSalesTransaction
from blackwidow.engine.extensions.model_descriptor import get_model_by_name

__author__ = 'Ziaul Haque'


@decorate(
    route(route='todays-primary-sale-with-customer', group='Primary Sales', group_order=3, module=ModuleEnum.Reports,
          display_name="Today's Customer With Primary Sale Value"))
class TodayPrimarySaleForCustomerReport(Report):
    class Meta:
        proxy = True

    def build_report(self, request=None, styled=False, **kwargs):
        today_begin = datetime.now().replace(hour=0, minute=0, second=0).timestamp() * 1000
        completed_sales_transactions = CompletedSalesTransaction.objects.exclude(
            client__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100'])
        today_sales = completed_sales_transactions.filter(last_updated__gte=today_begin).values('client').annotate(
            Sum('total'))

        report = []
        if styled:
            report.append(('Client', 'Primary Sale Value'))
        else:
            report.append(['Client', 'Primary Sale Value'])

        for sale in today_sales:
            clients = Client.objects.filter(pk=sale['client'])
            if clients.exists():
                client_obj = clients.first()
                model = get_model_by_name(client_obj.type)
                client = mark_safe("<a class='inline-link' target='_blank' href='" + reverse(
                    model.get_route_name(ViewActionEnum.Details),
                    kwargs={'pk': client_obj.pk}) + "' >" + client_obj.__str__() + "</a>")
            else:
                client = 'No Reference'

            if styled:
                _item = (client, str(sale['total__sum']))
            else:
                _item = [client, str(sale['total__sum'])]
            report.append(_item)

        if not styled:
            report = sorted(report)
        return report
