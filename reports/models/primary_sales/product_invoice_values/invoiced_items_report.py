from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from blackwidow.sales.models import TransactionBreakDown, Product
from blackwidow.sales.models.invoice.invoice import Invoice
from config.enums.modules_enum import ModuleEnum
from foodex.models import DeliveryTransaction
from reports.models import Report
from datetime import datetime
from django.db.models.aggregates import Sum

__author__ = 'Ziaul Haque'


@decorate(route(route='invoiced-items-report', group='Primary Sales', group_order=3, module=ModuleEnum.Reports,
                display_name="Today's Invoiced Items"))
class InvoicedItemReport(Report):
    class Meta:
        proxy = True

    def build_report(self, request=None, styled=False, **kwargs):
        invoices = Invoice.objects.exclude(counter_part__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100'])
        today_begin = datetime.now().replace(hour=0, minute=0, second=0).timestamp() * 1000
        today_invoices = list(invoices.filter(last_updated__gte=today_begin,
                                              type__in=['OpenInvoice', 'ClosedInvoice']))

        delivery_transactions = DeliveryTransaction.objects.filter(invoice__in=today_invoices)
        breakdown_ids = []
        for transaction in delivery_transactions:
            for b in transaction.breakdown.all():
                breakdown_ids.append(b.id)

        transaction_breakdowns = TransactionBreakDown.objects.filter(pk__in=breakdown_ids).values('product').annotate(
            Sum('quantity'))

        report = []
        if styled:
            report.append(('Item', 'Quantity'))
        else:
            report.append(['Item', 'Quantity'])

        for tx_breakdown in transaction_breakdowns:
            products = Product.objects.filter(pk=tx_breakdown['product'])
            if products.exists():
                product_obj = products.first()
                product = product_obj.__str__()
            else:
                product = 'No Reference'

            if styled:
                _item = (product, str(tx_breakdown['quantity__sum']))
            else:
                _item = [product, str(tx_breakdown['quantity__sum'])]
            report.append(_item)

        if not styled:
            report = sorted(report)
        return report