from datetime import datetime, timedelta
from django.core.urlresolvers import reverse
from django.db.models.aggregates import Sum
from django.utils.safestring import mark_safe
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.extensions.model_descriptor import get_model_by_name
from blackwidow.sales.models.orders.order_breakdown import OrderBreakdown
from blackwidow.sales.models.orders.order_history import OrderHistoryStatusEnum
from blackwidow.sales.models.orders.order_history_breakdown import OrderHistoryBreakdown
from blackwidow.sales.models.orders.secondary_pending_order import SecondaryPendingOrder
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from reports.models import Report
from django.db.models import Count

__author__ = 'Tareq'


@decorate(route(route='canceled-deleted-orders', group='Secondary Sales', group_order=4, module=ModuleEnum.Reports,
                display_name="Canceled Orders/Deleted Orders"))
class CanceledDeletedOrderReport(Report):
    class Meta:
        proxy = True

    def build_report(self, request=None, report_type=None, styled=False, **kwargs):
        cancel_time_range = (datetime.now() - timedelta(days=7)).timestamp() * 1000
        deleted_domain = OrderHistoryBreakdown.objects.filter(orderhistory__object_type=SecondaryPendingOrder.__name__,
                                                              orderhistory__action=OrderHistoryStatusEnum.Deleted.value)
        canceled_domain = OrderBreakdown.objects.filter(date_created__lte=cancel_time_range,
                                                        secondaryorder__type='SecondaryPendingOrder')

        ################
        if report_type is None or report_type == 'Region':
            report_type = 'Region'
            deleted_pk = 'orderhistory__to_client__clientassignment__infrastructureunit__parent_client__assigned_to__parent_id'
            canceled_pk = 'secondaryorder__to_client__clientassignment__infrastructureunit__parent_client__assigned_to__parent_id'
        elif report_type == 'Area':
            deleted_pk = 'orderhistory__to_client__clientassignment__infrastructureunit__parent_client__assigned_to__pk'
            canceled_pk = 'secondaryorder__to_client__clientassignment__infrastructureunit__parent_client__assigned_to__pk'
        else:
            deleted_pk = 'orderhistory__to_client__clientassignment__infrastructureunit__parent_client__pk'
            canceled_pk = 'secondaryorder__to_client__clientassignment__infrastructureunit__parent_client__pk'

        deleted_orders = deleted_domain.values(deleted_pk).annotate(
            total_value=Sum('total'))
        deleted_orders_count = deleted_domain.values(deleted_pk).annotate(
            total_count=Count('orderhistory__pk', distinct=True))
        canceled_orders = canceled_domain.values(canceled_pk).annotate(
            total_value=Sum('total'))
        canceled_orders_count = canceled_domain.values(canceled_pk).annotate(
            total_count=Count('secondaryorder__pk', distinct=True))

        report_dict = dict()

        for order in deleted_orders:
            model = get_model_by_name(report_type)
            region = model.objects.filter(pk=order[
                deleted_pk])
            if region.exists():
                region_obj = region.first()
                region = mark_safe("<a class='inline-link' target='_blank' href='" + reverse(
                    model.get_route_name(ViewActionEnum.Details),
                    kwargs={'pk': region_obj.pk}) + "' >" + region_obj.__str__() + "</a>")
                report_dict[order[
                    deleted_pk]] = {
                    'region': region,
                    'deleted_count': 0,
                    'deleted_value': order['total_value'],
                    'canceled_count': 0,
                    'canceled_value': 0
                }

        for order in deleted_orders_count:
            if order[
                deleted_pk] in report_dict:
                report_dict[order[
                    deleted_pk]][
                    'deleted_count'] = order['total_count']

        for order in canceled_orders:
            if order[
                canceled_pk] in report_dict:
                report_dict[order[
                    canceled_pk]][
                    'canceled_value'] = order['total_value']
                continue
            model = get_model_by_name(report_type)
            region = model.objects.filter(pk=order[
                canceled_pk])
            if region.exists():
                region_obj = region.first()
                region = mark_safe("<a class='inline-link' target='_blank' href='" + reverse(
                    model.get_route_name(ViewActionEnum.Details),
                    kwargs={'pk': region_obj.pk}) + "' >" + region_obj.__str__() + "</a>")
                report_dict[order[
                    canceled_pk]] = {
                    'region': region,
                    'deleted_count': 0,
                    'deleted_value': 0,
                    'canceled_count': 0,
                    'canceled_value': order['total_value']
                }

        for order in canceled_orders_count:
            if order[
                canceled_pk] in report_dict:
                report_dict[order[
                    canceled_pk]][
                    'canceled_count'] = order['total_count']

        report = []
        if styled:
            report.append(('Region', 'Canceled Order count', 'Canceled Order Value (BDT)', 'Deleted Order Count',
                           'Deleted Order value (BDT)'))
        else:
            report.append(['Region', 'Canceled Order count', 'Canceled Order Value (BDT)', 'Deleted Order Count',
                           'Deleted Order value (BDT)'])

        for order in report_dict.items():
            order = order[1]
            if styled:
                _item = (order['region'], order['canceled_count'], order['canceled_value'], order['deleted_count'],
                         order['deleted_value'])
            else:
                _item = [order['region'], order['canceled_count'], order['canceled_value'], order['deleted_count'],
                         order['deleted_value']]
            report.append(_item)

        if not styled:
            report = sorted(report)
        return report
