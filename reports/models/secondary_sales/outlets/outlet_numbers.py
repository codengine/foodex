from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from foodex.models import Retailers, Area
from foodex.models.clients.distributor import Distributor
from foodex.models.infrastructure.routes import Route
from reports.models import Report
from blackwidow.engine.extensions.model_descriptor import get_model_by_name
from django.db.models import Count

__author__ = 'Tareq'


@decorate(route(route='outlet-numbers', group='Secondary Sales', group_order=4, module=ModuleEnum.Reports,
                display_name="Outlet Numbers"))
class OutletNumberReport(Report):
    class Meta:
        proxy = True

    def build_report(self, request=None, report_type=None, styled=False, **kwargs):
        if report_type is None or report_type == 'Area':
            retailers = Retailers.objects.filter(parent__assigned_to__type=Area.__name__).exclude(
                parent__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100'])
            area_wise_retailers = retailers.values('parent__assigned_to').annotate(
                total_retailer=Count('parent__assigned_to'))

            report = []
            if styled:
                report.append(('Area', 'No of Outlets'))
            else:
                report.append(['Area', 'No of Outlets'])

            for outlet in area_wise_retailers:
                areas = Area.objects.filter(pk=outlet['parent__assigned_to'])
                if areas.exists():
                    area_obj = areas.first()
                    model = get_model_by_name(area_obj.type)
                    area = mark_safe("<a class='inline-link' target='_blank' href='" + reverse(
                        model.get_route_name(ViewActionEnum.Details),
                        kwargs={'pk': area_obj.pk}) + "' >" + area_obj.__str__() + "</a>")
                else:
                    area = 'No Reference'

                if styled:
                    _item = (area, str(outlet['total_retailer']))
                else:
                    _item = [area, str(outlet['total_retailer'])]
                report.append(_item)

            if not styled:
                report = sorted(report)
            return report

        elif report_type == 'Distributor':
            retailers = Retailers.objects.filter(parent__type=Distributor.__name__,
                                                 parent__assigned_to__type=Area.__name__).exclude(
                parent__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100'])
            distributor_wise_retailers = retailers.values('parent', 'parent__assigned_to').annotate(
                total_retailer=Count('parent'))

            report = []
            if styled:
                report.append(('Distributor', 'Area', 'No of Outlets'))
            else:
                report.append(['Distributor', 'Area', 'No of Outlets'])

            for outlet in distributor_wise_retailers:
                distributors = Distributor.objects.filter(pk=outlet['parent'])
                if distributors.exists():
                    dis_obj = distributors.first()
                    model = get_model_by_name(dis_obj.type)
                    distributor = mark_safe("<a class='inline-link' target='_blank' href='" + reverse(
                        model.get_route_name(ViewActionEnum.Details),
                        kwargs={'pk': dis_obj.pk}) + "' >" + dis_obj.__str__() + "</a>")
                else:
                    distributor = 'No Reference'

                areas = Area.objects.filter(pk=outlet['parent__assigned_to'])
                if areas.exists():
                    area_obj = areas.first()
                    model = get_model_by_name(area_obj.type)
                    area = mark_safe("<a class='inline-link' target='_blank' href='" + reverse(
                        model.get_route_name(ViewActionEnum.Details),
                        kwargs={'pk': area_obj.pk}) + "' >" + area_obj.__str__() + "</a>")
                else:
                    area = 'No Reference'

                if styled:
                    _item = (distributor, area, str(outlet['total_retailer']))
                else:
                    _item = [distributor, area, str(outlet['total_retailer'])]
                report.append(_item)

            if not styled:
                report = sorted(report)
            return report

        else:
            retailers = Retailers.objects.filter(parent__type=Distributor.__name__,
                                                 parent__assigned_to__type=Area.__name__).exclude(
                parent__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100'])
            route_wise_retailers = retailers.values('clientassignment__infrastructureunit', 'parent',
                                                    'parent__assigned_to').annotate(
                total_retailer=Count('parent'))

            report = []
            if styled:
                report.append(('Route', 'Distributor', 'Area', 'No of Outlets'))
            else:
                report.append(['Route', 'Distributor', 'Area', 'No of Outlets'])

            for outlet in route_wise_retailers:
                route = Route.objects.filter(pk=outlet['clientassignment__infrastructureunit']).first()
                if route is None:
                    route = 'No Reference'
                else:
                    route = mark_safe('<a class="inline-link" target="_blank" href="' + reverse(
                        Route.get_route_name(ViewActionEnum.Details),
                        kwargs={'pk': route.pk}) + '">' + route.__str__() + '</a>')

                distributors = Distributor.objects.filter(pk=outlet['parent'])
                if distributors.exists():
                    dis_obj = distributors.first()
                    model = get_model_by_name(dis_obj.type)
                    distributor = mark_safe("<a class='inline-link' target='_blank' href='" + reverse(
                        model.get_route_name(ViewActionEnum.Details),
                        kwargs={'pk': dis_obj.pk}) + "' >" + dis_obj.__str__() + "</a>")
                else:
                    distributor = 'No Reference'

                areas = Area.objects.filter(pk=outlet['parent__assigned_to'])
                if areas.exists():
                    area_obj = areas.first()
                    model = get_model_by_name(area_obj.type)
                    area = mark_safe("<a class='inline-link' target='_blank' href='" + reverse(
                        model.get_route_name(ViewActionEnum.Details),
                        kwargs={'pk': area_obj.pk}) + "' >" + area_obj.__str__() + "</a>")
                else:
                    area = 'No Reference'

                if styled:
                    _item = (route, distributor, area, str(outlet['total_retailer']))
                else:
                    _item = [route, distributor, area, str(outlet['total_retailer'])]
                report.append(_item)

            if not styled:
                report = sorted(report)
            return report
