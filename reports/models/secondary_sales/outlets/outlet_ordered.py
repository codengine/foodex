from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from blackwidow.sales.models import SecondaryTransaction, SecondaryCompletedOrder
from blackwidow.sales.models.orders.secondary_order import SecondaryOrder
from config.enums.modules_enum import ModuleEnum
from foodex.models import Retailers
from reports.models import Report
from datetime import datetime, timedelta
from django.db.models.aggregates import Sum

__author__ = 'Ziaul Haque'


@decorate(route(route='outlet-ordered-report', group='Secondary Sales', group_order=4, module=ModuleEnum.Reports,
                display_name="Out-let Summary Report"))
class OutletOrderedReport(Report):
    class Meta:
        proxy = True

    def build_report(self, request=None, **kwargs):
        secondary_transaction = SecondaryTransaction.objects.exclude(
            client__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100'])
        today_begin = datetime.now().replace(hour=0, minute=0, second=0).timestamp() * 1000
        today_transaction_value = secondary_transaction.filter(date_created__gte=today_begin).aggregate(Sum('total'))

        sec_completed_orders = SecondaryOrder.objects.exclude(
            from_client__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100'])
        today_total_outlet = sec_completed_orders.filter(date_created__gte=today_begin).distinct('to_client').count()

        today = datetime.now().replace(hour=0, minute=0, second=0)
        if today.weekday() == 6:
            week_begin = today - timedelta(days=1)
        elif today.weekday() < 6:
            week_begin = today - timedelta(days=(today.weekday() + 1))
        week_begin = week_begin.timestamp() * 1000
        weekly_total_outlet = sec_completed_orders.filter(date_created__gte=week_begin).distinct('to_client').count()

        month_begin = datetime.now().replace(day=1, hour=0, minute=0, second=0).timestamp() * 1000
        monthly_total_outlet = sec_completed_orders.filter(date_created__gte=month_begin).distinct('to_client').count()

        report = list()
        report.append(
            ("Total Value of Today's Secondary Sales",
             str(today_transaction_value['total__sum'] if today_transaction_value[
                                                              'total__sum'] is not None else 0.0) + " TK"),
        )
        report.append(
            ('Total No. of Out-let Ordered (Daily)', str(today_total_outlet))
        )
        report.append(
            ('Total No. of Out-let Ordered (Weekly)', str(weekly_total_outlet))
        )
        report.append(
            ('Total No. of Out-let Ordered (Monthly)', str(monthly_total_outlet))
        )
        return report


@decorate(route(route='new-out-let-opened', group='Secondary Sales', group_order=4, module=ModuleEnum.Reports,
                display_name="New & Active Out-lets"))
class NewOutletOpenedReport(Report):
    class Meta:
        proxy = True

    def build_report(self, request=None, **kwargs):
        retailers = Retailers.objects.exclude(code__in=['R-57523', 'R-05979', 'R-05990', 'R-05978'])
        today_begin = datetime.now().replace(hour=0, minute=0, second=0).timestamp() * 1000
        today_total_retailers = retailers.filter(date_created__gte=today_begin).count()

        today = datetime.now().replace(hour=0, minute=0, second=0)
        if today.weekday() == 6:
            week_begin = today - timedelta(days=1)
        elif today.weekday() < 6:
            week_begin = today - timedelta(days=(today.weekday() + 1))
        week_begin = week_begin.timestamp() * 1000
        weekly_total_retailers = retailers.filter(date_created__gte=week_begin).count()

        month_begin = datetime.now().replace(day=1, hour=0, minute=0, second=0).timestamp() * 1000
        monthly_total_retailers = retailers.filter(date_created__gte=month_begin).count()

        quarter_begin = (datetime.now().replace(hour=0, minute=0, second=0) - timedelta(days=90)).timestamp() * 1000
        sec_completed_orders = SecondaryOrder.objects.exclude(
            from_client__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100'])
        total_active_outlet = sec_completed_orders.filter(date_created__gte=quarter_begin).distinct('to_client').count()

        report = list()

        report.append(
            ('Total New Outlet opened (Today)', str(today_total_retailers))
        )
        report.append(
            ('Total New Outlet opened (This Week)', str(weekly_total_retailers))
        )
        report.append(
            ('Total New Outlet opened (This Month)', str(monthly_total_retailers))
        )
        report.append(
            ('Total Active Outlets (Ordered once in three months)', str(total_active_outlet))
        )
        return report
