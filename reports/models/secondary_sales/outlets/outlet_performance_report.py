from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from blackwidow.sales.models.orders.order_history import OrderHistoryStatusEnum
from blackwidow.sales.models.orders.order_history_breakdown import OrderHistoryBreakdown
from blackwidow.sales.models.orders.secondary_delivered_order import SecondaryDeliveredOrder
from blackwidow.sales.models.orders.secondary_pending_order import SecondaryPendingOrder
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from foodex.models.clients.retailers import Retailers
from reports.models import Report
from django.db.models.aggregates import Sum

__author__ = 'Tareq'


@decorate(
    route(route='outlet-performance-report', group='Secondary Sales', group_order=4, module=ModuleEnum.Reports,
          display_name="Outlet Order vs Delivery Report"))
class OutletPerformanceReport(Report):
    class Meta:
        proxy = True

    def build_report(self, request=None, date_from=0, date_to=0, region=None, area=None, distributor=None, route=None,
                     styled=False, **kwargs):

        domain = OrderHistoryBreakdown.objects.filter(date_created__gte=date_from,
                                                      orderhistory__action__isnull=False,
                                                      date_created__lte=date_to,
                                                      orderhistory__object_type__in=[SecondaryPendingOrder.__name__,
                                                                                     SecondaryDeliveredOrder.__name__])
        if region is None:
            pass
        elif area is None:
            domain = domain.filter(
                orderhistory__to_client__clientassignment__infrastructureunit__parent_client__assigned_to__parent_id=region)  # created by users are of the assigned region
        elif distributor is None:
            domain = domain.filter(
                orderhistory__to_client__clientassignment__infrastructureunit__parent_client__assigned_to_id=area)  # Created by users are of assigned area
        elif route is None:
            domain = domain.filter(
                orderhistory__to_client__clientassignment__infrastructureunit__parent_client_id=distributor)
        else:
            domain = domain.filter(
                orderhistory__to_client__clientassignment__infrastructureunit__pk=route)  # Created by users are of assigned distributor

        secondary_record = domain.values(
            'orderhistory__to_client__pk',
            'orderhistory__to_client__code',
            'orderhistory__to_client__name',
            'orderhistory__object_type', 'orderhistory__action').annotate(
            Sum('total'))
        report_dict = dict()
        for record in secondary_record:
            if not record['orderhistory__to_client__pk'] in report_dict:
                report_dict[record['orderhistory__to_client__pk']] = dict()
                report_dict[record['orderhistory__to_client__pk']]['sp'] = mark_safe(
                    "<a class='inline-link' target='_blank' href='" + reverse(
                        Retailers.get_route_name(ViewActionEnum.Details),
                        kwargs={'pk': record['orderhistory__to_client__pk']}) + "' >" + record[
                        'orderhistory__to_client__code'] + ': ' +
                    record['orderhistory__to_client__name'] + "</a>") if record[
                    'orderhistory__to_client__code'] else 'N/A'

            if record['orderhistory__object_type'] == SecondaryPendingOrder.__name__ and record[
                'orderhistory__action'] == OrderHistoryStatusEnum.Created.value:
                report_dict[record['orderhistory__to_client__pk']]['ordered'] = record['total__sum']
            elif record['orderhistory__object_type'] == SecondaryDeliveredOrder.__name__ and record[
                'orderhistory__action'] == OrderHistoryStatusEnum.Approved.value:  ## CHANGE TO DELIVERED
                report_dict[record['orderhistory__to_client__pk']]['delivered'] = record['total__sum']

        report = []
        if styled:
            report.append(('Retailer', 'Secondary Sale Value (Ordered)', 'Secondary Sale Value (Delivered)'))
        else:
            report.append(['Retailer', 'Secondary Sale Value (Ordered)', 'Secondary Sale Value (Delivered)'])

        for record in report_dict.items():
            record = record[1]
            sp = record['sp']
            ordered = record['ordered'] if 'ordered' in record else 0.0
            delivered = record['delivered'] if 'delivered' in record else 0.0

            if styled:
                _item = (sp, str(ordered), str(delivered))
            else:
                _item = [sp, str(ordered), str(delivered)]
            report.append(_item)

        if not styled:
            report = sorted(report)
        return report
