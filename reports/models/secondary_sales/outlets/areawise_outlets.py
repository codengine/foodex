from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from foodex.models import Retailers, Area
from reports.models import Report
from blackwidow.engine.extensions.model_descriptor import get_model_by_name
from django.db.models import Count

__author__ = 'Ziaul Haque'


# @decorate(route(route='areawise-outlets', group='Secondary Sales', group_order=4, module=ModuleEnum.Reports,
#                 display_name="AreaWise Out-lets"))
class AreawiseOutletReport(Report):
    class Meta:
        proxy = True

    def build_report(self, request=None, styled=False, **kwargs):
        retailers = Retailers.objects.filter(parent__assigned_to__type=Area.__name__).exclude(
            parent__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100'])
        area_wise_retailers = retailers.values('parent__assigned_to').annotate(
            total_retailer=Count('parent__assigned_to'))

        report = []
        if styled:
            report.append(('Area', 'No of Outlets'))
        else:
            report.append(['Area', 'No of Outlets'])

        for outlet in area_wise_retailers:
            areas = Area.objects.filter(pk=outlet['parent__assigned_to'])
            if areas.exists():
                area_obj = areas.first()
                model = get_model_by_name(area_obj.type)
                area = mark_safe("<a class='inline-link' target='_blank' href='" + reverse(
                    model.get_route_name(ViewActionEnum.Details),
                    kwargs={'pk': area_obj.pk}) + "' >" + area_obj.__str__() + "</a>")
            else:
                area = 'No Reference'

            if styled:
                _item = (area, str(outlet['total_retailer']))
            else:
                _item = [area, str(outlet['total_retailer'])]
            report.append(_item)

        if not styled:
            report = sorted(report)
        return report