from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from config.enums.modules_enum import ModuleEnum
from reports.models.base.base import OlapReport, Report

__author__ = 'Machine II'

@decorate(route(route='secondary-sales-within-area', group='Secondary Sales', group_order=4, module=ModuleEnum.Reports, display_name="Within Areas"))
class SecondarySalesReportWithinArea(OlapReport):
    class Meta:
        proxy = True
