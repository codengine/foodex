from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from blackwidow.sales.models import SecondaryCompletedOrder
from blackwidow.sales.models.orders.order_breakdown import OrderBreakdown
from blackwidow.sales.models.orders.order_history import OrderHistory, OrderHistoryStatusEnum
from blackwidow.sales.models.orders.order_history_breakdown import OrderHistoryBreakdown
from blackwidow.sales.models.orders.secondary_delivered_order import SecondaryDeliveredOrder
from blackwidow.sales.models.orders.secondary_order import SecondaryOrder
from config.enums.modules_enum import ModuleEnum
from reports.models import Report
from datetime import datetime, timedelta
from django.db.models.aggregates import Sum

__author__ = 'Ziaul Haque, Tareq'


@decorate(route(route='sp-order-average-value', group='Secondary Sales', group_order=4, module=ModuleEnum.Reports,
                display_name="Sales Person Order Average Value"))
class SPOrderAverageValueReport(Report):
    class Meta:
        proxy = True

    def build_report(self, request=None, **kwargs):
        sec_completed_orders = OrderHistory.objects.filter(
            action=OrderHistoryStatusEnum.Created.value).exclude(
            from_client__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100'])
        today_begin = datetime.now().replace(hour=0, minute=0, second=0).timestamp() * 1000
        today_total_sp = sec_completed_orders.filter(date_created__gte=today_begin).distinct('created_by').count()

        total_order_value = OrderHistoryBreakdown.objects.filter(
            orderhistory__action=OrderHistoryStatusEnum.Created.value,
            orderhistory__date_created__gte=today_begin).exclude(
            orderhistory__from_client__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100']).aggregate(Sum('total'))[
            'total__sum']
        if total_order_value is not None:
            today_avg_order_value = float("{0:.2f}".format(total_order_value / today_total_sp))
        else:
            today_avg_order_value = 0.0

        today = datetime.now().replace(hour=0, minute=0, second=0)
        if today.weekday() == 6:
            week_begin = today - timedelta(days=1)
        elif today.weekday() < 6:
            week_begin = today - timedelta(days=(today.weekday() + 1))
        week_begin = week_begin.timestamp() * 1000
        weekly_total_sp = sec_completed_orders.filter(date_created__gte=week_begin).distinct('created_by').count()
        total_order_value = OrderHistoryBreakdown.objects.filter(
            orderhistory__action=OrderHistoryStatusEnum.Created.value,
            orderhistory__date_created__gte=week_begin).exclude(
            orderhistory__from_client__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100']).aggregate(Sum('total'))[
            'total__sum']
        if total_order_value is not None:
            weekly_avg_order_value = float("{0:.2f}".format(total_order_value / weekly_total_sp))
        else:
            weekly_avg_order_value = 0.0

        month_begin = datetime.now().replace(day=1, hour=0, minute=0, second=0).timestamp() * 1000
        monthly_total_sp = sec_completed_orders.filter(date_created__gte=month_begin).distinct('created_by').count()
        total_order_value = OrderHistoryBreakdown.objects.filter(
            orderhistory__action=OrderHistoryStatusEnum.Created.value,
            orderhistory__date_created__gte=month_begin).exclude(
            orderhistory__from_client__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100']).aggregate(Sum('total'))[
            'total__sum']
        if total_order_value is not None:
            monthly_avg_order_value = float("{0:.2f}".format(total_order_value / monthly_total_sp))
        else:
            monthly_avg_order_value = 0.0

        year_begin = datetime.now().replace(month=1, day=1, hour=0, minute=0, second=0).timestamp() * 1000
        yearly_total_sp = sec_completed_orders.filter(date_created__gte=year_begin).distinct('created_by').count()
        total_order_value = OrderHistoryBreakdown.objects.filter(
            orderhistory__action=OrderHistoryStatusEnum.Created.value,
            orderhistory__date_created__gte=year_begin).exclude(
            orderhistory__from_client__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100']).aggregate(Sum('total'))[
            'total__sum']
        if total_order_value is not None:
            yearly_avg_order_value = float("{0:.2f}".format(total_order_value / yearly_total_sp))
        else:
            yearly_avg_order_value = 0.0

        report = list()
        report.append(
            ("Average value of a sales persons order(Today)", str(today_avg_order_value) + " TK")
        )
        report.append(
            ('Average value of a sales persons order(This Week)', str(weekly_avg_order_value) + " TK")
        )
        report.append(
            ('Average value of a sales persons order(This Month)', str(monthly_avg_order_value) + " TK")
        )
        report.append(
            ('Average value of a sales persons order(This Year)', str(yearly_avg_order_value) + " TK")
        )
        return report
