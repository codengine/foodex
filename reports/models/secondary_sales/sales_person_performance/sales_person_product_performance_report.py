from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from blackwidow.sales.models import SecondaryTransaction
from blackwidow.sales.models.orders.order_history import OrderHistoryStatusEnum
from blackwidow.sales.models.orders.order_history_breakdown import OrderHistoryBreakdown
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from foodex.models import ServicePerson
from reports.models import Report
from django.db.models.aggregates import Sum
from blackwidow.engine.extensions.model_descriptor import get_model_by_name

__author__ = 'Tareq'


@decorate(
    route(route='sales-person-product-report', group='Secondary Sales', group_order=4, module=ModuleEnum.Reports,
          display_name="Sales Person Product-wise Performance"))
class SalesPersonProductPerformanceReport(Report):
    class Meta:
        proxy = True

    def build_report(self, request=None, date_from=0, date_to=0, region=None, area=None, distributor=None,
                     category=None, product=None, styled=False,
                     **kwargs):

        domain = OrderHistoryBreakdown.objects.filter(date_created__gte=date_from,
                                                      orderhistory__action__in=[OrderHistoryStatusEnum.Created.value,
                                                                                OrderHistoryStatusEnum.Approved.value],
                                                      date_created__lte=date_to,
                                                      orderhistory__object_type__in=['SecondaryPendingOrder',
                                                                                     'SecondaryDeliveredOrder'])
        if region is None:
            pass
        elif area is None:
            domain = domain.filter(
                orderhistory__created_by__infrastructureuserassignment__client__assigned_to__parent_id=region)  # created by users are of the assigned region
        elif distributor is None:
            domain = domain.filter(
                orderhistory__created_by__infrastructureuserassignment__client__assigned_to_id=area)  # Created by users are of assigned area
        else:
            domain = domain.filter(
                orderhistory__created_by__infrastructureuserassignment__client__pk=distributor)  # Created by users are of assigned distributor

        if category is None:
            pass
        elif product is None:
            domain = domain.filter(product__categories__pk=category)
        else:
            domain = domain.filter(product_id=product)

        secondary_record = domain.values(
            'orderhistory__created_by__pk',
            'orderhistory__created_by__code',
            'orderhistory__created_by__name',
            'orderhistory__action').annotate(
            Sum('total'), Sum('total_items'))
        report_dict = dict()
        for record in secondary_record:
            if not record['orderhistory__created_by__pk'] in report_dict:
                report_dict[record['orderhistory__created_by__pk']] = dict()
                report_dict[record['orderhistory__created_by__pk']]['sp'] = mark_safe(
                    "<a class='inline-link' target='_blank' href='" + reverse(
                        ServicePerson.get_route_name(ViewActionEnum.Details),
                        kwargs={'pk': record['orderhistory__created_by__pk']}) + "' >" + record[
                        'orderhistory__created_by__code'] + ': ' +
                    record['orderhistory__created_by__name'] + "</a>") if record[
                    'orderhistory__created_by__code'] else 'N/A'

            if record['orderhistory__action'] == OrderHistoryStatusEnum.Created.value:
                report_dict[record['orderhistory__created_by__pk']]['q_ordered'] = record['total_items__sum']
                report_dict[record['orderhistory__created_by__pk']]['ordered'] = record['total__sum']
            elif record['orderhistory__action'] == OrderHistoryStatusEnum.Approved.value:  ## CHANGE TO DELIVERED
                report_dict[record['orderhistory__created_by__pk']]['q_delivered'] = record['total_items__sum']
                report_dict[record['orderhistory__created_by__pk']]['delivered'] = record['total__sum']

        report = []
        if styled:
            report.append(('Service Person', 'Secondary Sale (Ordered in Pcs)', 'Secondary Sale Value (Ordered in BDT)',
                           'Secondary Sale (Delivered in Pcs)', 'Secondary Sale Value (Delivered in BDT)'))
        else:
            report.append(['Service Person', 'Secondary Sale (Ordered in Pcs)', 'Secondary Sale Value (Ordered in BDT)',
                           'Secondary Sale (Delivered in Pcs)', 'Secondary Sale Value (Delivered in BDT)'])

        for record in report_dict.items():
            record = record[1]
            sp = record['sp']
            q_ordered = record['q_ordered'] if 'q_ordered' in record else 0
            ordered = record['ordered'] if 'ordered' in record else 0.0
            q_delivered = record['q_delivered'] if 'q_delivered' in record else 0
            delivered = record['delivered'] if 'delivered' in record else 0.0

            if styled:
                _item = (sp, str(q_ordered), str(ordered), str(q_delivered), str(delivered))
            else:
                _item = [sp, str(q_ordered), str(ordered), str(q_delivered), str(delivered)]
            report.append(_item)

        if not styled:
            report = sorted(report)
        return report
