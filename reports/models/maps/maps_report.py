from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from config.enums.modules_enum import ModuleEnum
from reports.models.base.base import OlapReport, Report

__author__ = 'Machine II'

@decorate(route(route='map-secondary-sale', group='Maps', group_order=2, module=ModuleEnum.Reports, display_name="Secondary Sales"))
class MapSecondarySaleReport(Report):
    class Meta:
        proxy = True

@decorate(route(route='map-retailer-location', group='Maps', group_order=2, module=ModuleEnum.Reports, display_name="Retailer locations"))
class MapRetailerLocationReport(Report):
    class Meta:
        proxy = True