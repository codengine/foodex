from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from config.enums.modules_enum import ModuleEnum
from reports.models.base.base import Report

__author__ = 'Machine II'

@decorate(route(route='reports-summary', group='Dashboard', group_order=1, module=ModuleEnum.Reports, display_name="Summary"))
class DashboardSummaryReport(Report):
    class Meta:
        proxy = True

@decorate(route(route='reports-detail', group='Dashboard', group_order=1, module=ModuleEnum.Reports, display_name="Detail"))
class DashboardDetailReport(Report):
    class Meta:
        proxy = True
