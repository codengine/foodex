from datetime import datetime
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from config.enums.modules_enum import ModuleEnum
from foodex.models.clients.distributor import Distributor
from foodex.models.transactions.completed_sales_transaction import CompletedSalesTransaction
from foodex.models.transactions.delivery_transaction import DeliveryTransaction
from foodex.models.transactions.delivery_transaction_braakdown import DeliveryTransactionBreakDown
from reports.models.base.base import Report

__author__ = 'Machine II'

@decorate(route(route='inventory-report', group='Inventory', group_order=6, module=ModuleEnum.Reports, display_name="Warehouse Inventories"))
class InventoryReport(Report):
    class Meta:
        proxy = True

@decorate(route(route='client-inventory-report', group='Inventory', group_order=6, module=ModuleEnum.Reports, display_name="Client Inventories"))
class ClientInventoryReport(Report):
    class Meta:
        proxy = True

@decorate(route(route='in-transit-report', group='Inventory', group_order=6, module=ModuleEnum.Reports, display_name="In Transit Report"))
class InTransitReport(Report):
    class Meta:
        proxy = True

    def build_report(self, request=None, region=None, area=None, distributor=None, time_from=None, time_to=None, styled=False, **kwargs):

        if distributor is not None:
            clients = Distributor.objects.filter(pk=distributor)
        elif area is not None:
            clients = Distributor.objects.filter(assigned_to=area)
        elif region is not None:
            clients = Distributor.objects.filter(assigned_to__parent=region)
        else:
            clients = Distributor.get_role_based_queryset()

        client_ids = [x.pk for x in clients]

        transaction_breakdowns = DeliveryTransactionBreakDown.objects.filter(transaction__client_id__in=client_ids,
                            transaction__transaction_time__gte=time_from, transaction__transaction_time__lte=time_to)\
            .values('product__code', 'product__name', 'quantity', 'stock_received', 'transaction__transaction_time',
                    'transaction__invoice')

        report = []
        if styled:
            report.append(('Sale', 'Product Code', 'Product Name', 'Sold Quantity', 'Received Quantity', 'Date'))
        else:
            report.append(['Sale', 'Product Code', 'Product Name', 'Sold Quantity', 'Received Quantity', 'Date'])

        for breakdown in transaction_breakdowns:
            completed_sales_transaction = CompletedSalesTransaction.objects.filter(invoice=breakdown['transaction__invoice'])
            if completed_sales_transaction.exists():
                completed_sales_transaction = completed_sales_transaction.first()
                completed_sales_transaction_code = completed_sales_transaction.code
            else:
                completed_sales_transaction = 'No reference'
                completed_sales_transaction_code = 'XXX'
            t_time = datetime.fromtimestamp(int(breakdown['transaction__transaction_time']/1000))
            if styled:
                _item = (completed_sales_transaction.__str__(), breakdown['product__code'], breakdown['product__name'],
                         str(breakdown['quantity']), str(breakdown['quantity'] - breakdown['stock_received']),
                         t_time.strftime('%Y-%m-%d %H:%M:%S'))
            else:
                _item = [completed_sales_transaction_code, breakdown['product__code'], breakdown['product__name'],
                         str(breakdown['quantity']), str(breakdown['quantity'] - breakdown['stock_received']),
                         t_time.strftime('%Y-%m-%d %H:%M:%S')]
            report.append(_item)

        if not styled:
            report = sorted(report)
        return report