from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from config.enums.modules_enum import ModuleEnum
from reports.models import Report
from datetime import datetime
from django.db.models.aggregates import Sum
from foodex.models.transactions.completed_sales_transaction import CompletedSalesTransaction

__author__ = 'Ziaul Haque'


@decorate(route(route='delivered-product-report', group='Inventory', group_order=6, module=ModuleEnum.Reports,
                display_name="Delivered Product Count"))
class DeliveredProductReport(Report):
    class Meta:
        proxy = True

    def build_report(self, request=None, time_from=None, time_to=None, styled=False, **kwargs):
        today_begin = datetime.now().replace(hour=0, minute=0, second=0).timestamp() * 1000
        completed_sales_transactions = CompletedSalesTransaction.objects.exclude(
            client__code__in=['DIST-00030', 'DIST-00099', 'DIST-00100'])
        today_delivered_products = completed_sales_transactions.filter(date_created__gte=time_from,
                                                                       date_created__lte=time_to).values(
            'breakdown__product__code', 'breakdown__product__name').annotate(Sum('breakdown__quantity'))

        report = []
        if styled:
            report.append(('Product Code', 'Product Name', 'Quantity'))
        else:
            report.append(['Product Code', 'Product Name', 'Quantity'])

        for product in today_delivered_products:
            product_code = product['breakdown__product__code']
            product_name = product['breakdown__product__name']
            quantity = product['breakdown__quantity__sum']

            if styled:
                _item = (product_code, product_name, str(quantity))
            else:
                _item = [product_code, product_name, str(quantity)]
            report.append(_item)

        if not styled:
            report = sorted(report)
        return report