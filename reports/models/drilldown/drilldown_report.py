from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from config.enums.modules_enum import ModuleEnum
from reports.models.base.base import Report

__author__ = 'Machine II'

@decorate(route(route='drill-down', group='Drill-Down', group_order=100, module=ModuleEnum.Reports, display_name="Drill-down Report"))
class DrillDownReport(Report):
    class Meta:
        proxy = True
