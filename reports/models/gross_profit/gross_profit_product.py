from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate
from config.enums.modules_enum import ModuleEnum
from reports.models.base.base import OlapReport

__author__ = 'Machine II'

# @decorate(route(route='gross-profit-product', group='Gross Profit', group_order=5, module=ModuleEnum.Reports, display_name="Products"))
class GrossProfitReportByProduct(OlapReport):
    class Meta:
        proxy = True
