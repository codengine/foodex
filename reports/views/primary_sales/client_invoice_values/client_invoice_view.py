from django.utils.datastructures import SortedDict
from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from reports.models.primary_sales.client_invoice_values.client_invoice_report import ClientInvoiceReport
from blackwidow.core.generics.views.list_view import GenericListView
from django.forms.forms import Form
from django import forms
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Ziaul Haque'


@decorate(override_view(model=ClientInvoiceReport, view=ViewActionEnum.Manage))
class ClientInvoiceReportView(GenericListView):
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_template_names(self):
        return ['reports/client-invoice-value.html']

    def get_wrapped_parameters(self, parameters):
        class DynamicForm(Form):
            pass

        form = DynamicForm()
        for p in parameters:
            form.fields[p['name']] = p['field']
        return form

    def get_report_parameters(self, **kwargs):
        parameters = SortedDict()
        parameters['G1'] = self.get_wrapped_parameters(({
                                                            'name': 'Daily',
                                                            'field': forms.CharField(label='Today')
                                                        },{
                                                            'name': 'Weekly',
                                                            'field': forms.CharField(label='Last Week')
                                                        },{
                                                            'name': 'Monthly',
                                                            'field': forms.CharField(label='Last Month')
                                                        },{
                                                            'name': 'Yearly',
                                                            'field': forms.CharField(label='Last Year')
                                                        },))
        return parameters

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Clientwise Total Invoice Value"
        context['enable_map'] = False
        context['table_ordering_column'] = 1
        context['parameters'] = self.get_report_parameters(**kwargs)
        return context

    def get_json_response(self, content, **kwargs):
        report_type = self.extract_parameter('report_type')
        data_dict = dict()
        report = ClientInvoiceReport().build_report(request=self.request, report_type=report_type, styled=True)

        data_dict['report'] = report
        return super().get_json_response(self.convert_context_to_json(data_dict), **kwargs)