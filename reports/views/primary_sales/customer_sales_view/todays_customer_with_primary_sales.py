from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from reports.models.primary_sales.customer_sales.todays_customer_with_primary_sales import \
    TodayPrimarySaleForCustomerReport
from blackwidow.core.generics.views.list_view import GenericListView
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Ziaul Haque'


@decorate(override_view(model=TodayPrimarySaleForCustomerReport, view=ViewActionEnum.Manage))
class TodayPrimarySaleForCustomerView(GenericListView):
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_template_names(self):
        return ['reports/total-invoiced-items.html']


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Today's Customer with Primary Sale Value"
        context['enable_map'] = False
        context['table_ordering_column'] = 0
        return context

    def get_json_response(self, content, **kwargs):
        data_dict = dict()
        report = TodayPrimarySaleForCustomerReport().build_report(request=self.request, styled=True)

        data_dict['report'] = report
        return super().get_json_response(self.convert_context_to_json(data_dict), **kwargs)