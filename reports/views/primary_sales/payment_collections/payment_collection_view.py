from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from reports.models.primary_sales.payment_collections.payment_collection_report import TotalPaymentCollectionReport
from blackwidow.core.generics.views.list_view import GenericListView
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Ziaul Haque'


@decorate(override_view(model=TotalPaymentCollectionReport, view=ViewActionEnum.Manage))
class TotalPaymentCollectionView(GenericListView):
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_template_names(self):
        return ['reports/total-invoice-value.html']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Total Invoice Value Report"
        context['enable_map'] = False
        context['report'] = TotalPaymentCollectionReport().build_report(request=self.request)
        return context

    def get_json_response(self, content, **kwargs):
        data_dict = dict()
        report = TotalPaymentCollectionReport().build_report(request=self.request)
        data_dict['report'] = report
        return super().get_json_response(self.convert_context_to_json(data_dict), **kwargs)