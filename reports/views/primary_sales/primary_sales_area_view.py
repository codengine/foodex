from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from blackwidow.sales.models.transactions.transaction import Transaction, TransactionType
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.clients.distributor import Distributor
from foodex.models.infrastructure.area import Area
from foodex.models.infrastructure.region import Region
from reports.config.constants import QUERY_OPEN_URL, QUERY_OPEN_MANUAL_PARAM_URL, PARAM_JOIN, QUERY_OPEN
from reports.models.primary_sales.primary_sales_area import PrimarySalesReportByArea
from reports.utils.report_param_validator import validated_report_param
from reports.views.base.report_view import AreaBasedReportView

from django.db.models import Q
from django import forms

__author__ = 'Machine II'

@decorate(override_view(model=PrimarySalesReportByArea, view=ViewActionEnum.Manage))
class PrimarySalesByAreaView(AreaBasedReportView):

    def get_report_parameters(self, **kwargs):
        parameters = super().get_report_parameters()
        parameters['G1'] = self.get_wrapped_parameters(({
                                                            'name': 'week-btn',
                                                            'field': forms.CharField(label='Weekly')
                                                        },{
                                                            'name': 'monthly-btn',
                                                            'field': forms.CharField(label='Monthly')
                                                        },{
                                                            'name': 'yearly-btn',
                                                            'field': forms.CharField(label='Yearly')
                                                        },{
                                                            'name': 'daily-btn',
                                                            'field': forms.CharField(label='Daily')
                                                        },))
        return parameters

    def get_json_response(self, content, **kwargs):
        report_url = ''
        try:
            distributor_id = self.extract_parameter('distributor')
            distributor = Distributor.objects.get(pk=distributor_id)
            distributor_name = distributor.name
            query_list = list()
            query_list.append(Q(('client_id', distributor_id)))
            query_list.append(Q(('type', 'PrimaryTransaction')))
            query_list.append(Q(('transaction_type', TransactionType.Sale.value)))
            param_val = validated_report_param(model=Transaction, query=query_list, text=distributor_name)
            if param_val != '':
                report_url = QUERY_OPEN_MANUAL_PARAM_URL + PARAM_JOIN + 'paramdistributor=' + param_val + PARAM_JOIN \
                             + QUERY_OPEN + 'primary-sale-'+ self.extract_parameter('type').lower() +'.saiku'
        except:
            try:
                area_id = self.extract_parameter('area')
                area = Area.objects.get(pk=area_id)
                area_name = area.name
                query_list = list()
                query_list.append(Q(('client__assigned_to_id', area_id)))
                query_list.append(Q(('type', 'PrimaryTransaction')))
                query_list.append(Q(('transaction_type', TransactionType.Sale.value)))
                param_val = validated_report_param(model=Transaction, query=query_list, text=area_name)
                if param_val != '':
                    report_url = QUERY_OPEN_MANUAL_PARAM_URL + PARAM_JOIN + 'paramarea=' + param_val + PARAM_JOIN \
                                 + QUERY_OPEN + 'primary-sale-'+ self.extract_parameter('type').lower() +'.saiku'
            except:
                try:
                    region_id = self.extract_parameter('region')
                    region = Region.objects.get(pk=region_id)
                    region_name = region.name
                    query_list = list()
                    query_list.append(Q(('client__assigned_to__parent__pk', region_id)))
                    query_list.append(Q(('type', 'PrimaryTransaction')))
                    query_list.append(Q(('transaction_type', TransactionType.Sale.value)))
                    param_val = validated_report_param(model=Transaction, query=query_list, text=region_name)
                    if param_val != '':
                        report_url = QUERY_OPEN_MANUAL_PARAM_URL + PARAM_JOIN + 'paramdregion=' + param_val + PARAM_JOIN \
                                     + QUERY_OPEN + 'primary-sale-'+ self.extract_parameter('type').lower() +'.saiku'
                except:
                    report_url = QUERY_OPEN_URL + 'primary-sale-'+ self.extract_parameter('type').lower() +'.saiku'
        data_dict = dict()
        data_dict['href'] = report_url
        return super().get_json_response(self.convert_context_to_json(data_dict), **kwargs)
