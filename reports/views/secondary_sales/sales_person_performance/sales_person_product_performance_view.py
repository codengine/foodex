from django.core.urlresolvers import reverse
from django.db.models.aggregates import Max
from django.utils.datastructures import SortedDict
from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.products.product_group import ProductGroup
from blackwidow.sales.models.transactions.transaction import Transaction
from foodex.models.clients.distributor import Distributor
from foodex.models.infrastructure.area import Area
from foodex.models.infrastructure.region import Region
from reports.models.secondary_sales.sales_person_performance.sales_person_product_performance_report import \
    SalesPersonProductPerformanceReport
from blackwidow.core.generics.views.list_view import GenericListView
from django.forms.forms import Form
from django import forms
from config.enums.view_action_enum import ViewActionEnum

import datetime

__author__ = 'Tareq'


@decorate(override_view(model=SalesPersonProductPerformanceReport, view=ViewActionEnum.Manage))
class SalesPersonProductPerformanceView(GenericListView):
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_template_names(self):
        return ['reports/date-range-table-report.html']

    def get_wrapped_parameters(self, parameters):
        class DynamicForm(Form):
            pass

        form = DynamicForm()
        for p in parameters:
            form.fields[p['name']] = p['field']
        return form

    def get_report_parameters(self, **kwargs):
        parameters = SortedDict()
        parameters['G1'] = self.get_wrapped_parameters(({
                                                            'name': 'from_date',
                                                            'field': forms.DateTimeField(input_formats=['%d/%m/%Y'],
                                                                                         widget=forms.DateInput(attrs={
                                                                                             'data-format': "dd/MM/yyyy",
                                                                                             'class': 'date-time-picker'},
                                                                                             format='%d/%m/%Y'))
                                                        }, {
                                                            'name': 'to_date',
                                                            'field': forms.DateTimeField(input_formats=['%d/%m/%Y'],
                                                                                         widget=forms.DateInput(attrs={
                                                                                             'data-format': "dd/MM/yyyy",
                                                                                             'class': 'date-time-picker'},
                                                                                             format='%d/%m/%Y'))
                                                        },))
        parameters['G2'] = self.get_wrapped_parameters(({'name': 'region',
                                                         'field': forms.ModelChoiceField(
                                                             queryset=Region.get_role_based_queryset(),
                                                             empty_label="All", label='Select Region',
                                                             required=False, widget=forms.Select(
                                                                 attrs={'class': 'select2', 'width': '220',
                                                                        'data-child': 'area'}))
                                                         }, {'name': 'area',
                                                             'field': forms.CharField(label='Select Area',
                                                                                      required=False,
                                                                                      widget=forms.TextInput(
                                                                                          attrs={
                                                                                              'data-child': 'distributor',
                                                                                              'width': '220',
                                                                                              'class': 'select2-input',
                                                                                              'data-depends-on': 'region',
                                                                                              'data-depends-property': 'parent:id',
                                                                                              'data-url': reverse(
                                                                                                  Area.get_route_name(
                                                                                                      action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
                                                             }, {
                                                            'name': 'distributor',
                                                            'field': forms.CharField(label='Select Distributor',
                                                                                     required=False,
                                                                                     widget=forms.TextInput(attrs={
                                                                                         'class': 'select2-input',
                                                                                         'width': '220',
                                                                                         'data-depends-on': 'area',
                                                                                         'data-depends-property': 'assigned_to:id',
                                                                                         'data-url': reverse(
                                                                                             Distributor.get_route_name(
                                                                                                 action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
                                                        },))
        parameters['G3'] = self.get_wrapped_parameters(({'name': 'category',
                                                         'field': forms.ModelChoiceField(
                                                             queryset=ProductGroup.get_role_based_queryset(),
                                                             empty_label="All", label='Select Category',
                                                             required=False, widget=forms.Select(
                                                                 attrs={'class': 'select2', 'width': '220',
                                                                        'data-child': 'product'}))
                                                         }, {'name': 'product',
                                                             'field': forms.CharField(label='Select Product',
                                                                                      required=False,
                                                                                      widget=forms.TextInput(
                                                                                          attrs={
                                                                                              'width': '220',
                                                                                              'class': 'select2-input',
                                                                                              'data-depends-on': 'category',
                                                                                              'data-depends-property': 'categories:id',
                                                                                              'data-url': reverse(
                                                                                                  Product.get_route_name(
                                                                                                      action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
                                                             },))
        return parameters

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Sales Person Performance"
        context['enable_map'] = False
        context['table_ordering_column'] = 1
        context['parameters'] = self.get_report_parameters(**kwargs)
        return context

    def get_json_response(self, content, **kwargs):
        region = self.extract_parameter('region')
        area = self.extract_parameter('area')
        distributor = self.extract_parameter('distributor')
        category = self.extract_parameter('category')
        product = self.extract_parameter('product')

        from_date = self.extract_parameter('from_date')
        if from_date is not None:
            f_date = datetime.datetime.strptime(from_date, "%d/%m/%Y").timestamp() * 1000
        else:
            f_date = 0

        to_date = self.extract_parameter('to_date')
        if to_date is not None:
            t_date = (datetime.datetime.strptime(to_date, "%d/%m/%Y") + datetime.timedelta(1)).timestamp() * 1000
        else:
            t_date = Transaction.objects.aggregate(Max('transaction_time'))['transaction_time__max']
        data_dict = dict()
        report = SalesPersonProductPerformanceReport().build_report(request=self.request, region=region, area=area,
                                                                    distributor=distributor, date_from=f_date,
                                                                    date_to=t_date, category=category, product=product,
                                                                    styled=True)

        data_dict['report'] = report
        return super().get_json_response(self.convert_context_to_json(data_dict), **kwargs)
