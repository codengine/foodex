from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from reports.models.secondary_sales.sales_person_performance.sales_person_order_report import SPOrderAverageValueReport
from blackwidow.core.generics.views.list_view import GenericListView
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Ziaul Haque'


@decorate(override_view(model=SPOrderAverageValueReport, view=ViewActionEnum.Manage))
class SPOrderAverageValueView(GenericListView):
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_template_names(self):
        return ['reports/total-invoice-value.html']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Sales Person Order Average Value"
        context['enable_map'] = False
        context['report'] = SPOrderAverageValueReport().build_report(request=self.request)
        return context

    def get_json_response(self, content, **kwargs):
        data_dict = dict()
        report = SPOrderAverageValueReport().build_report(request=self.request)
        data_dict['report'] = report
        return super().get_json_response(self.convert_context_to_json(data_dict), **kwargs)