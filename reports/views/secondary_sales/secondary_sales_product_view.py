from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.products.product_group import ProductGroup
from blackwidow.sales.models.transactions.transaction import TransactionType
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from config.enums.view_action_enum import ViewActionEnum
from reports.config.constants import QUERY_OPEN_MANUAL_PARAM_URL, PARAM_JOIN, QUERY_OPEN, QUERY_OPEN_URL
from reports.models.secondary_sales.secondary_sales_product import SecondarySalesReportByProduct
from reports.utils.report_param_validator import validated_report_param
from reports.views.base.report_view import ProductBasedReportView
from django import forms
from django.db.models import Q

__author__ = 'Machine II'

@decorate(override_view(model=SecondarySalesReportByProduct, view=ViewActionEnum.Manage))
class SecondarySalesByProductView(ProductBasedReportView):
    def get_json_response(self, content, **kwargs):
        return super().get_json_response(content, **kwargs)

    def get_report_parameters(self, **kwargs):
        parameters = super().get_report_parameters()
        parameters['G1'] = self.get_wrapped_parameters(({
                                                            'name': 'week-btn',
                                                            'field': forms.CharField(label='Weekly')#, widget=forms.Select(attrs={'href': 'http://localhost:8080'}))#'http://localhost:8080/?plugin=true&username=admin&password=admin&mode=embed#query/open//homes/home:admin/primary-sale-week.saiku')
                                                        },{
                                                            'name': 'monthly-btn',
                                                            'field': forms.CharField(label='Monthly')#, widget=forms.Select(attrs={'href': 'http://localhost:8080'}))#/?plugin=true&username=admin&password=admin&mode=embed#query/open//homes/home:admin/primary-sale-month.saiku')
                                                        },{
                                                            'name': 'yearly-btn',
                                                            'field': forms.CharField(label='Yearly')#, widget=forms.Select(attrs={'href': 'http://localhost:8080'}))#/?plugin=true&username=admin&password=admin&mode=embed#query/open//homes/home:admin/primary-sale-year.saiku')
                                                        },{
                                                            'name': 'daily-btn',
                                                            'field': forms.CharField(label='Daily')#, widget=forms.Select(attrs={'href': 'http://localhost:8080'}))#/?plugin=true&username=admin&password=admin&mode=embed#query/open//homes/home:admin/primary-sale-day.saiku')
                                                        },))
        return parameters

    def get_json_response(self, content, **kwargs):
        try:
            product_id = self.extract_parameter('product')
            product = Product.objects.get(pk=product_id)
            product_name = product.name

            query_list = list()
            query_list.append(Q(('product_id', product_id)))
            query_list.append(Q(('transaction__type', 'PrimaryTransaction')))
            query_list.append(Q(('transaction__transaction_type', TransactionType.Sale.value)))
            param_val = validated_report_param(model=TransactionBreakDown, query=query_list, text=product_name)

            if param_val != '':
                report_url = QUERY_OPEN_MANUAL_PARAM_URL + PARAM_JOIN + 'paramproduct=' + param_val + PARAM_JOIN \
                             + QUERY_OPEN + 'secondary-sale-product-'+ self.extract_parameter('type').lower() +'.saiku'
        except:
            try:
                category_id = self.extract_parameter('category')
                category = ProductGroup.objects.get(pk=category_id)
                category_name = category.name

                query_list = list()
                query_list.append(Q(('product__categories__id', category_id)))
                query_list.append(Q(('transaction__type', 'PrimaryTransaction')))
                query_list.append(Q(('transaction__transaction_type', TransactionType.Sale.value)))
                param_val = validated_report_param(model=TransactionBreakDown, query=query_list, text=category_name)
                if param_val != '':
                    report_url = QUERY_OPEN_MANUAL_PARAM_URL + PARAM_JOIN + 'paramcategory=' + param_val + PARAM_JOIN \
                                 + QUERY_OPEN + 'secondary-sale-product-'+ self.extract_parameter('type').lower() +'.saiku'
            except:
                report_url = QUERY_OPEN_URL + 'secondary-sale-product-'+ self.extract_parameter('type').lower() +'.saiku'
        data_dict = dict()
        data_dict['href'] = report_url
        return super().get_json_response(self.convert_context_to_json(data_dict), **kwargs)
