from django.forms.forms import Form
from django import forms
from django.utils.datastructures import SortedDict

from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from reports.models.secondary_sales.outlets.areawise_outlets import AreawiseOutletReport
from blackwidow.core.generics.views.list_view import GenericListView
from config.enums.view_action_enum import ViewActionEnum
from reports.models.secondary_sales.outlets.outlet_numbers import OutletNumberReport

__author__ = 'Tareq'


@decorate(override_view(model=OutletNumberReport, view=ViewActionEnum.Manage))
class OutletNumberReportView(GenericListView):
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_template_names(self):
        return ['reports/total-invoiced-items-control.html']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Outlet Numbers"
        context['table_ordering_column'] = 1
        context['enable_map'] = False
        context['parameters'] = self.get_report_parameters(**kwargs)
        return context

    def get_wrapped_parameters(self, parameters):
        class DynamicForm(Form):
            pass

        form = DynamicForm()
        for p in parameters:
            form.fields[p['name']] = p['field']
        return form

    def get_report_parameters(self, **kwargs):
        parameters = SortedDict()
        parameters['G1'] = self.get_wrapped_parameters(({
                                                            'name': 'Area',
                                                            'field': forms.CharField(label='Area')
                                                        }, {
                                                            'name': 'Distributor',
                                                            'field': forms.CharField(label='Distributor')
                                                        }, {
                                                            'name': 'Route',
                                                            'field': forms.CharField(label='Route')
                                                        },))
        return parameters

    def get_json_response(self, content, **kwargs):
        report_type = self.extract_parameter('report_type')
        data_dict = dict()
        report = OutletNumberReport().build_report(request=self.request, report_type=report_type, styled=True)

        data_dict['report'] = report
        return super().get_json_response(self.convert_context_to_json(data_dict), **kwargs)
