from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from reports.models.secondary_sales.outlets.outlet_ordered import OutletOrderedReport, NewOutletOpenedReport
from blackwidow.core.generics.views.list_view import GenericListView
from config.enums.view_action_enum import ViewActionEnum

__author__ = 'Ziaul Haque'


@decorate(override_view(model=OutletOrderedReport, view=ViewActionEnum.Manage))
class OutletOrderedView(GenericListView):
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_template_names(self):
        return ['reports/total-invoice-value.html']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Ordered Out-lets"
        context['enable_map'] = False
        context['report'] = OutletOrderedReport().build_report(request=self.request)
        return context

    def get_json_response(self, content, **kwargs):
        data_dict = dict()
        report = OutletOrderedReport().build_report(request=self.request)
        data_dict['report'] = report
        return super().get_json_response(self.convert_context_to_json(data_dict), **kwargs)



@decorate(override_view(model=NewOutletOpenedReport, view=ViewActionEnum.Manage))
class NewOutletOpenedView(GenericListView):
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_template_names(self):
        return ['reports/total-invoice-value.html']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "New & Active Out-lets"
        context['enable_map'] = False
        context['report'] = NewOutletOpenedReport().build_report(request=self.request)
        return context

    def get_json_response(self, content, **kwargs):
        data_dict = dict()
        report = NewOutletOpenedReport().build_report(request=self.request)
        data_dict['report'] = report
        return super().get_json_response(self.convert_context_to_json(data_dict), **kwargs)