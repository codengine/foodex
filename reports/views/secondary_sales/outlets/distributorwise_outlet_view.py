from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from blackwidow.core.generics.views.list_view import GenericListView
from config.enums.view_action_enum import ViewActionEnum
from reports.models.secondary_sales.outlets.distributor_wise_outlets import DistWiseOutletReport

__author__ = 'Ziaul Haque'


@decorate(override_view(model=DistWiseOutletReport, view=ViewActionEnum.Manage))
class DistWiseOutletView(GenericListView):
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_template_names(self):
        return ['reports/total-invoiced-items.html']


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "DistributorWise Out-lets"
        context['table_ordering_column'] = 1
        context['enable_map'] = False
        return context

    def get_json_response(self, content, **kwargs):
        data_dict = dict()
        report = DistWiseOutletReport().build_report(request=self.request, styled=True)

        data_dict['report'] = report
        return super().get_json_response(self.convert_context_to_json(data_dict), **kwargs)