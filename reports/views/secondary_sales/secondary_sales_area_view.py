from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from config.enums.view_action_enum import ViewActionEnum
from blackwidow.sales.models.transactions.transaction import Transaction, TransactionType
from foodex.models.clients.distributor import Distributor
from foodex.models.infrastructure.region import Region
from reports.config.constants import QUERY_OPEN_URL, QUERY_OPEN_MANUAL_PARAM_URL, PARAM_JOIN, QUERY_OPEN
from reports.models.secondary_sales.secondary_sales_area import SecondarySalesReportByArea
from reports.views.base.report_view import AreaBasedReportView
from reports.utils.report_param_validator import validated_report_param
from foodex.models.infrastructure.area import Area
from django import forms
from django.db.models import Q

__author__ = 'Machine II'

@decorate(override_view(model=SecondarySalesReportByArea, view=ViewActionEnum.Manage))
class SecondarySalesByAreaView(AreaBasedReportView):
    def get_json_response(self, content, **kwargs):
        return super().get_json_response(content, **kwargs)

    def get_report_parameters(self, **kwargs):
        parameters = super().get_report_parameters()
        parameters['G1'] = self.get_wrapped_parameters(({
                                                            'name': 'week-btn',
                                                            'field': forms.CharField(label='Weekly')#, widget=forms.Select(attrs={'href': 'http://localhost:8080'}))#'http://localhost:8080/?plugin=true&username=admin&password=admin&mode=embed#query/open//homes/home:admin/primary-sale-week.saiku')
                                                        },{
                                                            'name': 'monthly-btn',
                                                            'field': forms.CharField(label='Monthly')#, widget=forms.Select(attrs={'href': 'http://localhost:8080'}))#/?plugin=true&username=admin&password=admin&mode=embed#query/open//homes/home:admin/primary-sale-month.saiku')
                                                        },{
                                                            'name': 'yearly-btn',
                                                            'field': forms.CharField(label='Yearly')#, widget=forms.Select(attrs={'href': 'http://localhost:8080'}))#/?plugin=true&username=admin&password=admin&mode=embed#query/open//homes/home:admin/primary-sale-year.saiku')
                                                        },{
                                                            'name': 'daily-btn',
                                                            'field': forms.CharField(label='Daily')#, widget=forms.Select(attrs={'href': 'http://localhost:8080'}))#/?plugin=true&username=admin&password=admin&mode=embed#query/open//homes/home:admin/primary-sale-day.saiku')
                                                        },))
        return parameters

    def get_json_response(self, content, **kwargs):
        report_url = ''
        try:
            distributor_id = self.extract_parameter('distributor')
            distributor = Distributor.objects.get(pk=distributor_id)
            distributor_name = distributor.name
            query_list = list()
            query_list.append(Q(('client_id', distributor_id)))
            query_list.append(Q(('type', 'SecondaryTransaction')))
            query_list.append(Q(('transaction_type', TransactionType.Sale.value)))
            param = validated_report_param(model=Transaction, query=query_list, text=distributor_name)

            if param != '':
                report_url = QUERY_OPEN_MANUAL_PARAM_URL + PARAM_JOIN + 'paramdistributor=' + param + PARAM_JOIN \
                             + QUERY_OPEN + 'secondary-sale-'+ self.extract_parameter('type').lower() +'.saiku'
        except:
            try:
                area_id = self.extract_parameter('area')
                area = Area.objects.get(pk=area_id)
                area_name = area.name
                query_list = list()
                query_list.append(Q(('client__assigned_to_id', area_id)))
                query_list.append(Q(('type', 'SecondaryTransaction')))
                query_list.append(Q(('transaction_type', TransactionType.Sale.value)))
                param = validated_report_param(model=Transaction, query=query_list, text=area_name)
                if param != '':
                    report_url = QUERY_OPEN_MANUAL_PARAM_URL + PARAM_JOIN + 'paramarea=' + param + PARAM_JOIN \
                                 + QUERY_OPEN + 'secondary-sale-'+ self.extract_parameter('type').lower() +'.saiku'
            except:
                try:
                    region_id = self.extract_parameter('region')
                    region = Region.objects.get(pk=region_id)
                    region_name = region.name
                    query_list = list()
                    query_list.append(Q(('client__assigned_to__parent__pk', region_id)))
                    query_list.append(Q(('type', 'SecondaryTransaction')))
                    query_list.append(Q(('transaction_type', TransactionType.Sale.value)))
                    param = validated_report_param(model=Transaction, query=query_list, text=region_name)
                    if param != '':
                        report_url = QUERY_OPEN_MANUAL_PARAM_URL + PARAM_JOIN + 'paramdregion=' + param + PARAM_JOIN \
                                     + QUERY_OPEN + 'secondary-sale-'+ self.extract_parameter('type').lower() +'.saiku'
                except:
                    report_url = QUERY_OPEN_URL + 'secondary-sale-'+ self.extract_parameter('type').lower() +'.saiku'
        data_dict = dict()
        data_dict['href'] = report_url
        return super().get_json_response(self.convert_context_to_json(data_dict), **kwargs)
