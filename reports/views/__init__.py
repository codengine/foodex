__author__ = "auto generated"

from reports.views.base.report_view import GenericReportView, AreaBasedReportView, ProductBasedReportView
from reports.views.dashboard.dashboard_detail_view import DashBoardDetailView
from reports.views.dashboard.dashboard_summary_view import DashBoardSummaryView
from reports.views.drilldown.drill_down_report_view import DrillDownReportView
from reports.views.gross_profit.gross_profit_area_view import GrossProfitByAreaView
from reports.views.gross_profit.gross_profit_product_view import GrossProfitByProductView
from reports.views.inventory.delivered_products_view import DeliveredProductView
from reports.views.inventory.inventory_views import InventoryReportView, ClientInventoryReportView, InTransitReportView
from reports.views.maps.maps_retailer_location_view import MapRetailerLocationView
from reports.views.maps.maps_secondary_sales_view import MapSecondarySaleReportView
from reports.views.primary_sales.client_invoice_values.client_invoice_view import ClientInvoiceReportView
from reports.views.primary_sales.client_invoice_values.invoice_value_view import TotalInvoiceValueView
from reports.views.primary_sales.customer_sales_view.todays_customer_with_primary_sales import TodayPrimarySaleForCustomerView
from reports.views.primary_sales.payment_collections.payment_collection_view import TotalPaymentCollectionView
from reports.views.primary_sales.product_invoice_values.invoiced_items_view import TodayInvoicedItemsView
from reports.views.primary_sales.primary_sales_area_view import PrimarySalesByAreaView
from reports.views.primary_sales.primary_sales_product_view import PrimarySalesByProductView
from reports.views.secondary_sales.outlets.areawise_outlet_view import AreawiseOutletView
from reports.views.secondary_sales.outlets.distributorwise_outlet_view import DistWiseOutletView
from reports.views.secondary_sales.outlets.ordered_outlets_view import OutletOrderedView, NewOutletOpenedView
from reports.views.secondary_sales.outlets.outlet_numbers_view import OutletNumberReportView
from reports.views.secondary_sales.outlets.outlet_performance_view import OutletPerformanceView
from reports.views.secondary_sales.sales_person_performance.sales_person_order_view import SPOrderAverageValueView
from reports.views.secondary_sales.sales_person_performance.sales_person_performance_view import SalesPersonPerformanceView
from reports.views.secondary_sales.sales_person_performance.sales_person_product_performance_view import SalesPersonProductPerformanceView
from reports.views.secondary_sales.deleted_canceled_order_view import CanceledDeletedOrderReportView
from reports.views.secondary_sales.secondary_sales_area_view import SecondarySalesByAreaView
from reports.views.secondary_sales.secondary_sales_product_view import SecondarySalesByProductView
from reports.views.secondary_sales.secondary_sales_within_area_view import SecondarySalesWithinAreaView


__all__ = ['SalesPersonProductPerformanceView']
__all__ += ['SecondarySalesByAreaView']
__all__ += ['DashBoardDetailView']
__all__ += ['SecondarySalesWithinAreaView']
__all__ += ['SecondarySalesByProductView']
__all__ += ['TodayPrimarySaleForCustomerView']
__all__ += ['GrossProfitByProductView']
__all__ += ['DistWiseOutletView']
__all__ += ['DeliveredProductView']
__all__ += ['PrimarySalesByAreaView']
__all__ += ['MapSecondarySaleReportView']
__all__ += ['TotalPaymentCollectionView']
__all__ += ['DashBoardSummaryView']
__all__ += ['AreawiseOutletView']
__all__ += ['GrossProfitByAreaView']
__all__ += ['TodayInvoicedItemsView']
__all__ += ['SalesPersonPerformanceView']
__all__ += ['DrillDownReportView']
__all__ += ['MapRetailerLocationView']
__all__ += ['OutletPerformanceView']
__all__ += ['OutletNumberReportView']
__all__ += ['OutletOrderedView']
__all__ += ['NewOutletOpenedView']
__all__ += ['SPOrderAverageValueView']
__all__ += ['ClientInvoiceReportView']
__all__ += ['InventoryReportView']
__all__ += ['ClientInventoryReportView']
__all__ += ['InTransitReportView']
__all__ += ['PrimarySalesByProductView']
__all__ += ['GenericReportView']
__all__ += ['AreaBasedReportView']
__all__ += ['ProductBasedReportView']
__all__ += ['TotalInvoiceValueView']
__all__ += ['CanceledDeletedOrderReportView']
