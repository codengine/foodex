from django.db.models.aggregates import Sum, Min, Max
from blackwidow.core.generics.views.list_view import GenericListView
from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from blackwidow.sales.models.transactions.transaction import Transaction, TransactionType
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.clients.distributor import Distributor
from foodex.models.clients.retailers import Retailers
from foodex.models.infrastructure.area import Area
from foodex.models.infrastructure.region import Region
from reports.models.maps.maps_report import MapSecondarySaleReport
from django.core.urlresolvers import reverse
from django.forms.forms import Form
from django.utils.datastructures import SortedDict
from django import forms
import datetime

__author__ = 'Machine II'

@decorate(override_view(model=MapSecondarySaleReport, view=ViewActionEnum.Manage))
class MapSecondarySaleReportView(GenericListView):
    def get_template_names(self):
        return ['reports/map-secondary-sale.html']

    def get_wrapped_parameters(self, parameters):
        class DynamicForm(Form):
            pass

        form = DynamicForm()
        for p in parameters:
            form.fields[p['name']] = p['field']
        return form

    def get_report_parameters(self, **kwargs):
        parameters = SortedDict()
        parameters['G1'] = self.get_wrapped_parameters(({
                                                            'name': 'from_date',
                                                            'field': forms.DateTimeField(input_formats=['%d/%m/%Y'], widget=forms.DateInput(attrs={'data-format':"dd/MM/yyyy", 'class': 'date-time-picker'}, format='%d/%m/%Y'))
                                                        }, {
                                                            'name': 'to_date',
                                                            'field': forms.DateTimeField(input_formats=['%d/%m/%Y'], widget=forms.DateInput(attrs={'data-format':"dd/MM/yyyy", 'class': 'date-time-picker'}, format='%d/%m/%Y'))
                                                        }, ))
        parameters['G2'] = self.get_wrapped_parameters(( {'name': 'region',
                                                          'field': forms.ModelChoiceField(queryset=Region.get_role_based_queryset(),
                                                                                          empty_label="All", label='Select Region',
                                                                                          required=False, widget=forms.Select(
                                                                  attrs={'class': 'select2', 'width': '220', 'data-child': 'area'}))
                                                         },
                                                         {'name': 'area',
                                                         'field': forms.CharField(label='Select Area',
                                                                                  required=False, widget=forms.TextInput(
                                                                 attrs={'data-child': 'distributor','width': '220', 'class': 'select2-input', 'data-depends-on': 'region', 'data-depends-property': 'parent:id',
                                                                        'data-url': reverse(Area.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
                                                        },
                                                        {
                                                            'name': 'distributor',
                                                            'field': forms.CharField(label='Select Distributor', required=False,
                                                                                     widget=forms.TextInput(attrs={'class': 'select2-input',
                                                                                                                   'width': '220', 'data-depends-on': 'area', 'data-depends-property': 'assigned_to:id',
                                                                                                                   'data-url': reverse(Distributor.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
                                                        }, ))
        parameters['G3'] = self.get_wrapped_parameters(( ))
        return parameters

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Secondary Sales Location"
        context['enable_map'] = True
        context['parameters'] = self.get_report_parameters(**kwargs)
        return context

    def get_json_response(self, content, **kwargs):

        distributor = self.extract_parameter('distributor')
        area = self.extract_parameter('area')
        region = self.extract_parameter('region')

        from_date = self.extract_parameter('from_date')
        if from_date is not None:
            f_date = datetime.datetime.strptime(from_date, "%d/%m/%Y").timestamp() * 1000
        else:
            f_date = Transaction.objects.aggregate(Min('transaction_time'))['transaction_time__min']
            # f_date = datetime.datetime.utcfromtimestamp(f_date/1000).strftime("%d/%m/%Y")

        to_date = self.extract_parameter('to_date')
        if to_date is not None:
            t_date = (datetime.datetime.strptime(to_date, "%d/%m/%Y") + datetime.timedelta(1)).timestamp() * 1000
        else:
            t_date = Transaction.objects.aggregate(Max('transaction_time'))['transaction_time__max']
            # t_date = datetime.datetime.utcfromtimestamp(t_date/1000).strftime("%d/%m/%Y")

        if distributor is not None and distributor != '' and int(distributor):
            retailers = Retailers.objects.filter(clientassignment__infrastructureunit__parent_client__pk=distributor)\
                .values_list('pk', flat=True) # 'name', 'address__location__latitude', 'address__location__longitude')
        elif area is not None and area != '' and int(area)>0:
            retailers = Retailers.objects.filter(clientassignment__infrastructureunit__parent_client__assigned_to__pk=area)\
                .values_list('pk', flat=True)
        elif region is not None and region!='' and int(region)>0:
            retailers = Retailers.objects.filter(clientassignment__infrastructureunit__parent_client__assigned_to__parent__pk=region)\
                .values_list('pk', flat=True)
        else:
            retailers = Retailers.objects.all().values_list('pk', flat=True)

        secondary_sales = TransactionBreakDown.objects\
            .filter(transaction__client_2_id__in=retailers, transaction__transaction_type=TransactionType.Sale.value,
                    transaction__transaction_time__range=(f_date,t_date))\
            .values('transaction__client_2__pk','transaction__client_2__name', 'transaction__location__latitude',
                    'transaction__location__longitude')\
            .annotate(total_sales=Sum('total'))\
            .order_by('transaction__client_2__pk')

        retailer_locations = []
        for secondary_sale in secondary_sales:
            if secondary_sale['transaction__location__latitude'] is not None \
                    and secondary_sale['transaction__location__longitude'] is not None:

                retailer_locations.append({
                    'name': secondary_sale['transaction__client_2__name'],
                    'latitude': secondary_sale['transaction__location__latitude'] if secondary_sale['transaction__location__latitude'] is not None else 0,
                    'longitude': secondary_sale['transaction__location__longitude'] if secondary_sale['transaction__location__longitude'] is not None else 0,
                    'total_sales': secondary_sale['total_sales']
                })

        data_dict = dict()
        data_dict['title'] = "Secondary Retailers Location"
        data_dict['items'] = retailer_locations
        return super().get_json_response(self.convert_context_to_json(data_dict), **kwargs)