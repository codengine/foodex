from django.db.models.aggregates import Min, Max
from blackwidow.core.generics.views.list_view import GenericListView
from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from blackwidow.sales.models.transactions.transaction import Transaction
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.clients.distributor import Distributor
from foodex.models.clients.retailers import Retailers
from foodex.models.infrastructure.area import Area
from foodex.models.infrastructure.region import Region
from reports.models.maps.maps_report import MapSecondarySaleReport, MapRetailerLocationReport
from django.core.urlresolvers import reverse
from django.forms.forms import Form
from django.utils.datastructures import SortedDict
from django import forms
import datetime

__author__ = 'Machine II'

@decorate(override_view(model=MapRetailerLocationReport, view=ViewActionEnum.Manage))
class MapRetailerLocationView(GenericListView):
    def get_template_names(self):
        return ['reports/map-retailer-location.html']

    def get_wrapped_parameters(self, parameters):
        class DynamicForm(Form):
            pass

        form = DynamicForm()
        for p in parameters:
            form.fields[p['name']] = p['field']
        return form

    def get_report_parameters(self, **kwargs):
        parameters = SortedDict()
        parameters['G1'] = self.get_wrapped_parameters(({'name': 'region',
                                                            'field': forms.ModelChoiceField(queryset=Region.get_role_based_queryset(),
                                                                empty_label="All", label='Select Region',
                                                                required=False, widget=forms.Select(
                                                                    attrs={'class': 'select2', 'width': '220', 'data-child': 'area'}))
                                                        }, {'name': 'area',
                                                            'field': forms.CharField(label='Select Area',
                                                                                     required=False, widget=forms.TextInput(
                                                                    attrs={'data-child': 'distributor','width': '220', 'class': 'select2-input', 'data-depends-on': 'region', 'data-depends-property': 'parent:id',
                                                                           'data-url': reverse(Area.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
                                                        },{
                                                             'name': 'distributor',
                                                             'field': forms.CharField(label='Select Distributor', required=False,
                                                                                      widget=forms.TextInput(attrs={'class': 'select2-input',
                                                                                                                    'width': '220', 'data-depends-on': 'area', 'data-depends-property': 'assigned_to:id',
                                                                                                                    'data-url': reverse(Distributor.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
                                                         },))
        parameters['G2'] = self.get_wrapped_parameters(( ))
        parameters['G3'] = self.get_wrapped_parameters(( ))
        return parameters

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Retailer Location Map"
        context['enable_map'] = True
        context['parameters'] = self.get_report_parameters(**kwargs)
        return context

    def get_json_response(self, content, **kwargs):
        distributor = self.extract_parameter('distributor')
        area = self.extract_parameter('area')
        region = self.extract_parameter('region')

        if distributor is not None and distributor != '' and int(distributor):
            retailers = Retailers.objects.filter(clientassignment__infrastructureunit__parent_client__pk=distributor).values_list('name', 'address__location__latitude', 'address__location__longitude')
        elif area is not None and area != '' and int(area)>0:
            retailers = Retailers.objects.filter(clientassignment__infrastructureunit__parent_client__assigned_to__pk=area).values_list('name', 'address__location__latitude', 'address__location__longitude')
        elif region is not None and region!='' and int(region)>0:
            retailers = Retailers.objects.filter(clientassignment__infrastructureunit__parent_client__assigned_to__parent__pk=region).values_list('name', 'address__location__latitude', 'address__location__longitude')
        else:
            retailers = Retailers.objects.all().values_list('name', 'address__location__latitude', 'address__location__longitude')

        retailer_locations = []
        for retailer in retailers:
            retailer_locations.append({
                'name': retailer[0],
                'latitude': retailer[1] if retailer[1] is not None else 0,
                'longitude': retailer[2] if retailer[2] is not None else 0
            })

        data_dict = dict()
        data_dict['title'] = "Secondary Retailers Location"
        data_dict['items'] = retailer_locations
        return super().get_json_response(self.convert_context_to_json(data_dict), **kwargs)