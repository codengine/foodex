from django.db.models import Min, Max
from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from foodex.models import CompletedSalesTransaction
from reports.models.inventory.delivered_products import DeliveredProductReport
from blackwidow.core.generics.views.list_view import GenericListView
from config.enums.view_action_enum import ViewActionEnum
from django.forms.forms import Form
from django import forms
from django.utils.datastructures import SortedDict
import datetime

__author__ = 'Ziaul Haque'


@decorate(override_view(model=DeliveredProductReport, view=ViewActionEnum.Manage))
class DeliveredProductView(GenericListView):
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_template_names(self):
        return ['reports/delivered-products.html']

    def get_wrapped_parameters(self, parameters):
        class DynamicForm(Form):
            pass

        form = DynamicForm()
        for p in parameters:
            form.fields[p['name']] = p['field']
        return form

    def get_report_parameters(self, **kwargs):
        parameters = SortedDict()
        parameters['G1'] = self.get_wrapped_parameters(({
                                                            'name': 'from_date',
                                                            'field': forms.DateTimeField(input_formats=['%d/%m/%Y'],
                                                                                         widget=forms.DateInput(attrs={
                                                                                             'data-format': "dd/MM/yyyy",
                                                                                             'class': 'date-time-picker'},
                                                                                                                format='%d/%m/%Y'))
                                                        }, {
                                                            'name': 'to_date',
                                                            'field': forms.DateTimeField(input_formats=['%d/%m/%Y'],
                                                                                         widget=forms.DateInput(attrs={
                                                                                             'data-format': "dd/MM/yyyy",
                                                                                             'class': 'date-time-picker'},
                                                                                                                format='%d/%m/%Y'))
                                                        },))
        parameters['G2'] = self.get_wrapped_parameters([])
        parameters['G3'] = self.get_wrapped_parameters([])
        return parameters


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Delivered Products"
        context['enable_map'] = False
        context['parameters'] = self.get_report_parameters(**kwargs)
        return context

    def get_json_response(self, content, **kwargs):

        from_date = self.extract_parameter('from_date')
        if from_date is not None:
            f_date = datetime.datetime.strptime(from_date, "%d/%m/%Y").timestamp() * 1000
        else:
            f_date = CompletedSalesTransaction.objects.aggregate(Min('date_created'))['date_created__min']

        to_date = self.extract_parameter('to_date')
        if to_date is not None:
            t_date = (datetime.datetime.strptime(to_date, "%d/%m/%Y") + datetime.timedelta(1)).timestamp() * 1000
        else:
            t_date = CompletedSalesTransaction.objects.aggregate(Max('date_created'))['date_created__max']

        data_dict = dict()
        report = DeliveredProductReport().build_report(request=self.request, time_from=f_date, time_to=t_date,
                                                       styled=True)

        data_dict['report'] = report
        return super().get_json_response(self.convert_context_to_json(data_dict), **kwargs)