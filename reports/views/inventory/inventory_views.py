import datetime
import os
from openpyxl import Workbook
from openpyxl.styles import Style
from openpyxl.styles.alignment import Alignment
from openpyxl.styles.fonts import Font
from django.utils.datastructures import SortedDict
from django.views.generic.base import TemplateView
from django.shortcuts import redirect
from blackwidow.core.generics.views.list_view import GenericListView
from blackwidow.core.models.file.exportfileobject import ExportFileObject
from blackwidow.core.models.log.logs import ErrorLog
from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from blackwidow.engine.extensions.clock import Clock
from blackwidow.sales.models.transactions.transaction import Transaction
from config.enums.view_action_enum import ViewActionEnum
from reports.models.inventory.inventory_report import InventoryReport, ClientInventoryReport, InTransitReport
from blackwidow.core.generics.views.list_view import GenericListView
from django.core.urlresolvers import reverse
from django.forms.forms import Form
from django import forms
from blackwidow.core.models.manufacturers.manufacturer import Manufacturer
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.products.product_group import ProductGroup
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.clients.distributor import Distributor
from foodex.models.infrastructure.area import Area
from foodex.models.infrastructure.region import Region
from settings import EXPORT_FILE_ROOT

__author__ = 'Machine-PC'

@decorate(override_view(model=InventoryReport, view=ViewActionEnum.Manage))
class InventoryReportView(GenericListView):
    def get(self, request, *args, **kwargs):
        return redirect('/inventories/')

@decorate(override_view(model=ClientInventoryReport, view=ViewActionEnum.Manage))
class ClientInventoryReportView(GenericListView):
    def get(self, request, *args, **kwargs):
        return redirect('/client-inventories/')


@decorate(override_view(model=InTransitReport, view=ViewActionEnum.Manage))
class InTransitReportView(GenericListView):
    def get(self, request, *args, **kwargs):
        if request.GET.get('export', 'False') == 'True':
            from_date = self.extract_parameter('from_date')
            if from_date is not None:
                f_date = datetime.datetime.strptime(from_date, "%d/%m/%Y").timestamp() * 1000
            else:
                f_date = 0 # MIN

            to_date = self.extract_parameter('to_date')
            if to_date is not None:
                t_date = (datetime.datetime.strptime(to_date, "%d/%m/%Y") + datetime.timedelta(1)).timestamp() * 1000
            else:
                t_date = 9999999999999 # MAX
            dest_filename = self.__class__.generate_excel(request,f_date, t_date, from_date, to_date)
            if dest_filename is not None:
                if self.is_json_request(request):
                    return super().get_json_response(self.convert_context_to_json({
                        'success': True,
                        "url": '/export-files/download/' + str(dest_filename.pk)
                    }), **kwargs)
                messages.success(request, "In Transit Records has been successfully exported to files. " \
                                          "<ol><li>" + dest_filename.file.name + "</li>" + "Please visit the <strong>Exported Files</strong> section to download them." \
                                                                                           "<a class='btn btn-danger btn-small' href='/export-files/' %}'>View Exported Files</a>", "permanent")

        return super().get(request, *args, **kwargs)

    def get_template_names(self):
        return ['reports/in-transit.html']

    def get_wrapped_parameters(self, parameters):
        class DynamicForm(Form):
            pass

        form = DynamicForm()
        for p in parameters:
            form.fields[p['name']] = p['field']
        return form

    def get_report_parameters(self, **kwargs):
        parameters = SortedDict()
        parameters['G1'] = self.get_wrapped_parameters(({
                                                            'name': 'from_date',
                                                            'field': forms.DateTimeField(input_formats=['%d/%m/%Y'], widget=forms.DateInput(attrs={'data-format':"dd/MM/yyyy", 'class': 'date-time-picker'}, format='%d/%m/%Y'))
                                                        }, {
                                                            'name': 'to_date',
                                                            'field': forms.DateTimeField(input_formats=['%d/%m/%Y'], widget=forms.DateInput(attrs={'data-format':"dd/MM/yyyy", 'class': 'date-time-picker'}, format='%d/%m/%Y'))
                                                        },))
        parameters['G2'] = self.get_wrapped_parameters(({
                                                            'name': 'region',
                                                            'field': forms.ModelChoiceField(queryset=Region.get_role_based_queryset(),
                                                                                            empty_label="All", label='Select Region',
                                                                                            required=False, widget=forms.Select(
                                                                    attrs={'class': 'select2', 'width': '220', 'data-child': 'area'}))
                                                        }, {
                                                            'name': 'area',
                                                            'field': forms.CharField(label='Select Area',
                                                                                     required=False, widget=forms.TextInput(
                                                                    attrs={'data-child': 'distributor','width': '220', 'class': 'select2-input', 'data-depends-on': 'region', 'data-depends-property': 'parent:id',
                                                                           'data-url': reverse(Area.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
                                                        },{
                                                            'name': 'distributor',
                                                            'field': forms.CharField(label='Select Distributor', required=False,
                                                                                     widget=forms.TextInput(attrs={'class': 'select2-input',
                                                                                                                   'width': '220', 'data-depends-on': 'area', 'data-depends-property': 'assigned_to:id',
                                                                                                                   'data-url': reverse(Distributor.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
                                                        }, ))
        parameters['G3'] = self.get_wrapped_parameters([])
        return parameters

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "In Transit Report"
        context['enable_map'] = False
        context['parameters'] = self.get_report_parameters(**kwargs)
        return context

    def get_json_response(self, content, **kwargs):

        distributor = self.extract_parameter('distributor')
        area = self.extract_parameter('area')
        region = self.extract_parameter('region')

        from_date = self.extract_parameter('from_date')
        if from_date is not None:
            f_date = datetime.datetime.strptime(from_date, "%d/%m/%Y").timestamp() * 1000
        else:
            f_date = BuySaleTransaction.objects.aggregate(Min('transaction_time'))['transaction_time__min']

        to_date = self.extract_parameter('to_date')
        if to_date is not None:
            t_date = (datetime.datetime.strptime(to_date, "%d/%m/%Y") + datetime.timedelta(1)).timestamp() * 1000
        else:
            t_date = BuySaleTransaction.objects.aggregate(Max('transaction_time'))['transaction_time__max']

        data_dict = dict()
        
        report = InTransitReport().build_report(request=self.request, region=region, area=area, distributor=distributor,
                                                time_from=f_date, time_to=t_date, styled=True)
        
        data_dict['report'] = report
        return super().get_json_response(self.convert_context_to_json(data_dict), **kwargs)

    @classmethod
    def generate_excel(cls,request, f_date, t_date, from_detail, to_detail):
        try:
            path = os.path.join(EXPORT_FILE_ROOT)
            if not os.path.exists(path):
                os.makedirs(path)
            wb = Workbook()
            dest_filename = 'In_Transit_Report'+str(Clock.utcnow().strftime('%d%m%Y%H%M'))
            file_path = path + os.sep + dest_filename + '.xlsx'
            ws = wb.create_sheet(0)
            row_number = 1
            column_number = 1
            ws.title = 'In Transit Summary'
            header_cell = ws.cell(row=row_number, column=column_number)
            header_cell.value = "In Transit Summary (" + str(from_detail) + "-" + str(to_detail) + ")"
            # row = ws.row_dimensions[1]
            header_cell.style = Style(font=Font(
                name='Calibri',
                size=10,
                bold=True,
                vertAlign=None), alignment=Alignment(horizontal='general'))
            ws.merge_cells(start_row=row_number, start_column=column_number, end_row=row_number, end_column=column_number+4)
            row_number = 3

            report = InTransitReport().build_report(request=request, time_from=f_date, time_to=t_date)
            for _row in report:
                col = 1
                for _col in _row:
                    ws.cell(row=row_number, column=col).value = _col
                    col += 1

                row_number += 1

            wb.save(filename=file_path)
            export_file_object = ExportFileObject()
            export_file_object.path = file_path
            export_file_object.name = dest_filename
            export_file_object.file = file_path
            export_file_object.extension = '.xlsx'
            export_file_object.save()
            return export_file_object
        except Exception as exp:
            ErrorLog.log(exp)
            # wb.close()
            return None
