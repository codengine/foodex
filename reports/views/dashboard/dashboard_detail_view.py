import datetime
from decimal import Decimal
from datetime import timedelta
from django.db.models.aggregates import Sum
from blackwidow.core.generics.views.list_view import GenericListView
from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from blackwidow.sales.models.transactions.transaction import Transaction, TransactionType
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from config.enums.view_action_enum import ViewActionEnum
from reports.models.dashboard.dashboard_report import DashboardSummaryReport, DashboardDetailReport
from django.forms.forms import Form

__author__ = 'Machine II'

@decorate(override_view(model=DashboardDetailReport, view=ViewActionEnum.Manage))
class DashBoardDetailView(GenericListView):
    def get_template_names(self):
        return ['reports/report-detail-dashboard.html']

    def get_wrapped_parameters(self, parameters):
        class DynamicForm(Form):
            pass

        form = DynamicForm()
        for p in parameters:
            form.fields[p['name']] = p['field']
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Foodex: Sales Insight"
        return context

    def get_json_response(self, content, **kwargs):
        report_type = self.extract_parameter('type')
        period = self.extract_parameter('period')
        data = []
        if int(report_type) == 0:
            data = self.get_secondary_sales_by_distributor(period=period)
        elif int(report_type) == 2:
            data = self.get_secondary_sales_by_area(period=period)
        elif int(report_type) == 1:
            data = self.get_secondary_sales_by_product(period=period)
        elif int(report_type) == 3:
            data = self.get_secondary_sales_by_category(period=period)

        return super().get_json_response(self.convert_context_to_json(data), **kwargs)

    def get_secondary_sales_by_distributor(self, period='Monthly'):
        sales = []

        if period == 'Monthly':
            sales.append(['Distributor', 'Secondary Sales (last month,BDT)'])
            now = datetime.datetime.now()
            today_begin = now.replace(day=1, hour=0, minute=0, second=0)
            yesterday = today_begin
            yesterday_begin = today_begin.replace(month=now.month-1) if now.month > 1 \
                else today_begin.replace(month=12, year=now.year-1)
        else:
            sales.append(['Distributor', 'Secondary Sales (yesterday,BDT)'])
            now = datetime.datetime.now()
            today_begin = now.replace(hour=0, minute=0, second=0)

            yesterday = now - datetime.timedelta(days=1)
            yesterday_begin = today_begin - datetime.timedelta(days=1)

        now = now.timestamp() * 1000
        today_begin = today_begin.timestamp() * 1000
        yesterday = yesterday.timestamp() * 1000
        yesterday_begin = yesterday_begin.timestamp() * 1000

        yesterday_record = Transaction.objects.filter(transaction_time__gte=yesterday_begin, transaction_time__lt=yesterday,
                  transaction_type=TransactionType.Sale.value, type='SecondaryTransaction') \
            .values('client__pk', 'client__name') \
            .annotate(total=Sum('total')) \
            .order_by('-total')[:30]

        for r in yesterday_record:
            sales.append([r['client__name'], r['total']])

        data = {
            'type': 'ColumnChart',
            'options': {
                'legend': {'position': 'top', 'textStyle': {'fontSize': '14'}},
                'bar': {'groupWidth': '96%'},
                'backgroundColor': 'none',
                'title': 'Secondary Sales (Distributor)',
                'hAxis': {
                    'titleTextStyle': {
                        'color': 'red'
                    },
                    'textStyle': {
                        'fontSize': '9'
                    },
                    'maxAlternation': '4',
                    'slantedTextAngle': '90'
                }
            },
            'data': sales
        }
        return data

    def get_secondary_sales_by_area(self, period='Monthly'):
        sales = []
        now = datetime.datetime.now()
        yesterday_begin = now - timedelta(days=30)
        yesterday_begin = yesterday_begin.replace(hour=0,minute=0,second=0)

        now = now.timestamp() * 1000
        yesterday_begin = yesterday_begin.timestamp() * 1000

        yesterday_record = Transaction.objects.filter(transaction_time__gte=yesterday_begin, transaction_time__lt=now,
                                                      transaction_type=TransactionType.Sale.value, type='SecondaryTransaction') \
                               .values('client__assigned_to__pk', 'client__assigned_to__name', 'transaction_time', 'total')\
            .order_by('transaction_time')

        client_names = list(set([x['client__assigned_to__name'] for x in yesterday_record]))

        headers = ['Date']
        for client in client_names:
            headers.append(client)
        sales.append(headers)

        record = dict()

        st_t = yesterday_begin
        en_t = st_t + 24 * 60 * 60 * 1000
        index = 0
        while st_t < now:
            day = datetime.date.fromtimestamp(int(st_t/1000)).strftime('%Y-%m-%d')
            record[day] = dict()
            day_sale = [day]
            for client in client_names:
                record[day][client] = 0
            while index < len(yesterday_record) and yesterday_record[index]['transaction_time'] < en_t:
                client = yesterday_record[index]['client__assigned_to__name']
                record[day][client] += yesterday_record[index]['total']
                index += 1
            st_t = en_t
            en_t += 24 * 60 * 60 * 1000
            for key, val in record[day].items():
                day_sale.append(val)
            sales.append(day_sale)

        data = {
            'type': 'AreaChart',
            'options': {
                'legend': {'position': 'top', 'textStyle': {'fontSize': '14'}},
                'bar': {'groupWidth': '96%'},
                'backgroundColor': 'none',
                'isStacked': True,
                'title': 'Secondary sales last 30 days (Area)',
                'hAxis': {
                    'titleTextStyle': {
                        'color': 'red'
                    },
                    'textStyle': {
                        'fontSize': '9'
                    },
                    'maxAlternation': '4',
                    'slantedTextAngle': '90'
                }
            },
            'data': sales
        }
        return data

    def get_secondary_sales_by_product(self, period='Monthly'):
        sales = []

        if period == 'Monthly':
            sales.append(['Product', 'Secondary Sales (last month,BDT)'])
            now = datetime.datetime.now()
            today_begin = now.replace(day=1, hour=0, minute=0, second=0)
            yesterday = today_begin
            yesterday_begin = today_begin.replace(month=now.month-1) if now.month > 1 \
                else today_begin.replace(month=12, year=now.year-1)
        else:
            sales.append(['Product', 'Secondary Sales (yesterday,BDT)'])
            now = datetime.datetime.now()
            today_begin = now.replace(hour=0, minute=0, second=0)

            yesterday = now - datetime.timedelta(days=1)
            yesterday_begin = today_begin - datetime.timedelta(days=1)

        now = now.timestamp() * 1000
        today_begin = today_begin.timestamp() * 1000
        yesterday = yesterday.timestamp() * 1000
        yesterday_begin = yesterday_begin.timestamp() * 1000

        secondary_record = TransactionBreakDown.objects.filter(transaction__transaction_time__gte=yesterday_begin,
                                                               transaction__transaction_time__lte=yesterday,
                                                               transaction__transaction_type=TransactionType.Sale.value, transaction__type='SecondaryTransaction') \
                               .values('product_id', 'product__code') \
                               .annotate(total=Sum('total')) \
                               .order_by('-total')[:30]

        for r in secondary_record:
            sales.append([r['product__code'], r['total']])

        data = {
            'type': 'ColumnChart',
            'options': {
                'legend': {'position': 'top', 'textStyle': {'fontSize': '14'}},
                'bar': {'groupWidth': '85%'},
                'backgroundColor': 'none',
                'title': 'Secondary Sales (Product)',
                'hAxis': {
                    'titleTextStyle': {
                        'color': 'red'
                    },
                    'textStyle': {
                        'fontSize': '9'
                    },
                    'maxAlternation': '4',
                    'slantedTextAngle': '90'
                }
            },
            'data': sales
        }
        return data

    def get_secondary_sales_by_category(self, period='Monthly'):
        sales = []
        now = datetime.datetime.now()
        yesterday_begin = now - timedelta(days=30)
        yesterday_begin = yesterday_begin.replace(hour=0,minute=0,second=0)

        now = now.timestamp() * 1000
        yesterday_begin = yesterday_begin.timestamp() * 1000

        secondary_record = TransactionBreakDown.objects.filter(transaction__transaction_time__gte=yesterday_begin,
                                                               transaction__transaction_time__lte=now,
                                                               transaction__transaction_type=TransactionType.Sale.value, transaction__type='SecondaryTransaction') \
                               .values('product_id', 'product__categories__name', 'transaction__transaction_time', 'total')

        cat_names = list(set([x['product__categories__name'] for x in secondary_record]))

        headers = ['Date']
        for client in cat_names:
            headers.append(client)
        sales.append(headers)

        record = dict()

        st_t = yesterday_begin
        en_t = st_t + 24 * 60 * 60 * 1000
        index = 0
        while st_t < now:
            day = datetime.date.fromtimestamp(int(st_t/1000)).strftime('%Y-%m-%d')
            record[day] = dict()
            day_sale = [day]
            for client in cat_names:
                record[day][client] = 0
            while index < len(secondary_record) and secondary_record[index]['transaction__transaction_time'] < en_t:
                client = secondary_record[index]['product__categories__name']
                record[day][client] += secondary_record[index]['total']
                index += 1
            st_t = en_t
            en_t += 24 * 60 * 60 * 1000
            for key, val in record[day].items():
                day_sale.append(val)
            sales.append(day_sale)

        data = {
            'type': 'AreaChart',
            'options': {
                'legend': {'position': 'top', 'textStyle': {'fontSize': '14'}},
                'bar': {'groupWidth': '85%'},
                'isStacked': True,
                'backgroundColor': 'none',
                'title': 'Secondary Sales last 30 days (Categories)',
                'hAxis': {
                    'titleTextStyle': {
                        'color': 'red'
                    },
                    'textStyle': {
                        'fontSize': '9'
                    },
                    'maxAlternation': '4',
                    'slantedTextAngle': '90'
                }
            },
            'data': sales
        }
        return data