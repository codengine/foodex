import datetime
from decimal import Decimal
from django.db.models.aggregates import Sum
from blackwidow.core.generics.views.list_view import GenericListView
from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from blackwidow.sales.models.transactions.transaction import Transaction, TransactionType
from blackwidow.sales.models.transactions.transaction_break_down import TransactionBreakDown
from config.enums.view_action_enum import ViewActionEnum
from reports.models.dashboard.dashboard_report import DashboardSummaryReport
from django.forms.forms import Form

__author__ = 'Machine II'


@decorate(override_view(model=DashboardSummaryReport, view=ViewActionEnum.Manage))
class DashBoardSummaryView(GenericListView):
    def get_template_names(self):
        return ['reports/report-summary-dashboard.html']

    def get_wrapped_parameters(self, parameters):
        class DynamicForm(Form):
            pass

        form = DynamicForm()
        for p in parameters:
            form.fields[p['name']] = p['field']
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Foodex: Summary Dashboard"
        return context

    def get_json_response(self, content, **kwargs):
        report_type = self.extract_parameter('type')
        period = self.extract_parameter('period')
        data = []
        if int(report_type) == 0:
            data = self.get_primary_sales_data(period=period)
        elif int(report_type) == 1:
            data = self.get_secondary_sales_data(period=period)
        elif int(report_type) == 2:
            data = self.get_top_distributor_data(period=period)
        elif int(report_type) == 3:
            data = self.get_top_product_data(period=period)

        return super().get_json_response(self.convert_context_to_json(data), **kwargs)

    def get_primary_sales_data(self, period='Monthly'):
        sales = []

        if period == 'Monthly':
            sales.append(['Area', 'This month', 'Last month (up to same day)'])
            now = datetime.datetime.now()
            today_begin = now.replace(day=1, hour=0, minute=0, second=0)

            try:
                yesterday = now.replace(month=now.month - 1) if now.month > 1 \
                    else now.replace(month=12, year=now.year - 1)
            except:  # day out of range
                if now.month == 3:  # March to February, day gape is more than 1 day
                    yesterday = now.replace(month=2, day=28)
                else:
                    yesterday = now.replace(month=now.month - 1, day=now.day - 1) if now.month > 1 \
                        else now.replace(month=12, year=now.year - 1)
            yesterday_begin = today_begin.replace(month=now.month - 1) if now.month > 1 \
                else today_begin.replace(month=12, year=now.year - 1)
        else:
            sales.append(['Area', 'Today', 'Yesterday (up to same time)'])
            now = datetime.datetime.now()
            today_begin = now.replace(hour=0, minute=0, second=0)

            yesterday = now - datetime.timedelta(days=1)
            yesterday_begin = today_begin - datetime.timedelta(days=1)

        now = now.timestamp() * 1000
        today_begin = today_begin.timestamp() * 1000
        yesterday = yesterday.timestamp() * 1000
        yesterday_begin = yesterday_begin.timestamp() * 1000

        today_record = Transaction.objects.filter(transaction_time__gte=today_begin, transaction_time__lt=now,
                                                  transaction_type=TransactionType.Sale.value,
                                                  type='PrimaryTransaction') \
            .values('client__assigned_to__pk', 'client__assigned_to__name') \
            .annotate(total=Sum('total')) \
            .order_by('client__assigned_to__pk')

        yesterday_record = Transaction.objects.filter(transaction_time__gte=yesterday_begin,
                                                      transaction_time__lt=yesterday,
                                                      transaction_type=TransactionType.Sale.value,
                                                      type='PrimaryTransaction') \
            .values('client__assigned_to__pk', 'client__assigned_to__name') \
            .annotate(total=Sum('total')) \
            .order_by('client__assigned_to__pk')

        _t = _y = 0
        while _t < len(today_record) and _y < len(yesterday_record):
            y_id = yesterday_record[_y]['client__assigned_to__pk']
            t_id = today_record[_t]['client__assigned_to__pk']

            while y_id < t_id and _y < len(yesterday_record):
                sales.append([yesterday_record[_y]['client__assigned_to__name'], 0, yesterday_record[_y]['total']])
                _y += 1
                y_id = yesterday_record[_y]['client__assigned_to__pk']

            if y_id == t_id:
                sales.append([yesterday_record[_y]['client__assigned_to__name'], today_record[_t]['total'],
                              yesterday_record[_y]['total']])
                _t += 1
                _y += 1
            else:
                sales.append([today_record[_t]['client__assigned_to__name'], today_record[_t]['total'], 0])
                _t += 1

        while _y < len(yesterday_record):
            sales.append([yesterday_record[_y]['client__assigned_to__name'], 0, yesterday_record[_y]['total']])
            _y += 1

        while _t < len(today_record):
            sales.append([today_record[_t]['client__assigned_to__name'], today_record[_t]['total'], 0])
            _t += 1

        data = {
            'type': 'ColumnChart',
            'options': {
                'legend': {'position': 'top', 'textStyle': {'fontSize': '14'}},
                'bar': {'groupWidth': '96%'},
                'backgroundColor': 'none',
                'title': 'Primary Sales Value',
                'hAxis': {
                    'titleTextStyle': {
                        'color': 'red'
                    },
                    'textStyle': {
                        'fontSize': '9'
                    },
                    'maxAlternation': '4',
                    'slantedTextAngle': '90'
                }
            },
            'data': sales
        }
        return data

    def get_secondary_sales_data(self, period='Monthly'):
        sales = []

        if period == 'Monthly':
            sales.append(['Area', 'This month', 'Last month (up to same day)'])
            now = datetime.datetime.now()
            today_begin = now.replace(day=1, hour=0, minute=0, second=0)

            try:
                yesterday = now.replace(month=now.month - 1) if now.month > 1 \
                    else now.replace(month=12, year=now.year - 1)
            except:  # day out of range
                if now.month == 3:  # March to February, day gape is more than 1 day
                    yesterday = now.replace(month=2, day=28)
                else:
                    yesterday = now.replace(month=now.month - 1, day=now.day - 1) if now.month > 1 \
                        else now.replace(month=12, year=now.year - 1)
            yesterday_begin = today_begin.replace(month=now.month - 1) if now.month > 1 \
                else today_begin.replace(month=12, year=now.year - 1)
        else:
            sales.append(['Area', 'Today', 'Yesterday (up to same time)'])
            now = datetime.datetime.now()
            today_begin = now.replace(hour=0, minute=0, second=0)

            yesterday = now - datetime.timedelta(days=1)
            yesterday_begin = today_begin - datetime.timedelta(days=1)

        now = now.timestamp() * 1000
        today_begin = today_begin.timestamp() * 1000
        yesterday = yesterday.timestamp() * 1000
        yesterday_begin = yesterday_begin.timestamp() * 1000

        today_record = Transaction.objects.filter(transaction_time__gte=today_begin, transaction_time__lt=now,
                                                  transaction_type=TransactionType.Sale.value,
                                                  type='SecondaryTransaction') \
            .values('client__assigned_to__pk', 'client__assigned_to__name') \
            .annotate(total=Sum('total')) \
            .order_by('client__assigned_to__pk')

        yesterday_record = Transaction.objects.filter(transaction_time__gte=yesterday_begin,
                                                      transaction_time__lt=yesterday,
                                                      transaction_type=TransactionType.Sale.value,
                                                      type='SecondaryTransaction') \
            .values('client__assigned_to__pk', 'client__assigned_to__name') \
            .annotate(total=Sum('total')) \
            .order_by('client__assigned_to__pk')

        _t = _y = 0
        while _t < len(today_record) and _y < len(yesterday_record):
            y_id = yesterday_record[_y]['client__assigned_to__pk']
            t_id = today_record[_t]['client__assigned_to__pk']

            while y_id < t_id and _y < len(yesterday_record):
                sales.append([yesterday_record[_y]['client__assigned_to__name'], 0, yesterday_record[_y]['total']])
                _y += 1
                y_id = yesterday_record[_y]['client__assigned_to__pk']

            if y_id == t_id:
                sales.append([yesterday_record[_y]['client__assigned_to__name'], today_record[_t]['total'],
                              yesterday_record[_y]['total']])
                _t += 1
                _y += 1
            else:
                sales.append([today_record[_t]['client__assigned_to__name'], today_record[_t]['total'], 0])
                _t += 1

        while _y < len(yesterday_record):
            sales.append([yesterday_record[_y]['client__assigned_to__name'], 0, yesterday_record[_y]['total']])
            _y += 1

        while _t < len(today_record):
            sales.append([today_record[_t]['client__assigned_to__name'], today_record[_t]['total'], 0])
            _t += 1

        data = {
            'type': 'ColumnChart',
            'options': {
                'legend': {'position': 'top', 'textStyle': {'fontSize': '14'}},
                'bar': {'groupWidth': '96%'},
                'backgroundColor': 'none',
                'title': 'Secondary Sales Value',
                'hAxis': {
                    'titleTextStyle': {
                        'color': 'red'
                    },
                    'textStyle': {
                        'fontSize': '9'
                    },
                    'maxAlternation': '4',
                    'slantedTextAngle': '90'
                }
            },
            'data': sales
        }
        return data

    def get_top_distributor_data(self, period='Monthly'):

        sales = []
        sales.append(['Distributor', 'Secondary Sales (BDT)'])

        if period == 'Monthly':
            graph_title = 'Top Distributors (Last month)'
            now = datetime.datetime.now()
            today_begin = now.replace(day=1, hour=0, minute=0, second=0)
            yesterday = today_begin
            yesterday_begin = today_begin.replace(month=now.month - 1) if now.month > 1 \
                else today_begin.replace(month=12, year=now.year - 1)
        else:
            graph_title = 'Top Distributors (Yesterday)'
            now = datetime.datetime.now()
            today_begin = now.replace(hour=0, minute=0, second=0)

            yesterday = today_begin
            yesterday_begin = today_begin - datetime.timedelta(days=1)

        now = now.timestamp() * 1000
        today_begin = today_begin.timestamp() * 1000
        yesterday = yesterday.timestamp() * 1000
        yesterday_begin = yesterday_begin.timestamp() * 1000

        today_record = Transaction.objects.filter(transaction_time__gte=yesterday_begin, transaction_time__lt=yesterday,
                                                  transaction_type=TransactionType.Sale.value,
                                                  type='SecondaryTransaction') \
                           .values('client__pk', 'client__name') \
                           .annotate(total=Sum('total')) \
                           .order_by('-total')[:5]

        for record in today_record:
            sales.append([record['client__name'], record['total']])

        data = {
            'type': 'ColumnChart',
            'options': {
                'legend': {'position': 'top', 'textStyle': {'fontSize': '14'}},
                'bar': {'groupWidth': '96%'},
                'backgroundColor': 'none',
                'title': 'Top Distributors (This month)',
                'hAxis': {
                    'titleTextStyle': {
                        'color': 'red'
                    },
                    'textStyle': {
                        'fontSize': '9'
                    },
                    'slantedText': 'true',
                    'maxAlternation': '3',
                    'slantedTextAngle': '25'
                }
            },
            'data': sales
        }
        return data

    def get_top_product_data(self, period='Monthly'):

        now = datetime.datetime.now()

        if period == 'Monthly':
            graph_title = 'Top Products (last month)'
            today_begin = now.replace(day=1, hour=0, minute=0, second=0)
            yesterday = today_begin
            yesterday_begin = today_begin.replace(month=now.month - 1) if now.month > 1 \
                else today_begin.replace(month=12, year=now.year - 1)
        else:
            graph_title = 'Top Products (yesterday)'
            today_begin = now.replace(hour=0, minute=0, second=0)

            yesterday = today_begin
            yesterday_begin = today_begin - datetime.timedelta(days=1)

        now = now.timestamp() * 1000
        today_begin = today_begin.timestamp() * 1000
        yesterday = yesterday.timestamp() * 1000
        yesterday_begin = yesterday_begin.timestamp() * 1000

        secondary_record = TransactionBreakDown.objects.filter(transaction__transaction_time__gte=yesterday_begin,
                                                               transaction__transaction_time__lte=yesterday,
                                                               transaction__transaction_type=TransactionType.Sale.value,
                                                               transaction__type='SecondaryTransaction') \
                               .values('product_id', 'product__code') \
                               .annotate(total=Sum('total')) \
                               .order_by('total')[:5]
        p_ids = []
        for s in secondary_record:
            p_ids.append(s['product_id'])

        primary_record = TransactionBreakDown.objects.filter(transaction__transaction_time__gte=yesterday_begin,
                                                             transaction__transaction_time__lte=yesterday,
                                                             transaction__transaction_type=TransactionType.Sale.value,
                                                             transaction__type='PrimaryTransaction',
                                                             product_id__in=p_ids) \
            .values('product_id', 'product__code') \
            .annotate(total=Sum('total')) \
            .order_by('total')

        primary = []
        for p in primary_record:
            primary.append([p['product_id'], p['product__code'], p['total']])
        secondary = []
        for s in secondary_record:
            secondary.append([s['product_id'], s['product__code'], s['total']])

        primary.sort(key=lambda p: p[0])
        secondary.sort(key=lambda s: s[0])

        record = []
        _p = _s = 0

        while _p < len(primary) and _s < len(secondary):
            p_id = primary[_p][0]
            s_id = secondary[_s][0]

            while s_id < p_id and _s < len(secondary):
                record.append([secondary[_s][1], Decimal(0), secondary[_s][2]])
                _s += 1
                s_id = secondary[_s][0]

            if s_id == p_id:
                record.append([primary[_p][1], primary[_p][2], secondary[_s][2]])
                _p += 1
                _s += 1
            else:
                record.append([primary[_p][1], primary[_p][2], Decimal(0)])
                _p += 1

        while _p < len(primary):
            record.append([primary[_p][1], primary[_p][2], Decimal(0)])
            _p += 1

        while _s < len(secondary):
            record.append([secondary[_s][1], Decimal(0), secondary[_s][2]])
            _s += 1

        record.sort(key=lambda r: r[2])
        record = record[:5]
        record.append(['Product-Code', 'Primary Sales (BDT)', 'Secondary Sales (BDT)'])
        record.reverse()

        data = {
            'type': 'ColumnChart',
            'options': {
                'legend': {'position': 'top', 'textStyle': {'fontSize': '14'}},
                'bar': {'groupWidth': '90%'},
                'backgroundColor': 'none',
                'title': graph_title,
                # 'isStacked': 'true',
                'hAxis': {
                    'titleTextStyle': {
                        'color': 'red'
                    },
                    'textStyle': {
                        'fontSize': '9'
                    },
                    'slantedText': 'true',
                    'maxAlternation': '4',
                    'slantedTextAngle': '20'
                }
            },
            'data': record
        }
        return data
