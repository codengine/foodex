from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from config.enums.view_action_enum import ViewActionEnum
from reports.models.gross_profit.gross_profit_area import GrossProfitReportByArea
from reports.models.secondary_sales.secondary_sales_area import SecondarySalesReportByArea
from reports.views.base.report_view import AreaBasedReportView

__author__ = 'Machine II'

@decorate(override_view(model=GrossProfitReportByArea, view=ViewActionEnum.Manage))
class GrossProfitByAreaView(AreaBasedReportView):
    def get_json_response(self, content, **kwargs):
        return super().get_json_response(content, **kwargs)
