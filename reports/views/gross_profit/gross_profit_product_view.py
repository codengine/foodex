from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from config.enums.view_action_enum import ViewActionEnum
from reports.models.gross_profit.gross_profit_product import GrossProfitReportByProduct
from reports.views.base.report_view import ProductBasedReportView

__author__ = 'Machine II'

@decorate(override_view(model=GrossProfitReportByProduct, view=ViewActionEnum.Manage))
class GrossProfitByProductView(ProductBasedReportView):
    def get_json_response(self, content, **kwargs):
        return super().get_json_response(content, **kwargs)
