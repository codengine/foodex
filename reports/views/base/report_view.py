from blackwidow.core.generics.views.list_view import GenericListView
from django.core.urlresolvers import reverse
from django.forms.forms import Form
from django.utils.datastructures import SortedDict
from django import forms
from blackwidow.core.models.manufacturers.manufacturer import Manufacturer
from blackwidow.sales.models.products.product import Product
from blackwidow.sales.models.products.product_group import ProductGroup
from config.enums.view_action_enum import ViewActionEnum
from foodex.models.clients.distributor import Distributor
from foodex.models.infrastructure.area import Area
from foodex.models.infrastructure.region import Region

__author__ = 'Machine II'

class GenericReportView(GenericListView):

    def get_template_names(self):
        return ['reports/embed-report-view.html']

    def get_wrapped_parameters(self, parameters):
        class DynamicForm(Form):
            pass

        form = DynamicForm()
        for p in parameters:
            form.fields[p['name']] = p['field']
        return form

    def get_report_parameters(self, **kwargs):
        parameters = SortedDict()
        parameters['G1'] = self.get_wrapped_parameters([])
        parameters['G2'] = self.get_wrapped_parameters([])
        parameters['G3'] = self.get_wrapped_parameters([])
        return parameters

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Foodex: Report"
        context['enable_map'] = False
        context['parameters'] = self.get_report_parameters(**kwargs)
        return context



class AreaBasedReportView(GenericReportView):
    def get_report_parameters(self, **kwargs):
        parameters = super().get_report_parameters(**kwargs)
        parameters['G2'] = self.get_wrapped_parameters(({
                                                            'name': 'region',
                                                            'field': forms.ModelChoiceField(queryset=Region.get_role_based_queryset(),
                                                                                            empty_label="All", label='Select Region',
                                                                                            required=False, widget=forms.Select(
                                                                    attrs={'class': 'select2', 'width': '220', 'data-child': 'area'}))
                                                        }, {
                                                            'name': 'area',
                                                            'field': forms.CharField(label='Select Area',
                                                                                     required=False, widget=forms.TextInput(
                                                                    attrs={'data-child': 'distributor','width': '220', 'class': 'select2-input', 'data-depends-on': 'region', 'data-depends-property': 'parent:id',
                                                                           'data-url': reverse(Area.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
                                                        }, ))
        parameters['G3'] = self.get_wrapped_parameters(({
                                                            'name': 'distributor',
                                                            'field': forms.CharField(label='Select Distributor', required=False,
                                                                                     widget=forms.TextInput(attrs={'class': 'select2-input',
                                                                                                                   'width': '220', 'data-depends-on': 'area', 'data-depends-property': 'assigned_to:id',
                                                                                                                   'data-url': reverse(Distributor.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
                                                        },))
        return parameters



class ProductBasedReportView(GenericReportView):
    def get_report_parameters(self, **kwargs):
        parameters = super().get_report_parameters(**kwargs)
        parameters['G2'] = self.get_wrapped_parameters(({
                        'name': 'category',
                        'field': forms.ModelChoiceField(queryset=ProductGroup.get_role_based_queryset(),
                                                        empty_label="Select Categories", label='Select Category',
                                                        required=False, widget=forms.Select(
                                attrs={'class': 'select2', 'width': '220'}))
                    },  ))
        parameters['G3'] = self.get_wrapped_parameters(({
                        'name': 'product',
                        'field': forms.CharField(label='Select Product', required=False,
                                                 widget=forms.TextInput(attrs={'class': 'select2-input',
                                                                               'width': '220', 'data-depends-on': 'category', 'data-depends-property': '_search_categories:id',
                                                                               'data-url': reverse(Product.get_route_name(action=ViewActionEnum.Manage)) + "?format=json&search=1&disable_pagination=1"}))
                    },))
        return parameters
