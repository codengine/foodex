from blackwidow.engine.decorators.utility import decorate
from blackwidow.engine.decorators.view_decorators import override_view
from config.enums.view_action_enum import ViewActionEnum
from reports.config.constants import REPORT_SERVER, PARAM_BEGIN, DEFAULT_AUTH
from reports.models.drilldown.drilldown_report import DrillDownReport
from reports.views.base.report_view import GenericReportView

__author__ = 'Machine II'

@decorate(override_view(model=DrillDownReport, view=ViewActionEnum.Manage))
class DrillDownReportView(GenericReportView):

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['saiku_root'] = REPORT_SERVER + PARAM_BEGIN + DEFAULT_AUTH
        context['title'] = 'Foodex: Drill-Down Report'
        return context

    def get_template_names(self):
        return ['reports/embed-report-drill-down.html']