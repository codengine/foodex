__author__ = 'Mahmud'


API_LOGIN_URL = '/api/login'
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    )
}

REST_FRAMEWORK = {
    'PAGINATE_BY': 9999999999,                 # Default to 10
    'PAGINATE_BY_PARAM': 'page_size',  # Allow client to override, using `?page_size=xxx`.
    'MAX_PAGINATE_BY': 9999999999,            # Maximum limit allowed when using `?page_size=xxx`.
    'EXCEPTION_HANDLER': 'blackwidow.core.api.exceptions.exception_handler.BWApiExceptionHandler'
}