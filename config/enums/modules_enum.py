from enum import Enum

__author__ = 'Mahmud'


class ModuleEnum(Enum):
    Reports = {
        'title': "Reporting",
        'route': 'reports',
        "icon": "&#xe097;",
        'order': 0
    }
    Analysis = {
        'title': "Analysis",
        'route': 'analysis',
        "icon": "î‚˜",
        'order': 5
    }
    Targets = {
        'title': "Targets",
        'route': 'kpi',
        "icon": "î‚¶",
        'order': 10
    }
    Alert = {
        'title': "Alerts",
        'route': 'communication',
        "icon": "&#xe0f4;",
        'order': 15
    }
    CRM = {
        'title': "CRM",
        'route': 'crm',
        "icon": "î„›",
        'order': 20
    }
    Infrastructure = {
        'title': 'Infrastructure',
        'route': 'infrastructure',
        "icon": "î‡�",
        'order': 25
    }
    Users = {
        'title': "Users & Clients",
        'route': 'users',
        "icon": "î�±",
        'order': 30
    }
    Execute = {
        'title': "Execute",
        'route': 'execute',
        "icon": "î‚¶",
        'order': 34
    }
    Administration = {
        'title': "Administration",
        'route': 'administration',
        "icon": "î‚ˆ",
        'order': 101 #Keep this as highest value
    }
    Inventory = {
        'title': "Inventory",
        'route': 'inventory',
        "icon": "î„›",
        'order': 40
    }
    ImportExport = {
        'title': "Import/Export",
        'route': 'import-export',
        "icon": "î„›",
        'order': 97
    }
    Settings = {
        'title': "Settings",
        'route': 'settings',
        "icon": "î‚�",
        'order': 98
    }
    Help = {
        'title': "Help",
        'route': 'help',
        "icon": "î‚“",
        'order': 99
    }
    Maintenance = {
        'title': "Maintenance",
        'route': 'maintenance',
        "icon": "î‚“",
        'order': 100
    }
    DeviceManager = {
        'title': "DeviceManager",
        'route': 'device-manager',
        "icon": "î‚“",
        'order': 100
    }