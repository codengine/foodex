__author__ = 'Mahmud'

from enum import Enum


class ViewActionEnum(Enum):
    Details = 'details'
    Edit = 'edit'
    Delete = 'delete'
    Create = 'create'
    Restore = 'restore'
    Manage = 'manage'
    Tree = 'tree'
    Tab = 'tab'
    Import = 'import'
    AdvancedImport = 'advanced_import'
    AdvancedExport = 'advanced_export'
    Export = 'export'
    Download = 'download'
    Reload = 'reload'
    BackUp = 'backup'
    Print = 'print'
    RunImporter = 'run_importer'
    Mutate = 'mutate'
    Approve = 'approve'
    Reject = 'reject'
    ResetPassword = "reset_password"

    PartialEdit = 'partial_edit'
    PartialDelete = 'partial_delete'
    PartialCreate = 'partial_create'

    PartialBulkAdd = 'bulk_add'
    PartialBulkRemove = 'bulk_remove'

    AdvancedEdit = 'advanced_edit'
    AdvancedDownload = 'advanced_download'

    def __str__(self):
        return self.value
