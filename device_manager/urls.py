from django.conf.urls import patterns, url

from blackwidow.core.generics.views.dashboard_view import GenericDashboardView

urlpatterns = patterns('',
                       # -------------------- Dashboards ---------------------------------#
                       url(r'^device-manager/dashboard', GenericDashboardView.as_view(), name="device-manager_dashboard"),
                       )

