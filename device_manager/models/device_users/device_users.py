from blackwidow.core.models import WebUser

__author__ = 'zia ahmed'

# @decorate(is_object_context, save_audit_log,
#           route(route='device-users', group='Device', module=ModuleEnum.DeviceManager, display_name='Device User'))
class DeviceUser(WebUser):

    class Meta:
        proxy = True


    @property
    def render_email(self):
        if self.email:
            return self.email
        else:
            return "N/A"

    @property
    def render_phone(self):
        if self.phone:
            return self.phone
        else:
            return "N/A"

    @classmethod
    def table_columns(cls):
        return 'code', 'name', 'render_email', 'render_phone', 'designation', 'created_by', 'last_updated_by', 'last_updated'
