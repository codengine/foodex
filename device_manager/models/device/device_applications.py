from collections import OrderedDict
from django.core.urlresolvers import reverse
from django.db import transaction
from django.utils.safestring import mark_safe
from blackwidow.core.models import DeviceApplication, ApplicationPackage, ImporterConfig, ImporterColumnConfig
from blackwidow.core.viewmodels.tabs_config import TabView
from blackwidow.engine.decorators.expose_model import expose_api
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, is_object_context, save_audit_log
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from blackwidow.engine.decorators.enable_import import enable_import

__author__ = 'zia ahmed'


@decorate(is_object_context, save_audit_log, expose_api('device-applications'),
          route(route='device-applications', group='Application', module=ModuleEnum.DeviceManager, display_name='Device Application'))
class DeviceApplication(DeviceApplication):

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)

    @property
    def render_code(self):
        return mark_safe("<a class='inline-link' href='" + reverse(self.get_route_name(ViewActionEnum.Details), kwargs={'pk': self.pk}) + "' >" + self.code + "</a>")

    @property
    def render_packages(self):
        if self.packages:
            return self.packages.all()

    @classmethod
    def table_columns(cls):
        return "code", "name", "created_by", "date_created:Created On", "last_updated"

    @property
    def details_config(self):
        dict = OrderedDict()
        dict['code'] = self.code
        dict['name'] = self.name
        dict['created_by'] = self.created_by
        dict['created_on'] = self.render_timestamp(self.date_created)
        dict['updated_by'] = self.last_updated_by
        dict['last_updated'] = self.render_timestamp(self.last_updated)

        return dict

    @property
    def tabs_config(self):

        tabs = [TabView(
            title='Package(s)',
            access_key='applicationpackage',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            related_model='device_manager.ApplicationPackage',
            property=self.render_packages

        )]

        return tabs




@decorate(is_object_context, save_audit_log, enable_import,
          route(route='application-packages', group='Application', module=ModuleEnum.DeviceManager, display_name='Application Package'))
class ApplicationPackage(ApplicationPackage):

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)

    @classmethod
    def table_columns(cls):
        return "code", "name", "created_by", "date_created:Created On", "last_updated"

    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create, ViewActionEnum.Edit, ViewActionEnum.Delete, ViewActionEnum.AdvancedImport]


    @property
    def details_config(self):
        dict = OrderedDict()
        dict['code'] = self.code
        dict['name'] = self.name
        dict['created_by'] = self.created_by
        dict['created_on'] = self.render_timestamp(self.date_created)
        dict['updated_by'] = self.last_updated_by
        dict['last_updated'] = self.render_timestamp(self.last_updated)

        return dict


    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        importer_config, created = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        importer_config.starting_row = 1
        importer_config.save(**kwargs)
        importer_config.columns.all().delete()
        importer_config.columns.clear()
        columns = [
            ImporterColumnConfig(column=0, column_name='Application Name', property_name='application_name', ignore=False),
            ImporterColumnConfig(column=1, column_name='Package Name', property_name='package_name', ignore=False),
            ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            # importer_config.starting_row = 1
            importer_config.columns.add(c)
        return importer_config


    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):

        with transaction.atomic():
            organization = user.organization
            application_name = data['0']
            package_name = data['1']

            apps = DeviceApplication.objects.filter(name__iexact=str(application_name))
            if apps.exists():
                app = apps.first()
            else:
                app = DeviceApplication()
                app.name = application_name
                app.organization = organization
                app.created_by = user
                app.last_updated_by = user
                app.save()

            packages = ApplicationPackage.objects.filter(name__iexact=str(package_name))
            if not packages.exists():
                package = ApplicationPackage()
                package.name = package_name
                package.application = app
                package.organization = organization
                package.created_by = user
                package.last_updated = user
                package.save()
            else:
                package = packages.first()
            return package
