from collections import OrderedDict
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
import uuid
from django.db import transaction
from django.utils.safestring import mark_safe
from blackwidow.core.models import Device, ImporterConfig, ImporterColumnConfig, \
    ContactAddress, DeviceSettings, DeviceLocation, DeviceWipeData, DeviceLock, DeviceUpdateStatus,\
    Role, EmailAddress, InfrastructureUserAssignment, ErrorLog
from blackwidow.core.models.common.country import Country
from blackwidow.core.models.common.division import Division
from blackwidow.core.models.common.educational_qualification import EducationalQualification
from blackwidow.core.models.common.phonenumber import PhoneNumber
from blackwidow.core.models.common.state import State
from blackwidow.core.models.common.upazila import Upazila
from blackwidow.core.viewmodels.tabs_config import TabView, TabViewAction
from blackwidow.engine.decorators.enable_import import enable_import
from blackwidow.engine.decorators.route_partial_routes import route, partial_route
from blackwidow.engine.decorators.utility import decorate, is_object_context, save_audit_log
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from device_manager.models.device.device_applications import DeviceApplication
from foodex.models import ServicePerson, Distributor
from foodex.models.infrastructure.routes import Route

__author__ = 'zia ahmed'

@decorate(is_object_context, save_audit_log, enable_import,
          route(route='devices', group='Device', module=ModuleEnum.DeviceManager, display_name='Organization Device'),
          partial_route(relation=['normal'], models=[DeviceApplication, DeviceSettings]))
class OrganizationDevice(Device):

    class Meta:
        proxy = True

    @classmethod
    def get_button_title(cls, button=ViewActionEnum.Details):
        if button == ViewActionEnum.Mutate:
            return "Lock Device"
        elif button == ViewActionEnum.AdvancedImport:
            return "Import"
        return button.value

    def save(self, organization=None, *args, **kwargs):
        with transaction.atomic():
            self.type = OrganizationDevice.__name__
            self_obj = super().save(*args, **kwargs)
            return self_obj

    def mutate_to(self, cls=None):
        with transaction.atomic():

            device_lock_object = self.device_lock

            if device_lock_object.lock_flag is True:
                device_lock_object.lock_flag = False
            else:
                device_lock_object.lock_flag = True
            device_lock_object.save()
            return self

    def approve_to(self, cls=None, *args, **kwargs):
        with transaction.atomic():

            wipe_data_object = self.wipe_data

            if wipe_data_object.wipe_data_flag is True:
                wipe_data_object.wipe_data_flag = False
            else:
                wipe_data_object.wipe_data_flag = True
            wipe_data_object.save()
            return self

    def reject_to(self, cls=None, *args, **kwargs):
        with transaction.atomic():

            device_location_object = self.device_location

            if device_location_object.location_flag is True:
                device_location_object.location_flag = False
            else:
                device_location_object.location_flag = True
            device_location_object.save()
            return self

    @property
    def render_applications(self):
        if self.applications:
            return self.applications.all()
        else:
            return "N/A"

    @property
    def render_assigned_user(self):
        if self.service_person:
            return mark_safe("<a class='inline-link' href='" + reverse(ServicePerson.get_route_name(ViewActionEnum.Print), kwargs={'pk': self.service_person_id}) + "' >" + self.service_person.code+': '+self.service_person.name + "</a>")
        else:
            return "N/A"

    @property
    def render_location(self):
        if self.device_location:
            return self.device_location
        else:
            return 'N/A'

    @property
    def render_organization(self):
        return self.organization

    @classmethod
    def table_columns(cls):
        return "code", "uuid", "name", "pin_code", "puk_code", "render_assigned_user", "sim_number", "created_by", "date_created:Created On", "last_updated"


    @classmethod
    def get_url_by_name(cls, name):
        if name == 'render_assigned_user':
            return '__search__service-persons'
        return None

    @classmethod
    def filter_query(cls,query_set,custom_search_fields=[]):
        from django.db.models import Q
        from functools import reduce
        import operator
        for key, value in custom_search_fields:

            if key.startswith("__search__service-persons"):
                try:
                    or_list = []
                    values = value.split(",")
                    for val in values:
                        or_list.append(Q(service_person__name=val))
                    device = Device.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=device.values_list('pk', flat=True))
                except:
                    pass
            elif key.startswith("_search_service-persons:id"):
                try:
                    or_list = []
                    values = value.split(",")
                    for val in values:
                        or_list.append(Q(manufacturer__pk=val))
                    product = Device.objects.filter(reduce(operator.or_, or_list))
                    query_set = query_set.filter(pk__in=product.values_list('pk', flat=True))
                except:
                    pass

        return query_set


    @property
    def details_config(self):
        dict = OrderedDict()
        dict['code'] = self.code
        dict['uuid'] = self.uuid
        dict['name'] = self.name
        dict['category'] = self.category
        dict['pin_code'] = self.pin_code
        dict['puk_code'] = self.puk_code
        dict['Settings'] = self.device_settings
        dict['Update Status'] = self.deviceupdatestatus
        dict['lock_status'] = self.device_lock
        dict['wipe_data_status'] = self.wipe_data
        dict['Pass Code'] = self.device_lock.pass_code
        dict['Location'] = self.render_location
        dict['manufacturer'] = self.manufacturer
        dict['assigned_user'] = self.render_assigned_user
        dict['sim_number'] = self.sim_number
        dict['sim_serial_number'] = self.sim_serial_number
        dict['organization'] = self.render_organization
        dict['created_by'] = self.created_by
        dict['created_on'] = self.render_timestamp(self.date_created)
        dict['last_updated'] = self.render_timestamp(self.last_updated)

        return dict


    @classmethod
    def get_manage_buttons(cls):
        return [ViewActionEnum.Create, ViewActionEnum.Details, ViewActionEnum.Edit, ViewActionEnum.Delete, ViewActionEnum.AdvancedImport]


    def details_link_config(self, **kwargs):
        return [

            dict(
                name='Lock Device',
                action='mutate',
                icon='fbx-rightnav-tick',
                ajax='0',
                classes='disabled' if self.is_locked else 'manage-action all-action confirm-action',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Mutate),
                parent=None
            ),
            dict(
                name='Wipe Device Data',
                action='approve',
                icon='fbx-rightnav-delete',
                ajax='0',
                classes='disabled' if self.is_locked else 'manage-action all-action confirm-action',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Approve),
                parent=None
            ),
            dict(
                name='Update Map',
                action='reject',
                icon='fbx-rightnav-update-map',
                ajax='0',
                classes='disabled' if self.is_locked else 'manage-action all-action map-update-request',
                url_name=self.__class__.get_route_name(action=ViewActionEnum.Reject),
                parent=None
            )
        ]



    @property
    def tabs_config(self):

        tabs = list()

        tabs.append(TabView(
            title='Allowed Application(s)',
            access_key='allowedapplication',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            property=self.applications,
            add_more_queryset=DeviceApplication.objects.filter().exclude(pk__in=self.applications.values_list('id', flat=True)),
            related_model=DeviceApplication,
            actions=[
                TabViewAction(
                    title='Add',
                    action='add',
                    icon='icon-plus',
                    route_name=DeviceApplication.get_route_name(action=ViewActionEnum.PartialBulkAdd, parent=self.__class__.__name__.lower()),
                    css_class='manage-action load-modal fis-plus-ico',

                ),
                TabViewAction(
                    title='Edit',
                    action='partial-edit',
                    icon='icon-plus',
                    route_name=DeviceApplication.get_route_name(action=ViewActionEnum.PartialEdit, parent=self.__class__.__name__.lower()),
                    css_class='manage-action load-modal partial-edit-action fis-create-ico',
                    # enable_wide_popup=True
                ),
                TabViewAction(
                    title='Remove',
                    action='partial-remove',
                    icon='icon-remove',
                    route_name=DeviceApplication.get_route_name(action=ViewActionEnum.PartialBulkRemove, parent=self.__class__.__name__.lower()),
                    css_class='manage-action delete-item fis-remove-ico'
                )
            ]))

        tabs.append(TabView(
            title='Installed Fieldbuzz Application(s)',
            access_key='installedfieldbuzzapplication',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.INVERTED,
            related_model='core.InstalledFieldbuzzApplication',
            property=self.installedfieldbuzzapplication_set,
            queryset=self.installedfieldbuzzapplication_set

        ))

        tabs.append(TabView(
            title='Device Settings',
            access_key='devicesettings',
            route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
            relation_type=ModelRelationType.NORMAL,
            property=DeviceSettings.objects.filter(device=self),
            related_model=DeviceSettings,
            actions=[
                TabViewAction(
                    title='Edit',
                    action='partial-edit',
                    icon='icon-plus',
                    route_name=DeviceSettings.get_route_name(action=ViewActionEnum.PartialEdit, parent=self.__class__.__name__.lower()),
                    css_class='manage-action load-modal partial-edit-action fis-create-ico',
                    # enable_wide_popup=True
                )
            ]))

        return tabs


    @classmethod
    def importer_config(cls, organization=None, **kwargs):
        importer_config, created = ImporterConfig.objects.get_or_create(model=cls.__name__, organization=organization)
        importer_config.starting_row = 1
        importer_config.save(**kwargs)
        importer_config.columns.all().delete()
        importer_config.columns.clear()
        columns = [
            ImporterColumnConfig(column=0, column_name='Employee Name', property_name='user_name', ignore=False),
            ImporterColumnConfig(column=1, column_name='Phone Number',property_name='sim_number',ignore=False),
            ImporterColumnConfig(column=2, column_name='Email',property_name='email',ignore=False),
            ImporterColumnConfig(column=3, column_name='Dealer Code', ignore=False),
            ImporterColumnConfig(column=4, column_name='Distributor',property_name='distributor',ignore=False),
            ImporterColumnConfig(column=5, column_name='Date of birth',property_name='date-of-birth',ignore=False),
            ImporterColumnConfig(column=6, column_name='Sim IMEI',property_name='uuid',ignore=False),
            ImporterColumnConfig(column=7, column_name='Sim serial number',property_name='sim_serial_number',ignore=False),
            ]
        for c in columns:
            c.save(organization=organization, **kwargs)
            # importer_config.starting_row = 1
            importer_config.columns.add(c)
        return importer_config

    @classmethod
    def import_item(cls, config, sorted_columns, data, user=None, **kwargs):
        with transaction.atomic():
            organization = user.organization
            #Create/Update service person
            user_name = data['0']
            phone_number = data['1']
            email = data['2']
            date_of_birth = data['5']
            date_of_birth = date_of_birth.replace('.', '/')

            device_uuid = data['6']
            device_serial_number = data['7']
            try:
                service_person = ServicePerson.objects.filter(phones__phone=phone_number)
                if service_person.exists():
                    db_service_person = ServicePerson.objects.filter(phones__phone=phone_number).first()
                    ##delete existing address, phone numbers, email etc
                    db_service_person.addresses.all().delete()
                    db_service_person.emails.all().delete()
                    db_service_person.phones.all().delete()
                else:
                    db_service_person = ServicePerson()
                    db_service_person.tsync_id = uuid.uuid4()
                if User.objects.filter(username=phone_number).exists():
                    sp_user = User.objects.filter(username=phone_number).first()
                else:
                    sp_user = User()
                    sp_user.username = phone_number
                    sp_user.set_password('as1234')
                    sp_user.save()

                #Lets setup a dummy address
                sp_addresses = ContactAddress()
                sp_addresses.street = 'Gulshan'
                sp_addresses.city = 'Dhaka'
                sp_addresses.province = 'Gulshan'
                sp_addresses.country = Country.objects.all.first()
                sp_addresses.division = Division.objects.filter(name='Dhaka').first()
                sp_addresses.state = State.objects.filter(name='Dhaka').first() #District
                sp_addresses.is_primary = False
                sp_addresses.save()

                sp_email_address = EmailAddress()
                sp_email_address.organization = organization
                sp_email_address.email = email
                sp_email_address.save()

                sp_phones = PhoneNumber()
                sp_phones.phone = phone_number
                sp_phones.is_primary = True
                sp_phones.is_own = True
                sp_phones.save()

                sp_education = EducationalQualification()
                sp_education.degree = 'N/A'
                sp_education.board = 'N/A'
                sp_education.institute = 'N/A'
                sp_education.result = 'N/A'
                sp_education.year = 'N/A'
                sp_education.save()

                if not db_service_person.user:
                    db_service_person.user = sp_user
                db_service_person.organization = organization
                db_service_person.is_super = False
                db_service_person.name = user_name
                db_service_person.role = Role.objects.filter(name=ServicePerson.__name__)[0]
                db_service_person.device_id = device_uuid
                db_service_person.date_of_birth = date_of_birth
                db_service_person.education = sp_education
                db_service_person.save()

                db_service_person.addresses.add(sp_addresses)
                db_service_person.emails.add(sp_email_address)
                db_service_person.phones.add(sp_phones)
            except Exception as exp:
                ErrorLog.log(exp, phone_number)
                pass
            #Search Distributor or exit
            distributor_code = data['3']
            distributor = data['4']

            #Lets assign the employee under the distributor, but mind it, you still need to assign routes
            distributor_objects = Distributor.objects.filter(code=distributor_code)
            if distributor_objects.exists():
                try:
                    distributor = distributor_objects.first()
                    if distributor.assigned_employee.filter(role__name=ServicePerson.__name__).exists():
                        sp_assignment = distributor.assigned_employee.filter(role__name=ServicePerson.__name__).first()
                    else:
                        sp_assignment = InfrastructureUserAssignment()
                        sp_assignment.role = Role.objects.filter(name=ServicePerson.__name__)[0]
                        sp_assignment.organization = user.organization
                        sp_assignment.save()
                        distributor.assigned_employee.add(sp_assignment)
                    sp_assignment.users.add(db_service_person)

                    #Now the time to assign this sp to all the routes (routes that are under the distributor)
                    unassigned_routes_of_dist = Route.objects.filter(parent_client=distributor).exclude(pk__in=db_service_person.assigned_units.values_list('id', flat=True))
                    if unassigned_routes_of_dist.exists():
                        for unassigned_route in unassigned_routes_of_dist:
                            db_service_person.assigned_units.add(unassigned_route)
                except Exception as exp:
                    ErrorLog.log(exp,distributor_code)
                    pass
            else:
                print('could not find distributor')
                # ErrorLog.log('could not find distributor '+distributor_code)

            # Create Device Category
            category_name = 'Samsung'
            #Create Device
            device_objects = OrganizationDevice.objects.filter(uuid=device_uuid)
            if not device_objects.exists():
                device_object = OrganizationDevice()
                device_object.organization = organization
                device_object.created_by = user
                device_object.last_updated_by = user
                device_object.uuid = device_uuid
                device_object.name =category_name
                device_object.sim_serial_number = device_serial_number
                device_object.sim_number = '0'
                device_object.pin_code = '0'
                device_object.puk_code = '0'
                # device_object.category = category_object
                device_object.assigned_user = None
                device_object.service_person = db_service_person
                device_object.save()

                device_settings_obj = DeviceSettings()
                device_settings_obj.device = device_object
                device_settings_obj.screen_orientation = 2
                device_settings_obj.save()

                # Device Location
                device_location_obj = DeviceLocation()
                device_location_obj.device = device_object
                device_location_obj.save()

                # Device wipe data
                wipe_data_obj = DeviceWipeData()
                wipe_data_obj.device = device_object
                wipe_data_obj.save()

                # Device lock
                lock_obj = DeviceLock()
                lock_obj.device = device_object
                lock_obj.save()

                # Device update status
                update_status_obj = DeviceUpdateStatus()
                update_status_obj.device = device_object
                update_status_obj.save()

                # Assigned allow applications
                # allow_applications = DeviceApplication.objects.filter(organization=organization)
                allow_applications = list(db_service_person.role.allowed_apps.all())
                if len(allow_applications) > 0:
                    device_object.applications.add(*allow_applications)

                return device_object.pk



    # @classmethod
    # def exporter_config(cls, organization=None, **kwargs):
    #     if not organization:
    #         organization = Organization.objects.all().first()
    #     exporter_configs = ExporterConfig.objects.filter(model=cls.__name__, organization=organization)
    #     if not exporter_configs.exists():
    #         exporter_config = ExporterConfig()
    #         exporter_config.organization = organization
    #         exporter_config.starting_row = 1
    #         exporter_config.save(**kwargs)
    #     else:
    #         for e in exporter_configs:
    #             e.delete()
    #         exporter_config = ExporterConfig()
    #         exporter_config.organization = organization
    #         exporter_config.save(**kwargs)
    #
    #     columns = [
    #         ExporterColumnConfig(column=0, column_name='Employee Name', property_name='user_name', ignore=False),
    #         ExporterColumnConfig(column=1, column_name='Designation', property_name='designation', ignore=False),
    #         ExporterColumnConfig(column=2, column_name='Street/village', property_name='street_village', ignore=False),
    #         ExporterColumnConfig(column=3, column_name='Union/city', property_name='union_city', ignore=False),
    #         ExporterColumnConfig(column=4, column_name='Province/upzila', property_name='province_upazilla', ignore=False),
    #         ExporterColumnConfig(column=5, column_name='District', property_name='district_name', ignore=False),
    #         ExporterColumnConfig(column=6, column_name='Division', property_name='division_name', ignore=False),
    #         ExporterColumnConfig(column=7, column_name='Device UUID', property_name='uuid', ignore=False),
    #         ExporterColumnConfig(column=8, column_name='Device Name', property_name='device_name', ignore=False),
    #         ExporterColumnConfig(column=9, column_name='Sim Serial Number', property_name='sim_serial_number', ignore=False),
    #         ExporterColumnConfig(column=10, column_name='Sim Number', property_name='sim_number', ignore=False),
    #         ExporterColumnConfig(column=11, column_name='PIN Code', property_name='pin_code', ignore=False),
    #         ExporterColumnConfig(column=12, column_name='PUK Code', property_name='puk_code', ignore=False),
    #         ExporterColumnConfig(column=13, column_name='Screen Orientation', property_name='screen_orientation', ignore=False),
    #         ExporterColumnConfig(column=14, column_name='Device Category', property_name='device_category', ignore=False),
    #         ]
    #
    #     for c in columns:
    #         c.save(organization=organization, **kwargs)
    #         exporter_config.columns.add(c)
    #     return exporter_config
    #
    # def export_item(self, workbook=None, columns=None, row_number=None, **kwargs):
    #     # for column in columns:
    #     #     workbook.cell(row=row_number, column=column.column + 1).value = str(getattr(self, column.property_name))
    #     return self.pk, row_number + 1
    #
    # @classmethod
    # def finalize_export(cls, workbook=None, row_number=None, query_set=None, **kwargs):
    #     #file_name = 'Product_price_config' + (Clock.now().strftime('%d-%m-%Y'))
    #     return workbook
    #
    # @classmethod
    # def initialize_export(cls, workbook=None, columns=None, row_number=None, query_set=None, **kwargs):
    #     for column in columns:
    #         workbook.cell(row=1, column=column.column + 1).value = column.column_name
    #     return workbook, row_number