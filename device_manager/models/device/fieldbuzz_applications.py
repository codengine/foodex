from collections import OrderedDict
from django.db import transaction
from blackwidow.core.models import FieldBuzzApplication
from blackwidow.core.viewmodels.tabs_config import TabView, TabViewAction
from blackwidow.engine.decorators.route_partial_routes import route, partial_route
from blackwidow.engine.decorators.utility import decorate, is_object_context, save_audit_log
from config.enums import ViewActionEnum
from config.enums.modules_enum import ModuleEnum
from config.enums.tab_view_enum import ModelRelationType
from blackwidow.core.models.roles.role import Role

__author__ = 'zia ahmed'

@decorate(is_object_context, save_audit_log,
          route(route='fieldbuzz-applications', group='Application', module=ModuleEnum.DeviceManager, display_name='Fieldbuzz Applicaiton'),
          partial_route(relation='normal', models=[Role]))
class FieldbuzzApplication(FieldBuzzApplication):

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        with transaction.atomic():
            return super().save(*args, **kwargs)

    @property
    def render_apk_status(self):
        if self.status == self.INSTALL:
            return "Install"
        elif self.status == self.UPDATE:
            return "Update"

    @classmethod
    def table_columns(cls):
        return "code", "name", "package_name", "version_name", "version_code", "status:Apk Status", "created_by", "date_created:Created On", "last_updated"

    @property
    def details_config(self):
        dict = OrderedDict()
        dict['code'] = self.code
        dict['name'] = self.name
        dict['Package Name'] = self.package_name
        dict['Version Name'] = self.version_name
        dict['Version Code'] = self.version_code
        # dict['Organization'] = self.organization
        dict['Apk Status'] = self.render_apk_status
        dict['created_by'] = self.created_by
        dict['created_on'] = self.render_timestamp(self.date_created)
        dict['updated_by'] = self.last_updated_by
        dict['last_updated'] = self.render_timestamp(self.last_updated)

        return dict


    @property
    def tabs_config(self):
        tabs = [
            TabView(
                title='Role Assignment',
                access_key='roleassignforfieldbuzzapp',
                route_name=self.__class__.get_route_name(action=ViewActionEnum.Tab),
                relation_type=ModelRelationType.NORMAL,
                property=self.role_set,
                add_more_queryset=Role.objects.filter(deleted_level=0).exclude(pk__in=self.role_set.all().values_list('id', flat=True)),
                related_model=Role,
                actions=[
                    TabViewAction(
                        title='Add',
                        action='add',
                        icon='icon-plus',
                        route_name=Role.get_route_name(action=ViewActionEnum.PartialBulkAdd, parent=self.__class__.__name__.lower()),
                        css_class='manage-action load-modal fis-plus-ico',

                        ),
                    TabViewAction(
                        title='Remove',
                        action='partial-remove',
                        icon='icon-remove',
                        route_name=Role.get_route_name(action=ViewActionEnum.PartialBulkRemove, parent=self.__class__.__name__.lower()),
                        css_class='manage-action delete-item fis-remove-ico'
                    )
                ])
        ]
        return tabs