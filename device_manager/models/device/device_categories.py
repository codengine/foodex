from collections import OrderedDict
from django.db import transaction
from blackwidow.core.models import DeviceCategory
from blackwidow.engine.decorators.route_partial_routes import route
from blackwidow.engine.decorators.utility import decorate, is_object_context, save_audit_log, is_role_context
from config.enums.modules_enum import ModuleEnum

__author__ = 'zia ahmed'

@decorate(is_object_context, save_audit_log,
          route(route='device-categories', group='Device', module=ModuleEnum.DeviceManager, display_name='Device Category'))
class OrganizationDeviceCategory(DeviceCategory):

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        with transaction.atomic():
            super().save(*args, **kwargs)

    @classmethod
    def table_columns(cls):
        return "code", "name", "created_by", "date_created:Created On", "last_updated"

    @property
    def details_config(self):
        dict = OrderedDict()
        dict['code'] = self.code
        dict['name'] = self.name
        dict['created_by'] = self.created_by
        dict['created_on'] = self.render_timestamp(self.date_created)
        dict['updated_by'] = self.last_updated_by
        dict['last_updated'] = self.render_timestamp(self.last_updated)

        return dict