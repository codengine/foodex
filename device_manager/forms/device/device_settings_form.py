from blackwidow.core.mixins.fieldmixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin import GenericFormMixin
from blackwidow.core.models import DeviceSettings
from django import forms
__author__ = 'zia ahmed'


class DeviceSettingsForm(GenericFormMixin):

    def __init__(self, data=None, files=None, instance=None, prefix='', form_header='',  **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, form_header=form_header,  **kwargs)

    class Meta(GenericFormMixin.Meta):
        model = DeviceSettings
        fields = ['gps', 'wifi', 'bluetooth', 'mobile_data']
        # labels = {
        #     'name': 'Package Name'
        # }
