from django.db import transaction
from blackwidow.core.forms import FieldbuzzApplicationForm
from device_manager.models import FieldbuzzApplication

__author__ = 'zia ahmed'


class FieldBuzzApplicationForm(FieldbuzzApplicationForm):

    def __init__(self, data=None, files=None, instance=None, prefix='', form_header='',  **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, form_header=form_header,  **kwargs)

    class Meta(FieldbuzzApplicationForm.Meta):
        model = FieldbuzzApplication
        fields = ['name', 'package_name', 'version_name', 'version_code', 'status']

        def save(self, commit=True):
            with transaction.atomic():
                return super().save(commit)
