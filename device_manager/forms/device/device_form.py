from django.db import transaction
from blackwidow.core.forms import DeviceForm
from blackwidow.core.mixins.fieldmixin import GenericModelChoiceField
from device_manager.models import OrganizationDeviceCategory
from device_manager.models import OrganizationDevice, DeviceUser
from django import forms
from foodex.models import ServicePerson

__author__ = 'zia ahmed'


class OrganizationDeviceForm(DeviceForm):

    def __init__(self, data=None, files=None, instance=None, prefix='', form_header='',  **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, form_header=form_header,  **kwargs)
        self.fields['category'] = GenericModelChoiceField(widget=forms.Select(attrs={'class': 'select2'}), queryset=OrganizationDeviceCategory.objects.all(), initial=instance.category if instance is not None else None)
        if instance is not None:
            self.fields['service_person'] = GenericModelChoiceField(widget=forms.Select(attrs={'class': 'select2'}), queryset=ServicePerson.objects.all(), initial=instance.service_person if instance is not None else None)
        else:
            self.fields['service_person'] = GenericModelChoiceField(widget=forms.Select(attrs={'class': 'select2'}), queryset=ServicePerson.objects.filter(device=None), initial=instance.service_person if instance is not None else None)

    class Meta(DeviceForm.Meta):
        model = OrganizationDevice
        fields = ['uuid', 'name', 'sim_serial_number', 'sim_number', 'pin_code', 'puk_code']

    # def save(self, commit=True):
    #     with transaction.atomic():
    #         super().save(commit)
