from blackwidow.core.forms.device.device_category_form import DeviceCategoryForm
from blackwidow.core.mixins.formmixin import GenericFormMixin
from device_manager.models import OrganizationDeviceCategory

__author__ = 'zia ahmed'


class OrganizationDeviceCategoryForm(GenericFormMixin):

    def __init__(self, data=None, files=None, instance=None, prefix='', form_header='',  **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, form_header=form_header,  **kwargs)

    class Meta(DeviceCategoryForm.Meta):
        model = OrganizationDeviceCategory
        fields = ['name', ]

