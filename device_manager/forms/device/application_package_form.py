from blackwidow.core.mixins.fieldmixin import GenericModelChoiceField
from blackwidow.core.mixins.formmixin import GenericFormMixin
from device_manager.models.device.device_applications import DeviceApplication, ApplicationPackage
from django import forms
__author__ = 'zia ahmed'


class ApplicationPackageForm(GenericFormMixin):

    def __init__(self, data=None, files=None, instance=None, prefix='', form_header='',  **kwargs):
        super().__init__(data=data, files=files, instance=instance, prefix=prefix, form_header=form_header,  **kwargs)
        self.fields['application'] = GenericModelChoiceField(widget=forms.Select(attrs={'class': 'select2'}), queryset=DeviceApplication.objects.all(), initial=instance.application if instance is not None else None)


    class Meta(GenericFormMixin.Meta):
        model = ApplicationPackage
        fields = ['name', 'application']
        labels = {
            'name': 'Package Name'
        }
