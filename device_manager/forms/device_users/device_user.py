from blackwidow.core.forms import WebUserForm
from device_manager.models import DeviceUser

__author__ = 'zia ahmed'


class DeviceUserForm(WebUserForm):

    class Meta(WebUserForm.Meta):
        model = DeviceUser
