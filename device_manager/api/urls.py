from django.conf.urls import patterns, url
from device_manager.api.views.allowed_apps_view import AllowedAppsView
from device_manager.api.views.check_device_status import CheckStatusView
from device_manager.api.views.device_lock_view import DeviceLockView
from device_manager.api.views.device_settings import DeviceSettingsView
from device_manager.api.views.device_update_status_view import UpdateDeviceStatusView
from device_manager.api.views.fieldbuzz_apps_view import FieldBuzzAppsView
from device_manager.api.views.initial_device_settings_views import InitialDeviceSettingsView
from device_manager.api.views.installed_apps_view import InstalledAppsView
from device_manager.api.views.location_view import GetLocationView

urlpatterns = patterns('',
                       url(r'^api/device-initial-settings$', InitialDeviceSettingsView.as_view(), name='initial-settings'),
                       url(r'^api/device-allowed-apps$', AllowedAppsView.as_view(), name='allowed-apps'),
                       url(r'^api/device-settings$', DeviceSettingsView.as_view(), name='settings'),
                       url(r'^api/check-device-status$', CheckStatusView.as_view(), name='check-status'),
                       url(r'^api/update-device-location$', GetLocationView.as_view(), name='get-location'),
                       url(r'^api/lock-device$', DeviceLockView.as_view(), name='lock-device'),
                       url(r'^api/device-fieldbuzz-apps$', FieldBuzzAppsView.as_view(), name='fieldbuzz-apps'),
                       url(r'^api/device-update-status$', UpdateDeviceStatusView.as_view(), name='update-device-status'),
                       url(r'^api/device-installed_apps$', InstalledAppsView.as_view(), name='installed-apps'),
                       )




