from crequest.middleware import CrequestMiddleware
from rest_framework import exceptions, HTTP_HEADER_ENCODING
from rest_framework.authentication import TokenAuthentication, get_authorization_header
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import APIException
from blackwidow.core.managers.contextmanager import ContextManager
from blackwidow.core.models import Device
from blackwidow.core.models.users.user import ConsoleUser
from django.contrib.auth.models import User


__author__ = 'zia ahmed'


class DeviceUuidAuthentication(TokenAuthentication):

    def authenticate(self, request):
        if request.META.get('HTTP_UUID'):
            uuid = self.get_uuid_authorization_header(request).split()
            system_user = User.objects.filter(username='blackwidow')

            if not uuid or len(uuid) == 0 or uuid[0].lower() != b'device-uuid':
                return None

            if len(uuid) <= 1:
                msg = 'Invalid uuid header. No credentials provided.'
                raise exceptions.AuthenticationFailed(msg)
            if len(uuid) > 2:
                msg = 'Invalid uuid header. Uuid string should not contain spaces. '
                raise exceptions.AuthenticationFailed(msg)

            if not system_user.exists():
                msg = 'No system user available. '
                raise exceptions.AuthenticationFailed(msg)

            try:
                token, created = Token.objects.get_or_create(user=system_user.first())
                auth_user, token = self.authenticate_credentials(token.key)
                c_user = ConsoleUser.objects.get(user=auth_user)
                device_object = Device.objects.filter(uuid=uuid[1])
                if not device_object.exists():
                    msg = 'Invalid Device. ' + str(uuid[1].decode(HTTP_HEADER_ENCODING)) + " uuid doesn't exists in System."
                    raise exceptions.AuthenticationFailed(msg)
                ContextManager.initialize_context(request, {'user': c_user, 'org': c_user.organization, 'device': device_object})
                CrequestMiddleware.set_request(request)

            except Exception as exp:
                raise APIException(str(exp))

            return (auth_user, token)

        else:
            auth = get_authorization_header(request).split()

            if not auth or len(auth) == 0 or auth[0].lower() != b'token':
                return None

            if len(auth) <= 1:
                msg = 'Invalid token header. No credentials provided.'
                raise exceptions.AuthenticationFailed(msg)
            elif len(auth) > 2:
                msg = 'Invalid token header. Token string should not contain spaces.'
                raise exceptions.AuthenticationFailed(msg)

            try:
                auth_user, token = self.authenticate_credentials(auth[1])
                c_user = ConsoleUser.objects.get(user=auth_user)
                ContextManager.initialize_context(request, {'user': c_user, 'org': c_user.organization})
                CrequestMiddleware.set_request(request)
            except Exception as exp:
                raise APIException(str(exp))
            return (auth_user, token)

    def get_uuid_authorization_header(self, request):
        uuid = request.META.get('HTTP_UUID', b'')
        if isinstance(uuid, type('')):
            # Work around django test client oddness
            uuid = uuid.encode(HTTP_HEADER_ENCODING)
        return uuid