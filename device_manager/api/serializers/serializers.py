from rest_framework import serializers
from blackwidow.core.models import DeviceSettings, Device, FieldBuzzApplication, DeviceLocation, DeviceLock, \
    InstalledFieldbuzzApplication
from blackwidow.core.models.device.applications import ApplicationPackage, DeviceApplication
from device_manager.models import DeviceUser


class ApplicationPackageSerializer(serializers.ModelSerializer):

    class Meta:
        model = ApplicationPackage
        fields = ('name',)


class DeviceApplicationSerializer(serializers.ModelSerializer):

    packages = ApplicationPackageSerializer(many=True, read_only=True)

    class Meta:
        model = DeviceApplication
        fields = ('name', 'packages')


class DeviceSettingsSerializer(serializers.ModelSerializer):

    class Meta:
        model = DeviceSettings
        fields = ('gps', 'wifi', 'bluetooth', 'mobile_data', 'screen_orientation', 'last_updated')


class DeviceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Device
        fields = ('uuid', 'name', 'manufacturer', 'sim_serial_number', 'sim_number', 'pin_code', 'puk_code')


class DeviceUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = DeviceUser
        fields = ('name', 'phone')


class LocationSerializer(serializers.ModelSerializer):
    longitude = serializers.DecimalField(max_digits=20, decimal_places=10, coerce_to_string=False)
    latitude = serializers.DecimalField(max_digits=20, decimal_places=10, coerce_to_string=False)
    accuracy = serializers.DecimalField(max_digits=20, decimal_places=10, coerce_to_string=False)

    class Meta:
        model = DeviceLocation
        fields = ('latitude', 'longitude', 'accuracy', 'last_updated')

        extra_kwargs = {
            'latitude': {'required': True},
            'longitude': {'required': True},
            'accuracy': {'required': True},
            }


class DeviceLockSerializer(serializers.ModelSerializer):

    class Meta:
        model = DeviceLock
        fields = ('pass_code', 'lock_flag', 'last_updated')


class FieldBuzzAppSerializer(serializers.ModelSerializer):

    class Meta:
        model = FieldBuzzApplication
        fields = ('name', 'package_name', 'version_name', 'version_code', 'status', 'download', 'hashcode', 'relative_url',)


class InstalledAppsSerializer(serializers.ModelSerializer):

    class Meta:
        model = InstalledFieldbuzzApplication
        fields = ('application_name', 'package_name', 'version_name', 'version_code')
        extra_kwargs = {
            'application_name': {'required': True},
            'package_name': {'required': True},
            'version_name': {'required': True},
            'version_code': {'required': True},
            }


