from rest_framework.response import Response
from rest_framework.views import APIView
from blackwidow.core.api.athorization import IsAuthorized
from blackwidow.core.api.renderers import GenericJsonRenderer
from blackwidow.core.models import Device, DeviceSettings, FieldBuzzApplication, DeviceLocation, DeviceWipeData, \
    DeviceLock
from device_manager.api.athorization.device_uuid_authentication import DeviceUuidAuthentication
from device_manager.api.serializers.serializers import DeviceSerializer, DeviceApplicationSerializer, \
    DeviceSettingsSerializer, DeviceUserSerializer, FieldBuzzAppSerializer
from blackwidow.core.models.log.logs import ApiCallLog

__author__ = 'zia ahmed'


class InitialDeviceSettingsView(APIView):
    authentication_classes = (DeviceUuidAuthentication, )
    permission_classes = (IsAuthorized,)
    renderer_classes = (GenericJsonRenderer, )

    def finalize_response(self, request, response, *args, **kwargs):
        ApiCallLog.log(request=request, response=response)
        return super().finalize_response(request, response, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        data = dict()
        device_obj = request.c_device[0]

        try:
            serialized_device = DeviceSerializer(device_obj)
            serialized_apps = DeviceApplicationSerializer(device_obj.applications.all(), many=True)
            serialized_settings = DeviceSettingsSerializer(DeviceSettings.objects.get(device=device_obj))
            serialized_user = DeviceUserSerializer(device_obj.assigned_user)

            fieldbuzz_applications = FieldBuzzApplication.objects.filter(organization=device_obj.organization,deleted_level=0)

            if device_obj.service_person:
                user_role = device_obj.service_person.role
                fieldbuzz_applications = FieldBuzzApplication.objects.filter(organization=device_obj.organization, pk__in=user_role.fieldbuzz_apps.values_list('id', flat=True), deleted_level=0)
            serialized_fieldbuzz_apps = FieldBuzzAppSerializer(fieldbuzz_applications, many=True)

            my_list = []
            for app in fieldbuzz_applications:
                my_list.append(app.last_updated)

            updated_time = 0
            if len(my_list) > 0:
                updated_time = max(my_list)

            settings_timestamp = DeviceSettings.objects.get(device=device_obj).last_updated
            location_timestamp = DeviceLocation.objects.get(device=device_obj).last_updated
            wipe_data_timestamp = DeviceWipeData.objects.get(device=device_obj).last_updated
            device_lock_timestamp = DeviceLock.objects.get(device=device_obj).last_updated

            status = {
                'settings': settings_timestamp,
                'allowed_apps': device_obj.app_assignment_time,
                'fieldbuzz_apps': updated_time,
                'location': location_timestamp,
                'wipe_data': wipe_data_timestamp,
                'device_lock': device_lock_timestamp,
                }

            data['configurations'] = {
                'allowed_apps': serialized_apps.data,
                'fieldbuzz_apps': serialized_fieldbuzz_apps.data,
                'settings': serialized_settings.data,
                'device': serialized_device.data,
                'user': serialized_user.data,
                'status': status,
                }
            return Response(data)

        except Device.DoesNotExist:
            return Response({'message': 'Invalid Request', 'status': False})

