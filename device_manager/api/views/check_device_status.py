from django.core.urlresolvers import Resolver404
from rest_framework.views import APIView
from blackwidow.core.api.athorization import IsAuthorized
from blackwidow.core.api.renderers import GenericJsonRenderer
from blackwidow.core.models import DeviceSettings, DeviceLocation, DeviceWipeData, DeviceLock, FieldBuzzApplication
from device_manager.api.athorization.device_uuid_authentication import DeviceUuidAuthentication
from rest_framework.response import Response
from device_manager.api.serializers.serializers import DeviceApplicationSerializer
from blackwidow.core.models.log.logs import ApiCallLog

__author__ = 'zia ahmed'

class CheckStatusView(APIView):
    authentication_classes = (DeviceUuidAuthentication, )
    permission_classes = (IsAuthorized,)
    renderer_classes = (GenericJsonRenderer, )

    def finalize_response(self, request, response, *args, **kwargs):
        ApiCallLog.log(request=request, response=response)
        return super().finalize_response(request, response, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        data = dict()
        device_obj = request.c_device[0]
        try:
            settings_timestamp = DeviceSettings.objects.get(device=device_obj).last_updated
            location_timestamp = DeviceLocation.objects.get(device=device_obj).last_updated
            wipe_data_timestamp = DeviceWipeData.objects.get(device=device_obj).last_updated
            device_lock_timestamp = DeviceLock.objects.get(device=device_obj).last_updated

            fieldbuzz_apps = FieldBuzzApplication.objects.filter(organization=device_obj.organization, deleted_level=0)
            if device_obj.service_person:
                user_role = device_obj.service_person.role
                fieldbuzz_apps = FieldBuzzApplication.objects.filter(organization=device_obj.organization, pk__in=user_role.fieldbuzz_apps.values_list('id', flat=True), deleted_level=0)

            my_list = []
            for app in fieldbuzz_apps:
                my_list.append(app.last_updated)

            updated_time = 0
            if len(my_list) > 0:
                updated_time = max(my_list)

            data['settings'] = settings_timestamp
            data['allowed_apps'] = device_obj.app_assignment_time
            data['location'] = location_timestamp
            data['wipe_data'] = wipe_data_timestamp
            data['device_lock'] = device_lock_timestamp
            data['fieldbuzz_apps'] = updated_time

            return Response(data)

        except Resolver404 as exp:
            pass