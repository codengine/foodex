from django.core.urlresolvers import Resolver404
from rest_framework.views import APIView
from blackwidow.core.api.athorization import IsAuthorized
from blackwidow.core.api.renderers import GenericJsonRenderer
from blackwidow.core.models import DeviceSettings
from device_manager.api.athorization.device_uuid_authentication import DeviceUuidAuthentication
from rest_framework.response import Response
from device_manager.api.serializers.serializers import DeviceApplicationSerializer, DeviceSettingsSerializer
from blackwidow.core.models.log.logs import ApiCallLog

__author__ = 'zia ahmed'

class DeviceSettingsView(APIView):
    authentication_classes = (DeviceUuidAuthentication, )
    permission_classes = (IsAuthorized,)
    renderer_classes = (GenericJsonRenderer, )
    serializer_class = DeviceSettingsSerializer

    def finalize_response(self, request, response, *args, **kwargs):
        ApiCallLog.log(request=request, response=response)
        return super().finalize_response(request, response, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        data = dict()

        device_obj = request.c_device[0]
        try:
            settings = DeviceSettings.objects.get(device=device_obj)
            serialized = self.serializer_class(settings)
            data['settings'] = serialized.data

            return Response(serialized.data)

        except Resolver404 as exp:
            pass