from django.core.urlresolvers import Resolver404
from rest_framework import status
from rest_framework.views import APIView
from django.db import transaction
from blackwidow.core.api.athorization import IsAuthorized
from blackwidow.core.models import InstalledFieldbuzzApplication
from device_manager.api.athorization.device_uuid_authentication import DeviceUuidAuthentication
from device_manager.api.serializers.serializers import InstalledAppsSerializer
from rest_framework.response import Response
from rest_framework import exceptions
from blackwidow.core.models.log.logs import ApiCallLog

__author__ = 'zia ahmed'


class InstalledAppsView(APIView):
    authentication_classes = (DeviceUuidAuthentication, )
    permission_classes = (IsAuthorized,)
    # renderer_classes = (GenericJsonRenderer, )

    serializer_class = InstalledAppsSerializer

    def finalize_response(self, request, response, *args, **kwargs):
        ApiCallLog.log(request=request, response=response)
        return super().finalize_response(request, response, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = dict()
        device_obj = request.c_device[0]

        try:
            serialized = self.serializer_class(data=request.DATA, many=True)
            if serialized.is_valid() and len(serialized.data) > 0:
                with transaction.atomic():
                    InstalledFieldbuzzApplication.objects.filter(device=device_obj).delete()

                    for app in serialized.data:
                        install_app = InstalledFieldbuzzApplication()
                        install_app.application_name = app['application_name']
                        install_app.package_name = app['package_name']
                        install_app.version_name = app['version_name']
                        install_app.version_code = app['version_code']
                        install_app.device = device_obj
                        install_app.save()

                    return Response({'message': 'Successfully updated', 'status': True})
            else:
                msg = "Unsupported json format"
                raise exceptions.APIException(msg)
            return Response(serialized.errors, status=status.HTTP_400_BAD_REQUEST)

        except Resolver404 as exp:
            pass
