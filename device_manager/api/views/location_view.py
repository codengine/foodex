from django.core.urlresolvers import Resolver404
from django.db import transaction
from rest_framework import status
from rest_framework.views import APIView
from blackwidow.core.api.athorization import IsAuthorized
from blackwidow.core.api.renderers import GenericJsonRenderer
from blackwidow.core.models import DeviceLocation
from device_manager.api.athorization.device_uuid_authentication import DeviceUuidAuthentication
from device_manager.api.serializers.serializers import LocationSerializer
from rest_framework.response import Response
from blackwidow.core.models.log.logs import ApiCallLog

__author__ = 'zia ahmed'


class GetLocationView(APIView):
    authentication_classes = (DeviceUuidAuthentication, )
    permission_classes = (IsAuthorized, )
    renderer_classes = (GenericJsonRenderer, )
    serializer_class = LocationSerializer

    def finalize_response(self, request, response, *args, **kwargs):
        ApiCallLog.log(request=request, response=response)
        return super().finalize_response(request, response, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        data = dict()
        device_obj = request.c_device[0]
        try:
            location = DeviceLocation.objects.get(device=device_obj)
            serialized = self.serializer_class(location, data=request.DATA)
            if serialized.is_valid():
                with transaction.atomic():
                    serialized.save()
                return Response(serialized.data)
            return Response(serialized.errors, status=status.HTTP_400_BAD_REQUEST)

        except Resolver404 as exp:
            pass