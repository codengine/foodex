from django.core.urlresolvers import Resolver404
from rest_framework.views import APIView
from rest_framework.response import Response
from blackwidow.core.api.athorization import IsAuthorized
from blackwidow.core.api.renderers import GenericJsonRenderer
from blackwidow.core.models import FieldBuzzApplication
from device_manager.api.athorization.device_uuid_authentication import DeviceUuidAuthentication
from device_manager.api.serializers.serializers import FieldBuzzAppSerializer
from blackwidow.core.models.log.logs import ApiCallLog

__author__ = 'zia ahmed'


class FieldBuzzAppsView(APIView):
    authentication_classes = (DeviceUuidAuthentication, )
    permission_classes = (IsAuthorized,)
    renderer_classes = (GenericJsonRenderer, )
    serializer_class = FieldBuzzAppSerializer

    def finalize_response(self, request, response, *args, **kwargs):
        ApiCallLog.log(request=request, response=response)
        return super().finalize_response(request, response, *args, **kwargs)


    def get(self, request, *args, **kwargs):
        data = dict()
        device_obj = request.c_device[0]
        try:
            organization = device_obj.organization
            applications = FieldBuzzApplication.objects.filter(organization=organization, deleted_level=0)
            if device_obj.service_person:
                user_role = device_obj.service_person.role
                applications = FieldBuzzApplication.objects.filter(organization=organization, pk__in=user_role.fieldbuzz_apps.values_list('id', flat=True), deleted_level=0)

            serialized = self.serializer_class(applications, many=True)
            data['fieldbuzz_apps'] = serialized.data

            return Response(data)

        except Resolver404 as exp:
            pass