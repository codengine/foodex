from django.core.urlresolvers import Resolver404
from rest_framework.response import Response
from rest_framework.views import APIView
from blackwidow.core.api.athorization import IsAuthorized
from blackwidow.core.api.renderers import GenericJsonRenderer
from blackwidow.core.models import DeviceLock
from device_manager.api.athorization.device_uuid_authentication import DeviceUuidAuthentication
from device_manager.api.serializers.serializers import DeviceLockSerializer
from blackwidow.core.models.log.logs import ApiCallLog

__author__ = 'zia ahmed'


class DeviceLockView(APIView):
    authentication_classes = (DeviceUuidAuthentication, )
    permission_classes = (IsAuthorized,)
    renderer_classes = (GenericJsonRenderer, )
    serializer_class = DeviceLockSerializer

    def finalize_response(self, request, response, *args, **kwargs):
        ApiCallLog.log(request=request, response=response)
        return super().finalize_response(request, response, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        device_obj = request.c_device[0]

        try:
            device_lock = DeviceLock.objects.get(device=device_obj)
            serialized = self.serializer_class(device_lock)
            return Response(serialized.data)

        except Resolver404 as exp:
            pass