from django.core.urlresolvers import Resolver404
from django.db import transaction
from rest_framework.response import Response
from rest_framework.views import APIView
from blackwidow.core.api.athorization import IsAuthorized
from blackwidow.core.api.renderers import GenericJsonRenderer
from blackwidow.core.models import DeviceUpdateStatus
from device_manager.api.athorization.device_uuid_authentication import DeviceUuidAuthentication
from blackwidow.core.models.log.logs import ApiCallLog

__author__ = 'zia ahmed'


class UpdateDeviceStatusView(APIView):
    authentication_classes = (DeviceUuidAuthentication, )
    permission_classes = (IsAuthorized,)
    # renderer_classes = (GenericJsonRenderer, )

    def finalize_response(self, request, response, *args, **kwargs):
        ApiCallLog.log(request=request, response=response)
        return super().finalize_response(request, response, *args, **kwargs)


    def post(self, request, *args, **kwargs):
        device_obj = request.c_device[0]

        try:
            d_update = DeviceUpdateStatus.objects.get(device=device_obj)

            with transaction.atomic():
                d_update.status = DeviceUpdateStatus.UPDATED
                d_update.save()
            return Response({'message': 'Success'})

        except Resolver404 as exp:
            pass
