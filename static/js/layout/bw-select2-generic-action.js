/**
 *  * Created by Shamil on 31-Dec-15 10:16 AM
 * Organization FIS
 */

function markMatch(text, term, markup) {
    var match = text.toUpperCase().indexOf(term.toUpperCase()),
        tl = term.length;

    if (match < 0) {
        markup.push(text);
        return;
    }

    markup.push(text.substring(0, match));
    markup.push("<span class='select2-match'>");
    markup.push(text.substring(match, match + tl));
    markup.push("</span>");
    markup.push(text.substring(match + tl, text.length));
}

function ajax_call(data_url, complete_callback) {
    $.ajax(data_url,
        {
            dataType: "json"
        }).done(complete_callback);
}

var initial_value = {};
var select2_parents = [];
var select2_watcher = {}; //{ name:[select2_watcher1,select2_watcher2,select2_watcher3] } here select2_watcher1.. are names.
var select2_cached_data = {};

var disableSelect2 = function (select2) {
    $(select2).select2('enable', false);
};
var enableSelect2 = function (select2) {
    $(select2).select2('enable', true);
};
var select2ItemNameFormat = function (item) {
    // Ugly hack to show company name during order creation when the client is modern trade
    if (item.code.substring(0, 3) === 'MT-') {
        return item.code + (item.code == "---------" && item.id == "" ? "" : ": ") +
            item.company_name;
    }
    return item.code + (item.code == "---------" && item.id == "" ? "" : ": ") +
        item.name + (item.description == undefined ? '' : ' ' + item.description)
};
var select2Load = function (select2, result, select2_cached_data) {
    var items = [];
    for (var index = 0; index < result.items.length; index++) {
        var temp = result.items[index];
        temp['text'] = select2ItemNameFormat(temp);
        items.push(temp);
    }
    $(select2).select2({
        width: $(select2).attr('width') == 'null' ? 220 : $(select2).attr('width'),
        multiple: $(select2).attr('multiple') == 'multiple',
        data: items,
        initSelection: function (element, callback) {
            if (result != null) {
                var initValue = initial_value[$(select2).attr('id')];
                var selected = 0;
                for (var i = 0; i < items.length; i++) {
                    if ($(select2).attr('multiple') != 'multiple') {
                        var value = items[i].id;
                        var name = items[i].name;
                        if (value == initValue || name == initValue) {
                            selected = i;
                            break;
                        }
                    }
                }
                callback($(select2).attr('multiple') == 'multiple' ? items : items[selected]);
            }
        },
        formatSelection: function (item, container) {
            return item ? select2ItemNameFormat(item) : undefined;
        },
        formatResult: function (item, container, query) {
            var markup = [];
            markMatch(select2ItemNameFormat(item), query.term, markup);
            return markup.join("");
        }
    });
};

var select2APIDataLoad = function (select2, result, dummyFirstItem) {
    if (dummyFirstItem === true)
        result.items.unshift({"code": "---------", "name": "", "id": ""});
    var depends_on = $(select2).data('depends-on');
    if(typeof depends_on != "undefined" && depends_on != "") {
        select2_cached_data[depends_on] = result.items;
    }
    enableSelect2(select2);
    select2Load(select2, result, select2_cached_data[$(select2).attr('id')]);

    var init_value = initial_value[$(select2).attr('id')];
    var selected = [];
    for (var i = 0; i < result.items.length; i++) {
        if ((typeof init_value === 'undefined' || init_value === undefined || init_value === '') &&
            ($(select2).attr('multiple') == 'multiple')) {
            selected.push(result.items[i]);
        } else {
            var id = result.items[i].id;
            var name = result.items[i].name;
            if (id == init_value || name == init_value) {
                selected.push(result.items[i]);
                break;
            }
        }
    }
    if ($(select2).attr('multiple') == 'multiple') {
        $(select2).select2("data", selected).change();
    } else {
        $(select2).select2("data", selected[0]).change();
    }
};

var loadSelect2FromAPICall = function (select2, url, parameters, dummyFirstItem, isWatcher) {
    $.ajax({
        url: url,
        dataType: 'json',
        quietMillis: 250,
        data: parameters,
        success: function (result) {
            if (isWatcher === true) {
                for (var j = 0; j < select2_watcher[$(select2).prop("name")].length; j++) {
                    var child_select2 = "#id_" + select2_watcher[$(select2).prop("name")][j];
                    select2APIDataLoad(child_select2, result, (j == 0));
                }
            } else {
                select2APIDataLoad(select2, result, dummyFirstItem);
            }
        },
        cache: true
    });
};

var client_type = "";
window.client_type = client_type;


function reinitialize_all_prices(client_type) {
    all_product_select2s = $("select.select2, input[data-url].select2-input")
    if (all_product_select2s != null) {
        all_product_select2s.each(function () {

            if ($(this).hasClass("require-unitprice")) {
                var _this_val = $(this).val();
                if(_this_val == "") {
                    return;
                }
                var _this_object = $(this);
                _this_object.prop("disabled", true);
                var id = $(this).val();
                if(id==='') id = 0;
                var data_url = "";
                if(client_type == "distributor") {
                    data_url = "/product-price-value/?format=json&search=1&_search_gross_unit_price:product:id="+id;
                }
                else if(client_type == "retailer") {
                    data_url = "/product-price-value/?format=json&search=1&_search_dist_unit_price:product:id="+id;
                }
                else {
                    if($(this).hasClass("require-dist-price")){
                        if ($("input[id$=from_client]").length > 0) {
                            data_url = "/product-price-value/?format=json&search=1&_search_gross_unit_price:product:id="+id;

                        }
                    }
                    else if($(this).hasClass("require-gross-price")){
                        if ($("input[id$=client]").length > 0) {
                            client_id = $("input[id$=client]").val();
                            data_url = "/product-price-config/?format=json&search=1&_search_gross_unit_price:product:id=" + id + "&_search_gross_unit_price:client:id=" + client_id;

                        }
                    }
                }

                ajax_call(data_url, function (data) {
                    _this_object.prop("disabled", false);
                    try {
                        _this_object.parent().parent().parent().parent()
                            .find("input[id$=unit_price]").val(data.items[0].value.toFixed(1)).change();
                    } catch (e) {
                    }
                });
            }

        });
    }
}

function init_select() {
    all_select2s = $(".select2-input");
    if (all_select2s != null) {
        all_select2s.each(function () {
            var this_select2 = this;
            select2APIDataLoad(this_select2, { items: [] }, false);
            $(this_select2).on("change", function (e) {
                if($(this).hasClass("toggle_po_number") || $(this).hasClass("secondary_client")) {
                    var val = $(this).val();
                    if(typeof val != "undefined" && val != "") {
                        $.ajax({
                            url: "/clients/details/"+ $(this).val() +"?format=json",
                            dataType: 'json',
                            quietMillis: 250,
                            data: {},
                            success: function (result) {
                                if(typeof result.code != "undefined") {
                                    if(result.code.startsWith("DIST")) {
                                        $("#id_reference_po_number").addClass("hidden");
                                        $("#id_reference_po_number").parent().parent().addClass("hidden");
                                        client_type = "distributor";
                                    }
                                    else if(result.code.startsWith("R-")) {
                                        $("#id_reference_po_number").addClass("hidden");
                                        $("#id_reference_po_number").parent().parent().addClass("hidden");
                                        client_type = "retailer";
                                    }
                                    else {
                                        $("#id_reference_po_number").removeClass("hidden");
                                        $("#id_reference_po_number").parent().parent().removeClass("hidden");
                                        client_type = "no_distributor";
                                    }
                                    if(!$(this).hasClass("secondary_client")) {
                                        reinitialize_all_prices(client_type);
                                    }
                                }
                            },
                            cache: true
                        });
                    }
                }
            });

        });
    }
}


window.initial_loaded_prices = {};


function detect_client_type_initial(value) {
    var val = value;
    if(typeof val != "undefined" && val != "") {
        $.ajax({
            url: "/clients/details/"+ val +"?format=json",
            dataType: 'json',
            quietMillis: 250,
            data: {},
            success: function (result) {

                if(typeof result.code != "undefined") {
                    if(result.code.startsWith("DIST")) {
                        $("#id_reference_po_number").addClass("hidden");
                        $("#id_reference_po_number").parent().parent().addClass("hidden");
                        window.client_type = "distributor";
                    }
                    else if(result.code.startsWith("R-")) {
                        $("#id_reference_po_number").addClass("hidden");
                        $("#id_reference_po_number").parent().parent().addClass("hidden");
                        window.client_type = "retailer";
                    }
                    else {
                        $("#id_reference_po_number").removeClass("hidden");
                        $("#id_reference_po_number").parent().parent().removeClass("hidden");
                        window.client_type = "no_distributor";
                    }
                }
            },
            cache: true
        });
    }
}

var loadBWSelect2Fields = function (parent,added_new) {

    init_select();

    var all_select2s = null;
    if (parent == undefined || parent == null) {
        all_select2s = $("select.select2, input[data-url].select2-input");
    } else {
        all_select2s = $(parent).find("select.select2, input[data-url].select2-input");
    }
    if (all_select2s != null) {
        all_select2s.each(function () {
            var this_select2 = this;
            var data_url = $(this_select2).data('url');
            // Check if data-url property of select2 field is given or not

            if($(this).hasClass("toggle_po_number") || $(this).hasClass("secondary_client")) {
                var val = $(this).val();
                detect_client_type_initial(val);
            }

            if (data_url != null && data_url != undefined) {
                var depends_on = $(this_select2).data('depends-on');
                var value = $(this_select2).val();
                if (typeof value != 'undefined' && value != '') {
                    initial_value[$(this_select2).attr('id')] = value;
                }
                // Check if this select2 is dependent on others value or not
                if (depends_on == null || depends_on == "") {
                    disableSelect2(this_select2);
                    loadSelect2FromAPICall(this_select2, data_url, {all: ''}, false, false);
                } else {
                    var parent_prefix =
                        $(this_select2).data('prefix') == null ||
                        $(this_select2).data('prefix') == '-' ? '' : $(this_select2).data('prefix');
                    var dependencies = depends_on.split(',');
                    var properties = $(this_select2).data('depends-property').split(",");

                    for (var _d = 0; _d < dependencies.length; _d++) {
                        var d = dependencies[_d];
                        if ($(this_select2).prop("name").startsWith("suffix")) {
                            if (!select2_watcher.hasOwnProperty(d)) {
                                select2_watcher[d] = [];
                                var parent = "#id_" + d;
                                var parent_value = $(parent).val();
                                select2_watcher[d + '_value'] = parent_value;
                                $(parent).val(parent_value - 1);
                                $(parent).on("change", function (e) {

                                    var this_select = this;
                                    if (select2_watcher[$(this_select).prop("name")] != undefined &&
                                        select2_watcher[$(this_select).prop("name")].length >= 1) {

                                        var _dependentFirst = $("#id_" + select2_watcher[$(this_select).prop("name")][0]);
                                        var data_url = _dependentFirst.data("url");

                                        for (var i = 0; i < select2_watcher[$(this_select).prop("name")].length; i++) {
                                            disableSelect2("#id_" + select2_watcher[$(this_select).prop("name")][i]);
                                        }

                                        var properties = _dependentFirst.data('depends-property').split(",");
                                        var t = {};
                                        var named = [];
                                        if (_dependentFirst.data('named-parameters') != null) {
                                            named = _dependentFirst.data('named-parameters').split(",");
                                        }
                                        for (var dd = 0; dd < properties.length; dd++) {
                                            t[properties[dd]] = $(this_select).val();
                                        }
                                        loadSelect2FromAPICall(this_select, data_url, t, true, true);
                                    }
                                });
                            }
                            if (select2_watcher[d].indexOf($(this_select2).prop("name")) <= -1) {
                                select2_watcher[d].push($(this_select2).prop("name"));
                            }
                        }

                        var parent_id = "#id_" + parent_prefix + d;
                        if ($(parent_id).data('depends-on') == null || $(parent_id).data('depends-on') == "") {
                            select2_parents.push(parent_id);
                        }

                        $(document).off('change', parent_id).on('change', parent_id, function () {
                            var t = {};
                            var named = [];
                            if ($(this_select2).data('named-parameters') != null) {
                                named = $(this_select2).data('named-parameters').split(",");
                            }

                            var no_parameter_found = true;
                            for (var _dd = 0; _dd < properties.length; _dd++) {
                                var val = $("#id_" + parent_prefix + dependencies[_dd]).val();
                                if (val === '') {
                                    val = -1;
                                    no_parameter_found = false;
                                }
                                else {
                                    no_parameter_found = false;
                                }
                                t[properties[_dd]] = val;
                            }
                            if (no_parameter_found) {
                                select2Load(this_select2, null, []);
                            }
                            else {
                                for (var dd = 0; dd < named.length; dd++) {
                                    var nd = named[dd].split(':');
                                    data_url = data_url.replace(nd[1], $("#id_" + parent_prefix + nd[0]).val())
                                }
                                disableSelect2(this_select2);
                                loadSelect2FromAPICall(this_select2, data_url, t, true, false);
                            }
                        });
                    }
                }
            }
            else {
                if ($(this).hasClass("order-select2")) {
                    $(".select2.order-select2").css("width", "220px");
                    $(this).select2({width: '220px'});
                }
                else {
                    $(this).css("width", "220px");
                    $(this).select2({width: '220px'});
                }
            }
        });
        for (var i = 0; i < select2_parents.length; i++) {
            $(select2_parents[i]).val($(select2_parents[i]).find(":selected").val()).change();
        }
        for (var key in select2_watcher) {
            if (select2_watcher.hasOwnProperty(key)) {
                var select2_watcher_item = "#id_" + key;
                $(select2_watcher_item).val(select2_watcher[key + "_value"]).trigger("change");
                var watcher_name = select2_watcher[$(select2_watcher_item).prop("name")];
                for (var index = 0; index < select2_watcher[$(select2_watcher_item).prop("name")].length; index++) {
                    var $dependantThis = "#id_" + select2_watcher[$(select2_watcher_item).prop("name")][index];
                    $($dependantThis).on("change", function (e) {
                        if($(this).hasClass("edit-mode")) {
                            if(!window.initial_loaded_prices.hasOwnProperty($(this).prop("id"))) {
                                window.initial_loaded_prices[$(this).prop("id")] = true;
                                return;
                            }
                        }

                        if ($(this).hasClass("require-unitprice")) {
                            var _this_val = $(this).val();
                            if(_this_val == "") {
                                return;
                            }
                            var _this_object = $(this);
                            _this_object.prop("disabled", true);
                            var id = $(this).val();
                            if(id==='') id = 0;
                            var data_url = "";
                            if(client_type == "distributor") {
                                data_url = "/product-price-value/?format=json&search=1&_search_gross_unit_price:product:id="+id;
                            }
                            else if(client_type == "retailer") {
                                data_url = "/product-price-value/?format=json&search=1&_search_dist_unit_price:product:id="+id;
                            }
                            else {
                                if($(this).hasClass("require-dist-price")){
                                    if ($("input[id$=from_client]").length > 0) {
                                        data_url = "/product-price-value/?format=json&search=1&_search_gross_unit_price:product:id="+id;

                                    }
                                }
                                else if($(this).hasClass("require-gross-price")){
                                    if ($("input[id$=client]").length > 0) {
                                        client_id = $("input[id$=client]").val();
                                        data_url = "/product-price-config/?format=json&search=1&_search_gross_unit_price:product:id=" + id + "&_search_gross_unit_price:client:id=" + client_id;

                                    }
                                }
                            }


                            ajax_call(data_url, function (data) {
                                _this_object.prop("disabled", false);
                                try {
                                    _this_object.parent().parent().parent().parent()
                                        .find("input[id$=unit_price]").val(data.items[0].value.toFixed(1)).change();

                                } catch (e) {
                                }
                            });
                        }
                    });
                }

                //window.initial_price_loaded = true;

            }
        }
    }
};

var updateSearchSelectField = function (id, data_url) {
    var ttthis = id;
    $(ttthis).empty().select2('enable', false);
    $.ajax({
        url: data_url,
        type: 'get',
        success: function (result) {
            select2_cached_data[$(ttthis).attr('id')] = result.items;
            $(ttthis).select2('enable', true);
            var data = result.items;
            $.each(data, function (index, value) {
                $(ttthis).append('<option value="' + data[index].code + '">' + data[index].name + '</option>');
            });
            $(ttthis).val(data[0].code).change();
        }
    });
}

var updateSearchFields = function (clear) {
    var api_link = $(".search_property option:selected").data('api-link');
    if ($(".search_property option:selected").data('is-range') == 'True') {
        $("form.search-form").find(".datetimepicker").datetimepicker({
            pick12HourFormat: true
        });
        $(".search-form").find('span.add-on').show();
        $("div.query_1").attr('placeholder', 'From').show();
        $("div.query_2").attr('placeholder', 'To').show();
        $("div.query_3").hide();
        $("div.query_4").hide();
    } else if (api_link === null || api_link === '') {
        $("div.query_3").attr('placeholder', 'Search Term').show();
        $("div.query_1").hide();
        $("div.query_2").hide();
        $("div.query_4").hide();
        $(".search-form").find('span.add-on').hide();
    } else {
        $("div.query_4").attr('placeholder', 'Search Term').show();
        $("div.query_1").hide();
        $("div.query_2").hide();
        $("div.query_3").hide();
        $(".search-form").find('span.add-on').hide();
        updateSearchSelectField("#query_4", api_link);
    }
    if (clear == true) {
        $(".search-input").val('');
    }
};