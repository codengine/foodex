/**
 *  * Created by Shamil on 31-Dec-15 2:10 PM
 * Organization FIS
 */

$(document).on('click', ".load-download-modal", function () {
    $('#download-modal-form').find(".modal-footer").find(".btn-ok").prop('disabled', true);
    $('#download-modal-form').find('.modal-body').html('').addClass('loading');

    if ($(this).data('wide') == '1') {
        $('#download-modal-form').addClass('wide-modal')
    } else {
        $('#download-modal-form').removeClass('wide-modal')
    }

    var tthis = this;
    $('#download-modal-form')
        .find(".modal-footer")
        .find(".btn-cancel")
        .unbind('click').on('click', function () {
            $('#download-modal-form').modal('hide');
        });

    var download_timer;
    var interval = 1000; //1 second.
    var t = 1;
    var check_download_file_ready_ajax_progress = false;

    function prepare_download_url(file_name) {
        var msg = "Data is successfully processed to be downloaded. Please click <a style='color: blue;' href='/export-files/download/1/?names=" + file_name + "'>here</a> or the button bellow to download the file.<br><a class='btn btn-sm btn-danger' href='/export-files/download/1/?names=" + file_name + "'>Download file</a>";
        return msg
    }

    function check_download_file_ready(file_name) {
        if (!check_download_file_ready_ajax_progress) {
            $.ajax({
                url: "/export-files/?name=" + file_name + "&search=1&format=json",
                type: 'get',
                success: function (data) {
                    //console.log(data);
                    if (data.total_items > 0) {
                        var download_url = prepare_download_url(file_name);
                        $("#download-modal-form").find(".progress_status_msg").hide();
                        $('#download-modal-form').find('.modal-body').removeClass('loading');
                        $("#download-modal-form").find(".modal-body").show();
                        $("#download-modal-form").find(".modal-body").html(download_url);
                        $('#download-modal-form').find(".modal-footer").find(".btn-ok").text("Done");
                        stop_download_timer();
                    }
                    check_download_file_ready_ajax_progress = false;
                }
            });
            check_download_file_ready_ajax_progress = true;
        }
    }

    function start_download_timer(file_name) {
        download_timer = setInterval(function () {
            check_download_file_ready(file_name);
        }, interval);
    }

    function stop_download_timer() {
        if (download_timer) {
            clearInterval(download_timer);
        }
    }

    $('#download-modal-form')
        .find(".modal-footer")
        .find(".btn-ok")
        .unbind('click')
        .on('click', function (e) {
            if ($(this).text() == "Done") {
                $("#download-modal-form").modal('hide');
                return;
            }
            $('.btn-ok').prop('disabled', true);
            $("#download-modal-form").find(".progress_status_msg").html("<h4 style='padding: 17px;'>Please wait for a while. We are generating excel file to download...</h4>");
            $("#download-modal-form").find(".progress_status_msg").show();
            $("#download-modal-form").find(".modal-body").hide();
            $(this).text("Processing...");
            var _this_object = $(this);
            $.ajax({
                url: "advanced-download/",
                data: $("#id_advanced_download_form").serialize(),
                type: 'get',
                success: function (data) {
                    //$('.btn-ok').removeAttr('disabled');
                    $("#download-modal-form").find(".modal-body").show();
                    //console.log(data);
                    if (data.success == true) {
                        $("#download-modal-form").find(".progress_status_msg").html("<h4 style='padding: 17px;'>Please wait for a while. We are generating excel file to download...</h4>");
                        $("#download-modal-form").find(".progress_status_msg").show();
                        $("#download-modal-form").find(".modal-body").hide();
                        _this_object.text("Processing...");

                        start_download_timer(data.message);
                    }
                    else {
                        $("#download-modal-form").find(".progress_status_msg").hide();
                        $('#download-modal-form').find('.modal-body').removeClass('loading');
                        $("#download-modal-form").find(".progress_status_msg").html("File creation failed.");
                        $("#download-modal-form").find(".progress_status_msg").show();
                        _this_object.text("Start Download");
                        $("#id_advanced_download_form").show();
                    }
                }
            });
        });

    $('#download-modal-form').unbind('shown').on('shown', function () {
        $("#download-modal-form").find(".progress_status_msg").html("");
        $("#download-modal-form").find(".progress_status_msg").hide();
        $.ajax({
            url: "advanced-download/?get_form=1",// + "/" + $(tthis).data('id'),
            type: 'get',
            success: function (data) {
                $('.btn-ok').removeAttr('disabled');
                $('#download-modal-form').find('.modal-body').removeClass('loading');
                $('#download-modal-form').find(".modal-body").html(data);
                loadBWSelect2Fields();
                //updateSelect2Fields();
                $(".date-time-picker").datetimepicker();
                $('#download-modal-form').find('div.ajax-container').attr('data-url', $(tthis).attr('href'));
            }
        });
    });
    $("#download-modal-form").modal('show');
    return false;
});