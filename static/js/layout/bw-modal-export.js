/**
 *  * Created by Shamil on 31-Dec-15 2:10 PM
 * Organization FIS
 */
var export_running = false;
$(document).on('click', ".load-export-modal", function () {
    var $export_button = $(this);
    var button_label = $(this).find("span").text();
    $(this).find("span").text("Processing...");
    if(export_running == true) {
        return false;
    }
    export_running = true;
    $('#export-modal-form').find(".modal-footer").find(".btn-ok").prop('disabled', true);
    $('#export-modal-form').find('.modal-body').html('').addClass('loading');

    if ($(this).data('wide') == '1') {
        $('#export-modal-form').addClass('wide-modal')
    } else {
        $('#export-modal-form').removeClass('wide-modal')
    }

    var tthis = this;
    $('#export-modal-form')
        .find(".modal-footer")
        .find(".btn-cancel")
        .unbind('click').on('click', function () {
            $('#export-modal-form').modal('hide');
        });

    var export_timer;
    var interval = 1000; //1 second.
    var t = 1;
    var check_download_file_ready_ajax_progress = false;

    function prepare_download_url(file_name) {
        var msg = "Data has been successfully exported. Please click <a style='color: blue;' href='/export-files/download/1/?names=" + file_name + "'>here</a> or the button bellow to download the file.<br><a class='btn btn-sm btn-danger' href='/export-files/download/1/?names=" + file_name + "'>Download file</a>";
        return msg
    }

    function check_export_file_ready(file_name) {
        if (!check_download_file_ready_ajax_progress) {
            $.ajax({
                url: "/export-files/?name=" + file_name + "&search=1&format=json",
                type: 'get',
                success: function (data) {
                    //console.log(data);
                    if (data.total_items > 0) {
                        $export_button.find("span").text(button_label);
                        export_running = false;
                        window.location.href = "/export-files/download/1/?names=" + file_name;

//                        var download_url = prepare_download_url(file_name);
//                        $("#export-modal-form").find(".progress_status_msg").hide();
//                        $('#export-modal-form').find('.modal-body').removeClass('loading');
//                        $("#export-modal-form").find(".modal-body").show();
//                        $("#export-modal-form").find(".modal-body").html(download_url);
//                        $('#export-modal-form').find(".modal-footer").find(".btn-ok").text("Done");
                        stop_export_timer();
                    }
                    check_download_file_ready_ajax_progress = false;
                }
            });
            check_download_file_ready_ajax_progress = true;
        }
    }

    function start_export_timer(file_name) {
        export_timer = setInterval(function () {
            check_export_file_ready(file_name);
        }, interval);
    }

    function stop_export_timer() {
        if (export_timer) {
            clearInterval(export_timer);
        }
    }

    function export_execute_handler(success_callback) {
        var data = $("#id_advanced_export_form").serialize();
        if(data == "") {
            data = {};
        }
        if($("select[name=kpi-group-select2]").length) {
            data['group:id'] = $("select[name=kpi-group-select2]").val();
            data['filter'] = "true";
        }
        $.ajax({
            url: "advanced-export/",
            data: data,
            type: 'get',
            success: function (data) {
                if( typeof success_callback != "undefined" && typeof success_callback == "function") {
                    success_callback(data);
                }
            }
        });
    }

    $('#export-modal-form')
        .find(".modal-footer")
        .find(".btn-ok")
        .unbind('click')
        .on('click', function (e) {
            $("#export-modal-form").modal('hide');
            $('.btn-ok').prop('disabled', true);
            $("#export-modal-form").find(".progress_status_msg").html("<h4 style='padding: 17px;'>Please wait for a while. We are generating export...</h4>");
            $("#export-modal-form").find(".progress_status_msg").show();
            var _this_object = $(this);

            export_execute_handler(function(data) {
                $("#export-modal-form").find(".modal-body").show();
                //console.log(data);
                if (data.success == true) {
                    start_export_timer(data.message);
                }
                else {

                }
            });
        });

    $('#export-modal-form').unbind('shown').on('shown', function () {
        $("#export-modal-form").find(".progress_status_msg").hide();
        $('.btn-ok').removeAttr('disabled');
        $('#export-modal-form').find('.modal-body').removeClass('loading');
        $('#export-modal-form').find(".modal-body").html(export_form);
        loadBWSelect2Fields();
        //updateSelect2Fields();
        $(".date-time-picker").datetimepicker();
        $('#export-modal-form').find('div.ajax-container').attr('data-url', $(tthis).attr('href'));
    });

    var export_form = "";
    var form_required = false;
    $.ajax({
        url: "advanced-export/?get_form=1",// + "/" + $(tthis).data('id'),
        dataType: "json",
        type: 'get',
        success: function (data) {

            if(data.status == "SUCCESS") {
                console.log(data);
                if(data.data.form != 0) {
                    export_form = data.data.form;
                    $("#export-modal-form").modal('show');
                }
                else {
                    export_execute_handler(function(data) {
                        if (data.success == true) {
                            start_export_timer(data.message);
                        }
                        else {

                        }
                    });
                }
            }
            else {

            }
        }
    });
    return false;
});