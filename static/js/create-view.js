/**
 * Created by Mahmud on 9/2/2014.
 */

var DataDict = function (prefix, html) {
    var _self = this;
    _self.prefix = prefix;
    _self.html = html;
    return _self;
};
var formsetDict = [];

//build formset dictionary
var buildFormsetDictionary = function (a) {
    var prefix = $(a).data('prefix');
    //alert(prefix);
    var html = $('.' + prefix + '-formset-container:first').get(0).outerHTML;
    $(html).find('.select2').removeClass('select2-offscreen');
    $(html).find('.select2-container').remove();
    $(html).find('.select2').attr('tab-index', '1');

    formsetDict[formsetDict.length] = new DataDict(prefix, html);
};

var evaluationExpression = function (prefix, expression, fields) {
    var calc = expression;
    for (var i = 0; i < fields.length; i++) {
        var _c_1 = fields[i];
        var _c = fields[i].replace('[', '').replace(']', '');
        var elemId = prefix + _c;
        calc = calc.replace(_c_1, $("#id_" + elemId).val() == "" ? "0" : $("#id_" + elemId).val());
    }
    return eval(calc);
};

var updateCalculatedField = function () {
    $("[data-calculation]").each(function (e) {
        var tthis = this;
        var prefix = $(this).data('prefix');
        var re = /\[\w+\]/g;
        var calculation = $(this).data('calculation');
        var refs = calculation.match(re);
        for (var i = 0; i < refs.length; i++) {
            var _c = refs[i].replace('[', '').replace(']', '');
            var elemId = prefix + _c
            $("#id_" + elemId).change(function () {
                $(tthis).val(evaluationExpression(prefix, calculation, refs))
            });
        }
    });
};

var updateRefFields = function () {
    $("input[data-depends-on]").each(function () {
        if ($(this).hasClass('select2'))
            return;
        var ttthis = this;

        var data_url = $(this).data('url');
        var _prefix = $(ttthis).data('prefix') == null || $(ttthis).data('prefix') == '-' ? '' : $(ttthis).data('prefix');
        var dependencies = $(ttthis).data('depends-on').split(',');
        var properties = $(ttthis).data('depends-property').split(",");
        for (var _d = 0; _d < dependencies.length; _d++) {
            d = dependencies[_d];
            $(document)
                .off('change', "#id_" + _prefix + d)
                .on('change', "#id_" + _prefix + d, function () {
                    var t = {
                        //all: $(this).val()
                    };
                    var named = [];
                    if ($(ttthis).data('named-parameters') != null) {
                        named = $(ttthis).data('named-parameters').split(",");
                    }
                    for (var dd = 0; dd < properties.length; dd++) {
                        t[properties[dd]] = $("#id_" + _prefix + dependencies[dd]).val();
                    }
                    data_url = data_url;
                    for (var dd = 0; dd < named.length; dd++) {
                        var nd = named[dd].split(':');
                        data_url = data_url.replace(nd[1], $("#id_" + _prefix + nd[0]).val())
                    }
                    $(ttthis).select2('enable', false);
                    $(ttthis).select2('data', 'loading');
                    $.ajax({
                        url: data_url,
                        dataType: 'json',
                        quietMillis: 250,
                        data: t,
                        success: function (result) {
                            console.log(result);
                        },
                        cache: true
                    });
                });
        }
    });
}

$(function () {

    var changeProperty = function (node, props, prefix, index) {
        $.map(props, function (prop) {
            var current = $(node).attr(prop);
            if (current != undefined) {
                var reg = new RegExp(prefix + '-(\\d)+');
                current = current.replace(reg, prefix + '-' + index);
                $(node).attr(prop, current);
                return node;
            }
        });
    };

    var updateInputFields = function (prefix) {
        var all = $("." + prefix + "-formset-container");
        for (var i = 0; i < all.length; i++) {
            var $html = $(all[i]);
            $html.find('select[name^="' + prefix + '"], ' +
            'label[for^="' + prefix + '"], ' +
            'input[name^="' + prefix + '"], ' +
            'span[data-valmsg-for^="' + prefix + '"]').each(function () {
                changeProperty(this, ['name', 'for', 'id', 'data-valmsg-for', 'data-prefix'], prefix, i);
            });
        }
        $('input[name="' + prefix + '-TOTAL_FORMS"]').val(all.length);
        $(".date-selector").datepicker({format: 'dd-mm-yyyy'});
    };


    $(document)
        .off('click', '.btn-inline-remove')
        .on('click', '.btn-inline-remove', function () {
            var prefix = $(this).data('prefix');
            var tthis = $(this).closest('.' + prefix + '-formset-container');
            $(tthis).slideUp($(tthis).height() * 2, function () {
                if ($.trim($(tthis).find("." + prefix + "-id").val()) == '') {
                    $(this).remove();
                    updateInputFields(prefix);
                } else {
                    $(tthis).find("input[type='checkbox'][name$='-DELETE']").attr('checked', 'checked');
                }

                var count = $(".btn-inline-remove").length;
                if(count == 1) {
                    $(".btn-inline-remove").hide();
                }

            });
            return false;
        });

    $(document)
        .off('click', '.btn-inline-addmore')
        .on('click', '.btn-inline-addmore', function () {
            var prefix = $(this).data('prefix');
            var html = '';
            for (var i = 0; i < formsetDict.length; i++) {
                if (prefix == formsetDict[i].prefix) {
                    html = $(formsetDict[i].html).clone();
                    break;
                }
            }
            var $result = $(html).insertBefore($(this).closest("." + prefix + "-formset-addmore-container"));
            $result.hide().slideDown($result.height());
            //$result.find("select.select2").select2();
            //updateSelect2Fields($result)
            updateInputFields(prefix);
            //loadBWSelect2Fields($result, true);

            $result.find(".control-label").each(function(i) {
               var __this = $(this);
                if(__this.parent().find(".order-breakdown").length || __this.parent().find(".require-unitprice").length) {
                    __this.css("visibility", "hidden");
                }
            });

            $result.find(".btn-inline-remove").show();

            if($result.find(".select2").length) {
                var select2 = $result.find(".select2").select2({
                    width: "220px"
                });
            }

            $(".btn-inline-remove").each(function(i) {
               $(this).show();
            });

            $result.find("input[name$='total_items']").val(0);
            $result.find("input[name$='unit_price']").val(0);
            $result.find("input[name$='total']").val(0);

            all_select2s = $result.find("select.select2, input[data-url].select2-input");
            if (all_select2s != null)
            {
                all_select2s.each(function ()
                {
                    var this_select2 = this;
                    var depends_on = $(this_select2).data('depends-on');
                    var cached_items = select2_cached_data[depends_on];
                    select2APIDataLoad(this_select2, { items: cached_items }, false);

                    $(this_select2).on("change", function (e) {
                        if ($(this).hasClass("require-unitprice")) {
                            var _this_val = $(this).val();
                            if(_this_val == "") {
                                return;
                            }
                            var _this_object = $(this);
                            _this_object.prop("disabled", true);
                            var id = $(this).val();
                            if(id==='') id = 0;
                            var data_url = "";
                            if(window.client_type == "distributor") {
                                data_url = "/product-price-value/?format=json&search=1&_search_gross_unit_price:product:id="+id;
                            }
                            else if(window.client_type == "retailer") {
                                data_url = "/product-price-value/?format=json&search=1&_search_dist_unit_price:product:id="+id;
                            }
                            else {
                                if($(this).hasClass("require-dist-price")){
                                    if ($("#id_client").length > 0) {
                                        data_url = "/product-price-value/?format=json&search=1&_search_gross_unit_price:product:id="+id;

                                    }
                                }
                                else if($(this).hasClass("require-gross-price")){
                                    if ($("#id_client").length > 0) {
                                        client_id = $("#id_client").val();
                                        data_url = "/product-price-config/?format=json&search=1&_search_gross_unit_price:product:id=" + id + "&_search_gross_unit_price:client:id=" + client_id;

                                    }
                                }
                            }
                            ajax_call(data_url, function (data) {
                                _this_object.prop("disabled", false);
                                try {
                                    _this_object.parent().parent().parent().parent()
                                        .find("input[id$=unit_price]").val(data.items[0].value.toFixed(1)).change();
                                } catch (e) {

                                }
                            });
                        }
                    });

                });
            }

            //This part is to show the remove icon in the right side. Each time the product is added trash icon is visible to that
            //particular product so that he can drop it. The first default product is not deletable.

            //$(".btn-inline-remove:first").hide();

            $(".btn-inline-remove:not(:first)").css({
                "float": "right",
                "display": "block",
                "margin-right": "67px"
            });
            return false;
        });


    function UpdateTotalItems(this_obj) {
//        var number_of_packet = parseInt(this_obj.parent().parent().parent().find("input[id$=number_of_packet]").val());
//        var items_per_packet = parseInt(this_obj.parent().parent().parent().find("input[id$=items_per_packet]").val());
//        var loose_items = parseInt(this_obj.parent().parent().parent().find("input[id$=loose_items]").val());
//        if(number_of_packet < 0 || items_per_packet < 0 || loose_items < 0){
//            return;
//            //this_obj.parent().parent().parent().find("input[id$=number_of_packet]").val(0);
//        }
        var total_items = parseInt(this_obj.parent().parent().parent().parent().find("input[id$=total_items]").val()); //number_of_packet * items_per_packet + loose_items;
        //this_obj.parent().parent().parent().find("input[id$=total_items]").val(total_items);
        var unit_price = parseFloat(this_obj.parent().parent().parent().parent().find("input[id$=unit_price]").val());
        if (unit_price < 0) {
            return;
        }
        var subtotal = total_items * unit_price;
        //this_obj.parent().parent().parent().parent().find("input[id$=sub_total]").val(subtotal.toFixed(1));
//        var discount = parseFloat(this_obj.parent().parent().parent().find("input[id$=discount]").val());
//        if(parseFloat(discount) < 0 || parseFloat(discount) > 100){
//            return;
//        }
//        var total = subtotal - ((discount/100) * subtotal);
        this_obj.parent().parent().parent().parent().find("input[id$=-total]").val(subtotal.toFixed(1));
    }

    function CheckPositiveInteger(this_obj) {
        var number_of_packet = parseInt(this_obj.parent().parent().parent().find("input[id$=number_of_packet]").val());
        var items_per_packet = parseInt(this_obj.parent().parent().parent().find("input[id$=items_per_packet]").val());
        var loose_items = parseInt(this_obj.parent().parent().parent().find("input[id$=loose_items]").val());
        if (number_of_packet < 0) {
            this_obj.parent().parent().parent().find("input[id$=number_of_packet]").val(0);
        }
    }

    //$("input[id$=total_items]").prop("readonly","true");
    $("input[id$=sub_total]").prop("readonly", "true");
    $("input[id$=-total]").prop("readonly", "true");
    $("input[id$=unit_price]").prop("readonly", "true");

    $(document).on("change", "input[id$=number_of_packet]", function (e) {
        UpdateTotalItems($(this));
        //CheckPositiveInteger($(this));
    });

    $(document).on("change", "input[id$=items_per_packet]", function (e) {
        UpdateTotalItems($(this));
    });

    $(document).on("change", "input[id$=loose_items]", function (e) {
        UpdateTotalItems($(this));
    });

    $(document).on("keyup", "input[id$=unit_price]", function (e) {
        UpdateTotalItems($(this));
    });

    $(document).on("change", "input[id$=unit_price]", function (e) {
        UpdateTotalItems($(this));
    });

    $(document).on("change", "input[id$=discount]", function (e) {
        UpdateTotalItems($(this));
    });

    $(document).on("change", "input[id$=total_items]", function (e) {
        UpdateTotalItems($(this));
    });

    // Price config form

    // Net customer price

    function updateNetCustomerPrice(this_obj) {
        var ref_price = parseFloat(this_obj.val());
        var ref_net_cust_markup = $("input[id$=id_ref_net_cust_markup]").val();
        var net_customer_price = parseFloat(this_obj.val());

        if (this_obj.val() != '') {
            if (ref_net_cust_markup != '' && ref_net_cust_markup != 'undefined') {
                var value = ref_net_cust_markup.split('%');
                var ref_net_cust_markup_value = parseFloat(value.shift());
                if (ref_net_cust_markup_value > 0) {
                    net_customer_price = (ref_price + (ref_price * ref_net_cust_markup_value) / 100).toFixed(2);
                } else {
                    net_customer_price = (ref_price + (ref_price * ref_net_cust_markup_value) / 100).toFixed(2);
                }
            } else {
                net_customer_price = net_customer_price;
            }
        } else {
            net_customer_price = '';
        }
        $("input[id$=id_net_cust_price]").val(net_customer_price);
    }

    function updateNetCustomerPriceFromMarkup(this_obj) {
        var ref_price_raw = $("input[id$=id_ref_price]").val();
        var ref_net_cust_markup = this_obj.val();
        if (ref_price_raw != '') {
            ref_price = parseFloat(ref_price_raw)
            if (ref_net_cust_markup != '' && ref_net_cust_markup != 'undefined') {
                var value = ref_net_cust_markup.split('%');
                var ref_net_cust_markup_value = parseFloat(value.shift());
                net_customer_price = (ref_price + (ref_price * ref_net_cust_markup_value) / 100).toFixed(2);
            } else {
                net_customer_price = ref_price;
            }
        } else {
            net_customer_price = '';
        }
        $("input[id$=id_net_cust_price]").val(net_customer_price);
    }

    // Update net customer markup from  Net customer price field

    function updateNetCustomerMarkup(this_obj) {
        var ref_price = $("input[id$=id_ref_price]").val();
        var ref_net_cust_markup = $("input[id$=id_ref_net_cust_markup]").val();
        var net_customer_price = parseFloat(this_obj.val());
        var ref_net_cust_markup_value = ''
        if (this_obj.val() != '') {
            if ((ref_price != '' && ref_price != 'undefined')) {
                var ref_price_value = parseFloat(ref_price);
                var subtracted_value = net_customer_price - ref_price_value
                var ref_net_cust_markup = (subtracted_value * 100) / net_customer_price
                ref_net_cust_markup_value = ref_net_cust_markup.toFixed(2) + '%'
            }
        }

        $("input[id$=id_ref_net_cust_markup]").val(ref_net_cust_markup_value);

    }

    $(document).on("keyup change", "input[id$=id_ref_price]", function (e) {
        updateNetCustomerPrice($(this))
    });

    $(document).on("keyup change", "input[id$=id_ref_net_cust_markup]", function (e) {
        updateNetCustomerPriceFromMarkup($(this))
    });

    $(document).on("keyup change", "input[id$=id_net_cust_price]", function (e) {
        updateNetCustomerMarkup($(this))
    });

    // Gross Customer Price

    function updateGrossCustomerPriceFromVAT(this_obj) {
        var net_customer_price_raw = $("input[id$=id_net_cust_price]").val()
        var gross_cust_price = ''
        if (this_obj.val() != '' && net_customer_price_raw != '') {
            var net_customer_price = parseFloat(net_customer_price_raw)
            var value = this_obj.val().split('%');
            var vat_amount = parseFloat(value.shift());
            if (isNaN(vat_amount)) {
                gross_cust_price = net_customer_price_raw
            } else {
                gross_cust_price = (net_customer_price + (vat_amount * net_customer_price) / 100).toFixed(2);
            }
        } else {
            gross_cust_price = net_customer_price_raw
        }
        $("input[id$=id_gross_cust_price]").val(gross_cust_price);
    }

    // Update Distributor price from gross customer distributor markup field

    function updateDistPriceFromCDistMarkup(this_obj) {
        var gross_customer_price_raw = $("input[id$=id_gross_cust_price]").val();
        var distributor_price = '';
        if (this_obj.val() != '' && gross_customer_price_raw != '') {
            var gross_customer_price = parseFloat(gross_customer_price_raw);
            var value = this_obj.val().split('%');
            var markup_amount = parseFloat(value.shift());
            if (isNaN(markup_amount)) {
                distributor_price = gross_customer_price_raw;
            } else {
                distributor_price = (gross_customer_price + (markup_amount * gross_customer_price) / 100).toFixed(2);
            }
        } else {
            distributor_price = gross_customer_price_raw;
        }
        $("input[id$=id_dist_price]").val(distributor_price);
    }

    // Update Gross customer markup from Distributor price

    function updateGrossCustomerMarkup(this_obj) {
        var gross_customer_price_raw = $("input[id$=id_gross_cust_price]").val();
        var gross_cust_markup = $("input[id$=id_gross_cust_distributor_markup]").val();
        var distributor_price = parseFloat(this_obj.val());
        var gross_cust_markup_value = ''
        if (this_obj.val() != '') {
            if ((gross_customer_price_raw != '' && gross_customer_price_raw != 'undefined')) {
                var gross_customer_price = parseFloat(gross_customer_price_raw);
                var subtracted_value = distributor_price - gross_customer_price
                var gross_cust_markup = (subtracted_value * 100) / distributor_price
                gross_cust_markup_value = gross_cust_markup.toFixed(2) + '%'
            }
        }
        $("input[id$=id_gross_cust_distributor_markup]").val(gross_cust_markup_value);

    }

    $(document).on("keyup change", "input[id$=id_vat_rate]", function (e) {
        updateGrossCustomerPriceFromVAT($(this))
    });

    // On change Gross customer distributor markup field update distributor price

    $(document).on("keyup change", "input[id$=id_gross_cust_distributor_markup]", function (e) {
        updateDistPriceFromCDistMarkup($(this))
    });

    // On change Distributor price field update Gross customer distributor markup

    $(document).on("keyup change", "input[id$=id_dist_price]", function (e) {
        updateGrossCustomerMarkup($(this))
    });


    // Retailer Price

    // Update Retailer price from Distributor Retailer markup field

    function updateRetPriceFromDistRetMarkup(this_obj) {
        var distributor_price_raw = $("input[id$=id_dist_price]").val();
        var retailer_price = '';
        if (this_obj.val() != '' && distributor_price_raw != '') {
            var distributor_price = parseFloat(distributor_price_raw);
            var value = this_obj.val().split('%');
            var markup_amount = parseFloat(value.shift());
            if (isNaN(markup_amount)) {
                retailer_price = distributor_price_raw;
            } else {
                retailer_price = (distributor_price + (markup_amount * distributor_price) / 100).toFixed(2);
            }
        } else {
            retailer_price = distributor_price_raw;
        }
        $("input[id$=id_retail_price]").val(retailer_price);
    }

    // Update distributor retailer markup on change retailer price field

    function updateRetailerMarkup(this_obj) {
        var dist_price_raw = $("input[id$=id_dist_price]").val();
        var retailer_price = parseFloat(this_obj.val());
        var dist_retailer_markup_value = ''
        if (this_obj.val() != '') {
            if ((dist_price_raw != '' && dist_price_raw != 'undefined')) {
                var dist_price = parseFloat(dist_price_raw);
                console.log(dist_price);
                console.log(retailer_price);
                var subtracted_value = retailer_price - dist_price
                var dist_retailer_markup = (subtracted_value * 100) / retailer_price
                dist_retailer_markup_value = dist_retailer_markup.toFixed(2) + '%'
            }
        }
        $("input[id$=id_dist_retailer_markup]").val(dist_retailer_markup_value);

    }

    // On change retailer markup field update Retailer price

    $(document).on("keyup change", "input[id$=id_dist_retailer_markup]", function (e) {
        updateRetPriceFromDistRetMarkup($(this))
    });

    // On change Retailer price field update distributor retailer markup

    $(document).on("keyup change", "input[id$=id_retail_price]", function (e) {
        updateRetailerMarkup($(this))
    });


    function ajax_call(data_url, complete_callback) {
        $.ajax(data_url,
            {
                dataType: "json"
            }).done(complete_callback);
    }

    if ($("input[id$=invoice]").length > 0) {
        if ($("input[id$=invoice]").hasClass("require-invoice")) {
            UpdateInvoiceAmountOnInvoiceSelect($("input[id$=invoice]"));
        }
    }
    $("input[id$=total_available_damage_amount]").prop("readonly", true);

    function UpdateInvoiceAmountOnInvoiceSelect(element) {
        element.on("change", function (e) {
            var _this_object = $(this);

            if (_this_object.val() != '') {
                $("input[id$=total_amount]").prop("readonly", true);
                $("input[id$=due_amount]").prop("readonly", true);
                $("input[id$=total_available_damage_amount]").prop("readonly", true);

                _this_object.prop("disabled", true);

                var id = $(this).val();
                var data_url = "/open-invoices/?format=json&id=" + id + "&search=1";

                ajax_call(data_url, function (data) {
                    var price_total = data.items[0].price_total;
                    var paid_total = data.items[0].actual_amount_paid;
                    var due_total = price_total - paid_total;
                    var client_id = data.items[0].counter_part;
                    //alert(client_id);

                    var data_url = "/approved-damage-products/?format=json&client:pk=" + client_id + "&search=1";
                    ajax_call(data_url, function (data) {

                        var total_available_damage_amount = 0.0;
                        var total_damage_amount = 0.0;
                        var total_applied_damage_amount = 0.0;
                        if (data.items.length != 0){
                            for (var i = 0; i < data.items.length; i++) {
                                var damage_amount = data.items[i].value;
                                total_damage_amount += damage_amount;
                            }

                            var data_url = "/payments/?format=json&client:pk=" + client_id + "&search=1";
                            ajax_call(data_url, function (data) {
                                if (data.items.length != 0){
                                    for (var i = 0; i < data.items.length; i++) {
                                        var applied_damage_amount = data.items[i].apply_damage_amount;
                                        total_applied_damage_amount += applied_damage_amount;
                                    }
                                }
                                total_available_damage_amount = total_damage_amount - total_applied_damage_amount;
                                _this_object.parent().parent().parent().find("input[id$=total_available_damage_amount]").val(total_available_damage_amount.toFixed(1));
                                $("input[id$=apply_damage_amount]").prop("max", total_available_damage_amount);
                                $("input[id$=apply_damage_amount]").css('min-width',160);
                            });

                        }
                        _this_object.parent().parent().parent().find("input[id$=total_available_damage_amount]").val(total_available_damage_amount.toFixed(1));
                        $("input[id$=apply_damage_amount]").prop("max", total_available_damage_amount);
                        $("input[id$=apply_damage_amount]").css('min-width',160);

                    });



                    _this_object.parent().parent().parent().find("input[id$=total_amount]").val(price_total.toFixed(1));
                    _this_object.parent().parent().parent().find("input[id$=due_amount]").val(due_total.toFixed(1));

                    _this_object.prop("disabled", false);

                });

            }

            if (_this_object.val() == '') {
                $("input[id$=total_amount]").prop("readonly", false);
                $("input[id$=due_amount]").prop("readonly", false);
                _this_object.prop("disabled", false);

                _this_object.parent().parent().parent().find("input[id$=total_amount]").val(0.0);
                _this_object.parent().parent().parent().find("input[id$=due_amount]").val(0.0);
            }
        });
    };



    updateCalculatedField();
    updateRefFields();

    $("select.select2").select2('destroy');
    $(".btn-inline-addmore").each(function () {
        buildFormsetDictionary(this);
    });
    //updateSelect2Fields();
    //loadBWSelect2Fields();
    console.log("Update select2 fields called.");
    $(".date-selector").datepicker({format: 'dd-mm-yyyy'});
    $(".datetimepicker").datetimepicker({
        pick12HourFormat: true
    });

    $(document).ready(function () {
//        function UpdateUnitPriceOnProductSelect(element){
//            console.log(element);
//            element.select2().on("change",function(e){
//            var _this_object = $(this);
//            _this_object.prop("disabled",true);
//            var id = $(this).val();
//            var data_url = "/products/?format=json&id="+ id +"&search=1";
//            //alert(data_url);
//            ajax_call(data_url,function(data){
//                var price_id = data.items[0].prices[4];
//                var data_url = "/product-price-value/?format=json&search=1&id="+price_id;
//                ajax_call(data_url,function(data){
//                    _this_object.parent().parent().parent().parent().find("input[id$=unit_price]").val(data.items[0].value.toFixed(1));
//                    _this_object.parent().parent().parent().parent().find("input[id$=unit_price]").change();
//                    _this_object.prop("disabled",false);
//                });
//            });
//        });
//        };
//
//        //console.log($("input[id$=unit_price]"));
//        if($("input[id$=unit_price]").length > 0){
//            //console.log($("input[id$=product]"));
//            UpdateUnitPriceOnProductSelect($("input[id$=product]"));
//        }
//
//        if($("input[id$=total_items]").length > 0){
//            UpdateUnitPriceOnProductSelect($("input[id$=product]"));
//        }
    });

});


