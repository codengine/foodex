/**
 *  * Created by Shamil on 03-Jan-16 10:09 AM
 * Organization FIS
 */

$(function () {

    $(document).on('click', 'table th input:checkbox', function () {
        var that = this;
        $(this).closest('table').find('tr > td:first-child input:checkbox')
            .each(function () {
                this.checked = that.checked;
                $(this).closest('tr').toggleClass('selected');
            })
            .first().change();
    });

    $(document).on('change', 'table tr td input:checkbox', function () {
        //var cbox = this;

        var total = $('table tr td input:checkbox:checked');
        if (total.length == 1) {
            $("a.partial-edit-action").removeAttr('disabled');
            $("a.partial-edit-action").each(function () {
                var cbox = $("input[name='selection']:checked");
                var data_url = $(this).attr('href').split('//')[0];
                var link = data_url + "//" + $(cbox).val();
                $(this).attr('href', link);
            });
        } else {
            $("a.partial-edit-action").attr('disabled', 'disabled');
        }
    });


    $(document).on('change', 'table tr td input:checkbox', function () {
        var total = $('table tr td input:checkbox:checked');
        if (total.length > 0) {
            $("a.delete-item").removeAttr('disabled');
        } else {
            $("a.delete-item").attr('disabled', 'disabled');
        }
    });

    $(document).on('change', 'table tr td input:checkbox', function () {
        var cbox = this;
        var total = $('table tr td input:checkbox:checked');
        if (total.length == 1) {
            $("a.single-action").removeAttr('disabled');
            $("a.single-action").each(function () {
                $(this).attr('href', $(this).data('url').replace("{0}", $(cbox).val()));
            });
        } else {
            $("a.single-action").attr('disabled', 'disabled');
        }
        if (total.length > 0) {
            $("a.multi-action").removeAttr('disabled');
            $("a.multi-action").each(function () {
                if ($(this).data('url') != undefined) {
                    $(this).attr('href', $(this).data('url').replace("{0}", $.map($('table tr td input:checkbox:checked'), function (e, i) {
                        return $(e).val();
                    }).join(",")));
                }
            });
        } else {
            $("a.multi-action").attr('disabled', 'disabled');
        }
    });
});