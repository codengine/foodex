import importlib

from django.conf.urls import patterns, url
from rest_framework.routers import DefaultRouter

from blackwidow.core.api.routers.router import CustomRouter
from blackwidow.core.api.mixins.viewsetmixin.viewset_mixin import GenericApiViewSetMixin
from blackwidow.core.api.views.api_login_view import ApiLoginView
from blackwidow.core.api.views.api_status_view import ApiStatusView
from blackwidow.core.generics.views.advanced_edit_view import AdvancedGenericEditView
from blackwidow.core.generics.views.advanced_export_view import AdvancedGenericExportView
from blackwidow.core.generics.views.advanced_import_view import AdvancedGenericImportView
from blackwidow.core.generics.views.advanced_download_view import AdvancedGenericDownloadView
from blackwidow.core.generics.views.approve_view import GenericApproveView
from blackwidow.core.generics.views.bulk_views.reject_view import GenericRejectView
from blackwidow.core.generics.views.create_view import GenericCreateView
from blackwidow.core.generics.views.delete_view import GenericDeleteView
from blackwidow.core.generics.views.details_view import GenericDetailsView
from blackwidow.core.generics.views.edit_view import GenericEditView
from blackwidow.core.generics.views.list_view import GenericListView
from blackwidow.core.generics.views.mutation_view import GenericMutationView
from blackwidow.core.generics.views.partial_views.partial_add_list_view import PartialGenericAddListView
from blackwidow.core.generics.views.partial_views.partial_create_view import PartialGenericCreateView
from blackwidow.core.generics.views.partial_views.partial_delete_view import PartialGenericDeleteView
from blackwidow.core.generics.views.partial_views.partial_edit_view import PartialGenericEditView
from blackwidow.core.generics.views.partial_views.partial_remove_list_view import PartialGenericRemoveListView
from blackwidow.core.generics.views.partial_views.partial_tab_list_view import PartialGenericTabListView
from blackwidow.core.generics.views.restore_view import GenericRestoreView
from blackwidow.core.generics.views.tree_view import GenericTreeView
from blackwidow.core.generics.views.printable_views.printable_details_view import GenericPrintableContentView
from blackwidow.engine.decorators.utility import get_models_with_decorator
from blackwidow.engine.managers.menumanager import MenuManager
from blackwidow.core.generics.views.download_view import GenericDownloadView
from blackwidow.core.generics.views.export_view import GenericExportView
from blackwidow.core.generics.views.import_view import GenericImportRunnerView
from config.apps import INSTALLED_APPS
from config.enums.view_action_enum import ViewActionEnum




# ------------------------- general routes start ---------------------------------------
# tuple information:
# ==================
# 0 -> Url Type
# 1 -> Route Url Suffix
# 2 -> Success Url Type
# 3 -> Requires Form to be mentioned?
# 4 -> The view class
from foodex.error.error_view import ErrorView404

operations = [(ViewActionEnum.Create, '/create', ViewActionEnum.Manage, True, GenericCreateView, None),
              (ViewActionEnum.Edit, '/edit/(?P<pk>(\d{,}))', ViewActionEnum.Manage, True, GenericEditView, None),
              (ViewActionEnum.Export, '/export', ViewActionEnum.Manage, False, GenericExportView, None),
              (ViewActionEnum.Download, '/download/(?P<pks>((\d(\,)*){,}))', ViewActionEnum.Manage, False, GenericDownloadView, None),
              (ViewActionEnum.AdvancedDownload, '/advanced-download/$', ViewActionEnum.Manage, False, AdvancedGenericDownloadView, None),
              (ViewActionEnum.Details, '/details/(?P<pk>(\d{,}))', ViewActionEnum.Details, False, GenericDetailsView, None),
              (ViewActionEnum.Delete, '/delete/(?P<ids>((\d(\,)*){,}))', ViewActionEnum.Manage, False, GenericDeleteView, None),
              (ViewActionEnum.Restore, '/restore/(?P<ids>((\d(\,)*){,}))', ViewActionEnum.Manage, False, GenericRestoreView, None),
              (ViewActionEnum.Mutate, '/mutate/(?P<ids>((\d(\,)*){,}))', ViewActionEnum.Manage, False, GenericMutationView, None),
              (ViewActionEnum.Approve, '/approve/(?P<ids>((\d(\,)*){,}))', ViewActionEnum.Manage, False, GenericApproveView, None),
              (ViewActionEnum.Reject, '/reject/(?P<ids>((\d(\,)*){,}))', ViewActionEnum.Manage, False, GenericRejectView, None),
              (ViewActionEnum.Tab, "/tabs/(?P<pk>(\d{,}|\w{,}))/(?P<tab>(\w{,}))", ViewActionEnum.Details, False, PartialGenericTabListView, None),
              (ViewActionEnum.Import, '/import', ViewActionEnum.Manage, False, GenericImportRunnerView, None),
              (ViewActionEnum.RunImporter, '/run-importer', ViewActionEnum.Manage, False, GenericImportRunnerView, None),
              (ViewActionEnum.Tree, '/tree', ViewActionEnum.Tree, False, GenericTreeView, None),
              (ViewActionEnum.Print, '/print/(?P<pk>(\d{,}))', ViewActionEnum.Print, False, GenericPrintableContentView, None),
              (ViewActionEnum.PartialCreate, '/partial-create/(?P<parent_id>(\d{,}))/(?P<tab>(\w{,}))', ViewActionEnum.PartialCreate, True, PartialGenericCreateView, None),
              (ViewActionEnum.PartialDelete, '/partial-delete/(?P<parent_id>(\d{,}))/(?P<tab>(\w{,}))', ViewActionEnum.PartialDelete, False, PartialGenericDeleteView, None),
              (ViewActionEnum.PartialEdit, '/partial-edit/(?P<parent_id>(\d{,}))//(?P<pk>(\d{,}))', ViewActionEnum.PartialEdit, True, PartialGenericEditView, None),
              (ViewActionEnum.AdvancedExport, '/advanced-export/$', ViewActionEnum.AdvancedExport, True, AdvancedGenericExportView, None),
              (ViewActionEnum.AdvancedImport, '/advanced-import/$', ViewActionEnum.AdvancedImport, True, AdvancedGenericImportView, None),
              (ViewActionEnum.AdvancedEdit, '/advanced-edit/$', ViewActionEnum.AdvancedEdit, True, AdvancedGenericEditView, None),
              (ViewActionEnum.PartialBulkAdd, '/bulk-add/(?P<pk>(\d{,}))/(?P<tab>(\w{,}))', ViewActionEnum.PartialBulkAdd, False, PartialGenericAddListView, None),
              (ViewActionEnum.PartialBulkRemove, '/bulk-remove/(?P<pk>(\d{,}))/(?P<tab>(\w{,}))', ViewActionEnum.PartialBulkRemove, False, PartialGenericRemoveListView, None),
              (ViewActionEnum.Manage, '/$', ViewActionEnum.Manage, False, GenericListView, None)]

urlpatterns = MenuManager.generate_urls(operations)

for _app in INSTALLED_APPS:
    try:
        _url_module = importlib.import_module(_app + '.urls')
        _app_urls = getattr(_url_module, 'urlpatterns')
        urlpatterns += _app_urls
    except ImportError as exp:
        print(exp)
# ------------------------- general routes end -----------------------------------------

# ------------------------- api routes start -------------------------------------------
router = CustomRouter()

all_models = get_models_with_decorator('expose_api', INSTALLED_APPS, include_class=True)
for m in all_models:
    viewset_subclass = type(
        'RunTime_' + m.__name__ + '_ViewSet',
        (GenericApiViewSetMixin,),
        dict(
            model=m,
            serializer_class=m.get_serializer())
    )
    router.register(r'api/' + m._url_prefix, viewset_subclass)

_d_router = DefaultRouter()
urlpatterns += patterns('', url(r'^api$', _d_router.get_api_root_view(), name=_d_router.root_view_name))
urlpatterns += router.urls

for _app in INSTALLED_APPS:
    try:
        _url_module = importlib.import_module(_app + '.api.urls')
        _app_urls = getattr(_url_module, 'urlpatterns')
        urlpatterns += _app_urls
    except ImportError as exp:
        print(exp)

urlpatterns += patterns('',
                        url(r'^api/login', ApiLoginView.as_view()),
                        url(r'^404-error', ErrorView404.as_view(), name="404-error"),
                        url(r'^api/status', ApiStatusView.as_view()),
)

# ------------------------- api routes end --------------------------------------------

# --------------error 404 --- only for production-------------------

from django.views.generic.base import RedirectView

handler404 = RedirectView.as_view(url='/404-error/')