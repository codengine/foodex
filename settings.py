# Django settings for blackwidow project.
from django.conf.global_settings import AUTHENTICATION_BACKENDS

from django.conf.urls import patterns, url, include
from config.database import DATABASES
from config.session import SESSION_ENGINE
from config.test_settings import TEST_RUNNER
from config.celery_config import *
from config.email_config import *
from config.theme import *
from config.rest_framework_config import REST_FRAMEWORK, API_LOGIN_URL
import os
# from config.model_translation import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
# ('Your Name', 'your_email@example.com'),
)

PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))
MANAGERS = ADMINS

ALLOWED_HOSTS = ['*']

APPEND_SLASH = True


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Asia/Dhaka'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-US'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = False

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = PROJECT_PATH
MEDIA_URL = '/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"

from config.theme import *

SUB_SITE = ''
LOGIN_URL = '/account/login'
z_URL = '/static/'
STATICFILES_DIRS = (os.path.join(PROJECT_PATH, "static/"),)

STATIC_ROOT = os.path.join(PROJECT_PATH, 'static_media/')
STATIC_UPLOAD_ROOT = os.path.join(STATIC_ROOT, 'uploads/')
STATIC_URL = '/static/'
EXPORT_FILE_ROOT = os.path.join(PROJECT_PATH, STATIC_ROOT, 'exported-files')
IMAGE_UPLOAD_ROOT = os.path.join(PROJECT_PATH, STATIC_ROOT, 'images')
APK_UPLOAD_ROOT = os.path.join(STATIC_ROOT, 'fieldbuzz_applications/')
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'b%_^^c*72#h)wy%(y%_-9d6*_8@1@*2h9m&@wekv4)a9dirmk^'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)


# template context processors
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.request',

)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'blackwidow.core.middlewares.ProtectedFormPostMiddleWare',
    'django.middleware.locale.LocaleMiddleware',
    'crequest.middleware.CrequestMiddleware',
)

#Authentication Backend (for allauth)
AUTHENTICATION_BACKENDS = (
    "django.contrib.auth.backends.ModelBackend",
    "allauth.account.auth_backends.AuthenticationBackend",
)

AUTH_PROFILE_MODULE = 'core.ConsoleUser'

ROOT_URLCONF = 'urls'
# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'wsgi.application'

TEMPLATE_DIRS = (PROJECT_PATH, os.path.join(PROJECT_PATH, 'templates/'))

from config.apps import INSTALLED_APPS as BW_APPS

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    # 'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'modeltranslation',
    'django.contrib.admin.apps.SimpleAdminConfig',
    'widget_tweaks',
    'django_tables2',
    'djcelery',
    'extra_views',
    'rest_framework',
    'rest_framework.authtoken',
    'compressor',
    'blackwidow.scheduler',
    'crequest',
    'kombu.transport.django',
    # 'debug_toolbar'
    # 'south'
)

INSTALLED_APPS += BW_APPS

FILE_UPLOAD_HANDLER = ("django.core.files.uploadhandler.MemoryFileUploadHandler",
                     "django.core.files.uploadhandler.TemporaryFileUploadHandler",)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.simple_backend.SimpleEngine',
        },
    }

# debug_toolbar settings
# if DEBUG:
#     INTERNAL_IPS = ('127.0.0.1',)
#     MIDDLEWARE_CLASSES += (
#         'debug_toolbar.middleware.DebugToolbarMiddleware',
#     )
#
#     INSTALLED_APPS += (
#         'debug_toolbar',
#     )
#
#     DEBUG_TOOLBAR_PANELS = [
#         'debug_toolbar.panels.versions.VersionsPanel',
#         'debug_toolbar.panels.timer.TimerPanel',
#         'debug_toolbar.panels.settings.SettingsPanel',
#         'debug_toolbar.panels.headers.HeadersPanel',
#         'debug_toolbar.panels.request.RequestPanel',
#         'debug_toolbar.panels.sql.SQLPanel',
#         'debug_toolbar.panels.staticfiles.StaticFilesPanel',
#         'debug_toolbar.panels.templates.TemplatesPanel',
#         'debug_toolbar.panels.cache.CachePanel',
#         'debug_toolbar.panels.signals.SignalsPanel',
#         'debug_toolbar.panels.logging.LoggingPanel',
#         'debug_toolbar.panels.redirects.RedirectsPanel',
#         ]
#
#     DEBUG_TOOLBAR_CONFIG = {
#         'INTERCEPT_REDIRECTS': False,
#     }
#
#     CONFIG_DEFAULTS = {
#         # Toolbar options
#         'RESULTS_STORE_SIZE': 3,
#         'SHOW_COLLAPSED': True,
#         # Panel options
#         'INTERCEPT_REDIRECTS': True,
#         'SQL_WARNING_THRESHOLD': 100,   # milliseconds
#     }


gettext = lambda s: s
LANGUAGES = (
    ('en', gettext('English')),
    ('bd', gettext('Bangla')),
)
MODELTRANSLATION_LANGUAGES = ('en', 'bd')
MODELTRANSLATION_PREPOPULATE_LANGUAGE = 'en'

#debug_toolbar settings
# if DEBUG:
#     INTERNAL_IPS = ('127.0.0.1',)
#     MIDDLEWARE_CLASSES += (
#         'debug_toolbar.middleware.DebugToolbarMiddleware',
#     )
#
#     INSTALLED_APPS += (
#         'debug_toolbar',
#     )
#
#     DEBUG_TOOLBAR_PANELS = [
#         'debug_toolbar.panels.versions.VersionsPanel',
#         'debug_toolbar.panels.timer.TimerPanel',
#         'debug_toolbar.panels.settings.SettingsPanel',
#         'debug_toolbar.panels.headers.HeadersPanel',
#         'debug_toolbar.panels.request.RequestPanel',
#         'debug_toolbar.panels.sql.SQLPanel',
#         'debug_toolbar.panels.staticfiles.StaticFilesPanel',
#         'debug_toolbar.panels.templates.TemplatesPanel',
#         'debug_toolbar.panels.cache.CachePanel',
#         'debug_toolbar.panels.signals.SignalsPanel',
#         'debug_toolbar.panels.logging.LoggingPanel',
#         'debug_toolbar.panels.redirects.RedirectsPanel',
#     ]
#
#     DEBUG_TOOLBAR_CONFIG = {
#         'INTERCEPT_REDIRECTS': False,
#     }
